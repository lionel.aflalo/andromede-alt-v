const path          = require('path');
const tail          = require('zo-tail-f')
const readLastLines = require('read-last-lines').read;
const pretty        = require('pino-pretty')({colorize: true, translateTime: true});

const file    = path.resolve(__dirname, '../resources/core/logs/info.log');
const watcher = tail.watch(file, {interval: 1000});

readLastLines(file, 50).then(str => {

  const lines = str.split("\n");

  for(let i=0; i<lines.length; i++)
    process.stdout.write(pretty(lines[i]));

  watcher.on('line', line => {
    process.stdout.write(pretty(line));
  });

});
