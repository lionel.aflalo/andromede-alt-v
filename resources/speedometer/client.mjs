import * as alt from 'alt';
import {getEntitySpeed, getVehicleLightsState} from 'natives';

// More precise but less performances
const UseNative = true;

// Fuel Settings
const FuelActive = true;
const FuelMetaName = "fuel";

// Lights Settings
const EnableLights = true;

// Turn Lights Settings (Not Working ATM)
const EnableTurnLights = false;

let SpeedoView = new alt.WebView("http://resources/speedometer/html/index.html");

let InVehicle = false;
let CurrLights = false;
let CurrHighbeams = false;

function ShowSpeedo() {
	SpeedoView.emit("showSpeedo");
}

function HideSpeedo() {
	SpeedoView.emit("hideSpeedo");
}

function UpdateSpeedo(speed) {
	SpeedoView.emit("update", Math.ceil(speed));
}

function SetLightsState(state) {
	SpeedoView.emit("lights", state);
}

async function CheckLights(lights, highbeams) {
	if (CurrLights == lights && CurrHighbeams == highbeams)
		return;
	CurrLights = lights;
	CurrHighbeams = highbeams;
	SetLightsState(((lights == false && highbeams == false) ? 0 : ((lights == true && highbeams == false) ? 1 : 2)));
}

function UpdateFuel(fuelLevel) {
	SpeedoView.emit("fuel", fuelLevel);
}

SpeedoView.on('ready', () => {
	if (!FuelActive)
		SpeedoView.emit("disableFuel");
	if (!EnableLights)
		SpeedoView.emit("disableLights");
	if (!EnableTurnLights)
		SpeedoView.emit("disableTurnLights");
	HideSpeedo();
});

alt.on('update', () => {
	if (!InVehicle) {
		if (alt.Player.local.vehicle != null) {
			InVehicle = true;
			ShowSpeedo();
		}
	} else {
		if (alt.Player.local.vehicle == null) {
			HideSpeedo();
			return InVehicle = false;
		}
		if (!UseNative)
			UpdateSpeedo(alt.Player.local.vehicle.speed * 3.6);
		else
			UpdateSpeedo(getEntitySpeed(alt.Player.local.vehicle.scriptID) * 3.6);

		const [_, lights, highbeams] = getVehicleLightsState(alt.Player.local.vehicle.scriptID);
		CheckLights(lights, highbeams);
	}
});

if (FuelActive) {
	alt.setInterval(() => {
		if (InVehicle)
			UpdateFuel(alt.Player.local.vehicle.getSyncedMeta(FuelMetaName));
	}, 500);
}