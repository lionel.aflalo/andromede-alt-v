/// <reference path="./typings/altv-client.d.ts" />

const CUBE_SIZE = 100;

/* Libs */
import * as alt      from 'alt'
import * as natives  from 'natives'
import $             from '../lib/altv-api/client/index';
import * as entities from './entities/index';

const { joaat, delay, waitFor, nextTick } = $.utils;
const { fadeIn, fadeOut }                 = $.helpers;
const { Model }                           = $.types;
const { Vector3 }                         = $.math;

/* Modules */
import Cache              from './modules/cache';
import Console            from './modules/console';
import HUD                from './modules/hud';
import World              from './modules/world/index';
import WorldSelect        from './modules/worldselect';
import WorldSelectActions from './modules/worldselectactions';
import Skin               from './modules/skin';
import WM                 from './modules/windowmanager';
import MM                 from './modules/menumanager';
import Inventory          from './modules/inventory/index';
import OS                 from './modules/os';
import Interactable       from './modules/interactable/index';
import Vehicle            from './modules/vehicle/index';
import Area               from './modules/area/index';
import Notification       from './modules/notification/index';
import Interact           from './modules/interact/index';
import CarDealer          from './modules/cardealer/index';
import GizzTest           from './modules/gizztest/index';
import ATM                from './modules/atm/index';
import NPC                from './modules/npc/index';
import Shop               from './modules/shop/index';
import CasinoRoulette     from './modules/casino.roulette/index';
import Nightclub          from './modules/nightclub/index';
import Status             from './modules/status/index';
import Blip               from './modules/blip/index';

/* Main */

const Core = {
  ready     : false,
  callbackId: 0,
  callbacks : {},
  cubeSize  : CUBE_SIZE,
  cube      : {x: 0, y: 0, z: 0},
  worldId   : null,
};

Core.refreshPlayerSkins = function() {

  alt.Player.all
    .filter(player => player != alt.Player.local && $.utils.getDistance(alt.Player.local.pos, player.pos) <= 500)
    .forEach(async player => {
      await $.utils.waitFor(() => !!player.ped && !!player.getSyncedMeta('model'));
      const model = player.getSyncedMeta('model');
      player.ped.headBlendData.set(model.headBlendData);
    })
  ;

}

$.game.on('ready', async () => {
  
  alt.log('[core] Game ready');

  try {

    /* Init */
    natives.destroyAllCams(false);

    /* Init modules */
    const initMod = mod => {
      
      return new Promise((resolve, reject) => {

        if(typeof mod.init !== 'undefined') {
          
          const timeout = alt.setTimeout(() => {

            if(!mod.__LOADED) {
              alt.logError(`[core/${mod.__NAME}] load error`);
              reject();
            }
  
          }, 5000);

          mod
            .init()
            .then(() => {

              alt.clearTimeout(timeout);

              mod.__LOADED = true;
              alt.log(`[core/${mod.__NAME}] loaded`);
              resolve();
              
            })
            .catch(e => { throw e; })
          ;

        } else {
          
          mod.__LOADED = true;
          alt.log(`[core/${mod.__NAME}] loaded`);
          resolve()
          
        }

      });
      
    };

    const promises = [];

    /* Main modules */
    [
      Cache,
      Console,
      HUD,
      WM,
      MM,
    ].map(initMod).forEach(p => promises.push(p));
  
    await Promise.all(promises).catch(e => { throw e; });;

    promises.length = 0;

    /* Other modules */
    [
      World,
      WorldSelect,
      WorldSelectActions,
      Skin,
      Inventory,
      OS,
      Interactable,
      Vehicle,
      Area,
      Status,
      Notification,
      Interact,
      CarDealer,
      ATM,
      NPC,
      Shop,
      CasinoRoulette,
      Nightclub,
      Blip,
    ].map(initMod).forEach(p => promises.push(p));

    await Promise.all(promises).catch(e => { throw e; });;

    promises.length = 0;

    /* Test */
    [
      GizzTest,
    ].map(initMod).forEach(p => promises.push(p));

    await Promise.all(promises).catch(e => { throw e; });;

    Core.ready = true;

    alt.log('[core] Loaded all modules');

    /* Main */
    natives.displayRadar(false);

  } catch(e) { alt.logError(`${e.message} ${e.stack}`); }

});

alt.onServer('hz:player:connect', async () => {
  
  try {

    await waitFor(() => Core.ready);

    const discordInfos = alt.discordInfo();
    const identifiers  = {};
  
    identifiers.license = alt.getLicenseHash();
  
    if(discordInfos) {
      identifiers.discord = discordInfos.id;
    }
    
    alt.emitServer('hz:player:connect:ok', identifiers, discordInfos ? discordInfos.name : 'unnamed');

  } catch(e) { alt.logError(e); }

});

alt.onServer('hz:player:init', async (playerId, worldId) => {
  
  try {

    alt.Player.local.$id = playerId;
    Core.worldId         = worldId;

    const hzPlayer = await Cache.resolve({_type: 'Player', _id: playerId});

    if(!hzPlayer.identity) {
      
      const model   = new Model('mp_m_freemode_01');
      const success = await model.request();

      if(success) {

        await $.requestp('hz:player:model:set', +model);
        $.game.player.ped.components.setDefault();
        $.game.player.ped.heading  = 180.0;
        $.game.player.ped.position = new Vector3(402.99267578125, -996.694091796875, -99.000244140625);
    
        await Skin.create();
    
        alt.emitServer('hz:player:init:ok', {
          hash         : +$.game.player.model,
          headBlendData: $.game.player.ped.headBlendData.get(),
          faceFeatures : $.game.player.ped.faceFeatures.get(),
          components   : $.game.player.ped.components.get(),
          props        : $.game.player.ped.props.get(),
          overlays     : $.game.player.ped.overlays.get(),
        });

        Core.refreshPlayerSkins();

      } else alt.logError('Error loading player model');
  
    } else {

      const hZIdentity = hzPlayer.identity;
      const model      = new Model(hZIdentity.model.hash);

      const success = await model.request();

      if(success) {

        alt.log('hz:player:init => changed model');

        await $.requestp('hz:player:model:set', hZIdentity.model.hash || +model);

        await $.utils.waitFor(() => $.game.player.ped && $.game.player.ped.exists);

        const ped = $.game.player.ped;

        ped.components.set(hZIdentity.model.components || {});
        ped.overlays.set(hZIdentity.model.overlays     || {});
  
        ped.headBlendData.set(hZIdentity.model.headBlendData);

        ped.faceFeatures.set(hZIdentity.model.faceFeatures);
        ped.props.set(hZIdentity.model.props || {});
  
        alt.emitServer('hz:player:init:ok');

        Core.refreshPlayerSkins();

      } else alt.logError('Error loading player model');

    }

    const position = $.game.player.ped.position;

    $.game.emit('hz:player:init:done');

    const curr = {};
  
    curr.x = (position.x - (position.x % CUBE_SIZE)) / CUBE_SIZE;
    curr.y = (position.y - (position.y % CUBE_SIZE)) / CUBE_SIZE;
    curr.z = (position.z - (position.z % CUBE_SIZE)) / CUBE_SIZE;

    alt.setTimeout(() => {
      $.game.emit('hz:cube:change', curr);
    }, 1000);

    const vehicles = alt.Vehicle.all;

    for(let i=0; i<vehicles.length; i++) {
  
      const vehicle   = vehicles[i];

      if($.utils.getDistance(alt.Player.local.pos, vehicle.pos) <= 200) {

        const _id       = vehicle.getSyncedMeta('_id');
        const hzVehicle = await $.requestp('hz:entity:get', 'Vehicle', _id);
  
        if(hzVehicle) {
      
          {
            const { r, g, b} = hzVehicle.customPrimaryColor;
            natives.setVehicleCustomPrimaryColour(vehicle.scriptID, r, g, b);
          }
      
          {
            const { r, g, b} = hzVehicle.customSecondaryColor;
            natives.setVehicleCustomSecondaryColour(vehicle.scriptID, r, g, b);
          }
      
        }  

      }
      
    }

  } catch(e) { alt.logError('hz:player:init => ' + e); }

});

alt.onServer('hz:task:warpIntoVehicle', async vehicle => {
  
    await new Model(vehicle.model).request();
    await delay(0);

    natives.setPedIntoVehicle(+$.game.player.ped, vehicle.scriptID, -1);
});

alt.onServer('hz:time:set', (time, msPerGameMinute) => {

  const date = new Date(time * 1000);

  natives.setClockDate(date.getDay() + 1, date.getMonth(), date.getYear());
  natives.setClockTime(date.getHours(), date.getMinutes(), date.getSeconds());

  alt.setMsPerGameMinute(msPerGameMinute);

});

alt.on('syncedMetaChange', async (entity, key, value) => {

  if((entity instanceof alt.Player) && (key === 'model')) {

    const model   = new Model(value.hash)
    const success = await model.request();

    if(success) {

      await $.utils.waitFor(() => entity.ped && entity.ped.exists);

      const ped = entity.ped;

      if(entity === alt.Player.local) {
      
        if(value.components) {

          alt.log('syncedMetaChange => changed model');

          ped.components.setDefault();

          const components = {};

          const hasTorso = value.components[11] !== undefined;

          if(+model === $.utils.joaat('mp_m_freemode_01')) {

            components[3]  = {drawable: 15, texture: 0, palette: 0};
            components[4]  = {drawable: 14, texture: 0, palette: 0};
            components[6]  = {drawable: 34, texture: 0, palette: 0};
            components[8]  = {drawable: 15, texture: 0, palette: 0};
            components[11] = {drawable: 15, texture: 0, palette: 0};

          } else {

            components[3]  = {drawable: 15, texture: 0, palette: 0};
            components[4]  = {drawable: 14, texture: 0, palette: 0};
            components[6]  = {drawable: 35, texture: 0, palette: 0};
            components[8]  = {drawable: 15, texture: 0, palette: 0};
            components[11] = {drawable: 15, texture: 0, palette: 0};
          
          }

          for(let k in value.components)
            components[k] = value.components[k];

          if(!hasTorso)
            components[3] = {drawable: 15, texture: 0, palette: 0};

          ped.components.set(components);

        }

        if(value.props) {

          ped.props.setDefault();
          ped.props.set(value.props);
        }
  
      }

      ped.overlays.set(value.overlays || {});
      ped.headBlendData.set(value.headBlendData);
      ped.faceFeatures.set(value.faceFeatures);

    } else alt.logError('Error loading model', +model);
  }

});

let   inVehicle        = false;
const vehiclesInRadius = {};

alt.setInterval(async () => {

  if($.game.player.vehicle && !inVehicle) {
    inVehicle = true;
    natives.displayRadar(true);
  } else if(!$.game.player.vehicle && inVehicle) {
    inVehicle = false;
    natives.displayRadar(false);
  }

  natives.invalidateIdleCam();
  
  const vehicles = alt.Vehicle.all;

  for(let i=0; i<vehicles.length; i++) {

    const vehicle = vehicles[i];
    const _id     = vehicle.getSyncedMeta('_id');

    if(!vehiclesInRadius[_id] && $.utils.getDistance(alt.Player.local.pos, vehicle.pos) <= 200) {

      vehiclesInRadius[_id] = true;
      const hzVehicle = await $.requestp('hz:entity:get', 'Vehicle', _id);

      if(hzVehicle) {
    
        {
          const { r, g, b} = hzVehicle.customPrimaryColor;
          natives.setVehicleCustomPrimaryColour(vehicle.scriptID, r, g, b);
        }
    
        {
          const { r, g, b} = hzVehicle.customSecondaryColor;
          natives.setVehicleCustomSecondaryColour(vehicle.scriptID, r, g, b);
        }
    
      }
    

    } else if(vehiclesInRadius[_id] && $.utils.getDistance(alt.Player.local.pos, vehicle.pos) > 200) {

      vehiclesInRadius[_id] = false;
      delete Cache.data[_id];

    }

  }

}, 1000);

alt.setInterval(async () => {

  const position = alt.Player.local.pos;

  const curr = {};

  curr.x = (position.x - (position.x % CUBE_SIZE)) / CUBE_SIZE;
  curr.y = (position.y - (position.y % CUBE_SIZE)) / CUBE_SIZE;
  curr.z = (position.z - (position.z % CUBE_SIZE)) / CUBE_SIZE;

  if((curr.x !== Core.cube.x) || (curr.y !== Core.cube.y) || (curr.z !== Core.cube.z)) {

    Core.cube.x = curr.x;
    Core.cube.y = curr.y;
    Core.cube.z = curr.z;

    $.game.emit('hz:cube:change', curr);
  }

}, 500);

$.game.on('hz:cube:change', async cube => {

  // alt.log('cube => ' + JSON.stringify(cube));

  Core.refreshPlayerSkins();

});

export default Core;