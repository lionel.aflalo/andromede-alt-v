import HZEntity from './entity';

class HZArea extends HZEntity {

  static get _type() { return "Area"; }
  static get _subdocs() { return []; }

  get _subdocs() { return HZArea._subdocs; }
  get _id() { return this.data['_id']; }
  get _data() { return this.data['_data']; }
  get _type() { return this.data['_type']; }
  get name() { return this.data['name']; }
  get points() { return this.data['points']; }
  get tags() { return this.data['tags']; }

  set _id(val) { this.data['_id'] = val; }
  set _data(val) { this.data['_data'] = val; }
  set _type(val) { this.data['_type'] = val; }
  set name(val) { this.data['name'] = val; }
  set points(val) { this.data['points'] = val; }
  set tags(val) { this.data['tags'] = val; }

  constructor(data = {}) {

    super(data);

    this.data = {};

    this.data['_id'] = null;
    this.data['_data'] = {};
    this.data['_type'] = "Area";
    this.data['name'] = undefined;
    this.data['points'] = [];
    this.data['tags'] = [];

    this.subdocs = [];

    for(let k in data)
      this.data[k] = data[k]

  }

  // Given three colinear points p, q, r, the function checks if
  // point q lies on line segment 'pr'
  static onSegment(p, q, r) {

    if (
      q.x <= Math.max(p.x, r.x) &&
      q.x >= Math.min(p.x, r.x) &&
      q.y <= Math.max(p.y, r.y) &&
      q.y >= Math.min(p.y, r.y)
    ) return true;
    
    return false;
  }

  // To find orientation of ordered triplet (p, q, r).
  // The function returns following values
  // 0 --> p, q and r are colinear
  // 1 --> Clockwise
  // 2 --> Counterclockwise
  static orientation(p, q, r) {

    const val = (q.y - p.y) * (r.x - q.x) - (q.x - p.x) * (r.y - q.y);

    if (val == 0) return 0;
      return (val > 0) ? 1 : 2; // clock or counterclock wise
  }
    
  // The function that returns true if line segment 'p1q1'
  // and 'p2q2' intersect.
  static doIntersect(p1, q1, p2, q2) {

    // Find the four orientations needed for general and
    // special cases
    const o1 = this.orientation(p1, q1, p2);
    const o2 = this.orientation(p1, q1, q2);
    const o3 = this.orientation(p2, q2, p1);
    const o4 = this.orientation(p2, q2, q1);
  
    // General case
    if (o1 != o2 && o3 != o4)
        return true;
  
    // Special Cases
    // p1, q1 and p2 are colinear and p2 lies on segment p1q1
    if (o1 == 0 && this.onSegment(p1, p2, q1))
      return true;
  
    // p1, q1 and p2 are colinear and q2 lies on segment p1q1
    if (o2 == 0 && this.onSegment(p1, q2, q1))
      return true;
  
    // p2, q2 and p1 are colinear and p1 lies on segment p2q2
    if (o3 == 0 && this.onSegment(p2, p1, q2))
      return true;
  
      // p2, q2 and q1 are colinear and q1 lies on segment p2q2
    if (o4 == 0 && this.onSegment(p2, q1, q2))
      return true;
  
    return false; // Doesn't fall in any of the above cases
  }

  // Returns true if the point p lies inside the polygon[] with n vertices
  static isInside(p, polygon) {

      const pLength = polygon.length;

      // There must be at least 3 vertices in polygon[]
      if (pLength.length < 3)
        return false;
    
      // Create a point for line segment from p to infinite
      const extreme = {x: 1e9, y: p.y};
    
      // Count intersections of the above line with sides of polygon
      let count = 0;
      let i     = 0;

      do {
        const next = (i+1) % pLength ;
  
        // Check if the line segment from 'p' to 'extreme' intersects
        // with the line segment from 'polygon[i]' to 'polygon[next]'
        if (HZArea.doIntersect(polygon[i], polygon[next], p, extreme)) {
          // If the point 'p' is colinear with line segment 'i-next',
          // then check if it lies on segment. If it lies, return true,
          // otherwise false
          if (HZArea.orientation(polygon[i], p, polygon[next]) == 0)
            return HZArea.onSegment(polygon[i], p, polygon[next]);
  
          count++;
        }

        i = next;

      } while (i != 0);
    
      // Return true if count is odd, false otherwise
      return count % 2 == 1;
  }

  static getBoundingBox(polygon) {

    const min = {x:  +1e9, y: +1e9};
    const max = {x:  -1e9, y: -1e9};

    for(let i=0; i<polygon.length; i++) {
      if (polygon[i].x < min.x) min.x = polygon[i].x;
      if (polygon[i].y < min.y) min.y = polygon[i].y;
      if (polygon[i].x > max.x) max.x = polygon[i].x;
      if (polygon[i].y > max.y) max.y = polygon[i].y;
    }

    return {min, max};

  }

  static getGridPoints(polygon, offset) {

    const points = []
    const bb     = this.getBoundingBox(polygon);
    const min    = {x: bb.min.x - (bb.min.x % offset) - offset, y: bb.min.y - (bb.min.y % offset) - offset};
    const max    = {x: bb.max.x - (bb.max.x % offset) + offset, y: bb.max.y - (bb.max.y % offset) + offset};

    for (let i = min.x; i < max.x; i += offset) {
      for (let j = min.y; j< max.y; j += offset) {

      const p = {x: i, y: j}

      if(this.isInside(polygon, p))
        points.push(p);
      }
    }

    return points

  }

  isInside(p) {
    return HZArea.isInside(p, this.points);
  }

  getBoundingBox() {
    return HZArea.getBoundingBox(this.points);
  }

  getGridPoints(offset) {
    return HZArea.getGridPoints(this.points, offset);
  }
}

export default HZArea;
