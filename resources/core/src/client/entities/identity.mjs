import HZEnrolable from './enrolable';

class HZIdentity extends HZEnrolable {

  static get _type() { return "Identity"; }
  static get _subdocs() { return [{"name":"containers","type":"IdentityBaseContainer","isArray":true}]; }

  get _subdocs() { return HZIdentity._subdocs; }
  get _id() { return this.data['_id']; }
  get _data() { return this.data['_data']; }
  get roles() { return this.data['roles']; }
  get _type() { return this.data['_type']; }
  get birthDate() { return this.data['birthDate']; }
  get residentNumber() { return this.data['residentNumber']; }
  get weight() { return this.data['weight']; }
  get size() { return this.data['size']; }
  get model() { return this.data['model']; }
  get health() { return this.data['health']; }
  get armour() { return this.data['armour']; }
  get loadout() { return this.data['loadout']; }
  get position() { return this.data['position']; }
  get containers() { return this.data['containers']; }
  get containersEnsured() { return this.data['containersEnsured']; }
  get status() { return this.data['status']; }

  set _id(val) { this.data['_id'] = val; }
  set _data(val) { this.data['_data'] = val; }
  set roles(val) { this.data['roles'] = val; }
  set _type(val) { this.data['_type'] = val; }
  set birthDate(val) { this.data['birthDate'] = val; }
  set residentNumber(val) { this.data['residentNumber'] = val; }
  set weight(val) { this.data['weight'] = val; }
  set size(val) { this.data['size'] = val; }
  set model(val) { this.data['model'] = val; }
  set health(val) { this.data['health'] = val; }
  set armour(val) { this.data['armour'] = val; }
  set loadout(val) { this.data['loadout'] = val; }
  set position(val) { this.data['position'] = val; }
  set containers(val) { this.data['containers'] = val; }
  set containersEnsured(val) { this.data['containersEnsured'] = val; }
  set status(val) { this.data['status'] = val; }

  get ttl() { return this.data['ttl']; }
  get _subdocs() { return this.data['_subdocs']; }
  get _typeChain() { return this.data['_typeChain']; }
  get persist() { return this.data['persist']; }

  set ttl(val) { this.data['ttl'] = val; }
  set _subdocs(val) { this.data['_subdocs'] = val; }
  set _typeChain(val) { this.data['_typeChain'] = val; }
  set persist(val) { this.data['persist'] = val; }

  constructor(data = {}) {

    super(data);

    this.data = {};

      /* Fields */
    this.data['_id'] = null;
    this.data['_data'] = {};
    this.data['roles'] = [];
    this.data['_type'] = "Identity";
    this.data['birthDate'] = 662601600;
    this.data['residentNumber'] = 6557348799;
    this.data['weight'] = 45;
    this.data['size'] = 150;
    this.data['model'] = undefined;
    this.data['health'] = 200;
    this.data['armour'] = 0;
    this.data['loadout'] = [];
    this.data['position'] = null;
    this.data['containers'] = [];
    this.data['containersEnsured'] = false;
    this.data['status'] = {};
      /* Props */
    this.data['ttl'] = undefined;
    this.data['_subdocs'] = undefined;
    this.data['_typeChain'] = undefined;
    this.data['persist'] = undefined;

    this.subdocs = [{"name":"containers","typeChain":["Item","Container","IdentityBaseContainer"],"isArray":true}];

    for(let k in data)
      this.data[k] = data[k]

  }
}

export default HZIdentity;
