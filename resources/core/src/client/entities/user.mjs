import HZEnrolable from './enrolable';

class HZBankUser extends HZEnrolable {

  static get _type() { return "BankUser"; }
  static get _subdocs() { return []; }

  get _subdocs() { return HZBankUser._subdocs; }
  get _id() { return this.data['_id']; }
  get _data() { return this.data['_data']; }
  get roles() { return this.data['roles']; }
  get _type() { return this.data['_type']; }
  get address() { return this.data['address']; }
  get phoneNumber() { return this.data['phoneNumber']; }
  get accounts() { return this.data['accounts']; }
  get token() { return this.data['token']; }
  get active() { return this.data['active']; }

  set _id(val) { this.data['_id'] = val; }
  set _data(val) { this.data['_data'] = val; }
  set roles(val) { this.data['roles'] = val; }
  set _type(val) { this.data['_type'] = val; }
  set address(val) { this.data['address'] = val; }
  set phoneNumber(val) { this.data['phoneNumber'] = val; }
  set accounts(val) { this.data['accounts'] = val; }
  set token(val) { this.data['token'] = val; }
  set active(val) { this.data['active'] = val; }

  constructor(data = {}) {

    super(data);

    this.data = {};

    this.data['_id'] = null;
    this.data['_data'] = {};
    this.data['roles'] = [];
    this.data['_type'] = "BankUser";
    this.data['address'] = undefined;
    this.data['phoneNumber'] = undefined;
    this.data['accounts'] = [];
    this.data['token'] = null;
    this.data['active'] = true;

    this.subdocs = [];

    for(let k in data)
      this.data[k] = data[k]

  }
}

export default HZBankUser;
