import HZEntity from './entity';

class HZBankTransaction extends HZEntity {

  static get _type() { return "BankTransaction"; }
  static get _subdocs() { return []; }

  get _subdocs() { return HZBankTransaction._subdocs; }
  get _id() { return this.data['_id']; }
  get _data() { return this.data['_data']; }
  get _type() { return this.data['_type']; }
  get initiator() { return this.data['initiator']; }
  get sender() { return this.data['sender']; }
  get receiver() { return this.data['receiver']; }
  get timestamp() { return this.data['timestamp']; }
  get operationType() { return this.data['operationType']; }
  get operationTags() { return this.data['operationTags']; }
  get object() { return this.data['object']; }
  get amount() { return this.data['amount']; }

  set _id(val) { this.data['_id'] = val; }
  set _data(val) { this.data['_data'] = val; }
  set _type(val) { this.data['_type'] = val; }
  set initiator(val) { this.data['initiator'] = val; }
  set sender(val) { this.data['sender'] = val; }
  set receiver(val) { this.data['receiver'] = val; }
  set timestamp(val) { this.data['timestamp'] = val; }
  set operationType(val) { this.data['operationType'] = val; }
  set operationTags(val) { this.data['operationTags'] = val; }
  set object(val) { this.data['object'] = val; }
  set amount(val) { this.data['amount'] = val; }

  get ttl() { return this.data['ttl']; }
  get _subdocs() { return this.data['_subdocs']; }
  get _typeChain() { return this.data['_typeChain']; }

  set ttl(val) { this.data['ttl'] = val; }
  set _subdocs(val) { this.data['_subdocs'] = val; }
  set _typeChain(val) { this.data['_typeChain'] = val; }

  constructor(data = {}) {

    super(data);

    this.data = {};

      /* Fields */
    this.data['_id'] = null;
    this.data['_data'] = {};
    this.data['_type'] = "BankTransaction";
    this.data['initiator'] = null;
    this.data['sender'] = undefined;
    this.data['receiver'] = undefined;
    this.data['timestamp'] = undefined;
    this.data['operationType'] = undefined;
    this.data['operationTags'] = [];
    this.data['object'] = undefined;
    this.data['amount'] = undefined;
      /* Props */
    this.data['ttl'] = undefined;
    this.data['_subdocs'] = undefined;
    this.data['_typeChain'] = undefined;

    this.subdocs = [];

    for(let k in data)
      this.data[k] = data[k]

  }
}

export default HZBankTransaction;
