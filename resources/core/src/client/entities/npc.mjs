import HZWorldObject from './worldobject';

class HZNPC extends HZWorldObject {

  static get _type() { return "NPC"; }
  static get _subdocs() { return []; }

  get _subdocs() { return HZNPC._subdocs; }
  get _id() { return this.data['_id']; }
  get _data() { return this.data['_data']; }
  get _type() { return this.data['_type']; }
  get model() { return this.data['model']; }
  get position() { return this.data['position']; }
  get cube() { return this.data['cube']; }
  get name() { return this.data['name']; }
  get heading() { return this.data['heading']; }
  get animdict() { return this.data['animdict']; }
  get animation() { return this.data['animation']; }

  set _id(val) { this.data['_id'] = val; }
  set _data(val) { this.data['_data'] = val; }
  set _type(val) { this.data['_type'] = val; }
  set model(val) { this.data['model'] = val; }
  set position(val) { this.data['position'] = val; }
  set cube(val) { this.data['cube'] = val; }
  set name(val) { this.data['name'] = val; }
  set heading(val) { this.data['heading'] = val; }
  set animdict(val) { this.data['animdict'] = val; }
  set animation(val) { this.data['animation'] = val; }

  get ttl() { return this.data['ttl']; }
  get _subdocs() { return this.data['_subdocs']; }
  get _typeChain() { return this.data['_typeChain']; }

  set ttl(val) { this.data['ttl'] = val; }
  set _subdocs(val) { this.data['_subdocs'] = val; }
  set _typeChain(val) { this.data['_typeChain'] = val; }

  constructor(data = {}) {

    super(data);

    this.data = {};

      /* Fields */
    this.data['_id'] = null;
    this.data['_data'] = {};
    this.data['_type'] = "NPC";
    this.data['model'] = undefined;
    this.data['position'] = undefined;
    this.data['cube'] = undefined;
    this.data['name'] = undefined;
    this.data['heading'] = 0;
    this.data['animdict'] = "";
    this.data['animation'] = "";
      /* Props */
    this.data['ttl'] = undefined;
    this.data['_subdocs'] = undefined;
    this.data['_typeChain'] = undefined;

    this.subdocs = [];

    for(let k in data)
      this.data[k] = data[k]

  }
}

export default HZNPC;
