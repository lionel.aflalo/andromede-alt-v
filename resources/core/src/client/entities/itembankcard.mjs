import HZItem from './item';

class HZItemBankCard extends HZItem {

  static get _type() { return "ItemBankcard"; }
  static get _subdocs() { return []; }

  get _subdocs() { return HZItemBankCard._subdocs; }
  get _id() { return this.data['_id']; }
  get _data() { return this.data['_data']; }
  get _type() { return this.data['_type']; }
  get container() { return this.data['container']; }
  get name() { return this.data['name']; }
  get label() { return this.data['label']; }
  get count() { return this.data['count']; }
  get image() { return this.data['image']; }
  get shape() { return this.data['shape']; }
  get x() { return this.data['x']; }
  get y() { return this.data['y']; }
  get world() { return this.data['world']; }
  get lifetime() { return this.data['lifetime']; }
  get createTime() { return this.data['createTime']; }
  get mass() { return this.data['mass']; }
  get volume() { return this.data['volume']; }
  get number() { return this.data['number']; }
  get cvv() { return this.data['cvv']; }
  get pinCode() { return this.data['pinCode']; }
  get account() { return this.data['account']; }
  get user() { return this.data['user']; }
  get token() { return this.data['token']; }
  get activated() { return this.data['activated']; }

  set _id(val) { this.data['_id'] = val; }
  set _data(val) { this.data['_data'] = val; }
  set _type(val) { this.data['_type'] = val; }
  set container(val) { this.data['container'] = val; }
  set name(val) { this.data['name'] = val; }
  set label(val) { this.data['label'] = val; }
  set count(val) { this.data['count'] = val; }
  set image(val) { this.data['image'] = val; }
  set shape(val) { this.data['shape'] = val; }
  set x(val) { this.data['x'] = val; }
  set y(val) { this.data['y'] = val; }
  set world(val) { this.data['world'] = val; }
  set lifetime(val) { this.data['lifetime'] = val; }
  set createTime(val) { this.data['createTime'] = val; }
  set mass(val) { this.data['mass'] = val; }
  set volume(val) { this.data['volume'] = val; }
  set number(val) { this.data['number'] = val; }
  set cvv(val) { this.data['cvv'] = val; }
  set pinCode(val) { this.data['pinCode'] = val; }
  set account(val) { this.data['account'] = val; }
  set user(val) { this.data['user'] = val; }
  set token(val) { this.data['token'] = val; }
  set activated(val) { this.data['activated'] = val; }

  get ttl() { return this.data['ttl']; }
  get _subdocs() { return this.data['_subdocs']; }
  get _typeChain() { return this.data['_typeChain']; }
  get model() { return this.data['model']; }
  get usable() { return this.data['usable']; }
  get codeName() { return this.data['codeName']; }
  get variantHash() { return this.data['variantHash']; }
  get humanVariantHash() { return this.data['humanVariantHash']; }
  get actions() { return this.data['actions']; }
  get equip() { return this.data['equip']; }
  get limit() { return this.data['limit']; }
  get label() { return this.data['label']; }

  set ttl(val) { this.data['ttl'] = val; }
  set _subdocs(val) { this.data['_subdocs'] = val; }
  set _typeChain(val) { this.data['_typeChain'] = val; }
  set model(val) { this.data['model'] = val; }
  set usable(val) { this.data['usable'] = val; }
  set codeName(val) { this.data['codeName'] = val; }
  set variantHash(val) { this.data['variantHash'] = val; }
  set humanVariantHash(val) { this.data['humanVariantHash'] = val; }
  set actions(val) { this.data['actions'] = val; }
  set equip(val) { this.data['equip'] = val; }
  set limit(val) { this.data['limit'] = val; }
  set label(val) { this.data['label'] = val; }

  constructor(data = {}) {

    super(data);

    this.data = {};

      /* Fields */
    this.data['_id'] = null;
    this.data['_data'] = {};
    this.data['_type'] = "ItemBankcard";
    this.data['container'] = null;
    this.data['name'] = "itembankcard";
    this.data['label'] = "Carte Bancaire";
    this.data['count'] = 1;
    this.data['image'] = "bankcard";
    this.data['shape'] = {"grid":[{"x":-4,"y":-22.000000000000007}],"offset":{"x":4,"y":22.000000000000007}};
    this.data['x'] = 0;
    this.data['y'] = 0;
    this.data['world'] = null;
    this.data['lifetime'] = -1;
    this.data['createTime'] = undefined;
    this.data['mass'] = 10;
    this.data['volume'] = {"x":5,"y":10,"z":0.1};
    this.data['number'] = null;
    this.data['cvv'] = undefined;
    this.data['pinCode'] = "0000";
    this.data['account'] = null;
    this.data['user'] = null;
    this.data['token'] = null;
    this.data['activated'] = false;
      /* Props */
    this.data['ttl'] = undefined;
    this.data['_subdocs'] = undefined;
    this.data['_typeChain'] = undefined;
    this.data['model'] = undefined;
    this.data['usable'] = undefined;
    this.data['codeName'] = undefined;
    this.data['variantHash'] = undefined;
    this.data['humanVariantHash'] = undefined;
    this.data['actions'] = undefined;
    this.data['equip'] = undefined;
    this.data['limit'] = undefined;
    this.data['label'] = undefined;

    this.subdocs = [];

    for(let k in data)
      this.data[k] = data[k]

  }
}

export default HZItemBankCard;
