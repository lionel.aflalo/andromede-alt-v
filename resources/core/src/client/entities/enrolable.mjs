import HZEntity from './entity';

class HZEnrolable extends HZEntity {

  static get _type() { return undefined; }
  static get _subdocs() { return []; }

  get _subdocs() { return HZEnrolable._subdocs; }
  get _id() { return this.data['_id']; }
  get _data() { return this.data['_data']; }
  get roles() { return this.data['roles']; }

  set _id(val) { this.data['_id'] = val; }
  set _data(val) { this.data['_data'] = val; }
  set roles(val) { this.data['roles'] = val; }

  get ttl() { return this.data['ttl']; }
  get _subdocs() { return this.data['_subdocs']; }
  get _typeChain() { return this.data['_typeChain']; }

  set ttl(val) { this.data['ttl'] = val; }
  set _subdocs(val) { this.data['_subdocs'] = val; }
  set _typeChain(val) { this.data['_typeChain'] = val; }

  constructor(data = {}) {

    super(data);

    this.data = {};

      /* Fields */
    this.data['_id'] = null;
    this.data['_data'] = {};
    this.data['roles'] = [];
      /* Props */
    this.data['ttl'] = undefined;
    this.data['_subdocs'] = undefined;
    this.data['_typeChain'] = undefined;

    this.subdocs = [];

    for(let k in data)
      this.data[k] = data[k]

  }
}

export default HZEnrolable;
