import HZItem from './item';

class HZItemSandwich extends HZItem {

  static get _type() { return "ItemSandwich"; }
  static get _subdocs() { return []; }

  get _subdocs() { return HZItemSandwich._subdocs; }
  get _id() { return this.data['_id']; }
  get _data() { return this.data['_data']; }
  get _type() { return this.data['_type']; }
  get container() { return this.data['container']; }
  get name() { return this.data['name']; }
  get label() { return this.data['label']; }
  get count() { return this.data['count']; }
  get image() { return this.data['image']; }
  get shape() { return this.data['shape']; }
  get x() { return this.data['x']; }
  get y() { return this.data['y']; }
  get world() { return this.data['world']; }
  get lifetime() { return this.data['lifetime']; }
  get createTime() { return this.data['createTime']; }

  set _id(val) { this.data['_id'] = val; }
  set _data(val) { this.data['_data'] = val; }
  set _type(val) { this.data['_type'] = val; }
  set container(val) { this.data['container'] = val; }
  set name(val) { this.data['name'] = val; }
  set label(val) { this.data['label'] = val; }
  set count(val) { this.data['count'] = val; }
  set image(val) { this.data['image'] = val; }
  set shape(val) { this.data['shape'] = val; }
  set x(val) { this.data['x'] = val; }
  set y(val) { this.data['y'] = val; }
  set world(val) { this.data['world'] = val; }
  set lifetime(val) { this.data['lifetime'] = val; }
  set createTime(val) { this.data['createTime'] = val; }

  get ttl() { return this.data['ttl']; }
  get _subdocs() { return this.data['_subdocs']; }
  get _typeChain() { return this.data['_typeChain']; }
  get model() { return this.data['model']; }
  get usable() { return this.data['usable']; }
  get codeName() { return this.data['codeName']; }
  get variantHash() { return this.data['variantHash']; }
  get humanVariantHash() { return this.data['humanVariantHash']; }
  get actions() { return this.data['actions']; }
  get equip() { return this.data['equip']; }
  get limit() { return this.data['limit']; }

  set ttl(val) { this.data['ttl'] = val; }
  set _subdocs(val) { this.data['_subdocs'] = val; }
  set _typeChain(val) { this.data['_typeChain'] = val; }
  set model(val) { this.data['model'] = val; }
  set usable(val) { this.data['usable'] = val; }
  set codeName(val) { this.data['codeName'] = val; }
  set variantHash(val) { this.data['variantHash'] = val; }
  set humanVariantHash(val) { this.data['humanVariantHash'] = val; }
  set actions(val) { this.data['actions'] = val; }
  set equip(val) { this.data['equip'] = val; }
  set limit(val) { this.data['limit'] = val; }

  constructor(data = {}) {

    super(data);

    this.data = {};

      /* Fields */
    this.data['_id'] = null;
    this.data['_data'] = {};
    this.data['_type'] = "ItemSandwich";
    this.data['container'] = null;
    this.data['name'] = "itemsandwich";
    this.data['label'] = "Sandwich";
    this.data['count'] = 1;
    this.data['image'] = "sandwich";
    this.data['shape'] = {"grid":[{"x":2,"y":-14}],"offset":{"x":98,"y":14}};
    this.data['x'] = 0;
    this.data['y'] = 0;
    this.data['world'] = null;
    this.data['lifetime'] = -1;
    this.data['createTime'] = undefined;
      /* Props */
    this.data['ttl'] = undefined;
    this.data['_subdocs'] = undefined;
    this.data['_typeChain'] = undefined;
    this.data['model'] = undefined;
    this.data['usable'] = undefined;
    this.data['codeName'] = undefined;
    this.data['variantHash'] = undefined;
    this.data['humanVariantHash'] = undefined;
    this.data['actions'] = undefined;
    this.data['equip'] = undefined;
    this.data['limit'] = undefined;

    this.subdocs = [];

    for(let k in data)
      this.data[k] = data[k]

  }
}

export default HZItemSandwich;
