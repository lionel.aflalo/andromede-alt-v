import HZEntity from './entity';

class HZBankAutoWire extends HZEntity {

  static get _type() { return "BankAutoWire"; }
  static get _subdocs() { return []; }

  get _subdocs() { return HZBankAutoWire._subdocs; }
  get _id() { return this.data['_id']; }
  get _data() { return this.data['_data']; }
  get _type() { return this.data['_type']; }
  get sender() { return this.data['sender']; }
  get receiver() { return this.data['receiver']; }
  get amount() { return this.data['amount']; }
  get startDate() { return this.data['startDate']; }
  get frequency() { return this.data['frequency']; }
  get iterations() { return this.data['iterations']; }
  get day() { return this.data['day']; }
  get iterationsLeft() { return this.data['iterationsLeft']; }
  get lastIteration() { return this.data['lastIteration']; }
  get wireTags() { return this.data['wireTags']; }
  get active() { return this.data['active']; }

  set _id(val) { this.data['_id'] = val; }
  set _data(val) { this.data['_data'] = val; }
  set _type(val) { this.data['_type'] = val; }
  set sender(val) { this.data['sender'] = val; }
  set receiver(val) { this.data['receiver'] = val; }
  set amount(val) { this.data['amount'] = val; }
  set startDate(val) { this.data['startDate'] = val; }
  set frequency(val) { this.data['frequency'] = val; }
  set iterations(val) { this.data['iterations'] = val; }
  set day(val) { this.data['day'] = val; }
  set iterationsLeft(val) { this.data['iterationsLeft'] = val; }
  set lastIteration(val) { this.data['lastIteration'] = val; }
  set wireTags(val) { this.data['wireTags'] = val; }
  set active(val) { this.data['active'] = val; }

  get ttl() { return this.data['ttl']; }
  get _subdocs() { return this.data['_subdocs']; }
  get _typeChain() { return this.data['_typeChain']; }

  set ttl(val) { this.data['ttl'] = val; }
  set _subdocs(val) { this.data['_subdocs'] = val; }
  set _typeChain(val) { this.data['_typeChain'] = val; }

  constructor(data = {}) {

    super(data);

    this.data = {};

      /* Fields */
    this.data['_id'] = null;
    this.data['_data'] = {};
    this.data['_type'] = "BankAutoWire";
    this.data['sender'] = undefined;
    this.data['receiver'] = undefined;
    this.data['amount'] = undefined;
    this.data['startDate'] = undefined;
    this.data['frequency'] = 7;
    this.data['iterations'] = -1;
    this.data['day'] = 0;
    this.data['iterationsLeft'] = undefined;
    this.data['lastIteration'] = -1;
    this.data['wireTags'] = [];
    this.data['active'] = true;
      /* Props */
    this.data['ttl'] = undefined;
    this.data['_subdocs'] = undefined;
    this.data['_typeChain'] = undefined;

    this.subdocs = [];

    for(let k in data)
      this.data[k] = data[k]

  }
}

export default HZBankAutoWire;
