import * as alt from 'alt'
import * as natives from 'natives';
import $ from '../../../lib/altv-api/client/index';

import Interact     from '../interact/index';
import Notification from '../notification/index';
import CarDealer    from '../cardealer/index';

const { Entity, Model } = $.types;
const { Vector3 } = $.math;

const GizzTest = {};

GizzTest.__NAME = 'gizztest';

GizzTest.init = function() {

  return new Promise((resolve, reject) => {

    /*
    $.game.on('hz:player:init:done', async () => {

      const pos = new Vector3(353.33170000, -2315.83800000, 13.72306000);

      alt.setInterval(() => {
        natives.hideMapObjectThisFrame($.utils.joaat('po1_09_brig_m'));
        natives.hideMapObjectThisFrame($.utils.joaat('po1_09_brig_det_03'));
        natives.hideMapObjectThisFrame($.utils.joaat('po1_09_brig_det_04'));
        natives.hideMapObjectThisFrame($.utils.joaat('po1_09_brig_det_05'));
      }, 0);

      const model   = new Model('po1_09_brig_m_copy');
      const success = await model.request();

      alt.log(success);

      const bridge = new Entity(natives.createObjectNoOffset($.utils.joaat('po1_09_brig_m_copy'), pos.x, pos.y, pos.z, false, false, false));

      while(true) {

        while(bridge.position.z < 48.72306000) {
          const curr = bridge.position;
          curr.z += 0.05;
          bridge.position = curr;
          await $.utils.delay(20);
        }

        while(bridge.position.z > 13.72306000) {
          const curr = bridge.position;
          curr.z -= 0.05;
          bridge.position = curr;
          await $.utils.delay(20);
        }

      }

    });
    */
    
    resolve();

  });

}

export default GizzTest;
