import EventEmitter from 'events';
import * as alt     from 'alt'
import * as natives from 'natives';
import $            from '../../../lib/altv-api/client/index';
import Core         from '../../index';
import HUD          from '../hud/index';
import WM           from '../windowmanager/index';
import MM           from '../menumanager/index';
import WS           from '../worldselect/index';
import Cache        from '../cache/index';
import World        from '../world/index';
import Notification from '../notification/index';

import { HZItem, HZContainer, HZWorldContainer } from '../../entities/index';

const { Entity } = $.types;

const WORLD_ITEM_RADIUS = 5.0;

class Inventory extends EventEmitter {

  constructor() {

    super();

    this.window     = null;
    this.containers = [];

    this.__NAME = 'inventory';
  }
  
  getContainers(item) {

    const containers = [];
    
    if(item instanceof HZContainer)
      containers.push(item._id);
    
    for(let i=0; i<item.subdocs.length; i++) {

      const subdoc = item.subdocs[i];

      if(subdoc.typeChain.indexOf('Container') !== -1 && !subdoc.isArray && item[subdoc.name])
        containers.push(item[subdoc.name]);

    }

    return containers;

  }

  open(addon = []) {

    return new Promise((resolve, reject) => {

      if(this.window)
        this.window.destroy();

      const [_, screenX, screenY] = natives.getActiveScreenResolution();

      this.window = WM.open({
        url      : 'http://resource/dist/html/inventory/index.html',
        classname: 'stayinspace notitle transparent',
        x        : 0,
        y        : 0,
        width    : screenX,
        height   : screenY,
      });

      this.window.on('destroy', () => {
        this.window = null;
        resolve();
      });

      this.window.on('message', async msg => {

          try {

            switch(msg.action) {

              case 'ready' : {
                
                const playerId   = alt.Player.local.getSyncedMeta('player');
                const identityId = alt.Player.local.getSyncedMeta('identity');
                
                this.window.postMessage({action: 'set_identity', identity: identityId});

                const [hzPlayer, hzIdentity, world, addonContainers] = await Promise.all([
                  Cache.resolve({_type: 'Player',  _id: playerId}),
                  Cache.resolve({_type: 'Identity',  _id: identityId}),
                  Cache.resolve({_type: 'WorldContainer',  _id: Core.worldId}),
                  Cache.resolve(addon),
                ]);

                this.window.postMessage({action: 'set_player_roles', roles: hzPlayer.roles});
                
                const promises = [];

                for(let i=0; i<hzIdentity.containers.length; i++) {

                  const container = hzIdentity.containers[i];

                  for(let j=0; j<container.items.length; j++) {

                    const item = container.items[j];

                    if(!item)
                      continue;

                    const _containers = this.getContainers(item);

                    for(let k=0; k<_containers.length; k++)
                      promises.push(Cache.resolve({_type: _containers[k]._type,  _id: _containers[k]._id}));

                  }

                }

                for(let i=0; i<addonContainers.length; i++) {

                  const container = addonContainers[i];

                  for(let j=0; j<container.items.length; j++) {

                    const item = container.items[j];

                    if(!item)
                      continue;

                    const _containers = this.getContainers(item);

                    for(let k=0; k<_containers.length; k++)
                      promises.push(Cache.resolve({_type: _containers[k]._type,  _id: _containers[k]._id}));

                  }

                }

                const player     = alt.Player.local;
                const worldItems = world.items.filter(e => e && e.world && e.world.position && $.utils.getDistance(player.pos, e.world.position) <= WORLD_ITEM_RADIUS);  

                for(let j=0; j<worldItems.length; j++) {

                  const item = worldItems[j];

                  if(!item)
                    continue;

                  const _containers = this.getContainers(item);

                  for(let k=0; k<_containers.length; k++)
                    promises.push(Cache.resolve({_type: _containers[k]._type,  _id: _containers[k]._id}));

                }
                
                const results = await Promise.all(promises);

                const containers = hzIdentity.containers.concat(addonContainers).concat(results)
                  .map(e => ({...e.data, items: e.data.items.map(e => e ? e.data : null).filter(e => !!e)}))
                  .concat([{...world.data, items: worldItems.map(e => e ? e.data : null).filter(e => !!e)}])
                ;
    
                this.containers = hzIdentity.containers.concat(addonContainers).concat(results)
                  .map(e => ({_type: e._type, _id: e._id}))
                  .concat([{_type: world._type, _id: world._id}])
                ;

                this.window.postMessage({action: 'set_containers', containers});
                
                break;
              }
    
              case 'move_item' : {

                alt.emitServer('hz:item:move', msg.from, msg.to, msg.item, msg.x, msg.y, msg.count);

                const from = Cache.data[msg.from];
                const to   = Cache.data[msg.to];

                try {

                  if(from && to && (from instanceof HZWorldContainer) && !(to instanceof HZWorldContainer)) {

                    const loaded = World.loaded[msg.item];
  
                    if(loaded && loaded.entity) {
                      loaded.entity.delete();
                      delete World.loaded[msg.item];
                    }
  
                  }

                } catch(e) {  alt.logError(e.message, e.stack); }

                break;
              }

              case 'item_action' : {
                alt.emitServer('hz:item:action', msg._id, msg.itemAction);
                break;
              }

              case 'item_action_admin' : {
                alt.emitServer('hz:item:action:admin', msg._id, msg.itemAction);
                break;
              }
    
              default: break;

            }

          } catch (e) { alt.logError(e.message, e.stack); }

      });

    });

  }

  init() {

    return new Promise((resolve, reject) => {
      
      $.game.on('hz:cache:update:done', async (..._ids) => {

        try {
          
          if(this.window === null)
            return;

          const _containers = await Cache.resolve(this.containers);
          const containers  = _containers.filter(e => !!e).map(e => ({...e.data, items: e.data.items.map(e => e ? e.data : null).filter(e => !!e)}));
          const world       = containers.find(e => e._type === 'WorldContainer');

          if(world) {
            const player = alt.Player.local;
            world.items  = world.items.filter(e => e && e.world && e.world.position && $.utils.getDistance(player.pos, e.world.position) <= WORLD_ITEM_RADIUS);
          }

          this.window.postMessage({action: 'set_containers', containers});

        } catch(e) { alt.logError(e.message, e.stack); }

      });


      $.game.on('hz:interactable:interact:showcontainer', async (interactable, {ws, wsa}, delegateServer) => {
        const data = await $.requestp('hz:interactable:container:ensure', interactable._id);
        this.open(data);
        HUD.enable(false);
      });
    
      alt.onServer('hz:item:give:start', async (_id) => {

        HUD.disable(false);

        WS.enable(true, {mouse: true, camera: true, cursor: true});

        $.game.once('ws:mousedown:left', async ws => {

          WS.enable(false, {mouse: true, camera: true, cursor: true});

          if(ws.target instanceof alt.Player) {

            if(ws.target === alt.Player.local) {
              Notification.create('Vous devez séléctionner quelqu\'un d\'autre');
              return;
            }

            const hzItem = await Cache.resolve({_type: 'Item', _id});

            const menu = MM.open('Donner un item', [
              {type: 'input', label: 'Quantité', value: '1', name: 'qty'},
              {label: 'Valider', name: 'submit'},
            ]);

            menu.on('load', () => {
              HUD.enable(false);
            });

            menu.on('select', async (data, item) => {

              const byName = menu.by('name');
              let   qty    = isNaN(byName.qty.value) ? 1 : parseInt(byName.qty.value, 10);

              qty = Math.floor(qty);

              if(qty < 1)
                qty = 1;

              switch(item.name) {

                case 'submit' : {

                  menu.destroy();

                  const success = await $.requestp('hz:item:give', hzItem._id, ws.target, qty);

                  if(success) {
                    Notification.create('Vous avez donné x' + qty + ' ' + hzItem.label);
                  } else {
                    Notification.create('Item refusé');
                  }

                  break;
                }

                default: break;
              }

            });

          } else {

            Notification.create('Vous devez séléctionner un joueur');

          }

        });

      });

      $.registerCallback('hz:item:give:request', async (cb, _type, _id, qty) => {

        const hzItem = await Cache.resolve({_type, _id});

        const menu = MM.open('Accepter x' + qty + ' ' + hzItem.label + ' ?', [
          {label: 'Non', name: 'no'},
          {label: 'Oui', name: 'yes'},
        ]);

        menu.on('select', async (data, item) => {
          menu.destroy();
          cb(item.name === 'yes');
        });

      });
      
      resolve();
    
    });
  }
  
}

const instance = new Inventory();

export default instance;