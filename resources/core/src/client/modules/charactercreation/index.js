/// <reference path="../../typings/altv-client.d.ts" />

import * as alt     from 'alt';
import * as natives from 'natives';
import $            from '../../../lib/altv-api/client/index.js';
import * as nui     from '../../lib/nativeui/NativeUI.mjs';

class CharacterSkin {
	constructor() {
		this.Sex = 0;
		this.Mum = 0;
		this.Dad = 0;
		this.Resemblance = 0.5;
		this.SkinMix = 0.5
		this.Hairs = 0;
		this.HairsHighlights = 0;
		this.HairsColour = 0;
		this.HairsHighlightsColour = 0;
		this.Beard = 0;
		this.BeardSize = 0.0;
	}

	setMum(mum) {
		this.Mum = mum;
		this.applyHeadBlendDatas();
	}

	setDad(dad) {
		this.Dad = dad;
		this.applyHeadBlendDatas();
	}

	setResemblance(res) {
		this.Resemblance = res;
		this.applyHeadBlendDatas();
	}

	setSkinMix(mix) {
		this.SkinMix = mix;
		this.applyHeadBlendDatas();
	}

	setHairs(hairs, hairsHightlights) {
		this.Hairs = hairs;
		if (hairsColour != -1)
			this.HairsHighlights = hairsHightlights;
		this.applyHairs();
	}

	setHairsColour(hairsColour, highlightsColour = -1) {
		this.HairsColour = hairsColour;
		if (highlightsColour != -1)
			this.HairsHighlightsColour = highlightsColour;
		this.applyHairsColour();
	}

	setBeard(beard, beardSize = -1) {
		this.Beard = beard;
		if (beardSize != -1)
			this.BeardSize = beardSize;
		this.applyBeard();
	}

	applyHeadBlendDatas() {
		natives.setPedHeadBlendData(+$.game.player.ped, this.Mum, this.Dad, 0, this.Mum, this.Dad, 0, this.Resemblance, this.SkinMix, 0.0, false)
	}

	applyHairs() {
		natives.setPedComponentVariation(+$.game.player.ped, $.constants.PED_COMPONENTS.HAIR, 2, this.Hairs, this.HairsHighlights, 2);
	}

	applyHairsColour() {
		natives.setPedHairColor(+$.game.player.ped, this.HairsColour, this.HairsHighlightsColour)
	}

	applyBeard() {
		natives.setPedHeadOverlay(+$.game.player.ped, this.Beard, this.BeardSize);
	}

	applyAll() {
		this.applyHeadBlendDatas();
		this.applyHairs();
		this.applyHairsColour();
		this.applyBeard();
	}

	async setSex(sex) {
		let model;
		switch(sex) {
			case 0:
				model = new $.types.Model("mp_m_freemode_01");
				break;

			case 1:
				model = new $.types.Model("mp_f_freemode_01");
				break;
		}
		const success = await model.request();
		
		if (success) {

			natives.setPlayerModel($.game.player, model.valueOf());
			$.game.player.ped.components.setDefault();
			model.setAsNoLongerNeeded();

		} else throw new Error('Missing model');
	}

	toServerDatas() {
		return [this.Sex, this.Mum, this.Dad, this.Resemblance, this.SkinMix, this.Hairs, this.HairsColour, this.HairsHighlights, this.HairsHighlightsColour, this.Beard, this.BeardSize];
	}
}

const CharCreation = {
	CreationPosition: new $.math.Vector3(402.99267578125, -996.694091796875, -99.000244140625),
	Skin						: new CharacterSkin(),
	mainMenu				: null,
	valided					: false,
	CharDatas				: {
		Lastname	: "",
		Firstname	: "",
		Birthdate	: ""
	}
};

function CreateGeneticMenu() {

	let MumsTable = ["Hannah","Audrey","Jasmine","Giselle","Amelia","Isabella","Zoe","Ava","Camila","Violet","Sophia","Evelyn","Nicole","Ashley","Grace","Brianna","Natalie","Olivia","Elisabeth","Charlotte","Emma"];
	let DadsTable = ["Benjamin","Daniel","Joshua","Noah","Andrew","Juan","Alex","Isaac","Evan","Ethan","Vicent","Angel","Diego","Adrian","Gabriel","Michel","Santiago","Kevin","Louis","Samuel","Anthony"]

	const menu = new nui.Menu("Génétique", "Génétique de votre personnage", new nui.Point(50,50));

	let mumItem = new nui.UIMenuListItem("Mère", "Choisissez la mère de votre personnage", new nui.ItemsCollection(MumsTable));
	menu.AddItem(mumItem);

	let dadItem = new nui.UIMenuListItem("Mère", "Choisissez le père de votre personnage", new nui.ItemsCollection(DadsTable));
	menu.AddItem(dadItem);

	let resemblanceItem = new nui.UIMenuSliderItem("Ressemblance", 
		[0.0, 0.05, 0.1, 0.15, 0.2, 0.25, 0.3, 0.35, 0.4, 0.45, 0.5, 0.55, 0.6, 0.65, 0.7, 0.75, 0.8, 0.85, 0.9, 0.95, 1.0],
		11,
		"Ressemblez vous plus à votre mère ou votre père",
		true
	);
	menu.AddItem(resemblanceItem);

	let skinMixItem = new nui.UIMenuSliderItem("Couleur de Peau",
		[0.0, 0.05, 0.1, 0.15, 0.2, 0.25, 0.3, 0.35, 0.4, 0.45, 0.5, 0.55, 0.6, 0.65, 0.7, 0.75, 0.8, 0.85, 0.9, 0.95, 1.0],
		11,
		"Choisissez si votre couleur de peau est plus du côté de votre mère ou de votre père",
		true
	);
	menu.AddItem(skinMixItem);

	menu.ListChange.on((item, index) => {
		if (item == mumItem)
			CharCreation.Skin.setMum(index);
		else if (item == dadItem)
			CharCreation.Skin.setDad(index);
	});

	menu.SliderChange.on((item, index) => {
		if (item == resemblanceItem)
			CharCreation.Skin.setResemblance((index - 1)/10);
		else if (item == skinMixItem)
			CharCreation.Skin.setSkinMix((index - 1)/10);
	});

	menu.MenuClose.on(() => {
		CharCreation.mainMenu.Visible = true;
	});

	menu.Visible = false;

	return menu;
}

CharCreation.StartCharacterCreation = async function() {
	CharCreation.valided = false;
	CharCreation.Skin = new CharacterSkin();
	// Prevent Multiple Instance of Create Character Menu
	if (CharCreation.mainMenu != null) return;
	
	CharCreation.mainMenu = new nui.Menu("Création", "Créez votre personnage", new nui.Point(50,50));

	await $.helpers.screenFadeOut();
	await $.game.player.ped.teleport(CharCreation.CreationPosition);
	$.game.player.ped.heading = 180.0;
	CharCreation.Skin.applyAll();
	await $.helpers.screenFadeIn(1000);

	let sexItem = new nui.UIMenuListItem("Sexe", "Choisissez le sexe de votre personnage", new nui.ItemsCollection(["Homme", "Femme"]));
	CharCreation.mainMenu.AddItem(sexItem);

	let geneticMenu = CreateGeneticMenu();
	let geneticItem = new nui.UIMenuItem("Génétique");
	CharCreation.mainMenu.AddItem(geneticItem);

	let validItem = new nui.UIMenuItem("Valider");
	CharCreation.mainMenu.AddItem(validItem);

	CharCreation.mainMenu.ListChange.on(async (item, index) => {
		if (item == sexItem)
			CharCreation.Skin.setSex(index);
	});

	CharCreation.mainMenu.ItemSelect.on(item => {
		if (item == validItem) {
			CharCreation.valided = true;
			CharCreation.mainMenu.Close();
			alt.emitServer("charactercreation:end", CharCreation.CharDatas, CharCreation.Skin.toServerDatas());
		} else if (item == geneticItem) {
			CharCreation.mainMenu.Visible = false;
			geneticMenu.Visible = true;
		}
	});

	CharCreation.mainMenu.MenuClose.on(() => {
		CharCreation.mainMenu = null;
		CharCreation.Skin = new CharacterSkin();
		if (!CharCreation.valided)
			CharCreation.StartCharacterCreation();
	});

	CharCreation.mainMenu.Open();
}

alt.onServer("charactercreation:start", CharCreation.StartCharacterCreation);

export default CharacterCreation;