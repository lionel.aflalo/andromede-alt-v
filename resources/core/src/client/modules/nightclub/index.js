import * as alt from 'alt'
import * as natives from 'natives';

import WM from '../windowmanager/index.js';
import MM from '../menumanager/index.js';
import Cache from '../cache/index';
import Notification from '../notification/index';
import * as entities from '../../entities/index';
import $ from '../../../lib/altv-api/client/index';

const { Vector3 } = $.math;

const Nightclub = {

  interior: natives.getInteriorAtCoordsWithType(-1604, -3012, -80, 'ba_dlc_int_01_ba'),

  entitysets: [
    'int01_ba_security_upgrade',
    'int01_ba_equipment_upgrade',
    'int01_ba_style01',
    'int01_ba_equipment_setup',
    'int01_ba_style03_podium',
    'int01_ba_dj01',
    'int01_ba_booze_01',
    'dj_01_lights_01',
    'dj_02_lights_02',
    'dj_03_lights_03',
    'dj_04_lights_04',
    'int01_ba_bar_content',
    'int01_ba_lights_screen',
    'int01_ba_screen',
  ],

  ptfxSmoke: [],

  inInterior: false,

  webview: null,

};

Nightclub.__NAME = 'nightclub';

Nightclub.init = function() {

  return new Promise((resolve, reject) => {

    alt.setInterval(() => {

      const curr = natives.getInteriorFromEntity(alt.Player.local.scriptID);

      if(curr === this.interior) {

        if(!this.inInterior) {

          for(let i=0; i<this.entitysets.length; i++) {

            const entityset = this.entitysets[i];
  
            if(!natives.isInteriorEntitySetActive(this.interior, entityset))
              natives.activateInteriorEntitySet(this.interior, entityset);
  
          }
  
          natives.refreshInterior(this.interior);
          this.enableSmokeMachines();

        }

        this.inInterior = true;

      } else {

        if(this.inInterior) {

          this.disableSmokeMachines();

        }

        this.inInterior = false;

      }

    }, 2500);

    try {


    } catch(e) { alt.logError(e.message, e.stack); }

    resolve();

  });

}

Nightclub.enableSmokeMachines = async function() {

  natives.requestNamedPtfxAsset('scr_ba_club')
  await $.utils.waitFor(() => natives.hasNamedPtfxAssetLoaded('scr_ba_club'));

  for(let i = 0; i<6; i++) {
    natives.useParticleFxAsset('scr_ba_club');

    const f4065 = this.func_4065(i);
    const f4064 = this.func_4064(i);

    const ptfx = natives.startParticleFxLoopedAtCoord('scr_ba_club_smoke_machine', f4065.x, f4065.y, f4065.z, f4064.x, f4064.y, f4064.z, 5.0, 0, 0, 0, 1);
    
    natives.setParticleFxLoopedColour(ptfx, 64, 128, 255, 1);
    
    this.ptfxSmoke[i] = ptfx;
  }

  let filling = 0;
    
  while(this.inInterior && filling < 1) {

    filling += 0.02;

    for(let i = 0; i<6; i++)
      natives.setParticleFxLoopedEvolution(this.ptfxSmoke[i], 'FILL', filling, 0)

    await $.utils.delay(500);

  }

}

Nightclub.disableSmokeMachines = async function() {

  for(let i=0; i<this.ptfxSmoke.length; i++)
    natives.stopParticleFxLooped(this.ptfxSmoke[i], true);

  this.ptfxSmoke.length = 0;

}

Nightclub.func_4064 = function(param) {

  switch(param) {

    case 0: return new Vector3(0.0, 0.0, 0.0);
    case 1: return new Vector3(0.0, 0.0, 0.0);
    case 2: return new Vector3(0.0, 0.0, 0.0);
    case 3: return new Vector3(0.0, -10.0, 66.0);
    case 4: return new Vector3(0.0, -10.0, 110.0);
    case 5: return new Vector3(0.0, -10.0, -122.53);
    case 6: return new Vector3(0.0, -10.0, -166.97);
    
    default: return Vector3.zero;

  }
}

Nightclub.func_4065 = function(param) {

  switch(param) {

    case 0: return new Vector3(-1600.0, -3012.0, -80.0);
    case 1: return new Vector3(-1595.0, -3012.0, -80.0);
    case 2: return new Vector3(-1590.0, -3012.0, -80.0);
    case 3: return new Vector3(-1602.932, -3019.1, -79.99);
    case 4: return new Vector3(-1593.238, -3017.05, -79.99);
    case 5: return new Vector3(-1597.134, -3008.2, -79.99);
    case 6: return new ector3(-1589.966, -3008.518, -79.99);
    
    default: return Vector3.zero;

  }
}

export default Nightclub;
