import * as alt from 'alt'
import * as natives from 'natives';

const Blip = {
  list: [
    {name: 'LSPD'                , position: {x: 433.480,    y: -981.818,  z: 30.710},  sprite: 60,  color: 29},
    {name: 'BCSD Paleto'         , position: {x: -442.906,   y: 6016.894,  z: 31.712},  sprite: 60,  color: 29},
    {name: 'Hôpital Pillbox'     , position: {x: 328.996,    y: -590.619,  z: 71.266},  sprite: 542, color: 1},
    {name: 'Clinique Sandy Shore', position: {x: 1839.083,   y: 3673.635,  z: 34.277},  sprite: 61,  color: 1},
    {name: 'Lone Wolf Garage'    , position: {x: 117.363,    y: 6617.254,  z: 31.831},  sprite: 446, color: 5},
    {name: 'Donkey Punch Family' , position: {x: 413.196,    y: 6479.969,  z: 28.808},  sprite: 569, color: 1},
    {name: 'Burger Shot'         , position: {x: -1190.365,  y: -889.192,  z: 13.995},  sprite: 106, color: 17},
    {name: 'Pop\'s Diner'        , position: {x: 1589.204,   y: 6454.651,  z: 26.014},  sprite: 565, color: 71},
    {name: 'Fallen Angel Garage' , position: {x: 1179.003,   y: 2646.871,  z: 37.798},  sprite: 446, color: 5},
    {name: 'Pop\'s Diner'        , position: {x: 1589.204,   y: 6454.651,  z: 26.014},  sprite: 565, color: 71},
    {name: 'Downtown Cab & Co'   , position: {x: 905.649,    y: -174.002,  z: 74.078},  sprite: 56,  color: 5},
    {name: 'Gualdin Quay'        , position: {x: -2087.052,  y: -503.070,  z: 12.100},  sprite: 93,  color: 44},
    // {name: 'BCSD Grapeseed'      , position: {x: 1649.188,   y: 4881.917,  z: 42.038},  sprite: 60,  color: 29},
    {name: 'Casey\'s Diner'      , position: {x: 792.491,    y: -735.594,  z: 27.480},  sprite: 565, color: 71},
  ]
};

Blip.__NAME = 'blip';

Blip.init = function() {

  return new Promise((resolve, reject) => {

    for(let i=0; i<this.list.length; i++) {

      const infos = this.list[i];
      const blip  = new alt.PointBlip(infos.position.x, infos.position.y, infos.position.z);
      blip.sprite = infos.sprite;
      blip.color  = infos.color;
      blip.name   = infos.name;

    }

    resolve();

  });

}

export default Blip;
