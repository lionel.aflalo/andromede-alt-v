import * as alt from 'alt'
import * as natives from 'natives';
import * as entities from '../../entities/index';
import $ from '../../../lib/altv-api/client/index';

import HUD   from '../hud/index';
import WM    from '../windowmanager/index';
import Cache from '../cache/index';

const OS = {
  window: null,
  _id   : null,
};

OS.__NAME = 'os';

OS.open = function(_id, options = {}) {

  return new Promise(async (resolve, reject) => {

    if(this.window)
      this.window.destroy();

    OS._id = _id;

    const phone = await Cache.resolve({_type: 'ItemPhone', _id});
    
    const [_, screenX, screenY] = natives.getActiveScreenResolution();

    this.window = WM.open({
      url      : 'http://resource/dist/html/os/index.html',
      classname: 'stayinspace notitle transparent system',
      x        : screenX - 380 - 10,
      y        : screenY - 700 - 10,
      width    : 380,
      height   : 700,
    });

    this.window.on('destroy', () => {
      
      OS.window = null;
      OS._id    = null;

      resolve();
    });

    this.window.on('message', msg => {

      switch(msg.action) {

        case 'component_mounted' : {
          
          this.window.postMessage({
            action: 'load',
            data: {...phone.data, number: phone.simcardslot.items[0] ? phone.simcardslot.items[0].number : ''},
            options
          });

          break;
        }

        case 'send_message' : {
          alt.emitServer('hz:phone:message', _id, msg.to, msg.message);
          break;
        }

        case 'add_contact' : {
          alt.emitServer('hz:phone:contact:add', _id, msg.contact);
          break;
        }

        case 'save_contact' : {
          alt.emitServer('hz:phone:contact:save', _id, msg.index, msg.contact);
          break;
        }

        case 'call' : {
          alt.emitServer('hz:phone:call', _id, msg.number);
          break;
        }

        case 'accept_call' : {
          alt.emitServer('hz:phone:call:accept', msg.number, msg.target);
          break;
        }

        case 'deny_call' : {
          alt.emitServer('hz:phone:call:deny', msg.number, msg.target);
          break;
        }

        case 'end_call' : {
          alt.emitServer('hz:phone:call:end', msg.number, msg.target);
          break;
        }

        default: break;
      }

    });
    
  });

}

OS.init = function() {

  return new Promise((resolve, reject) => {

    alt.onServer('hz:item:use:phone', _id => {
      OS.open(_id);
    });

    alt.onServer('hz:phone:call:incoming', (from, phoneId) => {
      OS.open(phoneId, {call: from});
    });

    alt.onServer('hz:phone:call:end', () => {
      if(OS.window)
        OS.window.postMessage({action: 'end_call'});
    });

    $.game.on('hz:cache:update:done', async (..._ids) => {

      try {
        
        if(this.window === null)
          return;

        for(let i=0; i<_ids.length; i++) {

          const _id = _ids[i];

          if(_id === OS._id) {

            const phone = await Cache.resolve({_type: 'ItemPhone', _id});
            OS.window.postMessage({action: 'load', data: {...phone.data, number: phone.simcardslot.items[0] ? phone.simcardslot.items[0].number : ''}});

          }

        }

      } catch(e) { alt.logError(e.message, e.stack); }

    });
    
    resolve();

  });

}

export default OS;