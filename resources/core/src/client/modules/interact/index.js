import * as alt from 'alt'
import * as natives from 'natives';

import $ from '../../../lib/altv-api/client/index';

import Notification from '../notification/index';

const Interact = {
  id      : 0,
  data    : [],
  current : [],
  in      : [],
  using   : [],
};

Interact.registerMarker = function(position, data = {}, size = 1.5, color = {r: 0, g: 0, b: 128, a: 128}, markerType = 23, radius = 25.0) {

  this.data.push({
    type: 'marker',
    position,
    data,
    size,
    color,
    markerType,
    radius,
  });

}

Interact.__NAME = 'interact';

Interact.init = function() {

  return new Promise((resolve, reject) => {

    alt.on('keyup', key => {

      if (key === 69) { // E

        for(let i=0; i<this.in.length; i++) {

          const current = this.in[i];

          if(this.using.indexOf(current) === -1) {
            this.using.push(current);
            $.game.emit('interact:use', current);
          }

        }
      }

    });

    alt.setInterval(() => {

      const player   = alt.Player.local;
      const ped      = player.ped;
      const position = ped.position;

      for(let i=0; i<this.data.length; i++) {

        const current  = this.data[i];
        const distance = $.utils.getDistance(position, current.position);
        
        if(distance > current.radius) {

          const idx = this.current.indexOf(current);

          if(idx !== -1)
            this.current.splice(idx, 1);

          continue;
        }

        if(this.current.indexOf(current) !== -1)
          continue;

        this.current.push(current);

      }

    }, 200);

    alt.setInterval(() => {

      for(let i=0; i<this.current.length; i++) {

        const current = this.current[i];

        switch(current.type) {

          case 'marker' : {
            natives.drawMarker(current.markerType, current.position.x, current.position.y, current.position.z, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, current.size, current.size, current.size, current.color.r, current.color.g, current.color.b, current.color.a, false, true, 2, null, null, false);
            break;
          }

          default: break;

        }

      }

      natives.drawRect(0, 0, 0, 0, 0, 0, 0, 0); // Anti-flickering hack for drawMarker

    }, 0);

    alt.setInterval(() => {

      const player   = alt.Player.local;
      const ped      = player.ped;
      const position = ped.position;

      for(let i=0; i<this.current.length; i++) {

        const current = this.current[i];
        const distance = $.utils.getDistance(position, current.position);

        if((distance <= current.size) && (this.in.indexOf(current) === -1)) {
          
          this.in.push(current);
          $.game.emit('interact:enter', current);

        } else if(distance >= current.size && (this.in.indexOf(current) !== -1)) {

          const idx  = this.in.indexOf(current);
          const uIdx = this.using.indexOf(current);

          this.in.splice(idx, 1);

          if(uIdx !== 1)
            this.using.splice(uIdx, 1);

          $.game.emit('interact:exit', current);

        }

      }

    }, 100);

    $.game.on('interact:enter', e => {
      Notification.create(e.data.message, 10000);
    });

    resolve();

  });

}

export default Interact;
