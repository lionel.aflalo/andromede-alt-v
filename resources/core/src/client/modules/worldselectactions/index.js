import * as alt     from 'alt'
import * as natives from 'natives';
import $            from '../../../lib/altv-api/client/index';
import WorldSelect  from '../worldselect';
import HUD          from '../hud';
import MM           from '../menumanager';
import Area         from '../area';

const { Entity, Ped, Player, Vehicle } = $.types;

const WorldSelectAtions = {
  menu  : null,
  locked: false,
};

WorldSelectAtions.__NAME = 'worldselectactions';

WorldSelectAtions.init = function() {
  
  return new Promise((resolve, reject) => {

    alt.on('keyup', key => {
      
      if(key == 222) {
  
        if(WorldSelectAtions.menu !== null)
          WorldSelectAtions.menu.destroy();
  
      }
  
    });
  
    $.game.on('ws:mousedown:right', ws => {
      
      if(WorldSelectAtions.locked)
        return;

      ws.enable(false, {mouse: true, camera: false, cursor: true})
      
      if(WorldSelectAtions.menu != null)
        WorldSelectAtions.menu.destroy();
        
      alt.setTimeout(async () => {

        const mdata = {title: 'Menu', data: []};
  
        const onMap = () => {

          mdata.title = 'Map';
    
          $.game.emit('ws:context',     ws, WorldSelectAtions, mdata);
          $.game.emit('ws:context:map', ws, WorldSelectAtions, mdata);

          for(let i=0; i<Area.areas.length; i++) {

            const area = Area.areas[i];

            if(area.isInside(ws.coords))
              $.game.emit('ws:context:area', ws, WorldSelectAtions, mdata, area);
          
          }

        }

        if(ws.current.target instanceof Player) {
    
          mdata.title = 'Citoyen';
    
          $.game.emit('ws:context',        ws, WorldSelectAtions, mdata);
          $.game.emit('ws:context:player', ws, WorldSelectAtions, mdata);
  
        } else if(ws.current.target instanceof Ped) {
    
          mdata.title = 'Citoyen lambda';
    
          $.game.emit('ws:context',     ws, WorldSelectAtions, mdata);
          $.game.emit('ws:context:ped', ws, WorldSelectAtions, mdata);
  
        } else if(ws.current.target instanceof Vehicle) {
    
          mdata.title = 'Véhicule';
    
          $.game.emit('ws:context',         ws, WorldSelectAtions, mdata);
          $.game.emit('ws:context:vehicle', ws, WorldSelectAtions, mdata);
  
        } else if(ws.current.target instanceof Entity) {
    
          if(ws.current.target.isObject) {
    
            mdata.title = 'Objet';
    
            $.game.emit('ws:context',        ws, WorldSelectAtions, mdata);
            $.game.emit('ws:context:object', ws, WorldSelectAtions, mdata);
  
          } else {
            
            onMap();
  
          }
    
        } else {
    
          onMap();
  
        }
  
        WorldSelectAtions.menu = MM.create(mdata.title, mdata.data, {
          x: ws.current.mouse.position.x,
          y: ws.current.mouse.position.y
        });
  
        const promises = [];

        $.game.emit('ws:menu:before', ws, WorldSelectAtions, WorldSelectAtions.menu, promises);

        WorldSelectAtions.menu.on('destroy', () => {
          
          WorldSelectAtions.menu = null;
         
          WorldSelect.camera.render(false);
  
          if(!WorldSelectAtions.preventDisable)
            HUD.disable();
  
          WorldSelectAtions.preventDisable = false;

        });
  
        HUD.enable(false);

        await Promise.all(promises);
        await $.utils.delay(0);
        
        $.game.emit('ws:menu', ws, WorldSelectAtions, WorldSelectAtions.menu);

        WorldSelectAtions.menu.open();
  
        $.game.emit('ws:blur', ws);

      }, 300);
  
    });
  
    $.game.on('ws:hover:entity', ws => {
  
      if(WorldSelectAtions.locked)
        return;

      if(!natives.doesEntityExist(ws.targetId))
        return;
  
      /*
      if(ws.target instanceof Player) {
        natives.setEntityAlpha(+ws.target.ped, 204);
      } else if (ws.target instanceof Vehicle) {
        natives.setEntityAlpha(ws.target.scriptID, 204);
      } else {
        natives.setEntityAlpha(+ws.target, 204);
      }
      */
  
    });
  
    $.game.on('ws:blur', ws => {
  
      if(WorldSelectAtions.locked)
        return;

      if(!natives.doesEntityExist(ws.targetId))
        return;
  
      /*
      if(ws.target instanceof Player) {
        natives.resetEntityAlpha(+ws.target.ped);
      } else if (ws.target instanceof Vehicle) {
        natives.resetEntityAlpha(ws.target.scriptID);
      } else {
        natives.resetEntityAlpha(+ws.target);
      }
      */
  
    });
  
    $.game.on('ws:enabled', ws => {
  
      if(WorldSelectAtions.locked)
        return;

      if(WorldSelectAtions.menu !== null)
        WorldSelectAtions.menu.destroy();
  
    });
  
    $.game.on('ws:disabled', ws => {


      if(WorldSelectAtions.menu !== null)
        WorldSelectAtions.menu.destroy();
  
    });

    resolve();

  });
  
}

import './actions';

export default WorldSelectAtions;