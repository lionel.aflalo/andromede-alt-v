import * as alt     from 'alt';
import * as natives from 'natives';

import $ from '../../../lib/altv-api/client/index';

const { INPUT_GROUPS, CONTROLS } = $.constants;
const { Vector3 } = $.math;

import Cache             from '../cache/index';
import HUD               from '../hud/index';
import WM                from '../windowmanager/index';
import MM                from '../menumanager/index';
import Inventory         from '../inventory/index';
import Notification      from '../notification/index'
import Area              from '../area/index';
import NPC               from '../npc/index';

import animData       from './anims.json';
import seatData       from './seats.json';
import clipsetData    from './clipsets.json';
import facialAnimData from './facial_anims.json';

const CUBE_SIZE = 100;

const seatHashes = Object.keys(seatData).map(e => $.utils.joaat(e));

// Animations
const anims = { default: [], vehicle: [] };
const animsByCategory = { default: {}, vehicle: {} };

animData.default.forEach(e => {
  animsByCategory.default[e.category] = animsByCategory.default[e.category] || [];
  animsByCategory.default[e.category].push(e);
});

animData.vehicle.forEach(e => {
  animsByCategory.vehicle[e.category] = animsByCategory.vehicle[e.category] || [];
  animsByCategory.vehicle[e.category].push(e);
});

for (let k in animsByCategory.default) {
  const item = { label: k, value: k, children: [] };
  const data = animsByCategory.default[k];

  for (let i = 0; i < data.length; i++) item.children.push(data[i]);

  anims.default.push(item);
}

for (let k in animsByCategory.vehicle) {
  const item = { label: k, value: k, children: [] };
  const data = animsByCategory.vehicle[k];

  for (let i = 0; i < data.length; i++) item.children.push(data[i]);

  anims.vehicle.push(item);
}

// Clipsets
const clipsetsByCategory = {};
const hashes = Object.keys(clipsetData).map(e => parseInt(e, 10));

for (let i = 0; i < hashes.length; i++) {
  const hash = hashes[i];
  clipsetsByCategory[hash] = {};

  clipsetData[hash].forEach(e => {
    clipsetsByCategory[hash][e.category] = clipsetsByCategory[hash][e.category] || [];
    clipsetsByCategory[hash][e.category].push(e);
  });
}

// Facial Animations
const facialByCategory = {};
const facialHashes = Object.keys(facialAnimData).map(e => parseInt(e, 10));

for (let i = 0; i < facialHashes.length; i++) {
  const hash = facialHashes[i];
  facialByCategory[hash] = {};

  facialAnimData[hash].forEach(e => {
    facialByCategory[hash][e.category] = facialByCategory[hash][e.category] || [];
    facialByCategory[hash][e.category].push(e);
  });
}

$.game.on('ws:context:player', async (ws, wsa, mdata) => {
  
  const resolvePlayer   = Cache.resolve({_type: 'Player', _id: alt.Player.local.getSyncedMeta('player')});
  const resolveIdentity = Cache.resolve({_type: 'Identity', _id: alt.Player.local.getSyncedMeta('identity')});

  $.game.once('ws:menu:before', (ws, wsa, menu, promises) => {
    promises.push(resolvePlayer);
    promises.push(resolveIdentity);
  });

  const player   = await resolvePlayer;
  const identity = await resolveIdentity;

  if(player.roles.indexOf('admin') !== -1) {
    mdata.data.push({label: '🔱 Admin', value: 'admin'});
  }

  if((identity.roles.indexOf('ems') !== -1) && (ws.target !== alt.Player.local)) {
    mdata.data.push({label: '💛 Réanimer', value: 'revive'});
  }

  if(ws.target === alt.Player.local) {

    mdata.data.push({label: '⛔ Arrêter tout',      value: 'clear_tasks'});
    mdata.data.push({label: '📦 Inventaire',        value: 'inventory'})
    mdata.data.push({label: '😄 Animation',         value: 'animation'});
    mdata.data.push({label: '🤓 Animation faciale', value: 'facial_animation'});
    mdata.data.push({label: '🚶  Démarche',          value: 'clipset'});

  }

  const wsTarget = ws.target;
  
  $.game.once('ws:menu', (ws, wsa, menu) => {
    
    menu.on('select', async (data, item) => {

      switch (item.value) {
        
        case 'admin': {

          const targetIdentityId = wsTarget.getSyncedMeta('identity');
          const targetId         = wsTarget.getSyncedMeta('player');
          const targetIdentity   = await Cache.resolve({_type: 'Identity', _id: targetIdentityId});
          const target           = await Cache.resolve({_type: 'Player', _id: targetId});

          const submenu = MM.open(`${target.name}`, [
            {label: 'roles', value: 'roles'},
            {label: 'kick',  value: 'kick'},
            {label: 'ban',   value: 'ban'},
          ]);

          submenu.on('select', async (data2, item2) => {

            submenu.destroy();

            switch(item2.value) {

              case 'roles' : {

                try {

                  const rolesMenu = MM.open('Roles', [
                    {label: 'Player',   type: 'input', name: 'player',   value: target        .roles.map(e => e.trim()).join(',')},
                    {label: 'Identity', type: 'input', name: 'identity', value: targetIdentity.roles.map(e => e.trim()).join(',')},
                    {label: 'Valider',  name: 'submit'},
                  ]);

                  rolesMenu.on('select', (data3, item3) => {
                    
                    const byName        = rolesMenu.by('name');
                    const playerRoles   = byName.player  .value.split(',').map(e => e.trim());
                    const identityRoles = byName.identity.value.split(',').map(e => e.trim());

                    switch(item3.name) {

                      case 'submit' : {
                        
                        rolesMenu.destroy();
                        alt.emitServer('hz:admin:roles:set', target._id, targetIdentity._id, playerRoles, identityRoles);
                        HUD.disable(false);
                        break;
                      }

                      default: break;
                    }

                  });


                } catch(e) { allt.logError(e); }

                break;
              }

              case 'kick' : {
                alt.emitServer('hz:admin:kick', targetId);
                break;
              }

              case 'ban' : {
                alt.emitServer('hz:admin:ban', targetId);
                break;
              }

              default: break;
            }

          });

          break;

        }

        case 'revive' : {
          const targetIdentityId = wsTarget.getSyncedMeta('identity');
          alt.emitServer('hz:identity:revive', targetIdentityId);
          break;
        }

        case 'clear_tasks': {
          $.game.player.ped.clearTasks();
          break;
        }

        case 'inventory' : {
          wsa.preventDisable = true;
          Inventory.open();
          HUD.enable(true);
          break;
        }

        case 'animation': {
          const submenu = MM.open('Animation', anims.default);

          menu.on('destroy', () => submenu.destroy());

          submenu.on('select', async (data2, item2) => {
            if (item2.animType === 'scenario') {
              natives.taskStartScenarioInPlace(+$.game.player.ped, item2.anim, 0, true);
            } else {
              natives.requestAnimDict(item2.lib);
              await $.utils.waitFor(() => natives.hasAnimDictLoaded(item2.lib));
              natives.taskPlayAnim(
                +$.game.player.ped,
                item2.lib,
                item2.anim,
                8.0,
                -8.0,
                -1,
                1,
                0,
                false,
                false,
                false
              );
            }
          });

          break;
        }

        case 'clipset': {
          const hash = +$.game.player.ped.model;
          const categories = [{ label: 'Normal', value: 'reset' }].concat(
            Object.keys(clipsetsByCategory[hash]).map(e => ({ label: e, value: e }))
          );

          const submenu = MM.open('Démarche', categories);

          menu.on('destroy', () => submenu.destroy());

          submenu.on('select', (data2, item2) => {
            if (item2.value === 'reset') {
              natives.resetPedMovementClipset(+$.game.player.ped, 1.0);
              return;
            }

            const childmenu = MM.open(item2.value, clipsetsByCategory[hash][item2.value]);

            menu.on('destroy', () => childmenu.destroy());

            childmenu.on('select', async (data3, item3) => {
              natives.requestAnimSet(item3.clipset);
              await $.utils.waitFor(() => natives.hasAnimSetLoaded(item3.clipset));
              natives.setPedMovementClipset(+$.game.player.ped, item3.clipset, 1.0);
            });
          });
          break;
        }

        case 'facial_animation': {

          const hash = +$.game.player.ped.model;
          const categories = [{ label: 'Normal', value: 'reset' }].concat(
            Object.keys(facialByCategory[hash]).map(e => ({ label: e, value: e }))
          );

          const submenu = MM.open('Animation faciale', categories);

          menu.on('destroy', () => submenu.destroy());

          submenu.on('select', (data2, item2) => {
            
            if(item2.value === 'reset') {
              alt.emitServer('hz:sync:player:setting.facial_anim', {lib: null, anim: null});
              return;
            }

            const childmenu = MM.open(item2.value, facialByCategory[hash][item2.value]);

            menu.on('destroy', () => childmenu.destroy());

            childmenu.on('select', async (data3, item3) => {
              alt.emitServer('hz:sync:player:setting.facial_anim', {lib: item3.lib, anim: item3.anim});
            });
          });
          break;
        }
      }

    });
  });
});

alt.on('syncedMetaChange', async (entity, key, value) => {

  if((key === 'setting.facial_anim') && (entity instanceof alt.Player)) {

    let anim = value;
    let curr = anim;
    
    natives.requestAnimDict(anim.lib);
    await $.utils.waitFor(() => natives.hasAnimDictLoaded(anim.lib));

    do {
      natives.playFacialAnim(+entity.ped, anim.anim, anim.lib);
      await $.utils.delay(2000);
      curr = entity.getSyncedMeta('setting.facial_anim');
    } while((curr.lib === anim.lib) && (curr.anim === anim.anim));
  }

});

// Seat
$.game.on('ws:context:object', (ws, wsa, mdata) => {
  const model = ws.target.model;

  //if(seatHashes.indexOf(model) !== -1) {

  mdata.data.push({ label: "💺 S'asseoir", value: 'seat' });

  $.game.once('ws:menu', (ws, wsa, menu) => {
    menu.on('select', async (data, item) => {
      switch (item.value) {
        case 'seat': {
          try {

            if (!$.game.player.ped.inRangeTo(ws.target.position, 1.5, true)) {
              natives.taskTurnPedToFaceEntity(+$.game.player.ped, +ws.target, -1);
              await $.utils.delay(1000);
              await $.game.player.ped.taskGoStraightToCoord(ws.target, 1.0, -1, $.game.player.ped.heading, 0.25);
            }

            const center = natives.getOffsetFromEntityInWorldCoords(+ws.target, 0.0, 0.0, 0.0);

            const results = [
              natives.getGroundZFor3dCoord(center.x, center.y, center.z - 0.2),
              natives.getGroundZFor3dCoord(center.x, center.y, center.z - 0.1),
              natives.getGroundZFor3dCoord(center.x, center.y, center.z + 0.0),
              natives.getGroundZFor3dCoord(center.x, center.y, center.z + 0.1),
              natives.getGroundZFor3dCoord(center.x, center.y, center.z + 0.2)
            ];

            const groundZs = results
              .filter(e => e[0] && e[1] !== 0)
              .map(e => e[1])
              .sort((a,b) => Math.abs(center.z - a) - Math.abs(center.z - b));

            if (groundZs.length > 0)
              natives.taskStartScenarioAtPosition(
                +$.game.player.ped,
                'PROP_HUMAN_SEAT_BENCH',
                center.x,
                center.y,
                groundZs[0] + 0.5,
                ws.target.heading + 180.0,
                0,
                true,
                false
              );
              
          } catch (e) {
            alt.logError(e.message, e.stack);
          }
        }
      }
    });
  });

  //}
});

$.game.on('ws:context:vehicle', async (ws, wsa, mdata) => {
  
  const resolvePlayer   = Cache.resolve({_type: 'Player', _id: alt.Player.local.getSyncedMeta('player')});
  const resolveIdentity = Cache.resolve({_type: 'Identity', _id: alt.Player.local.getSyncedMeta('identity')});

  $.game.once('ws:menu:before', (ws, wsa, menu, promises) => {
    promises.push(resolvePlayer);
    promises.push(resolveIdentity);
  });

  const player   = await resolvePlayer;
  const identity = await resolveIdentity;

  if(identity.roles.indexOf('mecano') !== -1) {
    mdata.data.push({label: '🆗 Réparer',  value: 'repair'});
    mdata.data.push({label: '🆗 Nettoyer', value: 'clean'});
  }

  const wsTarget = ws.target;
  
  $.game.once('ws:menu', (ws, wsa, menu) => {
    
    menu.on('select', async (data, item) => {

      switch (item.value) {
        
        case 'repair': {

          natives.setVehicleEngineHealth(wsTarget.scriptID, 1000);
          natives.setVehicleEngineOn(wsTarget.scriptID, true, true );
          natives.setVehicleFixed(wsTarget.scriptID);

          break;

        }

        case 'clean' : {
          natives.setVehicleDirtLevel(wsTarget.scriptID, 0);
          break;
        }

      }

    });
  });
});

$.game.on('ws:context:map', async (ws, wsa, mdata) => {
  
  const resolvePlayer   = Cache.resolve({_type: 'Player', _id: alt.Player.local.getSyncedMeta('player')});
  const resolveIdentity = Cache.resolve({_type: 'Identity', _id: alt.Player.local.getSyncedMeta('identity')});

  $.game.once('ws:menu:before', (ws, wsa, menu, promises) => {
    promises.push(resolvePlayer);
    promises.push(resolveIdentity);
  });

  const player   = await resolvePlayer;
  const identity = await resolveIdentity;

  if(player.roles.indexOf('admin') !== -1) {
    mdata.data.push({label: '🔱 Admin', value: 'admin', children: [
      {label: 'Area', value: 'area'},
      {label: 'NPC',  value: 'npc'},
    ]});
  }

  $.game.once('ws:menu', (ws, wsa, menu) => {
    
    menu.on('select', (data, item) => {
      
      switch (item.value) {
        
        case 'area': {

          alt.setTimeout(async () => {

            const _points = await Area.create();
            const points  = _points.map(e => ({x: e.x, y: e.y, z: e.z}));

            const submenu = MM.open('Area', [
              {label: 'name', type: 'input', name: 'name', value: 'area_' + $.utils.getRandomString(8)},
              {label: 'tags', type: 'input', name: 'tags', value: ''},
              {label: 'submit', name: 'submit', value: 'submit'}
            ]);
  
            submenu.on('select', async (data2, item2) => {

              const byName = submenu.by('name');
              const name   = byName.name.value;
              const tags   = byName.tags.value.split(',').map(e => e.trim());

              switch(item2.value) {

                case 'submit' : {
                  
                  submenu.destroy();
                  alt.emitServer('hz:admin:area:create', name, points, tags);
                  break;
                }

                default: break;

              }


            });

          }, 300);

          break;

        }

        case 'npc': {

          const coords = new Vector3(ws.coords.x, ws.coords.y, ws.coords.z);

          HUD.enable();

          alt.setTimeout(async () => {

            const submenu = MM.open('NPC', [
              {label: 'name',      type: 'input', name: 'name',      value: 'npc_' + $.utils.getRandomString(8)},
              {label: 'model',     type: 'input', name: 'model',     value: ''},
              {label: 'animdict',  type: 'input', name: 'animdict',  value: ''},
              {label: 'animation', type: 'input', name: 'animation', value: ''},
              {label: 'heading',   type: 'input', name: 'heading',   value: 0.0},
              {label: 'submit', name: 'submit', value: 'submit'},
            ]);
  
            submenu.on('select', async (data2, item2) => {

              const byName    = submenu.by('name');
              const name      = byName.name.value;
              const model     = byName.model.value;
              const animdict  = byName.animdict.value;
              const animation = byName.animation.value;
              const heading   = parseFloat(byName.heading.value);

              switch(item2.value) {

                case 'submit' : {
                  
                  submenu.destroy();

                  HUD.disable();

                  const ped = await NPC.create(model, coords);

                  if(ped === null) {

                    Notification.create('<span style="color:red">Error: ped does not exists</span>');

                  } else {

                    ped.heading = heading;

                    if((animdict !== '') && (animation !== '')) {

                      natives.requestAnimDict(animdict);
  
                      await $.utils.waitFor(() => natives.hasAnimDictLoaded(animdict), 2500);
  
                      if(natives.hasAnimDictLoaded(animdict))
                        natives.taskPlayAnim(+ped, animdict, animation, 1.0, -1.0, -1, 1, 1, false, false, false);
                      else
                        natives.taskStandStill(+ped, -1);
  
                    } else natives.taskStandStill(+ped, -1);
  
                    natives.freezeEntityPosition(+ped, true);
                    natives.setEntityInvincible(+ped, true);
                    natives.setBlockingOfNonTemporaryEvents(+ped, true);

                    const onTick = () => {

                      natives.disableControlAction(INPUT_GROUPS.MOVE, CONTROLS.MOVE_LR, true);
                      natives.disableControlAction(INPUT_GROUPS.MOVE, CONTROLS.MOVE_UD, true);
                      natives.disableControlAction(INPUT_GROUPS.MOVE, CONTROLS.LOOK_LR, true);
                      natives.disableControlAction(INPUT_GROUPS.MOVE, CONTROLS.LOOK_UD, true);
                      natives.disableControlAction(INPUT_GROUPS.MOVE, CONTROLS.AIM, true);
                      natives.disableControlAction(INPUT_GROUPS.MOVE, CONTROLS.VEH_MOUSE_CONTROL_OVERRIDE, true);
                    
                      natives.disableControlAction(INPUT_GROUPS.MOVE, CONTROLS.ATTACK, true);
                      natives.disableControlAction(INPUT_GROUPS.MOVE, CONTROLS.MELEE_ATTACK_LIGHT, true);
                      natives.disableControlAction(INPUT_GROUPS.MOVE, CONTROLS.MELEE_ATTACK_HEAVY, true);
                      natives.disableControlAction(INPUT_GROUPS.MOVE, CONTROLS.MELEE_ATTACK_ALTERNATE, true);
                      natives.disableControlAction(INPUT_GROUPS.MOVE, CONTROLS.ATTACK2, true);
                      natives.disableControlAction(INPUT_GROUPS.MOVE, CONTROLS.MELEE_ATTACK1, true);
                      natives.disableControlAction(INPUT_GROUPS.MOVE, CONTROLS.MELEE_ATTACK2, true);
                    
                      natives.disableControlAction(INPUT_GROUPS.MOVE, CONTROLS.WEAPON_WHEEL_UD, true);
                      natives.disableControlAction(INPUT_GROUPS.MOVE, CONTROLS.WEAPON_WHEEL_NEXT, true);
                      natives.disableControlAction(INPUT_GROUPS.MOVE, CONTROLS.WEAPON_WHEEL_PREV, true);
                      natives.disableControlAction(INPUT_GROUPS.MOVE, CONTROLS.SELECT_NEXT_WEAPON, true);
                      natives.disableControlAction(INPUT_GROUPS.MOVE, CONTROLS.SELECT_PREV_WEAPON, true);
                    
                      const xMagnitude = natives.getDisabledControlNormal(INPUT_GROUPS.MOVE, CONTROLS.LOOK_LR);
                      // const yMagnitude = natives.getDisabledControlNormal(INPUT_GROUPS.MOVE, CONTROLS.LOOK_UD);

                      ped.heading += xMagnitude * 25;

                    };

                    $.game.on('tick', onTick);

                    alt.setTimeout(async () => {

                      await $.game.waitClick();

                      $.game.removeListener('tick', onTick);
  
                      alt.emitServer('hz:admin:npc:create', name, model, {
                        x: ped.position.x,
                        y: ped.position.y,
                        z: ped.position.z,
                      }, ped.heading, animdict, animation);
  

                    }, 300);

                  }

                  break;

                }

                default: break;

              }


            });

          }, 300);

          break;

        }

        default: break;
      }
    });

  });

  //}
});

$.game.on('ws:context:ped', async (ws, wsa, mdata) => {
  
  const target = ws.target;
  const coords = new Vector3(ws.coords.x, ws.coords.y, ws.coords.z);

  const cube = {};

  cube.x = (coords.x - (coords.x % CUBE_SIZE)) / CUBE_SIZE;
  cube.y = (coords.y - (coords.y % CUBE_SIZE)) / CUBE_SIZE;
  cube.z = (coords.z - (coords.z % CUBE_SIZE)) / CUBE_SIZE;

  const resolvePlayer   = Cache.resolve({_type: 'Player', _id: alt.Player.local.getSyncedMeta('player')});
  const resolveIdentity = Cache.resolve({_type: 'Identity', _id: alt.Player.local.getSyncedMeta('identity')});
  const getNpcs         = $.requestp('hz:npc:get:cube', cube);

  $.game.once('ws:menu:before', (ws, wsa, menu, promises) => {
    promises.push(resolvePlayer);
    promises.push(resolveIdentity);
    promises.push(getNpcs);
  });

  const player   = await resolvePlayer;
  const identity = await resolveIdentity;
  const npcInfos = await getNpcs;

  if(player.roles.indexOf('admin') !== -1) {

    mdata.data.push({label: '🔱 Admin', value: 'admin', children: [
      {label: 'NPC',  value: 'npc'},
    ]});

  }

  $.game.once('ws:menu', (ws, wsa, menu) => {
    
    menu.on('select', async (data, item) => {
      
      switch (item.value) {
        
        case 'npc': {

          const npcs = await Cache.resolve(npcInfos);
          const npc  = npcs.find(e => ($.utils.joaat(e.model) === +target.model) || ($.utils.getDistance(coords, e.position) <= 0.1));

          if(!npc)
            return;

          HUD.enable();

          alt.setTimeout(async () => {

            const submenu = MM.open('NPC', [
              {label: 'name',      type: 'input', name: 'name',      value: npc.name},
              {label: 'model',     type: 'input', name: 'model',     value: npc.model},
              {label: 'animdict',  type: 'input', name: 'animdict',  value: npc.animdict},
              {label: 'animation', type: 'input', name: 'animation', value: npc.animation},
              {label: 'heading',   type: 'input', name: 'heading',   value: npc.heading},
              {label: 'delete', name: 'delete', value: 'delete'},
              {label: 'submit', name: 'submit', value: 'submit'},
            ]);
  
            submenu.on('select', async (data2, item2) => {

              const byName    = submenu.by('name');
              const _id       = npc._id;
              const name      = byName.name.value;
              const model     = byName.model.value;
              const animdict  = byName.animdict.value;
              const animation = byName.animation.value;
              const heading   = parseFloat(byName.heading.value);

              switch(item2.value) {

                case 'delete' : {

                  submenu.destroy();
                  HUD.disable();

                  alt.emitServer('hz:admin:npc:delete', _id);
                  
                  target.delete();
                  
                  break;
                }

                case 'submit' : {
                  
                  submenu.destroy();
                  HUD.disable();

                  const ped = target;

                  if(ped === null) {

                    Notification.create('<span style="color:red">Error: ped does not exists</span>');

                  } else {

                    ped.heading = heading;

                    if((animdict !== '') && (animation !== '')) {

                      natives.requestAnimDict(animdict);
  
                      await $.utils.waitFor(() => natives.hasAnimDictLoaded(animdict), 2500);
  
                      if(natives.hasAnimDictLoaded(animdict))
                        natives.taskPlayAnim(+ped, animdict, animation, 1.0, -1.0, -1, 1, 1, false, false, false);
                      else
                        natives.taskStandStill(+ped, -1);
  
                    } else natives.taskStandStill(+ped, -1);
  
                    natives.freezeEntityPosition(+ped, true);
                    natives.setEntityInvincible(+ped, true);
                    natives.setBlockingOfNonTemporaryEvents(+ped, true);

                    const onTick = () => {

                      natives.disableControlAction(INPUT_GROUPS.MOVE, CONTROLS.MOVE_LR, true);
                      natives.disableControlAction(INPUT_GROUPS.MOVE, CONTROLS.MOVE_UD, true);
                      natives.disableControlAction(INPUT_GROUPS.MOVE, CONTROLS.LOOK_LR, true);
                      natives.disableControlAction(INPUT_GROUPS.MOVE, CONTROLS.LOOK_UD, true);
                      natives.disableControlAction(INPUT_GROUPS.MOVE, CONTROLS.AIM, true);
                      natives.disableControlAction(INPUT_GROUPS.MOVE, CONTROLS.VEH_MOUSE_CONTROL_OVERRIDE, true);
                    
                      natives.disableControlAction(INPUT_GROUPS.MOVE, CONTROLS.ATTACK, true);
                      natives.disableControlAction(INPUT_GROUPS.MOVE, CONTROLS.MELEE_ATTACK_LIGHT, true);
                      natives.disableControlAction(INPUT_GROUPS.MOVE, CONTROLS.MELEE_ATTACK_HEAVY, true);
                      natives.disableControlAction(INPUT_GROUPS.MOVE, CONTROLS.MELEE_ATTACK_ALTERNATE, true);
                      natives.disableControlAction(INPUT_GROUPS.MOVE, CONTROLS.ATTACK2, true);
                      natives.disableControlAction(INPUT_GROUPS.MOVE, CONTROLS.MELEE_ATTACK1, true);
                      natives.disableControlAction(INPUT_GROUPS.MOVE, CONTROLS.MELEE_ATTACK2, true);
                    
                      natives.disableControlAction(INPUT_GROUPS.MOVE, CONTROLS.WEAPON_WHEEL_UD, true);
                      natives.disableControlAction(INPUT_GROUPS.MOVE, CONTROLS.WEAPON_WHEEL_NEXT, true);
                      natives.disableControlAction(INPUT_GROUPS.MOVE, CONTROLS.WEAPON_WHEEL_PREV, true);
                      natives.disableControlAction(INPUT_GROUPS.MOVE, CONTROLS.SELECT_NEXT_WEAPON, true);
                      natives.disableControlAction(INPUT_GROUPS.MOVE, CONTROLS.SELECT_PREV_WEAPON, true);
                    
                      const xMagnitude = natives.getDisabledControlNormal(INPUT_GROUPS.MOVE, CONTROLS.LOOK_LR);
                      // const yMagnitude = natives.getDisabledControlNormal(INPUT_GROUPS.MOVE, CONTROLS.LOOK_UD);

                      ped.heading += xMagnitude * 25;

                    };

                    $.game.on('tick', onTick);

                    alt.setTimeout(async () => {

                      await $.game.waitClick();

                      $.game.removeListener('tick', onTick);
  
                      alt.emitServer('hz:admin:npc:edit', _id, name, model, {
                        x: ped.position.x,
                        y: ped.position.y,
                        z: ped.position.z,
                      }, ped.heading, animdict, animation);
  

                    }, 300);

                  }

                  break;

                }

                default: break;

              }


            });

          }, 300);

          break;

        }

        default: break;
      }
    });

  });

  //}
});

