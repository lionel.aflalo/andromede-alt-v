import * as alt from 'alt'
import * as natives from 'natives';

import $ from '../../../lib/altv-api/client/index';
const { Vector3 } = $.math;

import Cache from '../cache/index';
import HUD   from '../hud/index';
import WS    from '../worldselect/index';
import WSA   from '../worldselectactions/index';

const Area = {
  coords : new Vector3(0, 0, 0),
  offset : 1.0,
  points : [],
  areas  : [],
  debug  : false,
};

Area.__NAME = 'area';

Area.create = function() {

  return new Promise(async (resolve, reject) => {
    
    try {

      if(WS.enabled) {
        resolve(null);
        return;
      }
  
      WSA.locked         = true;
      Area.points.length = 0;
    
      WS.enable(true, {mouse: true, camera: true, cursor: false});
      
      $.game.on('tick', Area.tickCreate);
  
      let _btn = null;
  
      while(_btn !== 'right') {
        const { btn, x, y } = await $.game.waitClick();
        Area.points.push(Area.coords);
        _btn = btn;
      }
  
      const points = Area.points.slice(0);
  
      Area.points.length = 0;
  
      Area.disableCreateSelection();
  
      resolve(points);

    } catch(e) { alt.logError(e.message, e.stack); }


  });

}

Area.disableCreateSelection = function() {

  WSA.locked = false;

  WS.enable(false, {mouse: true, camera: true, cursor: false});

  $.game.removeListener('tick', Area.tickCreate);
}

Area.enableGridSelection = function(offset) {
  
  return new Promise(async (resolve, reject) => {
    
    if(WS.enabled) {
      resolve(null);
      return;
    }
    
    WSA.locked  = true;
    Area.offset = offset;
  
    WS.enable(true, {mouse: true, camera: true, cursor: false});
    
    $.game.on('tick', Area.tickGrid);
    
    const { btn, x, y } = await $.game.waitClick();
    
    Area.disableGridSelection();

    resolve(btn === 'left' ? Area.coords : null);

  });
}

Area.disableGridSelection = function() {

  WSA.locked = false;

  WS.enable(false, {mouse: true, camera: true, cursor: false});

  $.game.removeListener('tick', Area.tickGrid);
}

Area.tickCreate = function() {
  
  Area.coords = new Vector3(
    WS.coords.x,
    WS.coords.y,
    WS.coords.z
  );

  const points = Area.points.concat([WS.coords]);

  for(let i=1; i<points.length; i++) {

    const p1 = points[i - 1];
    const p2 = points[i];

    for(let j=0; j<20; j++)
      natives.drawLine(p1.x, p1.y, p1.z + j / 20, p2.x, p2.y, p2.z + j / 20, 0, 255, 0, 255);

    if(i + 1 === points.length) {

      const p1 = points[i];
      const p2 = points[0];
  
      for(let j=0; j<20; j++)
        natives.drawLine(p1.x, p1.y, p1.z + j / 20, p2.x, p2.y, p2.z + j / 20, 0, 255, 0, 255);

    }

  }

  Area.render();
}

Area.tickGrid = function() {
  
  Area.coords = new Vector3(
    WS.coords.x - WS.coords.x % Area.offset,
    WS.coords.y - WS.coords.y % Area.offset,
    WS.coords.z
  );

  Area.render();
}

Area.render = function() {
  natives.drawMarker(23, Area.coords.x, Area.coords.y, Area.coords.z + 0.05, 0.0, 0.0, 0.0, 0.0, 180.0, 0.0, Area.offset, Area.offset, Area.offset, 128, 0, 0, 75, false, true, 2, null, null, false);
  natives.drawRect(0, 0, 0, 0, 0, 0, 0, 0); // Anti-flickering hack
};

Area.renderDebug = function() {

  for(let a=0; a<this.areas.length; a++) {

    const points = this.areas[a].points;

    for(let i=1; i<points.length; i++) {
  
      const p1 = points[i - 1];
      const p2 = points[i];
  
      for(let j=0; j<20; j++)
        natives.drawLine(p1.x, p1.y, p1.z + j / 20, p2.x, p2.y, p2.z + j / 20, 0, 255, 0, 255);

      if(i + 1 === points.length) {

        const p1 = points[i];
        const p2 = points[0];
    
        for(let j=0; j<20; j++)
          natives.drawLine(p1.x, p1.y, p1.z + j / 20, p2.x, p2.y, p2.z + j / 20, 0, 255, 0, 255);

      }
  
    }

  }

  natives.drawRect(0, 0, 0, 0, 0, 0, 0, 0); // Anti-flickering hack
};

Area.init = function() {

  return new Promise(async (resolve, reject) => {

    const infos = await $.requestp('hz:area:getall');
    this.areas  = await Cache.resolve(infos);

    alt.setInterval(async () => {
      const infos = await $.requestp('hz:area:getall');
      this.areas  = await Cache.resolve(infos);
    }, 60000);

    alt.setInterval(() => {
      if(this.debug)
        this.renderDebug();
    }, 0);

    resolve();

  });

}

export default Area;
