import * as alt     from 'alt';
import * as natives from 'natives';

import $             from '../../../lib/altv-api/client/index';
import * as entities from '../../entities/index';
import Cache         from '../cache/index';

const Vehicle = {
  keyCache  : [],
  bypassKeys: false,
};

Vehicle.__NAME = 'vehicle';

Vehicle.init = function() {
  
  return new Promise((resolve, reject) => {

    alt.setInterval(async () => {

      try {

        const identityId = alt.Player.local.getSyncedMeta('identity');

        if(!identityId)
          return;
  
        const identity = await Cache.resolve({_type: 'Identity', _id: identityId});
        const keyCache = [];
  
        for(let i=0; i<identity.containers.length; i++) {
  
          const container = identity.containers[i];

          if(!container)
            continue;

          const items = container.items;
  
          for(let j=0; j<items.length; j++) {
  
            const item = items[j];
  
            if(!item)
              continue;

            if(item instanceof entities.HZItemKey) {
  
              const targets = item.targets.filter(e => e.type === 'vehicle');
              
              for(let k=0; k<targets.length; k++)
                keyCache.push((Math.floor(targets[k].model) + '_' + targets[k].plate).toLowerCase());
  
            }
  
          }
    
        }

        Vehicle.keyCache = keyCache;
  
        // alt.log(JSON.stringify(Vehicle.keyCache, null, 2));

      } catch(e) { alt.logError(e.message, e.stack); }

    }, 2500);

    alt.setInterval(async () => {

      const playerId = alt.Player.local.getSyncedMeta('player');

      if(!playerId)
        return;

      const player = await Cache.resolve({_type: 'Player', _id: playerId});

      Vehicle.bypassKeys = player.roles.indexOf('admin') !== -1;

    }, 8000);

    alt.setInterval(() => {

      if(Vehicle.bypassKeys)
        return;

      const ped = $.game.player.ped;
      const veh = natives.getVehiclePedIsTryingToEnter(+ped);

      if(veh !== 0 && (natives.getPedInVehicleSeat(veh, -1) === 0)) {

        const vehicle = alt.Vehicle.all.find(e => e.scriptID === veh);
        const str     = (Math.floor(natives.getEntityModel(veh)) + '_' + natives.getVehicleNumberPlateText(veh)).toLowerCase();

        // alt.log(str);

        if(vehicle && (Vehicle.keyCache.indexOf(str) === -1)) {
          natives.clearPedTasksImmediately(+ped);
        }

      }

    }, 250);

    resolve();
  });

}

export default Vehicle;
