import * as alt from 'alt'
import * as natives from 'natives';

import $    from '../../../lib/altv-api/client/index';
import Area from '../area/index';

const { Vector3 }                     = $.math;
const { Camera, Entity, Ped, Player } = $.types;

const WorldSelect = {
  enabled  : false,
  locked   : false,
  camera   : null,
  keyIsDown: false,
  hit      : false,
  coords   : new Vector3(0, 0, 0),
  surface  : new Vector3(0, 0, 0),
  entity   : null,
  target   : null,
  targetId : null,
  current  : {},
  screen   : {x: 1920, y: 1080},
  timeout  : -1,
  mouse : {
    position: {x: 0, y: 0},
    ratio   : {x: 0, y: 0},
  },
};

WorldSelect.__NAME = 'worldselect';

WorldSelect.init = async function() {

  WorldSelect.camera = Camera.create();

  alt.on('keydown', key => {
    
    if(key == 222) {
      
      WorldSelect.keyIsDown = true;

      if(WorldSelect.locked)
        return;

        alt.clearTimeout(WorldSelect.timeout);

        this.timeout = alt.setTimeout(() => {
          if(WorldSelect.keyIsDown)
            WorldSelect.enable(true);
        }, 100);

    }

  });
  
  alt.on('keyup', key => {

    if(key == 222) {

      WorldSelect.keyIsDown = false;

      if(WorldSelect.locked)
        return;

      WorldSelect.enable(false);
    }

  });

  $.game.on('tick', WorldSelect.lockonTick);
}

WorldSelect.onTick = function() {

  natives.setPlayerLockon(natives.playerId(), false);

  const {x, y}               = alt.getCursorPos();
  WorldSelect.mouse.position = {x, y};
  WorldSelect.mouse.ratio    = {x: x / WorldSelect.screen.x, y: y / WorldSelect.screen.y};

  const fov     = WorldSelect.camera.fov;
  const near    = 0.0;
  const far     = 1000.0;
  const right   = WorldSelect.camera.rightVector;
  const forward = WorldSelect.camera.forwardVector;
  const up      = WorldSelect.camera.upVector;
  const at      = WorldSelect.camera.position;
  const result  = $.helpers.screenToWorld(x, y, fov, near, far, right, forward, up, at);
  
  const rayHandle                                  = natives.startShapeTestRay(result.near.x, result.near.y, result.near.z, result.far.x, result.far.y, result.far.z, -1, 0, 0)
  const [_rayHandle, hit, coords, surface, entity] = natives.getShapeTestResult(rayHandle);

  WorldSelect.hit     = hit;
  WorldSelect.coords  = coords;
  WorldSelect.surface = surface;
  WorldSelect.entity  = entity;

  if(hit) {

    if(entity === 0) {

      if(WorldSelect.target !== null) {
        $.game.emit('ws:blur', WorldSelect);
      }
  
      WorldSelect.hit     = true;
      WorldSelect.coords  = new Vector3(0, 0, 0);
      WorldSelect.surface = new Vector3(0, 0, 0);
      WorldSelect.entity  = null;
      WorldSelect.target  = null;

    } else {

      if(WorldSelect.target !== null) {

        if(WorldSelect.target instanceof Player) {

          if(+WorldSelect.target.ped !== entity)
            $.game.emit('ws:blur', WorldSelect);
        
        } else if(+WorldSelect.target !== entity) {
          
          $.game.emit('ws:blur', WorldSelect);
        
        }

        
      }

      if(natives.isEntityAPed(entity)) {

        if(natives.isPedAPlayer(entity)) {
          
          WorldSelect.target   = alt.Player.all.find(e => e.scriptID === entity) || null;
          WorldSelect.targetId = WorldSelect.target ? WorldSelect.target.scriptID : null;
          
          $.game.emit('ws:hover:entity', WorldSelect);
          $.game.emit('ws:hover:player', WorldSelect);
        
        } else {

          WorldSelect.target = new Ped(entity);
          
          $.game.emit('ws:hover:entity', WorldSelect);
          $.game.emit('ws:hover:ped',    WorldSelect);

        }

      } else if(natives.isEntityAVehicle(entity)) {
        
        WorldSelect.target   = alt.Vehicle.all.find(e => e.scriptID === entity) || null;
        WorldSelect.targetId = WorldSelect.target ? WorldSelect.target.scriptID : null;

        $.game.emit('ws:hover:entity',  WorldSelect);
        $.game.emit('ws:hover:vehicle', WorldSelect);
      
      } else if(natives.isEntityAnObject(entity)) {

        WorldSelect.target   = new Entity(entity);
        WorldSelect.targetId = +WorldSelect.target;

        $.game.emit('ws:hover:entity', WorldSelect);
        $.game.emit('ws:hover:object', WorldSelect);
      
      } else {
        
        for(let i=0; i<Area.areas.length; i++) {

          const area = Area.areas[i];

          if(area.isInside(WorldSelect.coords))
            $.game.emit('ws:hover:area', WorldSelect, area);
        
        }

        if(WorldSelect.target !== null) {
          $.game.emit('ws:blur', WorldSelect);
        }
    
        WorldSelect.hit     = false;
        WorldSelect.entity  = null;
        WorldSelect.target  = null;

      }

    }

  } else {

    if(WorldSelect.target !== null) {
      $.game.emit('ws:blur', WorldSelect);
    }

    WorldSelect.hit     = false;
    WorldSelect.coords  = new Vector3(0, 0, 0);
    WorldSelect.surface = new Vector3(0, 0, 0);
    WorldSelect.entity  = null;
    WorldSelect.target  = null;

  }

};

WorldSelect.lockonTick = function() {

  if(WorldSelect.enabled)
    natives.setPlayerLockon(natives.playerId(), false);
  else
    natives.setPlayerLockon(natives.playerId(), true);

}

WorldSelect.onLeftClick = function() {
  $.game.emit('ws:mousedown:left', WorldSelect);
};

WorldSelect.onRightClick = function() {
  
  WorldSelect.current = undefined;

  WorldSelect.current = {
    ...WorldSelect,
    coords: new Vector3(WorldSelect.coords.x, WorldSelect.coords.y, WorldSelect.coords.z),
    mouse: {
      position: {...WorldSelect.mouse.position},
      ratio   : {...WorldSelect.mouse.ratio},
    },
    screen: {...WorldSelect.screen},
  };
  
  $.game.emit('ws:mousedown:right', WorldSelect);

};

WorldSelect.enable = function(enabled = true, options = {mouse: true, camera: true, cursor: true, click: true, context: true}) {

  options = {mouse: true, camera: true, cursor: true, click: true, context: true, ...options};

  if(enabled) {
    
    if(WorldSelect.enabled)
      return;

    WorldSelect.enabled = true;

    const [_, screenX, screenY] = natives.getActiveScreenResolution();
    
    WorldSelect.screen.x = screenX;
    WorldSelect.screen.y = screenY;
    
    if(options.camera) {
      WorldSelect.camera.fov      = $.game.camera.fov;
      WorldSelect.camera.position = $.game.camera.position;
      WorldSelect.camera.rotation = $.game.camera.rotation;
  
      WorldSelect.camera.render(true, 1000);
    }

    if(options.cursor)
      alt.showCursor(true);

    if(options.mouse)
      $.game.enableMouseInput(false);

    $.game.on('tick', WorldSelect.onTick);

    if(options.click)
      $.game.on('mousedown:left', WorldSelect.onLeftClick);

    if(options.context)
      $.game.on('mousedown:right', WorldSelect.onRightClick);

    $.game.emit('ws:enabled', WorldSelect);

  } else {

    if(!WorldSelect.enabled)
      return;

    WorldSelect.enabled = false;

    if(options.camera)
      WorldSelect.camera.render(false, 1000);
      
    if(options.cursor) {
      try { alt.showCursor(false); } catch(e) {};
    }
    
    if(options.mouse)
      $.game.enableMouseInput(true);
  
    $.game.removeListener('tick', WorldSelect.onTick);

    if(options.click)
      $.game.removeListener('mousedown:left', WorldSelect.onLeftClick);

    if(options.context)
      $.game.removeListener('mousedown:right', WorldSelect.onRightClick);

    $.game.emit('ws:blur', WorldSelect);
    $.game.emit('ws:disabled', WorldSelect);

  }

}

WorldSelect.lock = function(locked = true) {
  WorldSelect.locked = locked;
}

export default WorldSelect;