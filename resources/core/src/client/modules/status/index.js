import * as alt from 'alt'
import * as natives from 'natives';

import WM from '../windowmanager/index.js';

const Status = {
  win: null,
};

Status.__NAME = 'status';

Status.update = function(status) {
  this.win.postMessage({action: 'set_status', status});
}

Status.init = function() {

  return new Promise((resolve, reject) => {

    const [_, screenX, screenY] = natives.getActiveScreenResolution();

    this.win = WM.open({
      url        : 'http://resource/dist/html/status/index.html',
      closable   : false,
      stayinspace: false,
      classname  : 'transparent notitle fitcontent',
      width      : 200,
      x          : screenX / 2 - 200 / 2,
      y          : 10,
    });

    alt.onServer('hz:status:update', status => Status.update(status));

    resolve();

  });

}

export default Status;
