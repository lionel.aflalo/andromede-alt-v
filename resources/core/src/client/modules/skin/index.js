/// <reference path="../../typings/altv-client.d.ts" />

import * as alt          from 'alt';
import * as natives      from 'natives';
import $                 from '../../../lib/altv-api/client/index';
import HUD               from '../hud/index';
import WorldSelect       from '../worldselect/index';
import MM                from '../menumanager/index';

const { Vector3 }         = $.math;
const { Camera, Model }   = $.types;
const { joaat, nextTick } = $.utils;

const {
  PED_BONES,
  PED_COMPONENTS,
  PED_PROPS,
  PED_OVERLAYS
} = $.constants;

const Skin = {
  camera : null,
  menu   : null,
  fathers: [0, 1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14, 15, 16, 17, 18, 19, 20, 42, 43, 44],
  mothers: [21, 22, 23, 24, 25, 26, 27, 28, 29, 30, 31, 32, 33, 34, 35, 36, 37, 38, 39, 40, 41, 45],
  factors: [],
  names  : {
    fathers: ['Joao', 'Daniel', 'Mondesir', 'Mamadou', 'Pierre', 'Juan', 'Zixuan', 'Haoyu', 'Evan', 'Ethan', 'Vicente', 'Horacio', 'Diego', 'Adrian', 'Ernesto', 'Michael', 'Santiago', 'Junjie', 'Hao', 'Samuel', 'Anthony', 'Jeffrey', 'Mirko', 'John'],
    mothers: ['Hannah', 'Helene', 'Claudie', 'Gisele', 'Amelie', 'Maria', 'Zihan', 'Debbie', 'Camila', 'Violette', 'Lina', 'Evelyne', 'Nicole', 'Dorothee', 'Gracie', 'Briana', 'Nathalie', 'Olivia', 'Jiayi', 'Charlotte', 'Marina', 'Sonia'],
    factors: [],
  },
  faceFeatures : {
    data  : ['Nose_Width', 'Nose_Peak_Hight', 'Nose_Peak_Length', 'Nose_Bone_High', 'Nose_Peak_Lowering', 'Nose_Bone_Twist', 'EyeBrown_High', 'EyeBrown_Forward', 'Cheeks_Bone_High', 'Cheeks_Bone_Width', 'Cheeks_Width', 'Eyes_Openning', 'Lips_Thickness', 'Jaw_Bone_Width', 'Jaw_Bone_Back_Length', 'Chimp_Bone_Lowering', 'Chimp_Bone_Length', 'Chimp_Bone_Width', 'Chimp_Hole', 'Neck_Thikness',],
    labels: ['Taille du nez', 'Pointe du nez', 'Longueur du nez', 'Os du nez haut ', 'Abaissement du pic du nez', 'Torsion du nez  ', 'Sourcils', 'Cils', 'Joues - Haut', 'Largeur des os des joues', 'Largeur des joues', 'Ouverture des yeux', 'Epaisseur des lèvres', 'Largeur de la machoire', 'Longueur de la machoire', 'Abaissement de la machoire', 'Longueur du menton', 'Largeur du menton', 'Creux du menton', 'Epaisseur de la nuque',]
  }
};

Skin.factors       = Skin.mothers.concat(Skin.fathers);
Skin.names.factors = Skin.names.mothers.concat(Skin.names.fathers);

Skin.__NAME = 'skin';

Skin.create = async function() {
  
  $.game.enableMouseInput(false);
  WorldSelect.lock(true);

  if(this.camera !== null)
    this.camera.destroy();

  $.game.player.ped.freeze(true);

  const direction = $.game.player.ped.direction.scale(2.5);
  const position  = $.game.player.ped.position.clone().add(direction);
  this.camera     = Camera.create(position);

  this.camera.point($.game.player.ped);
  this.camera.render();

  await nextTick();

  await this.openMenu();

  $.game.enableMouseInput(true);
  WorldSelect.lock(false);

  this.camera.render(false);
  this.camera.destroy();

  $.game.player.ped.freeze(false);

  this.camera = null;
  this.menu   = null;

  HUD.disable(true);
}

Skin.openMenu = function() {

  return new Promise(async (resolve, reject) => {
    
    try {

      WorldSelect.locked = true;
      const ped          = $.game.player.ped;
      const components   = ped.components.get();

      this.menu = MM.open('Apparence', [
        {type: 'slider', name: 'gender', label: 'Genre', options: ['H', 'F'], data: {models: ['mp_m_freemode_01', 'mp_f_freemode_01']}},
        {name: 'head', label: 'Tête', children: [
          {name: 'head.shape',         label : 'Visage'},      
          {name: 'head.details',       label : 'Détails'},                
          {name: 'head.face_features', label : 'Caractéristiques'}, 
          {name: 'head.hairs',         label : 'Cheveux'},                                         
          {name: 'head.accessories',   label : 'Accessoires'},                
        ]},
        {name: 'body_up', label: 'Haut du corps', children: [
          {type: 'slider', name: 'body_up.ACCS_drawable',  label: 'T-Shirt',             value: components[PED_COMPONENTS.ACCS].drawable, min: 0, max: components[PED_COMPONENTS.ACCS].drawableCount - 1, data: {component: PED_COMPONENTS.ACCS}},
          {type: 'slider', name: 'body_up.ACCS_texture',   label: 'Coloris t-shirt',     value: components[PED_COMPONENTS.ACCS].texture,  min: 0, max: components[PED_COMPONENTS.ACCS].textureCount  - 1, data: {component: PED_COMPONENTS.ACCS}},
          {type: 'slider', name: 'body_up.JBIB_drawable',  label: 'Torse',               value: components[PED_COMPONENTS.JBIB].drawable, min: 0, max: components[PED_COMPONENTS.JBIB].drawableCount - 1, data: {component: PED_COMPONENTS.JBIB}},
          {type: 'slider', name: 'body_up.JBIB_texture',   label: 'Coloris torse',       value: components[PED_COMPONENTS.JBIB].texture,  min: 0, max: components[PED_COMPONENTS.JBIB].textureCount  - 1, data: {component: PED_COMPONENTS.JBIB}},
          {type: 'slider', name: 'body_up.UPPR_drawable',  label: 'Bras',                value: components[PED_COMPONENTS.UPPR].drawable, min: 0, max: components[PED_COMPONENTS.UPPR].drawableCount - 1, data: {component: PED_COMPONENTS.UPPR}},     
          {type: 'slider', name: 'body_up.UPPR_texture',   label: 'Coloris bras',        value: components[PED_COMPONENTS.UPPR].texture,  min: 0, max: components[PED_COMPONENTS.UPPR].textureCount  - 1, data: {component: PED_COMPONENTS.UPPR}},    
          {type: 'slider', name: 'body_up.TEEF_drawable',  label: 'Chaine',              value: components[PED_COMPONENTS.TEEF].drawable, min: 0, max: components[PED_COMPONENTS.TEEF].drawableCount - 1, data: {component: PED_COMPONENTS.TEEF}},   
          {type: 'slider', name: 'body_up.TEEF_texture',   label: 'Texture',             value: components[PED_COMPONENTS.TEEF].texture,  min: 0, max: components[PED_COMPONENTS.TEEF].textureCount  - 1, data: {component: PED_COMPONENTS.TEEF}},
          {type: 'slider', name: 'body_up.DECL_drawable',  label: 'Calques',             value: components[PED_COMPONENTS.DECL].drawable, min: 0, max: components[PED_COMPONENTS.DECL].drawableCount - 1, data: {component: PED_COMPONENTS.DECL}},     
          {type: 'slider', name: 'body_up.DECL_texture',   label: 'Coloris calques',     value: components[PED_COMPONENTS.DECL].texture,  min: 0, max: components[PED_COMPONENTS.DECL].textureCount  - 1, data: {component: PED_COMPONENTS.DECL}},
          {type: 'slider', name: 'body_up.HAND_drawable',  label: 'Sac',                 value: components[PED_COMPONENTS.HAND].drawable, min: 0, max: components[PED_COMPONENTS.HAND].drawableCount - 1, data: {component: PED_COMPONENTS.HAND}},
          {type: 'slider', name: 'body_up.HAND_texture',   label: 'Coloris sac',         value: components[PED_COMPONENTS.HAND].texture,  min: 0, max: components[PED_COMPONENTS.HAND].textureCount  - 1, data: {component: PED_COMPONENTS.HAND}},
          {type: 'slider', name: 'body_up.TASK_drawable',  label: 'Pare-balles',         value: components[PED_COMPONENTS.TASK].drawable, min: 0, max: components[PED_COMPONENTS.TASK].drawableCount - 1, data: {component: PED_COMPONENTS.TASK}},
          {type: 'slider', name: 'body_up.TASK_texture',   label: 'Coloris pare-balles', value: components[PED_COMPONENTS.TASK].texture,  min: 0, max: components[PED_COMPONENTS.TASK].textureCount  - 1, data: {component: PED_COMPONENTS.TASK}},
        ]},
        {name: 'body_down', label: 'Bas du corps', children: [
          {type: 'slider', name: 'body_down.LOWR_drawable', label: 'Pantalon',           value: components[PED_COMPONENTS.LOWR].drawable, min: 0, max: components[PED_COMPONENTS.LOWR].drawableCount - 1, data: {component: PED_COMPONENTS.LOWR}},
          {type: 'slider', name: 'body_down.LOWR_texture',  label: 'Coloris pantalon',   value: components[PED_COMPONENTS.LOWR].texture,  min: 0, max: components[PED_COMPONENTS.LOWR].textureCount  - 1, data: {component: PED_COMPONENTS.LOWR}},
          {type: 'slider', name: 'body_down.FEET_drawable', label: 'Chaussures',         value: components[PED_COMPONENTS.FEET].drawable, min: 0, max: components[PED_COMPONENTS.FEET].drawableCount - 1, data: {component: PED_COMPONENTS.FEET}},
          {type: 'slider', name: 'body_down.FEET_texture',  label: 'Coloris chaussures', value: components[PED_COMPONENTS.FEET].texture,  min: 0, max: components[PED_COMPONENTS.FEET].textureCount  - 1, data: {component: PED_COMPONENTS.FEET}},
        ]},
        {name: 'submit', label: 'Valider'},
      ]);
  
      this.menu.on('destroy', () => {
        WorldSelect.locked = false;
        resolve();
      });
  
      this.menu.on('load', () => {
        HUD.enable(false);
      });
  
      this.menu.on('select', async (data, item) => {

        try {

          switch(item.name) {
          
            case 'head' : {
  
              const direction      = $.game.player.ped.direction.scale(0.75);
              let   position       = $.game.player.ped.position.clone().add(direction);
              const coords         = natives.getPedBoneCoords(alt.Player.local.scriptID, PED_BONES.IK_Head, 0.0, 0.0, 0.0);
              const camPosition    = this.camera.position;
              camPosition.x        = position.x;
              camPosition.y        = position.y;
              camPosition.z        = coords.z;
              this.camera.position = camPosition;
  
              natives.pointCamAtPedBone(+this.camera, alt.Player.local.scriptID, PED_BONES.IK_Head, 0.0, 0.0, 0.0, true);
              
              break;
            }

            case 'head.shape' : {
              this.menu.close();
              await this.openHeadShapeMenu();
              this.menu.open();
              break;
            }

            case 'head.details' : {
              this.menu.close();
              await this.openHeadDetailsMenu();
              this.menu.open();
              break;
            }

            case 'head.face_features' : {
              this.menu.close();
              await this.openFaceFeatureMenu();
              this.menu.open();
              break;
            }
  
            case 'head.hairs' : {
              this.menu.close();
              await this.openHeadHairsMenu();
              this.menu.open();
              break;
            }

            case 'head.accessories' : {
              this.menu.close();
              await this.openHeadAccessoriesMenu();
              this.menu.open();
              break;
            }

            case 'body_up' : {
  
              const direction      = $.game.player.ped.direction.scale(1.2);
              let   position       = $.game.player.ped.position.clone().add(direction);
              const coords         = natives.getPedBoneCoords(alt.Player.local.scriptID, PED_BONES.SKEL_Spine3, 0.0, 0.0, 0.0);
              const camPosition    = this.camera.position;
              camPosition.x        = position.x;
              camPosition.y        = position.y;
              camPosition.z        = coords.z;
              this.camera.position = camPosition;
  
              natives.pointCamAtPedBone(+this.camera, alt.Player.local.scriptID, PED_BONES.SKEL_Spine3, 0.0, 0.0, 0.0, true);
              
              break;
            }
  
            case 'body_down' : {

              const direction      = $.game.player.ped.direction.scale(1.5);
              let   position       = $.game.player.ped.position.clone().add(direction);
              const coords         = natives.getPedBoneCoords(alt.Player.local.scriptID, PED_BONES.MH_L_Knee, 0.0, 0.0, 0.0);
              const camPosition    = this.camera.position;
              camPosition.x        = position.x;
              camPosition.y        = position.y;
              camPosition.z        = coords.z + 0.8;
              this.camera.position = camPosition;
  
              natives.pointCamAtPedBone(+this.camera, alt.Player.local.scriptID, PED_BONES.SKEL_L_Foot, 0.0, 0.0, 0.0, true);
              
              break;
            }
  
            case 'submit' : {
              this.menu.destroy();
              break;
            }
  
            default: break;
          }

        } catch(e) { alt.logError(e.message, e.stack); };

      });

      this.menu.on('change', async (data, item) => {

        switch(item.name) {
  
          case 'gender' : {
            
            alt.log(item.value, item.data.models[item.value]);

            const model = new Model(item.data.models[item.value]);
  
            const success = await model.request();
  
            if(!success)
              throw new Error('Can\'t load model');
            
            const components     = $.game.player.ped.components.get(); // backup
            await $.requestp('hz:player:model:set', +model);

            $.game.player.ped.components.set(components); // restore
  
            break;
          }
  
          default: {

            if(item.data.component !== undefined) {

              const component = $.game.player.ped.components[item.data.component];
    
              if(item.name.endsWith('_drawable')) {
    
                component.drawable                                = item.value;
                $.game.player.ped.components[item.data.component] = component;
                const _data                                       = $.game.player.ped.components[item.data.component];
                const texturePath                                 = data.path.slice(0);
    
                texturePath[texturePath.length - 1]++;  // Texure is one slot after drawable
    
                const chash = natives.getHashNameForComponent(alt.Player.local.scriptID, item.data.component, _data.drawable, _data.texture);
                const num   = natives.getNumForcedComponents(chash);
    
                for(let i=0; i<num; i++) {
                  const [_, hash, draw, comp] = natives.getForcedComponent(chash, i);
                  natives.setPedComponentVariation(alt.Player.local.scriptID, comp, draw, 0, 0);
                }
    
                this.menu.update(data.path, {...item});
                this.menu.update(texturePath, {max: $.game.player.ped.components[item.data.component].textureCount - 1});

              } else if(item.name.endsWith('_texture')) {
    
                component.texture                                 = item.value;
                $.game.player.ped.components[item.data.component] = component;
              
              }
    
              return;
            }

            break;
          }
  
        }

      });

    } catch(e) { reject(e); };

  });

}

Skin.openHeadShapeMenu = function() {

  return new Promise(async (resolve, reject) => {
    
    try {

      const headBlendData = $.game.player.ped.headBlendData.get();

      const menu = MM.open('Visage', [
        {type: 'slider', name: 'father',      label: 'Père',  options: this.names.fathers,    value: this.fathers.indexOf(headBlendData.shapeFirst),  data: {type: 'blenddata'}},
        {type: 'slider', name: 'mother',      label: 'Mère',  options: this.names.mothers,    value: this.mothers.indexOf(headBlendData.shapeSecond), data: {type: 'blenddata'}},
        {type: 'slider', name: 'factor',      label: 'Extra', options: this.names.factors,    value: this.factors.indexOf(headBlendData.shapeThird),  data: {type: 'blenddata'}},
        {type: 'slider', name: 'face_mix',    label: 'Mix visage', min: 0, max: 100, step: 5, value: headBlendData.shapeMix,   data: {type: 'blenddata'}},
        {type: 'slider', name: 'skin_mix',    label: 'Mix peau',   min: 0, max: 100, step: 5, value: headBlendData.skinMix,    data: {type: 'blenddata'}},
        {type: 'slider', name: 'third_mix',   label: 'Mix extra',  min: 0, max: 100, step: 5, value: headBlendData.thirdMix,   data: {type: 'blenddata'}},
        {type: 'slider', name: 'father_skin', label: 'Peau père',  min: 0, max: 45,           value: headBlendData.skinFirst,  data: {type: 'blenddata'}},
        {type: 'slider', name: 'mother_skin', label: 'Peau mère',  min: 0, max: 45,           value: headBlendData.skinSecond, data: {type: 'blenddata'}},
        {type: 'slider', name: 'factor_skin', label: 'Peau extra', min: 0, max: 45,           value: headBlendData.skinThird,  data: {type: 'blenddata'}},
        {name: 'submit', label: 'Valider'},
      ]);
  
      menu.on('destroy', () => {
        resolve();
      });

      menu.on('select', async (data, item) => {

        try {

          switch(item.name) {
        
            case 'submit' : {
              menu.destroy();
              break;
            }
  
            default: break;
          }

        } catch(e) { alt.logError(e.message, e.stack); };

      });

      menu.on('change', async (data, item) => {
      
        try {

          const ped = $.game.player.ped;

          const items = menu.by('name');

          if(item.data.type === 'blenddata') {

            ped.headBlendData.set({
              shapeFirst : this.fathers[items.father.value],
              shapeSecond: this.mothers[items.mother.value],
              shapeThird : this.factors[items.factor.value],
              shapeMix   : items.face_mix.value  / 100,
              skinMix    : items.skin_mix.value  / 100,
              thirdMix   : items.third_mix.value / 100,
              skinFirst  : items.father_skin.value,
              skinSecond : items.mother_skin.value,
              skinThird  : items.factor_skin.value,
            });

          }

        } catch(e) { alt.logError(e.message, e.stack); };

      });

    } catch(e) { reject(e); };

  });

}

Skin.openHeadDetailsMenu = function() {

  return new Promise(async (resolve, reject) => {

    try {

      const data = [

        [PED_OVERLAYS.BLEMISHES         , 'Imperfections'],
        [PED_OVERLAYS.BEARD             , 'Barbe'],
        [PED_OVERLAYS.EYEBROWS          , 'Sourcils'],
        [PED_OVERLAYS.AGE               , 'Rides'],
        [PED_OVERLAYS.MAKEUP            , 'Maquillage'],
        [PED_OVERLAYS.BLUSH             , 'Font de teint'],
        [PED_OVERLAYS.COMPLEXION        , 'Aspect'],
        [PED_OVERLAYS.LIPSTICK          , 'Rouge à lèvres'],
        [PED_OVERLAYS.MOLES_FRICKLES    , 'Taches de rousseur'],
        [PED_OVERLAYS.CHEST_HAIR        , 'Poils du torse'],
        [PED_OVERLAYS.BODY_BLEMISHES    , 'Imperfections du corps'],
        [PED_OVERLAYS.ADD_BODY_BLEMISHES, 'imperfections du corps +']

      ].map(e => ({label: e[1], name: e[0].toString(), value: e[0]})).concat({label: 'Valider', name: 'submit'});
      
      const menu = MM.open('Visage', data);
  
      menu.on('destroy', () => {
        resolve();
      });

      menu.on('select', async (data, item) => {

        try {

          switch(item.name) {
        
            case 'submit' : {
              menu.destroy();
              break;
            }

            default: {

              menu.close();
              await this.openHeadOverlayMenu(item.value);
              menu.open();

              break;
            }

          }

        } catch(e) { alt.logError(e.message, e.stack); };

      });

      menu.on('change', async (data, item) => {
      
        try {

          const items = menu.by('name');

          if(item.data.type === 'overlay_opacity') {

            const index = items[item.data.index].value;

            natives.setPedHeadOverlay(alt.Player.local.scriptID, item.data.id, index, item.value / 100);

          } else if(item.data.type === 'overlay_index') {

            const value = (item.value === -1) ? 255 : item.value;

            const opacity = items[item.data.opacity].value;
            natives.setPedHeadOverlay(alt.Player.local.scriptID, item.data.id, value, opacity / 100);
          
          } else if(item.data.type === 'overlay_color_type') {

            const overlayId = item.data.id;
            const color1    = items[item.data.colors[0]].value;
            const color2    = items[item.data.colors[1]].value;

            natives.setPedHeadOverlayColor(alt.Player.local.scriptID, overlayId, item.value, color1, color2)

          } else if(item.data.type === 'overlay_color_1') {

            const overlayId = items[item.data.colorType].value;
            const color1    = item.value;
            const color2    = items[item.data.color2].value;

            natives.setPedHeadOverlayColor(alt.Player.local.scriptID, overlayId, item.value, color1, color2)

          } else if(item.data.type === 'overlay_color_2') {

            const overlayId = items[item.data.colorType].value;
            const color1    = items[item.data.color1].value;;
            const color2    = item.value

            natives.setPedHeadOverlayColor(alt.Player.local.scriptID, overlayId, item.value, color1, color2)

          }

        } catch(e) { alt.logError(e.message, e.stack); };

      });

    } catch(e) { reject(e); };

  });

}

Skin.openFaceFeatureMenu = function() {

  return new Promise(async (resolve, reject) => {
    
    const ped      = $.game.player.ped;
    const features = ped.faceFeatures.get();

    try {

      let items = [{type: 'slider', name: 'eye_color', label: 'Couleur des yeux', min: 0, max: 31}];
      items     = items.concat(this.faceFeatures.data.map((e,i) => ({type: 'slider', name: e, label: this.faceFeatures.labels[i], value: features[i] * 10, min: -10, max: 10})));
      items     = items.concat([{name: 'submit', label: 'Valider'}]);

      const menu = MM.open('Caractéristiques', items);
  
      menu.on('destroy', () => {
        resolve();
      });

      menu.on('select', async (data, item) => {

        try {

          switch(item.name) {
        
            case 'submit' : {
              menu.destroy();
              break;
            }
  
            default: break;
          }

        } catch(e) { alt.logError(e.message, e.stack); };

      });

      menu.on('change', async (data, item) => {
      
        try {

          if(item.name === 'eye_color') {
            natives.setPedEyeColor(alt.Player.local.scriptID, item.value);
            return;
          }

          const items = menu.by('name');
          ped.faceFeatures[data.index - 1] = item.value / 10;

        } catch(e) { alt.logError(e.message, e.stack); };

      });

    } catch(e) { reject(e); };

  });

}

Skin.openHeadHairsMenu = function() {

  return new Promise(async (resolve, reject) => {
    
    try {

      const ped        = $.game.player.ped;
      const components = ped.components.get();

      const menu = MM.open('Cheveux', [
        {type : 'slider', name : 'HAIR_drawable',       label : 'Coupe de cheveux',   position: components[PED_COMPONENTS.HAIR].drawable, min: 0, max: components[PED_COMPONENTS.HAIR].drawableCount, data: {component: PED_COMPONENTS.HAIR}},
        {type : 'slider', name : 'HAIR_texture',        label : 'Variation de coupe', position: components[PED_COMPONENTS.HAIR].texture,  min: 0, max: components[PED_COMPONENTS.HAIR].textureCount,  data: {component: PED_COMPONENTS.HAIR}},
        {type : 'slider', name : 'HAIR_color',          label : 'Couleur',            min : 0, max : natives.getNumHairColors() - 1},
        {type : 'slider', name : 'HAIR_highlightColor', label : 'Reflets',            min : 0, max : natives.getNumHairColors() - 1},
        {name: 'submit', label: 'Valider'}
      ]);
  
      menu.on('destroy', () => {
        resolve();
      });

      menu.on('select', async (data, item) => {

        try {

          switch(item.name) {
        
            case 'submit' : {
              menu.destroy();
              break;
            }
  
            default: break;
          }

        } catch(e) { alt.logError(e.message, e.stack); };

      });

      menu.on('change', async (data, item) => {
      
        try {

          if(item.name.endsWith('_drawable')) {
    
            const component = $.game.player.ped.components[item.data.component];

            component.drawable                                = item.value;
            $.game.player.ped.components[item.data.component] = component;
          
          } else if(item.name.endsWith('_texture')) {

            const component = $.game.player.ped.components[item.data.component];

            component.texture                                 = item.value;
            $.game.player.ped.components[item.data.component] = component;
          
          } else {
            
            const items = menu.by('name');

            natives.setPedHairColor(+ped, items.HAIR_color.value, items.HAIR_highlightColor.value);

          }

        } catch(e) { alt.logError(e.message, e.stack); };

      });

    } catch(e) { reject(e); };

  });

}

Skin.openHeadOverlayMenu = function(overlay) {
  
  const names = {
    [PED_OVERLAYS.BLEMISHES]          : 'Imperfections',
    [PED_OVERLAYS.BEARD]              : 'Barbe',
    [PED_OVERLAYS.EYEBROWS]           : 'Sourcils',
    [PED_OVERLAYS.AGE]                : 'Rides',
    [PED_OVERLAYS.MAKEUP]             : 'Maquillage',
    [PED_OVERLAYS.BLUSH]              : 'Font de teint',
    [PED_OVERLAYS.COMPLEXION]         : 'Aspect',
    [PED_OVERLAYS.LIPSTICK]           : 'Rouge à lèvres',
    [PED_OVERLAYS.MOLES_FRICKLES]     : 'Taches de rousseur',
    [PED_OVERLAYS.CHEST_HAIR]         : 'Poils du torse',
    [PED_OVERLAYS.BODY_BLEMISHES]     : 'Imperfections du corps',
    [PED_OVERLAYS.ADD_BODY_BLEMISHES] : 'imperfections du corps +',
  };

  const name = names[overlay];

  return new Promise(async (resolve, reject) => {
    
    try {

      const ped      = $.game.player.ped;
      const overlays = ped.overlays.get();

      const menu = MM.open(name, [
        {type: 'slider', name : 'overlay_index',   label : 'Type',       value: overlays[overlay].index === 255 ? -1 : overlays[overlay].index, min: -1, max: overlays[overlay].count, data: {overlay: overlay}},
        {type: 'slider', name : 'overlay_opacity', label : 'Opacité',    value: Math.floor(overlays[overlay].opacity * 10), min: 0, max: 10, data: {overlay: overlay}},
        {type: 'slider', name : 'overlay_color1',  label : 'Couleur 1',  value: overlays[overlay].color1, min : 0, max : natives.getNumHairColors() - 1, data: {overlay: overlay}},
        {type: 'slider', name : 'overlay_color2',  label : 'Couleur 2',  value: overlays[overlay].color2, min : 0, max : natives.getNumHairColors() - 1, data: {overlay: overlay}},
        {name: 'submit', label: 'Valider'}
      ]);
  
      menu.on('destroy', () => {
        resolve();
      });

      menu.on('select', async (data, item) => {

        try {

          switch(item.name) {
        
            case 'submit' : {
              menu.destroy();
              break;
            }
  
            default: break;
          }

        } catch(e) { alt.logError(e.message, e.stack); };

      });

      menu.on('change', async (data, item) => {
      
        try {

          if(item.name.endsWith('_index')) {
    
            const overlay = $.game.player.ped.overlays[item.data.overlay];

            overlay.index                                 = item.value === -1 ? 255 : item.value;
            $.game.player.ped.overlays[item.data.overlay] = overlay;
          
          } else if(item.name.endsWith('_opacity')) {

            const overlay = $.game.player.ped.overlays[item.data.overlay];

            overlay.opacity                               = item.value / 10;
            $.game.player.ped.overlays[item.data.overlay] = overlay;
          
          } else {
            
            const items   = menu.by('name');
            const overlay = $.game.player.ped.overlays[item.data.overlay];
            
            overlay.color1 = items.overlay_color1.value;
            overlay.color2 = items.overlay_color2.value;

            $.game.player.ped.overlays[item.data.overlay] = overlay;

          }

        } catch(e) { alt.logError(e.message, e.stack); };

      });

    } catch(e) { reject(e); };
  
  });

}

Skin.openHeadAccessoriesMenu = function() {

  return new Promise(async (resolve, reject) => {
    
    try {

      const ped   = $.game.player.ped;
      const props = ped.props.get();

      const data = [
        {type: 'slider', label: 'Couvre-chef',             name: 'HEAD_drawable',           value: ped.props[PED_PROPS.HEAD].index,   min: -1, max: natives.getNumberOfPedPropDrawableVariations(+ped, PED_PROPS.HEAD          ) - 1, data: {prop: PED_PROPS.HEAD          }},
        {type: 'slider', label: 'Couvre-chef variante',    name: 'HEAD_texture',            value: ped.props[PED_PROPS.HEAD].texture, min:  0, max: natives.getNumberOfPedPropTextureVariations(+ped,  PED_PROPS.HEAD          ) - 1, data: {prop: PED_PROPS.HEAD          }},
        {type: 'slider', label: 'Lunettes',                name: 'EYES_drawable',           value: ped.props[PED_PROPS.EYES].index,   min: -1, max: natives.getNumberOfPedPropDrawableVariations(+ped, PED_PROPS.EYES          ) - 1, data: {prop: PED_PROPS.EYES          }},
        {type: 'slider', label: 'Lunettes variante',       name: 'EYES_texture',            value: ped.props[PED_PROPS.EYES].texture, min:  0, max: natives.getNumberOfPedPropTextureVariations(+ped,  PED_PROPS.EYES          ) - 1, data: {prop: PED_PROPS.EYES          }},
        {type: 'slider', label: 'Acc. Oreilles',           name: 'EARS_drawable',           value: ped.props[PED_PROPS.EARS].index,   min: -1, max: natives.getNumberOfPedPropDrawableVariations(+ped, PED_PROPS.EARS          ) - 1, data: {prop: PED_PROPS.EARS          }},
        {type: 'slider', label: 'Acc. Oreilles variante',  name: 'EARS_texture',            value: ped.props[PED_PROPS.EARS].texture, min:  0, max: natives.getNumberOfPedPropTextureVariations(+ped,  PED_PROPS.EARS          ) - 1, data: {prop: PED_PROPS.EARS          }},
        {label: 'Valider', name: 'submit'}
      ];

      const menu = MM.open('Accessoires', data);
  
      menu.on('destroy', () => {
        resolve();
      });

      menu.on('select', async (data, item) => {

        try {

          switch(item.name) {
        
            case 'submit' : {
              menu.destroy();
              break;
            }
  
            default: break;
          }

        } catch(e) { alt.logError(e.message, e.stack); };

      });

      menu.on('change', async (data, item) => {
      
        try {

          const items = menu.by('name');

          if(item.name.endsWith('_drawable')) {

            if(item.value === -1)
              natives.clearPedProp(+ped, item.data.prop);
            else
              natives.setPedPropIndex(+ped, item.data.prop, items[item.name].value, items[item.value, item.name.replace('_drawable', '_texture')].value, false);

            return;
          } else if(item.name.endsWith('_texture')) {

            const drawable = items[item.name.replace('_texture', '_drawable')];

            if(drawable.value === -1)
              natives.clearPedProp(+ped, drawable.data.prop);
            else
              natives.setPedPropIndex(+ped, item.data.prop, drawable.value, item.value, false);

          }

        } catch(e) { alt.logError(e.message, e.stack); };

      });

    } catch(e) { reject(e); };

  });

}

Skin.openClotheShopMenu = function() {

  return new Promise(async (resolve, reject) => {
    
    try {

      WorldSelect.locked = true;
      const ped          = $.game.player.ped;
      const components   = ped.components.get();
      const overlays     = ped.overlays.get();
      const position     = ped.position;

      const interior       = natives.getInteriorFromEntity(alt.Player.local.scriptID);
      const [_, pos, ikey] = natives.getInteriorInfo(interior);

      const offseta         = natives.getOffsetFromInteriorInWorldCoords(interior, 0, 0, 0);
      const offsetb         = natives.getOffsetFromInteriorInWorldCoords(interior, 1, 0, 0);
      const oa              = new Vector3(offseta.x, offseta.y, offseta.z);
      const ob              = new Vector3(offsetb.x, offsetb.y, offsetb.z);
      const direction       = ob.clone().sub(oa);
      const interiorHeading = Math.atan2(direction.y, direction.x) * 180 / Math.PI;

      switch(ikey) {

        case 225974099 : { // suburban
          ped.heading  = interiorHeading + 180.0;
          ped.position = natives.getOffsetFromInteriorInWorldCoords(interior, 3.465099, 8.658311, 1.0);
          break;
        }

        case 1753457757 : { // binco
          ped.heading  = interiorHeading + 180.0;
          ped.position = natives.getOffsetFromInteriorInWorldCoords(interior, -2.406127, 3.388417, 1.0);
          break;
        }

        case -298980389 : {
          ped.heading  = interiorHeading;
          ped.position = natives.getOffsetFromInteriorInWorldCoords(interior, 4.017949,3.520693, 1.5);
          break;
        }

        default: break;

      }

      const camDirection   = $.game.player.ped.direction.scale(2.5);
      const camPosition    = $.game.player.ped.position.clone().add(camDirection);
      this.camera.position = camPosition;

      this.camera.point($.game.player.ped);
      this.camera.render(true);

      this.menu = MM.open('Vêtements', [
        {name: 'body_up', label: 'Haut du corps', children: [
          {type: 'slider', name: 'body_up.ACCS_drawable',  label: 'T-Shirt',             value: components[PED_COMPONENTS.ACCS].drawable, min: 0, max: components[PED_COMPONENTS.ACCS].drawableCount - 1, data: {component: PED_COMPONENTS.ACCS}},
          {type: 'slider', name: 'body_up.ACCS_texture',   label: 'Coloris t-shirt',     value: components[PED_COMPONENTS.ACCS].texture,  min: 0, max: components[PED_COMPONENTS.ACCS].textureCount  - 1, data: {component: PED_COMPONENTS.ACCS}},
          {type: 'slider', name: 'body_up.JBIB_drawable',  label: 'Torse',               value: components[PED_COMPONENTS.JBIB].drawable, min: 0, max: components[PED_COMPONENTS.JBIB].drawableCount - 1, data: {component: PED_COMPONENTS.JBIB}},
          {type: 'slider', name: 'body_up.JBIB_texture',   label: 'Coloris torse',       value: components[PED_COMPONENTS.JBIB].texture,  min: 0, max: components[PED_COMPONENTS.JBIB].textureCount  - 1, data: {component: PED_COMPONENTS.JBIB}},
          {type: 'slider', name: 'body_up.UPPR_drawable',  label: 'Bras',                value: components[PED_COMPONENTS.UPPR].drawable, min: 0, max: components[PED_COMPONENTS.UPPR].drawableCount - 1, data: {component: PED_COMPONENTS.UPPR}},     
          {type: 'slider', name: 'body_up.UPPR_texture',   label: 'Coloris bras',        value: components[PED_COMPONENTS.UPPR].texture,  min: 0, max: components[PED_COMPONENTS.UPPR].textureCount  - 1, data: {component: PED_COMPONENTS.UPPR}},    
          {type: 'slider', name: 'body_up.TEEF_drawable',  label: 'Chaine',              value: components[PED_COMPONENTS.TEEF].drawable, min: 0, max: components[PED_COMPONENTS.TEEF].drawableCount - 1, data: {component: PED_COMPONENTS.TEEF}},   
          {type: 'slider', name: 'body_up.TEEF_texture',   label: 'Texture',             value: components[PED_COMPONENTS.TEEF].texture,  min: 0, max: components[PED_COMPONENTS.TEEF].textureCount  - 1, data: {component: PED_COMPONENTS.TEEF}},
          // {type: 'slider', name: 'body_up.DECL_drawable',  label: 'Calques',             value: components[PED_COMPONENTS.DECL].drawable, min: 0, max: components[PED_COMPONENTS.DECL].drawableCount - 1, data: {component: PED_COMPONENTS.DECL}},     
          // {type: 'slider', name: 'body_up.DECL_texture',   label: 'Coloris calques',     value: components[PED_COMPONENTS.DECL].texture,  min: 0, max: components[PED_COMPONENTS.DECL].textureCount  - 1, data: {component: PED_COMPONENTS.DECL}},
          {type: 'slider', name: 'body_up.HAND_drawable',  label: 'Sac',                 value: components[PED_COMPONENTS.HAND].drawable, min: 0, max: components[PED_COMPONENTS.HAND].drawableCount - 1, data: {component: PED_COMPONENTS.HAND}},
          {type: 'slider', name: 'body_up.HAND_texture',   label: 'Coloris sac',         value: components[PED_COMPONENTS.HAND].texture,  min: 0, max: components[PED_COMPONENTS.HAND].textureCount  - 1, data: {component: PED_COMPONENTS.HAND}},
          {type: 'slider', name: 'body_up.TASK_drawable',  label: 'Pare-balles',         value: components[PED_COMPONENTS.TASK].drawable, min: 0, max: components[PED_COMPONENTS.TASK].drawableCount - 1, data: {component: PED_COMPONENTS.TASK}},
          // {type: 'slider', name: 'body_up.TASK_texture',   label: 'Coloris pare-balles', value: components[PED_COMPONENTS.TASK].texture,  min: 0, max: components[PED_COMPONENTS.TASK].textureCount  - 1, data: {component: PED_COMPONENTS.TASK}},
        ]},
        {name: 'body_down', label: 'Bas du corps', children: [
          {type: 'slider', name: 'body_down.LOWR_drawable', label: 'Pantalon',           value: components[PED_COMPONENTS.LOWR].drawable, min: 0, max: components[PED_COMPONENTS.LOWR].drawableCount - 1, data: {component: PED_COMPONENTS.LOWR}},
          {type: 'slider', name: 'body_down.LOWR_texture',  label: 'Coloris pantalon',   value: components[PED_COMPONENTS.LOWR].texture,  min: 0, max: components[PED_COMPONENTS.LOWR].textureCount  - 1, data: {component: PED_COMPONENTS.LOWR}},
          {type: 'slider', name: 'body_down.FEET_drawable', label: 'Chaussures',         value: components[PED_COMPONENTS.FEET].drawable, min: 0, max: components[PED_COMPONENTS.FEET].drawableCount - 1, data: {component: PED_COMPONENTS.FEET}},
          {type: 'slider', name: 'body_down.FEET_texture',  label: 'Coloris chaussures', value: components[PED_COMPONENTS.FEET].texture,  min: 0, max: components[PED_COMPONENTS.FEET].textureCount  - 1, data: {component: PED_COMPONENTS.FEET}},
        ]},
        {name: 'cancel', label: 'Annuler'},
        {name: 'submit', label: 'Valider'},
      ]);
  
      this.menu.on('destroy', () => {
        WorldSelect.locked = false;
        resolve();
      });
  
      this.menu.on('load', () => {
        HUD.enable(false);
      });
  
      this.menu.on('select', async (data, item) => {

        try {

          switch(item.name) {
          
            case 'body_up' : {
  
              const direction      = $.game.player.ped.direction.scale(1.2);
              let   position       = $.game.player.ped.position.clone().add(direction);
              const coords         = natives.getPedBoneCoords(alt.Player.local.scriptID, PED_BONES.SKEL_Spine3, 0.0, 0.0, 0.0);
              const camPosition    = this.camera.position;
              camPosition.x        = position.x;
              camPosition.y        = position.y;
              camPosition.z        = coords.z;
              this.camera.position = camPosition;
  
              natives.pointCamAtPedBone(+this.camera, alt.Player.local.scriptID, PED_BONES.SKEL_Spine3, 0.0, 0.0, 0.0, true);
              
              break;
            }
  
            case 'body_down' : {

              const direction      = $.game.player.ped.direction.scale(1.5);
              let   position       = $.game.player.ped.position.clone().add(direction);
              const coords         = natives.getPedBoneCoords(alt.Player.local.scriptID, PED_BONES.MH_L_Knee, 0.0, 0.0, 0.0);
              const camPosition    = this.camera.position;
              camPosition.x        = position.x;
              camPosition.y        = position.y;
              camPosition.z        = coords.z + 0.8;
              this.camera.position = camPosition;
  
              natives.pointCamAtPedBone(+this.camera, alt.Player.local.scriptID, PED_BONES.SKEL_L_Foot, 0.0, 0.0, 0.0, true);
              
              break;
            }

            case 'cancel' : {

              ped.components.set(components);
              ped.overlays.set(overlays);

              this.menu.destroy();

              $.game.player.ped.position = position;

              break;
            }
  
            case 'submit' : {

              const success = await $.requestp('hz:skin:pay', 250);
            
              if(!success) {
                ped.components.set(components);
                ped.overlays.set(overlays); 
              }

              this.menu.destroy();
              
              $.game.player.ped.position = position;

              break;
            }
  
            default: break;
          }

        } catch(e) { alt.logError(e.message, e.stack); };

      });

      this.menu.on('change', async (data, item) => {
  
        if(item.data.component !== undefined) {

          const component = $.game.player.ped.components[item.data.component];

          if(item.name.endsWith('_drawable')) {

            component.drawable                                = item.value;
            $.game.player.ped.components[item.data.component] = component;
            const _data                                       = $.game.player.ped.components[item.data.component];
            const texturePath                                 = data.path.slice(0);

            texturePath[texturePath.length - 1]++;  // Texure is one slot after drawable

            const chash = natives.getHashNameForComponent(alt.Player.local.scriptID, item.data.component, _data.drawable, _data.texture);
            const num   = natives.getNumForcedComponents(chash);

            for(let i=0; i<num; i++) {
              const [_, hash, draw, comp] = natives.getForcedComponent(chash, i);
              natives.setPedComponentVariation(alt.Player.local.scriptID, comp, draw, 0, 0);
            }

            this.menu.update(data.path, {...item});
            this.menu.update(texturePath, {max: $.game.player.ped.components[item.data.component].textureCount - 1});

          } else if(item.name.endsWith('_texture')) {

            component.texture                                 = item.value;
            $.game.player.ped.components[item.data.component] = component;
          
          }

          return;
        }

      });

    } catch(e) { reject(e); };

  });

}

Skin.openBarberShopMenu = function() {
  
  return new Promise(async (resolve, reject) => {
    
    try {

      WorldSelect.locked = true;
      const ped          = $.game.player.ped;
      const components   = ped.components.get();
      const overlays     = ped.overlays.get();

      this.menu = MM.open('Coiffeur / Barbier', [
        {name: 'hair',   label: 'Cheveux'},
        {name: 'beard',  label: 'Barbe'},
        {name: 'cancel', label: 'Annuler'},
        {name: 'submit', label: 'Valider'}
      ]);
  
      this.menu.on('destroy', () => {
        WorldSelect.locked = false;
        resolve();
      });
  
      this.menu.on('load', () => {
        HUD.enable(false);
      });
  
      this.menu.on('select', async (data, item) => {
        
        switch(item.name) {

          case 'hair' : {
            await this.openHeadHairsMenu();
            break;
          }            component.drawable                                = item.value;
          $.game.player.ped.components[item.data.component] = component;
          const _data                                       = $.game.player.ped.components[item.data.component];
          const texturePath                                 = data.path.slice(0);

          texturePath[texturePath.length - 1]++;  // Texure is one slot after drawable

          const chash = natives.getHashNameForComponent(alt.Player.local.scriptID, item.data.component, _data.drawable, _data.texture);
          const num   = natives.getNumForcedComponents(chash);

          for(let i=0; i<num; i++) {
            const [_, hash, draw, comp] = natives.getForcedComponent(chash, i);
            natives.setPedComponentVariation(alt.Player.local.scriptID, comp, draw, 0, 0);
          }

          this.menu.update(data.path, {...item});
          this.menu.update(texturePath, {max: component.textureCount - 1});

          case 'beard' : {
            await this.openHeadOverlayMenu(PED_OVERLAYS.BEARD);
            break;
          }

          case 'cancel' : {
            
            ped.components.set(components);
            ped.overlays.set(overlays);

            this.menu.destroy();

            break;
          }

          case 'submit' : {

            const success = await $.requestp('hz:skin:pay', 100);
            
            if(!success) {
              ped.components.set(components);
              ped.overlays.set(overlays); 
            }

            this.menu.destroy();
            
            break;
          }

          default: break;
        }

      });

    } catch(e) { reject(e); };

  });  
  
}

Skin.init = function() {

  return new Promise((resolve, reject) => {

    $.game.on('hz:interactable:interact:openclotheshop', async (interactable, {ws, wsa}, delegateServer) => {
      
      $.game.enableMouseInput(false);
      WorldSelect.lock(true);
    
      if(this.camera !== null)
        this.camera.destroy();
    
      $.game.player.ped.freeze(true);
    
      const direction = $.game.player.ped.direction.scale(2.5);
      const position  = $.game.player.ped.position.clone().add(direction);
      this.camera     = Camera.create(position);
    
      this.camera.point($.game.player.ped);
      this.camera.render();
    
      await nextTick();
    
      await Skin.openClotheShopMenu();

      alt.emitServer('hz:identity:update:model', {
        hash         : +$.game.player.model,
        headBlendData: $.game.player.ped.headBlendData.get(),
        faceFeatures : $.game.player.ped.faceFeatures.get(),
        components   : $.game.player.ped.components.get(),
        props        : $.game.player.ped.props.get(),
        overlays     : $.game.player.ped.overlays.get(),
      });
    
      $.game.enableMouseInput(true);
      WorldSelect.lock(false);
    
      this.camera.render(false);
      this.camera.destroy();
    
      $.game.player.ped.freeze(false);
    
      this.camera = null;
      this.menu   = null;
    
      HUD.disable(true);

    });

    $.game.on('hz:interactable:interact:openbarbershop', async (interactable, {ws, wsa}, delegateServer) => {
      
      $.game.enableMouseInput(false);
      HUD.preventToggle = true;
      WorldSelect.lock(true);
    
      if(this.camera !== null)
        this.camera.destroy();
    
      const center = natives.getOffsetFromEntityInWorldCoords(+ws.target, 0.0, 0.0, 0.0);
      const camPos = natives.getOffsetFromEntityInWorldCoords(+ws.target, -0.75, -0.75, 1.5);
      const behind = natives.getOffsetFromEntityInWorldCoords(+ws.target, 0.0, 1.5, 0.0);

      if(
        (+ws.target.model === $.utils.joaat('v_serv_bs_barbchair2')) ||
        (+ws.target.model === $.utils.joaat('v_serv_bs_barbchair3')) ||
        (+ws.target.model === $.utils.joaat('v_serv_bs_barbchair5'))
      ) {

        natives.taskStartScenarioAtPosition(
          alt.Player.local.scriptID,
          'PROP_HUMAN_SEAT_BENCH',
          center.x,
          center.y,
          center.z + 0.75,
          ws.target.heading + 180.0,
          0,
          true,
          true
        );

      } else {

        const results = [
          natives.getGroundZFor3dCoord(center.x, center.y, center.z - 0.2),
          natives.getGroundZFor3dCoord(center.x, center.y, center.z - 0.1),
          natives.getGroundZFor3dCoord(center.x, center.y, center.z + 0.0),
          natives.getGroundZFor3dCoord(center.x, center.y, center.z + 0.1),
          natives.getGroundZFor3dCoord(center.x, center.y, center.z + 0.2)
        ];
  
        const groundZs = results
          .filter(e => e[0] && e[1] !== 0)
          .map(e => e[1])
          .sort((a,b) => Math.abs(center.z - a) - Math.abs(center.z - b));
  
        if (groundZs.length > 0) {
          natives.taskStartScenarioAtPosition(
            alt.Player.local.scriptID,
            'PROP_HUMAN_SEAT_BENCH',
            center.x,
            center.y,
            groundZs[0] + 0.5,
            ws.target.heading + 180.0,
            0,
            true,
            true
          );
        }

      }

      this.camera = Camera.create(camPos);
      
      natives.pointCamAtPedBone(+this.camera, alt.Player.local.scriptID, PED_BONES.IK_Head, 0.0, 0.0, 0.0, true);
      
      this.camera.render();
    
      await nextTick();
    
      await Skin.openBarberShopMenu();

      natives.clearPedTasksImmediately(alt.Player.local.scriptID);

      $.game.player.ped.position = behind;

      alt.emitServer('hz:identity:update:model', {
        hash         : +$.game.player.model,
        headBlendData: $.game.player.ped.headBlendData.get(),
        faceFeatures : $.game.player.ped.faceFeatures.get(),
        components   : $.game.player.ped.components.get(),
        props        : $.game.player.ped.props.get(),
        overlays     : $.game.player.ped.overlays.get(),
      });
    
      $.game.enableMouseInput(true);
      HUD.preventToggle = false;
      WorldSelect.lock(false);
    
      this.camera.render(false);
      this.camera.destroy();
    
      this.camera = null;
      this.menu   = null;
    
      HUD.disable(true);

    });

    resolve();

  });

}

export default Skin;
