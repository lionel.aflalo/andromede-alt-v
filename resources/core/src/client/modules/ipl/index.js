import * as alt from 'alt'
import * as natives from 'natives';

const IPL = {};

IPL.__NAME = 'ipl';

IPL.init = function() {

  return new Promise((resolve, reject) => {

    natives.requestIpl('rc12b_hospitalinterior');
    natives.removeIpl('rc12b_default');

    natives.requestIpl('vw_casino_billboard');
    natives.requestIpl('vw_casino_billboard_lod(1)');
    natives.requestIpl('vw_casino_billboard_lod');
    natives.requestIpl('vw_casino_carpark');
    natives.requestIpl('vw_casino_garage');
    natives.requestIpl('vw_casino_main');
    natives.requestIpl('vw_casino_penthouse');
    natives.requestIpl('vw_ch3_additions');
    natives.requestIpl('vw_ch3_additions_long_0');
    natives.requestIpl('vw_ch3_additions_strm_0');
    natives.requestIpl('vw_dlc_casino_door');
    natives.requestIpl('vw_dlc_casino_door_lod');
    natives.requestIpl('vw_int_placement_vw');
    natives.requestIpl("vw_dlc_casino_apart");
    natives.requestIpl("hei_dlc_casino_door");
    natives.requestIpl("hei_dlc_windows_casino");
    
    const casinoMain = natives.getInteriorAtCoords(976.636, 70.295, 115.164);

    natives.enableInteriorProp(casinoMain, 'Set_Pent_Tint_Shell');
    natives.setInteriorPropColor(casinoMain, 'Set_Pent_Tint_Shell', 3);
    natives.enableInteriorProp(casinoMain, 'Set_Pent_Spa_Bar_Open');
    natives.enableInteriorProp(casinoMain, 'Set_Pent_Arcade_Retro');
    natives.enableInteriorProp(casinoMain, 'Set_Pent_bar_light_01');
    natives.enableInteriorProp(casinoMain, 'Set_Pent_bar_party_2');
    natives.enableInteriorProp(casinoMain, 'Set_Pent_Bar_Clutter');
    natives.enableInteriorProp(casinoMain, 'Set_Pent_Clutter_03');

    resolve();

  });

}

export default IPL;
