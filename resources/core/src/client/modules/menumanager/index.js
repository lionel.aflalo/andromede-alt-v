import EventEmitter from 'events';
import * as alt     from 'alt'
import * as natives from 'natives';
import $            from '../../../lib/altv-api/client/index';

import WM from '../windowmanager/index.js';


class Menu extends EventEmitter {

  constructor(label = 'Menu', items = {}, props = {}) {

    super();

    props.system = props.system === undefined ? true : props.system;

    this.label     = label;
    this.items     = items;
    this.props     = props;
    this.firstOpen = true;

    const queue = [{children: this.items}];

    while(queue.length > 0) {

      const children = queue.shift();

      for(let i=0; i<children.length; i++) {

        if(children[i].children !== undefined)
          queue.push(children[i].children);
        else
          children[i].data = children[i].data || {};

      }

    }
  }

  close() {
    this.win.close();
  }

  open() {

    if(this.firstOpen) {

      this.firstOpen = false;

      const classNames = ['transparent', 'notitle', 'fitcontent'];

      if(this.props.system)
        classNames.push('system');
  
      this.win = WM.open({
        url        : 'http://resource/dist/html/menu/index.html',
        closable   : false,
        stayinspace: false,
        classname  : classNames.join(' '),
        width      : this.props.width  === undefined ? 0  : this.props.width,
        height     : this.props.height === undefined ? 0  : this.props.height,
        x          : this.props.x      === undefined ? 10 : this.props.x,
        y          : this.props.y      === undefined ? 10 : this.props.y,
      });
  
      this.win.on('message', msg => {
        
        switch(msg.action) {
    
          case 'component_mounted' : {
            
            this.win.postMessage({
              action: 'open_menu',
              label : this.label,
              items : this.items
            });
  
            break;
          }
  
          case 'menu_select' : {
            
            const path = msg.data.path.slice(0);
            let   item = {children: this.items};
            
            while(path.length > 0)
              item = item.children[path.shift()];
              
            this.emit('select', msg.data, item);
  
            break;
          }
  
          case 'menu_change' : {
            
            const path = msg.data.path.slice(0);
            let   item = {children: this.items};
            
            while(path.length > 0)
              item = item.children[path.shift()];
            
            item.value = (msg.data.value === undefined) ? null : msg.data.value;
  
            this.emit('change', msg.data, item);
  
            break;
          }
    
          default: break;
    
        }
    
      });
  
      this.win.on('load',    () => this.emit('load'));
      this.win.on('close',   () => this.emit('close'));
      this.win.on('destroy', () => this.emit('destroy'));

      return;
    }

    this.win.open();

  }

  destroy() {
    if(this.win)
      this.win.destroy();
  }

  update(path, data) {
    this.win.postMessage({action: 'update_menu', path, data});
  }

  by(prop) {
    
    const items = {};

    for(let i=0; i<this.items.length; i++)
      items[this.items[i][prop].toString()] = this.items[i];

    return items;
  }

}

const MenuManager = {

};

MenuManager.__NAME = 'menu';

MenuManager.open = function(label = 'Menu', items = {}, props = {}) {
  const menu = new Menu(label, items, props);
  menu.open();
  return menu;
}

MenuManager.create = function(label = 'Menu', items = {}, props = {}) {
  return new Menu(label, items, props);
}

export default MenuManager;