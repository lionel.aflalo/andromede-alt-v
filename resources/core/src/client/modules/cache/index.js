import * as alt from 'alt'
import * as entities from '../../entities/index';
import $ from '../../../lib/altv-api/client/index';

const Cache = {
  data          : {},
  ensureTimeouts: {},
  renew         : [],
};

Cache.__NAME = 'cache';

Cache.wrap = function(data) {

  for(let k in entities) {

    const mod = entities[k];
    
    if(mod._type === data._type) {
      return new mod(data);
    }

  }

  // throw new Error(`No entity matching ${data._type}`);

}

Cache.ensure = function(datas) {

  if(!(datas instanceof Array))
    datas = [datas];

  for(let i=0; i<datas.length; i++) {

    const data = datas[i];

    if(datas[i]) {

      if(data._type && data._id) {
        this.data[data._id] = this.wrap(data);
      }

    }

  }
}


Cache.resolve = function(datas) {

  const isArray = datas instanceof Array;

  if(!isArray)
    datas = [datas];

  return new Promise(async (resolve, reject) => {

    try {

      const promises = datas.map(e => e ? (Cache.data[e._id] ? new Promise(resolve2 => resolve2(null)) : $.requestp('hz:entity:get', e._type, e._id)) : new Promise(resolve2 => resolve2(null)));
      const results  = await Promise.all(promises);

      for(let i=0; i<results.length; i++) {

        const data = results[i];

        if(data) {
          
          Cache.ensure(data);
          Cache.ensureTTL(data);
        }
      }

      const final = [];

      for(let i=0; i<datas.length; i++) {
        final[i] = datas[i] ? (Cache.data[datas[i]._id] ? Cache.data[datas[i]._id] : null) : null;
      }

      for(let i=0; i<final.length; i++) {

        if(!final[i])
          continue;

        const subdocs = final[i]._subdocs;

        for(let j=0; j<subdocs.length; j++) {

          const subdoc = subdocs[j];
          const entry  = final[i][subdoc.name];

          if(subdoc.isArray) {

            final[i][subdoc.name] = await Cache.resolve(entry.map(e => e ? {_type: e._type, _id: e._id} : null));

          } else {

            if(entry)
              final[i][subdoc.name] = await Cache.resolve({_type: entry._type, _id: entry._id});

          }
        }

      }

      resolve(isArray ? final : final[0]);

    } catch(e) { alt.logError(e.message, e.stack); }

  });

}

Cache.ensureTTL = function(data) {

  if(data.ttl === -1)
    return;

  const entity = this.data[data._id];

  if(this.ensureTimeouts[data._id]) {
    $.game.emit('hz:cache:expire', entity);
    alt.clearTimeout(this.ensureTimeouts[data._id]);
  }

  this.ensureTimeouts[data._id] = alt.setTimeout(() => {

  delete this.data[data._id];

  $.game.emit('hz:cache:expire', entity);

  }, data.ttl);
}

Cache.init = function() {

  return new Promise((resolve, reject) => {

    alt.onServer('hz:cache:update', (..._ids) => {

      for(let i=0; i<_ids.length; i++)
        if(this.renew.indexOf(_ids[i]) === -1)
          this.renew.push(_ids[i]);

    });

    alt.setInterval(() => {

      if(this.renew.length === 0)
        return;

      const renew = this.renew.slice(0);

      this.renew.length = 0;

      for(let i=0; i<renew.length; i++) {
        delete this.data[renew[i]];
      }

      $.game.emit('hz:cache:update:done', ...renew);

    }, 500);

    resolve();

  });

}

export default Cache;