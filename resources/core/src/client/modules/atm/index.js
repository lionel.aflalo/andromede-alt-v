import * as alt from 'alt'
import * as natives from 'natives';

import WM from '../windowmanager/index.js';
import MM from '../menumanager/index.js';
import Cache from '../cache/index';
import Notification from '../notification/index';
import { HZItemBankCard } from '../../entities/index';
import $ from '../../../lib/altv-api/client/index';

const SERVER = 'server.andromederp.com';
// const SERVER = '127.0.0.1';

const ATM = {
  win: null,
};

ATM.__NAME = 'atm';

ATM.open = async function() {

  Notification.create('ATM Soon');
  return;

  if(this.win !== null)
    this.win.destroy();

  const identity = await Cache.resolve({_type: 'Identity', _id: alt.Player.local.getSyncedMeta('identity')});
  const cards    = [];

  for(let i=0; i<identity.containers.length; i++) {

    const container = identity.containers[i];
    const items     = container.items;

    for(let i=0; i<items.length; i++) {

      const item = items[i];

      if(item instanceof HZItemBankCard)
        cards.push(item);
    }

  }

  const menuData = cards.map(e => ({label: e.number, number: e.number, action: 'select_card'})).concat([{label: 'Annuler', action: 'cancel'}]);
  const menu     = MM.open('Séléctionner une carte', menuData);

  menu.on('select', async (data, item) => {

    switch(item.action) {

      case 'cancel' : {
        menu.destroy();
        break;
      }

      default: {

        menu.destroy();

        natives.requestAnimDict('amb@prop_human_atm@male@idle_a');
        await $.utils.waitFor(() => natives.hasAnimDictLoaded('amb@prop_human_atm@male@idle_a'));
        natives.taskPlayAnim(alt.Player.local.scriptID, 'amb@prop_human_atm@male@idle_a', 'idle_a', 1.0, -1.0, -1, 1, 1, false, false, false);

        const [_, screenX, screenY] = natives.getActiveScreenResolution();

        this.win = WM.open({
          url        : 'http://resource/dist/html/atm/index.html',
          closable   : false,
          stayinspace: false,
          classname  : 'transparent system notitle',
          width      : screenX - 600,
          height     : screenY - 100,
          x          : 300,
          y          : 50,
        });
      
        this.win.on('destroy', () => {
          this.win = null;
          natives.clearPedTasks(alt.Player.local.scriptID);
        });
      
        this.win.on('message', async msg => {
      
          if(msg.action === 'component_mounted') {

            this.win.postMessage({
              action: 'load_config',
              config: {
                socket: 'http://' + SERVER + ':5000',
                locale: 'fr',
              }
            });
        
            this.win.postMessage({action: 'set_card', number: item.number});

            const identity = await Cache.resolve({type: 'Identity', _id: alt.Player.local.getSyncedMeta('identity')});

            let cash = 0;

            for(let i=0; i<identity.containers.length; i++) {

              const container = identity.containers[i];
              const amounts   = container.items.filter(e => e._type === 'ItemBanknote').map(e => e.count).filter(e => !isNaN(e));
              cash += amounts.reduce((a,b) => a + b, 0);

            }

            this.win.postMessage({action: 'set_cash', cash});
          }

          if(msg.action === 'notification') {
            Notification.create(msg.msg);
          }
      
        });

        break;
      }

    }

  });

}

ATM.init = function() {

  return new Promise((resolve, reject) => {

    $.game.on('hz:interactable:interact:openatm', (interactable, {ws, wsa}, delegateServer) => ATM.open());

    resolve();

  });

}

export default ATM;
