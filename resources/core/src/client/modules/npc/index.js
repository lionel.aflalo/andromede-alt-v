import EventEmitter from 'events';
import * as alt from 'alt'
import * as natives from 'natives';
import $ from '../../../lib/altv-api/client/index';

const { Vector3 }    = $.math;
const { Ped, Model } = $.types;

import Core  from '../../index.js';
import Cache from '../cache/index.js';
import MM    from '../menumanager/index.js';
import HUD   from '../hud/index.js';
import WS  from '../worldselect/index';
import WSA from '../worldselectactions/index';

const NPC = {
  coords : new Vector3(0, 0, 0),
  heading: 0.0,
  npcs   : [],
  peds   : {},
};

NPC.__NAME = 'npc';

NPC.create = async function(model, coords) {

  NPC.coords    = new Vector3(coords.x, coords.y, coords.z);
  const _model  = new Model(model);
  const success = await _model.request();

  if(success) {

    return Ped.create(_model, coords);

  } else {

    return null;

  }

}

NPC.init = function() {

  return new Promise((resolve, reject) => {

    $.game.on('hz:cube:change', async cube => {

      for(let _id in NPC.peds)
        NPC.peds[_id].delete();

      NPC.peds = {};

      const npcs = await $.requestp('hz:npc:get:cube', cube);
      NPC.npcs   = await Cache.resolve(npcs);

      for(let i=0; i<NPC.npcs.length; i++) {

        const npc = NPC.npcs[i];
        
        if(NPC.peds[npc._id] === undefined) {

          (npc => {

            NPC.peds[npc._id] = null;

            NPC.create(npc.model, {...npc.position, z: npc.position.z - 1.0}).then(ped => {

              NPC.peds[npc._id] = ped;
              ped.heading       = npc.heading;
    
              ped.components.setDefault();
    
              if((npc.animdict !== '') && (npc.animation !== '')) {
    
                natives.requestAnimDict(npc.animdict);
    
                $.utils.waitFor(() => natives.hasAnimDictLoaded(npc.animdict), 2500).then(() => {

                  if(natives.hasAnimDictLoaded(npc.animdict))
                    natives.taskPlayAnim(+ped, npc.animdict, npc.animation, 1.0, -1.0, -1, 1, 1, false, false, false);
                  else
                    natives.taskStandStill(+ped, -1);

                });
  
    
              } else natives.taskStandStill(+ped, -1);
    
              natives.freezeEntityPosition(+ped, true);
              natives.setEntityInvincible(+ped, true);
              natives.setBlockingOfNonTemporaryEvents(+ped, true);

            })
          ;

          })(npc);

        }

      }

    });

    resolve();

  });

}

export default NPC;