import * as alt from 'alt'
import * as natives from 'natives';

import WM from '../windowmanager/index.js';

const Notification = {
  win: null,
};

Notification.__NAME = 'notification';

Notification.create = function(html, timeout = 5000, id = undefined, before = false) {

  this.win.postMessage({
    action: 'notification',
    html,
    timeout,
    id,
    before
  });

}

Notification.init = function() {

  return new Promise((resolve, reject) => {

    const [_, screenX, screenY] = natives.getActiveScreenResolution();

    this.win = WM.open({
      url        : 'http://resource/dist/html/notification/index.html',
      closable   : false,
      stayinspace: false,
      classname  : 'transparent system notitle',
      width      : 300,
      height     : screenY - 50,
      x          : screenX - 300,
      y          : 0,
    });

    alt.onServer('hz:notification:create', Notification.create.bind(Notification));

    resolve();

  });

}

export default Notification;
