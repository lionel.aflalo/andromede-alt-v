import EventEmitter from 'events';
import * as alt from 'alt'
import * as natives from 'natives';
import $ from '../../../lib/altv-api/client/index';

import Core  from '../../index.js';
import Cache from '../cache/index.js';
import MM    from '../menumanager/index.js';
import HUD   from '../hud/index.js';

const Interactable = {
  interactables: [],
};

Interactable.create = function(model, position) {

  const menu = MM.open('Interactable', [
    {name: 'name'       , type:  'input'   , label: 'name_lowercase'       , value: ''}    ,
    {name: 'action'     , type:  'input'   , label: 'Action'               , value: ''}    ,
    {name: 'all'        , type:  'check'   , label: 'All'                  , value: false} ,
    {name: 'group'      , type:  'input'   , label: 'group1,group2,group3' , value: ''}    ,
    {name: 'customData' , type:  'input'   , label: '{"json":"data"}'      , value: '{}'}  ,
    {name: 'submit'     , label: 'Submit'} ,
  ]);
  
  menu.on('select', async (data, item) => {

    menu.destroy();

    const byName        = menu.by('name');
    const name          = byName.name.value;
    const action        = byName.action.value;
    const all           = byName.all.value;
    const group         = byName.group.value.split(',');
    const customDataStr = byName.customData.value;

    if(group.length === 1 && group[0] === '')
      group.length = 0;

    try {

      const customData = JSON.parse(customDataStr);

      alt.emitServer('hz:interactable:create',
        name,
        action,
        model,
        all ? null : {x: position.x, y: position.y, z: position.z},
        group,
        customData
      );

      await $.utils.delay(800);

      const interactables        = await $.requestp('hz:interactable:get:cube', Core.cube);
      Interactable.interactables = await Cache.resolve(interactables);

    } catch(e) { alt.logError(e.message, e.stack); }

  });

}

Interactable.edit = async function(model, position, _id) {

  try {

    const interactable = await Cache.resolve({_type: 'Interactable', _id});

    if(!interactable)
      return;

    const menu = MM.open('Interactable', [
      {name: 'name',       type:  'input', label: 'name_lowercase',       value: interactable.name},
      {name: 'action',     type:  'input', label: 'Action',               value: interactable.action},
      {name: 'all',        type:  'check', label: 'All',                  value: !interactable.position},
      {name: 'group',      type:  'input', label: 'group1,group2,group3', value: interactable.group.join(',')},
      {name: 'customData', type:  'input', label: '{"json":"data"}',      value: JSON.stringify(interactable.customData)},
      {name: 'submit',     label: 'Submit'},
    ]);
    
    menu.on('select', async (data, item) => {

      menu.destroy();

      const byName        = menu.by('name');
      const name          = byName.name.value;
      const action        = byName.action.value;
      const all           = byName.all.value;
      const group         = byName.group.value.split(',');
      const customDataStr = byName.customData.value;

      if(group.length === 1 && group[0] === '')
        group.length = 0;

      try {

        const customData = JSON.parse(customDataStr);

        alt.emitServer('hz:interactable:update',
          _id,
          name,
          action,
          model,
          all ? null : {x: position.x, y: position.y, z: position.z},
          group,
          customData
        );

        const interactables        = await $.requestp('hz:interactable:get:cube', Core.cube);
        Interactable.interactables = await Cache.resolve(interactables);

      } catch(e) { alt.logError(e.message, e.stack); }

    });

  } catch(e) { alt.logError(e.message, e.stack); }
}

Interactable.__NAME = 'interactable';

Interactable.init = function() {

  return new Promise((resolve, reject) => {
  
    $.game.on('hz:cube:change', async cube => {
      const interactables        = await $.requestp('hz:interactable:get:cube', cube);
      Interactable.interactables = await Cache.resolve(interactables);
    });

    const onContext = async (ws, wsa, mdata) => {

      const target = ws.target;

      mdata.data.push({label: 'Interactable', value: 'interactable'});

      $.game.once('ws:menu', async (ws, wsa, menu) => {
        
        wsa.preventDisable = true;

        const player = await Cache.resolve({_type: 'Player', _id: alt.Player.local.getSyncedMeta('player')});

        menu.on('select', (data, item) => {

          try {

            switch (item.value) {
            
              case 'interactable': {

                const mdata2 = [
                  {label: 'Interagir', value: 'interact'},
                ];

                let found = null;

                for(let i=0; i<Interactable.interactables.length; i++) {
    
                  const interactable = Interactable.interactables[i];

                  if(interactable.position === null) {
                    
                    const model = +ws.target.model;

                    if(model === interactable.model) {
                      found = interactable;
                      break;
                    }

                  } else {

                    const distance = $.utils.getDistance(ws.target.position, interactable.position);
  
                    if(distance <= 0.01) {
                      found = interactable;
                      break;
                    }  

                  }

                }

                if((player.roles.indexOf('admin') !== -1) || (player.roles.indexOf('mod') !== -1)) {
    
                  if(found)
                    mdata2.push({label: 'Edit', value: 'edit'});
                  else
                    mdata2.push({label: 'Create', value: 'create'});

                }
  

                const menu2 = MM.open('Interactable', mdata2);

                menu2.on('select', (data, item) => {
                  
                  menu2.destroy();

                  HUD.enable(true);

                  switch(item.value) {
                    
                    case 'interact' : {
                     
                      if(!found)
                        break;

                      $.game.emit('hz:interactable:interact:' + found.action, found, {ws, wsa}, () => {
                        alt.emitServer('hz:interactable:interact', found._id);
                      });

                      break;
                    }

                    case 'edit' : {
                      Interactable.edit(+target.model, {x: target.position.x, y: target.position.y, z: target.position.z}, found._id);
                      break;
                    }

                    case 'create' : {
                      Interactable.create(+target.model, {x: target.position.x, y: target.position.y, z: target.position.z});
                      break;
                    }

                    default: break;

                  }

                });

                break;
              }
            }

          } catch (e) { alt.logError(e.message, e.stack); }

        });
      });

    }

    $.game.on('ws:context:object', onContext);
    $.game.on('ws:context:ped',    onContext);

    resolve();

  });

}

export default Interactable;