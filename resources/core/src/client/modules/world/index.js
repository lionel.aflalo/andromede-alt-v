import * as alt from 'alt'
import * as natives from 'natives';

import $ from '../../../lib/altv-api/client/index';

import Cache from '../cache/index';

const { Entity, Model } = $.types;

const World = {
  loaded: {},
  cube  : {x: 0, y: 0, z: 0},
};

World.__NAME = 'world';

World.ensureProps = async function() {

  const itemInfos = await $.requestp('hz:item:get:cube', this.cube);
  const items     = await Cache.resolve(itemInfos);
  
  for(let _id in this.loaded) {

    const loaded = this.loaded[_id];
    const idx    = items.findIndex(e => e._id === _id);

    if(idx === -1) {

      loaded.entity.delete();
      delete this.loaded[_id];

    } else {

      const item = items[idx];

      if(item.model !== +loaded.entity.model) {
  
        loaded.entity.delete();
        delete this.loaded[_id];
      
      }

    }

  }

  for(let i=0; i<items.length; i++) {

    const item    = items[i];
    const loaded  = this.loaded[item._id];
    const success = natives.hasModelLoaded(item.model) || await new Model(item.model).request();

    if(success) {
      
      if(!loaded) {

        this.loaded[item._id] = {
          item  : item,
          entity: new Entity(natives.createObjectNoOffset(item.model, item.world.position.x, item.world.position.y, item.world.position.z - 1.0, false, false, true)),
        };
  
        natives.freezeEntityPosition(+this.loaded[item._id].entity, true);
        natives.setEntityCollision(+this.loaded[item._id].entity, false, false);

      }

    } else {

      this.loaded[item._id] = {
        item  : item,
        entity: null,
      };

    }

  }

}

World.init = function() {

  return new Promise((resolve, reject) => {

    $.game.on('hz:cube:change', async cube => {
      this.cube = cube;
      this.ensureProps();
    });

    $.game.on('hz:cache:expire', async entity => {
      // Not necessary ?
    });

    alt.onServer('hz:seed:doplantanim', async () => {

      natives.requestAnimDict('amb@world_human_gardener_plant@male@idle_a');
      await $.utils.waitFor(() => natives.hasAnimDictLoaded('amb@world_human_gardener_plant@male@idle_a'));
      natives.taskPlayAnim(alt.Player.local.scriptID, 'amb@world_human_gardener_plant@male@idle_a', 'idle_a', 1.0, -1.0, 8000, 1, 1, false, false, false);
      await $.utils.waitFor(() => natives.hasEntityAnimFinished(alt.Player.local.scriptID, 'amb@world_human_gardener_plant@male@idle_a', 'idle_a', 3));

    });

    alt.setInterval(() => this.ensureProps(), 10000);

    resolve();

  });

}

export default World;
