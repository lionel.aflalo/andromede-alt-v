import * as alt from 'alt'
import * as natives from 'natives';

import HUD          from '../hud/index';
import WS           from '../worldselect/index';
import Notification from '../notification/index';
import MM           from '../menumanager/index';

import $ from '../../../lib/altv-api/client/index';

const { Vector3 }                    = $.math;
const { Model, Entity, Camera, Ped } = $.types;

const ROULETTE_BALL           = $.utils.joaat('vw_prop_roulette_ball');
const ROULETTE_MARKER_1       = $.utils.joaat('vw_prop_vw_marker_01a');
const ROULETTE_MARKER_2       = $.utils.joaat('vw_prop_vw_marker_02a');
const ROULETTE_CHIP           = $.utils.joaat('vw_prop_chip_100dollar_x1');
const ROULETTE_SEATS_BONES    = ['Chair_Seat_01', 'Chair_Seat_03', 'Chair_Seat_02', 'Chair_Seat_04'];
const ROULETTE_SEATS_HEADINGS = [0, 90, 0, 180];
const DEALER_OFFSET           = new Vector3(0, 0.7, 0.0);
const DEALER_HEADING          = 180;

const EARNINGS = {
  simple  : 35,
  half    : 17,
  triple  : 11,
  quarter : 8,
  basket  : 6,
  trans   : 5,
  dozen   : 2,
  column  : 2,
  red     : 1,
  black   : 1,
  even    : 1,
  odd     : 1,
  _1to18  : 1,
  _19to36 : 1,
};

const EARNING_NAMES = {
  simple  : 'Simple',
  half    : 'Double',
  triple  : 'Triple',
  quarter : 'Carré',
  basket  : 'Panier',
  trans   : 'Transversale',
  dozen   : 'Douzaine',
  column  : 'Colonne',
  red     : 'Rouges',
  black   : 'Noirs',
  even    : 'Pair',
  odd     : 'Impair',
  _1to18  : 'Manque',
  _19to36 : 'Passe',
}

const CasinoRoulette = {
  
  tables: [
    {model: $.utils.joaat('vw_prop_casino_roulette_01'), position: new Vector3(1144.814, 268.2634, -52.8409), data: {}},
    {model: $.utils.joaat('vw_prop_casino_roulette_01'), position: new Vector3(1150.355, 262.7224, -52.8409), data: {}},
    {model: $.utils.joaat('vw_prop_casino_roulette_01'), position: new Vector3(1133.958, 262.1071, -52.0409), data: {}},
    {model: $.utils.joaat('vw_prop_casino_roulette_01'), position: new Vector3(1129.595, 267.2637, -52.0409), data: {}},
    {model: $.utils.joaat('vw_prop_casino_roulette_01'), position: new Vector3(1144.618, 252.2411, -52.0409), data: {}}, 
    {model: $.utils.joaat('vw_prop_casino_roulette_01'), position: new Vector3(1148.981, 247.0846, -52.0409), data: {}}, 
  ],

  markerOffsets: {
    '0'  : new Vector3(-0.137451171875   , -0.146942138671875 , 0.9449996948242188),
    '00' : new Vector3(-0.1387939453125 , 0.10546875         , 0.9449996948242188),
    '1'  : new Vector3(-0.0560302734375 , -0.1898193359375   , 0.9449996948242188),
    '2'  : new Vector3(-0.0567626953125 , -0.024017333984375 , 0.9449996948242188),
    '3'  : new Vector3(-0.056884765625  , 0.141632080078125  , 0.9449996948242188),
    '4'  : new Vector3(0.02392578125    , -0.187347412109375 , 0.9449996948242188),
    '5'  : new Vector3(0.0240478515625  , -0.02471923828125  , 0.9449996948242188),
    '6'  : new Vector3(0.02392578125    , 0.1422119140625    , 0.9449996948242188),
    '7'  : new Vector3(0.1038818359375  , -0.18902587890625  , 0.9449996948242188),
    '8'  : new Vector3(0.1044921875     , -0.023834228515625 , 0.9449996948242188),
    '9'  : new Vector3(0.10546875       , 0.1419677734375    , 0.9449996948242188),
    '10' : new Vector3(0.18701171875    , -0.188385009765625 , 0.9449996948242188),
    '11' : new Vector3(0.18603515625    , -0.0238037109375   , 0.9449996948242188),
    '12' : new Vector3(0.1851806640625  , 0.143157958984375  , 0.9449996948242188),
    '13' : new Vector3(0.2677001953125  , -0.18780517578125  , 0.9449996948242188),
    '14' : new Vector3(0.26806640625    , -0.02301025390625  , 0.9449996948242188),
    '15' : new Vector3(0.26611328125    , 0.143310546875     , 0.9449996948242188),
    '16' : new Vector3(0.3497314453125  , -0.18829345703125  , 0.9449996948242188),
    '17' : new Vector3(0.349609375      , -0.023101806640625 , 0.9449996948242188),
    '18' : new Vector3(0.3497314453125  , 0.142242431640625  , 0.9449996948242188),
    '19' : new Vector3(0.4307861328125  , -0.18829345703125  , 0.9449996948242188),
    '20' : new Vector3(0.4312744140625  , -0.02392578125     , 0.9449996948242188),
    '21' : new Vector3(0.431884765625   , 0.1416015625       , 0.9449996948242188),
    '22' : new Vector3(0.51220703125    , -0.188873291015625 , 0.9449996948242188),
    '23' : new Vector3(0.5123291015625  , -0.023773193359375 , 0.9449996948242188),
    '24' : new Vector3(0.511962890625   , 0.14215087890625   , 0.9449996948242188),
    '25' : new Vector3(0.5931396484375  , -0.18890380859375  , 0.9449996948242188),
    '26' : new Vector3(0.59375          , -0.023651123046875 , 0.9449996948242188),
    '27' : new Vector3(0.59375          , 0.14080810546875   , 0.9449996948242188),
    '28' : new Vector3(0.67529296875    , -0.189849853515625 , 0.9449996948242188),
    '29' : new Vector3(0.6751708984375  , -0.02337646484375  , 0.9449996948242188),
    '30' : new Vector3(0.674560546875   , 0.141845703125     , 0.9449996948242188),
    '31' : new Vector3(0.756591796875   , -0.18798828125     , 0.9449996948242188),
    '32' : new Vector3(0.7547607421875  , -0.0234375         , 0.9449996948242188),
    '33' : new Vector3(0.7554931640625  , 0.14263916015625   , 0.9449996948242188),
    '34' : new Vector3(0.836669921875   , -0.188323974609375 , 0.9449996948242188),
    '35' : new Vector3(0.8365478515625  , -0.0244140625      , 0.9449996948242188),
    '36' : new Vector3(0.8359375        , 0.14276123046875   , 0.9449996948242188),
  },

  chipOffsets: [
    {coords: new Vector3(-0.154541015625  , -0.150604248046875 , 0.9449996948242188) , type: 'simple', numbers: ['0']},
    {coords: new Vector3(-0.1561279296875 , 0.11505126953125   , 0.9449996948242188) , type: 'simple', numbers: ['00']},
    {coords: new Vector3(-0.059326171875  , -0.18701171875     , 0.9449996948242188) , type: 'simple', numbers: ['1']},
    {coords: new Vector3(-0.058349609375  , -0.019378662109375 , 0.9449996948242188) , type: 'simple', numbers: ['2']},
    {coords: new Vector3(-0.0587158203125 , 0.142059326171875  , 0.9449996948242188) , type: 'simple', numbers: ['3']},
    {coords: new Vector3(0.02294921875    , -0.1920166015625   , 0.9449996948242188) , type: 'simple', numbers: ['4']},
    {coords: new Vector3(0.023193359375   , -0.01947021484375  , 0.9449996948242188) , type: 'simple', numbers: ['5']},
    {coords: new Vector3(0.024658203125   , 0.147369384765625  , 0.9449996948242188) , type: 'simple', numbers: ['6']},
    {coords: new Vector3(0.105224609375   , -0.1876220703125   , 0.9449996948242188) , type: 'simple', numbers: ['7']},
    {coords: new Vector3(0.1055908203125  , -0.028472900390625 , 0.9449996948242188) , type: 'simple', numbers: ['8']},
    {coords: new Vector3(0.10400390625    , 0.147430419921875  , 0.9449996948242188) , type: 'simple', numbers: ['9']},
    {coords: new Vector3(0.187744140625   , -0.191802978515625 , 0.9449996948242188) , type: 'simple', numbers: ['10']},
    {coords: new Vector3(0.1866455078125  , -0.02667236328125  , 0.9449996948242188) , type: 'simple', numbers: ['11']},
    {coords: new Vector3(0.1842041015625  , 0.145965576171875  , 0.9449996948242188) , type: 'simple', numbers: ['12']},
    {coords: new Vector3(0.2696533203125  , -0.182464599609375 , 0.9449996948242188) , type: 'simple', numbers: ['13']},
    {coords: new Vector3(0.265869140625   , -0.027862548828125 , 0.9449996948242188) , type: 'simple', numbers: ['14']},
    {coords: new Vector3(0.2667236328125  , 0.138946533203125  , 0.9449996948242188) , type: 'simple', numbers: ['15']},
    {coords: new Vector3(0.35009765625    , -0.186126708984375 , 0.9449996948242188) , type: 'simple', numbers: ['16']},
    {coords: new Vector3(0.348876953125   , -0.027740478515625 , 0.9449996948242188) , type: 'simple', numbers: ['17']},
    {coords: new Vector3(0.3497314453125  , 0.14715576171875   , 0.9449996948242188) , type: 'simple', numbers: ['18']},
    {coords: new Vector3(0.43212890625    , -0.17864990234375  , 0.9449996948242188) , type: 'simple', numbers: ['19']},
    {coords: new Vector3(0.4337158203125  , -0.02508544921875  , 0.9449996948242188) , type: 'simple', numbers: ['20']},
    {coords: new Vector3(0.430419921875   , 0.138336181640625  , 0.9449996948242188) , type: 'simple', numbers: ['21']},
    {coords: new Vector3(0.51416015625    , -0.18603515625     , 0.9449996948242188) , type: 'simple', numbers: ['22']},
    {coords: new Vector3(0.5135498046875  , -0.02301025390625  , 0.9449996948242188) , type: 'simple', numbers: ['23']},
    {coords: new Vector3(0.5146484375     , 0.14239501953125   , 0.9449996948242188) , type: 'simple', numbers: ['24']},
    {coords: new Vector3(0.59130859375    , -0.192413330078125 , 0.9449996948242188) , type: 'simple', numbers: ['25']},
    {coords: new Vector3(0.596923828125   , -0.022216796875    , 0.9449996948242188) , type: 'simple', numbers: ['26']},
    {coords: new Vector3(0.5924072265625  , 0.14385986328125   , 0.9449996948242188) , type: 'simple', numbers: ['27']},
    {coords: new Vector3(0.6749267578125  , -0.187286376953125 , 0.9449996948242188) , type: 'simple', numbers: ['28']},
    {coords: new Vector3(0.67431640625    , -0.0262451171875   , 0.9449996948242188) , type: 'simple', numbers: ['29']},
    {coords: new Vector3(0.6756591796875  , 0.140594482421875  , 0.9449996948242188) , type: 'simple', numbers: ['30']},
    {coords: new Vector3(0.7542724609375  , -0.19415283203125  , 0.9449996948242188) , type: 'simple', numbers: ['31']},
    {coords: new Vector3(0.7542724609375  , -0.01898193359375  , 0.9449996948242188) , type: 'simple', numbers: ['32']},
    {coords: new Vector3(0.75439453125    , 0.1448974609375    , 0.9449996948242188) , type: 'simple', numbers: ['33']},
    {coords: new Vector3(0.8392333984375  , -0.18951416015625  , 0.9449996948242188) , type: 'simple', numbers: ['34']},
    {coords: new Vector3(0.837646484375   , -0.023468017578125 , 0.9449996948242188) , type: 'simple', numbers: ['35']},
    {coords: new Vector3(0.8380126953125  , 0.14227294921875   , 0.9449996948242188) , type: 'simple', numbers: ['36']},
    {coords: new Vector3(-0.1368408203125 , -0.02099609375     , 0.9449996948242188) , type: 'half', numbers: ['0'    ,'00']},
    {coords: new Vector3(-0.055419921875  , -0.105804443359375 , 0.9449996948242188) , type: 'half', numbers: ['1'    ,'2']},
    {coords: new Vector3(-0.0567626953125 , 0.058624267578125  , 0.9449996948242188) , type: 'half', numbers: ['2'    ,'3']},
    {coords: new Vector3(0.02587890625    , -0.10498046875     , 0.9449996948242188) , type: 'half', numbers: ['4'    ,'5']},
    {coords: new Vector3(0.0244140625     , 0.058837890625     , 0.9449996948242188) , type: 'half', numbers: ['5'    ,'6']},
    {coords: new Vector3(0.100341796875   , -0.10382080078125  , 0.9449996948242188) , type: 'half', numbers: ['7'    ,'8']},
    {coords: new Vector3(0.1064453125     , 0.06011962890625   , 0.9449996948242188) , type: 'half', numbers: ['8'    ,'9']},
    {coords: new Vector3(0.19189453125    , -0.1060791015625   , 0.9449996948242188) , type: 'half', numbers: ['10'   ,'11']},
    {coords: new Vector3(0.1856689453125  , 0.05438232421875   , 0.9449996948242188) , type: 'half', numbers: ['11'   ,'12']},
    {coords: new Vector3(0.27099609375    , -0.10870361328125  , 0.9449996948242188) , type: 'half', numbers: ['13'   ,'14']},
    {coords: new Vector3(0.2667236328125  , 0.058502197265625  , 0.9449996948242188) , type: 'half', numbers: ['14'   ,'15']},
    {coords: new Vector3(0.3463134765625  , -0.107696533203125 , 0.9449996948242188) , type: 'half', numbers: ['16'   ,'17']},
    {coords: new Vector3(0.34814453125    , 0.0556640625       , 0.9449996948242188) , type: 'half', numbers: ['17'   ,'18']},
    {coords: new Vector3(0.42822265625    , -0.109130859375    , 0.9449996948242188) , type: 'half', numbers: ['19'   ,'20']},
    {coords: new Vector3(0.4302978515625  , 0.0550537109375    , 0.9449996948242188) , type: 'half', numbers: ['20'   ,'21']},
    {coords: new Vector3(0.511474609375   , -0.107421875       , 0.9449996948242188) , type: 'half', numbers: ['22'   ,'23']},
    {coords: new Vector3(0.512451171875   , 0.0614013671875    , 0.9449996948242188) , type: 'half', numbers: ['23'   ,'24']},
    {coords: new Vector3(0.5980224609375  , -0.107147216796875 , 0.9449996948242188) , type: 'half', numbers: ['25'   ,'26']},
    {coords: new Vector3(0.596435546875   , 0.0574951171875    , 0.9449996948242188) , type: 'half', numbers: ['26'   ,'27']},
    {coords: new Vector3(0.673828125      , -0.106903076171875 , 0.9449996948242188) , type: 'half', numbers: ['28'   ,'29']},
    {coords: new Vector3(0.6751708984375  , 0.058685302734375  , 0.9449996948242188) , type: 'half', numbers: ['29'   ,'30']},
    {coords: new Vector3(0.7532958984375  , -0.1102294921875   , 0.9449996948242188) , type: 'half', numbers: ['31'   ,'32']},
    {coords: new Vector3(0.750244140625   , 0.06103515625      , 0.9449996948242188) , type: 'half', numbers: ['32'   ,'33']},
    {coords: new Vector3(0.834716796875   , -0.108978271484375 , 0.9449996948242188) , type: 'half', numbers: ['34'   ,'35']},
    {coords: new Vector3(0.836181640625   , 0.05828857421875   , 0.9449996948242188) , type: 'half', numbers: ['35'   ,'36']},
    {coords: new Vector3(-0.0167236328125 , -0.187042236328125 , 0.9449996948242188) , type: 'half', numbers: ['1'    ,'4']},
    {coords: new Vector3(-0.0167236328125 , -0.02154541015625  , 0.9449996948242188) , type: 'half', numbers: ['2'    ,'5']},
    {coords: new Vector3(-0.0164794921875 , 0.140350341796875  , 0.9449996948242188) , type: 'half', numbers: ['3'    ,'6']},
    {coords: new Vector3(0.064453125      , -0.1865234375      , 0.9449996948242188) , type: 'half', numbers: ['4'    ,'7']},
    {coords: new Vector3(0.06494140625    , -0.01727294921875  , 0.9449996948242188) , type: 'half', numbers: ['5'    ,'8']},
    {coords: new Vector3(0.068603515625   , 0.13873291015625   , 0.9449996948242188) , type: 'half', numbers: ['6'    ,'9']},
    {coords: new Vector3(0.144287109375   , -0.184173583984375 , 0.9449996948242188) , type: 'half', numbers: ['7'    ,'10']},
    {coords: new Vector3(0.14501953125    , -0.024139404296875 , 0.9449996948242188) , type: 'half', numbers: ['8'    ,'11']},
    {coords: new Vector3(0.14501953125    , 0.136993408203125  , 0.9449996948242188) , type: 'half', numbers: ['9'    ,'12']},
    {coords: new Vector3(0.2291259765625  , -0.18670654296875  , 0.9449996948242188) , type: 'half', numbers: ['10'   ,'13']},
    {coords: new Vector3(0.227783203125   , -0.0242919921875   , 0.9449996948242188) , type: 'half', numbers: ['11'   ,'14']},
    {coords: new Vector3(0.2286376953125  , 0.14398193359375   , 0.9449996948242188) , type: 'half', numbers: ['12'   ,'15']},
    {coords: new Vector3(0.308349609375   , -0.18792724609375  , 0.9449996948242188) , type: 'half', numbers: ['13'   ,'16']},
    {coords: new Vector3(0.308837890625   , -0.02374267578125  , 0.9449996948242188) , type: 'half', numbers: ['14'   ,'17']},
    {coords: new Vector3(0.3099365234375  , 0.14410400390625   , 0.9449996948242188) , type: 'half', numbers: ['15'   ,'18']},
    {coords: new Vector3(0.39111328125    , -0.192230224609375 , 0.9449996948242188) , type: 'half', numbers: ['16'   ,'19']},
    {coords: new Vector3(0.390869140625   , -0.0189208984375   , 0.9449996948242188) , type: 'half', numbers: ['17'   ,'20']},
    {coords: new Vector3(0.39111328125    , 0.146514892578125  , 0.9449996948242188) , type: 'half', numbers: ['18'   ,'21']},
    {coords: new Vector3(0.470947265625   , -0.188690185546875 , 0.9449996948242188) , type: 'half', numbers: ['19'   ,'22']},
    {coords: new Vector3(0.4705810546875  , -0.0205078125      , 0.9449996948242188) , type: 'half', numbers: ['20'   ,'23']},
    {coords: new Vector3(0.4725341796875  , 0.140167236328125  , 0.9449996948242188) , type: 'half', numbers: ['21'   ,'24']},
    {coords: new Vector3(0.5491943359375  , -0.189666748046875 , 0.9449996948242188) , type: 'half', numbers: ['22'   ,'25']},
    {coords: new Vector3(0.548095703125   , -0.022552490234375 , 0.9449996948242188) , type: 'half', numbers: ['23'   ,'26']},
    {coords: new Vector3(0.553955078125   , 0.1446533203125    , 0.9449996948242188) , type: 'half', numbers: ['24'   ,'27']},
    {coords: new Vector3(0.6324462890625  , -0.191131591796875 , 0.9449996948242188) , type: 'half', numbers: ['25'   ,'28']},
    {coords: new Vector3(0.635498046875   , -0.0224609375      , 0.9449996948242188) , type: 'half', numbers: ['26'   ,'29']},
    {coords: new Vector3(0.6392822265625  , 0.139190673828125  , 0.9449996948242188) , type: 'half', numbers: ['27'   ,'30']},
    {coords: new Vector3(0.71533203125    , -0.187042236328125 , 0.9449996948242188) , type: 'half', numbers: ['28'   ,'31']},
    {coords: new Vector3(0.7181396484375  , -0.02447509765625  , 0.9449996948242188) , type: 'half', numbers: ['29'   ,'32']},
    {coords: new Vector3(0.7152099609375  , 0.138153076171875  , 0.9449996948242188) , type: 'half', numbers: ['30'   ,'33']},
    {coords: new Vector3(0.7969970703125  , -0.1904296875      , 0.9449996948242188) , type: 'half', numbers: ['31'   ,'34']},
    {coords: new Vector3(0.7955322265625  , -0.024871826171875 , 0.9449996948242188) , type: 'half', numbers: ['32'   ,'35']},
    {coords: new Vector3(0.7960205078125  , 0.137664794921875  , 0.9449996948242188) , type: 'half', numbers: ['33'   ,'36']},
    {coords: new Vector3(-0.0560302734375 , -0.271240234375    , 0.9449996948242188) , type: 'triple', numbers: ['1'    ,'2'    ,'3']},
    {coords: new Vector3(0.024658203125   , -0.271392822265625 , 0.9449996948242188) , type: 'triple', numbers: ['4'    ,'5'    ,'6']},
    {coords: new Vector3(0.1051025390625  , -0.272125244140625 , 0.9449996948242188) , type: 'triple', numbers: ['7'    ,'8'    ,'9']},
    {coords: new Vector3(0.1898193359375  , -0.27001953125     , 0.9449996948242188) , type: 'triple', numbers: ['10'   ,'11'   ,'12']},
    {coords: new Vector3(0.2696533203125  , -0.271697998046875 , 0.9449996948242188) , type: 'triple', numbers: ['13'   ,'14'   ,'15']},
    {coords: new Vector3(0.351318359375   , -0.268096923828125 , 0.9449996948242188) , type: 'triple', numbers: ['16'   ,'17'   ,'18']},
    {coords: new Vector3(0.4287109375     , -0.269561767578125 , 0.9449996948242188) , type: 'triple', numbers: ['19'   ,'20'   ,'21']},
    {coords: new Vector3(0.5098876953125  , -0.2716064453125   , 0.9449996948242188) , type: 'triple', numbers: ['22'   ,'23'   ,'24']},
    {coords: new Vector3(0.5960693359375  , -0.271148681640625 , 0.9449996948242188) , type: 'triple', numbers: ['25'   ,'26'   ,'27']},
    {coords: new Vector3(0.67724609375    , -0.268524169921875 , 0.9449996948242188) , type: 'triple', numbers: ['28'   ,'29'   ,'30']},
    {coords: new Vector3(0.7523193359375  , -0.27227783203125  , 0.9449996948242188) , type: 'triple', numbers: ['31'   ,'32'   ,'33']},
    {coords: new Vector3(0.8382568359375  , -0.272125244140625 , 0.9449996948242188) , type: 'triple', numbers: ['34'   ,'35'   ,'36']},
    {coords: new Vector3(-0.017333984375  , -0.106170654296875 , 0.9449996948242188) , type: 'quarter', numbers: ['1'    ,'2'    ,'4'    ,'5']},
    {coords: new Vector3(-0.0162353515625 , 0.060882568359375  , 0.9449996948242188) , type: 'quarter', numbers: ['2'    ,'3'    ,'5'    ,'6']},
    {coords: new Vector3(0.06591796875    , -0.110107421875    , 0.9449996948242188) , type: 'quarter', numbers: ['4'    ,'5'    ,'7'    ,'8']},
    {coords: new Vector3(0.0653076171875  , 0.060028076171875  , 0.9449996948242188) , type: 'quarter', numbers: ['5'    ,'6'    ,'8'    ,'9']},
    {coords: new Vector3(0.146484375      , -0.10888671875     , 0.9449996948242188) , type: 'quarter', numbers: ['7'    ,'8'    ,'10'   ,'11']},
    {coords: new Vector3(0.1451416015625  , 0.057159423828125  , 0.9449996948242188) , type: 'quarter', numbers: ['8'    ,'9'    ,'11'   ,'12']},
    {coords: new Vector3(0.22705078125    , -0.1092529296875   , 0.9449996948242188) , type: 'quarter', numbers: ['10'   ,'11'   ,'13'   ,'14']},
    {coords: new Vector3(0.22802734375    , 0.059356689453125  , 0.9449996948242188) , type: 'quarter', numbers: ['11'   ,'12'   ,'14'   ,'15']},
    {coords: new Vector3(0.307373046875   , -0.1043701171875   , 0.9449996948242188) , type: 'quarter', numbers: ['13'   ,'14'   ,'16'   ,'17']},
    {coords: new Vector3(0.309814453125   , 0.05584716796875   , 0.9449996948242188) , type: 'quarter', numbers: ['14'   ,'15'   ,'17'   ,'18']},
    {coords: new Vector3(0.3919677734375  , -0.111083984375    , 0.9449996948242188) , type: 'quarter', numbers: ['16'   ,'17'   ,'19'   ,'20']},
    {coords: new Vector3(0.3924560546875  , 0.0596923828125    , 0.9449996948242188) , type: 'quarter', numbers: ['17'   ,'18'   ,'20'   ,'21']},
    {coords: new Vector3(0.471923828125   , -0.1044921875      , 0.9449996948242188) , type: 'quarter', numbers: ['19'   ,'20'   ,'22'   ,'23']},
    {coords: new Vector3(0.4698486328125  , 0.060028076171875  , 0.9449996948242188) , type: 'quarter', numbers: ['20'   ,'21'   ,'23'   ,'24']},
    {coords: new Vector3(0.5531005859375  , -0.106170654296875 , 0.9449996948242188) , type: 'quarter', numbers: ['22'   ,'23'   ,'25'   ,'26']},
    {coords: new Vector3(0.5546875        , 0.059417724609375  , 0.9449996948242188) , type: 'quarter', numbers: ['23'   ,'24'   ,'26'   ,'27']},
    {coords: new Vector3(0.633544921875   , -0.101531982421875 , 0.9449996948242188) , type: 'quarter', numbers: ['25'   ,'26'   ,'28'   ,'29']},
    {coords: new Vector3(0.6337890625     , 0.0579833984375    , 0.9449996948242188) , type: 'quarter', numbers: ['26'   ,'27'   ,'29'   ,'30']},
    {coords: new Vector3(0.7156982421875  , -0.106292724609375 , 0.9449996948242188) , type: 'quarter', numbers: ['28'   ,'29'   ,'31'   ,'32']},
    {coords: new Vector3(0.7158203125     , 0.0604248046875    , 0.9449996948242188) , type: 'quarter', numbers: ['29'   ,'30'   ,'32'   ,'33']},
    {coords: new Vector3(0.7947998046875  , -0.108642578125    , 0.9449996948242188) , type: 'quarter', numbers: ['31'   ,'32'   ,'34'   ,'35']},
    {coords: new Vector3(0.7952880859375  , 0.059051513671875  , 0.9449996948242188) , type: 'quarter', numbers: ['32'   ,'33'   ,'35'   ,'36']},
    {coords: new Vector3(-0.099609375     , -0.2711181640625   , 0.9449996948242188) , type: 'basket', numbers: ['0'    ,'00'   ,'1'    ,'2'    ,'3']},
    {coords: new Vector3(-0.0147705078125 , -0.27154541015625  , 0.9449996948242188) , type: 'trans', numbers: ['1'    ,'2'    ,'3'    ,'4'    ,'5'   ,'6']},
    {coords: new Vector3(0.064697265625   , -0.270263671875    , 0.9449996948242188) , type: 'trans', numbers: ['4'    ,'5'    ,'6'    ,'7'    ,'8'   ,'9']},
    {coords: new Vector3(0.144775390625   , -0.271209716796875 , 0.9449996948242188) , type: 'trans', numbers: ['7'    ,'8'    ,'9'    ,'10'   ,'11'  ,'12']},
    {coords: new Vector3(0.226806640625   , -0.27142333984375  , 0.9449996948242188) , type: 'trans', numbers: ['10'   ,'11'   ,'12'   ,'13'   ,'14'  ,'15']},
    {coords: new Vector3(0.306396484375   , -0.27142333984375  , 0.9449996948242188) , type: 'trans', numbers: ['13'   ,'14'   ,'15'   ,'16'   ,'17'  ,'18']},
    {coords: new Vector3(0.3895263671875  , -0.27099609375     , 0.9449996948242188) , type: 'trans', numbers: ['16'   ,'17'   ,'18'   ,'19'   ,'20'  ,'21']},
    {coords: new Vector3(0.468017578125   , -0.275238037109375 , 0.9449996948242188) , type: 'trans', numbers: ['19'   ,'20'   ,'21'   ,'22'   ,'23'  ,'24']},
    {coords: new Vector3(0.5509033203125  , -0.2738037109375   , 0.9449996948242188) , type: 'trans', numbers: ['22'   ,'23'   ,'24'   ,'25'   ,'26'  ,'27']},
    {coords: new Vector3(0.6336669921875  , -0.27386474609375  , 0.9449996948242188) , type: 'trans', numbers: ['25'   ,'26'   ,'27'   ,'28'   ,'29'  ,'30']},
    {coords: new Vector3(0.7144775390625  , -0.272186279296875 , 0.9449996948242188) , type: 'trans', numbers: ['28'   ,'29'   ,'30'   ,'31'   ,'32'  ,'33']},
    {coords: new Vector3(0.7935791015625  , -0.272918701171875 , 0.9449996948242188) , type: 'trans', numbers: ['31'   ,'32'   ,'33'   ,'34'   ,'35'  ,'36']},
    {coords: new Vector3(0.0643310546875  , -0.304718017578125 , 0.9449996948242188) , type: 'dozen', numbers: ['1'    ,'2'    ,'3'    ,'4'    ,'5'   ,'6'    ,'7'  ,'8'  ,'9'  ,'10' ,'11' ,'12']}, //1st12
    {coords: new Vector3(0.392822265625   , -0.304779052734375 , 0.9449996948242188) , type: 'dozen', numbers: ['13'   ,'14'   ,'15'   ,'16'   ,'17'  ,'18'   ,'19' ,'20' ,'21' ,'22' ,'23' ,'24']},//2nd12
    {coords: new Vector3(0.712158203125   , -0.30303955078125  , 0.9449996948242188) , type: 'dozen', numbers: ['25'   ,'26'   ,'27'   ,'28'   ,'29'  ,'30'   ,'31' ,'32' ,'33' ,'34' ,'35' ,'36']},//3rd12
    {coords: new Vector3(0.9222412109375  , -0.185882568359375 , 0.9449996948242188) , type: 'dozen', numbers: ['1'    ,'4'    ,'7'    ,'10'   ,'13'  ,'16'   ,'19' ,'22' ,'25' ,'28' ,'31' ,'34']},//2to1
    {coords: new Vector3(0.9229736328125  , -0.0181884765625   , 0.9449996948242188) , type: 'dozen', numbers: ['2'    ,'5'    ,'8'    ,'11'   ,'14'  ,'17'   ,'20' ,'23' ,'26' ,'29' ,'32' ,'35']},//2to1
    {coords: new Vector3(0.9248046875     , 0.14849853515625   , 0.9449996948242188) , type: 'dozen', numbers: ['3'    ,'6'    ,'9'    ,'12'   ,'15'  ,'18'   ,'21' ,'24' ,'27' ,'30' ,'33' ,'36']},//2to1
    {coords: new Vector3(-0.011474609375  , -0.378875732421875 , 0.9449996948242188) , type: '_1to18', numbers: ['1'    ,'2'    ,'3'    ,'4'    ,'5'   ,'6'    ,'7'  ,'8'  ,'9'  ,'10' ,'11' ,'12'   ,'13'     ,'14' ,'15' ,'16' ,'17' ,'18']},//1-18
    {coords: new Vector3(0.142822265625   , -0.375732421875    , 0.9449996948242188) , type: 'even', numbers: ['2'    ,'4'    ,'6'    ,'8'    ,'10'  ,'12'   ,'14' ,'16' ,'18' ,'20' ,'22' ,'24'   ,'26'     ,'28' ,'30' ,'32' ,'34' ,'36']}, //even
    {coords: new Vector3(0.308349609375   , -0.37542724609375  , 0.9449996948242188) , type: 'red', numbers: ['1'    ,'3'    ,'5'    ,'7'    ,'9'   ,'12'   ,'14' ,'16' ,'18' ,'19' ,'21' ,'23'   ,'25'     ,'27' ,'30' ,'32' ,'34' ,'36']},//red
    {coords: new Vector3(0.4713134765625  , -0.376861572265625 , 0.9449996948242188) , type: 'black', numbers: ['2'    ,'4'    ,'6'    ,'8'    ,'10'  ,'11'   ,'13' ,'15' ,'17' ,'20' ,'22' ,'24'   ,'26'     ,'28' ,'29' ,'31' ,'33' ,'35']},//black
    {coords: new Vector3(0.6341552734375  , -0.376495361328125 , 0.9449996948242188) , type: 'odd', numbers: ['1'    ,'3'    ,'5'    ,'7'    ,'9'   ,'11'   ,'13' ,'15' ,'17' ,'19' ,'21' ,'23'   ,'25'     ,'27' ,'29' ,'31' ,'33' ,'35']},//odd
    {coords: new Vector3(0.7926025390625  , -0.382232666015625 , 0.9449996948242188) , type: '_19to36', numbers: ['19'   ,'20'   ,'21'   ,'22'   ,'23'  ,'24'   ,'25' ,'26' ,'27' ,'28' ,'29' ,'30'   ,'31'     ,'32' ,'33' ,'34' ,'35' ,'36']},//19-36
  ],

  lastPosition: null,

};

CasinoRoulette.__NAME = 'casino.roulette';

CasinoRoulette.init = function() {

  return new Promise(async (resolve, reject) => {

    await natives.requestAnimDict('anim_casino_b@amb@casino@games@roulette@dealer_female');
    await natives.requestAnimDict('anim_casino_b@amb@casino@games@roulette@table');

    for(let i=0; i<this.tables.length; i++)
      this.initTable(this.tables[i]);

    alt.setInterval(async () => {

      const ped      = alt.Player.local.ped;
      const position = ped.position;

      for(let i=0; i<this.tables.length; i++) {
        
        const table = this.tables[i];
        
        if($.utils.getDistance(position, table.position) <= 50) {

          const tableObj = natives.getClosestObjectOfType(table.position.x, table.position.y, table.position.z, 1.0, table.model, false, false, false);

          if(tableObj === 0) {

            await (new Model(table.model).request());
            
            table.data.obj    = new Entity(natives.createObject(table.model, table.position.x, table.position.y, table.position.z, false, false, true));  
            const model       = new Model('s_f_y_casino_01');

            await model.request();

            table.data.dealer = Ped.create(model, natives.getOffsetFromEntityInWorldCoords(+table.data.obj, DEALER_OFFSET.x, DEALER_OFFSET.y, DEALER_OFFSET.z));
            
            table.data.dealer.components.setDefault();
            
            table.data.dealer.heading = DEALER_HEADING;

            natives.freezeEntityPosition(+table.data.dealer, true);
            natives.setEntityInvincible(+table.data.dealer, true);
            natives.setBlockingOfNonTemporaryEvents(+table.data.dealer, true);

          } else {

            table.data.obj = new Entity(tableObj);

          }

        }

      }

    }, 2000);

    $.game.on('hz:interactable:interact:casino.roulette', async (interactable, {ws, wsa}, delegateServer) => {
      const table = this.tables.find(e => +e.data.obj === +ws.target);
      this.play(table);
    });

    resolve();

  });

}

CasinoRoulette.initTable = function(table) {

  table.data = {
    obj         : null,
    ball        : null,
    cam         : null,
    chip        : null,
    ...table.data,
    spins       : $.utils.getRandomInt(5, 10),
    spot        : null,
    markers     : [],
    chips       : [],
    bets        : new Map(),
    spinning    : false,
  };
}

CasinoRoulette.spinTable = async function(table) {

  return new Promise(async (resolve, reject) => {

    table.data.spinning = true;

    const bets = [];

    for(const [spot, chips] of table.data.bets) {
      const idx = this.chipOffsets.indexOf(spot);
      bets.push({idx, chips});
    }

    for(let i=0; i<table.data.markers.length; i++)
      natives.deleteObject(+table.data.markers[i]);

    for(let i=0; i<table.data.chips.length; i++)
      natives.deleteObject(+table.data.chips[i]);

    table.data.bets = new Map();

    const success = await $.requestp('hz:casino.roulette:pay', bets);

    if(success) {

      Notification.create('<span style="color:yellow; font-weight: bold;">Début de la partie...</span>');

    } else {

      Notification.create('<span style="color:red; font-weight: bold;">Mise incorrecte, vous n\'avez pas assez de jetons</span>');

      table.data.spinning = false;

      return;

    }

    const wheelBone         = natives.getEntityBoneIndexByName(+table.data.obj, 'Roulette_Wheel');
    const wheelBonePosition = natives.getWorldPositionOfEntityBone(+table.data.obj, wheelBone);
    const wheelBoneOffset   = natives.getOffsetFromEntityGivenWorldCoords(+table.data.obj, wheelBonePosition.x, wheelBonePosition.y, wheelBonePosition.z);
    const wheelAnimDuration = natives.getAnimDuration('anim_casino_b@amb@casino@games@roulette@table', 'loop_wheel');
  
    // WS.camera.position = new Vector3(wheelBonePosition.x, wheelBonePosition.y, wheelBonePosition.z + 0.75);
    // WS.camera.point(new Vector3(wheelBonePosition.x, wheelBonePosition.y, wheelBonePosition.z));


    natives.taskPlayAnim(+table.data.dealer, 'anim_casino_b@amb@casino@games@roulette@dealer_female', 'spin_wheel', 8.0, 1, -1, 2, 0.0, false, false, false);
    
    alt.setTimeout(() => {

      const spinInterval = alt.setInterval(async () => {
      
        if((table.data.spins--) === 0) {
    
          alt.clearInterval(spinInterval);
         
          natives.playEntityAnim(+table.data.obj, 'exit_1_wheel', 'anim_casino_b@amb@casino@games@roulette@table', 1000, false, true, true, 0, 136704);
          
          table.data.ball.position = wheelBonePosition;
          
          const exitBallNumber = $.utils.getRandomInt(1, 36).toString();
    
          natives.playEntityAnim(+table.data.ball, 'exit_' + exitBallNumber.toString() + '_ball', 'anim_casino_b@amb@casino@games@roulette@table', 1000, false, true, true, 0, 136704);
          
          await $.utils.waitFor(() => natives.hasEntityAnimFinished(+table.data.ball, 'anim_casino_b@amb@casino@games@roulette@table', 'exit_' + exitBallNumber.toString() + '_ball', 3));
        
          const ballWorldPosition = table.data.ball.position;
          const ballRelPosition   = natives.getOffsetFromEntityGivenWorldCoords(+table.data.obj, ballWorldPosition.x, ballWorldPosition.y, ballWorldPosition.z);
  
          const result = await $.requestp('hz:casino.roulette:result',
            {x: wheelBoneOffset.x, y: wheelBoneOffset.y, z: wheelBoneOffset.z},
            {x: ballRelPosition.x, y: ballRelPosition.y, z: ballRelPosition.z},
          );
          
          table.data.spinning = false;
  
          resolve(result);
  
          await $.utils.delay(3000);
  
          natives.deleteObject(+table.data.ball);
  
          // const coords = natives.getOffsetFromEntityInWorldCoords(+table.data.obj, 1.5, -1.5, 2.75);
          
          // WS.camera.position = coords;
          // WS.camera.point(table.data.obj);
          
          return;
        }
    
        natives.playEntityAnim(+table.data.obj, 'loop_wheel', 'anim_casino_b@amb@casino@games@roulette@table', 1000, false, false, false, 0, 136704);
    
      }, wheelAnimDuration * 1000);

      natives.playEntityAnim(+table.data.obj, 'spin_wheel', 'anim_casino_b@amb@casino@games@roulette@table', 1000, false, false, false, 0, 0);
    
    }, 1500);

    await (new Model(ROULETTE_BALL).request());
  
    natives.deleteObject(+table.data.ball);

    table.data.ball = new Entity(natives.createObject(ROULETTE_BALL, wheelBonePosition.x, wheelBonePosition.y, wheelBonePosition.z, false, false, true));
    
    alt.setTimeout(() => {
      natives.playEntityAnim(+table.data.ball, 'loop_ball', 'anim_casino_b@amb@casino@games@roulette@table', 1000, true, true, false, 0, 136704);
    }, 1500);

  });
}

CasinoRoulette.refreshMarkers = async function(table, spot) {

  for(let i=0; i<table.data.markers.length; i++)
    natives.deleteObject(+table.data.markers[i]);

  table.data.markers.length = 0;

  const {numbers} = spot;

  for(let i=0; i<numbers.length; i++) {

    const number = numbers[i];
    const coords = natives.getOffsetFromEntityInWorldCoords(+table.data.obj, this.markerOffsets[number].x, this.markerOffsets[number].y, this.markerOffsets[number].z);
    const model  = ((number === '0') || (number === '00')) ? ROULETTE_MARKER_1 : ROULETTE_MARKER_2;
    const marker = new Entity(natives.createObjectNoOffset(model, coords.x, coords.y, coords.z, false, false, false));

    natives.setEntityCollision(+marker, false, false);

    table.data.markers.push(marker);
  
  }

}

CasinoRoulette.getClosestSpot = function(table, coords) {

  let spot     = null;
  let distance = 0.05;

  for(let i=0; i<this.chipOffsets.length; i++) {

    const _spot     = this.chipOffsets[i];
    const _distance = $.utils.getDistance(coords, natives.getOffsetFromEntityInWorldCoords(+table.data.obj, _spot.coords.x,  _spot.coords.y,  _spot.coords.z));

    if(_distance < distance) {
      spot     = _spot;
      distance = _distance;
    }

  }
  
  return spot;

}

CasinoRoulette.play = async function(table) {

  this.lastPosition   = alt.Player.local.ped.position;
  const bones         = ROULETTE_SEATS_BONES.map(e => natives.getEntityBoneIndexByName(+table.data.obj, e));
  const bonePositions = bones.map(e => natives.getWorldPositionOfEntityBone(+table.data.obj, e));

  let foundSeat = null;

  for(let i=0; i<bones.length; i++) {

    const player = alt.Player.all.find(e => $.utils.getDistance(bonePositions[i], e.pos) <= 0.5);

    if(!player) {
      foundSeat = {bone: bones[i], position: bonePositions[i], heading: ROULETTE_SEATS_HEADINGS[i]};
      break;
    }

  }

  if(foundSeat === null) {

    return;

  } else {

    WS.enable(false);
    HUD.disable();
    WS.lock(true);

    natives.taskStartScenarioAtPosition(
      +alt.Player.local.ped,
      'PROP_HUMAN_SEAT_BENCH',
      foundSeat.position.x,
      foundSeat.position.y,
      foundSeat.position.z + 0.1,
      foundSeat.heading,
      0,
      true,
      true
    );

  }

  const coords   = natives.getOffsetFromEntityInWorldCoords(+table.data.obj, 1.5, -1.5, 2.75);
  table.data.cam = Camera.create(coords);

  table.data.cam.point(table.data.obj);
  table.data.cam.render(true, 2000);

  await $.utils.delay(2000);

  WS.camera.position = table.data.cam.position;
  WS.camera.rotation = table.data.cam.rotation;

  table.data.cam.render(false, 0);
  WS.camera.render(true, 0);

  WS.enable(true, {camera: false, context: false});

  HUD.preventToggle = true;

  table.data.chip = new Entity(natives.createObjectNoOffset(ROULETTE_CHIP, table.position.x, table.position.y, table.position.z, false, false, false));
  
  natives.setEntityCollision(+table.data.chip, false, false);
  natives.setEntityVisible(+table.data.chip, false, false);
  
  const onTick = () => {
    natives.disableControlAction(0, 200, true);
  }

  const onHover = ws => {
    if(+ws.target === +table.data.obj)
      this.onHover(table, ws);
  }

  const onBlur = ws => {
    this.onBlur(table, ws);
  }

  const onLeftClick = ws => {
    this.onLeftClick(table, ws);
  }

  const onRightClick = () => {
    this.onRightClick(table);
  }

  $.game.on('tick',              onTick);
  $.game.on('ws:hover:object',   onHover);
  $.game.on('ws:blur',           onBlur);
  $.game.on('ws:mousedown:left', onLeftClick)
  $.game.on('mousedown:right',   onRightClick);
  
  const onKeyDown = key => {

    if(key === 0x1B) {
      
      if(table.data.spinning)
        return;

      alt.off('keydown', onKeyDown);

      $.game.removeListener('ws:hover:object',   onHover);
      $.game.removeListener('ws:blur',           onBlur);
      $.game.removeListener('ws:mousedown:left', onLeftClick);
      $.game.removeListener('mousedown:right',   onRightClick);

      alt.setTimeout(() => {
        $.game.removeListener('tick', onTick)
      }, 300);

      WS.camera.point(null);

      HUD.preventToggle = false;
      WS.lock(false);
      WS.enable(false);
    
      natives.clearPedTasksImmediately(+alt.Player.local.ped);
  
      alt.Player.local.ped.position = this.lastPosition;
  
      natives.deleteObject(+table.data.ball);
      // natives.deleteObject(+table.data.chip);

      for(let i=0; i<table.data.markers.length; i++)
        natives.deleteObject(+table.data.markers[i]);
  
      for(let i=0; i<table.data.chips.length; i++)
        natives.deleteObject(+table.data.chips[i]);
  
      table.data.markers.length = 0;
      table.data.chips.length   = 0;

    }

  }

  alt.on('keydown', onKeyDown);

}

CasinoRoulette.onHover = function(table, ws) {

  if(table.data.spinning)
    return;

  const spot = this.getClosestSpot(table, ws.coords);

  if(spot) {

    if(spot !== table.data.spot) {
      table.data.spot = spot;
      this.refreshMarkers(table, spot);
    }

    /*
    if(table.data.chip) {
      natives.setEntityVisible(+table.data.chip, true, true);
      table.data.chip.position = new Vector3(ws.coords.x, ws.coords.y, ws.coords.z + 0.025);
    }
    */

  }

}

CasinoRoulette.onBlur = function(table, ws) {

  if(table.data.spinning)
    return;

  // natives.setEntityVisible(+table.data.chip, false, false);

}

CasinoRoulette.onLeftClick = function(table, ws) {

  if(table.data.spinning)
    return;

  const coords = ws.coords;
  const spot   = this.getClosestSpot(table, coords);

  if(spot){

    const menu = MM.open('Mise', [
      {label: 'Jetons',  name: 'chips',  type: 'input', value: table.data.bets.has(spot) ? table.data.get(spot).toString() : '1'},
      {label: 'Valider', name: 'submit', value: 'submit'},
    ]);

    HUD.enable(false);
    
    menu.on('select', (data, item) => {

      const byName = menu.by('name');
      let   chips  = parseInt(byName.chips.value, 10);

      if(isNaN(chips))
        chips = 1;

      switch(item.name) {

        case 'submit' : {
          
          if(!table.data.bets.has(spot)) {

            const spotCoords = natives.getOffsetFromEntityInWorldCoords(+table.data.obj, spot.coords.x, spot.coords.y, spot.coords.z);
            const chip       = new Entity(natives.createObjectNoOffset(ROULETTE_CHIP, spotCoords.x, spotCoords.y, spotCoords.z, false, false, false));
            
            table.data.chips.push(chip);
          }

          table.data.bets.set(spot, chips);

          HUD.disable();
          menu.destroy();

          break;
        }

        default: break;

      }

    });
  
  }

}

CasinoRoulette.onRightClick = async function(table) {

  if(table.data.spinning)
    return;

  const earnings = await this.spinTable(table);
  let   win      = false;

  for(let i=0; i<earnings.length; i++) {

    win = true;
    Notification.create('<span style="color:green; font-weight: bold;">' + earnings[i].label + ' <b>+' + earnings[i].amount + '</b></span>');

  }

  if(!win)
    Notification.create('<span style="color:red; font-weight: bold;">Perdu</span>');

  for(let i=0; i<table.data.markers.length; i++)
    natives.deleteObject(+table.data.markers[i]);

  for(let i=0; i<table.data.chips.length; i++)
    natives.deleteObject(+table.data.chips[i]);

  this.initTable(table);

}

export default CasinoRoulette;
