import EventEmitter from 'events';
import * as alt     from 'alt'
import * as natives from 'natives';
import $            from '../../../lib/altv-api/client/index';

import HUD from '../hud/index.js';

const { waitFor } = $.utils;

class Win extends EventEmitter {

  constructor(id, props) {
    
    super();

    for(let k in props)
      if(props.hasOwnProperty(k))
        this[k] = props[k];

    this.id = id;
  }

  close() {
    HUD.postMessage({action: 'close_window', id: this.id});
  }

  open() {
    HUD.postMessage({action: 'open_window', id: this.id});
  }

  destroy() {
    
    this.emit('destroy');

    HUD.postMessage({action: 'destroy_window', id: this.id});

    const idx = WindowManager.windows.indexOf(this);

    if(idx !== -1)
      WindowManager.windows.splice(idx, 1);

  }

  show() {
    HUD.postMessage({action: 'show_window', id: this.id});
  }

  postMessage(msg) {
    HUD.postMessage({action: 'post_window_message', id: this.id, msg});
  }

}

const WindowManager = {
  windows: [],
  id     : -1,
};

WindowManager.__NAME = 'window';

WindowManager.init = function() {

  return new Promise(async (resolve, reject) => {

    HUD.on('message', msg => {

      switch(msg.action) {

        case 'window_loaded' : {

          const win = this.windows.find(e => e.id === msg.id);

          if(win !== undefined) {
            win.emit('load');
          }

          break;
        }

        case 'window_open' : {

          const win = this.windows.find(e => e.id === msg.id);

          if(win !== undefined) {

            win.on('message', msg => {

              if(msg.action && msg.action.startsWith('internal:')) {
  
                const action = msg.action.substr(9);
  
                switch(action) {
  
                  case 'window:destroy' : {
                    win.destroy();
                    break;
                  }
  
                  default: break;
  
                }
  
              }
  
            });

            win.emit('open');
            
          }

          break;
        };

        case 'window_closed' : {

          const win = this.windows.find(e => e.id === msg.id);

          if(win !== undefined) {
            win.emit('close');
          }

          const remaining = this.windows.filter(e =>!e.system);

          if(remaining.length === 0)
            HUD.disable();

          break;
        };

        case 'window_destroyed' : {

          const win = this.windows.find(e => e.id === msg.id);
          
          if((win !== undefined) && (destroyed.indexOf(win) === -1)) 
            win.emit('destroy');

          break;
        };

        case 'window_message' : {

          const win = this.windows.find(e => e.id === msg.id);

          if(win !== undefined) {
            win.emit('message', msg.msg);
          }

          break;

        }

        default: break;

      }

    });

    resolve();

  });

}

WindowManager.open = function(props) {

  const id     = this.nextId();
  const _props = {...props, id};

  const win = new Win(id, props);

  this.windows.push(win);
  
  HUD.postMessage({
    action : 'open_window',
    id     : id,
    props  : _props,
  });

  win.on('message', msg => {

    switch(msg.action) {

      case 'internal:close' : {
        win.close();
        break;
      }

      case 'internal:destroy' : {
        win.destroy();
        break;
      }

      case 'internal:open' : {
        win.open();
        break;
      }

      default: break;

    }

  });

  return win;
}

WindowManager.nextId = function() {
  this.id = this.id >= 1000000 ? 0 : this.id + 1;
  return this.id;
}

export default WindowManager;