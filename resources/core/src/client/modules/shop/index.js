import * as alt from 'alt'
import * as natives from 'natives';

import HUD from '../hud/index.js';
import WM from '../windowmanager/index.js';
import MM from '../menumanager/index.js';
import Cache from '../cache/index';
import Notification from '../notification/index';
import * as entities from '../../entities/index';
import $ from '../../../lib/altv-api/client/index';

const Shop = {
  win       : null,
  cardTokens: {},
};

Shop.__NAME = 'atm';

Shop.open = async function(interactable) {
  
  const _id   = interactable._id;
  const cdata = interactable.customData;

  if(this.win !== null)
    this.win.destroy();

  const entitiesAsArray = Object.keys(entities).map(e => new entities[e]);
  const entitiesByName  = {};

  for(let i=0; i<entitiesAsArray.length; i++)
    entitiesByName[entitiesAsArray[i].name] = entitiesAsArray[i];

  const items = (cdata.items || []).map(e => ({
    _type: entitiesByName[e.name]._type,
    name : e.name,
    label: entitiesByName[e.name].label,
    image: entitiesByName[e.name].image,
    price: e.price
  }));
  
  const identity = await Cache.resolve({_type: 'Identity', _id: alt.Player.local.getSyncedMeta('identity')});
  const cards    = [];

  for(let i=0; i<identity.containers.length; i++) {

    const container = identity.containers[i];
    const items     = container.items;

    for(let i=0; i<items.length; i++) {

      const item = items[i];

      if(item instanceof entities.HZItemBankCard)
        cards.push(item);
    }

  }

  const menuData = [{label: 'Cash', action: 'select_cash'}].concat(cards.map(e => ({label: 'Carte: ' + e.number, number: e.number, action: 'select_card'})).concat([{label: 'Annuler', action: 'cancel'}]));
  const menu     = MM.open('Moyen de paiement', menuData);

  menu.on('load', () => HUD.enable());

  menu.on('select', async (data, item) => {

    const run = () => {

      switch(item.action) {

        case 'cancel' : {
          menu.destroy();
          break;
        }
  
        default: {
  
          menu.destroy();
          const [_, screenX, screenY] = natives.getActiveScreenResolution();
  
          this.win = WM.open({
            url        : 'http://resource/dist/html/shop/index.html',
            closable   : false,
            stayinspace: false,
            classname  : 'transparent system notitle',
            width      : screenX - 600,
            height     : screenY - 100,
            x          : 300,
            y          : 50,
          });
        
          this.win.on('destroy', () => {
            this.win = null;
          });
        
          this.win.on('message', async msg => {

            if(msg.action === 'component_mounted') {
              this.win.postMessage({action: 'set_mode',  mode: cdata.mode || 'buy'});
              this.win.postMessage({action: 'set_items', items});
            }
  
            if(msg.action === 'notification') {
              Notification.create(msg.msg);
            }
  
            if(msg.action === 'buy') {
  
              if(item.action === 'select_card') {
  
                alt.emitServer('hz:shop:buy', _id, msg.name, msg.qty, 'card', {number: item.number, token: Shop.cardTokens[item.number]});
  
              } else {
  
                alt.emitServer('hz:shop:buy', _id, msg.name, msg.qty, 'cash', {});
  
              }
  
            }

            if(msg.action === 'sell') {
  
              if(item.action === 'select_card') {
  
                alt.emitServer('hz:shop:sell', _id, msg.name, msg.qty, 'card', {number: item.number, token: Shop.cardTokens[item.number]});
  
              } else {
  
                alt.emitServer('hz:shop:sell', _id, msg.name, msg.qty, 'cash', {});
  
              }
  
            }
        
          });
  
          break;
        }
  
      }

    }

    switch(item.action) {

      case 'select_card' : {

        menu.destroy();

        const subMenu = MM.open('Code PIN', [
          {label: '0000',    type: 'input', value: '', name: 'pin'},
          {label: 'Valider', name: 'submit'},
        ]);
  
        subMenu.on('select', async (data2, item2) => {
  
          if(item2.name === 'submit') {
  
            subMenu.destroy();
  
            const byName  = subMenu.by('name');
            const pinCode = byName.pin.value;
    
            alt.emitServer('hz:shop:card:auth', item.number, pinCode);
    
            run();
  
          }
  
        });

        break;
      }

      case 'cancel' : {
        menu.destroy();
        break;
      }

      default: {
        menu.destroy();
        run();
        break;
      }

    }

  });

}

Shop.init = function() {

  return new Promise((resolve, reject) => {

    $.game.on('hz:interactable:interact:openshop', (interactable, {ws, wsa}, delegateServer) => Shop.open(interactable));

    alt.onServer('hz:shop:card:auth:token', (number, token) => {
      Shop.cardTokens[number] = token;
    });

    resolve();

  });

}

export default Shop;
