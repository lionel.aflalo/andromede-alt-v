import * as alt from 'alt'
import * as natives from 'natives';

import $ from '../../../lib/altv-api/client/index';

const { Camera, Model, Entity } = $.types;

import * as entities from '../../entities/index';
import HUD           from '../hud/index';
import WM            from '../windowmanager/index';
import MM            from '../menumanager/index';
import Interact      from '../interact/index';
import Cache         from '../cache/index';
import Shop          from '../shop/index';

import config from './config.json';

const CarDealer = {

  win: null,

  concessions: config.concessions.map(e => ({
    ...e,
    vehicles: e.vehicles.map(e => ({
      ...e, 
      hash: $.utils.joaat(e.name),
      gxt : natives.getLabelText(natives.getDisplayNameFromVehicleModel($.utils.joaat(e.name)))
    }))
  })),

  paymentData: null,

};

CarDealer.__NAME = 'cardealer';

CarDealer.open = function(name) {

  return new Promise(async (resolve, reject) => {

    const identity = await Cache.resolve({_type: 'Identity', _id: alt.Player.local.getSyncedMeta('identity')});
    const cards    = [];
  
    for(let i=0; i<identity.containers.length; i++) {
  
      const container = identity.containers[i];
      const items     = container.items;
  
      for(let i=0; i<items.length; i++) {
  
        const item = items[i];
  
        if(item instanceof entities.HZItemBankCard)
          cards.push(item);
      }
  
    }
  
    const menuData = [{label: 'Cash', action: 'select_cash'}].concat(cards.map(e => ({label: 'Carte: ' + e.number, number: e.number, action: 'select_card'})).concat([{label: 'Annuler', action: 'cancel'}]));
    const menu     = MM.open('Moyen de paiement', menuData);
  
    menu.on('load', () => HUD.enable(false));

    menu.on('select', async (data, item) => {
  
      const run = async () => {

        HUD.preventToggle = true;
  
        const player               = $.game.player;
        const ped                  = player.ped;
        const data                 = this.concessions.find(e => e.name === name);
        let pr = 0, pg = 0, pb = 0;
        let sr = 0, sg = 0, sb = 0;
        let interval, vehicle, cam = null;
        let rotationZ              = 0.0;
        let model                  = null;
    
        const [_, screenX, screenY] = natives.getActiveScreenResolution();
    
        this.win = WM.open({
          url        : 'http://resource/dist/html/cardealer/index.html',
          closable   : false,
          stayinspace: false,
          classname  : 'transparent system notitle',
          width      : 300,
          height     : screenY,
          x          : 0,
          y          : 0,
        });
      
        this.win.on('load', () => {
          HUD.enable(false);
        });
      
        this.win.on('message', async msg => {
      
          switch(msg.action) {
      
            case 'component_mounted' : {
              this.win.postMessage({action: 'load', data});
              break;
            }
      
            case 'cancel' : {
    
              HUD.preventToggle = false;
    
              cam.render(false);
              cam.destroy();
    
              natives.deleteVehicle(vehicle);
              alt.clearInterval(interval);
              HUD.disable(false);
              this.win.destroy();
    
              resolve(null);
    
              break;
            }
    
            case 'submit' : {
    
              HUD.preventToggle = false;
    
              cam.render(false);
              cam.destroy();
    
              natives.deleteVehicle(vehicle);
              alt.clearInterval(interval);
              HUD.disable(false);
              this.win.destroy();
    
              resolve({model: +model, pr, pg, pb, sr, sb, sg, position: data.spawn, concession: data.name});
    
              break;
            }
      
            case 'set_primary_color' : {
    
              pr = msg.r;
              pg = msg.g;
              pb = msg.b;
    
              natives.setVehicleCustomPrimaryColour(+vehicle, pr, pg, pb);
    
              break;
            }
    
            case 'set_secondary_color' : {
    
              sr = msg.r;
              sg = msg.g;
              sb = msg.b;
    
              natives.setVehicleCustomSecondaryColour(+vehicle, sr, sg, sb);
    
              break;
            }
    
            case 'set_vehicle' : {
    
              model = new Model(msg.vehicle.hash);
    
              await model.request();
    
              natives.deleteVehicle(vehicle);
    
              vehicle = new Entity(natives.createVehicle(+model, data.spawn.x, data.spawn.y, data.spawn.z, rotationZ, false, false, false));
    
              model.setAsNoLongerNeeded();
    
              natives.placeObjectOnGroundProperly(+vehicle);
              natives.setEntityCollision(+vehicle, false, false);
              natives.freezeEntityPosition(+vehicle, true);
    
              natives.setVehicleCustomPrimaryColour(+vehicle, pr, pg, pb);
              natives.setVehicleCustomSecondaryColour(+vehicle, sr, sg, sb);
    
              break;
            }
    
            default: break;
          }
      
        });
    
        cam   = Camera.create(data.cam);
        model = new Model(data.vehicles[0].hash);
    
        await model.request();
    
        vehicle = new Entity(natives.createVehicle(+model, data.spawn.x, data.spawn.y, data.spawn.z, 0.0, false, false, false));
    
        model.setAsNoLongerNeeded();
    
        natives.placeObjectOnGroundProperly(+vehicle);
        natives.setEntityCollision(+vehicle, false, false);
        natives.freezeEntityPosition(+vehicle, true);
    
        natives.setVehicleCustomPrimaryColour(+vehicle, pr, pg, pb);
        natives.setVehicleCustomSecondaryColour(+vehicle, sr, sg, sb);
    
        cam.point(vehicle);
        cam.render(true);
    
        interval = alt.setInterval(() => {
          
          const rotation   = vehicle.rotation;
          rotation.z      += 0.2;
          vehicle.position = {...data.spawn, z: vehicle.position.z};
          vehicle.rotation = rotation;
          rotationZ        = vehicle.rotation.z;
    
        }, 5);
  
      }

      switch(item.action) {

        case 'select_card' : {
  
          menu.destroy();

          const subMenu = MM.open('Code PIN', [
            {label: '0000',    type: 'input', value: '', name: 'pin'},
            {label: 'Valider', name: 'submit'},
          ]);
    
          subMenu.on('select', async (data2, item2) => {
    
            if(item2.name === 'submit') {
    
              subMenu.destroy();
    
              const byName  = subMenu.by('name');
              const pinCode = byName.pin.value;
      
              alt.emitServer('hz:shop:card:auth', item.number, pinCode);
      
              this.paymentData = {type: 'card', data: {number: item.number}};

              run();
    
            }
    
          });
  
          break;
        }
  
        case 'cancel' : {
          menu.destroy();
          break;
        }
  
        default: {
          menu.destroy();
          this.paymentData = {type: 'cash', data: {}};
          run();
          break;
        }
  
      }
  
    });

  });

}

CarDealer.init = function() {

  return new Promise((resolve, reject) => {

    for(let i=0; i<this.concessions.length; i++) {

      const concession = this.concessions[i];
      
      Interact.registerMarker(concession.blip, {
        type       : 'cardealer',
        message    : '[E] Concession',
        concession : concession.name
      });

      const blip = natives.addBlipForCoord(concession.blip.x, concession.blip.y, concession.blip.z);
      natives.setBlipSprite(blip, concession.blip.sprite);
      natives.setBlipColour(blip, concession.blip.color);
      natives.setBlipDisplay(blip, 4);
      natives.setBlipScale(blip, 1.0);
      natives.setBlipAsShortRange(blip, true);
      natives.beginTextCommandSetBlipName('STRING');
      natives.addTextComponentSubstringPlayerName(concession.label);
      natives.endTextCommandSetBlipName(blip);

    }

    $.game.on('interact:use', async e => {

      if(e.data.type === 'cardealer') {

        const data = await CarDealer.open(e.data.concession);

        if(data) {

          if(this.paymentData.type === 'card')
            this.paymentData.data.token = Shop.cardTokens[this.paymentData.data.number];

          alt.emitServer('hz:cardealer:spawnVehicle', data, this.paymentData.type, this.paymentData.data);
        }

      }

    });

    resolve();

  });

}

export default CarDealer;
