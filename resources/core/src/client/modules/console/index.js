import * as alt from 'alt';
import * as natives from 'natives';

import chat     from 'chat'
import $        from '../../../lib/altv-api/client/index';
import Skin     from '../Skin/index';
import Cache    from '../cache/index';
import Area     from '../area/index';

const Console = {
  commands: {},
};

Console.__NAME = 'console';

Console.commands['skin'] = async (...args) => {
  
  try {

    const player = await Cache.resolve({_type: 'Player', _id: alt.Player.local.getSyncedMeta('player')});

    if(!player || (player.roles.indexOf('admin') === -1))
      return;

    alt.log('Started skin editor');
  
    await Skin.create();
  
    alt.emitServer('hz:identity:update:model', {
      hash         : +$.game.player.model,
      headBlendData: $.game.player.ped.headBlendData.get(),
      faceFeatures : $.game.player.ped.faceFeatures.get(),
      components   : $.game.player.ped.components.get(),
      props        : $.game.player.ped.props.get(),
      overlays     : $.game.player.ped.overlays.get(),
    });
  
    alt.log('Stopped skin editor');

  } catch(e) { alt.logError(e.message, e.stack); }
}

Console.commands['anim'] = async (dict, name) => {

  natives.requestAnimDict(dict);

  await $.utils.waitFor(() => natives.hasAnimDictLoaded(dict), 2500);

  if(natives.hasAnimDictLoaded(dict))
    natives.taskPlayAnim(+alt.Player.local.ped, dict, name, 1.0, -1.0, -1, 1, 1, false, false, false);
}

Console.commands['area.debug'] = () => {
  Area.debug = !Area.debug;
}

Console.init = function() {
  
  return new Promise((resolve, reject) => {

    alt.on('consoleCommand', (cmd, ...args) => {

      if(this.commands[cmd] !== undefined)
        this.commands[cmd](...args);
      else
        alt.emitServer('hz:cmd', cmd, ...args);

    });

    resolve();
  });

}

export default Console;
