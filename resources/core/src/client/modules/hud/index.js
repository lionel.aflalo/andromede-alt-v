import EventEmitter from 'events';
import * as alt from 'alt'
import * as natives from 'natives';
import $ from '../../../lib/altv-api/client/index';

import WorldSelect from '../worldselect/index.js';

const { WebView } = alt;

class HUD extends EventEmitter {

  constructor() {

    super();

    this.view          = null;
    this.time          = null;
    this.enabled       = false;
    this.preventToggle = false;

    this.__NAME = 'hud';
  }

  init() {

    return new Promise((resolve, reject) => {
      
      try {
        
        alt.on('keydown', key => {
          this.time = +new Date;
        });
        
        alt.on('keyup', key => {
      
          if(key == 222) {
  
            const now = +new Date;
  
            if(now - this.time <= 250) {

              if(!this.preventToggle)
                this.toggle();
            }
          }
      
        });
  
        this.view = new WebView('http://resource/dist/html/main/index.html');
  
        this.view.on('hud.message', json => {
    
          let msg = {};

          try { msg = JSON.parse(json); } catch(e) {}

          if(msg.action === undefined)
            return;

          switch(msg.action) {
        
            case 'init' : {
              
              alt.setTimeout(() => {
                alt.log('[core/hud] webview init done');
                resolve();
              }, 0);
    
              break;
            }
    
            default: break;
          }
        
          this.emit('message', msg);

        });
  
      } catch(e) { reject(e); }
  
    });
  
  }
  
  enable(overlay = undefined) {
  
    alt.clearTimeout(WorldSelect.timeout);

    if(this.enabled)
      return;

    this.enabled = true;
  
    this.postMessage({action:'show', overlay});
  
    WorldSelect.locked = true;
  
    alt.toggleGameControls(false);
    this.view.focus();
    alt.showCursor(true);
  
  }
  
  disable(overlay = undefined) {
  
    if(!this.enabled)
      return;

    this.enabled = false;

    this.postMessage({action: 'hide', overlay});

    WorldSelect.locked = false;
    alt.showCursor(false);
    this.view.unfocus();
    alt.toggleGameControls(true);
  }
  
  toggle() {
  
    if(this.enabled)
      this.disable(true);
    else
      this.enable(true);
  
  }

  postMessage(msg) {
    //this.view.execJS(`app.onMessage(${JSON.stringify(msg)});`);
    this.view.emit('script.message', JSON.stringify(msg));
  }
}

const instance = new HUD();

export default instance;