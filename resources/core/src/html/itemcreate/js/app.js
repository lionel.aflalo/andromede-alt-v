'use strict';

(() => {

  fabric.Object.prototype.exportPNG = function() {
    function trimCanvas(canvas)
    {
      var ctx = canvas.getContext('2d'),
        w = canvas.width,
        h = canvas.height,
        pix = {x:[], y:[]}, n,
        imageData = ctx.getImageData(0,0,w,h),
        fn = function(a,b) { return a-b };

      for (var y = 0; y < h; y++) {
        for (var x = 0; x < w; x++) {
          if (imageData.data[((y * w + x) * 4)+3] > 0) {
            pix.x.push(x);
            pix.y.push(y);
          }
        }
      }
      pix.x.sort(fn);
      pix.y.sort(fn);
      n = pix.x.length-1;

      w = pix.x[n] - pix.x[0];
      h = pix.y[n] - pix.y[0];
      var cut = ctx.getImageData(pix.x[0], pix.y[0], w, h);

      canvas.width = w;
      canvas.height = h;
      ctx.putImageData(cut, 0, 0);
    };

    var bound = this.getBoundingRect(),
      json = JSON.stringify(this),
      canvas = fabric.util.createCanvasElement();
    canvas.width = bound.width;
    canvas.height = bound.height;
    var fcanvas = new fabric.Canvas(canvas, {enableRetinaScaling:false});

    fabric.util.enlivenObjects([JSON.parse(json)], function(objects) {
      objects.forEach(function(o) {
        o.top -= bound.top;
        o.left -= bound.left;
        fcanvas.add(o);
      });
      fcanvas.renderAll();

      var canvas = fcanvas.getElement();
      trimCanvas(canvas);

      var url = canvas.toDataURL('image/png'),
          img = new Image();
      img.width = canvas.width;
      img.height = canvas.height;
      img.src = url;
      document.body.appendChild(img);

    });
  };

  const ENDPOINT = '/api';

  const CANVAS_WIDTH  = 801;
  const CANVAS_HEIGHT = 801;
  const GRID_WIDTH    = 800;
  const GRID_HEIGHT   = 800;
  const GRID_SIZE     = 100;

  const ToolbarElement = props => $wrap(`
    <div class="toolbar-element <%% $props.selected ? 'selected' : '' %%>">
      <i class="fas fa-<%% $props.icon %%> fa-2x"></i>
    </div>
  `, {
    ...props,
    '#onmouseup' : function(e) {
      
      this.$props.selected = !this.$props.selected;

      if(this.$props.selected) {
        app.img.moveTo(1000);
        app.img.set('selectable', true);
      } else {
        app.img.moveTo(0);
        app.img.set('selectable', false);
      }

      this.$props.app.c.discardActiveObject();
      this.$props.app.c.renderAll(); 

    }
  });

  const Toolbar = props => $wrap(`
    <div class="toolbar" style="width: <%% props.gw %%>px;">
      <div class="toolbar-children">
        <% $props.children %>
      </div>
      <div class="toolbar-url">
        <input type="text" name="url" value="<%% $props.url %%>"/>
      </div>
    </div>
  `, {
    '#onkeyup' : function(e) {
      
      clearTimeout(this.loadTimeout);

      const url = this.querySelector('input[name=url]');

      this.$props.url = url.value;
      const s         = url.selectionStart;

      setTimeout(() => {
        const url = this.querySelector('input[name=url]');
        url.focus();
        url.setSelectionRange(s, s);
      }, 0);

      this.loadTimeout = setTimeout(() => {

        if(this.$props.app.img !== null)
          this.$props.app.c.remove(this.$props.app.img);

        fabric.Image.fromURL('http://localhost:5000/api/img/load/' + encodeURIComponent(url.value), (img) => {
          this.$props.app.img = img;
          this.$props.app.c.add(img);
          img.scaleToWidth(CANVAS_WIDTH / 2);
                  app.img.exportPNG();
        });

      }, 1000);
    },
    children: [],
    selected: false,
    url: '',
    ...props
  });

  const Main = props => $wrap(`
    <div class="main-wrap">
      <% $props.toolbar %>
      <canvas class="canvas-view" width="<%% props.cw %%>" height="<%% props.cw %%>"></canvas>
    </div>
  `, {
    toolbar: Toolbar({
      ...props,
      children: [
        ToolbarElement({...props, icon: 'arrows-alt', selected: true}),
      ]
    }),
    ...props
  });

  window.app = {};
  app.img    = null;
  app.root   = document.querySelector('#root');

  app.main = Main({
    app,
    cw: CANVAS_WIDTH,
    ch: CANVAS_HEIGHT,
    gw: GRID_WIDTH,
    gh: GRID_HEIGHT,
  });

  app.root.appendChild(app.main);

  app.canvas = app.main.querySelector('.canvas-view');
  app.c      = new fabric.Canvas(app.canvas);

  for(let x=0; x<GRID_WIDTH; x += GRID_SIZE) {
    for(let y=0; y<GRID_HEIGHT; y += GRID_SIZE) {

      const rect = new fabric.Rect({
        left: x,
        top: y,
        width: GRID_SIZE,
        height: GRID_SIZE,
        fill: 'rgba(100, 100, 100, 0.25)',
        stroke: '#000',
        strokeWidth: 1,
        selectable: false,
        clickable: false,
      });

      rect.$enabled = false;

      rect.on('mouseover', e => {

        if(rect.$enabled)
          return;

        rect.set('fill', 'rgba(25, 25, 25, 0.25)');
        app.c.renderAll();
      });

      rect.on('mouseout', e => {

        if(rect.$enabled)
          return;

        rect.set('fill', 'rgba(100, 100, 100, 0.25)');
        app.c.renderAll();
      });

      rect.on('mouseup', e => {

        rect.$enabled = !rect.$enabled;
        
        if(rect.$enabled)
          rect.set('fill', 'rgba(25, 25, 100, 0.25)');
        else
          rect.set('fill', 'rgba(100, 100, 100, 0.25)');

        app.c.renderAll();
      });

      app.c.add(rect);
    }
  }

  const url = 'https://www.stickpng.com/assets/images/580b585b2edbce24c47b242e.png';

})();