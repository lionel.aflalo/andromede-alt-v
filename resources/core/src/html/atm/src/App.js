import React, { Component } from 'react';
import { Modal, ModalBody, Container, Row, Col, Button } from 'reactstrap';
import i18next from 'i18next';
import {reactI18nextModule } from "react-i18next";
import { withNamespaces } from 'react-i18next';
import { resources } from './translations';

import NumPad from './components/NumPad';
import MainView from './components/views/Main';
import AccountStetemantView from './components/views/AccountStatement';
import WithdrawView from './components/views/Withdraw';
import DepositView from './components/views/Deposit';
import TransferView from './components/views/Transfer';
import ChangePinView from './components/views/ChangePin';

import 'bootstrap/dist/css/bootstrap.min.css';
import './superhero-bootstrap.css';
import './App.css';

const i18nextConfig = {
  resources,
  lng: "en",
  fallbackLng: "en",
  interpolation: {
    escapeValue: false
  }
};

i18next
  .use(reactI18nextModule)
  .init(i18nextConfig)
;

class App extends Component {

  static get views() {

    return {
      main             : MainView,
      account_statement: AccountStetemantView,
      withdraw         : WithdrawView,
      deposit          : DepositView,
      transfer         : TransferView,
      changepin        : ChangePinView,
    }

  }

  constructor(props) {

    super(props);

    this.numPad = null;

    this.state = {
      view      : 'main',
      viewProps : {},
      user      : null,
      card      : null,
      config    : null,
      cardNumber: null,
      cash      : 0,
    };

    if(process.env.NODE_ENV === 'development') {
      this.state.user       = {"_id":"5bf227331d8ceb4a1c44bedd","_data":{},"_references":{},"roles":[],"_type":"StandardBankUser","address":"654 zef 654","phoneNumber":56645,"accounts":["5bf227f2b23ecb499c6901fa","5bf27170dc50dc4f581ac460","5bf50470bf19ac1e70cb8437","5bf504e58ad7c1408cb0499d"],"token":null,"active":true,"firstName":"John","lastName":"Smith","residentNumber":123456789,"login":"john.smith2890","_subdocs":[],"typeChain":["BankUser","StandardBankUser"]};
      this.state.card       = {"_id":"5bfaad660bb77807bcda7c22","_data":{},"_references":{},"_type":"BankCard","name":"bankcard","count":1,"removeable":true,"giveable":true,"equipable":false,"mass":10,"volume":{"x":5,"y":10,"z":0.1},"compressible":false,"image":null,"model":289396019,"rotation":{"x":0,"y":0,"z":0},"flags":0,"number":"2680434235418851","cvv":"162","pinCode":"7190","account":"5bf504e58ad7c1408cb0499d","user":"5bf227331d8ceb4a1c44bedd","token":"850a19598fb37cfaa05a5733af722e0d","_subdocs":[],"typeChain":["Item","BankCard"],"stackable":true,"_containers":[],"usable":false,"codeName":"smys","variantHash":"6306835ef361478e467259c13777fc846a1029f3","humanVariantHash":"curled-i-hundred","actions":[],"equip":{"default":{"visible":false,"bone":-1,"offset":{"x":0,"y":0,"z":0},"rotation":{"x":0,"y":0,"z":0}}},"label":"bankcard:2680 4342 3541 8851 CVV:162"};
    }

    window.addEventListener('message', e => this.onMessage(JSON.parse(e.data)));

    window.App = this;
  }

  onMessage(data) {
    
    switch(data.action) {
      
      case 'load_config' : {
        
        this.setState({config: data.config}, () => {
          const i18ncfg = {...i18nextConfig};
          i18ncfg.lng = this.state.config.locale;
          i18next.init(i18ncfg);
        });

        break;
      }

      case 'set_card' : {
        this.setState({cardNumber: data.number});
        break;
      }

      case 'set_cash' : {
        this.setState({cash: data.cash});
        break;
      }

      default: break;
    }

  }

  setCurrentView(name, props = {}, cb) {
    this.setState({view: name, viewProps: props}, cb);
  }

  componentDidMount() {

    if(process.env.NODE_ENV === 'development') {

      this.onMessage({
        action: 'load_config',
        config: {
          socket: 'http://127.0.0.1:5000',
          locale: 'fr',
        }
      });

      this.onMessage({
        action: 'set_card',
        number: '2680434235418851',
      });

    }

    window.parent.postMessage(JSON.stringify({action: 'component_mounted'}), '*');
    
  }

  handleNumpad(val) {

    const formData = {number: this.state.cardNumber, pinCode: val}

    fetch(this.state.config.socket + '/api/atm/auth', {
      method: 'POST',
      headers: {
        'Accept': 'application/json',
        'Content-Type': 'application/json'
      },
      body: JSON.stringify(formData)
    })
      .then(response => response.json())
      .then(data => {

        if(data.error)
          this.numPad.setState({value: ''})
        else
          this.setState(data);
      })
    ;

  }

  generateViewElements(children) {

    const elements    = [];
    let   rowElements = [];

    for(let i=0; i<children.length; i++) {

      const child = children[i];
      const cls   = (i % 2 === 0) ? 'left' : 'right';
      rowElements.push(<Col md={6} className={cls + " h-100"} key={i}>{child}</Col>);

      if(rowElements.length === 2 || (i+1) === children.length) {
        elements.push(<Row key={elements.length}>{rowElements}</Row>);
        rowElements = [];
      }

    }

    return elements;

  }

  render() {
    
    const CurrentView = withNamespaces('common')(App.views[this.state.view]);

    if(this.state.config && this.state.cardNumber) {

      return (
        <div className="app">
          <Modal isOpen={!this.state.user} className="numpad-modal">
            <ModalBody>
              <NumPad ref={e => this.numPad = e} parent={this} onSubmit={val => this.handleNumpad(val)}/>
            </ModalBody>
          </Modal>
          <div className="app-header">
            <img src="./img/logo.png" className="app-header-logo"/>
            <div className="app-header-logo">Andro<span className="app-header-logo-accent">BANK</span></div>
          </div>
          <CurrentView {...this.state.viewProps} parent={this}/>
        </div>
      );

    } else return <div />

  }
}

export default withNamespaces('common')(App);
