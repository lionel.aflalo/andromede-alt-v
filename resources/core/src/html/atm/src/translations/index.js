import resources from './resources';

export const config = {
  interpolation: { escapeValue: false }, // React already does escaping
  fallbackLng: 'en',
  resources,
};

export { resources };
