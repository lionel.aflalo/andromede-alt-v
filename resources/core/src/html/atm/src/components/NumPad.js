import React, { Component } from 'react';
import { Container, Row, Col, Button, Input } from 'reactstrap';

import './NumPad.css';


class NumPad extends Component {

  constructor(props) {

    super(props);

    this.onKeyPress = e => {

      e.preventDefault();

      let value = this.state.value.toString();

      if(!isNaN(e.key) && this.state.value.length < 4) {
        value = this.state.value + e.key;
      } else {

        if(e.keyCode === 13) {
          this.handleSubmit();
        }

      }

      this.setState({value});

    };

    this.state = {
    	value: '',
    };
  }

  componentDidMount() {
    window.addEventListener('keypress', this.onKeyPress);
  }

  componentWillUnmount() {
    window.removeEventListener('keypress', this.onKeyPress);
  }

  handleClick(char) {

  	let value = this.state.value.toString();

  	if(!isNaN(char)) {
  		
  		value += char.toString();
  	
      this.setState({value: (value.length > 4) ? value.substr(0, 4) : value});

  	} else {

  		if(char === 'c')
  			window.parent.postMessage(JSON.stringify({action: 'internal:close'}), '*');

  		if(char === 's')
  			this.handleSubmit();
  	}

  }

  handleSubmit() {

  	this.props.parent.setState({handleNumpad: null});

  	(this.props.onSubmit ? this.props.onSubmit : val => {})(this.state.value);
  }

  render() {
    return (
      <Container className="numpad">
      	<Row>
      		<Col className="pincode-wrapper" md={12}>
      			<Input className="pincode" type="password" value={this.state.value} disabled/>
      		</Col>
      	</Row>
	    	<Row>
	    		<Col className="button-wrapper" md={4}>
	    			<Button onClick={e => this.handleClick(1)}>1</Button>
	    		</Col>
	    		<Col className="button-wrapper" md={4}>
	    			<Button onClick={e => this.handleClick(2)}>2</Button>
	    		</Col>
	    		<Col className="button-wrapper" md={4}>
	    			<Button onClick={e => this.handleClick(3)}>3</Button>
	    		</Col>
	    	</Row>
	    	<Row>
	    		<Col className="button-wrapper" md={4}>
	    			<Button onClick={e => this.handleClick(4)}>4</Button>
	    		</Col>
	    		<Col className="button-wrapper" md={4}>
	    			<Button onClick={e => this.handleClick(5)}>5</Button>
	    		</Col>
	    		<Col className="button-wrapper" md={4}>
	    			<Button onClick={e => this.handleClick(6)}>6</Button>
	    		</Col>
	    	</Row>
	    	<Row>
	    		<Col className="button-wrapper" md={4}>
	    			<Button onClick={e => this.handleClick(7)}>7</Button>
	    		</Col>
	    		<Col className="button-wrapper" md={4}>
	    			<Button onClick={e => this.handleClick(8)}>8</Button>
	    		</Col>
	    		<Col className="button-wrapper" md={4}>
	    			<Button onClick={e => this.handleClick(9)}>9</Button>
	    		</Col>
	    	</Row>
	    	<Row>
	    		<Col className="button-wrapper" md={4}>
	    			<Button onClick={e => this.handleClick(0)}>0</Button>
	    		</Col>
	    		<Col className="button-wrapper cancel" md={4}>
	    			<Button onClick={e => this.handleClick('c')}>X</Button>
	    		</Col>
	    		<Col className="button-wrapper submit" md={4}>
	    			<Button onClick={e => this.handleClick('s')}>OK</Button>
	    		</Col>
	    	</Row>
	    </Container>
    );
  }
}

export default NumPad;
