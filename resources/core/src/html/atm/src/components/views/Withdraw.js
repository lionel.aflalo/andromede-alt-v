import React, { Component } from 'react';
import { Container, Row, Col, Button } from 'reactstrap';

import './Main.css';


class WithdrawView extends Component {

  constructor(props) {

    super(props);

    this.state = {
      accounts: null,
      account : null,
    };

  }

  componentDidMount() {
    this.fetchAccounts();
  }

  fetchAccounts() {

    const { parent } = this.props;

    fetch(parent.state.config.socket + '/api/atm/user.accounts/' + parent.state.user._id, {
      headers: {
        'Authorization' : 'Basic ' + btoa(parent.state.card.number + ':' + parent.state.card.token),
      }
    })
      .then(response => response.json())
      .then(accounts => {
        this.setState({accounts: accounts.filter(e => e.type === 'main')});
      })
    ;

  }

  withdraw(amount) {

    const { parent, t } = this.props;
    const formData   = {amount}

    fetch(parent.state.config.socket + '/api/atm/withdraw/' + this.state.account._id, {
      method: 'POST',
      headers: {
        'Authorization' : 'Basic ' + btoa(parent.state.card.number + ':' + parent.state.card.token),
        'Accept'        : 'application/json',
        'Content-Type'  : 'application/json'
      },
      body: JSON.stringify(formData)
    })
      .then(response => response.json())
      .then(data => {

        if(data.error) {
          window.parent.postMessage(JSON.stringify({action: 'notification', msg: t('withdraw_failed')}), '*');
        } else {
          parent.setState({cash: parent.state.cash - amount});
          window.parent.postMessage(JSON.stringify({action: 'notification', msg: t('withdraw_success', {amount})}), '*');
        }

      })
    ;

  }

  render() {

  	const { parent, t } = this.props;

    if(this.state.account) {

      const amounts = [10, 20, 50, 70, 100, 200];

      const elements = parent.generateViewElements([
        <Button className="atm-button">Solde: {this.state.account.money}$</Button>
      ].concat(amounts.map((amount, i) => (
        <Button className="atm-button" onClick={e => this.withdraw(amount)}>{amount}$</Button>
      ))).concat([
        <Button className="atm-button" onClick={e => parent.setCurrentView('main')}>{t('back')}</Button>,
      ]));

      return (
        <Container className="app-view view-main layout-buttons">
          {elements}
        </Container>
      );

    } else if(this.state.accounts) {

        const elements = parent.generateViewElements(this.state.accounts.map(account => {
          return (
            <Button className="atm-button" onClick={e => this.setState({account})}>
              {account.label.toUpperCase() + ' #' + account.number.toString().padStart(8, '0')}
            </Button>
          );
        }).concat([
          <Button className="atm-button" onClick={e => parent.setCurrentView('main')}>{t('back')}</Button>
        ]));

        return (
          <Container className="app-view app-view view-accountstatement layout-buttons">
            {elements}
          </Container>
        );

    } else return <div />

  }
}

export default WithdrawView;
