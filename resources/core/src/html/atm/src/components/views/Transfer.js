import React, { Component } from 'react';
import { Container, Row, Col, Button } from 'reactstrap';
import { Formik, Field, Form } from 'formik';
import SelectField from '../form-formik/SelectField';

import './Transfer.css';


class TransferView extends Component {

  constructor(props) {

    super(props);

    this.state = {
      accounts: null,
    };

  }

  componentDidMount() {
    this.fetchAccounts();
  }

  fetchAccounts() {

    const { parent } = this.props;

    fetch(parent.state.config.socket + '/api/atm/user.accounts/' + parent.state.user._id, {
      headers: {
        'Authorization' : 'Basic ' + btoa(parent.state.card.number + ':' + parent.state.card.token),
      }
    })
      .then(response => response.json())
      .then(accounts => {
        this.setState({accounts});
      })
    ;

  }

  transfer(sender, receiver, amount) {

    const { parent, t } = this.props;
    const formData   = {amount}

    fetch(parent.state.config.socket + '/api/atm/transfer/' + sender + '/' + receiver, {
      method: 'POST',
      headers: {
        'Authorization' : 'Basic ' + btoa(parent.state.card.number + ':' + parent.state.card.token),
        'Accept'        : 'application/json',
        'Content-Type'  : 'application/json'
      },
      body: JSON.stringify(formData)
    })
      .then(response => response.json())
      .then(data => {

        if(data.error)
          window.parent.postMessage(JSON.stringify({action: 'notification', msg: t('transfer_failed')}), '*');
        else
          window.parent.postMessage(JSON.stringify({action: 'notification', msg: t('transfer_success', {amount})}), '*');

      })
    ;

  }

  render() {

  	const { parent, t } = this.props;

    if(this.state.accounts) {

      const initialValues = {}

      const options = this.state.accounts.filter(e => e.type === 'main').map(e => ({value: e.number, label: '#' + e.number.toString().padStart(8, '0') + ' ' + e.label.toUpperCase()}));

      if(options.length > 0)
        initialValues.sender = options[0].value;
              
      return (
        <Container className="app-view view-transfer">
          <Container className="app-content">
            <Formik
              initialValues={initialValues}
              onSubmit={(values) => this.transfer(values.sender, values.receiver, values.amount)}
              render={() => (
                <Form className="form form--horizontal">
                  <div className="form__form-group">
                    <div className="form__form-group-field">
                      <span>{t('sender')}</span>
                      <Field
                        name="sender"
                        component={SelectField}
                        options={options}
                      />
                    </div>
                  </div>
                  <div className="form__form-group">
                    <div className="form__form-group-field">
                      <span>{t('receiver')}</span>
                      <Field name="receiver" type="string" placeholder={t('receiver')}/>
                    </div>
                  </div>
                  <div className="form__form-group">
                    <div className="form__form-group-field">
                      <span>{t('amount')}</span>
                      <Field name="amount" type="number" placeholder={t('amount')}/>
                    </div>
                  </div>
                  <div className="form__form-group">
                    <div className="form__form-group-field actions">
                      <Button className="atm-button" onClick={e => parent.setCurrentView('main')}>{t('back')}</Button>
                      <Button className="atm-button" type="submit" >{t('execute_transfer')}</Button>
                    </div>
                  </div>
                </Form>
              )}
            />
          </Container>
        </Container>
      );

    } else return <div/>;

  }
}

export default TransferView;
