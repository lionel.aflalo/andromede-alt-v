import React, { Component } from 'react';
import { Container, Modal, ModalBody, Row, Col, Button} from 'reactstrap';

import NumPad from '../NumPad';

import './ChangePin.css';

class ChangePinView extends Component {

  constructor(props) {

    super(props);

    this.state = {

    };

    this.numpad = null;

  }

  handleNumpad(val) {

    const { parent, t } = this.props;
    const formData      = {number: parent.state.cardNumber, pinCode: val}

    fetch(parent.state.config.socket + '/api/atm/changepin', {
      method: 'POST',
      headers: {
        'Accept': 'application/json',
        'Content-Type': 'application/json'
      },
      body: JSON.stringify(formData)
    })
      .then(response => response.json())
      .then(data => {
        parent.setCurrentView('main');
      })
    ;

  } 

  render() {

    const { parent, t } = this.props;
    
    return (
      <Container className="app-view view-changepin">
          <Modal isOpen={!this.state.user} className="numpad-modal">
            <ModalBody>
              <NumPad ref={e => this.numPad = e} parent={parent} onSubmit={val => this.handleNumpad(val)}/>
            </ModalBody>
          </Modal>
	    </Container>
    );
  }
}

export default ChangePinView;
