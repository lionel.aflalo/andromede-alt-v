import React, { Component } from 'react';
import { Container, Row, Col, Button } from 'reactstrap';

import './AccountStatement.css';


class AccountStatementView extends Component {

  constructor(props) {

    super(props);

    this.state = {
    	accounts    : null,
    	account     : null,
      transactions: null
    };

  }

  componentDidMount() {
    this.fetchAccounts();
  }

  fetchAccounts() {

  	const { parent } = this.props;

    fetch(parent.state.config.socket + '/api/atm/user.accounts/' + parent.state.user._id, {
      headers: {
        'Authorization' : 'Basic ' + btoa(parent.state.card.number + ':' + parent.state.card.token),
      }
    })
      .then(response => response.json())
      .then(accounts => {
        this.setState({accounts});
      })
    ;

  }

  fetchTransactions() {

    const { parent } = this.props;

    fetch(parent.state.config.socket + '/api/atm/transactions/' + this.state.account._id, {
      headers: {
        'Authorization' : 'Basic ' + btoa(parent.state.card.number + ':' + parent.state.card.token),
      }
    })
      .then(response => response.json())
      .then(transactions => {
        this.setState({transactions});
      })
    ;

  }

  printAccountStatement() {

    const { parent, t } = this.props;

    fetch(parent.state.config.socket + '/api/atm/print/accountstatement/' + this.state.account._id, {
      method: 'POST',
      headers: {
        'Authorization' : 'Basic ' + btoa(parent.state.card.number + ':' + parent.state.card.token),
      }
    })
      .then(response => response.json())
      .then(data => {
        window.parent.postMessage(JSON.stringify({action: 'notification', msg: t('print_success')}), '*');
      })
    ;

  }

  render() {

  	const { parent, t } = this.props;

  	if(this.state.accounts) {

  		if(this.state.account && this.state.transactions) {

        let transactions = this.state.transactions.slice(0);
        transactions.sort((a,b) => b.timestamp - a.timestamp);
        transactions = transactions.slice(0, 10);
        transactions.sort((a,b) => a.timestamp - b.timestamp)

        return (
          <Container className="app-view view-accountstatement">
            <Container className="app-content transaction-list">
              
              <Row>
                <Col md={12} className="actions">
                  10 {t('x_last_transactions')}
                </Col>
              </Row>

              {transactions.map((e,i) => {

                const date       = new Date(e.timestamp * 1000);
                const dateStr    = date.getDate() + '/' + (date.getMonth() + 1) + ' ' + (date.getHours() + 1).toString().padStart(2, '0') + ':' + (date.getMinutes() + 1).toString().padStart(2, '0');
                const isSender   = this.state.account._id === e.sender;
                const isReceiver = this.state.account._id === e.receiver;
                const isNegative = (isSender && e.amount >= 0) || (isReceiver && e.amount < 0);

                return (
                  <Row key={i} className="transaction">
                    <Col md={3} className="transaction-date">{dateStr}</Col>
                    <Col md={7} className="transaction-object">{e.object}</Col>
                    <Col md={2} className="transaction-amount" style={{color: isNegative ? '#F00' : '#0DBB19'}}>{isNegative ? '-' : '+'}{e.amount} $</Col>
                  </Row>
                );
              })}

              <Row key={transactions.length}>
                <Col md={3}></Col>
                <Col md={7}></Col>
                <Col md={2} className="account-money" style={{color: this.state.account.money >= 0 ? '#0DBB19' : '#F00'}}>{this.state.account.money >= 0 ? '+' : '-'}{this.state.account.money} $</Col>
              </Row>

              <Row>
                <Col md={12} className="actions">
                  <Button className="atm-button" onClick={e => this.setState({account: null, transactions: null})}>{t('back')}</Button>
                  <Button className="atm-button" onClick={e => this.printAccountStatement()}>{t('print')}</Button>
                </Col>
              </Row>

            </Container>
          </Container>
        );

  		} else {

        const elements = parent.generateViewElements(this.state.accounts.map(account => {
          return (
            <Button className="atm-button" onClick={e => this.setState({account}, () => this.fetchTransactions())}>
              {account.label.toUpperCase() + ' #' + account.number.toString().padStart(8, '0')}
            </Button>
          );
        }).concat([
          <Button className="atm-button" onClick={e => parent.setCurrentView('main')}>{t('back')}</Button>
        ]));

        return (
          <Container className="app-view app-view view-accountstatement layout-buttons">
            {elements}
          </Container>
        );

  		}

  	} else return <div />;

  }
}

export default AccountStatementView;
