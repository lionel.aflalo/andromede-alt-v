import React, { Component } from 'react';
import { Container, Row, Col, Button } from 'reactstrap';

import './Main.css';

class MainView extends Component {

  constructor(props) {

    super(props);

    this.state = {

    };

  }

  render() {

  	const { parent, t } = this.props;
  	
  	const elements = parent.generateViewElements([
			<Button className="atm-button atm-button-withdraw" onClick={e => parent.setCurrentView('withdraw')}>{t('withdraw')}</Button>,
			<Button className="atm-button" onClick={e => parent.setCurrentView('transfer')}>{t('transfer')}</Button>,
			<Button className="atm-button atm-button-quit" onClick={e => window.parent.postMessage(JSON.stringify({action: 'internal:destroy'}, '*'))}>{t('exit')}</Button>,
      <Button className="atm-button" onClick={e => parent.setCurrentView('deposit')}>{t('deposit')}</Button>,
      <Button className="atm-button" onClick={e => parent.setCurrentView('changepin')}>{t('changepin')}</Button>,
    ]);

    return (
      <Container className="app-view view-main layout-buttons">
        {elements}
	    </Container>
    );
  }
}

export default MainView;
