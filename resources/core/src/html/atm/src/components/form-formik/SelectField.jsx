import React from 'react'
import Select, { Option } from 'react-select'

import './SelectField.css';

const SelectField = ({options, field, form, disabled, onChange}) => (
  <Select
    className="react-select-container"
    classNamePrefix="react-select"
    options={options}
    name={field.name}
    value={options ? options.find(option => option.value === field.value) : ''}
    disabled={disabled || false}
    onChange={(option: Option) => {
    	
    	const val = option ? option.value : null;
    	
    	form.setFieldValue(field.name, val)

    	if(onChange)
    		onChange(val);

    }}
    onBlur={field.onBlur}
  />
);

export default SelectField;