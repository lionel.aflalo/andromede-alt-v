import React from 'react'
import DatePicker from 'react-datepicker';
import moment from 'moment';

const DatePickerField = ({field, form, disabled, onChange}) => (

  <div className="date-picker">
    <DatePicker
      className="form__form-group-datepicker"
      selected={moment(new Date(field.value * 1000))}
      disabled={disabled || false}
      dateFormat="LL"
      onBlur={field.onBlur}
      onChange={date => {
        
        form.setFieldValue(field.name, Math.floor(date / 1000))
        
        if(onChange)
          onChange(+date);
      }}
    />
  </div>
);

export default DatePickerField;