import HZEntity from './entity';

class HZArea extends HZEntity {

  static get _type() { return "Area"; }
  static get _subdocs() { return []; }

  get _subdocs() { return HZArea._subdocs; }
  get _id() { return this.data['_id']; }
  get _data() { return this.data['_data']; }
  get _type() { return this.data['_type']; }
  get name() { return this.data['name']; }
  get points() { return this.data['points']; }
  get tags() { return this.data['tags']; }

  set _id(val) { this.data['_id'] = val; }
  set _data(val) { this.data['_data'] = val; }
  set _type(val) { this.data['_type'] = val; }
  set name(val) { this.data['name'] = val; }
  set points(val) { this.data['points'] = val; }
  set tags(val) { this.data['tags'] = val; }

  get ttl() { return this.data['ttl']; }
  get _subdocs() { return this.data['_subdocs']; }
  get _typeChain() { return this.data['_typeChain']; }

  set ttl(val) { this.data['ttl'] = val; }
  set _subdocs(val) { this.data['_subdocs'] = val; }
  set _typeChain(val) { this.data['_typeChain'] = val; }

  constructor(data = {}) {

    super(data);

    this.data = {};

      /* Fields */
    this.data['_id'] = null;
    this.data['_data'] = {};
    this.data['_type'] = "Area";
    this.data['name'] = undefined;
    this.data['points'] = [];
    this.data['tags'] = [];
      /* Props */
    this.data['ttl'] = undefined;
    this.data['_subdocs'] = undefined;
    this.data['_typeChain'] = undefined;

    this.subdocs = [];

    for(let k in data)
      this.data[k] = data[k]

  }
}

export default HZArea;
