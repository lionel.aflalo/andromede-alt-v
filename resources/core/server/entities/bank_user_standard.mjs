import HZBankUser from './bank_user';

class HZStandardBankUser extends HZBankUser {

  static get _type() { return "StandardBankUser"; }
  static get _subdocs() { return []; }

  get _subdocs() { return HZStandardBankUser._subdocs; }
  get _id() { return this.data['_id']; }
  get _data() { return this.data['_data']; }
  get roles() { return this.data['roles']; }
  get _type() { return this.data['_type']; }
  get accounts() { return this.data['accounts']; }
  get token() { return this.data['token']; }
  get active() { return this.data['active']; }
  get residentNumber() { return this.data['residentNumber']; }
  get firstName() { return this.data['firstName']; }
  get lastName() { return this.data['lastName']; }
  get login() { return this.data['login']; }
  get password() { return this.data['password']; }

  set _id(val) { this.data['_id'] = val; }
  set _data(val) { this.data['_data'] = val; }
  set roles(val) { this.data['roles'] = val; }
  set _type(val) { this.data['_type'] = val; }
  set accounts(val) { this.data['accounts'] = val; }
  set token(val) { this.data['token'] = val; }
  set active(val) { this.data['active'] = val; }
  set residentNumber(val) { this.data['residentNumber'] = val; }
  set firstName(val) { this.data['firstName'] = val; }
  set lastName(val) { this.data['lastName'] = val; }
  set login(val) { this.data['login'] = val; }
  set password(val) { this.data['password'] = val; }

  get ttl() { return this.data['ttl']; }
  get _subdocs() { return this.data['_subdocs']; }
  get _typeChain() { return this.data['_typeChain']; }

  set ttl(val) { this.data['ttl'] = val; }
  set _subdocs(val) { this.data['_subdocs'] = val; }
  set _typeChain(val) { this.data['_typeChain'] = val; }

  constructor(data = {}) {

    super(data);

    this.data = {};

      /* Fields */
    this.data['_id'] = null;
    this.data['_data'] = {};
    this.data['roles'] = [];
    this.data['_type'] = "StandardBankUser";
    this.data['accounts'] = [];
    this.data['token'] = null;
    this.data['active'] = true;
    this.data['residentNumber'] = undefined;
    this.data['firstName'] = undefined;
    this.data['lastName'] = undefined;
    this.data['login'] = undefined;
    this.data['password'] = "KSejw2fOuh";
      /* Props */
    this.data['ttl'] = undefined;
    this.data['_subdocs'] = undefined;
    this.data['_typeChain'] = undefined;

    this.subdocs = [];

    for(let k in data)
      this.data[k] = data[k]

  }
}

export default HZStandardBankUser;
