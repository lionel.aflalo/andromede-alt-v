import HZAccount from './account';

class HZBankAccount extends HZAccount {

  static get _type() { return "BankAccount"; }
  static get _subdocs() { return []; }

  get _subdocs() { return HZBankAccount._subdocs; }
  get _id() { return this.data['_id']; }
  get _data() { return this.data['_data']; }
  get _type() { return this.data['_type']; }
  get limit() { return this.data['limit']; }
  get money() { return this.data['money']; }
  get type() { return this.data['type']; }
  get label() { return this.data['label']; }
  get number() { return this.data['number']; }
  get limits() { return this.data['limits']; }
  get active() { return this.data['active']; }

  set _id(val) { this.data['_id'] = val; }
  set _data(val) { this.data['_data'] = val; }
  set _type(val) { this.data['_type'] = val; }
  set limit(val) { this.data['limit'] = val; }
  set money(val) { this.data['money'] = val; }
  set type(val) { this.data['type'] = val; }
  set label(val) { this.data['label'] = val; }
  set number(val) { this.data['number'] = val; }
  set limits(val) { this.data['limits'] = val; }
  set active(val) { this.data['active'] = val; }

  get ttl() { return this.data['ttl']; }
  get _subdocs() { return this.data['_subdocs']; }
  get _typeChain() { return this.data['_typeChain']; }

  set ttl(val) { this.data['ttl'] = val; }
  set _subdocs(val) { this.data['_subdocs'] = val; }
  set _typeChain(val) { this.data['_typeChain'] = val; }

  constructor(data = {}) {

    super(data);

    this.data = {};

      /* Fields */
    this.data['_id'] = null;
    this.data['_data'] = {};
    this.data['_type'] = "BankAccount";
    this.data['limit'] = -1;
    this.data['money'] = 0;
    this.data['type'] = undefined;
    this.data['label'] = "";
    this.data['number'] = null;
    this.data['limits'] = undefined;
    this.data['active'] = true;
      /* Props */
    this.data['ttl'] = undefined;
    this.data['_subdocs'] = undefined;
    this.data['_typeChain'] = undefined;

    this.subdocs = [];

    for(let k in data)
      this.data[k] = data[k]

  }
}

export default HZBankAccount;
