import HZEnrolable from './enrolable';

class HZBankUser extends HZEnrolable {

  static get _type() { return "BankUser"; }
  static get _subdocs() { return []; }

  get _subdocs() { return HZBankUser._subdocs; }
  get _id() { return this.data['_id']; }
  get _data() { return this.data['_data']; }
  get roles() { return this.data['roles']; }
  get _type() { return this.data['_type']; }
  get accounts() { return this.data['accounts']; }
  get token() { return this.data['token']; }
  get active() { return this.data['active']; }
  get residentNumber() { return this.data['residentNumber']; }

  set _id(val) { this.data['_id'] = val; }
  set _data(val) { this.data['_data'] = val; }
  set roles(val) { this.data['roles'] = val; }
  set _type(val) { this.data['_type'] = val; }
  set accounts(val) { this.data['accounts'] = val; }
  set token(val) { this.data['token'] = val; }
  set active(val) { this.data['active'] = val; }
  set residentNumber(val) { this.data['residentNumber'] = val; }

  get ttl() { return this.data['ttl']; }
  get _subdocs() { return this.data['_subdocs']; }
  get _typeChain() { return this.data['_typeChain']; }

  set ttl(val) { this.data['ttl'] = val; }
  set _subdocs(val) { this.data['_subdocs'] = val; }
  set _typeChain(val) { this.data['_typeChain'] = val; }

  constructor(data = {}) {

    super(data);

    this.data = {};

      /* Fields */
    this.data['_id'] = null;
    this.data['_data'] = {};
    this.data['roles'] = [];
    this.data['_type'] = "BankUser";
    this.data['accounts'] = [];
    this.data['token'] = null;
    this.data['active'] = true;
    this.data['residentNumber'] = undefined;
      /* Props */
    this.data['ttl'] = undefined;
    this.data['_subdocs'] = undefined;
    this.data['_typeChain'] = undefined;

    this.subdocs = [];

    for(let k in data)
      this.data[k] = data[k]

  }
}

export default HZBankUser;
