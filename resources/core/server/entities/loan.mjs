import HZEntity from './entity';

class HZBankLoan extends HZEntity {

  static get _type() { return "BankLoan"; }
  static get _subdocs() { return []; }

  get _subdocs() { return HZBankLoan._subdocs; }
  get _id() { return this.data['_id']; }
  get _data() { return this.data['_data']; }
  get _type() { return this.data['_type']; }
  get receiver() { return this.data['receiver']; }
  get amount() { return this.data['amount']; }
  get remaining() { return this.data['remaining']; }
  get autoWire() { return this.data['autoWire']; }
  get formula() { return this.data['formula']; }
  get number() { return this.data['number']; }
  get active() { return this.data['active']; }

  set _id(val) { this.data['_id'] = val; }
  set _data(val) { this.data['_data'] = val; }
  set _type(val) { this.data['_type'] = val; }
  set receiver(val) { this.data['receiver'] = val; }
  set amount(val) { this.data['amount'] = val; }
  set remaining(val) { this.data['remaining'] = val; }
  set autoWire(val) { this.data['autoWire'] = val; }
  set formula(val) { this.data['formula'] = val; }
  set number(val) { this.data['number'] = val; }
  set active(val) { this.data['active'] = val; }

  constructor(data = {}) {

    super(data);

    this.data = {};

    this.data['_id'] = null;
    this.data['_data'] = {};
    this.data['_type'] = "BankLoan";
    this.data['receiver'] = undefined;
    this.data['amount'] = undefined;
    this.data['remaining'] = undefined;
    this.data['autoWire'] = undefined;
    this.data['formula'] = undefined;
    this.data['number'] = null;
    this.data['active'] = true;

    this.subdocs = [];

    for(let k in data)
      this.data[k] = data[k]

  }
}

export default HZBankLoan;
