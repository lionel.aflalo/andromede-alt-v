import HZEnrolable from './enrolable';

class HZPlayer extends HZEnrolable {

  static get _type() { return "Player"; }
  static get _subdocs() { return [{"name":"identities","type":"Identity","isArray":true},{"name":"identity","type":"Identity","isArray":false}]; }

  get _subdocs() { return HZPlayer._subdocs; }
  get _id() { return this.data['_id']; }
  get _data() { return this.data['_data']; }
  get roles() { return this.data['roles']; }
  get _type() { return this.data['_type']; }
  get name() { return this.data['name']; }
  get identifiers() { return this.data['identifiers']; }
  get password() { return this.data['password']; }
  get identities() { return this.data['identities']; }
  get identity() { return this.data['identity']; }

  set _id(val) { this.data['_id'] = val; }
  set _data(val) { this.data['_data'] = val; }
  set roles(val) { this.data['roles'] = val; }
  set _type(val) { this.data['_type'] = val; }
  set name(val) { this.data['name'] = val; }
  set identifiers(val) { this.data['identifiers'] = val; }
  set password(val) { this.data['password'] = val; }
  set identities(val) { this.data['identities'] = val; }
  set identity(val) { this.data['identity'] = val; }

  get ttl() { return this.data['ttl']; }
  get _subdocs() { return this.data['_subdocs']; }
  get _typeChain() { return this.data['_typeChain']; }

  set ttl(val) { this.data['ttl'] = val; }
  set _subdocs(val) { this.data['_subdocs'] = val; }
  set _typeChain(val) { this.data['_typeChain'] = val; }

  constructor(data = {}) {

    super(data);

    this.data = {};

      /* Fields */
    this.data['_id'] = null;
    this.data['_data'] = {};
    this.data['roles'] = [];
    this.data['_type'] = "Player";
    this.data['name'] = undefined;
    this.data['identifiers'] = undefined;
    this.data['password'] = null;
    this.data['identities'] = [];
    this.data['identity'] = null;
      /* Props */
    this.data['ttl'] = undefined;
    this.data['_subdocs'] = undefined;
    this.data['_typeChain'] = undefined;

    this.subdocs = [{"name":"identities","typeChain":["Identity"],"isArray":true},{"name":"identity","typeChain":["Identity"],"isArray":false}];

    for(let k in data)
      this.data[k] = data[k]

  }
}

export default HZPlayer;
