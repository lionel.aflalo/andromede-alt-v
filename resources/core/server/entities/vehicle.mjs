import HZItem from './item';

class HZVehicle extends HZItem {

  static get _type() { return "Vehicle"; }
  static get _subdocs() { return [{"name":"trunk","type":"TrunkContainer","isArray":false}]; }

  get _subdocs() { return HZVehicle._subdocs; }
  get _id() { return this.data['_id']; }
  get _data() { return this.data['_data']; }
  get _type() { return this.data['_type']; }
  get container() { return this.data['container']; }
  get name() { return this.data['name']; }
  get label() { return this.data['label']; }
  get count() { return this.data['count']; }
  get image() { return this.data['image']; }
  get shape() { return this.data['shape']; }
  get x() { return this.data['x']; }
  get y() { return this.data['y']; }
  get world() { return this.data['world']; }
  get lifetime() { return this.data['lifetime']; }
  get createTime() { return this.data['createTime']; }
  get trunk() { return this.data['trunk']; }
  get model() { return this.data['model']; }
  get pos() { return this.data['pos']; }
  get rot() { return this.data['rot']; }
  get mods() { return this.data['mods']; }
  get appearanceDataBase64() { return this.data['appearanceDataBase64']; }
  get damageStatusBase64() { return this.data['damageStatusBase64']; }
  get gamestateDataBase64() { return this.data['gamestateDataBase64']; }
  get scriptDataBase64() { return this.data['scriptDataBase64']; }
  get activeRadioStation() { return this.data['activeRadioStation']; }
  get bodyAdditionalHealth() { return this.data['bodyAdditionalHealth']; }
  get bodyHealth() { return this.data['bodyHealth']; }
  get customPrimaryColor() { return this.data['customPrimaryColor']; }
  get customSecondaryColor() { return this.data['customSecondaryColor']; }
  get customTires() { return this.data['customTires']; }
  get darkness() { return this.data['darkness']; }
  get dashboardColor() { return this.data['dashboardColor']; }
  get daylightOn() { return this.data['daylightOn']; }
  get dirtLevel() { return this.data['dirtLevel']; }
  get engineHealth() { return this.data['engineHealth']; }
  get engineOn() { return this.data['engineOn']; }
  get flamethrowerActive() { return this.data['flamethrowerActive']; }
  get handbrakeActive() { return this.data['handbrakeActive']; }
  get hasArmoredWindows() { return this.data['hasArmoredWindows']; }
  get headlightColor() { return this.data['headlightColor']; }
  get interiorColor() { return this.data['interiorColor']; }
  get lightsMultiplier() { return this.data['lightsMultiplier']; }
  get livery() { return this.data['livery']; }
  get lockState() { return this.data['lockState']; }
  get manualEngineControl() { return this.data['manualEngineControl']; }
  get modKit() { return this.data['modKit']; }
  get modKitsCount() { return this.data['modKitsCount']; }
  get neon() { return this.data['neon']; }
  get neonColor() { return this.data['neonColor']; }
  get nightlightOn() { return this.data['nightlightOn']; }
  get numberPlateIndex() { return this.data['numberPlateIndex']; }
  get numberPlateText() { return this.data['numberPlateText']; }
  get pearlColor() { return this.data['pearlColor']; }
  get petrolTankHealth() { return this.data['petrolTankHealth']; }
  get primaryColor() { return this.data['primaryColor']; }
  get repairsCount() { return this.data['repairsCount']; }
  get roofLivery() { return this.data['roofLivery']; }
  get roofOpened() { return this.data['roofOpened']; }
  get secondaryColor() { return this.data['secondaryColor']; }
  get sirenActive() { return this.data['sirenActive']; }
  get tireSmokeColor() { return this.data['tireSmokeColor']; }
  get wheelColor() { return this.data['wheelColor']; }
  get wheelsCount() { return this.data['wheelsCount']; }
  get windowTint() { return this.data['windowTint']; }

  set _id(val) { this.data['_id'] = val; }
  set _data(val) { this.data['_data'] = val; }
  set _type(val) { this.data['_type'] = val; }
  set container(val) { this.data['container'] = val; }
  set name(val) { this.data['name'] = val; }
  set label(val) { this.data['label'] = val; }
  set count(val) { this.data['count'] = val; }
  set image(val) { this.data['image'] = val; }
  set shape(val) { this.data['shape'] = val; }
  set x(val) { this.data['x'] = val; }
  set y(val) { this.data['y'] = val; }
  set world(val) { this.data['world'] = val; }
  set lifetime(val) { this.data['lifetime'] = val; }
  set createTime(val) { this.data['createTime'] = val; }
  set trunk(val) { this.data['trunk'] = val; }
  set model(val) { this.data['model'] = val; }
  set pos(val) { this.data['pos'] = val; }
  set rot(val) { this.data['rot'] = val; }
  set mods(val) { this.data['mods'] = val; }
  set appearanceDataBase64(val) { this.data['appearanceDataBase64'] = val; }
  set damageStatusBase64(val) { this.data['damageStatusBase64'] = val; }
  set gamestateDataBase64(val) { this.data['gamestateDataBase64'] = val; }
  set scriptDataBase64(val) { this.data['scriptDataBase64'] = val; }
  set activeRadioStation(val) { this.data['activeRadioStation'] = val; }
  set bodyAdditionalHealth(val) { this.data['bodyAdditionalHealth'] = val; }
  set bodyHealth(val) { this.data['bodyHealth'] = val; }
  set customPrimaryColor(val) { this.data['customPrimaryColor'] = val; }
  set customSecondaryColor(val) { this.data['customSecondaryColor'] = val; }
  set customTires(val) { this.data['customTires'] = val; }
  set darkness(val) { this.data['darkness'] = val; }
  set dashboardColor(val) { this.data['dashboardColor'] = val; }
  set daylightOn(val) { this.data['daylightOn'] = val; }
  set dirtLevel(val) { this.data['dirtLevel'] = val; }
  set engineHealth(val) { this.data['engineHealth'] = val; }
  set engineOn(val) { this.data['engineOn'] = val; }
  set flamethrowerActive(val) { this.data['flamethrowerActive'] = val; }
  set handbrakeActive(val) { this.data['handbrakeActive'] = val; }
  set hasArmoredWindows(val) { this.data['hasArmoredWindows'] = val; }
  set headlightColor(val) { this.data['headlightColor'] = val; }
  set interiorColor(val) { this.data['interiorColor'] = val; }
  set lightsMultiplier(val) { this.data['lightsMultiplier'] = val; }
  set livery(val) { this.data['livery'] = val; }
  set lockState(val) { this.data['lockState'] = val; }
  set manualEngineControl(val) { this.data['manualEngineControl'] = val; }
  set modKit(val) { this.data['modKit'] = val; }
  set modKitsCount(val) { this.data['modKitsCount'] = val; }
  set neon(val) { this.data['neon'] = val; }
  set neonColor(val) { this.data['neonColor'] = val; }
  set nightlightOn(val) { this.data['nightlightOn'] = val; }
  set numberPlateIndex(val) { this.data['numberPlateIndex'] = val; }
  set numberPlateText(val) { this.data['numberPlateText'] = val; }
  set pearlColor(val) { this.data['pearlColor'] = val; }
  set petrolTankHealth(val) { this.data['petrolTankHealth'] = val; }
  set primaryColor(val) { this.data['primaryColor'] = val; }
  set repairsCount(val) { this.data['repairsCount'] = val; }
  set roofLivery(val) { this.data['roofLivery'] = val; }
  set roofOpened(val) { this.data['roofOpened'] = val; }
  set secondaryColor(val) { this.data['secondaryColor'] = val; }
  set sirenActive(val) { this.data['sirenActive'] = val; }
  set tireSmokeColor(val) { this.data['tireSmokeColor'] = val; }
  set wheelColor(val) { this.data['wheelColor'] = val; }
  set wheelsCount(val) { this.data['wheelsCount'] = val; }
  set windowTint(val) { this.data['windowTint'] = val; }

  get ttl() { return this.data['ttl']; }
  get _subdocs() { return this.data['_subdocs']; }
  get _typeChain() { return this.data['_typeChain']; }
  get model() { return this.data['model']; }
  get usable() { return this.data['usable']; }
  get codeName() { return this.data['codeName']; }
  get variantHash() { return this.data['variantHash']; }
  get humanVariantHash() { return this.data['humanVariantHash']; }
  get actions() { return this.data['actions']; }
  get equip() { return this.data['equip']; }
  get limit() { return this.data['limit']; }
  get persist() { return this.data['persist']; }

  set ttl(val) { this.data['ttl'] = val; }
  set _subdocs(val) { this.data['_subdocs'] = val; }
  set _typeChain(val) { this.data['_typeChain'] = val; }
  set model(val) { this.data['model'] = val; }
  set usable(val) { this.data['usable'] = val; }
  set codeName(val) { this.data['codeName'] = val; }
  set variantHash(val) { this.data['variantHash'] = val; }
  set humanVariantHash(val) { this.data['humanVariantHash'] = val; }
  set actions(val) { this.data['actions'] = val; }
  set equip(val) { this.data['equip'] = val; }
  set limit(val) { this.data['limit'] = val; }
  set persist(val) { this.data['persist'] = val; }

  constructor(data = {}) {

    super(data);

    this.data = {};

      /* Fields */
    this.data['_id'] = null;
    this.data['_data'] = {};
    this.data['_type'] = "Vehicle";
    this.data['container'] = null;
    this.data['name'] = "genericvehicle";
    this.data['label'] = "Item";
    this.data['count'] = 1;
    this.data['image'] = "undefined";
    this.data['shape'] = {"grid":[{"x":-17,"y":-7}],"offset":{"x":17,"y":7}};
    this.data['x'] = 0;
    this.data['y'] = 0;
    this.data['world'] = null;
    this.data['lifetime'] = -1;
    this.data['createTime'] = undefined;
    this.data['trunk'] = undefined;
    this.data['model'] = undefined;
    this.data['pos'] = undefined;
    this.data['rot'] = undefined;
    this.data['mods'] = {};
    this.data['appearanceDataBase64'] = undefined;
    this.data['damageStatusBase64'] = undefined;
    this.data['gamestateDataBase64'] = undefined;
    this.data['scriptDataBase64'] = undefined;
    this.data['activeRadioStation'] = undefined;
    this.data['bodyAdditionalHealth'] = undefined;
    this.data['bodyHealth'] = undefined;
    this.data['customPrimaryColor'] = undefined;
    this.data['customSecondaryColor'] = undefined;
    this.data['customTires'] = undefined;
    this.data['darkness'] = undefined;
    this.data['dashboardColor'] = undefined;
    this.data['daylightOn'] = undefined;
    this.data['dirtLevel'] = undefined;
    this.data['engineHealth'] = undefined;
    this.data['engineOn'] = undefined;
    this.data['flamethrowerActive'] = undefined;
    this.data['handbrakeActive'] = undefined;
    this.data['hasArmoredWindows'] = undefined;
    this.data['headlightColor'] = undefined;
    this.data['interiorColor'] = undefined;
    this.data['lightsMultiplier'] = undefined;
    this.data['livery'] = undefined;
    this.data['lockState'] = undefined;
    this.data['manualEngineControl'] = undefined;
    this.data['modKit'] = undefined;
    this.data['modKitsCount'] = undefined;
    this.data['neon'] = undefined;
    this.data['neonColor'] = undefined;
    this.data['nightlightOn'] = undefined;
    this.data['numberPlateIndex'] = undefined;
    this.data['numberPlateText'] = undefined;
    this.data['pearlColor'] = undefined;
    this.data['petrolTankHealth'] = undefined;
    this.data['primaryColor'] = undefined;
    this.data['repairsCount'] = undefined;
    this.data['roofLivery'] = undefined;
    this.data['roofOpened'] = undefined;
    this.data['secondaryColor'] = undefined;
    this.data['sirenActive'] = undefined;
    this.data['tireSmokeColor'] = undefined;
    this.data['wheelColor'] = undefined;
    this.data['wheelsCount'] = undefined;
    this.data['windowTint'] = undefined;
      /* Props */
    this.data['ttl'] = undefined;
    this.data['_subdocs'] = undefined;
    this.data['_typeChain'] = undefined;
    this.data['model'] = undefined;
    this.data['usable'] = undefined;
    this.data['codeName'] = undefined;
    this.data['variantHash'] = undefined;
    this.data['humanVariantHash'] = undefined;
    this.data['actions'] = undefined;
    this.data['equip'] = undefined;
    this.data['limit'] = undefined;
    this.data['persist'] = undefined;

    this.subdocs = [{"name":"trunk","typeChain":["Item","Container","TrunkContainer"],"isArray":false}];

    for(let k in data)
      this.data[k] = data[k]

  }
}

export default HZVehicle;
