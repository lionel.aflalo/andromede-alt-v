const alt = require('alt');
const config = require('./config.json');
const Server = require('./server');

/*
try {

  const apm = require('elastic-apm-node').start({
    serviceName: 'altv-andromede',
    secretToken: '',
    serverUrl: ''
  })

} catch(e) {}
*/

let server;

process.on('uncaughtException', err => {

  if(server && server.logger)
    server.logger.error(`Unhandled exception\n${err.stack}`);
  else
    alt.logError(err);
});

process.on('unhandledRejection', (err, p) => {

  if(server && server.logger)
    server.logger.error(`Unhandled promise rejection\n${err.stack}`);
  else
    alt.logError(err);

});

server = new Server(config);

setImmediate(async () => {

  try {

    await server.start()

    alt.log('[core] started');

  } catch(e) { alt.logError(e.message, e.stack); }

});
