module.exports = (horizon) => {

  return {

    weapons           : require(__dirname + '../../data/weapons.json'),
    weapon_components : require(__dirname + '../../data/components.json'),

  }

}
