const alt = require('alt');
const fs  = require('fs');

module.exports = horizon => {

  const $                                  = horizon.$;
  const { joaat, toUnsigned, getDistance } = $.utils;
  const WEAPONS                            = horizon.requireESM('../src/lib/altv-api/common/data/weapons.json');

  const HZEntity       = horizon.require('entity');
  const HZItem         = horizon.require('item');
  const HZPlayer       = horizon.require('player');
  const HZIdentity     = horizon.require('identity');
  const HZVehicle      = horizon.require('vehicle');
  const HZItemKey      = horizon.require('itemkey');
  const HZItemBankCard = horizon.require('itembankcard');
  const HZSyncTime     = horizon.require('synctime');

  const config = {
    commands: {},
  };

  config.commands['car'] = async (player, model) => {

    let modelHash = 0;

    if(isNaN(model))
      modelHash = joaat(model);
    else
      modelHash = toUnsigned(parseInt(model, 10));

    const hzVehicle = await HZVehicle.create(model, player.pos, player.rot);

    hzVehicle.altVehicle.customPrimaryColor   = {r: 255, g: 255, b: 255};
    hzVehicle.altVehicle.customSecondaryColor = {r: 255, g: 255, b: 255}

    await $.utils.delay(400);

    alt.emitClient(player, 'hz:task:warpIntoVehicle', hzVehicle.altVehicle);

  }

  config.commands['dv'] = async (player, model) => {

    let closestVehicle  = null;
    let closestDistance = Infinity;

    for(vehicle of alt.Vehicle.all) {

      const distance = getDistance(player.pos, vehicle.pos);

      if(distance <= closestDistance) {
        closestVehicle  = vehicle;
        closestDistance = distance;
      }

    }

    if(closestVehicle !== null) {

      const _idStr = closestVehicle.getSyncedMeta('_id');

      if(_idStr) {

        const hzVehicle = await HZVehicle.findOne({_id: HZVehicle.id(_idStr)});

        if(hzVehicle)
          await hzVehicle.delete();

      }

      closestVehicle.destroy();
    }

  }

  config.commands['time'] = (player, hours, minutes = '0') => {

    hours      = parseInt(hours,   10);
    minutes    = parseInt(minutes, 10);
    const date = new Date(HZSyncTime.time * 1000);

    date.setHours(hours);
    date.setMinutes(minutes);

    HZSyncTime.time = Math.floor(+date / 1000);

    alt.Player.all.forEach(e => alt.emitClient(e, 'hz:time:set', HZSyncTime.time, HZSyncTime.MS_PER_GAME_MINUTE));

  }

  config.commands['genentities'] = (player, ...args) => {
    
    const allFields = HZEntity.fields;
    const allProps  = HZEntity.props;

    let wrap = ``;

    const modules = [];

    for(const m in horizon.modules) {

      const mn = m.split('/').join('_');

      if(HZEntity.prototype.isPrototypeOf(horizon.modules[m].prototype)) {

        const mod    = horizon.modules[m];
        const fields = allFields[mod.prototype.constructor.name];
        const props  = allProps[mod.prototype.constructor.name] || [];
        const ctor   = Object.getPrototypeOf(mod.prototype).constructor.name;

        let parent;

        for(let k in horizon.modules)
          if(horizon.modules[k].prototype && (horizon.modules[k].prototype.constructor.name === ctor))
            parent = k.split('/').join('_');

        modules.push(mod.prototype.constructor.name);

        let code = `import ${ctor} from './${parent}';\n\n`;

        code += `class ${mod.prototype.constructor.name} extends ${ctor} {\n\n`;

        wrap += `import ${mod.prototype.constructor.name} from './${mn}';\n`;

        code += `  static get _type() { return ${JSON.stringify(mod._type)}; }\n`

        code += `  static get _subdocs() { return ${JSON.stringify(mod.subdocs.map(e => ({
          name      : e.name,
          type      : mod.fields[e.type.prototype.constructor.name]._type.options.default,
          typeChain : e._typeChain,
          isArray   : e.isArray
        })))}; }\n\n`;

        code += `  get _subdocs() { return ${mod.prototype.constructor.name}._subdocs; }\n`;

        for(let f in fields) {
          code += `  get ${f}() { return this.data['${f}']; }\n`;
        }

        code += `\n`;
      
        for(let f in fields) {
          code += `  set ${f}(val) { this.data['${f}'] = val; }\n`;
        }

        code += `\n`;

        for(let i=0; i<props.length; i++) {
          const p = props[i].name;
          code += `  get ${p}() { return this.data['${p}']; }\n`;
        }

        code += `\n`;

        for(let i=0; i<props.length; i++) {
          const p = props[i].name;
          code += `  set ${p}(val) { this.data['${p}'] = val; }\n`;
        }

        code += `\n`;

        code += `  constructor(data = {}) {\n\n`;
        code += `    super(data);\n\n`;
        code += `    this.data = {};\n\n`;


        code += `      /* Fields */\n`;   
        for(let f in fields) {
          code += `    this.data['${f}'] = ${JSON.stringify(fields[f].options.default)};\n`;
        }

        code += `      /* Props */\n`;
        for(let i=0; i<props.length; i++) {

          const p = props[i].name;
          code   += `    this.data['${p}'] = ${JSON.stringify(this[p])};\n`;
        }

        code += `\n`;
        code += `    this.subdocs = ${JSON.stringify(mod.subdocs)};\n\n`;
        code += `    for(let k in data)\n`;
        code += `      this.data[k] = data[k]\n\n`;
        code += `  }\n`;
        code += `}\n\n`;

        code += `export default ${mod.prototype.constructor.name};\n`;

        fs.writeFileSync(__dirname + '/../entities/' + mn + '.mjs', code);
      }
    }

    wrap += `export { ${modules.join(', ')} };`;

    fs.writeFileSync(__dirname + '/../entities/index.mjs', wrap);

    console.log('done generating entities');

  }

  config.commands['genitems'] = (player, ...args) => {

    const items = fs.readdirSync(__dirname + '/../data/img/items').filter(e => e.endsWith('.json')).map(e => e.replace('.json', ''));
    
    for(let i=0; i<items.length; i++) {

      try {

        const item           = items[i];
        const shape          = JSON.parse(fs.readFileSync(__dirname + '/../data/img/items/' + item + '.json').toString());
        const firstCharUpper = item[0].toUpperCase() + item.substr(1);
        const moduleName     = 'HZItem' + firstCharUpper;
        const dirname        = __dirname + '/../items/item' + item;
        const filename       = dirname + '/index.js';
  
        console.log(item);

        let code =
  `module.exports = (horizon, config) => {
     
    const HZItem = horizon.require('item');
  
    class ${moduleName} extends HZItem {
  
    }
  
    ${moduleName}
      .inheritFields(HZItem)
      .inheritActions(HZItem)
      .field('_type', {default: 'Item${firstCharUpper}'})
      .field('image', {default: '${item}'})
      .field('shape', {default: ${JSON.stringify(shape)}})
      .field('name',  {default: 'item${item}'})
      .field('label', {default: '${firstCharUpper}'})
    ;
  
    return ${moduleName};
  
  }`;
  
        if(!fs.existsSync(dirname))
          fs.mkdirSync(dirname);
  
        fs.writeFileSync(filename, code);

      } catch(e) {
        console.error(e.message);
      }

    }

  }

  config.commands['give'] = async (player, name, count = 1, ...args) => {

    count = parseInt(count, 10);

    const data  = {};

    for(let i=0; i<args.length; i += 2) {
      const val = isNan(args[i + 1]) ? args[i + 1] : parseInt(args[i + 1], 10);
      data[args[i]] = val;
    }

    const mod = horizon.modules[name] || {};

    if(HZItem.prototype.isPrototypeOf(mod.prototype)) {

      const hzPlayer   = await HZPlayer.findOne({_id: HZPlayer.id(player.getSyncedMeta('player'))});
      const hzIdentity = await HZIdentity.findOne({_id: HZIdentity.id(player.getSyncedMeta('identity'))})

      console.log(hzPlayer.name + ' ' + '(' + hzPlayer._idStr + ') give ' + name + ' ' + count + ' => ' + JSON.stringify(data));

      const item   = new mod(data);
      item.count   = count;
      const result = hzIdentity.getFreeSpace(item);

      if(result) {

        if(item.initable)
          await item.init();

        item.x = result.x;
        item.y = result.y;

        result.container.items.push(item);

        result.container.cache(true);
        await item.save();

        horizon.updateCache(player, result.container._idStr);

        result.container.save();
        
      }

    }

  }

  config.commands['vkey'] = async (player, name, ...args) => {

    const vehicles = alt.Vehicle.all;
    let vehicle    = null;
    let distance   = Infinity;

    for(let i=0; i<vehicles.length; i++) {

      const _vehicle  = vehicles[i];
      const _distance = horizon.$.utils.getDistance(player.pos, _vehicle.pos);

      if(_distance < distance) {
        vehicle  = _vehicle;
        distance = _distance;
      }

    }

    if(vehicle) {

      const hzIdentity = await HZIdentity.findOne({_id: HZIdentity.id(player.getSyncedMeta('identity'))});

      if(!hzIdentity)
        return;
      
      const key = new HZItemKey({targets: [{type: 'vehicle', model: vehicle.model, plate: vehicle.numberPlateText}]});
      const result = hzIdentity.getFreeSpace(key);

      if(result) {

        key.x = result.x;
        key.y = result.y;

        result.container.items.push(key);

        result.container.cache(true);
        await key.save();

        horizon.updateCache(player, result.container._idStr);

        result.container.save();
        
      }

    }

  }

  return config;

}