module.exports = horizon => {

  const HZIdentityBaseContainer      = horizon.require('identitybasecontainer');
  const HZIdentityComponentContainer = horizon.require('identitycomponentcontainer');
  const HZIdentityPropContainer      = horizon.require('identitypropcontainer');

  const config = {

    containers: [
      HZIdentityBaseContainer,
      HZIdentityComponentContainer,
      HZIdentityPropContainer
    ],

  }

  return config;

}
