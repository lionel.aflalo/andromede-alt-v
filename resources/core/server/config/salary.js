module.exports = horizon => {

  const config = {};

  config.interval = 30 * 60 * 1000;

  config.salaries = {

    'sdf': 0,
    
    'rsa' : 30,

    'mechanic:boss'            : 225,  // patron
    'mechanic:workshop_leader' : 195,  // chef d'atelier
    'mechanic:mechanic'        : 165,  // mecano
    'mechanic:apprentice'      : 135,  // apprenti
    
    'grocer:boss'              : 225,  // patron
    'grocer:manager'           : 195,  // chef d'atelier
    'grocer:permanent'         : 165,  // mecano
    'grocer:temporary'         : 135,  // apprenti

    'lspd:commander'           : 315, // commandant
    'lspd:captain'             : 285, // capitaine
    'lspd:lieutenant'          : 255, // lieutenant
    // 'lspd:sergeantchief2'      : 75,  // sergeant-chef 2
    'lspd:sergeantchief1'      : 225,  // sergeant-chef 1
    // 'lspd:sergeant3'           : 58,  // sergeant 3
    // 'lspd:sergeant2'           : 54,  // sergeant 2
    'lspd:sergeant1'           : 195,  // sergeant 1
    // 'lspd:officier3'           : 38,  // officier 3
    // 'lspd:officer2'            : 34,  // officier 2
    'lspd:officer1'            : 165,  // officier 1
    'lspd:cadet'               : 135,  // cadet

    'bcsd:sheriff'             : 285, // sheriff
    'bcsd:deputysheriff'       : 255,  // sheriff adjoint
    // 'bcsd:deputychief2'        : 75,  // sheriff suppléant 2
    'bcsd:deputychief1'        : 225,  // sheriff suppléant 1
    // 'bcsd:delegate2'           : 58,  // sheriff délégué 2
    // 'bcsd:delegate1'           : 90,  // sheriff délégué 1
    'bcsd:deputy'              : 195,  // député
    // 'bcsd:ranger3'             : 38,  // ranger 3
    // 'bcsd:ranger2'             : 34,  // ranger 2
    'bcsd:ranger1'             : 165,  // ranger 1
    'bcsd:cadet'               : 135,  // cadet

    // 'ems:coordinator'          : 135, // coordinateur
    'ems:director'             : 285, // directeur
    'ems:specialistdoctor'     : 255,  // médecin specialiste/chirurgien
    // 'ems:medicine'             : 70,  // médicine
    'ems:urgentist'            : 225, // urgentiste
    'ems:internal'             : 195,  // interne
    'ems:nurse'                : 165,  // infirnmier
    'ems:ambulancer'           : 135  // ambulancier

  };

  return config;
}