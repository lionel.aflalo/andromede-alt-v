module.exports = horizon => {

  const $ = horizon.$;

  const config = {

    earnings: {
      simple  : 35,
      half    : 17,
      triple  : 11,
      quarter : 8,
      basket  : 6,
      trans   : 5,
      dozen   : 2,
      column  : 2,
      red     : 1,
      black   : 1,
      even    : 1,
      odd     : 1,
      _1to18  : 1,
      _19to36 : 1,
    },

    earningNames: {
      simple  : 'Simple',
      half    : 'Double',
      triple  : 'Triple',
      quarter : 'Carré',
      basket  : 'Panier',
      trans   : 'Transversale',
      dozen   : 'Douzaine',
      column  : 'Colonne',
      red     : 'Rouges',
      black   : 'Noirs',
      even    : 'Pair',
      odd     : 'Impair',
      _1to18  : 'Manque',
      _19to36 : 'Passe',
    },

    chips: [
      {type: 'simple', numbers: ['0']},
      {type: 'simple', numbers: ['00']},
      {type: 'simple', numbers: ['1']},
      {type: 'simple', numbers: ['2']},
      {type: 'simple', numbers: ['3']},
      {type: 'simple', numbers: ['4']},
      {type: 'simple', numbers: ['5']},
      {type: 'simple', numbers: ['6']},
      {type: 'simple', numbers: ['7']},
      {type: 'simple', numbers: ['8']},
      {type: 'simple', numbers: ['9']},
      {type: 'simple', numbers: ['10']},
      {type: 'simple', numbers: ['11']},
      {type: 'simple', numbers: ['12']},
      {type: 'simple', numbers: ['13']},
      {type: 'simple', numbers: ['14']},
      {type: 'simple', numbers: ['15']},
      {type: 'simple', numbers: ['16']},
      {type: 'simple', numbers: ['17']},
      {type: 'simple', numbers: ['18']},
      {type: 'simple', numbers: ['19']},
      {type: 'simple', numbers: ['20']},
      {type: 'simple', numbers: ['21']},
      {type: 'simple', numbers: ['22']},
      {type: 'simple', numbers: ['23']},
      {type: 'simple', numbers: ['24']},
      {type: 'simple', numbers: ['25']},
      {type: 'simple', numbers: ['26']},
      {type: 'simple', numbers: ['27']},
      {type: 'simple', numbers: ['28']},
      {type: 'simple', numbers: ['29']},
      {type: 'simple', numbers: ['30']},
      {type: 'simple', numbers: ['31']},
      {type: 'simple', numbers: ['32']},
      {type: 'simple', numbers: ['33']},
      {type: 'simple', numbers: ['34']},
      {type: 'simple', numbers: ['35']},
      {type: 'simple', numbers: ['36']},
      {type: 'half', numbers: ['0'    ,'00']},
      {type: 'half', numbers: ['1'    ,'2']},
      {type: 'half', numbers: ['2'    ,'3']},
      {type: 'half', numbers: ['4'    ,'5']},
      {type: 'half', numbers: ['5'    ,'6']},
      {type: 'half', numbers: ['7'    ,'8']},
      {type: 'half', numbers: ['8'    ,'9']},
      {type: 'half', numbers: ['10'   ,'11']},
      {type: 'half', numbers: ['11'   ,'12']},
      {type: 'half', numbers: ['13'   ,'14']},
      {type: 'half', numbers: ['14'   ,'15']},
      {type: 'half', numbers: ['16'   ,'17']},
      {type: 'half', numbers: ['17'   ,'18']},
      {type: 'half', numbers: ['19'   ,'20']},
      {type: 'half', numbers: ['20'   ,'21']},
      {type: 'half', numbers: ['22'   ,'23']},
      {type: 'half', numbers: ['23'   ,'24']},
      {type: 'half', numbers: ['25'   ,'26']},
      {type: 'half', numbers: ['26'   ,'27']},
      {type: 'half', numbers: ['28'   ,'29']},
      {type: 'half', numbers: ['29'   ,'30']},
      {type: 'half', numbers: ['31'   ,'32']},
      {type: 'half', numbers: ['32'   ,'33']},
      {type: 'half', numbers: ['34'   ,'35']},
      {type: 'half', numbers: ['35'   ,'36']},
      {type: 'half', numbers: ['1'    ,'4']},
      {type: 'half', numbers: ['2'    ,'5']},
      {type: 'half', numbers: ['3'    ,'6']},
      {type: 'half', numbers: ['4'    ,'7']},
      {type: 'half', numbers: ['5'    ,'8']},
      {type: 'half', numbers: ['6'    ,'9']},
      {type: 'half', numbers: ['7'    ,'10']},
      {type: 'half', numbers: ['8'    ,'11']},
      {type: 'half', numbers: ['9'    ,'12']},
      {type: 'half', numbers: ['10'   ,'13']},
      {type: 'half', numbers: ['11'   ,'14']},
      {type: 'half', numbers: ['12'   ,'15']},
      {type: 'half', numbers: ['13'   ,'16']},
      {type: 'half', numbers: ['14'   ,'17']},
      {type: 'half', numbers: ['15'   ,'18']},
      {type: 'half', numbers: ['16'   ,'19']},
      {type: 'half', numbers: ['17'   ,'20']},
      {type: 'half', numbers: ['18'   ,'21']},
      {type: 'half', numbers: ['19'   ,'22']},
      {type: 'half', numbers: ['20'   ,'23']},
      {type: 'half', numbers: ['21'   ,'24']},
      {type: 'half', numbers: ['22'   ,'25']},
      {type: 'half', numbers: ['23'   ,'26']},
      {type: 'half', numbers: ['24'   ,'27']},
      {type: 'half', numbers: ['25'   ,'28']},
      {type: 'half', numbers: ['26'   ,'29']},
      {type: 'half', numbers: ['27'   ,'30']},
      {type: 'half', numbers: ['28'   ,'31']},
      {type: 'half', numbers: ['29'   ,'32']},
      {type: 'half', numbers: ['30'   ,'33']},
      {type: 'half', numbers: ['31'   ,'34']},
      {type: 'half', numbers: ['32'   ,'35']},
      {type: 'half', numbers: ['33'   ,'36']},
      {type: 'triple', numbers: ['1'    ,'2'    ,'3']},
      {type: 'triple', numbers: ['4'    ,'5'    ,'6']},
      {type: 'triple', numbers: ['7'    ,'8'    ,'9']},
      {type: 'triple', numbers: ['10'   ,'11'   ,'12']},
      {type: 'triple', numbers: ['13'   ,'14'   ,'15']},
      {type: 'triple', numbers: ['16'   ,'17'   ,'18']},
      {type: 'triple', numbers: ['19'   ,'20'   ,'21']},
      {type: 'triple', numbers: ['22'   ,'23'   ,'24']},
      {type: 'triple', numbers: ['25'   ,'26'   ,'27']},
      {type: 'triple', numbers: ['28'   ,'29'   ,'30']},
      {type: 'triple', numbers: ['31'   ,'32'   ,'33']},
      {type: 'triple', numbers: ['34'   ,'35'   ,'36']},
      {type: 'quarter', numbers: ['1'    ,'2'    ,'4'    ,'5']},
      {type: 'quarter', numbers: ['2'    ,'3'    ,'5'    ,'6']},
      {type: 'quarter', numbers: ['4'    ,'5'    ,'7'    ,'8']},
      {type: 'quarter', numbers: ['5'    ,'6'    ,'8'    ,'9']},
      {type: 'quarter', numbers: ['7'    ,'8'    ,'10'   ,'11']},
      {type: 'quarter', numbers: ['8'    ,'9'    ,'11'   ,'12']},
      {type: 'quarter', numbers: ['10'   ,'11'   ,'13'   ,'14']},
      {type: 'quarter', numbers: ['11'   ,'12'   ,'14'   ,'15']},
      {type: 'quarter', numbers: ['13'   ,'14'   ,'16'   ,'17']},
      {type: 'quarter', numbers: ['14'   ,'15'   ,'17'   ,'18']},
      {type: 'quarter', numbers: ['16'   ,'17'   ,'19'   ,'20']},
      {type: 'quarter', numbers: ['17'   ,'18'   ,'20'   ,'21']},
      {type: 'quarter', numbers: ['19'   ,'20'   ,'22'   ,'23']},
      {type: 'quarter', numbers: ['20'   ,'21'   ,'23'   ,'24']},
      {type: 'quarter', numbers: ['22'   ,'23'   ,'25'   ,'26']},
      {type: 'quarter', numbers: ['23'   ,'24'   ,'26'   ,'27']},
      {type: 'quarter', numbers: ['25'   ,'26'   ,'28'   ,'29']},
      {type: 'quarter', numbers: ['26'   ,'27'   ,'29'   ,'30']},
      {type: 'quarter', numbers: ['28'   ,'29'   ,'31'   ,'32']},
      {type: 'quarter', numbers: ['29'   ,'30'   ,'32'   ,'33']},
      {type: 'quarter', numbers: ['31'   ,'32'   ,'34'   ,'35']},
      {type: 'quarter', numbers: ['32'   ,'33'   ,'35'   ,'36']},
      {type: 'basket', numbers: ['0'    ,'00'   ,'1'    ,'2'    ,'3']},
      {type: 'trans', numbers: ['1'    ,'2'    ,'3'    ,'4'    ,'5'   ,'6']},
      {type: 'trans', numbers: ['4'    ,'5'    ,'6'    ,'7'    ,'8'   ,'9']},
      {type: 'trans', numbers: ['7'    ,'8'    ,'9'    ,'10'   ,'11'  ,'12']},
      {type: 'trans', numbers: ['10'   ,'11'   ,'12'   ,'13'   ,'14'  ,'15']},
      {type: 'trans', numbers: ['13'   ,'14'   ,'15'   ,'16'   ,'17'  ,'18']},
      {type: 'trans', numbers: ['16'   ,'17'   ,'18'   ,'19'   ,'20'  ,'21']},
      {type: 'trans', numbers: ['19'   ,'20'   ,'21'   ,'22'   ,'23'  ,'24']},
      {type: 'trans', numbers: ['22'   ,'23'   ,'24'   ,'25'   ,'26'  ,'27']},
      {type: 'trans', numbers: ['25'   ,'26'   ,'27'   ,'28'   ,'29'  ,'30']},
      {type: 'trans', numbers: ['28'   ,'29'   ,'30'   ,'31'   ,'32'  ,'33']},
      {type: 'trans', numbers: ['31'   ,'32'   ,'33'   ,'34'   ,'35'  ,'36']},
      {type: 'dozen', numbers: ['1'    ,'2'    ,'3'    ,'4'    ,'5'   ,'6'    ,'7'  ,'8'  ,'9'  ,'10' ,'11' ,'12']}, //1st12
      {type: 'dozen', numbers: ['13'   ,'14'   ,'15'   ,'16'   ,'17'  ,'18'   ,'19' ,'20' ,'21' ,'22' ,'23' ,'24']},//2nd12
      {type: 'dozen', numbers: ['25'   ,'26'   ,'27'   ,'28'   ,'29'  ,'30'   ,'31' ,'32' ,'33' ,'34' ,'35' ,'36']},//3rd12
      {type: 'dozen', numbers: ['1'    ,'4'    ,'7'    ,'10'   ,'13'  ,'16'   ,'19' ,'22' ,'25' ,'28' ,'31' ,'34']},//2to1
      {type: 'dozen', numbers: ['2'    ,'5'    ,'8'    ,'11'   ,'14'  ,'17'   ,'20' ,'23' ,'26' ,'29' ,'32' ,'35']},//2to1
      {type: 'dozen', numbers: ['3'    ,'6'    ,'9'    ,'12'   ,'15'  ,'18'   ,'21' ,'24' ,'27' ,'30' ,'33' ,'36']},//2to1
      {type: '_1to18', numbers: ['1'    ,'2'    ,'3'    ,'4'    ,'5'   ,'6'    ,'7'  ,'8'  ,'9'  ,'10' ,'11' ,'12'   ,'13'     ,'14' ,'15' ,'16' ,'17' ,'18']},//1-18
      {type: 'even', numbers: ['2'    ,'4'    ,'6'    ,'8'    ,'10'  ,'12'   ,'14' ,'16' ,'18' ,'20' ,'22' ,'24'   ,'26'     ,'28' ,'30' ,'32' ,'34' ,'36']}, //even
      {type: 'red', numbers: ['1'    ,'3'    ,'5'    ,'7'    ,'9'   ,'12'   ,'14' ,'16' ,'18' ,'19' ,'21' ,'23'   ,'25'     ,'27' ,'30' ,'32' ,'34' ,'36']},//red
      {type: 'black', numbers: ['2'    ,'4'    ,'6'    ,'8'    ,'10'  ,'11'   ,'13' ,'15' ,'17' ,'20' ,'22' ,'24'   ,'26'     ,'28' ,'29' ,'31' ,'33' ,'35']},//black
      {type: 'odd', numbers: ['1'    ,'3'    ,'5'    ,'7'    ,'9'   ,'11'   ,'13' ,'15' ,'17' ,'19' ,'21' ,'23'   ,'25'     ,'27' ,'29' ,'31' ,'33' ,'35']},//odd
      {type: '_19to36', numbers: ['19'   ,'20'   ,'21'   ,'22'   ,'23'  ,'24'   ,'25' ,'26' ,'27' ,'28' ,'29' ,'30'   ,'31'     ,'32' ,'33' ,'34' ,'35' ,'36']},//19-36
    ],
    
    wheel: {
      numbers: ['25', '29', '12', '8', '19', '31', '18', '6', '21', '33', '16', '4', '23', '35', '14', '2', '0', '28', '9', '26', '30', '11', '7', '20', '32', '17', '5', '22', '34', '15', '3', '24', '36', '13', '1', '00', '27', '10']
    },

  };

  return config;
}