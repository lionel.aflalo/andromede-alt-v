module.exports = (horizon) => {

	const HZBankUser          = horizon.require('bank/user');
	const HZBankAccount       = horizon.require('bank/account');
	const HZStandardBankUser  = horizon.require('bank/user/standard');
	const HZProBankUser       = horizon.require('bank/user/pro');
	const HZBankAccountLimits = horizon.require('bank/account/limits');

	const HZBankConfig = {
		default: [
    ],
	};

	return HZBankConfig;

}