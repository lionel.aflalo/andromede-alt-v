'use strict'

require = require('esm')(module, {mode: 'strict'});

const alt          = require('alt');
const EventEmitter = require('events');
const Stream       = require('stream');
const fs           = require('fs');
const http         = require('http');
const SocketIO     = require('socket.io');
const pino         = require('pino');
const express      = require('express');
const cors         = require('cors');
const bodyParser   = require('body-parser')
const MongoClient  = require('mongodb').MongoClient;
const ObjectID     = require('mongodb').ObjectID;
const async        = require('async');
const { spawn }    = require('child_process');
const cwd          = process.cwd();
const { env }      = process;
const Topo         = require('topo');
const $            = require('../src/lib/altv-api/server/index').default;

class Server extends EventEmitter {

  constructor(config) {

    super();

    this.$            = $;
    this.requireESM   = require;
    this.config       = config;
    this.app          = null;
    this.http         = null;
    this.io           = null;
    this.db           = null;
    this.logger       = null;
    this.modules      = {};
    this.cacheUpdates = new Map();
  }

  bindEvents() {

    alt.log('[core] bindEvents');

    // Database
    this.on('db.connect', (database) => {
      this.logger.info('connected to database');
    });

    // HTTP
    this.on('http.listen', (port) => {
      this.logger.info(`server listening on port ${port}`);
    });

    // Modules
    this.on('module.load.success', (name) => {
      this.logger.info(`[${name}] loaded`);
    });

    this.on('module.load.error', (name, err) => {
      this.logger.error(`module [${name}] load error : ${err.message}\n${err.stack}`);
    });

    this.on('module.init.success', (name) => {
      this.logger.info(`[${name}] initialized`);
    });

    this.on('module.init.error', (name, err) => {
      this.logger.error(`module [${name}] init error : ${err.message}\n${err.stack}`);
    });

    this.on('modules.loaded', (name) => {

      this.logger.info('loaded all modules');

      if((process.argv.length > 2) && (process.argv[2] == 'entities')) {

        this.logger.info('Generating entities');

      }

    });

    $.registerCallback('hz:entity:get', async (player, cb, _type, _id) => {
      
      // console.log(player.id, _type, _id);

      let mod = null;

      for(let k in this.modules)
        if(this.modules.entity.prototype.isPrototypeOf(this.modules[k].prototype) && (this.modules[k]._type === _type))
          mod = this.modules[k];

      if(mod) {
        
        const entity = await mod.findOneOfType({_id: mod.id(_id)});
        cb(entity ? entity.serialize(player) : null);
      
      } else cb(null);

    });
    

  }

  dbConnect() {

    alt.log('[core] db connect');

    return new Promise((resolve, reject) => {
      
      MongoClient.connect(this.config.mongoConnectionString, {useNewUrlParser: true}, (err, db) => {

        if(err) {
          reject(err);
          return;
        }

        this.db = db.db(this.config.mongoDatabase);

        this.emit('db.connect', this.config.mongoDatabase);

        resolve();

      });

    });
  }

  async init() {
   
    alt.log('[core] init');

    this.app  = express();

    this.app.use(cors());
    this.app.use('/static', express.static(__dirname + '/www'));
    this.app.use(bodyParser.json({limit: '50mb'}));
    this.app.use(bodyParser.urlencoded({limit: '50mb', extended: true}));

    this.http = http.Server(this.app);
    this.io   = SocketIO(this.http);

    this.setMaxListeners(0);
    this.bindEvents();
    this.initLogger();
    
    await this.dbConnect();
    await this.loadModules();

    this.initCacheUpdate();
  }

  initLogger() {

    alt.log('[core] init logger');

    this.logThrough = new Stream.PassThrough();
    this.logger     = pino({name: 'altv-core', prettyPrint: false}, this.logThrough);

    /*
    this.logger = {
      info:  () => {},
      error: () => {},
    };
    */


    const child = spawn(`${cwd}/node.exe`, [
      require.resolve('pino-tee'),
      'info',  `${this.config.logPath}/info.log`,
      'warn',  `${this.config.logPath}/warn.log`,
      'error', `${this.config.logPath}/error.log`,
      'fatal', `${this.config.logPath}/fatal.log`
    ], {cwd, env});


    this.logThrough.pipe(child.stdin);

  }

  listen() {

    alt.log('[core] listen');

    return new Promise((resolve, reject) => {

      this.http.listen(this.config.port, err => {

        if(err) {
          reject(err);
          return;
        }

        this.emit('http.listen', this.config.port);

        resolve();

      });

    });

  }

  loadModules() {

    alt.log('[core] load modules');

    return new Promise((resolve, reject) => {

      try {

        const toInit  = [];
        const modules = [];
        const files   = fs.readdirSync(`${__dirname}/modules`);
  
        // Require modules
        for(let i=0; i<files.length; i++) {
  
          const name = files[i];
          const mod  = this.require(name) || undefined;
  
          modules.push({mod, name});
        }
  
        // Sort modules with optionnal __before and __after properties
        const moduleTopology = new Topo();
  
        for(let i=0; i<modules.length; i++) {
  
          const module = modules[i];
  
          const {mod, name} = modules[i];
          const before      = mod ? (mod.__before || undefined) : undefined;
          const after       = mod ? (mod.__after  || undefined) : undefined;
  
          moduleTopology.add(name, {before, after, group: name});
  
        }
  
        const sortedModules = moduleTopology.nodes.map(e => ({mod: this.modules[e], name: e}));
  
        for(let i=0; i<sortedModules.length; i++) {
  
          const {mod, name} = sortedModules[i];
  
          if(mod && (typeof mod.__init === 'function') && (toInit.findIndex(e => e.func === mod.__init) === -1))
            toInit.push({func: mod.__init, mod, name});
        }
  
        // Create tasks from toInit array
        const tasks = toInit.map(e => {
  
          return cb => {
  
            process.nextTick(() => {
  
              e.func(err => {
                
                if(err) {
                  this.emit('module.init.error', e.name, err);
                  cb(err);
                  return;
                }
  
                this.emit('module.init.success', e.name);
                cb(null);
  
              });
  
            });
  
          }
  
        });
  
        // Init modules in series
        async.series(tasks, (results) => {
          this.emit('modules.loaded');
          resolve();
        });

      } catch(e) { reject(e); }

    });

  }

  require(name) {

    const configDirectory = `${__dirname}/config`;
    const moduleDirectory = `${__dirname}/modules`;

    if(this.modules[name] === undefined) {

      alt.log(`[core] load ${name}`);

      let config = {};

      if(fs.existsSync(`${configDirectory}/${name}.json`))
        config = JSON.parse(fs.readFileSync(`${configDirectory}/${name}.json`))
      else if(fs.existsSync(`${configDirectory}/${name}.js`))
        config = require(`${configDirectory}/${name}.js`)(this);

      try {

        this.modules[name] = require(`${moduleDirectory}/${name}`)(this, config);
        this.emit('module.load.success', name);

      } catch(err) {

        this.emit('module.load.error', name, err);

      }

    }

    return this.modules[name];

  }

  updateCache(player, ..._idStrs) {

    this.cacheUpdates.set(player, this.cacheUpdates.get(player) || []);
    const data = this.cacheUpdates.get(player);
    
    for(let i=0; i<_idStrs.length; i++) {

      const _idStr = _idStrs[i].toString();

      if(data.indexOf(_idStr) === -1)
        data.push(_idStr);

    }

  }

  initCacheUpdate() {

    setInterval(() => {

      for(const [player, data] of this.cacheUpdates) {
        
        if(!player && (player !== null)) {
          this.cacheUpdates.delete(player);
          continue;
        }

        if(data.length > 0) {
          // console.log(player ? player.id : 'null', ...data);
          alt.emitClient(player, 'hz:cache:update', ...data);
          data.length = 0;
        }

      }

    }, 500);

  }

  async start() {

    alt.log('[core] start');

    alt.Vehicle.all.forEach(e => e.destroy());

    await this.init();
    await this.listen();

    alt.emitClient(null, 'hz:player:connect');
  }
}

module.exports = Server;
