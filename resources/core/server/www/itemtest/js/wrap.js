(() => {

  const bindRe     = /\<\%([\S\s]*?)\%\>/;
  const bindRePure = /\<\%\%([\S\s]*?)\%\%\>/;

  window.$wrap = (html = '', props = {}) => {

    if(html instanceof HTMLElement && html.$props !== undefined)
      return html;

    let el;

    if(html instanceof HTMLElement) {
      
      el = html;
    
    } else {

      html = html.toString().trim();

      if(html.indexOf('<') === 0) {
        
        const match = html.match('<^\%(.*?)\/{0,1}>')

        if(match)
          el = document.createElement(match[1]);
        else
          el = document.createElement('div');

      } else el = document.createElement('span');

    }

    const $props = {...props};

    for(let k in $props) {
      if($props.hasOwnProperty(k)) {

        if(k.startsWith('@'))
        el.setAttribute(k.substr(1), $props[k]);

        if(k.startsWith('#'))
        el[k.substr(1)] = $props[k];

      }
    }

    const _$props = new Proxy($props, {
      
      set: (obj, key, val) => {
        
        const evt = new CustomEvent('$change', {detail: {key, val}});
        
        el.dispatchEvent(evt);
        
        obj[key] = val;

        if(key.startsWith('@'))
          this.setAttribute(key.substr(1), val);

        if(key.startsWith('#'))
          this[key.substr(1)] = val;

        el.$render();
        return true;
      }

    });

    Object.defineProperty(el, '$props', {
      get: ()  => _$props,
      set: val => {
        for(let k in val)
        _$props[k] = val;
      }
    });

    el.$render = function() {
      
      if(html instanceof HTMLElement)
        return;

      let boundHTML = html.toString();

      let match = boundHTML.match(bindRePure);

      while(match != null) {

        let ret = undefined;
        eval(`ret = ${match[1]};`);

        while(typeof ret === 'function')
          ret = ret();

        boundHTML = boundHTML.replace(`<%%${match[1]}%%>`, ret);
        match     = boundHTML.match(bindRePure);
      }

      const slotData = [];
      match          = boundHTML.match(bindRe);

      while(match != null) {

        let ret = undefined;
        eval(`ret = ${match[1]};`);

        while(typeof ret === 'function')
          ret = ret();

        if(ret instanceof Array) {
          
          for(let i=0; i<ret.length; i++) {
            
            while(typeof ret[i] === 'function')
              ret[i] = ret[i]();
          }
          
          slotData.push($pack(ret));

        } else slotData.push($wrap(ret));

        boundHTML = boundHTML.replace(`<%${match[1]}%>`, `<slot></slot>`);
        match     = boundHTML.match(bindRe);
      }

      el.innerHTML = boundHTML;

      const slots = el.querySelectorAll('slot');

      for(let i=0; i<slots.length; i++)
        slots[i].parentNode.replaceChild(slotData[i], slots[i]);

    }.bind(el);

    el.$set = function(...data) {
      
      if(data[0] instanceof Array)
        data = data[0];

      for(const child of this.childNodes) {
        this.removeChild(child);
        child.remove();
      }

      for(const html of data) {

        if(html instanceof HTMLElement)
          this.appendChild(html);
        else
          this.appendChild($wrap(html));

      }

    }.bind(el);

    el.$render();

    el.$props.onMount && setTimeout(() => el.$props.onMount.apply(el, []), 0);

    return el;

  }

  window.$pack = function(...elems) {

    if(elems[0] instanceof Array)
      elems = elems[0];

    const pack = document.createElement('div');

    elems.forEach(e => pack.appendChild($wrap(e)));

    return $wrap(pack);
  }

})();
