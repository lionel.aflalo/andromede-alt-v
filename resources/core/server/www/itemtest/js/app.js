'use strict';

(() => {

  const ENDPOINT = '/api';

  const CANVAS_WIDTH  = 801;
  const CANVAS_HEIGHT = 801;
  const GRID_WIDTH    = 800;
  const GRID_HEIGHT   = 800;
  const GRID_SIZE     = 100;

  const Main = props => $wrap(`
    <div class="main-wrap">
      <canvas class="canvas-view" width="<%% props.cw %%>" height="<%% props.cw %%>"></canvas>
    </div>
  `, {...props});

  window.app = {};
  app.img    = null;
  app.root   = document.querySelector('#root');

  app.main = Main({
    app,
    cw: CANVAS_WIDTH,
    ch: CANVAS_HEIGHT,
    gw: GRID_WIDTH,
    gh: GRID_HEIGHT,
  });

  app.root.appendChild(app.main);

  app.canvas = app.main.querySelector('.canvas-view');
  app.c      = new fabric.Canvas(app.canvas);
  app.rects  = [];

  (async () => {

    const res  = await fetch('./data/test.json');
    const json = await res.json();

    const bb = {x: Infinity, y: Infinity};

    json.grid.forEach(e => {

      if(e.x < bb.x)
        bb.x = e.x;

      if(e.y < bb.y)
        bb.y = e.y;

    });

    let offset = {x: 0, y: 0};

    if(bb.x < 0)
      offset.x = Math.abs(bb.x);

    if(bb.y < 0)
      offset.y = Math.abs(bb.y);

    bb.x += offset.x;
    bb.y += offset.y;

    for(let x=0; x<GRID_WIDTH; x += GRID_SIZE) {
      for(let y=0; y<GRID_HEIGHT; y += GRID_SIZE) {

        const isGridRect = json.grid.filter(e => {
          return (
            ((e.x + offset.x) - bb.x >= x)            &&
            ((e.y + offset.y) - bb.y >= y)            &&
            ((e.x + offset.x) - bb.x < x + GRID_SIZE) &&
            ((e.y + offset.y) - bb.y < y + GRID_SIZE)
          );
        }).length > 0;

        if(isGridRect) {

          const rect = new fabric.Rect({
            left: x,
            top: y,
            width: GRID_SIZE,
            height: GRID_SIZE,
            fill: 'rgba(100, 100, 100, 0.25)',
            stroke: '#000',
            strokeWidth: 1,
            selectable: false,
            clickable: false,
          });

          app.c.add(rect);
          app.rects.push(rect);
        }

      }
    }

    fabric.Image.fromURL('./data/test.png', img => {
      app.img = img;
      app.img.left = json.offset.x;
      app.img.top  = json.offset.y;
      app.c.add(img);
    });

  })();

})();