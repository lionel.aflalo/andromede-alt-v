module.exports = (horizon, config) => {
     
    const HZItem = horizon.require('item');
  
    class HZItemDonuts extends HZItem {
  
    }
  
    HZItemDonuts
      .inheritFields(HZItem)
      .inheritActions(HZItem)
      .field('_type', {default: 'ItemDonuts'})
      .field('image', {default: 'donuts'})
      .field('shape', {default: {"grid":[{"x":-2,"y":-21.873076923076724}],"offset":{"x":2,"y":21.873076923076724}}})
      .field('name',  {default: 'itemdonuts'})
      .field('label', {default: 'Donuts'})
    ;
  
    return HZItemDonuts;
  
  }