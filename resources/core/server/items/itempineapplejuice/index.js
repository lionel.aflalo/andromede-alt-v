module.exports = (horizon, config) => {
     
    const HZItem = horizon.require('item');
  
    class HZItemPineapplejuice extends HZItem {
  
    }
  
    HZItemPineapplejuice
      .inheritFields(HZItem)
      .inheritActions(HZItem)
      .field('_type', {default: 'ItemPineapplejuice'})
      .field('image', {default: 'pineapplejuice'})
      .field('shape', {default: {"grid":[{"x":-23.00000000000034,"y":-0.9305912596403232}],"offset":{"x":23.00000000000034,"y":0.9305912596403232}}})
      .field('name',  {default: 'itempineapplejuice'})
      .field('label', {default: 'Pineapplejuice'})
    ;
  
    return HZItemPineapplejuice;
  
  }