module.exports = (horizon, config) => {
     
    const HZItem = horizon.require('item');
  
    class HZItemWeaponflare extends HZItem {
  
    }
  
    HZItemWeaponflare
      .inheritFields(HZItem)
      .inheritActions(HZItem)
      .field('_type', {default: 'ItemWeaponflare'})
      .field('image', {default: 'weaponflare'})
      .field('shape', {default: {"grid":[{"x":-28.083333333333258,"y":-16.65277777777783},{"x":-28.083333333333258,"y":83.34722222222217}],"offset":{"x":28.083333333333258,"y":16.65277777777783}}})
      .field('name',  {default: 'itemweaponflare'})
      .field('label', {default: 'Weaponflare'})
    ;
  
    return HZItemWeaponflare;
  
  }