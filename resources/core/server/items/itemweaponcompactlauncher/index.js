module.exports = (horizon, config) => {
     
    const HZItem = horizon.require('item');
  
    class HZItemWeaponcompactlauncher extends HZItem {
  
    }
  
    HZItemWeaponcompactlauncher
      .inheritFields(HZItem)
      .inheritActions(HZItem)
      .field('_type', {default: 'ItemWeaponcompactlauncher'})
      .field('image', {default: 'weaponcompactlauncher'})
      .field('shape', {default: {"grid":[{"x":-13.999999999999972,"y":-22.76309226932665},{"x":-13.999999999999972,"y":77.23690773067335},{"x":86.00000000000003,"y":-22.76309226932665},{"x":86.00000000000003,"y":77.23690773067335},{"x":186.00000000000003,"y":-22.76309226932665}],"offset":{"x":13.999999999999972,"y":22.76309226932665}}})
      .field('name',  {default: 'itemweaponcompactlauncher'})
      .field('label', {default: 'Weaponcompactlauncher'})
    ;
  
    return HZItemWeaponcompactlauncher;
  
  }