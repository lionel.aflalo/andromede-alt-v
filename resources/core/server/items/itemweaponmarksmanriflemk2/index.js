module.exports = (horizon, config) => {
     
    const HZItem = horizon.require('item');
  
    class HZItemWeaponmarksmanriflemk2 extends HZItem {
  
    }
  
    HZItemWeaponmarksmanriflemk2
      .inheritFields(HZItem)
      .inheritActions(HZItem)
      .field('_type', {default: 'ItemWeaponmarksmanriflemk2'})
      .field('image', {default: 'weaponmarksmanriflemk2'})
      .field('shape', {default: {"grid":[{"x":-7.999999999999943,"y":-30.980528511821944},{"x":-7.999999999999943,"y":69.01947148817806},{"x":92.00000000000006,"y":-30.980528511821944},{"x":92.00000000000006,"y":69.01947148817806},{"x":192.00000000000006,"y":-30.980528511821944},{"x":192.00000000000006,"y":69.01947148817806},{"x":292.00000000000006,"y":-30.980528511821944},{"x":392.00000000000006,"y":-30.980528511821944},{"x":492.00000000000006,"y":-30.980528511821944}],"offset":{"x":7.999999999999943,"y":30.980528511821944}}})
      .field('name',  {default: 'itemweaponmarksmanriflemk2'})
      .field('label', {default: 'Weaponmarksmanriflemk2'})
    ;
  
    return HZItemWeaponmarksmanriflemk2;
  
  }