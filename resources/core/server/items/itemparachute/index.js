module.exports = (horizon, config) => {
     
    const HZItem = horizon.require('item');
  
    class HZItemParachute extends HZItem {
  
    }
  
    HZItemParachute
      .inheritFields(HZItem)
      .inheritActions(HZItem)
      .field('_type', {default: 'ItemParachute'})
      .field('image', {default: 'parachute'})
      .field('shape', {default: {"grid":[{"x":-27,"y":-24.880165289256183},{"x":-27,"y":75.11983471074382},{"x":-27,"y":175.11983471074382},{"x":73,"y":-24.880165289256183},{"x":73,"y":75.11983471074382},{"x":73,"y":175.11983471074382}],"offset":{"x":27,"y":24.880165289256183}}})
      .field('name',  {default: 'itemparachute'})
      .field('label', {default: 'Parachute'})
    ;
  
    return HZItemParachute;
  
  }