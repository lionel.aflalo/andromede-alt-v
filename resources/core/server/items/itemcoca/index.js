module.exports = (horizon, config) => {
     
    const HZItem = horizon.require('item');
  
    class HZItemCoca extends HZItem {
  
    }
  
    HZItemCoca
      .inheritFields(HZItem)
      .inheritActions(HZItem)
      .field('_type', {default: 'ItemCoca'})
      .field('image', {default: 'coca'})
      .field('shape', {default: {"grid":[{"x":-18.999999999999773,"y":-5.5198300283284425},{"x":-18.999999999999773,"y":94.48016997167156},{"x":81.00000000000023,"y":-5.5198300283284425},{"x":81.00000000000023,"y":94.48016997167156}],"offset":{"x":18.999999999999773,"y":5.5198300283284425}}})
      .field('name',  {default: 'itemcoca'})
      .field('label', {default: 'Coca'})
    ;
  
    return HZItemCoca;
  
  }