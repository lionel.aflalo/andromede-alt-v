module.exports = (horizon, config) => {
     
    const HZItem = horizon.require('item');
  
    class HZItemWeaponproxmine extends HZItem {
  
    }
  
    HZItemWeaponproxmine
      .inheritFields(HZItem)
      .inheritActions(HZItem)
      .field('_type', {default: 'ItemWeaponproxmine'})
      .field('image', {default: 'weaponproxmine'})
      .field('shape', {default: {"grid":[{"x":-36.00000000000006,"y":-10.111382113821037},{"x":63.99999999999994,"y":-10.111382113821037},{"x":163.99999999999994,"y":-10.111382113821037}],"offset":{"x":36.00000000000006,"y":10.111382113821037}}})
      .field('name',  {default: 'itemweaponproxmine'})
      .field('label', {default: 'Weaponproxmine'})
    ;
  
    return HZItemWeaponproxmine;
  
  }