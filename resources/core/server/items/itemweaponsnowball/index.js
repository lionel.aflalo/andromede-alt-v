module.exports = (horizon, config) => {
     
    const HZItem = horizon.require('item');
  
    class HZItemWeaponsnowball extends HZItem {
  
    }
  
    HZItemWeaponsnowball
      .inheritFields(HZItem)
      .inheritActions(HZItem)
      .field('_type', {default: 'ItemWeaponsnowball'})
      .field('image', {default: 'weaponsnowball'})
      .field('shape', {default: {"grid":[{"x":-2,"y":-6.562639821029052}],"offset":{"x":2,"y":6.562639821029052}}})
      .field('name',  {default: 'itemweaponsnowball'})
      .field('label', {default: 'Weaponsnowball'})
    ;
  
    return HZItemWeaponsnowball;
  
  }