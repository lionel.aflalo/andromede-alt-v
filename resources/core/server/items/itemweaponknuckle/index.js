module.exports = (horizon, config) => {
     
    const HZItem = horizon.require('item');
  
    class HZItemWeaponknuckle extends HZItem {
  
    }
  
    HZItemWeaponknuckle
      .inheritFields(HZItem)
      .inheritActions(HZItem)
      .field('_type', {default: 'ItemWeaponknuckle'})
      .field('image', {default: 'weaponknuckle'})
      .field('shape', {default: {"grid":[{"x":-32.999999999999886,"y":-7.666950596252036},{"x":67.00000000000011,"y":-7.666950596252036}],"offset":{"x":32.999999999999886,"y":7.666950596252036}}})
      .field('name',  {default: 'itemweaponknuckle'})
      .field('label', {default: 'Weaponknuckle'})
    ;
  
    return HZItemWeaponknuckle;
  
  }