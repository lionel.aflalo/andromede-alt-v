module.exports = (horizon, config) => {
     
    const HZItem = horizon.require('item');
  
    class HZItemWeaponrevolver extends HZItem {
  
    }
  
    HZItemWeaponrevolver
      .inheritFields(HZItem)
      .inheritActions(HZItem)
      .field('_type', {default: 'ItemWeaponrevolver'})
      .field('image', {default: 'weaponrevolver'})
      .field('shape', {default: {"grid":[{"x":-7.999999999999972,"y":-9.370744010088345},{"x":-7.999999999999972,"y":90.62925598991166},{"x":92.00000000000003,"y":-9.370744010088345},{"x":192.00000000000003,"y":-9.370744010088345}],"offset":{"x":7.999999999999972,"y":9.370744010088345}}})
      .field('name',  {default: 'itemweaponrevolver'})
      .field('label', {default: 'Weaponrevolver'})
    ;
  
    return HZItemWeaponrevolver;
  
  }