module.exports = (horizon, config) => {
     
    const HZItem = horizon.require('item');
  
    class HZItemWeaponcarbinerifle extends HZItem {
  
    }
  
    HZItemWeaponcarbinerifle
      .inheritFields(HZItem)
      .inheritActions(HZItem)
      .field('_type', {default: 'ItemWeaponcarbinerifle'})
      .field('image', {default: 'weaponcarbinerifle'})
      .field('shape', {default: {"grid":[{"x":-16.00000000000017,"y":-2},{"x":83.99999999999983,"y":-2},{"x":83.99999999999983,"y":98},{"x":183.99999999999983,"y":-2},{"x":283.99999999999983,"y":-2},{"x":383.99999999999983,"y":-2}],"offset":{"x":16.00000000000017,"y":2}}})
      .field('name',  {default: 'itemweaponcarbinerifle'})
      .field('label', {default: 'Weaponcarbinerifle'})
    ;
  
    return HZItemWeaponcarbinerifle;
  
  }