module.exports = (horizon, config) => {
     
    const HZItem = horizon.require('item');
  
    class HZItemWristlet extends HZItem {
  
    }
  
    HZItemWristlet
      .inheritFields(HZItem)
      .inheritActions(HZItem)
      .field('_type', {default: 'ItemWristlet'})
      .field('image', {default: 'wristlet'})
      .field('shape', {default: {"grid":[{"x":-30.448398576512545,"y":-13.551601423487455},{"x":69.55160142348745,"y":-13.551601423487455}],"offset":{"x":30.448398576512545,"y":13.551601423487455}}})
      .field('name',  {default: 'itemwristlet'})
      .field('label', {default: 'Wristlet'})
    ;
  
    return HZItemWristlet;
  
  }