module.exports = (horizon, config) => {
     
    const HZItem = horizon.require('item');
  
    class HZItemWeaponhammer extends HZItem {
  
    }
  
    HZItemWeaponhammer
      .inheritFields(HZItem)
      .inheritActions(HZItem)
      .field('_type', {default: 'ItemWeaponhammer'})
      .field('image', {default: 'weaponhammer'})
      .field('shape', {default: {"grid":[{"x":-13.000000000000227,"y":-3.8822629969420177},{"x":86.99999999999977,"y":-3.8822629969420177},{"x":186.99999999999977,"y":-3.8822629969420177}],"offset":{"x":13.000000000000227,"y":3.8822629969420177}}})
      .field('name',  {default: 'itemweaponhammer'})
      .field('label', {default: 'Weaponhammer'})
    ;
  
    return HZItemWeaponhammer;
  
  }