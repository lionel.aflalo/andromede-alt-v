module.exports = (horizon, config) => {
     
    const HZItem = horizon.require('item');
  
    class HZItemWaterbottle extends HZItem {
  
    }
  
    HZItemWaterbottle
      .inheritFields(HZItem)
      .inheritActions(HZItem)
      .field('_type', {default: 'ItemWaterbottle'})
      .field('image', {default: 'waterbottle'})
      .field('shape', {default: {"grid":[{"x":-12.01560781928356,"y":-8.98439218071644},{"x":-12.01560781928356,"y":91.01560781928356}],"offset":{"x":12.01560781928356,"y":8.98439218071644}}})
      .field('name',  {default: 'itemwaterbottle'})
      .field('label', {default: 'Waterbottle'})
    ;
  
    return HZItemWaterbottle;
  
  }