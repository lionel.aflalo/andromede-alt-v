module.exports = (horizon, config) => {
     
    const HZItem = horizon.require('item');
  
    class HZItemFirstaidkit extends HZItem {
  
    }
  
    HZItemFirstaidkit
      .inheritFields(HZItem)
      .inheritActions(HZItem)
      .field('_type', {default: 'ItemFirstaidkit'})
      .field('image', {default: 'firstaidkit'})
      .field('shape', {default: {"grid":[{"x":-46,"y":-1.75},{"x":-46,"y":98.25},{"x":54,"y":-1.75},{"x":154,"y":-1.75},{"x":154,"y":98.25}],"offset":{"x":46,"y":1.75}}})
      .field('name',  {default: 'itemfirstaidkit'})
      .field('label', {default: 'Firstaidkit'})
    ;
  
    return HZItemFirstaidkit;
  
  }