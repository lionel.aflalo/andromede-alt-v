module.exports = (horizon, config) => {
     
    const HZItem = horizon.require('item');
  
    class HZItemWeaponrailgun extends HZItem {
  
    }
  
    HZItemWeaponrailgun
      .inheritFields(HZItem)
      .inheritActions(HZItem)
      .field('_type', {default: 'ItemWeaponrailgun'})
      .field('image', {default: 'weaponrailgun'})
      .field('shape', {default: {"grid":[{"x":-59.00000000000023,"y":-43.73272138228947},{"x":-59.00000000000023,"y":56.26727861771053},{"x":40.99999999999977,"y":-43.73272138228947},{"x":40.99999999999977,"y":56.26727861771053},{"x":140.99999999999977,"y":-43.73272138228947},{"x":140.99999999999977,"y":56.26727861771053},{"x":240.99999999999977,"y":-43.73272138228947},{"x":240.99999999999977,"y":56.26727861771053},{"x":340.9999999999998,"y":-43.73272138228947},{"x":340.9999999999998,"y":56.26727861771053},{"x":440.9999999999998,"y":-43.73272138228947},{"x":440.9999999999998,"y":56.26727861771053}],"offset":{"x":59.00000000000023,"y":43.73272138228947}}})
      .field('name',  {default: 'itemweaponrailgun'})
      .field('label', {default: 'Weaponrailgun'})
    ;
  
    return HZItemWeaponrailgun;
  
  }