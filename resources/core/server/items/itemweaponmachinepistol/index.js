module.exports = (horizon, config) => {
     
    const HZItem = horizon.require('item');
  
    class HZItemWeaponmachinepistol extends HZItem {
  
    }
  
    HZItemWeaponmachinepistol
      .inheritFields(HZItem)
      .inheritActions(HZItem)
      .field('_type', {default: 'ItemWeaponmachinepistol'})
      .field('image', {default: 'weaponmachinepistol'})
      .field('shape', {default: {"grid":[{"x":-25.999999999999773,"y":-17.16026711185299},{"x":-25.999999999999773,"y":82.83973288814701},{"x":74.00000000000023,"y":-17.16026711185299},{"x":174.00000000000023,"y":-17.16026711185299}],"offset":{"x":25.999999999999773,"y":17.16026711185299}}})
      .field('name',  {default: 'itemweaponmachinepistol'})
      .field('label', {default: 'Weaponmachinepistol'})
    ;
  
    return HZItemWeaponmachinepistol;
  
  }