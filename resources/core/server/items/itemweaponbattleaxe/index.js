module.exports = (horizon, config) => {
     
    const HZItem = horizon.require('item');
  
    class HZItemWeaponbattleaxe extends HZItem {
  
    }
  
    HZItemWeaponbattleaxe
      .inheritFields(HZItem)
      .inheritActions(HZItem)
      .field('_type', {default: 'ItemWeaponbattleaxe'})
      .field('image', {default: 'weaponbattleaxe'})
      .field('shape', {default: {"grid":[{"x":-1.0000000000000568,"y":-14.453362255965317},{"x":98.99999999999994,"y":-14.453362255965317},{"x":198.99999999999994,"y":-14.453362255965317},{"x":198.99999999999994,"y":85.54663774403468}],"offset":{"x":1.0000000000000568,"y":14.453362255965317}}})
      .field('name',  {default: 'itemweaponbattleaxe'})
      .field('label', {default: 'Weaponbattleaxe'})
    ;
  
    return HZItemWeaponbattleaxe;
  
  }