module.exports = (horizon, config) => {
     
    const HZItem = horizon.require('item');
  
    class HZItemBanana extends HZItem {
  
    }
  
    HZItemBanana
      .inheritFields(HZItem)
      .inheritActions(HZItem)
      .field('_type', {default: 'ItemBanana'})
      .field('image', {default: 'banana'})
      .field('shape', {default: {"grid":[{"x":-3.0000000000001137,"y":-21.614355231143577}],"offset":{"x":3.0000000000001137,"y":21.614355231143577}}})
      .field('name',  {default: 'itembanana'})
      .field('label', {default: 'Banana'})
    ;
  
    return HZItemBanana;
  
  }