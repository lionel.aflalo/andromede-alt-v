module.exports = (horizon, config) => {
     
    const HZItem = horizon.require('item');
  
    class HZItemDuffbeerbottle extends HZItem {
  
    }
  
    HZItemDuffbeerbottle
      .inheritFields(HZItem)
      .inheritActions(HZItem)
      .field('_type', {default: 'ItemDuffbeerbottle'})
      .field('image', {default: 'duffbeerbottle'})
      .field('shape', {default: {"grid":[{"x":-23,"y":-5.573656845753703},{"x":-23,"y":94.4263431542463}],"offset":{"x":23,"y":5.573656845753703}}})
      .field('name',  {default: 'itemduffbeerbottle'})
      .field('label', {default: 'Duffbeerbottle'})
    ;
  
    return HZItemDuffbeerbottle;
  
  }