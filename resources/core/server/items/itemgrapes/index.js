module.exports = (horizon, config) => {
     
    const HZItem = horizon.require('item');
  
    class HZItemGrapes extends HZItem {
  
    }
  
    HZItemGrapes
      .inheritFields(HZItem)
      .inheritActions(HZItem)
      .field('_type', {default: 'ItemGrapes'})
      .field('image', {default: 'grapes'})
      .field('shape', {default: {"grid":[{"x":-2.9999999999998863,"y":-14.066037735849022}],"offset":{"x":2.9999999999998863,"y":14.066037735849022}}})
      .field('name',  {default: 'itemgrapes'})
      .field('label', {default: 'Grapes'})
    ;
  
    return HZItemGrapes;
  
  }