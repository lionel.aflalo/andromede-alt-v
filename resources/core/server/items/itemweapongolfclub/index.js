module.exports = (horizon, config) => {
     
    const HZItem = horizon.require('item');
  
    class HZItemWeapongolfclub extends HZItem {
  
    }
  
    HZItemWeapongolfclub
      .inheritFields(HZItem)
      .inheritActions(HZItem)
      .field('_type', {default: 'ItemWeapongolfclub'})
      .field('image', {default: 'weapongolfclub'})
      .field('shape', {default: {"grid":[{"x":-7.703757225433492,"y":-18.296242774566508},{"x":92.29624277456651,"y":-18.296242774566508},{"x":192.2962427745665,"y":-18.296242774566508},{"x":292.2962427745665,"y":-18.296242774566508}],"offset":{"x":7.703757225433492,"y":18.296242774566508}}})
      .field('name',  {default: 'itemweapongolfclub'})
      .field('label', {default: 'Weapongolfclub'})
    ;
  
    return HZItemWeapongolfclub;
  
  }