module.exports = (horizon, config) => {
     
    const HZItem = horizon.require('item');
  
    class HZItemWeaponpumpshotgunmk2 extends HZItem {
  
    }
  
    HZItemWeaponpumpshotgunmk2
      .inheritFields(HZItem)
      .inheritActions(HZItem)
      .field('_type', {default: 'ItemWeaponpumpshotgunmk2'})
      .field('image', {default: 'weaponpumpshotgunmk2'})
      .field('shape', {default: {"grid":[{"x":-12.000000000000199,"y":-36.51217765042986},{"x":-12.000000000000199,"y":63.48782234957014},{"x":87.9999999999998,"y":-36.51217765042986},{"x":87.9999999999998,"y":63.48782234957014},{"x":187.9999999999998,"y":-36.51217765042986},{"x":287.9999999999998,"y":-36.51217765042986},{"x":387.9999999999998,"y":-36.51217765042986}],"offset":{"x":12.000000000000199,"y":36.51217765042986}}})
      .field('name',  {default: 'itemweaponpumpshotgunmk2'})
      .field('label', {default: 'Weaponpumpshotgunmk2'})
    ;
  
    return HZItemWeaponpumpshotgunmk2;
  
  }