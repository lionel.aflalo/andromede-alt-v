module.exports = (horizon, config) => {
     
    const HZItem = horizon.require('item');
  
    class HZItemMojito extends HZItem {
  
    }
  
    HZItemMojito
      .inheritFields(HZItem)
      .inheritActions(HZItem)
      .field('_type', {default: 'ItemMojito'})
      .field('image', {default: 'mojito'})
      .field('shape', {default: {"grid":[{"x":-15.937185929648194,"y":-2}],"offset":{"x":15.937185929648194,"y":2}}})
      .field('name',  {default: 'itemmojito'})
      .field('label', {default: 'Mojito'})
    ;
  
    return HZItemMojito;
  
  }