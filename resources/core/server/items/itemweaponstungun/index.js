module.exports = (horizon, config) => {
     
    const HZItem = horizon.require('item');
  
    class HZItemWeaponstungun extends HZItem {
  
    }
  
    HZItemWeaponstungun
      .inheritFields(HZItem)
      .inheritActions(HZItem)
      .field('_type', {default: 'ItemWeaponstungun'})
      .field('image', {default: 'weaponstungun'})
      .field('shape', {default: {"grid":[{"x":-16,"y":-6.547983310153029},{"x":-16,"y":93.45201668984697},{"x":84,"y":-6.547983310153029},{"x":184,"y":-6.547983310153029}],"offset":{"x":16,"y":6.547983310153029}}})
      .field('name',  {default: 'itemweaponstungun'})
      .field('label', {default: 'Weaponstungun'})
    ;
  
    return HZItemWeaponstungun;
  
  }