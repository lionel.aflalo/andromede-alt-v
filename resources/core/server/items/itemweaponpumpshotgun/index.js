module.exports = (horizon, config) => {
     
    const HZItem = horizon.require('item');
  
    class HZItemWeaponpumpshotgun extends HZItem {
  
    }
  
    HZItemWeaponpumpshotgun
      .inheritFields(HZItem)
      .inheritActions(HZItem)
      .field('_type', {default: 'ItemWeaponpumpshotgun'})
      .field('image', {default: 'weaponpumpshotgun'})
      .field('shape', {default: {"grid":[{"x":-9.999999999999886,"y":-39.89903846153845},{"x":-9.999999999999886,"y":60.10096153846155},{"x":90.00000000000011,"y":-39.89903846153845},{"x":90.00000000000011,"y":60.10096153846155},{"x":190.0000000000001,"y":-39.89903846153845},{"x":290.0000000000001,"y":-39.89903846153845},{"x":390.0000000000001,"y":-39.89903846153845}],"offset":{"x":9.999999999999886,"y":39.89903846153845}}})
      .field('name',  {default: 'itemweaponpumpshotgun'})
      .field('label', {default: 'Weaponpumpshotgun'})
    ;
  
    return HZItemWeaponpumpshotgun;
  
  }