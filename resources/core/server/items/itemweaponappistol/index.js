module.exports = (horizon, config) => {
     
    const HZItem = horizon.require('item');
  
    class HZItemWeaponappistol extends HZItem {
  
    }
  
    HZItemWeaponappistol
      .inheritFields(HZItem)
      .inheritActions(HZItem)
      .field('_type', {default: 'ItemWeaponappistol'})
      .field('image', {default: 'weaponappistol'})
      .field('shape', {default: {"grid":[{"x":-14.999999999999886,"y":-23.037931034482654},{"x":-14.999999999999886,"y":76.96206896551735},{"x":85.00000000000011,"y":-23.037931034482654},{"x":185.0000000000001,"y":-23.037931034482654}],"offset":{"x":14.999999999999886,"y":23.037931034482654}}})
      .field('name',  {default: 'itemweaponappistol'})
      .field('label', {default: 'Weaponappistol'})
    ;
  
    return HZItemWeaponappistol;
  
  }