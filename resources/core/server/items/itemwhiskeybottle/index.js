module.exports = (horizon, config) => {
     
    const HZItem = horizon.require('item');
  
    class HZItemWhiskeybottle extends HZItem {
  
    }
  
    HZItemWhiskeybottle
      .inheritFields(HZItem)
      .inheritActions(HZItem)
      .field('_type', {default: 'ItemWhiskeybottle'})
      .field('image', {default: 'whiskeybottle'})
      .field('shape', {default: {"grid":[{"x":-15,"y":-6.625},{"x":-15,"y":93.375}],"offset":{"x":15,"y":6.625}}})
      .field('name',  {default: 'itemwhiskeybottle'})
      .field('label', {default: 'Whiskeybottle'})
    ;
  
    return HZItemWhiskeybottle;
  
  }