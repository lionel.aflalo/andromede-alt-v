module.exports = (horizon, config) => {
     
    const HZItem = horizon.require('item');
  
    class HZItemWatermelon extends HZItem {
  
    }
  
    HZItemWatermelon
      .inheritFields(HZItem)
      .inheritActions(HZItem)
      .field('_type', {default: 'ItemWatermelon'})
      .field('image', {default: 'watermelon'})
      .field('shape', {default: {"grid":[{"x":-19,"y":-3.6738544474392256},{"x":81,"y":-3.6738544474392256}],"offset":{"x":19,"y":3.6738544474392256}}})
      .field('name',  {default: 'itemwatermelon'})
      .field('label', {default: 'Watermelon'})
    ;
  
    return HZItemWatermelon;
  
  }