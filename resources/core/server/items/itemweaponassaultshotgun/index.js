module.exports = (horizon, config) => {
     
    const HZItem = horizon.require('item');
  
    class HZItemWeaponassaultshotgun extends HZItem {
  
    }
  
    HZItemWeaponassaultshotgun
      .inheritFields(HZItem)
      .inheritActions(HZItem)
      .field('_type', {default: 'ItemWeaponassaultshotgun'})
      .field('image', {default: 'weaponassaultshotgun'})
      .field('shape', {default: {"grid":[{"x":-21.000000000000142,"y":-6.366094986807411},{"x":78.99999999999986,"y":-6.366094986807411},{"x":78.99999999999986,"y":93.63390501319259},{"x":178.99999999999986,"y":-6.366094986807411},{"x":278.9999999999999,"y":-6.366094986807411},{"x":378.9999999999999,"y":-6.366094986807411}],"offset":{"x":21.000000000000114,"y":6.366094986807411}}})
      .field('name',  {default: 'itemweaponassaultshotgun'})
      .field('label', {default: 'Weaponassaultshotgun'})
    ;
  
    return HZItemWeaponassaultshotgun;
  
  }