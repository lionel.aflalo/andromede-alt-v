module.exports = (horizon, config) => {
     
    const HZItem = horizon.require('item');
  
    class HZItemWeaponcarbineriflemk2 extends HZItem {
  
    }
  
    HZItemWeaponcarbineriflemk2
      .inheritFields(HZItem)
      .inheritActions(HZItem)
      .field('_type', {default: 'ItemWeaponcarbineriflemk2'})
      .field('image', {default: 'weaponcarbineriflemk2'})
      .field('shape', {default: {"grid":[{"x":-18.00000000000003,"y":-0.13578500707217245},{"x":81.99999999999997,"y":-0.13578500707217245},{"x":81.99999999999997,"y":99.86421499292783},{"x":181.99999999999997,"y":-0.13578500707217245},{"x":181.99999999999997,"y":99.86421499292783},{"x":282,"y":-0.13578500707217245},{"x":382,"y":-0.13578500707217245}],"offset":{"x":18.00000000000003,"y":0.13578500707217245}}})
      .field('name',  {default: 'itemweaponcarbineriflemk2'})
      .field('label', {default: 'Weaponcarbineriflemk2'})
    ;
  
    return HZItemWeaponcarbineriflemk2;
  
  }