module.exports = (horizon, config) => {
     
    const HZItem = horizon.require('item');
  
    class HZItemWeaponheavyshotgun extends HZItem {
  
    }
  
    HZItemWeaponheavyshotgun
      .inheritFields(HZItem)
      .inheritActions(HZItem)
      .field('_type', {default: 'ItemWeaponheavyshotgun'})
      .field('image', {default: 'weaponheavyshotgun'})
      .field('shape', {default: {"grid":[{"x":-11,"y":-17.056962025316466},{"x":89,"y":-17.056962025316466},{"x":89,"y":82.94303797468353},{"x":189,"y":-17.056962025316466},{"x":189,"y":82.94303797468353},{"x":289,"y":-17.056962025316466},{"x":389,"y":-17.056962025316466}],"offset":{"x":11,"y":17.056962025316466}}})
      .field('name',  {default: 'itemweaponheavyshotgun'})
      .field('label', {default: 'Weaponheavyshotgun'})
    ;
  
    return HZItemWeaponheavyshotgun;
  
  }