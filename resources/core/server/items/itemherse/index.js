module.exports = (horizon, config) => {
     
    const HZItem = horizon.require('item');
  
    class HZItemHerse extends HZItem {
  
    }
  
    HZItemHerse
      .inheritFields(HZItem)
      .inheritActions(HZItem)
      .field('_type', {default: 'ItemHerse'})
      .field('image', {default: 'herse'})
      .field('shape', {default: {"grid":[{"x":-5.171624713958863,"y":-50.82837528604114},{"x":-5.171624713958863,"y":49.17162471395886},{"x":-5.171624713958863,"y":149.17162471395886},{"x":94.82837528604114,"y":-50.82837528604114},{"x":94.82837528604114,"y":49.17162471395886},{"x":94.82837528604114,"y":149.17162471395886},{"x":194.82837528604114,"y":-50.82837528604114},{"x":194.82837528604114,"y":49.17162471395886},{"x":194.82837528604114,"y":149.17162471395886},{"x":294.82837528604114,"y":-50.82837528604114},{"x":294.82837528604114,"y":49.17162471395886},{"x":294.82837528604114,"y":149.17162471395886}],"offset":{"x":5.171624713958863,"y":50.82837528604114}}})
      .field('name',  {default: 'itemherse'})
      .field('label', {default: 'Herse'})
    ;
  
    return HZItemHerse;
  
  }