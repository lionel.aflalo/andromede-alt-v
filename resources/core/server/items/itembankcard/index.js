module.exports = (horizon, config) => {
     
    const HZItem = horizon.require('item');
  
    class HZItemBankcard extends HZItem {
  
    }
  
    HZItemBankcard
      .inheritFields(HZItem)
      .inheritActions(HZItem)
      .field('_type', {default: 'ItemBankcard'})
      .field('image', {default: 'bankcard'})
      .field('shape', {default: {"grid":[{"x":-4,"y":-20.999999999999993}],"offset":{"x":4,"y":20.999999999999993}}})
      .field('name',  {default: 'itembankcard'})
      .field('label', {default: 'Bankcard'})
    ;
  
    return HZItemBankcard;
  
  }