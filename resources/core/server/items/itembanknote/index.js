module.exports = (horizon, config) => {
     
    const HZItem = horizon.require('item');
  
    class HZItemBanknote extends HZItem {
  
    }
  
    HZItemBanknote
      .inheritFields(HZItem)
      .inheritActions(HZItem)
      .field('_type', {default: 'ItemBanknote'})
      .field('image', {default: 'banknote'})
      .field('shape', {default: {"grid":[{"x":-6,"y":-9}],"offset":{"x":6,"y":9}}})
      .field('name',  {default: 'itembanknote'})
      .field('label', {default: 'Banknote'})
    ;
  
    return HZItemBanknote;
  
  }