module.exports = (horizon, config) => {
     
    const HZItem = horizon.require('item');
  
    class HZItemWeapongrenadelauncher extends HZItem {
  
    }
  
    HZItemWeapongrenadelauncher
      .inheritFields(HZItem)
      .inheritActions(HZItem)
      .field('_type', {default: 'ItemWeapongrenadelauncher'})
      .field('image', {default: 'weapongrenadelauncher'})
      .field('shape', {default: {"grid":[{"x":-4.000000000000028,"y":-16.516703786191556},{"x":-4.000000000000028,"y":83.48329621380844},{"x":95.99999999999997,"y":-16.516703786191556},{"x":95.99999999999997,"y":83.48329621380844},{"x":195.99999999999997,"y":-16.516703786191556},{"x":195.99999999999997,"y":83.48329621380844},{"x":296,"y":-16.516703786191556}],"offset":{"x":4.000000000000028,"y":16.516703786191556}}})
      .field('name',  {default: 'itemweapongrenadelauncher'})
      .field('label', {default: 'Weapongrenadelauncher'})
    ;
  
    return HZItemWeapongrenadelauncher;
  
  }