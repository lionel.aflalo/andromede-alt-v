module.exports = (horizon, config) => {
     
    const HZItem = horizon.require('item');
  
    class HZItemWeapondagger extends HZItem {
  
    }
  
    HZItemWeapondagger
      .inheritFields(HZItem)
      .inheritActions(HZItem)
      .field('_type', {default: 'ItemWeapondagger'})
      .field('image', {default: 'weapondagger'})
      .field('shape', {default: {"grid":[{"x":-10.000000000000028,"y":-15.077792553191557},{"x":89.99999999999997,"y":-15.077792553191557},{"x":189.99999999999997,"y":-15.077792553191557}],"offset":{"x":10.000000000000028,"y":15.077792553191557}}})
      .field('name',  {default: 'itemweapondagger'})
      .field('label', {default: 'Weapondagger'})
    ;
  
    return HZItemWeapondagger;
  
  }