module.exports = (horizon, config) => {
     
    const HZItem = horizon.require('item');
  
    class HZItemWeaponcombatpistol extends HZItem {
  
    }
  
    HZItemWeaponcombatpistol
      .inheritFields(HZItem)
      .inheritActions(HZItem)
      .field('_type', {default: 'ItemWeaponcombatpistol'})
      .field('image', {default: 'weaponcombatpistol'})
      .field('shape', {default: {"grid":[{"x":-11.999999999999915,"y":-7.092134831460726},{"x":-11.999999999999915,"y":92.90786516853927},{"x":88.00000000000009,"y":-7.092134831460726},{"x":188.00000000000009,"y":-7.092134831460726}],"offset":{"x":11.999999999999915,"y":7.092134831460726}}})
      .field('name',  {default: 'itemweaponcombatpistol'})
      .field('label', {default: 'Weaponcombatpistol'})
    ;
  
    return HZItemWeaponcombatpistol;
  
  }