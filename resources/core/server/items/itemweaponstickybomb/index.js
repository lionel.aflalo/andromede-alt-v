module.exports = (horizon, config) => {
     
    const HZItem = horizon.require('item');
  
    class HZItemWeaponstickybomb extends HZItem {
  
    }
  
    HZItemWeaponstickybomb
      .inheritFields(HZItem)
      .inheritActions(HZItem)
      .field('_type', {default: 'ItemWeaponstickybomb'})
      .field('image', {default: 'weaponstickybomb'})
      .field('shape', {default: {"grid":[{"x":-23.000000000000057,"y":-5.9051094890510285},{"x":76.99999999999994,"y":-5.9051094890510285},{"x":176.99999999999994,"y":-5.9051094890510285}],"offset":{"x":23.000000000000057,"y":5.9051094890510285}}})
      .field('name',  {default: 'itemweaponstickybomb'})
      .field('label', {default: 'Weaponstickybomb'})
    ;
  
    return HZItemWeaponstickybomb;
  
  }