module.exports = (horizon, config) => {
     
    const HZItem = horizon.require('item');
  
    class HZItemWeaponminigun extends HZItem {
  
    }
  
    HZItemWeaponminigun
      .inheritFields(HZItem)
      .inheritActions(HZItem)
      .field('_type', {default: 'ItemWeaponminigun'})
      .field('image', {default: 'weaponminigun'})
      .field('shape', {default: {"grid":[{"x":-43.000000000000114,"y":-5.660621761658035},{"x":-43.000000000000114,"y":94.33937823834196},{"x":56.999999999999886,"y":-5.660621761658035},{"x":56.999999999999886,"y":94.33937823834196},{"x":156.9999999999999,"y":-5.660621761658035},{"x":156.9999999999999,"y":94.33937823834196},{"x":256.9999999999999,"y":-5.660621761658035},{"x":256.9999999999999,"y":94.33937823834196},{"x":356.9999999999999,"y":-5.660621761658035},{"x":356.9999999999999,"y":94.33937823834196},{"x":456.9999999999999,"y":-5.660621761658035},{"x":456.9999999999999,"y":94.33937823834196},{"x":556.9999999999999,"y":-5.660621761658035},{"x":556.9999999999999,"y":94.33937823834196}],"offset":{"x":43.000000000000114,"y":5.660621761658035}}})
      .field('name',  {default: 'itemweaponminigun'})
      .field('label', {default: 'Weaponminigun'})
    ;
  
    return HZItemWeaponminigun;
  
  }