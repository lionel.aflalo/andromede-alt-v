module.exports = (horizon, config) => {
     
    const HZItem = horizon.require('item');
  
    class HZItemFishingrod extends HZItem {
  
    }
  
    HZItemFishingrod
      .inheritFields(HZItem)
      .inheritActions(HZItem)
      .field('_type', {default: 'ItemFishingrod'})
      .field('image', {default: 'fishingrod'})
      .field('shape', {default: {"grid":[{"x":-13,"y":-10.664117647058902},{"x":-13,"y":89.3358823529411},{"x":87,"y":-10.664117647058902},{"x":87,"y":89.3358823529411}],"offset":{"x":13,"y":10.664117647058902}}})
      .field('name',  {default: 'itemfishingrod'})
      .field('label', {default: 'Fishingrod'})
    ;
  
    return HZItemFishingrod;
  
  }