module.exports = (horizon, config) => {
     
    const HZItem = horizon.require('item');
  
    class HZItemRadio extends HZItem {
  
    }
  
    HZItemRadio
      .inheritFields(HZItem)
      .inheritActions(HZItem)
      .field('_type', {default: 'ItemRadio'})
      .field('image', {default: 'radio'})
      .field('shape', {default: {"grid":[{"x":-21,"y":-9.175257731958709},{"x":-21,"y":90.82474226804129}],"offset":{"x":21,"y":9.175257731958709}}})
      .field('name',  {default: 'itemradio'})
      .field('label', {default: 'Radio'})
    ;
  
    return HZItemRadio;
  
  }