module.exports = (horizon, config) => {
     
    const HZItem = horizon.require('item');
  
    class HZItemTeatowel extends HZItem {
  
    }
  
    HZItemTeatowel
      .inheritFields(HZItem)
      .inheritActions(HZItem)
      .field('_type', {default: 'ItemTeatowel'})
      .field('image', {default: 'teatowel'})
      .field('shape', {default: {"grid":[{"x":-27,"y":-10.355678233438539},{"x":73,"y":-10.355678233438539}],"offset":{"x":27,"y":10.355678233438539}}})
      .field('name',  {default: 'itemteatowel'})
      .field('label', {default: 'Teatowel'})
    ;
  
    return HZItemTeatowel;
  
  }