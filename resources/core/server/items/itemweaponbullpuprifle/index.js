module.exports = (horizon, config) => {
     
    const HZItem = horizon.require('item');
  
    class HZItemWeaponbullpuprifle extends HZItem {
  
    }
  
    HZItemWeaponbullpuprifle
      .inheritFields(HZItem)
      .inheritActions(HZItem)
      .field('_type', {default: 'ItemWeaponbullpuprifle'})
      .field('image', {default: 'weaponbullpuprifle'})
      .field('shape', {default: {"grid":[{"x":-14.999999999999915,"y":-9.287066246056725},{"x":85.00000000000009,"y":-9.287066246056725},{"x":85.00000000000009,"y":90.71293375394328},{"x":185.00000000000009,"y":-9.287066246056725},{"x":285.0000000000001,"y":-9.287066246056725}],"offset":{"x":14.999999999999886,"y":9.287066246056725}}})
      .field('name',  {default: 'itemweaponbullpuprifle'})
      .field('label', {default: 'Weaponbullpuprifle'})
    ;
  
    return HZItemWeaponbullpuprifle;
  
  }