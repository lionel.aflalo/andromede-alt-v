module.exports = (horizon, config) => {
     
    const HZItem = horizon.require('item');
  
    class HZItemWeaponvintagepistol extends HZItem {
  
    }
  
    HZItemWeaponvintagepistol
      .inheritFields(HZItem)
      .inheritActions(HZItem)
      .field('_type', {default: 'ItemWeaponvintagepistol'})
      .field('image', {default: 'weaponvintagepistol'})
      .field('shape', {default: {"grid":[{"x":-18.999999999999886,"y":-15.381656804733666},{"x":-18.999999999999886,"y":84.61834319526633},{"x":81.00000000000011,"y":-15.381656804733666},{"x":181.0000000000001,"y":-15.381656804733666}],"offset":{"x":18.999999999999886,"y":15.381656804733666}}})
      .field('name',  {default: 'itemweaponvintagepistol'})
      .field('label', {default: 'Weaponvintagepistol'})
    ;
  
    return HZItemWeaponvintagepistol;
  
  }