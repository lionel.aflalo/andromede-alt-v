module.exports = (horizon, config) => {
     
    const HZItem = horizon.require('item');
  
    class HZItemWeaponmachete extends HZItem {
  
    }
  
    HZItemWeaponmachete
      .inheritFields(HZItem)
      .inheritActions(HZItem)
      .field('_type', {default: 'ItemWeaponmachete'})
      .field('image', {default: 'weaponmachete'})
      .field('shape', {default: {"grid":[{"x":-3.9999999999999716,"y":-32.160115606936415},{"x":96.00000000000003,"y":-32.160115606936415},{"x":196.00000000000003,"y":-32.160115606936415}],"offset":{"x":3.9999999999999716,"y":32.160115606936415}}})
      .field('name',  {default: 'itemweaponmachete'})
      .field('label', {default: 'Weaponmachete'})
    ;
  
    return HZItemWeaponmachete;
  
  }