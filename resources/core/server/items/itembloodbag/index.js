module.exports = (horizon, config) => {
     
    const HZItem = horizon.require('item');
  
    class HZItemBloodbag extends HZItem {
  
    }
  
    HZItemBloodbag
      .inheritFields(HZItem)
      .inheritActions(HZItem)
      .field('_type', {default: 'ItemBloodbag'})
      .field('image', {default: 'bloodbag'})
      .field('shape', {default: {"grid":[{"x":-4,"y":-34.20555555555552},{"x":-4,"y":65.79444444444448}],"offset":{"x":4,"y":34.20555555555552}}})
      .field('name',  {default: 'itembloodbag'})
      .field('label', {default: 'Bloodbag'})
    ;
  
    return HZItemBloodbag;
  
  }