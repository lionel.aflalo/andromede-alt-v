module.exports = (horizon, config) => {
     
    const HZItem = horizon.require('item');
  
    class HZItemWeaponhatchet extends HZItem {
  
    }
  
    HZItemWeaponhatchet
      .inheritFields(HZItem)
      .inheritActions(HZItem)
      .field('_type', {default: 'ItemWeaponhatchet'})
      .field('image', {default: 'weaponhatchet'})
      .field('shape', {default: {"grid":[{"x":-5.999999999999943,"y":-38.44363839285711},{"x":94.00000000000006,"y":-38.44363839285711},{"x":194.00000000000006,"y":-38.44363839285711},{"x":194.00000000000006,"y":61.55636160714289}],"offset":{"x":5.999999999999943,"y":38.44363839285711}}})
      .field('name',  {default: 'itemweaponhatchet'})
      .field('label', {default: 'Weaponhatchet'})
    ;
  
    return HZItemWeaponhatchet;
  
  }