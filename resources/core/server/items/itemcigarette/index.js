module.exports = (horizon, config) => {
     
    const HZItem = horizon.require('item');
  
    class HZItemCigarette extends HZItem {
  
    }
  
    HZItemCigarette
      .inheritFields(HZItem)
      .inheritActions(HZItem)
      .field('_type', {default: 'ItemCigarette'})
      .field('image', {default: 'cigarette'})
      .field('shape', {default: {"grid":[{"x":-20,"y":-4}],"offset":{"x":20,"y":4}}})
      .field('name',  {default: 'itemcigarette'})
      .field('label', {default: 'Cigarette'})
    ;
  
    return HZItemCigarette;
  
  }