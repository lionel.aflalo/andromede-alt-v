module.exports = (horizon, config) => {
     
    const HZItem = horizon.require('item');
  
    class HZItemOrange extends HZItem {
  
    }
  
    HZItemOrange
      .inheritFields(HZItem)
      .inheritActions(HZItem)
      .field('_type', {default: 'ItemOrange'})
      .field('image', {default: 'orange'})
      .field('shape', {default: {"grid":[{"x":-2.9999999999998863,"y":-12.201101928374555}],"offset":{"x":2.9999999999998863,"y":12.201101928374555}}})
      .field('name',  {default: 'itemorange'})
      .field('label', {default: 'Orange'})
    ;
  
    return HZItemOrange;
  
  }