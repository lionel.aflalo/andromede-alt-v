module.exports = (horizon, config) => {
     
    const HZItem = horizon.require('item');
  
    class HZItemDoll extends HZItem {
  
    }
  
    HZItemDoll
      .inheritFields(HZItem)
      .inheritActions(HZItem)
      .field('_type', {default: 'ItemDoll'})
      .field('image', {default: 'doll'})
      .field('shape', {default: {"grid":[{"x":-9.761693579392613,"y":-9.238306420607387}],"offset":{"x":9.761693579392613,"y":9.238306420607387}}})
      .field('name',  {default: 'itemdoll'})
      .field('label', {default: 'Doll'})
    ;
  
    return HZItemDoll;
  
  }