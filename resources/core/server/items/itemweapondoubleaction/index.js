module.exports = (horizon, config) => {
     
    const HZItem = horizon.require('item');
  
    class HZItemWeapondoubleaction extends HZItem {
  
    }
  
    HZItemWeapondoubleaction
      .inheritFields(HZItem)
      .inheritActions(HZItem)
      .field('_type', {default: 'ItemWeapondoubleaction'})
      .field('image', {default: 'weapondoubleaction'})
      .field('shape', {default: {"grid":[{"x":-11.999999999999972,"y":-19.212189616252772},{"x":-11.999999999999972,"y":80.78781038374723},{"x":88.00000000000003,"y":-19.212189616252772},{"x":188.00000000000003,"y":-19.212189616252772}],"offset":{"x":11.999999999999972,"y":19.212189616252772}}})
      .field('name',  {default: 'itemweapondoubleaction'})
      .field('label', {default: 'Weapondoubleaction'})
    ;
  
    return HZItemWeapondoubleaction;
  
  }