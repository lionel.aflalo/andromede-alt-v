module.exports = (horizon, config) => {
     
    const HZItem = horizon.require('item');
  
    class HZItemWeaponsmokegrenade extends HZItem {
  
    }
  
    HZItemWeaponsmokegrenade
      .inheritFields(HZItem)
      .inheritActions(HZItem)
      .field('_type', {default: 'ItemWeaponsmokegrenade'})
      .field('image', {default: 'weaponsmokegrenade'})
      .field('shape', {default: {"grid":[{"x":-17.00000000000017,"y":-19.923076923077133},{"x":82.99999999999983,"y":-19.923076923077133}],"offset":{"x":17.00000000000017,"y":19.923076923077133}}})
      .field('name',  {default: 'itemweaponsmokegrenade'})
      .field('label', {default: 'Weaponsmokegrenade'})
    ;
  
    return HZItemWeaponsmokegrenade;
  
  }