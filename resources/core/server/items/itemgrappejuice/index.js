module.exports = (horizon, config) => {
     
    const HZItem = horizon.require('item');
  
    class HZItemGrappejuice extends HZItem {
  
    }
  
    HZItemGrappejuice
      .inheritFields(HZItem)
      .inheritActions(HZItem)
      .field('_type', {default: 'ItemGrappejuice'})
      .field('image', {default: 'grappejuice'})
      .field('shape', {default: {"grid":[{"x":-26,"y":-3.6977329974811255}],"offset":{"x":26,"y":3.6977329974811255}}})
      .field('name',  {default: 'itemgrappejuice'})
      .field('label', {default: 'Grappejuice'})
    ;
  
    return HZItemGrappejuice;
  
  }