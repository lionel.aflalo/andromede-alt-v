module.exports = (horizon, config) => {
     
    const HZItem = horizon.require('item');
  
    class HZItemWeapongrenade extends HZItem {
  
    }
  
    HZItemWeapongrenade
      .inheritFields(HZItem)
      .inheritActions(HZItem)
      .field('_type', {default: 'ItemWeapongrenade'})
      .field('image', {default: 'weapongrenade'})
      .field('shape', {default: {"grid":[{"x":-21,"y":-9.374157303370794}],"offset":{"x":21,"y":9.374157303370794}}})
      .field('name',  {default: 'itemweapongrenade'})
      .field('label', {default: 'Weapongrenade'})
    ;
  
    return HZItemWeapongrenade;
  
  }