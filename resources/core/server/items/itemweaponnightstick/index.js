module.exports = (horizon, config) => {
     
    const HZItem = horizon.require('item');
  
    class HZItemWeaponnightstick extends HZItem {
  
    }
  
    HZItemWeaponnightstick
      .inheritFields(HZItem)
      .inheritActions(HZItem)
      .field('_type', {default: 'ItemWeaponnightstick'})
      .field('image', {default: 'weaponnightstick'})
      .field('shape', {default: {"grid":[{"x":-2.000000000000199,"y":-51.37276785714289},{"x":-2.000000000000199,"y":48.62723214285711},{"x":97.9999999999998,"y":-51.37276785714289},{"x":197.9999999999998,"y":-51.37276785714289},{"x":297.9999999999998,"y":-51.37276785714289}],"offset":{"x":2.000000000000199,"y":51.37276785714289}}})
      .field('name',  {default: 'itemweaponnightstick'})
      .field('label', {default: 'Weaponnightstick'})
    ;
  
    return HZItemWeaponnightstick;
  
  }