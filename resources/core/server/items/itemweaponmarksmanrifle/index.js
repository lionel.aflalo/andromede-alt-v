module.exports = (horizon, config) => {
     
    const HZItem = horizon.require('item');
  
    class HZItemWeaponmarksmanrifle extends HZItem {
  
    }
  
    HZItemWeaponmarksmanrifle
      .inheritFields(HZItem)
      .inheritActions(HZItem)
      .field('_type', {default: 'ItemWeaponmarksmanrifle'})
      .field('image', {default: 'weaponmarksmanrifle'})
      .field('shape', {default: {"grid":[{"x":-11.999999999999886,"y":-16.656042496680016},{"x":88.00000000000011,"y":-16.656042496680016},{"x":88.00000000000011,"y":83.34395750331998},{"x":188.0000000000001,"y":-16.656042496680016},{"x":188.0000000000001,"y":83.34395750331998},{"x":288.0000000000001,"y":-16.656042496680016},{"x":388.0000000000001,"y":-16.656042496680016}],"offset":{"x":11.999999999999886,"y":16.656042496680016}}})
      .field('name',  {default: 'itemweaponmarksmanrifle'})
      .field('label', {default: 'Weaponmarksmanrifle'})
    ;
  
    return HZItemWeaponmarksmanrifle;
  
  }