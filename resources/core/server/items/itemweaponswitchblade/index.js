module.exports = (horizon, config) => {
     
    const HZItem = horizon.require('item');
  
    class HZItemWeaponswitchblade extends HZItem {
  
    }
  
    HZItemWeaponswitchblade
      .inheritFields(HZItem)
      .inheritActions(HZItem)
      .field('_type', {default: 'ItemWeaponswitchblade'})
      .field('image', {default: 'weaponswitchblade'})
      .field('shape', {default: {"grid":[{"x":-4,"y":-33.63793103448279},{"x":96,"y":-33.63793103448279}],"offset":{"x":4,"y":33.63793103448279}}})
      .field('name',  {default: 'itemweaponswitchblade'})
      .field('label', {default: 'Weaponswitchblade'})
    ;
  
    return HZItemWeaponswitchblade;
  
  }