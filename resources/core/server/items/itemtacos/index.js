module.exports = (horizon, config) => {
     
    const HZItem = horizon.require('item');
  
    class HZItemTacos extends HZItem {
  
    }
  
    HZItemTacos
      .inheritFields(HZItem)
      .inheritActions(HZItem)
      .field('_type', {default: 'ItemTacos'})
      .field('image', {default: 'tacos'})
      .field('shape', {default: {"grid":[{"x":3,"y":-8.999999999999986}],"offset":{"x":97,"y":9}}})
      .field('name',  {default: 'itemtacos'})
      .field('label', {default: 'Tacos'})
    ;
  
    return HZItemTacos;
  
  }