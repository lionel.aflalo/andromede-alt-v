module.exports = (horizon, config) => {
     
    const HZItem = horizon.require('item');
  
    class HZItemWeedseed extends HZItem {
  
    }
  
    HZItemWeedseed
      .inheritFields(HZItem)
      .inheritActions(HZItem)
      .field('_type', {default: 'ItemWeedseed'})
      .field('image', {default: 'weedseed'})
      .field('shape', {default: {"grid":[{"x":-43.00000000000017,"y":-3.0761124121781904},{"x":56.99999999999983,"y":-3.0761124121781904}],"offset":{"x":43.00000000000017,"y":3.0761124121781904}}})
      .field('name',  {default: 'itemweedseed'})
      .field('label', {default: 'Weedseed'})
    ;
  
    return HZItemWeedseed;
  
  }