module.exports = (horizon, config) => {
     
    const HZItem = horizon.require('item');
  
    class HZItemWeaponsnspistolmk2 extends HZItem {
  
    }
  
    HZItemWeaponsnspistolmk2
      .inheritFields(HZItem)
      .inheritActions(HZItem)
      .field('_type', {default: 'ItemWeaponsnspistolmk2'})
      .field('image', {default: 'weaponsnspistolmk2'})
      .field('shape', {default: {"grid":[{"x":-55.00000000000003,"y":-11.310240963855449},{"x":44.99999999999997,"y":-11.310240963855449}],"offset":{"x":55.00000000000006,"y":11.310240963855449}}})
      .field('name',  {default: 'itemweaponsnspistolmk2'})
      .field('label', {default: 'Weaponsnspistolmk2'})
    ;
  
    return HZItemWeaponsnspistolmk2;
  
  }