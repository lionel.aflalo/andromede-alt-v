module.exports = (horizon, config) => {
     
    const HZItem = horizon.require('item');
  
    class HZItemWeaponheavysnipermk2 extends HZItem {
  
    }
  
    HZItemWeaponheavysnipermk2
      .inheritFields(HZItem)
      .inheritActions(HZItem)
      .field('_type', {default: 'ItemWeaponheavysnipermk2'})
      .field('image', {default: 'weaponheavysnipermk2'})
      .field('shape', {default: {"grid":[{"x":-21.000000000000057,"y":-8.26896067415737},{"x":78.99999999999994,"y":-8.26896067415737},{"x":78.99999999999994,"y":91.73103932584263},{"x":178.99999999999994,"y":-8.26896067415737},{"x":178.99999999999994,"y":91.73103932584263},{"x":278.99999999999994,"y":-8.26896067415737},{"x":378.99999999999994,"y":-8.26896067415737},{"x":478.99999999999994,"y":-8.26896067415737}],"offset":{"x":21.000000000000057,"y":8.26896067415737}}})
      .field('name',  {default: 'itemweaponheavysnipermk2'})
      .field('label', {default: 'Weaponheavysnipermk2'})
    ;
  
    return HZItemWeaponheavysnipermk2;
  
  }