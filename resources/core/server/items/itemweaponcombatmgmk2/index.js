module.exports = (horizon, config) => {
     
    const HZItem = horizon.require('item');
  
    class HZItemWeaponcombatmgmk2 extends HZItem {
  
    }
  
    HZItemWeaponcombatmgmk2
      .inheritFields(HZItem)
      .inheritActions(HZItem)
      .field('_type', {default: 'ItemWeaponcombatmgmk2'})
      .field('image', {default: 'weaponcombatmgmk2'})
      .field('shape', {default: {"grid":[{"x":-32.99999999999983,"y":-17.492406542056074},{"x":67.00000000000017,"y":-17.492406542056074},{"x":67.00000000000017,"y":82.50759345794393},{"x":167.00000000000017,"y":-17.492406542056074},{"x":167.00000000000017,"y":82.50759345794393},{"x":267.00000000000017,"y":-17.492406542056074},{"x":367.00000000000017,"y":-17.492406542056074}],"offset":{"x":32.99999999999983,"y":17.492406542056074}}})
      .field('name',  {default: 'itemweaponcombatmgmk2'})
      .field('label', {default: 'Weaponcombatmgmk2'})
    ;
  
    return HZItemWeaponcombatmgmk2;
  
  }