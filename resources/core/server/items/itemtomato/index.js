module.exports = (horizon, config) => {
     
    const HZItem = horizon.require('item');
  
    class HZItemTomato extends HZItem {
  
    }
  
    HZItemTomato
      .inheritFields(HZItem)
      .inheritActions(HZItem)
      .field('_type', {default: 'ItemTomato'})
      .field('image', {default: 'tomato'})
      .field('shape', {default: {"grid":[{"x":-8,"y":-13.931250000000091}],"offset":{"x":8,"y":13.931250000000091}}})
      .field('name',  {default: 'itemtomato'})
      .field('label', {default: 'Tomato'})
    ;
  
    return HZItemTomato;
  
  }