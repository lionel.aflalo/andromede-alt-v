module.exports = (horizon, config) => {
     
    const HZItem = horizon.require('item');
  
    class HZItemWeaponrayminigun extends HZItem {
  
    }
  
    HZItemWeaponrayminigun
      .inheritFields(HZItem)
      .inheritActions(HZItem)
      .field('_type', {default: 'ItemWeaponrayminigun'})
      .field('image', {default: 'weaponrayminigun'})
      .field('shape', {default: {"grid":[{"x":-25.000000000000114,"y":-3.023006134969364},{"x":-25.000000000000114,"y":96.97699386503064},{"x":74.99999999999989,"y":-3.023006134969364},{"x":74.99999999999989,"y":96.97699386503064},{"x":174.9999999999999,"y":-3.023006134969364},{"x":174.9999999999999,"y":96.97699386503064},{"x":274.9999999999999,"y":-3.023006134969364},{"x":274.9999999999999,"y":96.97699386503064},{"x":374.9999999999999,"y":-3.023006134969364},{"x":374.9999999999999,"y":96.97699386503064},{"x":474.9999999999999,"y":-3.023006134969364},{"x":474.9999999999999,"y":96.97699386503064}],"offset":{"x":25.000000000000114,"y":3.023006134969364}}})
      .field('name',  {default: 'itemweaponrayminigun'})
      .field('label', {default: 'Weaponrayminigun'})
    ;
  
    return HZItemWeaponrayminigun;
  
  }