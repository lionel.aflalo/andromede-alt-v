module.exports = (horizon, config) => {
     
    const HZItem = horizon.require('item');
  
    class HZItemWeaponknife extends HZItem {
  
    }
  
    HZItemWeaponknife
      .inheritFields(HZItem)
      .inheritActions(HZItem)
      .field('_type', {default: 'ItemWeaponknife'})
      .field('image', {default: 'weaponknife'})
      .field('shape', {default: {"grid":[{"x":-11.000000000000028,"y":-33.76621989406226},{"x":88.99999999999997,"y":-33.76621989406226}],"offset":{"x":11.000000000000028,"y":33.76621989406226}}})
      .field('name',  {default: 'itemweaponknife'})
      .field('label', {default: 'Weaponknife'})
    ;
  
    return HZItemWeaponknife;
  
  }