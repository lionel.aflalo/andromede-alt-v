module.exports = (horizon, config) => {
     
    const HZItem = horizon.require('item');
  
    class HZItemWeaponbat extends HZItem {
  
    }
  
    HZItemWeaponbat
      .inheritFields(HZItem)
      .inheritActions(HZItem)
      .field('_type', {default: 'ItemWeaponbat'})
      .field('image', {default: 'weaponbat'})
      .field('shape', {default: {"grid":[{"x":-3.9999999999998295,"y":-32.86511024643323},{"x":96.00000000000017,"y":-32.86511024643323},{"x":196.00000000000017,"y":-32.86511024643323},{"x":296.00000000000017,"y":-32.86511024643323}],"offset":{"x":3.9999999999998295,"y":32.86511024643323}}})
      .field('name',  {default: 'itemweaponbat'})
      .field('label', {default: 'Weaponbat'})
    ;
  
    return HZItemWeaponbat;
  
  }