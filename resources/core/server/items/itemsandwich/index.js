module.exports = (horizon, config) => {
     
    const HZItem = horizon.require('item');
  
    class HZItemSandwich extends HZItem {
  
    }
  
    HZItemSandwich
      .inheritFields(HZItem)
      .inheritActions(HZItem)
      .field('_type', {default: 'ItemSandwich'})
      .field('image', {default: 'sandwich'})
      .field('shape', {default: {"grid":[{"x":2,"y":-14}],"offset":{"x":98,"y":14}}})
      .field('name',  {default: 'itemsandwich'})
      .field('label', {default: 'Sandwich'})
    ;
  
    return HZItemSandwich;
  
  }