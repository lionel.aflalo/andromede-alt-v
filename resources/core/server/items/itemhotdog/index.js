module.exports = (horizon, config) => {
     
    const HZItem = horizon.require('item');
  
    class HZItemHotdog extends HZItem {
  
    }
  
    HZItemHotdog
      .inheritFields(HZItem)
      .inheritActions(HZItem)
      .field('_type', {default: 'ItemHotdog'})
      .field('image', {default: 'hotdog'})
      .field('shape', {default: {"grid":[{"x":-6.0000000000001705,"y":-11.999999999999986}],"offset":{"x":6.0000000000001705,"y":11.999999999999986}}})
      .field('name',  {default: 'itemhotdog'})
      .field('label', {default: 'Hotdog'})
    ;
  
    return HZItemHotdog;
  
  }