module.exports = (horizon, config) => {
     
    const HZItem = horizon.require('item');
  
    class HZItemHandcuffkey extends HZItem {
  
    }
  
    HZItemHandcuffkey
      .inheritFields(HZItem)
      .inheritActions(HZItem)
      .field('_type', {default: 'ItemHandcuffkey'})
      .field('image', {default: 'handcuffkey'})
      .field('shape', {default: {"grid":[{"x":-3.999999999999943,"y":-33.555785123966984}],"offset":{"x":3.999999999999943,"y":33.555785123966984}}})
      .field('name',  {default: 'itemhandcuffkey'})
      .field('label', {default: 'Handcuffkey'})
    ;
  
    return HZItemHandcuffkey;
  
  }