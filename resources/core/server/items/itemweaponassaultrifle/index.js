module.exports = (horizon, config) => {
     
    const HZItem = horizon.require('item');
  
    class HZItemWeaponassaultrifle extends HZItem {
  
    }
  
    HZItemWeaponassaultrifle
      .inheritFields(HZItem)
      .inheritActions(HZItem)
      .field('_type', {default: 'ItemWeaponassaultrifle'})
      .field('image', {default: 'weaponassaultrifle'})
      .field('shape', {default: {"grid":[{"x":-14.511889035666968,"y":-9.128275243478356},{"x":85.48811096433303,"y":-9.128275243478356},{"x":85.48811096433303,"y":90.87172475652164},{"x":185.48811096433303,"y":-9.128275243478356},{"x":185.48811096433303,"y":90.87172475652164},{"x":285.48811096433303,"y":-9.128275243478356},{"x":385.48811096433303,"y":-9.128275243478356}],"offset":{"x":14.511889035666968,"y":9.128275243478356}}})
      .field('name',  {default: 'itemweaponassaultrifle'})
      .field('label', {default: 'Weaponassaultrifle'})
    ;
  
    return HZItemWeaponassaultrifle;
  
  }