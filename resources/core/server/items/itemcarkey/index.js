module.exports = (horizon, config) => {
     
    const HZItem = horizon.require('item');
  
    class HZItemCarkey extends HZItem {
  
    }
  
    HZItemCarkey
      .inheritFields(HZItem)
      .inheritActions(HZItem)
      .field('_type', {default: 'ItemCarkey'})
      .field('image', {default: 'carkey'})
      .field('shape', {default: {"grid":[{"x":-2.0000000000001705,"y":-28.203416149068516}],"offset":{"x":2.0000000000001705,"y":28.203416149068516}}})
      .field('name',  {default: 'itemcarkey'})
      .field('label', {default: 'Carkey'})
    ;
  
    return HZItemCarkey;
  
  }