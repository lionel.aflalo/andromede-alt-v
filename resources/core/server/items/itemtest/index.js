module.exports = (horizon, config) => {
     
    const HZItem = horizon.require('item');
  
    class HZItemTest extends HZItem {
  
    }
  
    HZItemTest
      .inheritFields(HZItem)
      .inheritActions(HZItem)
      .field('_type', {default: 'ItemTest'})
      .field('image', {default: 'test'})
      .field('shape', {default: {"grid":[{"x":-109.76169357939261,"y":-109.23830642060739}],"offset":{"x":9.761693579392613,"y":9.238306420607387}}})
      .field('name',  {default: 'itemtest'})
      .field('label', {default: 'Test'})
    ;
  
    return HZItemTest;
  
  }