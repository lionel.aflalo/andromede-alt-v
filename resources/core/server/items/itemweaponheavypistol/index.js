module.exports = (horizon, config) => {
     
    const HZItem = horizon.require('item');
  
    class HZItemWeaponheavypistol extends HZItem {
  
    }
  
    HZItemWeaponheavypistol
      .inheritFields(HZItem)
      .inheritActions(HZItem)
      .field('_type', {default: 'ItemWeaponheavypistol'})
      .field('image', {default: 'weaponheavypistol'})
      .field('shape', {default: {"grid":[{"x":-21.999999999999886,"y":-5.395294117647097},{"x":-21.999999999999886,"y":94.6047058823529},{"x":78.00000000000011,"y":-5.395294117647097},{"x":178.0000000000001,"y":-5.395294117647097}],"offset":{"x":21.999999999999886,"y":5.395294117647097}}})
      .field('name',  {default: 'itemweaponheavypistol'})
      .field('label', {default: 'Weaponheavypistol'})
    ;
  
    return HZItemWeaponheavypistol;
  
  }