module.exports = (horizon, config) => {
     
    const HZItem = horizon.require('item');
  
    class HZItemWeaponmg extends HZItem {
  
    }
  
    HZItemWeaponmg
      .inheritFields(HZItem)
      .inheritActions(HZItem)
      .field('_type', {default: 'ItemWeaponmg'})
      .field('image', {default: 'weaponmg'})
      .field('shape', {default: {"grid":[{"x":-25.000000000000057,"y":-3.3910550458715534},{"x":74.99999999999994,"y":-3.3910550458715534},{"x":74.99999999999994,"y":96.60894495412845},{"x":174.99999999999994,"y":-3.3910550458715534},{"x":274.99999999999994,"y":-3.3910550458715534},{"x":374.99999999999994,"y":-3.3910550458715534},{"x":474.99999999999994,"y":-3.3910550458715534}],"offset":{"x":25.000000000000057,"y":3.3910550458715534}}})
      .field('name',  {default: 'itemweaponmg'})
      .field('label', {default: 'Weaponmg'})
    ;
  
    return HZItemWeaponmg;
  
  }