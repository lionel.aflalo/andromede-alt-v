module.exports = (horizon, config) => {
     
    const HZItem = horizon.require('item');
  
    class HZItemNike extends HZItem {
  
    }
  
    HZItemNike
      .inheritFields(HZItem)
      .inheritActions(HZItem)
      .field('_type', {default: 'ItemNike'})
      .field('image', {default: 'nike'})
      .field('shape', {default: {"grid":[{"x":-3.0000000000002274,"y":63.90179717586636},{"x":-3.0000000000002274,"y":163.90179717586636},{"x":96.99999999999977,"y":-36.09820282413364},{"x":96.99999999999977,"y":63.90179717586636},{"x":96.99999999999977,"y":163.90179717586636},{"x":196.99999999999977,"y":-36.09820282413364},{"x":196.99999999999977,"y":63.90179717586636},{"x":296.9999999999998,"y":-36.09820282413364},{"x":296.9999999999998,"y":63.90179717586636}],"offset":{"x":3.0000000000002274,"y":36.09820282413364}}})
      .field('name',  {default: 'itemnike'})
      .field('label', {default: 'Nike'})
    ;
  
    return HZItemNike;
  
  }