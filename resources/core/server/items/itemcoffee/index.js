module.exports = (horizon, config) => {
     
    const HZItem = horizon.require('item');
  
    class HZItemCoffee extends HZItem {
  
    }
  
    HZItemCoffee
      .inheritFields(HZItem)
      .inheritActions(HZItem)
      .field('_type', {default: 'ItemCoffee'})
      .field('image', {default: 'coffee'})
      .field('shape', {default: {"grid":[{"x":-21.000000000000114,"y":-3.726973684210634}],"offset":{"x":21.000000000000114,"y":3.726973684210634}}})
      .field('name',  {default: 'itemcoffee'})
      .field('label', {default: 'Coffee'})
    ;
  
    return HZItemCoffee;
  
  }