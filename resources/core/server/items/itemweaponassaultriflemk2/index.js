module.exports = (horizon, config) => {
     
    const HZItem = horizon.require('item');
  
    class HZItemWeaponassaultriflemk2 extends HZItem {
  
    }
  
    HZItemWeaponassaultriflemk2
      .inheritFields(HZItem)
      .inheritActions(HZItem)
      .field('_type', {default: 'ItemWeaponassaultriflemk2'})
      .field('image', {default: 'weaponassaultriflemk2'})
      .field('shape', {default: {"grid":[{"x":-12.000000000000142,"y":-21.183937823834185},{"x":87.99999999999986,"y":-21.183937823834185},{"x":87.99999999999986,"y":78.81606217616581},{"x":187.99999999999986,"y":-21.183937823834185},{"x":187.99999999999986,"y":78.81606217616581},{"x":287.9999999999999,"y":-21.183937823834185},{"x":387.9999999999999,"y":-21.183937823834185}],"offset":{"x":12.000000000000142,"y":21.183937823834185}}})
      .field('name',  {default: 'itemweaponassaultriflemk2'})
      .field('label', {default: 'Weaponassaultriflemk2'})
    ;
  
    return HZItemWeaponassaultriflemk2;
  
  }