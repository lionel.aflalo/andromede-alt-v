module.exports = (horizon, config) => {
     
    const HZItem = horizon.require('item');
  
    class HZItemSexbeach extends HZItem {
  
    }
  
    HZItemSexbeach
      .inheritFields(HZItem)
      .inheritActions(HZItem)
      .field('_type', {default: 'ItemSexbeach'})
      .field('image', {default: 'sexbeach'})
      .field('shape', {default: {"grid":[{"x":-30.000000000000227,"y":-2.233766233766346}],"offset":{"x":30.000000000000227,"y":2.233766233766346}}})
      .field('name',  {default: 'itemsexbeach'})
      .field('label', {default: 'Sexbeach'})
    ;
  
    return HZItemSexbeach;
  
  }