module.exports = (horizon, config) => {
     
    const HZItem = horizon.require('item');
  
    class HZItemWeaponraycarbine extends HZItem {
  
    }
  
    HZItemWeaponraycarbine
      .inheritFields(HZItem)
      .inheritActions(HZItem)
      .field('_type', {default: 'ItemWeaponraycarbine'})
      .field('image', {default: 'weaponraycarbine'})
      .field('shape', {default: {"grid":[{"x":-29.00000000000017,"y":-15.762376237623812},{"x":-29.00000000000017,"y":84.23762376237619},{"x":70.99999999999983,"y":-15.762376237623812},{"x":70.99999999999983,"y":84.23762376237619},{"x":170.99999999999983,"y":-15.762376237623812},{"x":170.99999999999983,"y":84.23762376237619},{"x":270.99999999999983,"y":-15.762376237623812},{"x":270.99999999999983,"y":84.23762376237619},{"x":370.99999999999983,"y":-15.762376237623812},{"x":370.99999999999983,"y":84.23762376237619},{"x":470.99999999999983,"y":-15.762376237623812},{"x":470.99999999999983,"y":84.23762376237619}],"offset":{"x":29.00000000000017,"y":15.762376237623812}}})
      .field('name',  {default: 'itemweaponraycarbine'})
      .field('label', {default: 'Weaponraycarbine'})
    ;
  
    return HZItemWeaponraycarbine;
  
  }