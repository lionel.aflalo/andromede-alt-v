module.exports = (horizon, config) => {
     
    const HZItem = horizon.require('item');
  
    class HZItemWeaponminismg extends HZItem {
  
    }
  
    HZItemWeaponminismg
      .inheritFields(HZItem)
      .inheritActions(HZItem)
      .field('_type', {default: 'ItemWeaponminismg'})
      .field('image', {default: 'weaponminismg'})
      .field('shape', {default: {"grid":[{"x":-18.000000000000057,"y":-11.479430379746816},{"x":-18.000000000000057,"y":88.52056962025318},{"x":81.99999999999994,"y":-11.479430379746816},{"x":181.99999999999994,"y":-11.479430379746816}],"offset":{"x":18.000000000000057,"y":11.479430379746816}}})
      .field('name',  {default: 'itemweaponminismg'})
      .field('label', {default: 'Weaponminismg'})
    ;
  
    return HZItemWeaponminismg;
  
  }