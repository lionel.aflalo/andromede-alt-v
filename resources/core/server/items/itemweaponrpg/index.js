module.exports = (horizon, config) => {
     
    const HZItem = horizon.require('item');
  
    class HZItemWeaponrpg extends HZItem {
  
    }
  
    HZItemWeaponrpg
      .inheritFields(HZItem)
      .inheritActions(HZItem)
      .field('_type', {default: 'ItemWeaponrpg'})
      .field('image', {default: 'weaponrpg'})
      .field('shape', {default: {"grid":[{"x":-37.00000000000006,"y":-8.059352517985644},{"x":62.99999999999994,"y":-8.059352517985644},{"x":162.99999999999994,"y":-8.059352517985644},{"x":262.99999999999994,"y":-8.059352517985644},{"x":262.99999999999994,"y":91.94064748201436},{"x":362.99999999999994,"y":-8.059352517985644},{"x":362.99999999999994,"y":91.94064748201436},{"x":462.99999999999994,"y":-8.059352517985644},{"x":563,"y":-8.059352517985644}],"offset":{"x":37.00000000000006,"y":8.059352517985644}}})
      .field('name',  {default: 'itemweaponrpg'})
      .field('label', {default: 'Weaponrpg'})
    ;
  
    return HZItemWeaponrpg;
  
  }