module.exports = (horizon, config) => {
     
    const HZItem = horizon.require('item');
  
    class HZItemWeaponmicrosmg extends HZItem {
  
    }
  
    HZItemWeaponmicrosmg
      .inheritFields(HZItem)
      .inheritActions(HZItem)
      .field('_type', {default: 'ItemWeaponmicrosmg'})
      .field('image', {default: 'weaponmicrosmg'})
      .field('shape', {default: {"grid":[{"x":-18,"y":1},{"x":82,"y":1},{"x":182,"y":1},{"x":182,"y":101},{"x":282,"y":1},{"x":382,"y":1}],"offset":{"x":18,"y":99}}})
      .field('name',  {default: 'itemweaponmicrosmg'})
      .field('label', {default: 'Weaponmicrosmg'})
    ;
  
    return HZItemWeaponmicrosmg;
  
  }