module.exports = (horizon, config) => {
     
    const HZItem = horizon.require('item');
  
    class HZItemWeaponpistolmk2 extends HZItem {
  
    }
  
    HZItemWeaponpistolmk2
      .inheritFields(HZItem)
      .inheritActions(HZItem)
      .field('_type', {default: 'ItemWeaponpistolmk2'})
      .field('image', {default: 'weaponpistolmk2'})
      .field('shape', {default: {"grid":[{"x":-19.999999999999886,"y":-11.789598108746986},{"x":-19.999999999999886,"y":88.21040189125301},{"x":80.00000000000011,"y":-11.789598108746986},{"x":180.0000000000001,"y":-11.789598108746986}],"offset":{"x":19.999999999999886,"y":11.789598108746986}}})
      .field('name',  {default: 'itemweaponpistolmk2'})
      .field('label', {default: 'Weaponpistolmk2'})
    ;
  
    return HZItemWeaponpistolmk2;
  
  }