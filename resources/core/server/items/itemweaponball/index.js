module.exports = (horizon, config) => {
     
    const HZItem = horizon.require('item');
  
    class HZItemWeaponball extends HZItem {
  
    }
  
    HZItemWeaponball
      .inheritFields(HZItem)
      .inheritActions(HZItem)
      .field('_type', {default: 'ItemWeaponball'})
      .field('image', {default: 'weaponball'})
      .field('shape', {default: {"grid":[{"x":-4,"y":-5.75}],"offset":{"x":4,"y":5.75}}})
      .field('name',  {default: 'itemweaponball'})
      .field('label', {default: 'Weaponball'})
    ;
  
    return HZItemWeaponball;
  
  }