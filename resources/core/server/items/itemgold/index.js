module.exports = (horizon, config) => {
     
    const HZItem = horizon.require('item');
  
    class HZItemGold extends HZItem {
  
    }
  
    HZItemGold
      .inheritFields(HZItem)
      .inheritActions(HZItem)
      .field('_type', {default: 'ItemGold'})
      .field('image', {default: 'gold'})
      .field('shape', {default: {"grid":[{"x":-21,"y":-8.787610619468978},{"x":-21,"y":91.21238938053102},{"x":79,"y":-8.787610619468978},{"x":79,"y":91.21238938053102}],"offset":{"x":21,"y":8.787610619468978}}})
      .field('name',  {default: 'itemgold'})
      .field('label', {default: 'Gold'})
    ;
  
    return HZItemGold;
  
  }