module.exports = (horizon, config) => {
     
    const HZItem = horizon.require('item');
  
    class HZItemWeaponbottle extends HZItem {
  
    }
  
    HZItemWeaponbottle
      .inheritFields(HZItem)
      .inheritActions(HZItem)
      .field('_type', {default: 'ItemWeaponbottle'})
      .field('image', {default: 'weaponbottle'})
      .field('shape', {default: {"grid":[{"x":-9.000000000000142,"y":-24.76666666666665},{"x":90.99999999999986,"y":-24.76666666666665}],"offset":{"x":9.000000000000142,"y":24.76666666666665}}})
      .field('name',  {default: 'itemweaponbottle'})
      .field('label', {default: 'Weaponbottle'})
    ;
  
    return HZItemWeaponbottle;
  
  }