module.exports = (horizon, config) => {
     
    const HZItem = horizon.require('item');
  
    class HZItemCocacola extends HZItem {
  
    }
  
    HZItemCocacola
      .inheritFields(HZItem)
      .inheritActions(HZItem)
      .field('_type', {default: 'ItemCocacola'})
      .field('image', {default: 'cocacola'})
      .field('shape', {default: {"grid":[{"x":-25,"y":-4.474813432835845}],"offset":{"x":25,"y":4.474813432835845}}})
      .field('name',  {default: 'itemcocacola'})
      .field('label', {default: 'Cocacola'})
    ;
  
    return HZItemCocacola;
  
  }