module.exports = (horizon, config) => {
     
    const HZItem = horizon.require('item');
  
    class HZItemWeaponsnspistol extends HZItem {
  
    }
  
    HZItemWeaponsnspistol
      .inheritFields(HZItem)
      .inheritActions(HZItem)
      .field('_type', {default: 'ItemWeaponsnspistol'})
      .field('image', {default: 'weaponsnspistol'})
      .field('shape', {default: {"grid":[{"x":-52.000000000000114,"y":-15.045454545454618},{"x":47.999999999999886,"y":-15.045454545454618}],"offset":{"x":52.000000000000114,"y":15.045454545454618}}})
      .field('name',  {default: 'itemweaponsnspistol'})
      .field('label', {default: 'Weaponsnspistol'})
    ;
  
    return HZItemWeaponsnspistol;
  
  }