module.exports = (horizon, config) => {
     
    const HZItem = horizon.require('item');
  
    class HZItemWeaponsmg extends HZItem {
  
    }
  
    HZItemWeaponsmg
      .inheritFields(HZItem)
      .inheritActions(HZItem)
      .field('_type', {default: 'ItemWeaponsmg'})
      .field('image', {default: 'weaponsmg'})
      .field('shape', {default: {"grid":[{"x":-10.999999999999972,"y":-23.671875},{"x":89.00000000000003,"y":-23.671875},{"x":89.00000000000003,"y":76.328125},{"x":189.00000000000003,"y":-23.671875},{"x":289,"y":-23.671875}],"offset":{"x":10.999999999999972,"y":23.671875}}})
      .field('name',  {default: 'itemweaponsmg'})
      .field('label', {default: 'Weaponsmg'})
    ;
  
    return HZItemWeaponsmg;
  
  }