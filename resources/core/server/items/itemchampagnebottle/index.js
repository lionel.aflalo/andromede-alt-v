module.exports = (horizon, config) => {
     
    const HZItem = horizon.require('item');
  
    class HZItemChampagnebottle extends HZItem {
  
    }
  
    HZItemChampagnebottle
      .inheritFields(HZItem)
      .inheritActions(HZItem)
      .field('_type', {default: 'ItemChampagnebottle'})
      .field('image', {default: 'champagnebottle'})
      .field('shape', {default: {"grid":[{"x":-15.999999999999659,"y":-5.399999999999636},{"x":-15.999999999999659,"y":94.60000000000036}],"offset":{"x":15.999999999999659,"y":5.399999999999636}}})
      .field('name',  {default: 'itemchampagnebottle'})
      .field('label', {default: 'Champagnebottle'})
    ;
  
    return HZItemChampagnebottle;
  
  }