module.exports = (horizon, config) => {
     
    const HZItem = horizon.require('item');
  
    class HZItemAdrenaline extends HZItem {
  
    }
  
    HZItemAdrenaline
      .inheritFields(HZItem)
      .inheritActions(HZItem)
      .field('_type', {default: 'ItemAdrenaline'})
      .field('image', {default: 'adrenaline'})
      .field('shape', {default: {"grid":[{"x":-21.87272727272739,"y":-20.12727272727284},{"x":78.12727272727261,"y":-20.12727272727284}],"offset":{"x":21.87272727272739,"y":20.12727272727284}}})
      .field('name',  {default: 'itemadrenaline'})
      .field('label', {default: 'Adrenaline'})
    ;
  
    return HZItemAdrenaline;
  
  }