module.exports = (horizon, config) => {
     
    const HZItem = horizon.require('item');
  
    class HZItemSeed extends HZItem {
  
    }
  
    HZItemSeed
      .inheritFields(HZItem)
      .inheritActions(HZItem)
      .field('_type', {default: 'ItemSeed'})
      .field('image', {default: 'seed'})
      .field('shape', {default: {"grid":[{"x":-32.0982905982906,"y":-6.901709401709397}],"offset":{"x":32.0982905982906,"y":6.901709401709397}}})
      .field('name',  {default: 'itemseed'})
      .field('label', {default: 'Seed'})
    ;
  
    return HZItemSeed;
  
  }