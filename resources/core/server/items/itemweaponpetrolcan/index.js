module.exports = (horizon, config) => {
     
    const HZItem = horizon.require('item');
  
    class HZItemWeaponpetrolcan extends HZItem {
  
    }
  
    HZItemWeaponpetrolcan
      .inheritFields(HZItem)
      .inheritActions(HZItem)
      .field('_type', {default: 'ItemWeaponpetrolcan'})
      .field('image', {default: 'weaponpetrolcan'})
      .field('shape', {default: {"grid":[{"x":-20,"y":-32.736486486486456},{"x":-20,"y":67.26351351351354},{"x":-20,"y":167.26351351351354},{"x":80,"y":-32.736486486486456},{"x":80,"y":67.26351351351354},{"x":80,"y":167.26351351351354}],"offset":{"x":20,"y":32.736486486486456}}})
      .field('name',  {default: 'itemweaponpetrolcan'})
      .field('label', {default: 'Weaponpetrolcan'})
    ;
  
    return HZItemWeaponpetrolcan;
  
  }