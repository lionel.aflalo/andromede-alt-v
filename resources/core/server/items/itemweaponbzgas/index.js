module.exports = (horizon, config) => {
     
    const HZItem = horizon.require('item');
  
    class HZItemWeaponbzgas extends HZItem {
  
    }
  
    HZItemWeaponbzgas
      .inheritFields(HZItem)
      .inheritActions(HZItem)
      .field('_type', {default: 'ItemWeaponbzgas'})
      .field('image', {default: 'weaponbzgas'})
      .field('shape', {default: {"grid":[{"x":-17.00000000000017,"y":-19.923076923077133},{"x":82.99999999999983,"y":-19.923076923077133}],"offset":{"x":17.00000000000017,"y":19.923076923077133}}})
      .field('name',  {default: 'itemweaponbzgas'})
      .field('label', {default: 'Weaponbzgas'})
    ;
  
    return HZItemWeaponbzgas;
  
  }