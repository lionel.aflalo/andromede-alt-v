module.exports = (horizon, config) => {
     
    const HZItem = horizon.require('item');
  
    class HZItemCocabottle extends HZItem {
  
    }
  
    HZItemCocabottle
      .inheritFields(HZItem)
      .inheritActions(HZItem)
      .field('_type', {default: 'ItemCocabottle'})
      .field('image', {default: 'cocabottle'})
      .field('shape', {default: {"grid":[{"x":-8.449894796014632,"y":-4.5501052039853676},{"x":-8.449894796014632,"y":95.44989479601463},{"x":-8.449894796014632,"y":195.44989479601463}],"offset":{"x":8.449894796014632,"y":4.5501052039853676}}})
      .field('name',  {default: 'itemcocabottle'})
      .field('label', {default: 'Cocabottle'})
    ;
  
    return HZItemCocabottle;
  
  }