module.exports = (horizon, config) => {
     
    const HZItem = horizon.require('item');
  
    class HZItemBulletproof extends HZItem {
  
    }
  
    HZItemBulletproof
      .inheritFields(HZItem)
      .inheritActions(HZItem)
      .field('_type', {default: 'ItemBulletproof'})
      .field('image', {default: 'bulletproof'})
      .field('shape', {default: {"grid":[{"x":-15,"y":-9.52235294117645},{"x":-15,"y":90.47764705882355},{"x":85,"y":-9.52235294117645},{"x":85,"y":90.47764705882355}],"offset":{"x":15,"y":9.52235294117645}}})
      .field('name',  {default: 'itembulletproof'})
      .field('label', {default: 'Bulletproof'})
    ;
  
    return HZItemBulletproof;
  
  }