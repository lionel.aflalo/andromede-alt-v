module.exports = (horizon, config) => {
     
    const HZItem = horizon.require('item');
  
    class HZItemFruitsalad extends HZItem {
  
    }
  
    HZItemFruitsalad
      .inheritFields(HZItem)
      .inheritActions(HZItem)
      .field('_type', {default: 'ItemFruitsalad'})
      .field('image', {default: 'fruitsalad'})
      .field('shape', {default: {"grid":[{"x":-5.999999999999773,"y":-8.94059405940584}],"offset":{"x":5.999999999999773,"y":8.94059405940584}}})
      .field('name',  {default: 'itemfruitsalad'})
      .field('label', {default: 'Fruitsalad'})
    ;
  
    return HZItemFruitsalad;
  
  }