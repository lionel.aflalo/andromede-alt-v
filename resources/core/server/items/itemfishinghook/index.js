module.exports = (horizon, config) => {
     
    const HZItem = horizon.require('item');
  
    class HZItemFishinghook extends HZItem {
  
    }
  
    HZItemFishinghook
      .inheritFields(HZItem)
      .inheritActions(HZItem)
      .field('_type', {default: 'ItemFishinghook'})
      .field('image', {default: 'fishinghook'})
      .field('shape', {default: {"grid":[{"x":-11,"y":-15.726400000000012}],"offset":{"x":11,"y":15.726400000000012}}})
      .field('name',  {default: 'itemfishinghook'})
      .field('label', {default: 'Fishinghook'})
    ;
  
    return HZItemFishinghook;
  
  }