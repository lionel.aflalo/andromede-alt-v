module.exports = (horizon, config) => {
     
    const HZItem = horizon.require('item');
  
    class HZItemBurger extends HZItem {
  
    }
  
    HZItemBurger
      .inheritFields(HZItem)
      .inheritActions(HZItem)
      .field('_type', {default: 'ItemBurger'})
      .field('image', {default: 'burger'})
      .field('shape', {default: {"grid":[{"x":-1,"y":-6}],"offset":{"x":1,"y":6}}})
      .field('name',  {default: 'itemburger'})
      .field('label', {default: 'Burger'})
    ;
  
    return HZItemBurger;
  
  }