module.exports = (horizon, config) => {
     
    const HZItem = horizon.require('item');
  
    class HZItemWeaponrevolvermk2 extends HZItem {
  
    }
  
    HZItemWeaponrevolvermk2
      .inheritFields(HZItem)
      .inheritActions(HZItem)
      .field('_type', {default: 'ItemWeaponrevolvermk2'})
      .field('image', {default: 'weaponrevolvermk2'})
      .field('shape', {default: {"grid":[{"x":-15.00000000000017,"y":-17.33928571428578},{"x":-15.00000000000017,"y":82.66071428571422},{"x":84.99999999999983,"y":-17.33928571428578},{"x":184.99999999999983,"y":-17.33928571428578}],"offset":{"x":15.00000000000017,"y":17.33928571428578}}})
      .field('name',  {default: 'itemweaponrevolvermk2'})
      .field('label', {default: 'Weaponrevolvermk2'})
    ;
  
    return HZItemWeaponrevolvermk2;
  
  }