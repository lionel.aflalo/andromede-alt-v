module.exports = (horizon, config) => {
     
    const HZItem = horizon.require('item');
  
    class HZItemWeaponspecialcarbinemk2 extends HZItem {
  
    }
  
    HZItemWeaponspecialcarbinemk2
      .inheritFields(HZItem)
      .inheritActions(HZItem)
      .field('_type', {default: 'ItemWeaponspecialcarbinemk2'})
      .field('image', {default: 'weaponspecialcarbinemk2'})
      .field('shape', {default: {"grid":[{"x":-17.000000000000114,"y":-1.1917344173441506},{"x":82.99999999999989,"y":-1.1917344173441506},{"x":82.99999999999989,"y":98.80826558265585},{"x":182.9999999999999,"y":-1.1917344173441506},{"x":182.9999999999999,"y":98.80826558265585},{"x":282.9999999999999,"y":-1.1917344173441506},{"x":382.9999999999999,"y":-1.1917344173441506}],"offset":{"x":17.000000000000114,"y":1.1917344173441506}}})
      .field('name',  {default: 'itemweaponspecialcarbinemk2'})
      .field('label', {default: 'Weaponspecialcarbinemk2'})
    ;
  
    return HZItemWeaponspecialcarbinemk2;
  
  }