module.exports = (horizon, config) => {
     
    const HZItem = horizon.require('item');
  
    class HZItemWeaponspecialcarbine extends HZItem {
  
    }
  
    HZItemWeaponspecialcarbine
      .inheritFields(HZItem)
      .inheritActions(HZItem)
      .field('_type', {default: 'ItemWeaponspecialcarbine'})
      .field('image', {default: 'weaponspecialcarbine'})
      .field('shape', {default: {"grid":[{"x":-17.5000000000002,"y":-8.648058252427177},{"x":82.4999999999998,"y":-8.648058252427177},{"x":82.4999999999998,"y":91.35194174757282},{"x":182.4999999999998,"y":-8.648058252427177},{"x":182.4999999999998,"y":91.35194174757282},{"x":282.4999999999998,"y":-8.648058252427177}],"offset":{"x":17.500000000000227,"y":8.648058252427177}}})
      .field('name',  {default: 'itemweaponspecialcarbine'})
      .field('label', {default: 'Weaponspecialcarbine'})
    ;
  
    return HZItemWeaponspecialcarbine;
  
  }