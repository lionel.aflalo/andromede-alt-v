module.exports = (horizon, config) => {
     
    const HZItem = horizon.require('item');
  
    class HZItemGrilledhedgehog extends HZItem {
  
    }
  
    HZItemGrilledhedgehog
      .inheritFields(HZItem)
      .inheritActions(HZItem)
      .field('_type', {default: 'ItemGrilledhedgehog'})
      .field('image', {default: 'grilledhedgehog'})
      .field('shape', {default: {"grid":[{"x":3,"y":-13.59228650137743}],"offset":{"x":97,"y":13.59228650137743}}})
      .field('name',  {default: 'itemgrilledhedgehog'})
      .field('label', {default: 'Grilledhedgehog'})
    ;
  
    return HZItemGrilledhedgehog;
  
  }