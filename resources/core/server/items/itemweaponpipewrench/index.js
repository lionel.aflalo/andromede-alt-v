module.exports = (horizon, config) => {
     
    const HZItem = horizon.require('item');
  
    class HZItemWeaponpipewrench extends HZItem {
  
    }
  
    HZItemWeaponpipewrench
      .inheritFields(HZItem)
      .inheritActions(HZItem)
      .field('_type', {default: 'ItemWeaponpipewrench'})
      .field('image', {default: 'weaponpipewrench'})
      .field('shape', {default: {"grid":[{"x":-23.999999999999915,"y":-22.991972477064223},{"x":76.00000000000009,"y":-22.991972477064223},{"x":176.00000000000009,"y":-22.991972477064223},{"x":276.0000000000001,"y":-22.991972477064223}],"offset":{"x":23.999999999999915,"y":22.991972477064223}}})
      .field('name',  {default: 'itemweaponpipewrench'})
      .field('label', {default: 'Weaponpipewrench'})
    ;
  
    return HZItemWeaponpipewrench;
  
  }