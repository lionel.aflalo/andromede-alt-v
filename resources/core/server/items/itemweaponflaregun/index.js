module.exports = (horizon, config) => {
     
    const HZItem = horizon.require('item');
  
    class HZItemWeaponflaregun extends HZItem {
  
    }
  
    HZItemWeaponflaregun
      .inheritFields(HZItem)
      .inheritActions(HZItem)
      .field('_type', {default: 'ItemWeaponflaregun'})
      .field('image', {default: 'weaponflaregun'})
      .field('shape', {default: {"grid":[{"x":-16.000000000000227,"y":-9.3456486042694},{"x":-16.000000000000227,"y":90.6543513957306},{"x":83.99999999999977,"y":-9.3456486042694},{"x":183.99999999999977,"y":-9.3456486042694}],"offset":{"x":16.000000000000227,"y":9.3456486042694}}})
      .field('name',  {default: 'itemweaponflaregun'})
      .field('label', {default: 'Weaponflaregun'})
    ;
  
    return HZItemWeaponflaregun;
  
  }