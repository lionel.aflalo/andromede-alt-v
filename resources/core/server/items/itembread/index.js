module.exports = (horizon, config) => {
     
    const HZItem = horizon.require('item');
  
    class HZItemBread extends HZItem {
  
    }
  
    HZItemBread
      .inheritFields(HZItem)
      .inheritActions(HZItem)
      .field('_type', {default: 'ItemBread'})
      .field('image', {default: 'bread'})
      .field('shape', {default: {"grid":[{"x":-1,"y":-10.53125}],"offset":{"x":1,"y":10.53125}}})
      .field('name',  {default: 'itembread'})
      .field('label', {default: 'Bread'})
    ;
  
    return HZItemBread;
  
  }