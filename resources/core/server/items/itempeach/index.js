module.exports = (horizon, config) => {
     
    const HZItem = horizon.require('item');
  
    class HZItemPeach extends HZItem {
  
    }
  
    HZItemPeach
      .inheritFields(HZItem)
      .inheritActions(HZItem)
      .field('_type', {default: 'ItemPeach'})
      .field('image', {default: 'peach'})
      .field('shape', {default: {"grid":[{"x":-5.9999999999998295,"y":-10.169014084507126}],"offset":{"x":5.9999999999998295,"y":10.169014084507126}}})
      .field('name',  {default: 'itempeach'})
      .field('label', {default: 'Peach'})
    ;
  
    return HZItemPeach;
  
  }