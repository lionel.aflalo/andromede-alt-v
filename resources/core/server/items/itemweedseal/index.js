module.exports = (horizon, config) => {
     
    const HZItem = horizon.require('item');
  
    class HZItemWeedseal extends HZItem {
  
    }
  
    HZItemWeedseal
      .inheritFields(HZItem)
      .inheritActions(HZItem)
      .field('_type', {default: 'ItemWeedseal'})
      .field('image', {default: 'weedseal'})
      .field('shape', {default: {"grid":[{"x":-3.5000000000001137,"y":-39.03676470588243}],"offset":{"x":3.5000000000001137,"y":39.03676470588243}}})
      .field('name',  {default: 'itemweedseal'})
      .field('label', {default: 'Weedseal'})
    ;
  
    return HZItemWeedseal;
  
  }