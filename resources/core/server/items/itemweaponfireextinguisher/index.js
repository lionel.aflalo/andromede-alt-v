module.exports = (horizon, config) => {
     
    const HZItem = horizon.require('item');
  
    class HZItemWeaponfireextinguisher extends HZItem {
  
    }
  
    HZItemWeaponfireextinguisher
      .inheritFields(HZItem)
      .inheritActions(HZItem)
      .field('_type', {default: 'ItemWeaponfireextinguisher'})
      .field('image', {default: 'weaponfireextinguisher'})
      .field('shape', {default: {"grid":[{"x":-54,"y":-33},{"x":46,"y":-33},{"x":46,"y":67},{"x":46,"y":167}],"offset":{"x":54,"y":33}}})
      .field('name',  {default: 'itemweaponfireextinguisher'})
      .field('label', {default: 'Weaponfireextinguisher'})
    ;
  
    return HZItemWeaponfireextinguisher;
  
  }