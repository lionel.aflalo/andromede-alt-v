module.exports = (horizon, config) => {
     
    const HZItem = horizon.require('item');
  
    class HZItemWeaponbullpupriflemk2 extends HZItem {
  
    }
  
    HZItemWeaponbullpupriflemk2
      .inheritFields(HZItem)
      .inheritActions(HZItem)
      .field('_type', {default: 'ItemWeaponbullpupriflemk2'})
      .field('image', {default: 'weaponbullpupriflemk2'})
      .field('shape', {default: {"grid":[{"x":-59.00000000000006,"y":-3.918954248365935},{"x":40.99999999999994,"y":-3.918954248365935},{"x":40.99999999999994,"y":96.08104575163406},{"x":140.99999999999994,"y":-3.918954248365935},{"x":140.99999999999994,"y":96.08104575163406},{"x":240.99999999999994,"y":-3.918954248365935},{"x":340.99999999999994,"y":-3.918954248365935}],"offset":{"x":59.00000000000006,"y":3.918954248365935}}})
      .field('name',  {default: 'itemweaponbullpupriflemk2'})
      .field('label', {default: 'Weaponbullpupriflemk2'})
    ;
  
    return HZItemWeaponbullpupriflemk2;
  
  }