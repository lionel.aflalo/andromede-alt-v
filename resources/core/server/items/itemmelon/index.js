module.exports = (horizon, config) => {
     
    const HZItem = horizon.require('item');
  
    class HZItemMelon extends HZItem {
  
    }
  
    HZItemMelon
      .inheritFields(HZItem)
      .inheritActions(HZItem)
      .field('_type', {default: 'ItemMelon'})
      .field('image', {default: 'melon'})
      .field('shape', {default: {"grid":[{"x":-59,"y":-3.4944751381215156},{"x":41,"y":-3.4944751381215156}],"offset":{"x":59,"y":3.4944751381215156}}})
      .field('name',  {default: 'itemmelon'})
      .field('label', {default: 'Melon'})
    ;
  
    return HZItemMelon;
  
  }