module.exports = (horizon, config) => {
     
    const HZItem = horizon.require('item');
  
    class HZItemWeaponcombatpdw extends HZItem {
  
    }
  
    HZItemWeaponcombatpdw
      .inheritFields(HZItem)
      .inheritActions(HZItem)
      .field('_type', {default: 'ItemWeaponcombatpdw'})
      .field('image', {default: 'weaponcombatpdw'})
      .field('shape', {default: {"grid":[{"x":-15.999999999999972,"y":-24.01627604166663},{"x":84.00000000000003,"y":-24.01627604166663},{"x":84.00000000000003,"y":75.98372395833337},{"x":184.00000000000003,"y":-24.01627604166663},{"x":284,"y":-24.01627604166663}],"offset":{"x":15.999999999999972,"y":24.01627604166663}}})
      .field('name',  {default: 'itemweaponcombatpdw'})
      .field('label', {default: 'Weaponcombatpdw'})
    ;
  
    return HZItemWeaponcombatpdw;
  
  }