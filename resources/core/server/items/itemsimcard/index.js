module.exports = (horizon, config) => {
     
    const HZItem = horizon.require('item');
  
    class HZItemSimcard extends HZItem {
  
    }
  
    HZItemSimcard
      .inheritFields(HZItem)
      .inheritActions(HZItem)
      .field('_type', {default: 'ItemSimcard'})
      .field('image', {default: 'simcard'})
      .field('shape', {default: {"grid":[{"x":-2,"y":-19.000000000000004}],"offset":{"x":2,"y":19}}})
      .field('name',  {default: 'itemsimcard'})
      .field('label', {default: 'Simcard'})
    ;
  
    return HZItemSimcard;
  
  }