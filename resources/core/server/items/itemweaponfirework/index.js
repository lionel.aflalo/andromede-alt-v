module.exports = (horizon, config) => {
     
    const HZItem = horizon.require('item');
  
    class HZItemWeaponfirework extends HZItem {
  
    }
  
    HZItemWeaponfirework
      .inheritFields(HZItem)
      .inheritActions(HZItem)
      .field('_type', {default: 'ItemWeaponfirework'})
      .field('image', {default: 'weaponfirework'})
      .field('shape', {default: {"grid":[{"x":-34.000000000000085,"y":-4.937908496732007},{"x":65.99999999999991,"y":-4.937908496732007},{"x":165.99999999999991,"y":-4.937908496732007},{"x":165.99999999999991,"y":95.062091503268},{"x":265.9999999999999,"y":-4.937908496732007},{"x":265.9999999999999,"y":95.062091503268},{"x":365.9999999999999,"y":-4.937908496732007},{"x":465.9999999999999,"y":-4.937908496732007}],"offset":{"x":34.000000000000114,"y":4.937908496732007}}})
      .field('name',  {default: 'itemweaponfirework'})
      .field('label', {default: 'Weaponfirework'})
    ;
  
    return HZItemWeaponfirework;
  
  }