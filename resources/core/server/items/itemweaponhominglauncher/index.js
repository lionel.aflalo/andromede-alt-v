module.exports = (horizon, config) => {
     
    const HZItem = horizon.require('item');
  
    class HZItemWeaponhominglauncher extends HZItem {
  
    }
  
    HZItemWeaponhominglauncher
      .inheritFields(HZItem)
      .inheritActions(HZItem)
      .field('_type', {default: 'ItemWeaponhominglauncher'})
      .field('image', {default: 'weaponhominglauncher'})
      .field('shape', {default: {"grid":[{"x":-23.000000000000057,"y":-53.68192090395485},{"x":-23.000000000000057,"y":46.31807909604515},{"x":76.99999999999994,"y":-53.68192090395485},{"x":76.99999999999994,"y":46.31807909604515},{"x":176.99999999999994,"y":-53.68192090395485},{"x":176.99999999999994,"y":46.31807909604515},{"x":276.99999999999994,"y":-53.68192090395485},{"x":276.99999999999994,"y":46.31807909604515},{"x":376.99999999999994,"y":-53.68192090395485},{"x":376.99999999999994,"y":46.31807909604515},{"x":476.99999999999994,"y":-53.68192090395485},{"x":476.99999999999994,"y":46.31807909604515}],"offset":{"x":23.000000000000057,"y":53.68192090395485}}})
      .field('name',  {default: 'itemweaponhominglauncher'})
      .field('label', {default: 'Weaponhominglauncher'})
    ;
  
    return HZItemWeaponhominglauncher;
  
  }