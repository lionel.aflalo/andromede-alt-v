module.exports = (horizon, config) => {
     
    const HZItem = horizon.require('item');
  
    class HZItemWeaponcrowbar extends HZItem {
  
    }
  
    HZItemWeaponcrowbar
      .inheritFields(HZItem)
      .inheritActions(HZItem)
      .field('_type', {default: 'ItemWeaponcrowbar'})
      .field('image', {default: 'weaponcrowbar'})
      .field('shape', {default: {"grid":[{"x":-7.000000000000085,"y":-9.654999999999973},{"x":92.99999999999991,"y":-9.654999999999973},{"x":192.99999999999991,"y":-9.654999999999973},{"x":292.9999999999999,"y":-9.654999999999973}],"offset":{"x":7.000000000000114,"y":9.654999999999973}}})
      .field('name',  {default: 'itemweaponcrowbar'})
      .field('label', {default: 'Weaponcrowbar'})
    ;
  
    return HZItemWeaponcrowbar;
  
  }