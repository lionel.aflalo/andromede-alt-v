module.exports = (horizon, config) => {
     
    const HZItem = horizon.require('item');
  
    class HZItemWeaponcombatmg extends HZItem {
  
    }
  
    HZItemWeaponcombatmg
      .inheritFields(HZItem)
      .inheritActions(HZItem)
      .field('_type', {default: 'ItemWeaponcombatmg'})
      .field('image', {default: 'weaponcombatmg'})
      .field('shape', {default: {"grid":[{"x":-46.999999999999886,"y":-0.9053156146179617},{"x":53.000000000000114,"y":-0.9053156146179617},{"x":53.000000000000114,"y":99.09468438538204},{"x":153.0000000000001,"y":-0.9053156146179617},{"x":153.0000000000001,"y":99.09468438538204},{"x":253.0000000000001,"y":-0.9053156146179617},{"x":353.0000000000001,"y":-0.9053156146179617},{"x":453.0000000000001,"y":-0.9053156146179617}],"offset":{"x":46.999999999999886,"y":0.9053156146179617}}})
      .field('name',  {default: 'itemweaponcombatmg'})
      .field('label', {default: 'Weaponcombatmg'})
    ;
  
    return HZItemWeaponcombatmg;
  
  }