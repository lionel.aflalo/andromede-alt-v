module.exports = (horizon, config) => {
     
    const HZItem = horizon.require('item');
  
    class HZItemWeaponmolotov extends HZItem {
  
    }
  
    HZItemWeaponmolotov
      .inheritFields(HZItem)
      .inheritActions(HZItem)
      .field('_type', {default: 'ItemWeaponmolotov'})
      .field('image', {default: 'weaponmolotov'})
      .field('shape', {default: {"grid":[{"x":-21,"y":-11.541916167664567},{"x":-21,"y":88.45808383233543}],"offset":{"x":21,"y":11.541916167664567}}})
      .field('name',  {default: 'itemweaponmolotov'})
      .field('label', {default: 'Weaponmolotov'})
    ;
  
    return HZItemWeaponmolotov;
  
  }