module.exports = (horizon, config) => {
     
    const HZItem = horizon.require('item');
  
    class HZItemWeapondbshotgun extends HZItem {
  
    }
  
    HZItemWeapondbshotgun
      .inheritFields(HZItem)
      .inheritActions(HZItem)
      .field('_type', {default: 'ItemWeapondbshotgun'})
      .field('image', {default: 'weapondbshotgun'})
      .field('shape', {default: {"grid":[{"x":-24.999999999999943,"y":-24.355402542372815},{"x":-24.999999999999943,"y":75.64459745762719},{"x":75.00000000000006,"y":-24.355402542372815},{"x":175.00000000000006,"y":-24.355402542372815},{"x":275.00000000000006,"y":-24.355402542372815}],"offset":{"x":24.999999999999943,"y":24.355402542372815}}})
      .field('name',  {default: 'itemweapondbshotgun'})
      .field('label', {default: 'Weapondbshotgun'})
    ;
  
    return HZItemWeapondbshotgun;
  
  }