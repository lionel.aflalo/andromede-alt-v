module.exports = (horizon, config) => {
     
    const HZItem = horizon.require('item');
  
    class HZItemWeaponmarksmanpistol extends HZItem {
  
    }
  
    HZItemWeaponmarksmanpistol
      .inheritFields(HZItem)
      .inheritActions(HZItem)
      .field('_type', {default: 'ItemWeaponmarksmanpistol'})
      .field('image', {default: 'weaponmarksmanpistol'})
      .field('shape', {default: {"grid":[{"x":-26.99999999999949,"y":-12.626752336448476},{"x":-26.99999999999949,"y":87.37324766355152},{"x":73.00000000000051,"y":-12.626752336448476},{"x":173.0000000000005,"y":-12.626752336448476},{"x":273.0000000000005,"y":-12.626752336448476}],"offset":{"x":26.99999999999949,"y":12.626752336448476}}})
      .field('name',  {default: 'itemweaponmarksmanpistol'})
      .field('label', {default: 'Weaponmarksmanpistol'})
    ;
  
    return HZItemWeaponmarksmanpistol;
  
  }