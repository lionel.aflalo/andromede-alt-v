module.exports = (horizon, config) => {
     
    const HZItem = horizon.require('item');
  
    class HZItemDufflebag extends HZItem {
  
    }
  
    HZItemDufflebag
      .inheritFields(HZItem)
      .inheritActions(HZItem)
      .field('_type', {default: 'ItemDufflebag'})
      .field('image', {default: 'dufflebag'})
      .field('shape', {default: {"grid":[],"offset":{"x":33.703703703703866,"y":13.296296296296305}}})
      .field('name',  {default: 'itemdufflebag'})
      .field('label', {default: 'Dufflebag'})
    ;
  
    return HZItemDufflebag;
  
  }