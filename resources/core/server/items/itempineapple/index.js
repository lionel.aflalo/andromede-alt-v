module.exports = (horizon, config) => {
     
    const HZItem = horizon.require('item');
  
    class HZItemPineapple extends HZItem {
  
    }
  
    HZItemPineapple
      .inheritFields(HZItem)
      .inheritActions(HZItem)
      .field('_type', {default: 'ItemPineapple'})
      .field('image', {default: 'pineapple'})
      .field('shape', {default: {"grid":[{"x":-10.66023942937818,"y":-7.33976057062182},{"x":-10.66023942937818,"y":92.66023942937818}],"offset":{"x":10.66023942937818,"y":7.33976057062182}}})
      .field('name',  {default: 'itempineapple'})
      .field('label', {default: 'Pineapple'})
    ;
  
    return HZItemPineapple;
  
  }