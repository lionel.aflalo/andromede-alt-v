module.exports = (horizon, config) => {
     
    const HZItem = horizon.require('item');
  
    class HZItemWeapongusenberg extends HZItem {
  
    }
  
    HZItemWeapongusenberg
      .inheritFields(HZItem)
      .inheritActions(HZItem)
      .field('_type', {default: 'ItemWeapongusenberg'})
      .field('image', {default: 'weapongusenberg'})
      .field('shape', {default: {"grid":[{"x":-20.000000000000057,"y":-1.5683510963382332},{"x":79.99999999999994,"y":-1.5683510963382332},{"x":179.99999999999994,"y":-1.5683510963382332},{"x":279.99999999999994,"y":-1.5683510963382332},{"x":379.99999999999994,"y":-1.5683510963382332}],"offset":{"x":20.000000000000057,"y":1.5683510963382332}}})
      .field('name',  {default: 'itemweapongusenberg'})
      .field('label', {default: 'Weapongusenberg'})
    ;
  
    return HZItemWeapongusenberg;
  
  }