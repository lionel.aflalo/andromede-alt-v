module.exports = (horizon, config) => {
     
    const HZItem = horizon.require('item');
  
    class HZItemWeaponsmgmk2 extends HZItem {
  
    }
  
    HZItemWeaponsmgmk2
      .inheritFields(HZItem)
      .inheritActions(HZItem)
      .field('_type', {default: 'ItemWeaponsmgmk2'})
      .field('image', {default: 'weaponsmgmk2'})
      .field('shape', {default: {"grid":[{"x":-4.999999999999915,"y":-11.153225806451474},{"x":-4.999999999999915,"y":88.84677419354853},{"x":95.00000000000009,"y":-11.153225806451474},{"x":95.00000000000009,"y":88.84677419354853},{"x":195.00000000000009,"y":-11.153225806451474}],"offset":{"x":4.999999999999915,"y":11.153225806451474}}})
      .field('name',  {default: 'itemweaponsmgmk2'})
      .field('label', {default: 'Weaponsmgmk2'})
    ;
  
    return HZItemWeaponsmgmk2;
  
  }