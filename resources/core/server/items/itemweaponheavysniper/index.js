module.exports = (horizon, config) => {
     
    const HZItem = horizon.require('item');
  
    class HZItemWeaponheavysniper extends HZItem {
  
    }
  
    HZItemWeaponheavysniper
      .inheritFields(HZItem)
      .inheritActions(HZItem)
      .field('_type', {default: 'ItemWeaponheavysniper'})
      .field('image', {default: 'weaponheavysniper'})
      .field('shape', {default: {"grid":[{"x":-16.000000000000057,"y":-10.471929824561471},{"x":83.99999999999994,"y":-10.471929824561471},{"x":83.99999999999994,"y":89.52807017543853},{"x":183.99999999999994,"y":-10.471929824561471},{"x":183.99999999999994,"y":89.52807017543853},{"x":283.99999999999994,"y":-10.471929824561471},{"x":383.99999999999994,"y":-10.471929824561471},{"x":483.99999999999994,"y":-10.471929824561471},{"x":584,"y":-10.471929824561471}],"offset":{"x":16.000000000000057,"y":10.471929824561471}}})
      .field('name',  {default: 'itemweaponheavysniper'})
      .field('label', {default: 'Weaponheavysniper'})
    ;
  
    return HZItemWeaponheavysniper;
  
  }