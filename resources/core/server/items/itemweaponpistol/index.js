module.exports = (horizon, config) => {
     
    const HZItem = horizon.require('item');
  
    class HZItemWeaponpistol extends HZItem {
  
    }
  
    HZItemWeaponpistol
      .inheritFields(HZItem)
      .inheritActions(HZItem)
      .field('_type', {default: 'ItemWeaponpistol'})
      .field('image', {default: 'weaponpistol'})
      .field('shape', {default: {"grid":[{"x":-9.000000000000085,"y":-1.8106060606060055},{"x":-9.000000000000085,"y":98.189393939394},{"x":90.99999999999991,"y":-1.8106060606060055},{"x":190.99999999999991,"y":-1.8106060606060055}],"offset":{"x":9.000000000000085,"y":1.8106060606060055}}})
      .field('name',  {default: 'itemweaponpistol'})
      .field('label', {default: 'Weaponpistol'})
    ;
  
    return HZItemWeaponpistol;
  
  }