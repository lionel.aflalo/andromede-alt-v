module.exports = (horizon, config) => {
     
    const HZItem = horizon.require('item');
  
    class HZItemWeaponflashlight extends HZItem {
  
    }
  
    HZItemWeaponflashlight
      .inheritFields(HZItem)
      .inheritActions(HZItem)
      .field('_type', {default: 'ItemWeaponflashlight'})
      .field('image', {default: 'weaponflashlight'})
      .field('shape', {default: {"grid":[{"x":-11.999999999999886,"y":-36.72872996300862},{"x":88.00000000000011,"y":-36.72872996300862}],"offset":{"x":11.999999999999886,"y":36.72872996300862}}})
      .field('name',  {default: 'itemweaponflashlight'})
      .field('label', {default: 'Weaponflashlight'})
    ;
  
    return HZItemWeaponflashlight;
  
  }