module.exports = (horizon, config) => {
     
    const HZItem = horizon.require('item');
  
    class HZItemWeaponraypistol extends HZItem {
  
    }
  
    HZItemWeaponraypistol
      .inheritFields(HZItem)
      .inheritActions(HZItem)
      .field('_type', {default: 'ItemWeaponraypistol'})
      .field('image', {default: 'weaponraypistol'})
      .field('shape', {default: {"grid":[{"x":-3.999999999999858,"y":-10.383211678832026},{"x":-3.999999999999858,"y":89.61678832116797},{"x":96.00000000000014,"y":-10.383211678832026},{"x":96.00000000000014,"y":89.61678832116797},{"x":196.00000000000014,"y":-10.383211678832026}],"offset":{"x":3.999999999999858,"y":10.383211678832026}}})
      .field('name',  {default: 'itemweaponraypistol'})
      .field('label', {default: 'Weaponraypistol'})
    ;
  
    return HZItemWeaponraypistol;
  
  }