module.exports = (horizon, config) => {
     
    const HZItem = horizon.require('item');
  
    class HZItemWeaponsniperrifle extends HZItem {
  
    }
  
    HZItemWeaponsniperrifle
      .inheritFields(HZItem)
      .inheritActions(HZItem)
      .field('_type', {default: 'ItemWeaponsniperrifle'})
      .field('image', {default: 'weaponsniperrifle'})
      .field('shape', {default: {"grid":[{"x":-19.999999999999886,"y":-8.241016109045859},{"x":80.00000000000011,"y":-8.241016109045859},{"x":180.0000000000001,"y":-8.241016109045859},{"x":280.0000000000001,"y":-8.241016109045859},{"x":380.0000000000001,"y":-8.241016109045859},{"x":480.0000000000001,"y":-8.241016109045859}],"offset":{"x":19.999999999999886,"y":8.241016109045859}}})
      .field('name',  {default: 'itemweaponsniperrifle'})
      .field('label', {default: 'Weaponsniperrifle'})
    ;
  
    return HZItemWeaponsniperrifle;
  
  }