module.exports = (horizon, config) => {
     
    const HZItem = horizon.require('item');
  
    class HZItemSausage extends HZItem {
  
    }
  
    HZItemSausage
      .inheritFields(HZItem)
      .inheritActions(HZItem)
      .field('_type', {default: 'ItemSausage'})
      .field('image', {default: 'sausage'})
      .field('shape', {default: {"grid":[{"x":-1.6236933797908932,"y":-40.37630662020911}],"offset":{"x":1.6236933797908932,"y":40.37630662020911}}})
      .field('name',  {default: 'itemsausage'})
      .field('label', {default: 'Sausage'})
    ;
  
    return HZItemSausage;
  
  }