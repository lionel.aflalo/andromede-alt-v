module.exports = (horizon, config) => {
     
    const HZItem = horizon.require('item');
  
    class HZItemWeaponassaultsmg extends HZItem {
  
    }
  
    HZItemWeaponassaultsmg
      .inheritFields(HZItem)
      .inheritActions(HZItem)
      .field('_type', {default: 'ItemWeaponassaultsmg'})
      .field('image', {default: 'weaponassaultsmg'})
      .field('shape', {default: {"grid":[{"x":-17.00000000000003,"y":-3.9268867924528195},{"x":82.99999999999997,"y":-3.9268867924528195},{"x":82.99999999999997,"y":96.07311320754718},{"x":182.99999999999997,"y":-3.9268867924528195},{"x":283,"y":-3.9268867924528195}],"offset":{"x":17.00000000000003,"y":3.9268867924528195}}})
      .field('name',  {default: 'itemweaponassaultsmg'})
      .field('label', {default: 'Weaponassaultsmg'})
    ;
  
    return HZItemWeaponassaultsmg;
  
  }