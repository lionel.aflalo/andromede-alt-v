module.exports = (horizon, config) => {
     
    const HZItem = horizon.require('item');
  
    class HZItemPhone extends HZItem {
  
    }
  
    HZItemPhone
      .inheritFields(HZItem)
      .inheritActions(HZItem)
      .field('_type', {default: 'ItemPhone'})
      .field('image', {default: 'phone'})
      .field('shape', {default: {"grid":[{"x":-7.999999999999545,"y":-18.99135802469118},{"x":-7.999999999999545,"y":81.00864197530882}],"offset":{"x":7.999999999999545,"y":18.99135802469118}}})
      .field('name',  {default: 'itemphone'})
      .field('label', {default: 'Phone'})
    ;
  
    return HZItemPhone;
  
  }