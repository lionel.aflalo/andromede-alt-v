module.exports = (horizon, config) => {
     
    const HZItem = horizon.require('item');
  
    class HZItemDefibrillator extends HZItem {
  
    }
  
    HZItemDefibrillator
      .inheritFields(HZItem)
      .inheritActions(HZItem)
      .field('_type', {default: 'ItemDefibrillator'})
      .field('image', {default: 'defibrillator'})
      .field('shape', {default: {"grid":[{"x":-14.000000000000114,"y":-14.66545012165443},{"x":-14.000000000000114,"y":85.33454987834557},{"x":85.99999999999989,"y":-14.66545012165443},{"x":85.99999999999989,"y":85.33454987834557},{"x":185.9999999999999,"y":-14.66545012165443},{"x":185.9999999999999,"y":85.33454987834557}],"offset":{"x":14.000000000000114,"y":14.66545012165443}}})
      .field('name',  {default: 'itemdefibrillator'})
      .field('label', {default: 'Defibrillator'})
    ;
  
    return HZItemDefibrillator;
  
  }