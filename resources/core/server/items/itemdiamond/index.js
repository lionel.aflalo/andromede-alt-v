module.exports = (horizon, config) => {
     
    const HZItem = horizon.require('item');
  
    class HZItemDiamond extends HZItem {
  
    }
  
    HZItemDiamond
      .inheritFields(HZItem)
      .inheritActions(HZItem)
      .field('_type', {default: 'ItemDiamond'})
      .field('image', {default: 'diamond'})
      .field('shape', {default: {"grid":[{"x":-1.9999999999995453,"y":-24.89691943127957}],"offset":{"x":1.9999999999995453,"y":24.89691943127957}}})
      .field('name',  {default: 'itemdiamond'})
      .field('label', {default: 'Diamond'})
    ;
  
    return HZItemDiamond;
  
  }