module.exports = (horizon, config) => {
     
    const HZItem = horizon.require('item');
  
    class HZItemWeaponpoolcue extends HZItem {
  
    }
  
    HZItemWeaponpoolcue
      .inheritFields(HZItem)
      .inheritActions(HZItem)
      .field('_type', {default: 'ItemWeaponpoolcue'})
      .field('image', {default: 'weaponpoolcue'})
      .field('shape', {default: {"grid":[{"x":-9.000000000000028,"y":-39.81322957198449},{"x":90.99999999999997,"y":-39.81322957198449},{"x":190.99999999999997,"y":-39.81322957198449},{"x":291,"y":-39.81322957198449}],"offset":{"x":9.000000000000028,"y":39.81322957198449}}})
      .field('name',  {default: 'itemweaponpoolcue'})
      .field('label', {default: 'Weaponpoolcue'})
    ;
  
    return HZItemWeaponpoolcue;
  
  }