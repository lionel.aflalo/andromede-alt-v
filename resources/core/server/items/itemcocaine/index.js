module.exports = (horizon, config) => {
     
    const HZItem = horizon.require('item');
  
    class HZItemCocaine extends HZItem {
  
    }
  
    HZItemCocaine
      .inheritFields(HZItem)
      .inheritActions(HZItem)
      .field('_type', {default: 'ItemCocaine'})
      .field('image', {default: 'cocaine'})
      .field('shape', {default: {"grid":[{"x":-3.410605131648481e-13,"y":-21.672093023256025}],"offset":{"x":3.410605131648481e-13,"y":21.672093023256025}}})
      .field('name',  {default: 'itemcocaine'})
      .field('label', {default: 'Cocaine'})
    ;
  
    return HZItemCocaine;
  
  }