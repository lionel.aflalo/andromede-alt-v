module.exports = (horizon, config) => {
     
    const HZItem = horizon.require('item');
  
    class HZItemDoliprane extends HZItem {
  
    }
  
    HZItemDoliprane
      .inheritFields(HZItem)
      .inheritActions(HZItem)
      .field('_type', {default: 'ItemDoliprane'})
      .field('image', {default: 'doliprane'})
      .field('shape', {default: {"grid":[{"x":-8,"y":-28.4375}],"offset":{"x":8,"y":28.4375}}})
      .field('name',  {default: 'itemdoliprane'})
      .field('label', {default: 'Doliprane'})
    ;
  
    return HZItemDoliprane;
  
  }