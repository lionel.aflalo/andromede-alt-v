module.exports = (horizon, config) => {
     
    const HZItem = horizon.require('item');
  
    class HZItemFries extends HZItem {
  
    }
  
    HZItemFries
      .inheritFields(HZItem)
      .inheritActions(HZItem)
      .field('_type', {default: 'ItemFries'})
      .field('image', {default: 'fries'})
      .field('shape', {default: {"grid":[{"x":-6,"y":-19.1879795396419}],"offset":{"x":6,"y":19.1879795396419}}})
      .field('name',  {default: 'itemfries'})
      .field('label', {default: 'Fries'})
    ;
  
    return HZItemFries;
  
  }