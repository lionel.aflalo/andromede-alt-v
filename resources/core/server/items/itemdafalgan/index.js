module.exports = (horizon, config) => {
     
    const HZItem = horizon.require('item');
  
    class HZItemDafalgan extends HZItem {
  
    }
  
    HZItemDafalgan
      .inheritFields(HZItem)
      .inheritActions(HZItem)
      .field('_type', {default: 'ItemDafalgan'})
      .field('image', {default: 'dafalgan'})
      .field('shape', {default: {"grid":[{"x":-5.999999999999886,"y":-17.168269230769283}],"offset":{"x":5.999999999999886,"y":17.168269230769283}}})
      .field('name',  {default: 'itemdafalgan'})
      .field('label', {default: 'Dafalgan'})
    ;
  
    return HZItemDafalgan;
  
  }