module.exports = (horizon, config) => {
     
    const HZItem = horizon.require('item');
  
    class HZItemMegaphone extends HZItem {
  
    }
  
    HZItemMegaphone
      .inheritFields(HZItem)
      .inheritActions(HZItem)
      .field('_type', {default: 'ItemMegaphone'})
      .field('image', {default: 'megaphone'})
      .field('shape', {default: {"grid":[{"x":-9.575999999999794,"y":-36.424000000000206},{"x":-9.575999999999794,"y":63.575999999999794},{"x":90.4240000000002,"y":-36.424000000000206},{"x":90.4240000000002,"y":63.575999999999794}],"offset":{"x":9.575999999999794,"y":36.424000000000206}}})
      .field('name',  {default: 'itemmegaphone'})
      .field('label', {default: 'Megaphone'})
    ;
  
    return HZItemMegaphone;
  
  }