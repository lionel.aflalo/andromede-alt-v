module.exports = (horizon, config) => {
     
    const HZItem = horizon.require('item');
  
    class HZItemChips extends HZItem {
  
    }
  
    HZItemChips
      .inheritFields(HZItem)
      .inheritActions(HZItem)
      .field('_type', {default: 'ItemChips'})
      .field('image', {default: 'chips'})
      .field('shape', {default: {"grid":[{"x":-12.999999999999929,"y":-4.026558891454897}],"offset":{"x":12.999999999999929,"y":4.026558891454897}}})
      .field('name',  {default: 'itemchips'})
      .field('label', {default: 'Chips'})
    ;
  
    return HZItemChips;
  
  }