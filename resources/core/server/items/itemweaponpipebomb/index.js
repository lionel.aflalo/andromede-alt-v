module.exports = (horizon, config) => {
     
    const HZItem = horizon.require('item');
  
    class HZItemWeaponpipebomb extends HZItem {
  
    }
  
    HZItemWeaponpipebomb
      .inheritFields(HZItem)
      .inheritActions(HZItem)
      .field('_type', {default: 'ItemWeaponpipebomb'})
      .field('image', {default: 'weaponpipebomb'})
      .field('shape', {default: {"grid":[{"x":-26.999999999999943,"y":-16.039808917197433},{"x":73.00000000000006,"y":-16.039808917197433},{"x":173.00000000000006,"y":-16.039808917197433}],"offset":{"x":26.999999999999943,"y":16.039808917197433}}})
      .field('name',  {default: 'itemweaponpipebomb'})
      .field('label', {default: 'Weaponpipebomb'})
    ;
  
    return HZItemWeaponpipebomb;
  
  }