module.exports = (horizon, config) => {
     
    const HZItem = horizon.require('item');
  
    class HZItemWeaponadvancedrifle extends HZItem {
  
    }
  
    HZItemWeaponadvancedrifle
      .inheritFields(HZItem)
      .inheritActions(HZItem)
      .field('_type', {default: 'ItemWeaponadvancedrifle'})
      .field('image', {default: 'weaponadvancedrifle'})
      .field('shape', {default: {"grid":[{"x":-34,"y":-7.55130057803467},{"x":66,"y":-7.55130057803467},{"x":166,"y":-7.55130057803467},{"x":166,"y":92.44869942196533},{"x":266,"y":-7.55130057803467},{"x":366,"y":-7.55130057803467}],"offset":{"x":34,"y":7.55130057803467}}})
      .field('name',  {default: 'itemweaponadvancedrifle'})
      .field('label', {default: 'Weaponadvancedrifle'})
    ;
  
    return HZItemWeaponadvancedrifle;
  
  }