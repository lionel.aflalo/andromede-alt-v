module.exports = (horizon, config) => {
     
    const HZItem = horizon.require('item');
  
    class HZItemKey extends HZItem {
  
    }
  
    HZItemKey
      .inheritFields(HZItem)
      .inheritActions(HZItem)
      .field('_type', {default: 'ItemKey'})
      .field('image', {default: 'key'})
      .field('shape', {default: {"grid":[{"x":-2.9999999999998863,"y":-13.408185840707915}],"offset":{"x":2.9999999999998863,"y":13.408185840707915}}})
      .field('name',  {default: 'itemkey'})
      .field('label', {default: 'Key'})
    ;
  
    return HZItemKey;
  
  }