module.exports = (horizon, config) => {
     
    const HZItem = horizon.require('item');
  
    class HZItemWeaponmusket extends HZItem {
  
    }
  
    HZItemWeaponmusket
      .inheritFields(HZItem)
      .inheritActions(HZItem)
      .field('_type', {default: 'ItemWeaponmusket'})
      .field('image', {default: 'weaponmusket'})
      .field('shape', {default: {"grid":[{"x":-22.000000000000057,"y":-12.346557759626535},{"x":77.99999999999994,"y":-12.346557759626535},{"x":177.99999999999994,"y":-12.346557759626535},{"x":277.99999999999994,"y":-12.346557759626535},{"x":377.99999999999994,"y":-12.346557759626535},{"x":477.99999999999994,"y":-12.346557759626535}],"offset":{"x":22.000000000000057,"y":12.346557759626535}}})
      .field('name',  {default: 'itemweaponmusket'})
      .field('label', {default: 'Weaponmusket'})
    ;
  
    return HZItemWeaponmusket;
  
  }