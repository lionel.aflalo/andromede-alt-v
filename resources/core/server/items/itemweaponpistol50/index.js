module.exports = (horizon, config) => {
     
    const HZItem = horizon.require('item');
  
    class HZItemWeaponpistol50 extends HZItem {
  
    }
  
    HZItemWeaponpistol50
      .inheritFields(HZItem)
      .inheritActions(HZItem)
      .field('_type', {default: 'ItemWeaponpistol50'})
      .field('image', {default: 'weaponpistol50'})
      .field('shape', {default: {"grid":[{"x":-22.000000000000057,"y":-19.66348448687347},{"x":-22.000000000000057,"y":80.33651551312653},{"x":77.99999999999994,"y":-19.66348448687347},{"x":177.99999999999994,"y":-19.66348448687347}],"offset":{"x":22.000000000000057,"y":19.66348448687347}}})
      .field('name',  {default: 'itemweaponpistol50'})
      .field('label', {default: 'Weaponpistol50'})
    ;
  
    return HZItemWeaponpistol50;
  
  }