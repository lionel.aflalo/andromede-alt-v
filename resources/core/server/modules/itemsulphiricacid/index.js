module.exports = (horizon, config) => {
     
    const HZItem = horizon.require('item');
  
    class HZItemSulphiricacid extends HZItem {
  
    }
  
    HZItemSulphiricacid
      .inheritFields(HZItem)
      .inheritActions(HZItem)
      .field('_type', {default: 'ItemSulphiricacid'})
      .field('image', {default: 'sulphiricacid'})
      .field('shape', {default: {"grid":[{"x":-10,"y":-12.036532951289246},{"x":-10,"y":87.96346704871075},{"x":-10,"y":187.96346704871075},{"x":90,"y":-12.036532951289246},{"x":90,"y":87.96346704871075},{"x":90,"y":187.96346704871075}],"offset":{"x":10,"y":12.036532951289246}}})
      .field('name',  {default: 'itemsulphiricacid'})
      .field('label', {default: 'Acide Sulfurique'})
    ;
  
    return HZItemSulphiricacid;
  
  }