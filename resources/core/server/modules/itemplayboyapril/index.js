module.exports = (horizon, config) => {
   
  const HZItem = horizon.require('item');

  class HZItemPlayboyapril extends HZItem {

  }

  HZItemPlayboyapril
    .inheritFields(HZItem)
    .inheritActions(HZItem)
    .field('_type', {default: 'ItemPlayboyapril'})
    .field('image', {default: 'playboyapril'})
    .field('shape', {default: {"grid":[],"offset":{"x":1.9999999999999716,"y":37.30919931856897}}})
    .field('name',  {default: 'itemplayboyapril'})
    .field('label', {default: 'Playboy Avril'})
  ;

  return HZItemPlayboyapril;

}