'use strict';

const async = require('async');

module.exports = function(horizon, config) {

	const Utils              = horizon.require('utils');
	const HZItem             = horizon.require('item');
	const HZStandardBankUser = horizon.require('bank/user/standard');
	const HZBankAccount      = horizon.require('bank/account');

	class HZItemBankCard extends HZItem {
		
    initNumber(cb) {

    	let found = false;

      async.whilst(
        ()    => !found,
        (cb2) => {

          let number = '';

          for(let i=0; i<16; i++)
          	number += Utils.getRandomInt(0, 9).toString();

          HZItemBankCard.findOne({number})
            .then(card => {

              if(card === null)
                found = true

              cb2(null, number)
            })
          ;

        },
        cb
      );

		}
		
		async initAccount(cb) {

			const account = new HZBankAccount({
				type  : 'main',
				limits: ['unlimited']
			});

			await account.init();
			await account.save();

			cb(null, account._id);

		}

		async initUser(cb) {

			const user = new HZStandardBankUser({
				firstName     : Utils.getRandomString(10),
				lastName      : Utils.getRandomString(10),
				residentNumber: null
			});

			await user.init();
			await user.save();

			cb(null, user._id);
		}

		init() {
			
			return new Promise((resolve, reject) => {

				super.init().then(async () => {

					const user = await HZStandardBankUser.findOne({_id: this.user});
					user.accounts.push(this.account);
					await user.save();

					resolve();

				});

			});

		}

		async activate(cb, identity, count, ...args) {

			const player = identity.getAltPlayer();
			const user = await HZStandardBankUser.findOne({_id: this.user});

			if(user.residentNumber) {
				
				this.activated = true;

				this.save();
				
				horizon.updateCache(player, this._idStr);
				
				cb();
				return;
			
			} else {

				this.activated      = true;
				user.residentNumber = identity.residentNumber;

				user.save();
				this.save();

				horizon.updateCache(player, this._idStr);
				cb();
				return;

			}

		}

	}

	HZItemBankCard
    .inheritFields(HZItem)
    .inheritActions(HZItem)
    .field('_type',     {default: 'ItemBankcard'})
    .field('image',     {default: 'bankcard'})
    .field('shape',     {default: {"grid":[{"x":-4,"y":-22.000000000000007}],"offset":{"x":4,"y":22.000000000000007}}})
    .field('name',      {default: 'itembankcard'})
    .field('label',     {default: 'Carte Bancaire'})
		.field('mass',      {default: 10})
		.field('volume',    {default: {x: 5, y: 10, z: 0.1}})
		.field('number',    {default: null, init: 'initNumber'})
		.field('cvv',       {default: () => Utils.getRandomInt(0, 999) .toString().padStart(3, '0')})
		.field('pinCode',   {default: '0000'})
		.field('account',   {default: null, init: 'initAccount'})
		.field('user',      {default: null, init: 'initUser'})
		.field('token',     {default: null})
		.field('activated', {default: false})
		.field('image',     {default: 'bankcard'})
		.prop('label', function() {

			const number = this.number || '';
			let fNumber  = '';

			for(let i=0; i < number.length; i++) {
				
				fNumber += number[i];
				
				if((i+1) % 4 === 0 && i !== 0 && (i+1) < number.length)
					fNumber += ' ';
			}

			return 'bankcard:' + fNumber + ' CVV:' + this.cvv;

		})
		.action('activate', 'activate', function() {return !this.activated})
	;

	return HZItemBankCard;

}
