const alt    = require('alt');
const crypto = require('crypto');

module.exports = (horizon, config) => {

  const HZIdentity     = horizon.require('identity');
  const HZItem         = horizon.require('item');
  const HZInteratable  = horizon.require('interactable');
  const HZItemBankCard = horizon.require('itembankcard');
  const HZItemBanknote = horizon.require('itembanknote');
  const HZBankAccount  = horizon.require('bank/account');

  class HZShop {

  }

  const instance = new HZShop();

  instance.__init = function(cb) {

    alt.onClient('hz:shop:card:auth', (player, number, pinCode) => {

      HZItemBankCard
        .findOne({number, pinCode})
        .then(card => {

          if(card !== null) {

            card.token = crypto.randomBytes(16).toString('hex');

            card.save();

            alt.emitClient(player, 'hz:shop:card:auth:token', number, card.token);

          } else alt.emitClient(player, 'hz:notification:create', '<span style="color:red">Code incorrect</span>');

        })
      ;

    });

    alt.onClient('hz:shop:buy', async (player, _idStr, itemName, qty, paymentType = 'cash', paymentData = {}) => {

      const _id          = HZInteratable.id(_idStr);
      const interactable = await HZInteratable.findOne({_id});
      const items        = interactable.customData.items || [];
      const itemInfos    = items.find(e => e.name === itemName);

      if(!itemInfos)
        return;

      const total = Math.ceil(itemInfos.price * qty);

      const itemDef = Object.keys(horizon.modules)
        .map(e => horizon.modules[e])
        .find(e =>
          HZItem.prototype.isPrototypeOf(e.prototype) &&
          (e.fields[e.prototype.constructor.name].name.options.default === itemInfos.name)
        )
      ;

      if(!itemDef || !HZItem.prototype.isPrototypeOf(itemDef.prototype)) {
        alt.emitClient(player, 'hz:notification:create', '<span style="color:red">Error: item not found</span>');
        return;
      }

      const item = new itemDef({count: 1});

      await item.init();

      HZIdentity
        .findOne({_id: HZIdentity.id(player.getSyncedMeta('identity'))})
        .then(async identity => {

          if(identity === null)
            return;

          if(paymentType === 'cash') {

            const found = identity.queryItem({_type: item._type}).sort((a, b) => a.item.count - b.item.count);

            if((found.length > 0) && ((found[0].item.count + qty) <= found[0].item.limit)) {
              found[0].item.count += qty;
              await found[0].item.save();
              horizon.updateCache(player, found[0].item._idStr);
              alt.emitClient(player, 'hz:notification:create', '<span style="color:green">Vous avez acheté x1 ' + item.label + '</span>');
              return;
            }

            const freeSpace = identity.getFreeSpace(item);

            if(!freeSpace) {
                      
              alt.emitClient(player, 'hz:notification:create', '<span style="color:red">Pas assez d\'espace disponible dans l\'inventaire</span>');
              return;
      
            }

            let   count    = total;
            const results  = identity.queryItem({_type: 'ItemBanknote'}).sort((a, b) => a.count - b.count);
            const toDelete = [];

            for(let i=0; i<results.length; i++) {

              const result = results[i];
              let   rCount = 0;

              while((count > 0) && (rCount <= result.item.count)) {
                count--;
                rCount++;
              }
              
              if(rCount > 0)
                toDelete.push({...result, count: rCount});

            }

            if(count === 0) {
              
              for(let i=0; i<toDelete.length; i++) {

                const td       = toDelete[i];
                td.item.count -= td.count;

                if(td.item.count === 0) {
                  
                  await td.container.removeItem(td.item);
                  await td.item.delete();
                
                  horizon.updateCache(null, td.container._idStr);

                } else {

                  await td.item.save();
                  horizon.updateCache(null, td.item._idStr);

                }

              }

              await freeSpace.container.addItem(item, freeSpace.x, freeSpace.y);
        
              horizon.updateCache(player, freeSpace.container._idStr);

              alt.emitClient(player, 'hz:notification:create', '<span style="color:green">Vous avez acheté x1 ' + item.label + '</span>');

            } else {
              
              alt.emitClient(player, 'hz:notification:create', '<span style="color:red">Pas assez de cash</span>');

            }

          } else if(paymentType === 'card') {

            const card = await HZItemBankCard.findOne({number: paymentData.number, token: paymentData.token});

            if(card === null) {
              alt.emitClient(player, 'hz:notification:create', '<span style="color:red">Carte non authentifiée</span>');
              return;
            }

            const freeSpace = identity.getFreeSpace(item);

            if(!freeSpace) {
                      
              alt.emitClient(player, 'hz:notification:create', '<span style="color:red">Pas assez d\'espace disponible dans l\'inventaire</span>');
              return;
      
            }

            const account = await HZBankAccount.findOne({_id: card.account});

            if(!account) {
              alt.emitClient(player, 'hz:notification:create', '<span style="color:red">Compte introuvable</span>');
              return;
            }

            if(account.money < total) {
              alt.emitClient(player, 'hz:notification:create', '<span style="color:red">Fonds insuffisants</span>');
              return;
            }

            account.money -= total;

            await account.save();
            await freeSpace.container.addItem(item, freeSpace.x, freeSpace.y);
        
            horizon.updateCache(player, freeSpace.container._idStr);

            alt.emitClient(player, 'hz:notification:create', '<span style="color:green">Vous avez acheté x1 ' + item.label + '</span>');

          }

        })
      ;

    });
    
    alt.onClient('hz:shop:sell', async (player, _idStr, itemName, qty, paymentType = 'cash', paymentData = {}) => {

      const _id          = HZInteratable.id(_idStr);
      const interactable = await HZInteratable.findOne({_id});
      const items        = interactable.customData.items || [];
      const itemInfos    = items.find(e => e.name === itemName);

      if(!itemInfos)
        return;

      const total = Math.floor(itemInfos.price * qty);

      const itemDef = Object.keys(horizon.modules)
        .map(e => horizon.modules[e])
        .find(e =>
          HZItem.prototype.isPrototypeOf(e.prototype) &&
          (e.fields[e.prototype.constructor.name].name.options.default === itemInfos.name)
        )
      ;

      if(!itemDef || !HZItem.prototype.isPrototypeOf(itemDef.prototype)) {
        alt.emitClient(player, 'hz:notification:create', '<span style="color:red">Error: item not found</span>');
        return;
      }

      const item = new itemDef({count: 1});

      HZIdentity
        .findOne({_id: HZIdentity.id(player.getSyncedMeta('identity'))})
        .then(async identity => {

          if(identity === null)
            return;

          if(paymentType === 'cash') {

            const results = identity.queryItem({_type: item._type}).sort((a, b) => b.item.count - a.item.count);
            const owned   = results.map(e => e.item.count).reduce((a,b) => a + b, 0);

            if(owned < qty) {

              alt.emitClient(player, 'hz:notification:create', '<span style="color:red">Item introuvable</span>');
              return;
            }

            
            let count = qty;

            for(let i=0; i<results.length; i++) {

              const {container, item} = results[i];

              while((count > 0) && item.count > 0) {
                item.count--;
                count--;
              }

              if(item.count === 0) {
                
                container.removeItem(item);
                item.delete();
                container.save();
                
                horizon.updateCache(player, container._idStr);
              
              } else {

                item.save();
                
                horizon.updateCache(player, item._idStr);
              
              }

            }

            const banknoteResults = identity.queryItem({_type: 'ItemBanknote'}).sort((a, b) => a.item.count - b.item.count);

            if(banknoteResults.length > 0) {

              banknoteResults[0].item.count += total;

              banknoteResults[0].item.save();

              horizon.updateCache(player, banknoteResults[0].item._idStr);

              alt.emitClient(player, 'hz:notification:create', '<span style="color:green">Vous avez vendu x1 ' + item.label + '</span>');

            } else {

              const banknote  = new HZItemBanknote({count: total});
              const freeSpace = identity.getFreeSpace(banknote);

              if(freeSpace) {

                await banknote.save();
                freeSpace.container.addItem(banknote, freeSpace.x, freeSpace.y);

                horizon.updateCache(player, freeSpace.container._idStr);

                alt.emitClient(player, 'hz:notification:create', '<span style="color:green">Vous avez vendu x1 ' + item.label + '</span>');

              } else {
                
                alt.emitClient(player, 'hz:notification:create', '<span style="color:red">Plus d\'espace disponible dans l\'inventaire</span>');

              }

            }

          } else if(paymentType === 'card') {

            const card = await HZItemBankCard.findOne({number: paymentData.number, token: paymentData.token});

            if(card === null) {
              alt.emitClient(player, 'hz:notification:create', '<span style="color:red">Carte non authentifiée</span>');
              return;
            }

            const freeSpace = identity.getFreeSpace(item);

            if(!freeSpace) {
                      
              alt.emitClient(player, 'hz:notification:create', '<span style="color:red">Pas assez d\'espace disponible dans l\'inventaire</span>');
              return;
      
            }

            const account = await HZBankAccount.findOne({_id: card.account});

            if(!account) {
              alt.emitClient(player, 'hz:notification:create', '<span style="color:red">Compte introuvable</span>');
              return;
            }

            account.money += total;

            await account.save();
            await freeSpace.container.addItem(item, freeSpace.x, freeSpace.y);
        
            horizon.updateCache(player, freeSpace.container._idStr);

            alt.emitClient(player, 'hz:notification:create', '<span style="color:green">Vous avez vendu x1 ' + item.label + '</span>');

          }

        })
      ;

    });

    cb();

  }

  return instance;
}