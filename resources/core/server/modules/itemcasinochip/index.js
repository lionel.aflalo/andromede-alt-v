module.exports = (horizon, config) => {
     
    const HZItem = horizon.require('item');
  
    class HZItemCasinochip extends HZItem {
  
    }
  
    HZItemCasinochip
      .inheritFields(HZItem)
      .inheritActions(HZItem)
      .field('_type', {default: 'ItemCasinochip'})
      .field('image', {default: 'casinochip'})
      .field('shape', {default: {"grid":[{"x":-2,"y":-2}],"offset":{"x":2,"y":2}}})
      .field('name',  {default: 'itemcasinochip'})
      .field('label', {default: 'Jeton de casino'})
      .prop('limit', () => 99999)
    ;
  
    return HZItemCasinochip;
  
  }