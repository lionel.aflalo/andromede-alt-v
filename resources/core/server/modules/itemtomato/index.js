module.exports = (horizon, config) => {
     
    const HZItem = horizon.require('item');
  
    class HZItemTomato extends HZItem {
  
      async use(cb, identity, count = 1) {

        const player   = identity.getAltPlayer();
        const toUpdate = [];

        identity.status.hunger += 0.1;
        identity.status.thirst += 0.1;

        if(identity.status.hunger > 1)
          identity.status.hunger = 1;

        if(identity.status.thirst > 1)
          identity.status.thirst = 1;

        this.count--;

        if(this.count === 0) {

          const container = await horizon.modules.container.findOne({_id: this.container});

          if(container) {
            container.removeItem(this);
            toUpdate.push(container._idStr);
          }

          this.delete();

        } else {
          
          this.save();

          toUpdate.push(this._idStr);

        }
        
        identity.cache(true);

        horizon.updateCache(player, ...toUpdate);

        cb();
      }

    }
  
    HZItemTomato
      .inheritFields(HZItem)
      .inheritActions(HZItem)
      .field('_type', {default: 'ItemTomato'})
      .field('image', {default: 'tomato'})
      .field('shape', {default: {"grid":[{"x":-8,"y":-13.931250000000091}],"offset":{"x":8,"y":13.931250000000091}}})
      .field('name',  {default: 'itemtomato'})
      .field('label', {default: 'Tomate'})
      .prop('limit',  () => 10)
    ;
  
    return HZItemTomato;
  
  }