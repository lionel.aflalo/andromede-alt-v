const alt = require('alt');

module.exports = (horizon, config) => {
   
  const $ = horizon.$;

  const HZPlayer      = horizon.require('player');
  const HZIdentity    = horizon.require('identity');
	const HZWorldObject = horizon.require('worldobject');

  class HZNPC extends HZWorldObject {

  }

  HZNPC
    .collection('npcs')
    .inheritFields(HZWorldObject)
    .field('_type',     {default: 'NPC'})
    .field('name',      {required: true})
    .field('model',     {required: true})
    .field('heading',   {default: 0.0})
    .field('animdict',  {default: ''})
    .field('animation', {default: ''})
  ;

  HZNPC.__init = function(cb) {

    alt.onClient('hz:admin:npc:create', async (player, name, model, position, heading = 0.0, animdict = '', animation = '') => {

      const playerId = HZPlayer.id(player.getSyncedMeta('player'));
      const hzPlayer = await HZPlayer.findOne({_id: playerId});
      
      if(hzPlayer.hasRole('admin')) {

        const npc    = new HZNPC({name, model, position, heading, animdict, animation});
        npc.position = position;

        await npc.save();

        alt.emitClient(player, 'hz:notification:create', '<span style="color:green">Success: NPC created</span>');

      }

    });

    alt.onClient('hz:admin:npc:edit', async (player, _idStr, name, model, position, heading = 0.0, animdict = '', animation = '') => {

      const playerId = HZPlayer.id(player.getSyncedMeta('player'));
      const hzPlayer = await HZPlayer.findOne({_id: playerId});
      
      if(hzPlayer.hasRole('admin')) {

        const npcId    = HZNPC.id(_idStr);
        const npc      = await HZNPC.findOne({_id: npcId});

        npc.name      = name;
        npc.model     = model;;
        npc.position  = position;
        npc.heading   = heading;
        npc.animdict  = animdict;
        npc.animation = animation;

        await npc.save();

        alt.emitClient(player, 'hz:notification:create', '<span style="color:green">Success: NPC edited</span>');

      }

    });

    alt.onClient('hz:admin:npc:delete', async (player, _idStr) => {

      const playerId = HZPlayer.id(player.getSyncedMeta('player'));
      const hzPlayer = await HZPlayer.findOne({_id: playerId});
      
      if(hzPlayer.hasRole('admin')) {

        const npcId    = HZNPC.id(_idStr);
        const npc      = await HZNPC.findOne({_id: npcId});

        await npc.delete();

        alt.emitClient(player, 'hz:notification:create', '<span style="color:green">Success: NPC deleted</span>');

      }

    });

    $.registerCallback('hz:npc:get:cube', async (player, cb, cube, radius = 2) => {

      const npcs = await HZNPC
        .find({
          $or: [
            {
              'cube.x': {$gte: cube.x - radius, $lte: cube.x + radius},
              'cube.y': {$gte: cube.y - radius, $lte: cube.y + radius},
              'cube.z': {$gte: cube.z - radius, $lte: cube.z + radius},
            },
            {
              position: null,
            }
          ]
        })
      ;

      cb(npcs.map(e => ({_type: e._type, _id: e._idStr})));

    });

    cb();
  }

  return HZNPC;

}