module.exports = (horizon, config) => {
   
  const HZItem = horizon.require('item');

  class HZItemPlayboymarch extends HZItem {

  }

  HZItemPlayboymarch
    .inheritFields(HZItem)
    .inheritActions(HZItem)
    .field('_type', {default: 'ItemPlayboymarch'})
    .field('image', {default: 'playboymarch'})
    .field('shape', {default: {"grid":[],"offset":{"x":1,"y":41.232012513034306}}})
    .field('name',  {default: 'itemplayboymarch'})
    .field('label', {default: 'Playboy Mars'})
  ;

  return HZItemPlayboymarch;

}