module.exports = (horizon, config) => {
     
    const HZItem = horizon.require('item');
  
    class HZItemMelon extends HZItem {
  
      async use(cb, identity, count = 1) {

        const player   = identity.getAltPlayer();
        const toUpdate = [];

        identity.status.hunger += 0.05;

        if(identity.status.hunger > 1)
          identity.status.hunger = 1;

        if(identity.status.thirst > 1)
          identity.status.thirst = 1;

        this.count--;

        if(this.count === 0) {

          const container = await horizon.modules.container.findOne({_id: this.container});

          if(container) {
            container.removeItem(this);
            toUpdate.push(container._idStr);
          }

          this.delete();

        } else {
          
          this.save();

          toUpdate.push(this._idStr);

        }
        
        identity.cache(true);

        horizon.updateCache(player, ...toUpdate);

        cb();
      }

    }
  
    HZItemMelon
      .inheritFields(HZItem)
      .inheritActions(HZItem)
      .field('_type', {default: 'ItemMelon'})
      .field('image', {default: 'melon'})
      .field('shape', {default: {"grid":[{"x":-59,"y":-3.4944751381215156},{"x":41,"y":-3.4944751381215156}],"offset":{"x":59,"y":3.4944751381215156}}})
      .field('name',  {default: 'itemmelon'})
      .field('label', {default: 'Melon'})
    ;
  
    return HZItemMelon;
  
  }