module.exports = (horizon, config) => {
   
  const HZItem = horizon.require('item');

  class HZItemPlayboyoctober extends HZItem {

  }

  HZItemPlayboyoctober
    .inheritFields(HZItem)
    .inheritActions(HZItem)
    .field('_type', {default: 'ItemPlayboyoctober'})
    .field('image', {default: 'playboyoctober'})
    .field('shape', {default: {"grid":[{"x":-1.999999999999993,"y":-22.04614019520841},{"x":-1.999999999999993,"y":77.95385980479159}],"offset":{"x":1.999999999999993,"y":22.04614019520841}}})
    .field('name',  {default: 'itemplayboyoctober'})
    .field('label', {default: 'Playboy Octobre'})
  ;

  return HZItemPlayboyoctober;

}