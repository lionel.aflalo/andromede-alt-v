module.exports = (horizon, config) => {
     
    const HZItem = horizon.require('item');
  
    class HZItemHandcuff extends HZItem {
  
    }
  
    HZItemHandcuff
      .inheritFields(HZItem)
      .inheritActions(HZItem)
      .field('_type', {default: 'ItemHandcuff'})
      .field('image', {default: 'handcuff'})
      .field('shape', {default: {"grid":[{"x":-10.000000000000028,"y":-13.563868613138766},{"x":89.99999999999997,"y":-13.563868613138766}],"offset":{"x":10,"y":13.563868613138766}}})
      .field('name',  {default: 'itemhandcuff'})
      .field('label', {default: 'Menottes'})
    ;
  
    return HZItemHandcuff;
  
  }