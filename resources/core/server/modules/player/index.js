'use strict';

const alt    = require('alt');
const plimit = require('p-limit');

const onClient = alt.onClient;

module.exports = function(horizon, config) {

	const $ = horizon.$;

	const HZEnrolable       = horizon.require('enrolable');
	const HZIdentity        = horizon.require('identity');
  const HZWorldContainer  = horizon.require('worldcontainer');
  const HZItemBanknote    = horizon.require('itembanknote');
  const HZItemBread       = horizon.require('itembread');
	const HZItemWaterBottle = horizon.require('itemwaterbottle');
	const HZItemBankCard    = horizon.require('itembankcard');

	let dimension = 1000;

	class HZPlayer extends HZEnrolable {
    
		getAltPlayer() {
			return alt.Player.all.find(e => e.getSyncedMeta('player') && (e.getSyncedMeta('player') === this._idStr));
		}

	}

	HZPlayer
		.collection('players')
		.inheritFields(HZEnrolable)
    .field('_type'       , {default: 'Player'})
		.field('name'        , {required: true})
    .field('identifiers' , {required: true})
    .field('password'    , {default: null})
    .field('identities'  , {default: [],   subdoc: HZIdentity})
    .field('identity'    , {default: null, subdoc: HZIdentity})
	;

	alt.Player.prototype.getPlayer = function() {
		return HZPlayer.findOne({_id: HZPlayer.id(this.getSyncedMeta('player'))});
	}

	alt.Player.prototype.getIdentity = function() {
		return HZIdentity.findOne({_id: HZIdentity.id(this.getSyncedMeta('identity'))});
	}

	HZPlayer.__init = function(cb) {
    
    alt.on('playerConnect', player => {
			
			horizon.logger.info('playerConnect');
			
			player.dimension = 0;
			player.model     = $.utils.joaat('mp_m_freemode_01');
			
			alt.emitClient(player, 'hz:player:connect');
		
		});

		$.registerCallback('hz:player:model:set', (player, cb, model) => {
			
			const pos    = player.pos;
			player.model = model;
			player.pos   = pos;
			
			cb();
		
		});


		alt.onClient('hz:player:connect:ok', async (player, identifiers, name) => {

			const query = {$or: []};
			let   count = 0;

			Object.keys(identifiers).forEach(e => {

				if(!identifiers[e])
					return;

				const obj = {};
				obj['identifiers.' + e] = identifiers[e];
				query.$or.push(obj);

				count++;

			});

			if(count === 0) {
				horizon.logger.info('Kicking player, no identifier');
				player.kick();
				return;
			}

			const hzPlayer = await HZPlayer.findOrCreateOfType(query, {
				identifiers,
				name
			});

			for(let name in identifiers)
				hzPlayer.identifiers[name] = identifiers[name];

			if(hzPlayer.identities.length === 0)
				player.dimension = dimension++;

			player.setSyncedMeta('player', hzPlayer._id.toString()); 

			await hzPlayer.save();

      if(!hzPlayer.hasOneRole('admin', 'whitelist')) {

        horizon.logger.info('Kicking player, not admin and not whitelisted');
        player.kick();

        return;

      }

			horizon.emit('player.loaded', player);

		});

		alt.onClient('hz:player:init:ok', async (player, model) => {
			
			const hzPlayer = await HZPlayer.findOne({_id: HZPlayer.id(player.getSyncedMeta('player'))});

      let identity = null;

			if(model) {

				identity = new HZIdentity({
					model: {
						hash         : +player.model,
						faceFeatures : model.faceFeatures,
						components   : model.components,
						overlays     : model.overlays,
						headBlendData: model.headBlendData,
						props        : model.props,
          },
          roles: ['rsa'],
				});

				await identity.ensureContainers();
        
        const container = identity.containers[0];

				const card     = new HZItemBankCard({count: 1});
        const banknote = new HZItemBanknote({count: 1500});
        const bread    = new HZItemBread({count: 3});
				const water    = new HZItemWaterBottle({count: 3});

				await card.init();
        await Promise.all([card.save(), banknote.save(), bread.save(), water.save()]);

        let result = identity.getFreeSpace(card);
        await container.addItem(card, result.x, result.y);
				
				result = identity.getFreeSpace(banknote);
        await container.addItem(banknote, result.x, result.y);
				
				result = identity.getFreeSpace(bread);
        await container.addItem(bread, result.x, result.y);
				
				result = identity.getFreeSpace(water);
        await container.addItem(water, result.x, result.y);

				await identity.save(true);
				
				await identity.ensureComponents();
				await identity.ensureProps();

				hzPlayer.identity = identity;

				await hzPlayer.save();

			} else identity = hzPlayer.identity;

			player.setSyncedMeta('identity', hzPlayer.identity ? hzPlayer.identity._idStr : null);
      
			if(hzPlayer.identity)
				hzPlayer.identity.ensureModel();

			player.pos       = hzPlayer.identity.position ? hzPlayer.identity.position : config.spawn.position;
			player.dimension = 0;

      horizon.emit('player.inited', player);
      
      if(identity.hasRole('task_force')) {
        player.giveWeapon(horizon.$.utils.joaat('weapon_stungun'), 1000, false);
        player.giveWeapon(horizon.$.utils.joaat('weapon_nightstick'), 1000, false);
      }
      
      if(identity.hasRole('ems')) {
        player.giveWeapon(horizon.$.utils.joaat('weapon_flashlight'), 1000, false);
      }

		});

		horizon.on('player.loaded', async player => {
			
			horizon.logger.info('player.loaded');

			const world = await HZWorldContainer.findOneOfType({name: 'world'});

			alt.emitClient(player, 'hz:player:init', player.getSyncedMeta('player'), world._idStr);
		
		});

		horizon.on('player.inited', async player => {
			horizon.logger.info('player.inited');
		});

    // Save identities
		setInterval(() => {
			
			const players  = alt.Player.all;
			const promises = [];
			
			for(let i=0; i<players.length; i++) {

				const player        = players[i];
				const identityIdStr = player.getSyncedMeta('identity');

				if(!identityIdStr)
					continue;

				(player => {

					player
						.getIdentity()
						.then(identity => {

							identity.health   = player.health;
							identity.armour   = player.armour;
							identity.position = {x: player.pos.x, y: player.pos.y, z: player.pos.z, h: player.rot.z};
							
							identity.save();
						})
					;

				})(player);

			}

		}, 5000);

		alt.on('playerDisconnect', async (player, reason) => {

			horizon.logger.info('player.disconnect');

			player
				.getIdentity()
				.then(identity => {
          if(identity !== null)
            identity.save();
				})
			;

		});

		alt.on('resourceStop', () => {

			const players  = alt.Player.all;
			const promises = [];
			
			for(let i=0; i<players.length; i++) {

				const player        = players[i];
				const identityIdStr = player.getSyncedMeta('identity');

				if(!identityIdStr)
					continue;

				(player => {

					player
						.getIdentity()
						.then(identity => {
							identity.save();
						})
					;

				})(player);

			}

		});
		
		cb();

	}

	return HZPlayer;

};
