'use strict';

const async        = require('async');
const EventEmitter = require('events');
const ObjectID     = require('mongodb').ObjectID;
const clone        = require('clone');

module.exports = function(horizon, config) {

  const CACHE_TIME         = config.cache.time;
  const CACHE_TICK         = config.cache.tick;
  const CACHE_RESOLVE_TIME = config.cache.resolveTime;
  const CACHE_LOG_INTERVAL = config.cache.logInterval;

  const fields     = {};
  const props      = {};
  const types      = {};
  const data       = {};
  const typeChains = {};
  const cache      = {}; //const cache  = new Proxy({}, {get: (obj, prop) => undefined}); // Disable cache

  /**
   * Represents a cache pointer.
   * @memberof Server
   */
  class HZCachePointer {

    /**
     * Create a cache pointer.
     * @param {HZEntity} entity - instance of HZEntity
     * @param {HZEntity} parent - instance of HZEntity
     * @param {String} oldId - old id in case of change
     */
    constructor(entity, parent = null, oldId = null) {

      if(oldId) {

        console.log(oldId);

        const oldIdStr       = oldId.toString();
        const pointer        = cache[oldIdStr];
        cache[entity._idStr] = pointer;
        delete cache[oldIdStr];
        pointer.load(entity);
        return cache[entity._idStr];
      }

      if(entity instanceof HZCachePointer)
        return entity;

      if(cache[entity._idStr]) {
        cache[entity._idStr].load(entity, parent);
        return cache[entity._idStr];
      }

      /** @member {Boolean} - wether the persistence is enabled by default */
      this.defaultPersist = entity.persist;

      /** @member {Boolean} - wether the persistence is enabled */
      this.persist = entity.persist;

      /** @member {Object} - resolved data (plain object) */
      this.data = null;

      /** @member {HZEntity} - HZEntity instance */
      this.entity = null;

      /** @member {Number} - timeout for clearing cache */
      this.resolveTimeout = null;

      /** @member {Array<HZCachePointer>} - parent HZCachePointer instances */
      this.parents = [];

      /** @member {Array<HZCachePointer>} - children HZCachePointer instances */
      this.children = [];

      if(parent && this.parents.indexOf(parent) === -1)
        this.parents.push(parent);

      this.load(entity, parent);

      if(this.persist)
        this.entity = this.resolve();

      cache[entity._idStr] = this;

    }

    /**
     * Load an entity inside current HZCachePointer instance
     * @param {HZEntity} entity - instance of HZEntity
     * @param {HZEntity} parent - instance of HZEntity
     */
    load(entity, parent = null) {

      this._type          = entity._type;
      this._id            = entity._id;
      this._idStr         = this._id.toString();
      this.time           = +new Date;
      this.entity         = null;
      const foundChildren = [];
      const data          = {...entity.fields};

      if(parent && this.parents.indexOf(parent) === -1)
        this.parents.push(parent);

      for(let k in fields[entity.constructor.name]) {

        const options = fields[entity.constructor.name][k].options;

        if(typeof options.subdoc !== 'undefined') {

          if(entity[k] instanceof Array) {

            data[k] = entity[k].map(e => e ? new HZCachePointer(e, this) : null);

            data[k].forEach(e => e !== null && foundChildren.findIndex(c => c._id.equals(e._id)) === -1 && foundChildren.push(e));

          } else if(entity[k] instanceof options.subdoc) {

            data[k] = new HZCachePointer(entity[k], this);

            if(foundChildren.findIndex(c => c._id.equals(data[k]._id)) === -1)
              foundChildren.push(data[k]);

          }

        }

      }

      for(let i=0; i<this.children.length; i++)
        if(foundChildren.indexOf(this.children[i]) === -1 && this.children[i].parents.indexOf(this) !== -1)
          this.children[i].parents.splice(this.children[i].parents.indexOf(this), 1);

      this.children.length = 0;

      for(let i=0; i<foundChildren.length; i++)
        this.children.push(foundChildren[i]);

      this.data = data;

    }

    /**
     * Resolve current HZCachePointer instance to HZEntity
     */
    resolve() {

      const _fields = fields[types[this.data._type].name];

      // Increase cache time
      if(this.resolveTimeout)
        clearTimeout(this.resolveTimeout);

      if(!this.persist) {

        this.resolveTimeout = setTimeout(() => {
          this.entity         = null
          this.resolveTimeout = null
        }, CACHE_RESOLVE_TIME);

      }

      // Found cached entity
      if(this.entity) {

        for(let k in _fields) {

          const options = _fields[k].options;

          if(typeof options.subdoc != 'undefined') {

            if(options.default instanceof Array)
              this.entity[k] = this.data[k].map(e => e ? e.resolve() : null);
            else if(this.data[k] instanceof HZCachePointer)
              this.entity[k] = this.data[k].resolve();

          }

        }

        return this.entity;
      }

      // No cached entity : resolve
      const data = {};

      for(let k in _fields) {

        const options = _fields[k].options;

        if(typeof options.subdoc != 'undefined') {

          if(options.default instanceof Array)
            data[k] = this.data[k].map(e => e ? e.resolve() : null);
          else if(this.data[k] instanceof HZCachePointer)
            data[k] = this.data[k].resolve();

        } else data[k] = this.data[k];

      }

      this.entity = new types[this._type](data);

      return this.entity;

    }

    /**
     * Remove current HZCachePointer instance from cache
     */
    delete() {

      for(let i=0; i<this.children.length; i++) {

        const child = cache[this.children[i]._id.toString()];

        if(!child)
          continue;

        const idx = child.parents.findIndex(e => e._id.equals(this._id));

        if(idx !== -1)
          child.parents.splice(idx, 1);

      }

      delete cache[this._idStr];

    }

  }

  /**
   * Represents an entity. Example: {@tutorial HZEntity}
   * @memberof Core
   */
  class HZEntity extends EventEmitter {

    /**
     * Create an entity.
     * @param {object} data - MongoDB query
     */
    constructor(data = {}) {

      if (new.target === HZEntity)
        throw new TypeError('Cannot construct HZEntity instances directly, must be extended');

      super();

      /** @member {Boolean} - wether this instance has to be saved */
      this.__toSave = false;

      /** @member {Boolean} - wether this instance will have a new id when saved */
      this.__newId = false;

      /** @member {Array} - fields defined via .field() */
      this.fields = [];

      for(let k in data) {

        if(k == '_id')
          this._idStr = data[k].toString();

        this.fields[k] = data[k];
      }

      for(let k in fields[this.constructor.name]) {

        const options = fields[this.constructor.name][k].options;

        if(typeof this[k] == 'undefined' && typeof options.default != 'undefined')
          this.fields[k] = (typeof options.default == 'function') ? options.default.bind(this)() : clone(options.default);

        if(options.required && typeof this[k] == 'undefined')
          throw new Error('Field \'' + k + '\' is required for ' + this.constructor.name);
      }

      if(!this._id) {
        this.__toSave = true;
        this.__newId  = true;

        /** @member {ObjectID} - id in database */
        this._id = ObjectID();

        /** @member {Boolean} - id in database converted to string */
        this._idStr = this._id.toString();
      }

    }

    /**
     * fields
     */
    static get fields() { return fields; }

    /**
     * props
     */
    static get props() { return props; }

    /**
     * types
     */
    static get types()  { return types;  }

    /**
     * cache
     */
    static get cache()  { return cache;  }

    /**
     * type chain
     */
    static get typeChain() {
      return typeChains[this.prototype.constructor.name];
    }

    /**
     * wether the current class is initable or not
     */
    static get initable() {

      for(let k in fields[this.prototype.constructor.name]) {

        const options = fields[this.prototype.constructor.name][k].options;

        if(options.init)
          return true;
      }

      return false;

    }

    /**
     * subdocs
     */
    static get subdocs() {

      const subdocs = [];

      for(let k in fields[this.prototype.constructor.name]) {

        const options = fields[this.prototype.constructor.name][k].options;

        if(typeof options.subdoc !== 'undefined') {
          subdocs.push({name: k, type: options.subdoc, typeChain: typeChains[options.subdoc.prototype.constructor.name], isArray: options.default instanceof Array});
        }
      }

      return subdocs;

    }

    /**
     * subdocs
     */
    get subdocs() {
      return this.constructor.subdocs;
    }

    /**
     * Returns wether the current instance is initable or not
     */
    get initable() {

      for(let k in fields[this.constructor.name]) {

        const options = fields[this.constructor.name][k].options;

        if(options.init)
          return true;
      }

      return false;

    }

    /**
     * Find one record in the database matching given query
     * @param {Object} data - MongoDB query
     */
    static findOne(data, withCache = true) {

      if(withCache && data._id && Object.keys(data).length === 1 && cache[data._id.toString()]) {

        return new Promise((resolve, reject) => {

          const instance = this.getCache(data._id.toString());
          resolve(instance);

        });


      }

      return new Promise((resolve, reject) => {

        const idStr = (data._id) ? data._id.toString() : null;

        setImmediate(() => {

          horizon.db.collection(this.prototype.__collection)
            .findOne(data)
            .then((doc) => {

              if(doc != null) {

                if(typeof types[doc._type] == 'undefined') {
                  reject('field _type has no default value for ' + doc._type)
                } else {

                  const idStr  = doc._id.toString();
                  const cached = HZEntity.getCache(idStr)

                  if(cached) {
                    resolve(cached);
                    return;
                  }

                  const instance = new types[doc._type](doc);
                  const promises = [];

                  for(let k in fields[instance.constructor.name]) {

                    const options = fields[instance.constructor.name][k].options;

                    if(typeof options.subdoc != 'undefined') {

                      if(instance[k] instanceof Array) {

                        const promise = new Promise(function(resolve, reject) {;

                          const target = doc[this.k] || [];

                          options.subdoc
                            .find({_id: {$in: target}})
                            .then((_docs) => {

                              const docs = [];

                              for(let i=0; i<target.length; i++) {

                                if(doc[this.k][i] == null) {

                                  docs.push(null)

                                } else {

                                  for(let j=0; j<_docs.length; j++) {

                                    if(_docs[j] && _docs[j]._id.equals(target[i])) {
                                      docs.push(_docs[j]);
                                      break;
                                    }

                                  }

                                }

                              }

                              instance[this.k] = docs;

                              resolve();
                            })
                          ;

                        }.bind({k}));

                        promises.push(promise);

                      }

                      if(instance[k] instanceof ObjectID) {

                        const promise = new Promise(function(resolve, reject) {;

                          options.subdoc
                            .findOne({_id: doc[this.k]})
                            .then((doc) => {

                              instance[this.k] = doc;

                              resolve();
                            })
                          ;

                        }.bind({k}));

                        promises.push(promise);

                      }

                    }
                  }

                  Promise.all(promises).then(() => {

                    const cached = HZEntity.getCache(idStr)

                    if(cached) {

                      resolve(cached);

                    } else {

                      instance.cache(true);
                      resolve(instance);

                    }

                  });

                }
              } else resolve(null);

            })
            .catch((e) => {
              throw e;
            })
          ;

        });

      });

    }


    /**
     * Find one record in the database matching given query and strictly matching _type field delared via .field('_type')
     * @param {Object} data - MongoDB query
     */
    static findOneOfType(data) {
      data._type = fields[this.prototype.constructor.name]._type.options.default;
      return this.findOne(data);
    }

    /**
     * Find all records in the databse matching given query
     * @param {Object} data - MongoDB query
     */
    static find(data, skip = null, limit = null, sort = null) {

      if(data._id && data._id.$in instanceof Array) {

        if(data._id.$in.length === 0) {
          return new Promise((resolve, reject) => {
            resolve([]);
          });
        }

        const instances = data._id.$in.map(e => e ? this.getCache(e) : null);
        let   foundAll  = true;

        for(let i=0; i<data._id.$in.length; i++) {
          if(data._id.$in[i] instanceof ObjectID && !instances[i]) {
            foundAll = false;
            break
          }
        }

        if(foundAll) {

          return new Promise((resolve, reject) => {
            resolve(instances);
          });

        }

      }

      return new Promise((resolve, reject) => {

        setImmediate(() => {

          const search = () => {

            let chain = horizon.db.collection(this.prototype.__collection).find(data);

            if(sort !== null)
              chain = chain.sort(sort);

            if(skip !== null)
              chain = chain.skip(skip);

            if(limit !== null)
              chain = chain.limit(limit);

            const docs = [];

            chain.on('data', doc => docs.push(doc));


            chain.on('end', () => {

              const instances = [];
              const promises  = [];

              for(let i=0; i<docs.length; i++) {

                const doc = docs[i];

                if(typeof types[doc._type] == 'undefined') {
                  reject('field _type has no default value for ' + docs[i]._type)
                  return;
                } else {

                  const idStr  = doc._id.toString();
                  const cached = HZEntity.getCache(idStr)

                  if(cached) {
                    instances.push(cached);
                    continue;
                  }

                  const instance = new types[doc._type](docs[i]);

                  for(let k in fields[instance.constructor.name]) {

                    const options = fields[instance.constructor.name][k].options;

                    if(typeof options.subdoc != 'undefined') {

                      if(instance[k] instanceof Array) {

                        const promise = new Promise(function(resolve, reject) {;

                          const target = this.doc[this.k] || [];

                          options.subdoc
                            .find({_id: {$in: target}})
                            .then((_docs) => {

                              const docs = [];

                              for(let i=0; i<target.length; i++) {

                                if(this.doc[this.k][i] == null) {

                                  docs.push(null)

                                } else {

                                  for(let j=0; j<_docs.length; j++) {

                                    if(_docs[j] && _docs[j]._id.equals(target[i])) {
                                      docs.push(_docs[j]);
                                      break;
                                    }

                                  }

                                }

                              }

                              this.instance[this.k] = docs;
                              resolve();
                            })
                          ;

                        }.bind({instance, k, doc: docs[i]}));

                        promises.push(promise);

                      }

                      if(instance[k] instanceof ObjectID) {

                        const promise = new Promise(function(resolve, reject) {;

                          options.subdoc
                            .findOne({_id: this.doc[this.k]})
                            .then((doc) => {

                              this.instance[this.k] = doc;

                              resolve();
                            })
                          ;

                        }.bind({instance, k, doc: docs[i]}));

                        promises.push(promise);

                      }

                    }
                  }

                  instances.push(instance);

                }

              }

              Promise.all(promises).then(() => {

                for(let i=0; i<instances.length; i++) {

                  const idStr  = instances[i]._idStr;
                  const cached = HZEntity.getCache(idStr)

                  if(cached) {

                    instances[i] = cached;

                  } else {

                    instances[i].cache(true);

                  }

                }

                resolve(instances);

              });

            });

          }

          search();

        });

      });

    }

    /**
     * Find all records in the databse matching given query and strictly matching _type field delared via .field('_type')
     * @param {Object} data - MongoDB query
     */
    static findOfType(data, skip = null, limit = null, sort = null) {
      data._type = fields[this.prototype.constructor.name]._type.options.default;
      return this.find(data, skip, limit, sort);
    }


    /**
     * Count records in the databse matching given query
     * @param {Object} data - MongoDB query
     */
    static count(data) {
      return horizon.db.collection(this.prototype.__collection)
        .count(data)
      ;
    }

    /**
     * Find or create record
     * @param {Object} data - MongoDB query
     */
    static findOrCreate(data, newData = null) {

      const construct = this.prototype.constructor;
      newData         = newData || data;

      return construct
        .findOne(data)
        .then((entity) => {

          return new Promise((resolve, reject) => {

            if(entity == null) {

              entity = new construct(newData);

              entity
                .save()
                .then(() => resolve(entity))
              ;

            } else resolve(entity);

          });

        })

    }

    /**
     * Find or create record strictly matching _type field delared via .field('_type')
     * @param {Object} data - MongoDB query
     */
    static findOrCreateOfType(data, newData = null) {
      data._type = fields[this.prototype.constructor.name]._type.options.default;
      return this.findOrCreate(data, newData);
    }

    /**
     * Define field
     * @param {String} k - key
     * @param {Object} options - options
     */
    static field(k, options = null) {

      if(options === null)
        return fields[this.prototype.constructor.name][k];

      var options                                = options                                 || {};
      fields[this.prototype.constructor.name]    = fields[this.prototype.constructor.name] || {};
      fields[this.prototype.constructor.name][k] = {options: options};

      if(k == '_type') {
        this._type = options.default;
        types[options.default] = this.prototype.constructor;
        typeChains[this.prototype.constructor.name] = typeChains[this.prototype.constructor.name] || [];
        typeChains[this.prototype.constructor.name].push(options.default);
      }

      if(options.init && !options.initCheck)
        options.initCheck = v => v !== null;

      if(!options.customGetter) {
        
        Object.defineProperty(this.prototype, k, {

          get: function()  {
            return this.fields[k];
          },

          configurable: true

        });
        
      }

      if(!options.customSetter) {

        Object.defineProperty(this.prototype, k, {

          set: function(v) {
            this.fields[k] = v;
          },

          configurable: true

        });

      }

      return this;

    }

    /**
     * Define prop
     * @param {String} k - key
     * @param {Function} cb - callback
     */
    static prop(k, cb) {

      props[this.prototype.constructor.name] = props[this.prototype.constructor.name] || []
      props[this.prototype.constructor.name].push({name: k, cb: cb});

      Object.defineProperty(this.prototype, k, {get: cb, set: val => {}, configurable: true});

      return this;

    }

    /**
     * Inherit fields from baseClass
     * @param {Object} baseClass - base class
     */
    static inheritFields(baseClass) {

      if(typeof fields[baseClass.prototype.constructor.name] == 'undefined')
        return;

      fields[this.prototype.constructor.name] = fields[this.prototype.constructor.name] || {};

      typeChains[this.prototype.constructor.name] = (typeChains[baseClass.prototype.constructor.name] || []).concat(typeChains[this.prototype.constructor.name] || []);

      for(let k in fields[baseClass.prototype.constructor.name]){

        const options                              = fields[baseClass.prototype.constructor.name][k].options;
        fields[this.prototype.constructor.name][k] = {options: options};

        if(options.customGetter) {

          Object.defineProperty(this.prototype, k, {
            
            set: function(val) {
              Object.getOwnPropertyDescriptor(baseClass.prototype, k).get.apply(this, []);
            },

            configurable: true

          });

        } else {

          Object.defineProperty(this.prototype, k, {

            get: function()  {
              return this.fields[k];
            },
  
            configurable: true
  
          });

        }

        if(options.customSetter) {

          Object.defineProperty(this.prototype, k, {
            set: function(val) {
              Object.getOwnPropertyDescriptor(baseClass.prototype, k).set.apply(this, [val]);
            }
          });

        } else {

          Object.defineProperty(this.prototype, k, {

            set: function(v) {
              this.fields[k] = v;
            }

          });

        }

      }

      const _props = (props[baseClass.prototype.constructor.name] || []).slice(0);

      for(let i=0; i<_props.length; i++) {

        props[this.prototype.constructor.name] = props[this.prototype.constructor.name] || []
        props[this.prototype.constructor.name].push({name: _props[i].name, cb: _props[i].cb});

        Object.defineProperty(this.prototype, _props[i].name, {get: _props[i].cb, set: val => {}, configurable: true});

      }

      return this;

    }

    /**
     * Define collection or get current collection
     * @param {String} name - collection name
     */
    static collection(name) {

      if(typeof name == 'undefined')
        return horizon.db.collection(this.prototype.__collection);

      this.prototype.__collection = name;
      return this;
    }

    /**
     * Returns ObjectID instance from string
     * @param {String} id - id
     */
    static id(id) {
      return ObjectID(id)
    }

    /**
     * Get entity from cache
     * @param  {ObjectID} id
     */
    static getCache(id) {

      const idStr  = id.toString();
      const entity = cache[idStr] ? cache[idStr].resolve() : undefined;

      return entity;

    }

    /**
     * Serialize an entity
     */
    static serialize(copy) {

      const obj     = {_type: copy._type, _id: copy._id.toString()};
      const _fields = fields[types[copy._type].name];

      for(let k in _fields) {
        if(_fields.hasOwnProperty(k)) {

          const options = _fields[k].options;

          if(typeof options.subdoc !== 'undefined'){

            if(copy[k] instanceof Array)
              obj[k] = copy[k].map(e => (!e || !e._id) ? null : {_type: e._type, _id: e._idStr});
            else
              obj[k] = (!copy[k] || !copy[k]._id) ? null : {_type: copy[k]._type, _id: copy[k]._idStr};

          } else {

            if(copy[k] instanceof ObjectID) {

              obj[k] = copy[k].toString();

            } else if(copy[k] instanceof Array) {

              obj[k] = copy[k].map(e => e instanceof ObjectID ? {_id: e.toString()} : e);

            } else {

              obj[k] = copy[k];

            }

          }

        }
      }

      const propKeys = props[types[copy._type].prototype.constructor.name].map(e => e.name);

      if(typeof propKeys != 'undefined')
        for(let i=0; i<propKeys.length; i++)
          obj[propKeys[i]] = copy[propKeys[i]];

      return obj;

    }


    /**
     * Initialize current entity.
     */
    init() {

      const toInit = [];

      for(let k in fields[this.constructor.name]) {

        const options = fields[this.constructor.name][k].options;

        if(options.required && typeof this[k] == 'undefined')
          throw new Error('Field \'' + k + '\' is required for ' + this.constructor.name);

        if(typeof this[k] == 'undefined') {

          if(typeof options.default === 'undefined')
            this[k] = null;
          else
            this[k] = (typeof options.default == 'function') ? options.default.bind(this)() : options.default;
        }

        if(typeof options.init == 'string') {

          ((k, options) => {

            const isInited = options.initCheck(this[k]);

            if(!isInited) {

              toInit.push((cb) => {

                this[options.init]((err, result) => {

                  if(err)
                    throw err;

                  this[k] = result;

                  cb(null);
                })

              });

            }

          })(k, options);

        }
      }

      return new Promise((resolve, reject) => {

        if(toInit.length === 0) {

          setImmediate(() => {
            this.emit('init');
            resolve()
          });

        } else {

          async.parallel(toInit, (err, results) => {
            this.emit('init');
            resolve();
          });

        }

      });

    }

    /**
     * Add / remove current entity from cache
     * @param  {Boolean} enabled
     * @param  {ObjectID oldId
     */
    cache(enabled, oldId = null) {

      if(enabled)
        new HZCachePointer(this, null, oldId);
      else
        delete cache[this._idStr];

    }


    /**
     * Save current entity.
     */
    save(withSubdocs = false, retries = 0, oldId = null) {

      this.__toSave = false;

      this.cache(true, oldId);

      return new Promise((resolve, reject) => {

        const data     = {};
        const promises = [];

        for(let k in fields[this.constructor.name]) {

          const options = fields[this.constructor.name][k].options;

          if(typeof options.subdoc != 'undefined') {

            if(this[k] instanceof Array) {

              for(let i=0; i<this[k].length; i++)
                if(this[k][i] instanceof options.subdoc && (this[k][i].__toSave || withSubdocs))
                  promises.push(this[k][i].save(withSubdocs));

            } else if(this[k] instanceof options.subdoc) {

              if(this[k].__toSave || withSubdocs)
                promises.push(this[k].save(withSubdocs));

            }

          }

          data[k] = this[k];

        }

        if(this._id == null || this.__newId) {

          this.__newId = false;

          Promise.all(promises).then(() => {

            for(let k in fields[this.constructor.name]) {

              const options = fields[this.constructor.name][k].options;

              if(typeof options.subdoc != 'undefined') {

                if(this[k] instanceof Array)
                  data[k] = this[k].map(e => e._id);
                else if(this[k] instanceof options.subdoc)
                  data[k] = this[k]._id;

              } else data[k] = this[k];

            }

            horizon.db.collection(this.__collection)
              .insertOne(data)
              .then((result) => {

                this._id    = result.insertedId;
                this._idStr = this._id.toString();

                this.emit('save');
                horizon.emit('entity.save', this);

                resolve(this);

              })
              .catch((err) => {

                console.log(`Retry save [${this._type}] => ${retries + 1} (${err.message})`);

                let oldId = null;

                if(err.code === 11000) {
                  oldId        = this._id;
                  this.__newId = true;
                  this._id     = ObjectID();
                  this._idStr  = this._id.toString();
                }

                setImmediate(() => {

                  this
                    .save(withSubdocs, ++retries, oldId)
                    .then(() => resolve(this))
                  ;

                });


              })
            ;

          });

        } else {

          Promise.all(promises).then(() => {

            for(let k in fields[this.constructor.name]) {

              if(k == '_id')
                continue;

              const options = fields[this.constructor.name][k].options;

              if(typeof options.subdoc != 'undefined') {
                if(this[k] instanceof Array)
                  data[k] = this[k].map(e => (e == null) ? null : e._id);
                else if(this[k] instanceof options.subdoc)
                  data[k] = (this[k] == null) ? null : this[k]._id;
              } else data[k] = this[k];

            }

            delete data._id;

            horizon.db.collection(this.__collection)
              .updateOne({_id: HZEntity.id(this._id)}, {$set: data})
              .then((result) => {

                this.emit('save');
                horizon.emit('entity.save', this);

                resolve(this);

              })
              .catch((err) => {

                console.log(`Retry save [${this._type}] => ${retries + 1} (${err.message})`);

                setImmediate(() => {

                  this
                    .save(withSubdocs, ++retries, oldId = this._id)
                    .then(() => resolve(this))
                  ;

                });

              })
            ;

          });

        }

      });

    }

    /**
     * Fetch fresh entity data
     */
    fetch() {

      if(this._id == null) {

        return new Promise((resolve, reject) => {
          resolve(this);
        })

      }

      return this.constructor
        .findOne({_id: HZEntity.id(this._id)})
        .then((entity) => {

          for(let k in fields[this.constructor.name])
            this[k] = entity[k];

          return new Promise((resolve, reject) => {
            resolve(this);
          })

        });
      ;

    }

    load(data = {}) {

      for(let k in fields[this.constructor.name])
        if(data.hasOwnProperty(k) && k !== '_id')
          this[k] = data[k];

      return this;
    }

    /**
     * Bind volatile data to entity, will not be saved in database
     * @param {String} k - Key
     * @param {Any} v - Value
     */
    data(k, v) {

      if(this._id == null)
        throw new Error('Can\'t bind or load data, no _id field');

      if(typeof v != 'undefined')
        data[this._id][k] = v;
      else if(typeof k != 'undefined')
        return data[this._id][k];
      else
        return data[this._id];

    }

    /**
     * Delete entity in database
     */
    delete() {

      if(!this._id)
        return new Promise((resolve, reject) => resolve());

      const idStr    = this._idStr;
      const promises = [];

      for(let k in fields[this.constructor.name]) {

        const options = fields[this.constructor.name][k].options;

        if(typeof options.subdoc != 'undefined') {

          if(options.isLinked !== false) {

            if(this[k] instanceof Array) {

              for(let i=0; i<this[k].length; i++)
                if(this[k][i] != null)
                  promises.push(this[k][i].delete());

            } else if(this[k] instanceof options.subdoc) {

              promises.push(this[k].delete());

            }

          }

        }

      }

      promises.push(horizon.db.collection(this.__collection).deleteOne({_id: this._id}));

      return new Promise((resolve, reject) => {

        if(cache[this._idStr])
          cache[this._idStr].delete();

        Promise.all(promises).then(resolve);

      });

     }

    /**
     * Clone current entity to new HZEntity instance
     */
    clone() {

      const propKeys = props[this.constructor.name].map(e => e.name);
      const data     = {};

      for(let k in this.fields)
        if(k !== '_id')
          data[k] = this[k];

      if(typeof propKeys != 'undefined')
        for(let i=0; i<propKeys.length; i++)
          data[propKeys[i]] = this[propKeys[i]];

      return new this.constructor(data, false);

    }

    /**
     * Copy current entity to plain object
     */
    copy() {

      const propKeys = props[this.constructor.name].map(e => e.name);
      const obj      = {};

      for(let k in this.fields)
        obj[k] = this[k] instanceof Array ? this[k].slice(0) : this[k];

      if(typeof propKeys != 'undefined')
        for(let i=0; i<propKeys.length; i++)
          obj[propKeys[i]] = this[propKeys[i]];

      obj._id    = this._id;
      obj._idStr = this._idStr;

      return obj;

    }

    /**
     * Clear volatile data
     */
    clearData() {

      if(this._id == null)
        throw new Error('Can\'t clear data, no _id field');

      for(let k in data[this._id])
        if(data[this._id].hasOwnProperty(k))
          delete data[this._id][k];
    }

    /**
     * Create JSON string from current entity
     * @param {Number} space - Space
     */
    toJSON(space = 0) {
      return JSON.stringify(this.toObject(), space);
    }

    /*
    static unserialize(data) {

      if(data === null)
        return null;

      const obj = {};

      for(let k in data) {

        if(data[k] !== null && typeof data&& typeof data[k] === 'object' && typeof data[k]._id !== 'undefined')
          obj[k] = (typeof data[k]._type === 'undefined') ? ObjectID(data[k]._id) : this.unserialize(data[k]);
        else if(data[k] instanceof Array)
          obj[k] = data[k].map(e => (e === null) ? null : (typeof e == 'object' && typeof e._id !== 'undefined' ? (typeof e._type === 'undefined' ? ObjectID(e._id) : this.unserialize(e)) : e));
        else
          obj[k] = data[k];

      }

      return new types[data._type](obj);

    }
    */

    /**
     * Serialize current entity
     */
    serialize(...args) {

      const obj = {_type: this._type, _id: this._idStr};

      for(let k in this.fields) {
        if(this.fields.hasOwnProperty(k)) {

          const options = fields[this.constructor.name][k].options;

          if(typeof options.subdoc !== 'undefined'){

            if(this.fields[k] instanceof Array)
              obj[k] = this.fields[k].map(e => (!e || !e._id) ? null : {_type: e._type, _id: e._idStr});
            else
              obj[k] = (!this.fields[k] || !this.fields[k]._id) ? null : {_type: this.fields[k]._type, _id: this.fields[k]._idStr};

          } else {

            if(this.fields[k] instanceof ObjectID) {

              obj[k] = this.fields[k].toString();

            } else if(this.fields[k] instanceof Array) {

              obj[k] = this.fields[k].map(e => e instanceof ObjectID ? {_id: e.toString()} : e);

            } else {

              obj[k] = this.fields[k];

            }

          }

        }
      }

      const propKeys = props[this.constructor.name].map(e => e.name);

      if(typeof propKeys != 'undefined')
        for(let i=0; i<propKeys.length; i++)
          obj[propKeys[i]] = this[propKeys[i]];

      return obj;

    }

    /**
     * Create object from current entity
     */
    toObject(ignorePrivate = true, withSubdocs = true) {

      const obj      = {};
      const propKeys = props[this.constructor.name].map(e => e.name);

      for(let k in this.fields) {
        if(this.fields.hasOwnProperty(k)) {

          const options = fields[this.constructor.name][k].options;

          if(options.private && !ignorePrivate)
            continue;

          if(k != '_id' && typeof fields[this.constructor.name][k].options.subdoc != 'undefined'){

            if(withSubdocs) {

              if(this.fields[k] instanceof Array)
                obj[k] = this.fields[k].map(e => (e == null) ? null : e.toObject());
              else if(this.fields[k] instanceof HZEntity)
                obj[k] = (this.fields[k] == null) ? null : this.fields[k].toObject();

            }

          } else {

            if(this.fields[k] instanceof ObjectID) {

              obj[k] = this.fields[k].toString();

            } else if(this.fields[k] instanceof Array) {

              obj[k] = this.fields[k].map(e => e instanceof ObjectID ? e.toString() : e);

            } else obj[k] = this.fields[k];

          }

        }
      }

      if(typeof propKeys != 'undefined')
        for(let i=0; i<propKeys.length; i++)
          obj[propKeys[i]] = this[propKeys[i]];

      return obj;
    }

    /**
     * Create object from current entity (keep only structure)
     */
    toStructure() {

      const obj      = {};
      const propKeys = props[this.constructor.name].map(e => e.name);

      for(let k in this.fields) {
        if(this.fields.hasOwnProperty(k)) {

          if(k != '_id' && typeof fields[this.constructor.name][k].options.subdoc != 'undefined'){

            if(this.fields[k] instanceof Array)
              obj[k] = [];
            else if(this.fields[k] instanceof HZEntity)
              obj[k] = null;

          } else {
            obj[k] = this.fields[k];
          }

        }
      }

      if(typeof propKeys != 'undefined') {

        for(let i=0; i<propKeys.length; i++)
          obj[propKeys[i]] = this[propKeys[i]];

      }

      return obj;
    }

  }

  HZEntity
    .field('_id',         {default: null})
    .field('_data',       {default: {}})
    .prop('ttl',          () => -1)
    .prop('_subdocs', function(){
      return this.subdocs.map(e => ({
        name      : e.name,
        type      : fields[e.type.prototype.constructor.name]._type.options.default,
        typeChain : e._typeChain,
        isArray   : e.isArray
      }
    ))})
    .prop('_typeChain', function() {
      return typeChains[this.constructor.name];
    });

  ;

  HZEntity.__init = function(cb) {

    setInterval(() => {

      const now   = +new Date;
      let   count = 0;

      for(let k in cache) {

        const entry      = cache[k];
        const oldPersist = entry.persist;
        const persist    = entry.parents.filter(e => e.persist).length > 0;

        if(persist)
          entry.persist = true;
        else
          entry.persist = entry.defaultPersist;

        if(oldPersist && !entry.persist) {

          if(entry.resolveTimeout)
            clearTimeout(entry.resolveTimeout);

          entry.resolveTimeout = setTimeout(() => {
            entry.entity         = null
            entry.resolveTimeout = null
          }, CACHE_RESOLVE_TIME);


        } else if(!oldPersist && entry.persist && !entry.entity) {

          entry.entity = entry.resolve();

        }

        if(!entry.persist && now - entry.time >= CACHE_TIME && entry.parents.length === 0) {
          entry.delete();
          count++;
        }

      }

    }, CACHE_TICK);

    setInterval(() => {

      const keys = Object.keys(cache);

      horizon.logger.info(keys.length + ' entries in cache | ' + keys.filter(e => cache[e].entity).length + ' entities in pointers' + ' | ' + keys.filter(e => cache[e].persist).length + ' persistent');

    }, CACHE_LOG_INTERVAL);

    cb();

  }


  return HZEntity;

}
