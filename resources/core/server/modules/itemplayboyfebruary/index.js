module.exports = (horizon, config) => {
   
  const HZItem = horizon.require('item');

  class HZItemPlayboyfebruary extends HZItem {

  }

  HZItemPlayboyfebruary
    .inheritFields(HZItem)
    .inheritActions(HZItem)
    .field('_type', {default: 'ItemPlayboyfebruary'})
    .field('image', {default: 'playboyfebruary'})
    .field('shape', {default: {"grid":[],"offset":{"x":5.999999999999986,"y":30.003311258278245}}})
    .field('name',  {default: 'itemplayboyfebruary'})
    .field('label', {default: 'Playboy Février'})
  ;

  return HZItemPlayboyfebruary;

}