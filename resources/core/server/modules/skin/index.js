const alt = require('alt');

module.exports = (horizon, config) => {

  const $ = horizon.$;

  const HZIdentity = horizon.require('identity');

  class HZSkin {

  }

  const instance = new HZSkin;

  instance.__init = function(cb) {

    const pay = async (player, cb, amount) => {

      const identity = await HZIdentity.findOne({_id: HZIdentity.id(player.getSyncedMeta('identity'))});

      if(identity === null) {
        cb(false);
        return;
      }

      let   count    = amount;
      const results  = identity.queryItem({_type: 'ItemBanknote'}).sort((a, b) => a.count - b.count);
      const toDelete = [];

      for(let i=0; i<results.length; i++) {

        const result = results[i];
        let   rCount = 0;

        while((count > 0) && (rCount <= result.item.count)) {
          count--;
          rCount++;
        }
        
        if(rCount > 0)
          toDelete.push({...result, count: rCount});

      }

      if(count === 0) {
        
        for(let i=0; i<toDelete.length; i++) {

          const td       = toDelete[i];
          td.item.count -= td.count;

          if(td.item.count === 0) {
            
            await td.container.removeItem(td.item);
            await td.item.delete();
          
            horizon.updateCache(null, td.container._idStr);

          } else {

            await td.item.save();
            horizon.updateCache(null, td.item._idStr);

          }

        }

        alt.emitClient(player, 'hz:notification:create', '<span style="color:green">Vous avez payé $' + amount + '</span>');

        cb(true);

      } else {
        
        alt.emitClient(player, 'hz:notification:create', '<span style="color:red">Pas assez de cash</span>');

        cb(false);

      }

    }

    $.registerCallback('hz:skin:pay', pay);

    cb();

  }

  return instance;

}