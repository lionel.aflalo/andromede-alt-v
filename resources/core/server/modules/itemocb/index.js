module.exports = (horizon, config) => {
     
    const HZItem = horizon.require('item');
  
    class HZItemOcb extends HZItem {
  
    }
  
    HZItemOcb
      .inheritFields(HZItem)
      .inheritActions(HZItem)
      .field('_type', {default: 'ItemOcb'})
      .field('image', {default: 'ocb'})
      .field('shape', {default: {"grid":[{"x":-12,"y":-30.314255214320838},{"x":88,"y":-30.314255214320838}],"offset":{"x":12,"y":30.314255214320838}}})
      .field('name',  {default: 'itemocb'})
      .field('label', {default: 'OCB'})
    ;
  
    return HZItemOcb;
  
  }