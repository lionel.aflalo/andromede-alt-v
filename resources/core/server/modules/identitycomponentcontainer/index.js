'use strict';

const alt = require('alt');

module.exports = function(horizon, config) {

	const HZContainer = horizon.require('container');

	class HZIdentityComponentContainer extends HZContainer {

		save(...args) {

			const player = alt.Player.all.find(e => e.getSyncedMeta('identity') && (e.getSyncedMeta('identity') === this.identity.toString()));

			if(player) {

				const model = player.getSyncedMeta('model');

				if(model) {

					const components = {};

					for(let i=0; i<this.items.length; i++) {
		
						const item = this.items[i];
		
						if(!item)
							continue;
		
						components[item.component] = {drawable: item.drawable, texture: item.texture, palette: item.palette};
		
					}
		
					model.components = components;

					player.setSyncedMeta('model', model);

				}
				
			}

			return super.save(...args);
		}

	}

	HZIdentityComponentContainer
    .inheritFields(HZContainer)
    .inheritActions(HZContainer)
		.field('_type',    {default: 'IdentityComponentContainer'})
		.field('name',     {default:'identity_components'})
    .field('rows',     {default: -1})
    .field('cols',     {default: -1})
		.field('label',    {default: 'IdentityComponents'})
		.field('identity', {required: true})
	;

	return HZIdentityComponentContainer;

};
