module.exports = (horizon, config) => {
     
    const HZItem = horizon.require('item');
  
    class HZItemCamera extends HZItem {
  
    }
  
    HZItemCamera
      .inheritFields(HZItem)
      .inheritActions(HZItem)
      .field('_type', {default: 'ItemCamera'})
      .field('image', {default: 'camera'})
      .field('shape', {default: {"grid":[{"x":-4.000000000000114,"y":-18.74738493723862},{"x":-4.000000000000114,"y":81.25261506276138},{"x":95.99999999999989,"y":-18.74738493723862},{"x":95.99999999999989,"y":81.25261506276138}],"offset":{"x":4.0000000000001705,"y":18.74738493723862}}})
      .field('name',  {default: 'itemcamera'})
      .field('label', {default: 'Appareil photo'})
    ;
  
    return HZItemCamera;
  
  }