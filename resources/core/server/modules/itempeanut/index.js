module.exports = (horizon, config) => {
     
    const HZItem = horizon.require('item');
  
    class HZItemPeanut extends HZItem {
  
    }
  
    HZItemPeanut
      .inheritFields(HZItem)
      .inheritActions(HZItem)
      .field('_type', {default: 'ItemPeanut'})
      .field('image', {default: 'peanut'})
      .field('shape', {default: {"grid":[{"x":-3,"y":-38.531145717463914},{"x":-3,"y":61.468854282536086}],"offset":{"x":3,"y":38.531145717463914}}})
      .field('name',  {default: 'itempeanut'})
      .field('label', {default: 'Cacahuète'})
    ;
  
    return HZItemPeanut;
  
  }