'use strict';

const alt = require('alt');

module.exports = function(horizon, config) {

	const HZContainer = horizon.require('container');

	class HZIdentityPropContainer extends HZContainer {

		save(...args) {

			const player = alt.Player.all.find(e => e.getSyncedMeta('identity') === this.identity.toString());

			if(player) {

				const model = player.getSyncedMeta('model');

				if(model) {

					const props = {};

					for(let i=0; i<this.items.length; i++) {
		
						const item = this.items[i];
		
						if(!item)
							continue;
		
						props[item.component] = {drawable: item.drawable, texture: item.texture};
		
					}
		
					model.props = props;

					player.setSyncedMeta('model', model);

				}
				
			}

			return super.save(...args);
		}

	}

	HZIdentityPropContainer
    .inheritFields(HZContainer)
    .inheritActions(HZContainer)
		.field('_type',    {default: 'IdentityPropContainer'})
		.field('name',     {default:'identity_props'})
    .field('rows',     {default: -1})
    .field('cols',     {default: -1})
		.field('label',    {default: 'IdentityProps'})
		.field('identity', {required: true})
	;

	return HZIdentityPropContainer;

};
