module.exports = (horizon, config) => {
     
    const HZItem = horizon.require('item');
  
    class HZItemCoffee extends HZItem {
  
      async use(cb, identity, count = 1) {

        const player   = identity.getAltPlayer();
        const toUpdate = [];

        identity.status.thirst += 0.1;

        if(identity.status.hunger > 1)
          identity.status.hunger = 1;

        if(identity.status.thirst > 1)
          identity.status.thirst = 1;

        this.count--;

        if(this.count === 0) {

          const container = await horizon.modules.container.findOne({_id: this.container});

          if(container) {
            container.removeItem(this);
            toUpdate.push(container._idStr);
          }

          this.delete();

        } else {
          
          this.save();

          toUpdate.push(this._idStr);

        }
        
        identity.cache(true);

        horizon.updateCache(player, ...toUpdate);

        cb();
      }

    }
  
    HZItemCoffee
      .inheritFields(HZItem)
      .inheritActions(HZItem)
      .field('_type', {default: 'ItemCoffee'})
      .field('image', {default: 'coffee'})
      .field('shape', {default: {"grid":[{"x":-21.000000000000114,"y":-3.726973684210634}],"offset":{"x":21.000000000000114,"y":3.726973684210634}}})
      .field('name',  {default: 'itemcoffee'})
      .field('label', {default: 'Café'})
    ;
  
    return HZItemCoffee;
  
  }