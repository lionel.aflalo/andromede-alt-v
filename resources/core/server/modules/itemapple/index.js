module.exports = (horizon, config) => {
     
    const HZItem = horizon.require('item');
  
    class HZItemApple extends HZItem {
  
    }
  
    HZItemApple
      .inheritFields(HZItem)
      .inheritActions(HZItem)
      .field('_type', {default: 'ItemApple'})
      .field('image', {default: 'apple'})
      .field('shape', {default: {"grid":[],"offset":{"x":10,"y":4}}})
      .field('name',  {default: 'itemapple'})
      .field('label', {default: 'Pomme'})
    ;
  
    return HZItemApple;
  
  }