const alt = require('alt');

module.exports = (horizon, config) => {
 
  const $ = horizon.$;

  const HZIdentity       = horizon.require('identity');
  const HZItemCasinoChip = horizon.require('itemcasinochip');

  class CasinoRoulette {
    
    constructor() {
      this.bets = new Map();
    }

    calculateEarnings(number, bets) {
      
      const earnings = [];

      for(let i=0; i<bets.length; i++) {

        const bet  = bets[i];
        const spot = config.chips[bet.idx];

        if(!spot)
          return [];

        if(spot.numbers.indexOf(number) !== -1) {

          earnings.push({
            label: config.earningNames[spot.type],
            amount: bet.chips * config.earnings[spot.type]
          });

        }

      }

      return earnings;

    }

    getResult(wheelBoneOffset, ballRelPosition) {

      const ballAngleRad = Math.atan2(ballRelPosition.y - wheelBoneOffset.y, ballRelPosition.x - wheelBoneOffset.x);
      let   ballAngleDeg = 360 - ((((ballAngleRad * (180 / Math.PI)) + 360) % 360) - ((360 / 38) / 2));

      if(ballAngleDeg >= 360)
        ballAngleDeg -= 360;

      if(ballAngleDeg < 0)
        ballAngleDeg += 360;

      let index = Math.round((ballAngleDeg / 360) * (config.wheel.numbers.length - 1));

      if(index < 0)                               index += config.wheel.numbers.length;
      if(index > config.wheel.numbers.length - 1) index -= config.wheel.numbers.length;

      return config.wheel.numbers[index];

    }

  }

  const instance = new CasinoRoulette;

  instance.__init = function(cb) {

    $.registerCallback('hz:casino.roulette:pay', async (player, cb, bets) => {

      const identity = await HZIdentity.findOne({_id: HZIdentity.id(player.getSyncedMeta('identity'))});

      if(!identity) {
        cb(false);
        return;
      }

      const amount = bets.map(e => e.chips).reduce((a, b) => a + b, 0);

      if(amount === 0) {
        cb(false);
        return;
      }

      let   count    = amount;
      const results  = identity.queryItem({_type: 'ItemCasinochip'}).sort((a, b) => a.count - b.count);
      const toDelete = [];

      for(let i=0; i<results.length; i++) {

        const result = results[i];
        let   rCount = 0;

        while((count > 0) && (rCount < result.item.count)) {
          count--;
          rCount++;
        }
        
        if(rCount > 0)
          toDelete.push({...result, count: rCount});
      }

      if(count === 0) {
        
        for(let i=0; i<toDelete.length; i++) {

          const td       = toDelete[i];
          td.item.count -= td.count;

          if(td.item.count === 0) {
            
            await td.container.removeItem(td.item);
            await td.item.delete();
          
            horizon.updateCache(player, td.container._idStr);

          } else {

            await td.item.save();
            horizon.updateCache(player, td.item._idStr);

          }

        }

        instance.bets.set(player, bets);

        cb(true);

      } else {
        
        instance.bets.set(player, null);
        cb(false);

      }

    });

    $.registerCallback('hz:casino.roulette:result', async (player, cb, wheelBoneOffset, ballRelPosition) => {

      const number   = instance.getResult(wheelBoneOffset, ballRelPosition);
      const bets     = instance.bets.get(player);
      const earnings = instance.calculateEarnings(number, bets);
      const total    = earnings.map(e => e.amount).reduce((a,b) => a + b, 0);

      instance.bets.set(player, null);

      const identity = await HZIdentity.findOne({_id: HZIdentity.id(player.getSyncedMeta('identity'))});

      if(!identity) {
        cb([]);
        return;
      }

      const results = identity.queryItem({_type: 'ItemCasinochip'}).sort((a, b) => a.count - b.count);

      if(results.length > 0) {
        results[0].item.count += total;
        await results[0].item.save();
        horizon.updateCache(null, results[0].item._idStr);
      } else {

        const item      = new HZItemCasinoChip({count: total});
        const freeSpace = identity.getFreeSpace(item);

        if(freeSpace)
          await freeSpace.container.addItem(item, freeSpace.x, freeSpace.y);
        else
          alt.emitClient(player, 'hz:notification:create', 'Erreur, plus de place dans l\'inventaire');

      }

      cb(earnings);

    });

    cb();

  }

  return instance;

}