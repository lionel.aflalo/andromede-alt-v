'use strict';

module.exports = function(horizon, config) {

  const HZEntity = horizon.require('entity');

	class HZAccount extends HZEntity {
		
		set money(val) {

			if(val > this.money) {
				this.emit('add', val - this.money);
				horizon.emit('account.add', this);
			} else if(val < this.money) {
				this.emit('remove',  this.money - val);
				horizon.emit('account.remove', this);
			}

			this.fields.money = val;

		}

	}

	HZAccount
		.collection('accounts')
		.inheritFields(HZEntity)
		.field('_type', {default: 'Account'})
		.field('limit', {default: -1})
		.field('money', {default: 0, customSetter: true})
	;

	return HZAccount;

}
