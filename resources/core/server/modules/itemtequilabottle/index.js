module.exports = (horizon, config) => {
     
    const HZItem = horizon.require('item');
  
    class HZItemTequilabottle extends HZItem {
  
    }
  
    HZItemTequilabottle
      .inheritFields(HZItem)
      .inheritActions(HZItem)
      .field('_type', {default: 'ItemTequilabottle'})
      .field('image', {default: 'tequilabottle'})
      .field('shape', {default: {"grid":[{"x":-12,"y":-3.4208860759492836},{"x":-12,"y":96.57911392405072}],"offset":{"x":12,"y":3.4208860759492836}}})
      .field('name',  {default: 'itemtequilabottle'})
      .field('label', {default: 'Bouteille de tequila'})
    ;
  
    return HZItemTequilabottle;
  
  }