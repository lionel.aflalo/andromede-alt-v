module.exports = (horizon, config) => {
   
  const HZItem = horizon.require('item');

  class HZItemPlayboyjanuary extends HZItem {

  }

  HZItemPlayboyjanuary
    .inheritFields(HZItem)
    .inheritActions(HZItem)
    .field('_type', {default: 'ItemPlayboyjanuary'})
    .field('image', {default: 'playboyjanuary'})
    .field('shape', {default: {"grid":[{"x":-2,"y":-31.62087912087918},{"x":-2,"y":68.37912087912082}],"offset":{"x":2,"y":31.62087912087918}}})
    .field('name',  {default: 'itemplayboyjanuary'})
    .field('label', {default: 'Playboy Janvier'})
  ;

  return HZItemPlayboyjanuary;

}