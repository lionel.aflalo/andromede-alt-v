module.exports = (horizon, config) => {
     
    const HZcontainer = horizon.require('container');
  
    class HZSimcardSlot extends HZcontainer {

    }

    HZSimcardSlot
      .inheritFields(HZcontainer)
      .inheritActions(HZcontainer)
      .field('_type', {default: 'SimcardSlot'})
      .field('rows',  {default: 1})
      .field('cols',  {default: 1})
      .field('name',  {default: 'simcarslot'})
      .field('label', {default: 'SimcardSlot'})
    ;
  
    return HZSimcardSlot;
  
  }