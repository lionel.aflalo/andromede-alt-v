'use strict';

module.exports = (horizon, config) => {

  const Utils  = horizon.require('utils');
  const HZItem = horizon.require('item');

  const weapons    = require('../../../src/lib/altv-api/common/data/weapons.json');
  const components = require('../../../src/lib/altv-api/common/data/components.json');
  
  class HZWeapon extends HZItem {
    
  }

  HZWeapon
    .inheritFields(HZItem)
    .inheritActions(HZItem)
    .field('_type', {default: 'Weapon'})
    .field('hash', {required: true})
  ;

  return HZWeapon;

};