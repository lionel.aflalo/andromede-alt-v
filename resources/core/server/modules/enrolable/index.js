'use strict';

module.exports = function(horizon, config) {

	const HZEntity = horizon.require('entity');

	class HZEnrolable extends HZEntity {

		constructor(data) {

	    if (new.target === HZEnrolable)
	      throw new TypeError('Cannot construct HZEnrolable instances directly, must be extended');

			super(data);

		}

		hasRole(role) {
			return this.roles.findIndex(e => e.indexOf(role) === 0) != -1
		}

    hasRoles(...roles) {

      let hasRoles = true;

      if(roles.length > 0 && roles[0] instanceof Array)
        roles = roles[0];

      for(let i=0; i<roles.length; i++) {
        if(!this.hasRole(roles[i])) {
          hasRoles = false;
          break;
        }
      }

      return hasRoles;

    }

    hasOneRole(...roles) {

      if(roles.length === 0)
        return true;

      if(roles.length > 0 && roles[0] instanceof Array)
        roles = roles[0];

      for(let i=0; i<roles.length; i++)
        if(this.hasRole(roles[i]))
        	return true;

      return false;
    }

    static getSubRoles(roles, role) {
      
      const subRoles = [];

      for(let i=0; i<roles.length; i++) {

        if(roles[i].indexOf(role) === 0) {

          let subRole = roles[i].substr(role.length);

          if(subRole[0] == ':')
            subRole = subRole.substr(1);

          subRoles.push(subRole);

        }

      }

      return subRoles;

    }

    getSubRoles(role) {
      return this.constructor.getSubRoles(this.roles, role);
    }

    static hasPartialRole(roles, role) {
      return roles.findIndex(e => e.indexOf(role) !== -1) !== -1;
    }

    hasPartialRole(role) {
      return this.constructor.hasPartialRole(this.roles, role);
    }

	}

	HZEnrolable
		.inheritFields(HZEntity)
		.field('roles', {default: []})
	;

	return HZEnrolable;

};