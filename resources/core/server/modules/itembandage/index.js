module.exports = (horizon, config) => {
     
    const HZItem = horizon.require('item');
  
    class HZItemBandage extends HZItem {
  
    }
  
    HZItemBandage
      .inheritFields(HZItem)
      .inheritActions(HZItem)
      .field('_type', {default: 'ItemBandage'})
      .field('image', {default: 'bandage'})
      .field('shape', {default: {"grid":[{"x":-25,"y":-7.1453125000000455}],"offset":{"x":25,"y":7.1453125000000455}}})
      .field('name',  {default: 'itembandage'})
      .field('label', {default: 'Bandage'})
    ;
  
    return HZItemBandage;
  
  }