module.exports = (horizon, config) => {
     
    const HZItem = horizon.require('item');
  
    class HZItemLockpick extends HZItem {
  
    }
  
    HZItemLockpick
      .inheritFields(HZItem)
      .inheritActions(HZItem)
      .field('_type', {default: 'ItemLockpick'})
      .field('image', {default: 'lockpick'})
      .field('shape', {default: {"grid":[{"x":-21.999999999999886,"y":-9.360228716645452},{"x":78.00000000000011,"y":-9.360228716645452}],"offset":{"x":21.999999999999886,"y":9.360228716645452}}})
      .field('name',  {default: 'itemlockpick'})
      .field('label', {default: 'Lockpick'})
    ;
  
    return HZItemLockpick;
  
  }