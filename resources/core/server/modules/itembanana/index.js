module.exports = (horizon, config) => {
     
    const HZItem = horizon.require('item');
  
    class HZItemBanana extends HZItem {
  
      async use(cb, identity, count = 1) {

        const player   = identity.getAltPlayer();
        const toUpdate = [];

        identity.status.hunger += 0.1;

        if(identity.status.hunger > 1)
          identity.status.hunger = 1;

        if(identity.status.thirst > 1)
          identity.status.thirst = 1;

        this.count--;

        if(this.count === 0) {

          const container = await horizon.modules.container.findOne({_id: this.container});

          if(container) {
            container.removeItem(this);
            toUpdate.push(container._idStr);
          }

          this.delete();

        } else {
          
          this.save();

          toUpdate.push(this._idStr);

        }
        
        identity.cache(true);

        horizon.updateCache(player, ...toUpdate);

        cb();
      }

    }
  
    HZItemBanana
      .inheritFields(HZItem)
      .inheritActions(HZItem)
      .field('_type', {default: 'ItemBanana'})
      .field('image', {default: 'banana'})
      .field('shape', {default: {"grid":[{"x":-3.0000000000001137,"y":-21.614355231143577}],"offset":{"x":3.0000000000001137,"y":21.614355231143577}}})
      .field('name',  {default: 'itembanana'})
      .field('label', {default: 'Banane'})
    ;
  
    return HZItemBanana;
  
  }