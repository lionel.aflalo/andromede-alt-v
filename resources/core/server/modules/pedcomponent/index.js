module.exports = (horizon, config) => {

  const $                  = horizon.$;
  const { PED_COMPONENTS } = $.constants;

  const HZItem = horizon.require('item');

  class HZPedComponent extends HZItem {
    
    isTorso() {
      return this.component === PED_COMPONENTS.UPPR;
    }

    isGloves() {

      if(this.isTorso())
        return this.drawable > 8;

      return false;
    }

  }

  const hasImage = [1, 4, 6, 11];

  const getImage = function() {

    let src = 'pedcomponents/' + this.parentModel + '/component_' + this.component + '_' + this.drawable + '_' + this.texture;

    if(hasImage.indexOf(this.component) === -1)
      src = 'pedcomponents/component_' + this.component;

    return src;
  }

  HZPedComponent
    .inheritFields(HZItem)
    .inheritActions(HZItem)
    .field('_type',       {default: 'PedComponent'})
    .field('parentModel', {required: true})
    .field('name',        {default: 'pedcomponent'})
    .field('component',   {required: true})
    .field('drawable',    {required: true})
    .field('texture',     {required: true})
    .field('palette',     {required: true})
    .prop('image', getImage);
  ;

  return HZPedComponent;

}