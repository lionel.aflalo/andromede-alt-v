const alt = require('alt');

module.exports = (horizon, config) => {
     
    const HZItem        = horizon.require('item');
    const HZIdentity    = horizon.require('identity');
    const HZItemSimcard = horizon.require('itemsimcard');
    const HZSimcardSlot = horizon.require('simcardslot');

    const phonesByPlayer  = new Map();
    const phonesByNumber  = {};
    const playersByNumber = {};
    const numbersByPhone  = {};
  
    const calls = {};

    class HZItemPhone extends HZItem {

      initSimCardSlot(cb) {
        const slot = new HZSimcardSlot();
        slot.save().then(() => cb(null, slot));
      }

      use(cb, identity, ...args) {

        const player = identity.getAltPlayer();

        if(identity.health > 0)
          alt.emitClient(player, 'hz:item:use:phone', this._idStr);
        else
          alt.emitClient(player, 'hz:notification:create', 'Action impossible, vous êtes dans le coma');
      }

    }
  
    HZItemPhone
      .inheritFields(HZItem)
      .inheritActions(HZItem)
      .field('_type',       {default: 'ItemPhone'})
      .field('image',       {default: 'phone'})
      .field('shape',       {default: {"grid":[{"x":-7.999999999999545,"y":-18.99135802469118},{"x":-7.999999999999545,"y":81.00864197530882}],"offset":{"x":7.999999999999545,"y":18.99135802469118}}})
      .field('name',        {default: 'itemphone'})
      .field('label',       {default: 'Téléphone'})
      .field('contacts',    {default: []})
      .field('messages',    {default: []})
      .field('simcardslot', {subdoc: HZSimcardSlot, default: null, init: 'initSimCardSlot'})
      .prop('persist', () => false)
    ;

    HZItemPhone.__init = function(cb) {

      setInterval(async () => {

        for(let i=0; i<alt.Player.all.length; i++) {

          const player        = alt.Player.all[i];
          let   phones        = [];
          const identityIdStr = player.getSyncedMeta('identity');
          const identity      = await HZIdentity.findOne({_id: HZIdentity.id(identityIdStr)});

          if(identity) {

            for(let j=0; j<identity.containers.length; j++) {

              const container = identity.containers[j];
              const _phones   = container.items.filter(e => e instanceof HZItemPhone);

              phones = phones.concat(_phones);
            }

            phonesByPlayer[player] = phones;

            for(let j=0; j<phones.length; j++) {
  
              const phone   = phones[j];
              const simcard = phone.simcardslot.items[0];

              if(simcard) {
                phonesByNumber[simcard.number]  = phone;
                playersByNumber[simcard.number] = player;
                numbersByPhone[phone._idStr]    = simcard.number;
              }
  
            }

          }

        }

      }, 5000);

      alt.onClient('hz:phone:message', async (player, phoneIdStr, to, body) => {

        const identityIdStr = player.getSyncedMeta('identity');
        const identity      = await HZIdentity.findOne({_id: HZIdentity.id(identityIdStr)});
        const phone         = await HZItemPhone.findOne({_id: HZItemPhone.id(phoneIdStr)});

        if(!identity || !phone)
          return;

        const message = {from: phone.simcardslot.items[0].number, to, body, time: Math.floor(new Date / 1000)};

        phone.messages.push(message);
        
        const toPlayer = playersByNumber[to];
        
        if(toPlayer) {

          const toPhone = phonesByNumber[to];
          toPhone.messages.push(message);

          await toPhone.save();
      
          horizon.updateCache(toPlayer, toPhone._idStr);
        }

        if(toPlayer && toPlayer !== player) {
          horizon.updateCache(player, phone._idStr);
          await phone.save();
        }

      });

      alt.onClient('hz:phone:contact:add', async (player, phoneIdStr, contact) => {

        const identityIdStr = player.getSyncedMeta('identity');
        const identity      = await HZIdentity.findOne({_id: HZIdentity.id(identityIdStr)});
        const phone         = await HZItemPhone.findOne({_id: HZItemPhone.id(phoneIdStr)});

        if(!identity || !phone)
          return;

        phone.contacts.push(contact);

        await phone.save();

        horizon.updateCache(player, phone._idStr);

      });

      alt.onClient('hz:phone:contact:save', async (player, phoneIdStr, index, contact) => {

        const identityIdStr = player.getSyncedMeta('identity');
        const identity      = await HZIdentity.findOne({_id: HZIdentity.id(identityIdStr)});
        const phone         = await HZItemPhone.findOne({_id: HZItemPhone.id(phoneIdStr)});

        if(!identity || !phone)
          return;

        phone.contacts[index] = contact;

        await phone.save();

        horizon.updateCache(player, phone._idStr);

      });

      alt.onClient('hz:phone:call', async (player, phoneIdStr, to) => {

        const identityIdStr = player.getSyncedMeta('identity');
        const identity      = await HZIdentity.findOne({_id: HZIdentity.id(identityIdStr)});
        const phone         = await HZItemPhone.findOne({_id: HZItemPhone.id(phoneIdStr)});

        if(!identity || !phone)
          return;
        
        const toPlayer = playersByNumber[to];
        const toPhone  = phonesByNumber[to];

        if(!toPlayer || !toPhone)
          return;

        alt.emitClient(toPlayer, 'hz:phone:call:incoming', numbersByPhone[phone._idStr], toPhone._idStr);

      });

      alt.onClient('hz:phone:call:accept', (player, number, target) => {

        console.log('accept', number, target);

        const toPlayer = playersByNumber[target];
        const key      = [number, target].sort().join('_');
        calls[key]     = new alt.VoiceChannel(false, -1);

        calls[key].addPlayer(player);
        calls[key].addPlayer(toPlayer);

      });

      alt.onClient('hz:phone:call:deny', (player, number, target) => {

        console.log('deny', number, target);

        const toPlayer = playersByNumber[target];
        alt.emitClient(toPlayer, 'hz:phone:call:end');
      });

      alt.onClient('hz:phone:call:end', (player, number, target) => {

        console.log('end', number, target);

        const toPlayer = playersByNumber[target];
        const key      = [number, target].sort().join('_');
        
        calls[key].destroy();
        
        alt.emitClient(toPlayer, 'hz:phone:call:end');

      });

      cb();

    }
  
    return HZItemPhone;
  
  }