module.exports = (horizon, config) => {
     
    const HZItem = horizon.require('item');
  
    class HZItemImmatcertificate extends HZItem {
  
    }
  
    HZItemImmatcertificate
      .inheritFields(HZItem)
      .inheritActions(HZItem)
      .field('_type', {default: 'ItemImmatcertificate'})
      .field('image', {default: 'immatcertificate'})
      .field('shape', {default: {"grid":[],"offset":{"x":2.9999999999999574,"y":33.75170842824605}}})
      .field('name',  {default: 'itemimmatcertificate'})
      .field('label', {default: 'Certificat d\'immatriculation'})
    ;
  
    return HZItemImmatcertificate;
  
  }