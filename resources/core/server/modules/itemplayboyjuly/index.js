module.exports = (horizon, config) => {
   
  const HZItem = horizon.require('item');

  class HZItemPlayboyjuly extends HZItem {

  }

  HZItemPlayboyjuly
    .inheritFields(HZItem)
    .inheritActions(HZItem)
    .field('_type', {default: 'ItemPlayboyjuly'})
    .field('image', {default: 'playboyjuly'})
    .field('shape', {default: {"grid":[],"offset":{"x":4,"y":29.79170344218892}}})
    .field('name',  {default: 'itemplayboyjuly'})
    .field('label', {default: 'Playboy Juillet'})
  ;

  return HZItemPlayboyjuly;

}