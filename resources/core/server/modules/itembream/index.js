module.exports = (horizon, config) => {
     
    const HZItem = horizon.require('item');
  
    class HZItemBream extends HZItem {
  
    }
  
    HZItemBream
      .inheritFields(HZItem)
      .inheritActions(HZItem)
      .field('_type', {default: 'ItemBream'})
      .field('image', {default: 'bream'})
      .field('shape', {default: {"grid":[{"x":-12.303934871099045,"y":-9.696065128900955},{"x":87.69606512890095,"y":-9.696065128900955}],"offset":{"x":12.303934871099045,"y":9.696065128900955}}})
      .field('name',  {default: 'itembream'})
      .field('label', {default: 'Bream'})
    ;
  
    return HZItemBream;
  
  }