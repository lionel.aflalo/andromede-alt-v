module.exports = (horizon, config) => {
     
    const HZItem = horizon.require('item');
  
    class HZItemChips extends HZItem {
  
      async use(cb, identity, count = 1) {

        const player   = identity.getAltPlayer();
        const toUpdate = [];

        identity.status.hunger += 0.1;

        if(identity.status.hunger > 1)
          identity.status.hunger = 1;

        if(identity.status.thirst > 1)
          identity.status.thirst = 1;

        this.count--;

        if(this.count === 0) {

          const container = await horizon.modules.container.findOne({_id: this.container});

          if(container) {
            container.removeItem(this);
            toUpdate.push(container._idStr);
          }

          this.delete();

        } else {
          
          this.save();

          toUpdate.push(this._idStr);

        }
        
        identity.cache(true);

        horizon.updateCache(player, ...toUpdate);

        cb();
      }

    }
  
    HZItemChips
      .inheritFields(HZItem)
      .inheritActions(HZItem)
      .field('_type', {default: 'ItemChips'})
      .field('image', {default: 'chips'})
      .field('shape', {default: {"grid":[{"x":-12.999999999999929,"y":-4.026558891454897}],"offset":{"x":12.999999999999929,"y":4.026558891454897}}})
      .field('name',  {default: 'itemchips'})
      .field('label', {default: 'Chips'})
    ;
  
    return HZItemChips;
  
  }