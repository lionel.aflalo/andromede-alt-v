'use strict';

const crypto = require('crypto');
const rhd    = require('node-humanhash');

module.exports = function(horizon, config) {

  const Utils        = {};
  const hexCache     = {};

  Utils.getRandomFloat = function(min, max) {
    return Math.random() * (max - min) + min;
  }

  Utils.getRandomInt = function(min, max) {
    return Math.floor(Math.random() * (max - min + 1) + min);
  }

  Utils.getRandomBool = function() {
    return Math.random() >= 0.5;
  }

  Utils.getRandomString = function(length = 8) {

    let s = '';
    
    const randomchar = () => {
      
      const n = Math.floor(Math.random() * 62);
      
      if (n < 10)
        return n; //1-10
      
      if (n < 36)
        return String.fromCharCode(n + 55); //A-Z
      
      return String.fromCharCode(n + 61); //a-z
    
    }
    
    while (s.length < length)
      s += randomchar();

    return s;
  }

  Utils.twosComplement = function(value) {
    return (~value + 1) >>> 0;
  }

  Utils.toUnsigned = function(value) {
    return value >>> 0;
  }

  Utils.toSigned = function(value, nbit = 32) {
    value = value << 32 - nbit;
    value = value >> 32 - nbit;
    return value;
  }

  Utils.nameClass = function(name, cls) {
    return ({[name] : class extends cls {}})[name];
  }

  Utils.joaat = function(key){

    var keyLowered = key.toLowerCase();
    var length = keyLowered.length;
    var hash, i;

    for (hash = i = 0; i < length; i++) {
      hash += keyLowered.charCodeAt(i);
      hash += (hash <<  10);
      hash ^= (hash >>> 6);
    }

    hash += (hash <<  3);
    hash ^= (hash >>> 11);
    hash += (hash <<  15);

    return Utils.toSigned(hash);

  }

  Utils.hash = function(str) {
    const shasum = crypto.createHash('sha1');
    shasum.update(str);
    return shasum.digest('hex');
  }

  Utils.humanHash = function(str, length = 6) {

    while(str.length < length)
      str += '_';

    return rhd.humanizeDigest(str.substr(0, length)).split(' ').join('-');
  }

  Utils.codeName = function(str, length = 40, codeLength = 4) {

    while(str.length < length)
      str += '_';

    const letters = rhd.humanizeDigest(str.substr(0, length)).split(' ').map(e => e[0].toLowerCase());

    letters.length = codeLength;

    return letters.join('');
  }

  Utils.getDistance = function(a, b, useZ = true) {

    if(!a || !b)
      return Infinity;

    if(useZ) {

      return Math.sqrt(
        Math.pow(b.x - a.x, 2) +
        Math.pow(b.y - a.y, 2) +
        Math.pow(b.z - a.z, 2)
      );

    } else {

      return Math.sqrt(
        Math.pow(b.x - a.x, 2) +
        Math.pow(b.y - a.y, 2)
      );

    }

  }

  Utils.dec2hex = function(str) {

    if(hexCache[str])
      return hexCache[str];

    let dec = str.toString().split(''), sum = [], hex = [], i, s;

    while(dec.length){

      s = 1 * dec.shift();

      for(i = 0; s || i < sum.length; i++){
        s += (sum[i] || 0) * 10;
        sum[i] = s % 16;
        s = (s - sum[i]) / 16;
      }

    }

    while(sum.length)
      hex.push(sum.pop().toString(16));

    hexCache[str] = hex.join('');

    return hexCache[str];
  }

  return Utils;

};
