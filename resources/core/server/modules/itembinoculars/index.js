module.exports = (horizon, config) => {
     
    const HZItem = horizon.require('item');
  
    class HZItemBinoculars extends HZItem {
  
    }
  
    HZItemBinoculars
      .inheritFields(HZItem)
      .inheritActions(HZItem)
      .field('_type', {default: 'ItemBinoculars'})
      .field('image', {default: 'binoculars'})
      .field('shape', {default: {"grid":[{"x":-8.000000000000085,"y":-34.857526881720446},{"x":-8.000000000000085,"y":65.14247311827955},{"x":91.99999999999991,"y":-34.857526881720446},{"x":91.99999999999991,"y":65.14247311827955}],"offset":{"x":8.000000000000057,"y":34.857526881720446}}})
      .field('name',  {default: 'itembinoculars'})
      .field('label', {default: 'Jumelles'})
    ;
  
    return HZItemBinoculars;
  
  }