const alt = require('alt');

module.exports = (horizon, config) => {

  const HZPlayer   = horizon.require('player');
  const HZDiscord  = horizon.require('discord');
  const HZIdentity = horizon.require('identity');

  const HZAdmin = {};

  HZAdmin.__init = function(cb) {

    alt.onClient('hz:admin:roles:set', async (player, playerIdStr, identityIdStr, playerRoles = null, identityRoles = null) => {

      const playerId = HZPlayer.id(player.getSyncedMeta('player'));
      const hzPlayer = await HZPlayer.findOne({_id: playerId});
  
      if(!hzPlayer || !hzPlayer.hasRole('admin') || !playerRoles || !identityRoles)
        return;

      const targetPlayerId   = HZPlayer.id(playerIdStr);
      const targetPlayer     = await HZPlayer.findOne({_id: targetPlayerId});
      const targetIdentityId = targetPlayer.identity._id;
      const targetIdentity   = targetPlayer.identity;

      if(identityRoles instanceof Array)
        targetIdentity.roles = identityRoles;

      if(playerRoles instanceof Array)
        targetPlayer.roles = playerRoles;

      targetIdentity.cache(true);
      targetPlayer.cache(true);

      horizon.updateCache(null, targetIdentity._idStr, targetPlayer._idStr);
      
      Promise.all([targetIdentity.save(), targetPlayer.save()]);

    });

    alt.onClient('hz:admin:kick', async (player, _idStr) => {

      const playerId = HZPlayer.id(player.getSyncedMeta('player'));
      const hzPlayer = await HZPlayer.findOne({_id: playerId});
  
      if(!hzPlayer || !hzPlayer.hasRole('admin'))
        return;

      const targetId = HZPlayer.id(_idStr);
      const target   = await HZPlayer.findOne({_id: targetId});

      target.getAltPlayer().kick();

    });

    alt.onClient('hz:admin:ban', async (player, _idStr) => {

      const playerId = HZPlayer.id(player.getSyncedMeta('player'));
      const hzPlayer = await HZPlayer.findOne({_id: playerId});
  
      if(!hzPlayer || !hzPlayer.hasRole('admin'))
        return;
  
      const targetId = HZPlayer.id(_idStr);
      const target   = await HZPlayer.findOne({_id: targetId});
      
      horizon.logger.info(`Banning ${target.name} (${target._idStr})`);
  
      target.roles = target.roles.filter(e => e !== 'whitelist');
      
      target.cache(true);

      horizon.updateCache(null, _idStr);

      target.save();
  
      const altPlayer = target.getAltPlayer();
  
      if(altPlayer)
        altPlayer.kick();
  
      if(target.identifiers.discord) {
  
        const guild       = HZDiscord.client.guilds.get(HZDiscord.config.guild);
        const discordUser = guild.roles.get(HZDiscord.config.whitelist).members.find(e => e.user.id === target.identifiers.discord);
  
        if(discordUser)
          discordUser.removeRole(HZDiscord.config.whitelist);
      }
  
    });

    cb();

  }

  return HZAdmin;
};