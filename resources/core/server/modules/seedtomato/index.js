module.exports = (horizon, config) => {

	const $              = horizon.$;
	const HZItemSeed     = horizon.require('itemseed');
	const HZSproutTomato = horizon.require('sprouttomato');

	class HZItemSeedTomato extends HZItemSeed {

		grow() {
			return HZSproutTomato;
		}

	}

	HZItemSeedTomato
		.inheritFields(HZItemSeed)
		.inheritActions(HZItemSeed)
		.field('_type',  {default: 'SeedTomato'})
		.field('name',   {default: 'seedtomato'})
		.field('label',  {default: 'SeedTomato'})
    .field('image',  {default: 'seed'})
    .field('shape',  {default: {"grid":[{"x":-32.0982905982906,"y":-6.901709401709397}],"offset":{"x":32.0982905982906,"y":6.901709401709397}}})
	;

	return HZItemSeedTomato;

}