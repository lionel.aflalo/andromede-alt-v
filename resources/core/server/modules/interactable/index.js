'use strict';

const alt = require('alt');

module.exports = function(horizon, config) {

  const $             = horizon.$;
  const HZPlayer      = horizon.require('player');
  const HZIdentity    = horizon.require('identity');
	const HZWorldObject = horizon.require('worldobject');
 
	class HZInteractable extends HZWorldObject {

	}

	HZInteractable
		.collection('interactables')
		.inheritFields(HZWorldObject)
    .field('_type',      {default: 'Interactable'})
    .field('name',       {required: true})
		.field('action',     {required: true})
		.field('group',      {default: []})
		.field('customData', {default: {}})
  ;
  
  HZInteractable.__init = function(cb) {

    $.registerCallback('hz:interactable:get:cube', async (player, cb, cube, radius = 2) => {

      const interactables = await HZInteractable
        .find({
          $or: [
            {
              'cube.x': {$gte: cube.x - radius, $lte: cube.x + radius},
              'cube.y': {$gte: cube.y - radius, $lte: cube.y + radius},
              'cube.z': {$gte: cube.z - radius, $lte: cube.z + radius},
            },
            {
              position: null,
            }
          ]
        })
      ;

      cb(interactables.map(e => ({_type: e._type, _id: e._idStr})));

    });

    $.registerCallback('hz:interactable:get:model', async (player, cb, model) => {
      const interactables = await HZInteractable.find({model});
      cb(interactables.map(e => ({_type: e._type, _id: e._idStr})));
    });

    $.registerCallback('hz:interactable:container:ensure', async (player, cb, interactableIdStr) => {

      const _id          = HZInteractable.id(interactableIdStr);
      const interactable = await HZInteractable.findOne({_id});

      const defs = interactable.customData.containers.map(e => ({
        mod: HZInteractable.types[e._type || 'SimpleContainer'],
        name: e.name,
        rows: e.rows,
        cols: e.cols
      }));

      const promises = [];

      for(let i=0; i<defs.length; i++) {

        const def = defs[i];

        promises.push(def.mod.findOrCreateOfType({name: def.name}, {
          name: def.name,
          rows: def.rows,
          cols: def.cols,
        }));

      }

      const containers = await Promise.all(promises);
      
      for(let i=0; i<containers.length; i++)
        containers[i].cache(true);

      cb(containers.map(e => ({_type: e._type, _id: e._idStr})));
      
    });

    alt.onClient('hz:interactable:create', async (player, name, action, model, position, group = [], customData = {}) => {

      console.log(name, model, position, group, customData);

      const hzPlayer = await HZPlayer.findOne({_id: HZPlayer.id(player.getSyncedMeta('player'))});

      if(!hzPlayer.hasRole('admin')) {
        horizon.logger.info(`${hzPlayer.name} (${hzPlayer._idStr}) tried to create an interactable while not admin`);
        return;
      }

      const interactable    = new HZInteractable({name, action, model, position, group, customData});
      interactable.position = position;
      
      interactable.save();
    });

    alt.onClient('hz:interactable:update', async (player, _idStr, name, action, model, position, group = [], customData = {}) => {

      const hzPlayer = await HZPlayer.findOne({_id: HZPlayer.id(player.getSyncedMeta('player'))});

      if(!hzPlayer.hasRole('admin')) {
        horizon.logger.info(`${hzPlayer.name} (${hzPlayer._idStr}) tried to create an interactable while not admin`);
        return;
      }

      const interactable = await HZInteractable.findOne({_id: HZInteractable.id(_idStr)});

      if(interactable) {

        interactable.load({name, action, model, position, group, customData});

        interactable.cache(true);

        horizon.updateCache(null, interactable._idStr);

        interactable.save();

      }

    });

    alt.onClient('hz:interactable:interact', async (player, _idStr) => {

      const interactable = HZInteractable.findOne({_id: HZInteractable.id(_idStr)});

      if(interactable) {

        const identity = HZIdentity.findOne({_id: HZIdentity.id(player.getSyncedMeta('identity'))});

        if(identity) {

          horizon.emit('interactable.interact:' + interactable.action, player, identity, interactable.customData);

        }

      }

    });

    cb();

  }

	return HZInteractable;
};
