module.exports = (horizon, config) => {
     
    const HZItem = horizon.require('item');
  
    class HZItemKey extends HZItem {
  
    }
  
    HZItemKey
      .inheritFields(HZItem)
      .inheritActions(HZItem)
      .field('_type', {default: 'ItemKey'})
      .field('image', {default: 'carkey'})
      .field('shape', {default: {"grid":[{"x":-2.0000000000001705,"y":-28.203416149068516}],"offset":{"x":2.0000000000001705,"y":28.203416149068516}}})
      .field('name',  {default: 'itemkey'})
      .field('label', {default: 'Clé'})
      .field('targets', {default: []})
    ;
  
    return HZItemKey;
  
  }