'use strict';

module.exports = function(horizon, config) {

  const HZContainer = horizon.require('container');

  class HZTrunkContainer extends HZContainer {

  }

  HZTrunkContainer
    .inheritFields(HZContainer)
    .field('_type', {default: 'TrunkContainer'})
  ;

  return HZTrunkContainer;

};
