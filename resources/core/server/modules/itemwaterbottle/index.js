module.exports = (horizon, config) => {
     
    const HZItem = horizon.require('item');
  
    class HZItemWaterbottle extends HZItem {
  
      async use(cb, identity, count = 1) {

        const player   = identity.getAltPlayer();
        const toUpdate = [];

        identity.status.thirst += 0.25;

        if(identity.status.hunger > 1)
          identity.status.hunger = 1;

        if(identity.status.thirst > 1)
          identity.status.thirst = 1;

        this.count--;

        if(this.count === 0) {

          const container = await horizon.modules.container.findOne({_id: this.container});

          if(container) {
            container.removeItem(this);
            toUpdate.push(container._idStr);
          }

          this.delete();

        } else {
          
          this.save();

          toUpdate.push(this._idStr);

        }
        
        identity.cache(true);

        horizon.updateCache(player, ...toUpdate);

        cb();
      }

    }
  
    HZItemWaterbottle
      .inheritFields(HZItem)
      .inheritActions(HZItem)
      .field('_type', {default: 'ItemWaterbottle'})
      .field('image', {default: 'waterbottle'})
      .field('shape', {default: {"grid":[{"x":-12.01560781928356,"y":-8.98439218071644},{"x":-12.01560781928356,"y":91.01560781928356}],"offset":{"x":12.01560781928356,"y":8.98439218071644}}})
      .field('name',  {default: 'itemwaterbottle'})
      .field('label', {default: 'Bouteille d\'eau'})
    ;
  
    return HZItemWaterbottle;
  
  }