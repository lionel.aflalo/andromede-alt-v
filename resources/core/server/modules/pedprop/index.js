module.exports = (horizon, config) => {

  const $             = horizon.$;
  const { PED_PROPS } = $.constants;

  const HZItem = horizon.require('item');

  class HZPedProp extends HZItem {

  }

  const getImage = function() {

    let src = 'pedprops/prop_' + this.component;

    return src;
  }

  HZPedProp
    .inheritFields(HZItem)
    .inheritActions(HZItem)
    .field('_type',       {default: 'PedProp'})
    .field('name',        {default: 'pedprop'})
    .field('parentModel', {required: true})
    .field('component',   {required: true})
    .field('drawable',    {required: true})
    .field('texture',     {required: true})
    .prop('image', getImage);
  ;

  return HZPedProp;

}