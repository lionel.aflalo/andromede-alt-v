'use strict';

const async    = require('async');
const ObjectID = require('mongodb').ObjectID;
const alt      = require('alt');

module.exports = function(horizon, config) {

  const $        = horizon.$;
  const Utils    = horizon.require('utils');
  const HZEntity = horizon.require('entity');

  const GRID_SIZE = 100;
  const CUBE_SIZE = 100;

  const actions     = {};

  /** Class representing an item*/
  class HZItem extends HZEntity {

    set count(val) {

      if(val > this.count) {
        this.emit('add', val - this.count, this.count);
        horizon.emit('item.add', this, val - this.count, this.count);
      } else if(val < this.count) {
        this.emit('remove',  this.count - val, this.count);
        horizon.emit('item.remove', this, this.count - val, this.count);
      }

      this.fields.count = val;
      this.__toSave     = true;
    }

    set world(val) {

      this.fields.world = {
        
        position: val.position,
        
        cube: {
          x: (val.position.x - (val.position.x % CUBE_SIZE)) / CUBE_SIZE,
          y: (val.position.y - (val.position.y % CUBE_SIZE)) / CUBE_SIZE,
          z: (val.position.z - (val.position.z % CUBE_SIZE)) / CUBE_SIZE,
        }

      };

      this.__toSave = true;
    }

    /**
     * Create an item.
     * @param {Object} data - Fields
     */
    constructor(data) {

      super(data);

      this._actions = [];

      if(typeof actions[this.constructor.name] != 'undefined')
        for(let i=0; i<actions[this.constructor.name].length; i++)
          this._actions.push(actions[this.constructor.name][i]);
    }

    /**
     * Check if current item can do given actions
     * @param {String} actionName - Action name
     */
    canDo(actionName) {

      for(let i=0; i<this._actions.length; i++)
        if(this._actions[i].name == actionName)
          return (typeof this._actions[i].condition == 'function') ? this._actions[i].condition.apply(this) : this._actions[i].condition;

      return false;

    }

    /**
     * Do an action
     * @param {String} actionName - Action name
     * @param {Any} args - Arguments
     */
    do(actionName, cb, identity = null, count = 1, ...args) {

      for(let i=0; i<this._actions.length; i++) {
        if(this._actions[i].name == actionName) {
          
          const realArgs = [cb, identity, count].concat(args);
          
          this.emit('action.' + actionName, count, ...args);
          horizon.emit ('item.action.' + actionName, this, count, ...args);
          
          this[this._actions[i].method].apply(this, realArgs);
       
        }
      }

      return this;
    }

    /**
     * Define an action for the current instance
     * @param {String} name - Action name
     * @param {String} method - Target method
     * @param {(Boolean|Function)} condition - Condition
     */
    action(name, method, condition) {
      this._actions.push({name, method, condition});
      return this;
    }

    getParentContainers() {
      return horizon.modules.container.find({items: {$in: [this._id]}});
    }

    getShape() {

      const _shape  = [];
      const bb      = {x: Infinity, y: Infinity};
      let   offsetX = 0;
      let   offsetY = 0;

      this.shape.grid.forEach(e => {

        if(e.x < bb.x)
          bb.x = e.x;

        if(e.y < bb.y)
          bb.y = e.y;

      });

      if(bb.x < 0)
        offsetX = Math.abs(bb.x);

      if(bb.y < 0)
        offsetY = Math.abs(bb.y);

      bb.x += offsetX;
      bb.y += offsetY;

      this.shape.grid.forEach(e => {

        const x  = e.x + offsetX;
        const y  = e.y + offsetY;
        const gX = Math.floor(x / GRID_SIZE);
        const gY = Math.floor(y / GRID_SIZE);

        _shape[gY]     = _shape[gY] || [];
        _shape[gY][gX] = true;

        let xMax = 0;

        _shape.forEach(y => y.forEach((x, ix) => {
          if(ix > xMax)
            xMax = ix;
        }));

        for(let _y = 0; _y<_shape.length; _y++)
          for(let _x = 0; _x <= xMax; _x++)
            if(!_shape[_y][_x])
              _shape[_y][_x] = false;

      });

      return _shape;
    }

    async give(cb, identity, count = 1) {
      const player = identity.getAltPlayer();
      alt.emitClient(player, 'hz:item:give:start', this._idStr);
    }

    /**
     * Define an action for all HZItem instances
     * @param {String} name - Action name
     * @param {String} method - Target method
     * @param {(Boolean|Function)} condition - Condition
     */
    static action(name, method, condition, noArgs = true) {
      actions[this.prototype.constructor.name] = actions[this.prototype.constructor.name] || [];
      actions[this.prototype.constructor.name].push({name, method, condition, noArgs});

      return this;
    }

    /**
     * Inherit actions from class
     * @param {Object} baseClass - Base class
     */
    static inheritActions(baseClass) {

      if(typeof actions[baseClass.prototype.constructor.name] == 'undefined')
        return;

      actions[this.prototype.constructor.name] = actions[this.prototype.constructor.name] || [];

      for(let i=0; i<actions[baseClass.prototype.constructor.name].length; i++){
        actions[this.prototype.constructor.name].push(actions[baseClass.prototype.constructor.name][i]);
      }

      return this;

    }
  }

  function getCanoDoActions() {

    const actions = [];

    for(let i=0; i<this._actions.length; i++)
      if(this.canDo(this._actions[i].name) && this._actions[i].noArgs)
        actions.push(this._actions[i].name);

    return actions;
  }

  HZItem
    .collection('items')
    .inheritFields(HZEntity)
    .field('_type'       , {default: 'Item'})
    .field('container'   , {default: null})
    .field('name'        , {required: true})
    .field('label'       , {default: 'Item'})
    .field('count'       , {default: 1, customSetter: true})
    .field('image'       , {default: ''})
    .field('shape'       , {default: {"grid":[{"x":-17,"y":-7}],"offset":{"x":17,"y":7}}})
    .field('x'           , {default: 0})
    .field('y'           , {default: 0})
    .field('world'       , {default: null, customSetter: true})
    .field('lifetime'    , {default: -1})
    .field('createTime'  , {default: () => Math.floor(new Date / 1000)})
    .prop('model'            , () => Utils.joaat('prop_money_bag_01'))
    .prop('usable'           , function() {return typeof this.use == 'function'})
    .prop('codeName'         , function() {return this._id ? Utils.codeName(Utils.hash(this._id.toString())) : '####'})
    .prop('variantHash'      , function() {return Utils.hash(this.constructor.name)})
    .prop('humanVariantHash' , function() {return Utils.humanHash(this.variantHash)})
    .prop('actions'          , getCanoDoActions)
    .prop('equip'            , function() {return {visible : false, bone: -1, offset: {x: 0, y: 0, z: 0}, rotation: {x: 0, y:0, z: 0}}})
    .prop('limit'            , () => 1)
    .field('image'           , {default: 'undefined'})
    .action('use',  'use',  function() {return this.usable && typeof this.use == 'function'})
    .action('give', 'give', () => true)
  ;

  HZItem.__init = function(cb) {

    alt.onClient('hz:item:action', async (player, _idStr, action) => {

      const identityIdStr = player.getSyncedMeta('identity');
      const identity = await horizon.modules.identity.findOne({_id: horizon.modules.identity.id(identityIdStr)});
      const item     = await HZItem.findOne({_id: HZItem.id(_idStr)});

      if(identity !== null && item !== null) {
        
        item.do(action, function(...args) {
          // Todo callback
        }, identity, 1, /* ...args */); // Todo variable count + args

      }

    });

    alt.onClient('hz:item:action:admin', async (player, _idStr, action) => {

      const playerIdStr   = player.getSyncedMeta('player');
      const identityIdStr = player.getSyncedMeta('identity');
      const hzPlayer      = await horizon.modules.player.findOne({_id: horizon.modules.player.id(playerIdStr)});
      const identity      = await horizon.modules.identity.findOne({_id: horizon.modules.identity.id(identityIdStr)});
      const item          = await HZItem.findOne({_id: HZItem.id(_idStr)});

      if(hzPlayer.hasRole('admin') && identity !== null && item !== null) {
        
        switch(action) {

          case 'delete_item': {
            const cid       = item.container;
            const container = await horizon.modules.container.findOne({_id: cid});

            if(container)
              await container.removeItem(item);

            await item.delete();
            horizon.updateCache(null, container.toString());
            break;
          }

          default: break;
        }

      }

    });

    $.registerCallback('hz:item:get:cube', async (player, cb, cube, radius = 2) => {

      const world = await horizon.modules.worldcontainer.findOne({name: 'world'});

      const items = world.items.filter(e => {

        return (
          e &&
          e.world &&
          e.world.cube &&
          e.world.cube.x >= cube.x - radius && e.world.cube.x <= cube.x + radius && 
          e.world.cube.y >= cube.y - radius && e.world.cube.y <= cube.y + radius &&
          e.world.cube.z >= cube.z - radius && e.world.cube.z <= cube.z + radius
        );

      });

      cb(items.map(e => ({_type: e._type, _id: e._idStr})));

    });

    cb();
  }

  $.registerCallback('hz:item:give', async (player, cb, _idStr, toPlayer, qty = 1) => {

    if(isNaN(qty) || qty < 1 || !toPlayer) {
      cb(false);
      return;
    }

    qty = Math.ceil(qty);

    const item     = await HZItem.findOne({_id: HZItem.id(_idStr)});
    const total    = qty > item.count ? item.count : qty;
    const accepted = await $.requestp(toPlayer, 'hz:item:give:request', item._type, item._idStr, qty);

    if(accepted) {

      const toIdentity = await horizon.modules.identity.findOne({_id: horizon.modules.identity.id(toPlayer.getSyncedMeta('identity'))});

      if(!toIdentity) {
        cb(false);
        return;
      }

      const fromContainer = await horizon.modules.container.findOne({_id: item.container});

      if(!fromContainer) {
        cb(false);
        return;
      }

      if(item.limit === 1) {

        const freeSpace = toIdentity.getFreeSpace(item);

        if(freeSpace) {
          
          const toContainer = freeSpace.container;

          let count = item.count;

          count -= total;

          if(count === 0) {

            await fromContainer.removeItem(item);
            await toContainer.addItem(item, freeSpace.x, freeSpace.y);

            horizon.updateCache(null, fromContainer._idStr, toContainer._idStr);

          } else {
            
            item.count = count;

            const newItem = new (item.constructor)({count: total});

            await item.save();
            await newItem.save();
            await toContainer.addItem(newItem, freeSpace.x, freeSpace.y);

            horizon.updateCache(null, fromContainer._idStr, toContainer._idStr, item._idStr);
          
          }
          
          cb(true);
        
        } else {
          
          cb(false);
        
        }

      } else {

        const freeSpace = toIdentity.getFreeSpace(item, total);
          
        if(freeSpace) {
          
          item.count -= total;

          const newItem = new (item.constructor)({count: total});

          await item.save();
          await newItem.save();

          const toContainer = freeSpace.container;
          
          await toContainer.addItem(newItem, freeSpace.x, freeSpace.y);
          
          horizon.updateCache(null, item._idStr, toContainer._idStr, item._idStr);

          cb(true);
        
        } else {
          
          cb(false);
        
        }
        
      }

    } else cb(false);

  });

  return HZItem;
};
