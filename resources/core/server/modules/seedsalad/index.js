module.exports = (horizon, config) => {

	const $              = horizon.$;
	const HZItemSeed     = horizon.require('itemseed');
	const HZSproutSalad = horizon.require('sprouttomato');

	class HZItemSeedSalad extends HZItemSeed {

		grow() {
			return HZSproutSalad;
		}

	}

	HZItemSeedSalad
		.inheritFields(HZItemSeed)
		.inheritActions(HZItemSeed)
		.field('_type',  {default: 'SeedSalad'})
		.field('name',   {default: 'SeedSalad'})
		.field('label',  {default: 'SeedSalad'})
    .field('image',  {default: 'seed'})
    .field('shape',  {default: {"grid":[{"x":-32.0982905982906,"y":-6.901709401709397}],"offset":{"x":32.0982905982906,"y":6.901709401709397}}})
	;

	return HZItemSeedSalad;

}