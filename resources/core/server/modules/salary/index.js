const alt = require('alt');

module.exports = (horizon, config) => {

  const HZIdentity              = horizon.require('identity');
  const HZIdentityBaseContainer = horizon.require('identitybasecontainer');
  const HZItemBanknote          = horizon.require('itembanknote');

  const Salary = {
    start: Math.floor(new Date / 1000),
  };

  Salary.processPayments = function() {

  }

  Salary.__init = function(cb) {

    /*
    setInterval(async () => {

      const players     = alt.Player.all;
      const identityIds = players.map(e => HZIdentity.id(e.getSyncedMeta('identity')));
      const identities  = await HZIdentity.find({_id: {$in: identityIds}});

      for(let i=0; i<players.length; i++) {

        (async (player, identity) => {

          if(!identity)
            return;

          try {

            for(const role in config.salaries) {

              const salary = config.salaries[role];
  
              if(identity.hasRole(role)) {
  
                const matches   = identity.queryItem({_type: 'ItemBanknote'});
                let   container = null;
                let   item      = null;
  
                if(matches.length > 0) {

                  container  = matches[0].container;
                  item       = matches[0].item;
                  item.count += salary;

                  item.cache(true);

                  horizon.updateCache(player, container._idStr, item._idStr);

                  item.save();

                } else {

                  container = identity.containers.find(e => e instanceof HZIdentityBaseContainer);
                  item      = new HZItemBanknote({count: salary});

                  await item.save();

                  container.addItem(item);

                  container.cache(true);

                  horizon.updateCache(player, container._idStr, item._idStr);

                  container.save();

                }
  
              }
  
            }

          } catch(e) { console.error(e); }
        
        })(players[i], identities[i]);

      }

    }, config.interval);
    */

    cb();
  }

  return Salary;
}