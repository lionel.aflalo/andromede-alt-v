const alt = require('alt');

const MS_PER_GAME_MINUTE = 1000;

module.exports = (horizon, config) => {

  class HZSyncTime {

    static get MS_PER_GAME_MINUTE() { return MS_PER_GAME_MINUTE; }

    constructor() {
      this.time = Math.floor(+new Date / 1000);
    }

  }

  const instance = new HZSyncTime;
  
  instance.__init = function(cb) {

    alt.Player.all.forEach(e => alt.emitClient(e, 'hz:time:set', instance.time, MS_PER_GAME_MINUTE));

    alt.on('playerConnect', player => {
      alt.emitClient(player, 'hz:time:set', instance.time, MS_PER_GAME_MINUTE);
    });

    setInterval(() => {
      instance.time += 60;
    }, MS_PER_GAME_MINUTE);

    setInterval(() => {
      alt.emitClient(null, 'hz:time:set', instance.time, MS_PER_GAME_MINUTE);
    }, 60 * 1000);

    cb();

  };

  return instance;
}