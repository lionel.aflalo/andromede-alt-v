module.exports = (horizon, config) => {
     
    const HZItem = horizon.require('item');
  
    class HZItemWatermelon extends HZItem {
  
      async use(cb, identity, count = 1) {

        const player   = identity.getAltPlayer();
        const toUpdate = [];

        identity.status.hunger += 0.05;

        if(identity.status.hunger > 1)
          identity.status.hunger = 1;

        if(identity.status.thirst > 1)
          identity.status.thirst = 1;

        this.count--;

        if(this.count === 0) {

          const container = await horizon.modules.container.findOne({_id: this.container});

          if(container) {
            container.removeItem(this);
            toUpdate.push(container._idStr);
          }

          this.delete();

        } else {
          
          this.save();

          toUpdate.push(this._idStr);

        }
        
        identity.cache(true);

        horizon.updateCache(player, ...toUpdate);

        cb();
      }

    }
  
    HZItemWatermelon
      .inheritFields(HZItem)
      .inheritActions(HZItem)
      .field('_type', {default: 'ItemWatermelon'})
      .field('image', {default: 'watermelon'})
      .field('shape', {default: {"grid":[{"x":-19,"y":-3.6738544474392256},{"x":81,"y":-3.6738544474392256}],"offset":{"x":19,"y":3.6738544474392256}}})
      .field('name',  {default: 'itemwatermelon'})
      .field('label', {default: 'Pastèque'})
    ;
  
    return HZItemWatermelon;
  
  }