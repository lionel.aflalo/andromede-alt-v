module.exports = (horizon, config) => {
     
    const HZItem = horizon.require('item');
  
    class HZItemVodkabottle extends HZItem {
  
    }
  
    HZItemVodkabottle
      .inheritFields(HZItem)
      .inheritActions(HZItem)
      .field('_type', {default: 'ItemVodkabottle'})
      .field('image', {default: 'vodkabottle'})
      .field('shape', {default: {"grid":[{"x":-18,"y":-5.5},{"x":-18,"y":94.5}],"offset":{"x":18,"y":5.5}}})
      .field('name',  {default: 'itemvodkabottle'})
      .field('label', {default: 'Bouteille de vodka'})
    ;
  
    return HZItemVodkabottle;
  
  }