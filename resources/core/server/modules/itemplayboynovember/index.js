module.exports = (horizon, config) => {
   
  const HZItem = horizon.require('item');

  class HZItemPlayboynovember extends HZItem {

  }

  HZItemPlayboynovember
    .inheritFields(HZItem)
    .inheritActions(HZItem)
    .field('_type', {default: 'ItemPlayboynovember'})
    .field('image', {default: 'playboynovember'})
    .field('shape', {default: {"grid":[{"x":-3.999999999999943,"y":-29.834801762114466},{"x":-3.999999999999943,"y":70.16519823788553}],"offset":{"x":3.999999999999943,"y":29.834801762114466}}})
    .field('name',  {default: 'itemplayboynovember'})
    .field('label', {default: 'PlayboyNovembre'})
  ;

  return HZItemPlayboynovember;

}