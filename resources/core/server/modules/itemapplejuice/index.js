module.exports = (horizon, config) => {
     
    const HZItem = horizon.require('item');
  
    class HZItemApplejuice extends HZItem {
  
    }
  
    HZItemApplejuice
      .inheritFields(HZItem)
      .inheritActions(HZItem)
      .field('_type', {default: 'ItemApplejuice'})
      .field('image', {default: 'applejuice'})
      .field('shape', {default: {"grid":[{"x":-3,"y":-6.618840579710195}],"offset":{"x":3,"y":6.618840579710195}}})
      .field('name',  {default: 'itemapplejuice'})
      .field('label', {default: 'Jus de pomme'})
    ;
  
    return HZItemApplejuice;
  
  }