const alt          = require('alt');
const EventEmitter = require('events');

module.exports = (horizon, config) => {

  const chat     = horizon.requireESM('../../chat');
  const HZPlayer = horizon.require('player');

  class HZConsole extends EventEmitter {

    constructor() {
      super();
    }

  }

  const instance = new HZConsole();

  instance.__init = function(cb) {

    alt.onClient('hz:cmd', async (player, cmd, ...args) => {
      
      const hzPlayer = await HZPlayer.findOne({_id: HZPlayer.id(player.getSyncedMeta('player'))});

      if(!hzPlayer || !hzPlayer.hasRole('admin')) {
        horizon.logger.info(`Player ${hzPlayer.name} (${hzPlayer._idStr}) tried to enter console command while not admin => ${cmd} ${args.join(' ')}`);
        return;
      }

      if(config.commands[cmd] !== undefined)
        config.commands[cmd](player, ...args);

    });

    cb();

  }

  return instance;

}