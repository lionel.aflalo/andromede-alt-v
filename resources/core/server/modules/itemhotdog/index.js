module.exports = (horizon, config) => {
     
    const HZItem = horizon.require('item');
  
    class HZItemHotdog extends HZItem {
  
      async use(cb, identity, count = 1) {

        const player   = identity.getAltPlayer();
        const toUpdate = [];

        identity.status.hunger += 0.35;

        if(identity.status.hunger > 1)
          identity.status.hunger = 1;

        if(identity.status.thirst > 1)
          identity.status.thirst = 1;

        this.count--;

        if(this.count === 0) {

          const container = await horizon.modules.container.findOne({_id: this.container});

          if(container) {
            container.removeItem(this);
            toUpdate.push(container._idStr);
          }

          this.delete();

        } else {
          
          this.save();

          toUpdate.push(this._idStr);

        }
        
        identity.cache(true);

        horizon.updateCache(player, ...toUpdate);

        cb();
      }

    }
  
    HZItemHotdog
      .inheritFields(HZItem)
      .inheritActions(HZItem)
      .field('_type', {default: 'ItemHotdog'})
      .field('image', {default: 'hotdog'})
      .field('shape', {default: {"grid":[{"x":-6.0000000000001705,"y":-11.999999999999986}],"offset":{"x":6.0000000000001705,"y":11.999999999999986}}})
      .field('name',  {default: 'itemhotdog'})
      .field('label', {default: 'Hot-Dog'})
    ;
  
    return HZItemHotdog;
  
  }