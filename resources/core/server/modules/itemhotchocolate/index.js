module.exports = (horizon, config) => {
     
    const HZItem = horizon.require('item');
  
    class HZItemHotchocolate extends HZItem {
  
    }
  
    HZItemHotchocolate
      .inheritFields(HZItem)
      .inheritActions(HZItem)
      .field('_type', {default: 'ItemHotchocolate'})
      .field('image', {default: 'hotchocolate'})
      .field('shape', {default: {"grid":[{"x":-13.999999999999886,"y":-4.144314868804486}],"offset":{"x":13.999999999999886,"y":4.144314868804486}}})
      .field('name',  {default: 'itemhotchocolate'})
      .field('label', {default: 'Chocolat chaud'})
    ;
  
    return HZItemHotchocolate;
  
  }