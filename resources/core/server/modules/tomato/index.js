'use strict';

module.exports = function(horizon, config) {

	const Utils  = horizon.require('utils');
	const HZItem = horizon.require('item');

	class HZTomato extends HZItem {

		constructor() {
			
			const actions = [];

			for(let i=0; i<this._actions.length; i++) {
				console.log(this._actions[i].name, this.canDo(this._actions[i].name), this._actions[i].noArgs);
			}

		}

		use(cb, identity, count = 1) {
			identity.status.hunger += 0.1;
			identity.status.thirst += 0.1;
			identity.cache(true);
			cb();
		}

	}

	HZTomato
		.inheritFields(HZItem)
		.inheritActions(HZItem)
		.field('_type',     {default: 'Tomato'})
		.field('name',      {default: 'Tomato'})
    .field('lifetime',  {default: 7 * 24 * 3600})
    .field('image',     {default: 'tomato'})
	;

	return HZTomato;
};
