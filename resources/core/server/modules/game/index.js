'use strict';

const alt          = require('alt');
const EventEmitter = require('events');

module.exports = function(horizon, config) {

	class HZGame extends EventEmitter {

		constructor() {

      super();

      this.bindEvents();

    }
    
    bindEvents() {

    }

	}

  const instance = new HZGame();

	return instance;

};