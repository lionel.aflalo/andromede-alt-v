module.exports = (horizon, config) => {
     
    const HZItem = horizon.require('item');
  
    class HZItemBiafine extends HZItem {
  
    }
  
    HZItemBiafine
      .inheritFields(HZItem)
      .inheritActions(HZItem)
      .field('_type', {default: 'ItemBiafine'})
      .field('image', {default: 'biafine'})
      .field('shape', {default: {"grid":[{"x":-30,"y":-4.653061224489704}],"offset":{"x":30,"y":4.653061224489704}}})
      .field('name',  {default: 'itembiafine'})
      .field('label', {default: 'Biafine'})
    ;
  
    return HZItemBiafine;
  
  }