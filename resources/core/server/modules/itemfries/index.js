module.exports = (horizon, config) => {
     
    const HZItem = horizon.require('item');
  
    class HZItemFries extends HZItem {
  
      async use(cb, identity, count = 1) {

        const player   = identity.getAltPlayer();
        const toUpdate = [];

        identity.status.hunger += 0.2;

        if(identity.status.hunger > 1)
          identity.status.hunger = 1;

        if(identity.status.thirst > 1)
          identity.status.thirst = 1;

        this.count--;

        if(this.count === 0) {

          const container = await horizon.modules.container.findOne({_id: this.container});

          if(container) {
            container.removeItem(this);
            toUpdate.push(container._idStr);
          }

          this.delete();

        } else {
          
          this.save();

          toUpdate.push(this._idStr);

        }
        
        identity.cache(true);

        horizon.updateCache(player, ...toUpdate);

        cb();
      }

    }
  
    HZItemFries
      .inheritFields(HZItem)
      .inheritActions(HZItem)
      .field('_type', {default: 'ItemFries'})
      .field('image', {default: 'fries'})
      .field('shape', {default: {"grid":[{"x":-6,"y":-19.1879795396419}],"offset":{"x":6,"y":19.1879795396419}}})
      .field('name',  {default: 'itemfries'})
      .field('label', {default: 'Frites'})
    ;
  
    return HZItemFries;
  
  }