const alt = require('alt');

module.exports = (horizon, config) => {

  const $              = horizon.$;
  const HZVehicle      = horizon.require('vehicle');
  const HZIdentity     = horizon.require('identity');
	const HZBankAccount  = horizon.require('bank/account');
  const HZItemBankCard = horizon.require('itembankcard');
  const HZItemKey      = horizon.require('itemkey');

  class HZCarDealer {

  }

  const instance = new HZCarDealer;

  instance.__init = function(cb) {

    alt.onClient('hz:cardealer:spawnVehicle', async (player, data, paymentType = 'cash', paymentData = {}) => {
      
      const concession = config.concessions.find(e => e.name === data.concession);

      if(!concession)
        return;

      const infos = concession.vehicles.find(e => $.utils.joaat(e.name) === data.model);

      if(!infos)
        return;

      const total    = infos.price;
      const identity = await HZIdentity.findOne({_id: HZIdentity.id(player.getSyncedMeta('identity'))});

      if(!identity)
        return;

      const giveKey = async (vehicle) => {

        const key = new HZItemKey({targets: [{type: 'vehicle', model: vehicle.model, plate: vehicle.numberPlateText}]});
        const result = identity.getFreeSpace(key);
  
        if(result) {
  
          key.x = result.x;
          key.y = result.y;
  
          result.container.items.push(key);
  
          result.container.cache(true);
          await key.save();
  
          horizon.updateCache(player, result.container._idStr);
  
          result.container.save();
          
        }

      }

      if(paymentType === 'cash') {

        let   count    = total;
        const results  = identity.queryItem({_type: 'ItemBanknote'}).sort((a, b) => a.count - b.count);
        const toDelete = [];

        for(let i=0; i<results.length; i++) {

          const result = results[i];
          let   rCount = 0;

          while((count > 0) && (rCount <= result.item.count)) {
            count--;
            rCount++;
          }
          
          if(rCount > 0)
            toDelete.push({...result, count: rCount});

        }

        if(count === 0) {
          
          for(let i=0; i<toDelete.length; i++) {

            const td       = toDelete[i];
            td.item.count -= td.count;

            if(td.item.count === 0) {
              
              await td.container.removeItem(td.item);
              await td.item.delete();
            
              horizon.updateCache(player, td.container._idStr);

            } else {

              await td.item.save();
              horizon.updateCache(player, td.item._idStr);

            }

          }

          const hzVehicle = await HZVehicle.create(data.model, data.position);

          hzVehicle.altVehicle.customPrimaryColor   = {r: data.pr, g: data.pg, b: data.pb};
          hzVehicle.altVehicle.customSecondaryColor = {r: data.sr, g: data.sg, b: data.sb};
        
          await giveKey(hzVehicle.altVehicle);
          await $.utils.delay(400);
        
          alt.emitClient(player, 'hz:task:warpIntoVehicle', hzVehicle.altVehicle);
          alt.emitClient(player, 'hz:notification:create', '<span style="color:green">Vous avez acheté un véhicule');

        } else {
          
          alt.emitClient(player, 'hz:notification:create', '<span style="color:red">Pas assez de cash</span>');

        }

      } else if(paymentType === 'card') {

        const card = await HZItemBankCard.findOne({number: paymentData.number, token: paymentData.token});

        if(card === null) {
          alt.emitClient(player, 'hz:notification:create', '<span style="color:red">Carte non authentifiée</span>');
          return;
        }

        const account = await HZBankAccount.findOne({_id: card.account});

        if(!account) {
          alt.emitClient(player, 'hz:notification:create', '<span style="color:red">Compte introuvable</span>');
          return;
        }

        if(account.money < total) {
          alt.emitClient(player, 'hz:notification:create', '<span style="color:red">Fonds insuffisants</span>');
          return;
        }

        account.money -= total;

        await account.save();

        const hzVehicle = await HZVehicle.create(data.model, data.position);

        hzVehicle.altVehicle.customPrimaryColor   = {r: data.pr, g: data.pg, b: data.pb};
        hzVehicle.altVehicle.customSecondaryColor = {r: data.sr, g: data.sg, b: data.sb};
      
        await giveKey(hzVehicle.altVehicle);
        await $.utils.delay(400);
      
        alt.emitClient(player, 'hz:task:warpIntoVehicle', hzVehicle.altVehicle);
        alt.emitClient(player, 'hz:notification:create', '<span style="color:green">Vous avez acheté un véhicule</span>');

      }

    });

    cb();

  }

  return instance;

}