module.exports = (horizon, config) => {
     
    const HZItem = horizon.require('item');
  
    class HZItemCondom extends HZItem {
  
    }
  
    HZItemCondom
      .inheritFields(HZItem)
      .inheritActions(HZItem)
      .field('_type', {default: 'ItemCondom'})
      .field('image', {default: 'condom'})
      .field('shape', {default: {"grid":[{"x":-15,"y":-33.26426799007436},{"x":-15,"y":66.73573200992564}],"offset":{"x":15,"y":33.26426799007436}}})
      .field('name',  {default: 'itemcondom'})
      .field('label', {default: 'Préservatif'})
    ;
  
    return HZItemCondom;
  
  }