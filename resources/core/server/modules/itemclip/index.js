module.exports = (horizon, config) => {
     
    const HZItem = horizon.require('item');
  
    class HZItemClip extends HZItem {
  
    }
  
    HZItemClip
      .inheritFields(HZItem)
      .inheritActions(HZItem)
      .field('_type', {default: 'ItemClip'})
      .field('image', {default: 'clip'})
      .field('shape', {default: {"grid":[{"x":-14.628407518372569,"y":-32.87128466464583},{"x":-14.628407518372569,"y":67.12871533535417}],"offset":{"x":14.628407518372569,"y":32.87128466464583}}})
      .field('name',  {default: 'itemclip'})
      .field('label', {default: 'Chargeur'})
    ;
  
    return HZItemClip;
  
  }