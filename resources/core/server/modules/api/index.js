const fs      = require('fs');
const axios   = require('axios');
const gm      = require('gm');
const express = require('express');

module.exports = (horizon, config) => {

  class HZAPI {

    constructor() {

      this.router = express.Router();

      this.router.use(this.allowCrossDomain.bind(this));

      this.router.use('/atm/', require(__dirname + '/routers/atm/index.js')(horizon, horizon.require('bank')));

      horizon.app.get('/api/img/load/:url', async (req, res) => {

        const aRes = await axios({
          method      : 'get',
          url         : req.params.url,
          responseType: 'stream'
        });

        if(aRes.status !== 200) {
          res.end('');
          return;
        }

        gm(aRes.data).trim().stream().pipe(res);
        
      });

      horizon.app.post('/api/item/save', async (req, res) => {

        const base64Data = req.body.img.replace(/^data:image\/png;base64,/, "");
        const buffer     = Buffer.from(base64Data, 'base64');

        fs.writeFile(__dirname + '../../../data/img/items/' + req.body.name + '.png', buffer, (err) => {
          console.log(err);
        });

        fs.writeFile(__dirname + '../../../data/img/items/' + req.body.name + '.json', JSON.stringify({
          grid   : req.body.grid,
          offset : req.body.offset
        }) , (err) => {
          console.log(err);
        });

        fs.writeFile(__dirname + '../../../www/itemtest/data/test.png', buffer, (err) => {
          console.log(err);
        });

        fs.writeFile(__dirname + '../../../www/itemtest/data/test.json', JSON.stringify({
          grid   : req.body.grid,
          offset : req.body.offset
        }) , (err) => {
          console.log(err);
        });

        res.end('');

      });

    }

    allowCrossDomain(req, res, next) {

      res.header('Access-Control-Allow-Origin',  '*');
      res.header('Access-Control-Allow-Methods', 'GET,PUT,POST,DELETE');
      res.header('Access-Control-Allow-Headers', 'Content-Type, Authorization');

      if (req.method === 'OPTIONS')
        res.sendStatus(200);
      else
        next();

    };

  }

  const instance = new HZAPI();

  instance.__init = function(cb) {

    horizon.app.use('/api/', instance.router);

    cb();
  }

  return instance;

}