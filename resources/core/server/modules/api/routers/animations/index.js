'use strict';

module.exports = function(horizon, coreAnimationModule) {

	const express      = require('express');
    const HZLog = horizon.require('log');

	class AnimationRouter {

		constructor() {
		  	this.router_ = express.Router();

			this.router_.param('id', function (req, res, next, id) {
			  req.param._id = id;
			  next();
			});

			this.router_.get('/', this.getList_.bind(this));
			this.router_.post('/', this.add_.bind(this));
			this.router_.delete('/:id', this.remove_.bind(this));
			this.router_.put('/:id', this.update_.bind(this));

			return this.router_;
		}

		getList_(req, res, next) {
            coreAnimationModule.find({}).then((list) => {
            	if (list) {
               		res.json(list.map((item) => { return item.toObject() }));
            	} else {
                    res.json({error: "Can't get list"});
            	}
            });
         }

        add_(req, res, next) {
	        if (req.body && req.body.name) {
                req.body._type = "Animation";
                coreAnimationModule.findOrCreate({name: req.body.name}, req.body).then((entity, exist) => {
                if (!exist)
                    HZLog(HZLog.ADDANIMATION, req.param.user, req.body);
                res.json(exist ? {error: "Animation already exist"} : {result: "ok"})
                });
	        }
        }

        remove_(req, res, next) {
	        if (req.param._id) {
                coreAnimationModule.findOne({_id: coreAnimationModule.id(req.param._id)}).then((entity) => {
                    if (entity) {
                        HZLog(HZLog.REMOVEANIMATION, req.param.user, entity);
                    	entity.delete();
                    	res.json({result: "ok"});
                    }else {
                    	res.json({error: "Animation do not exist"});
                    }
                });
	        }
        }

        update_(req, res, next) {
            if (req.body && req.param._id) {
                coreAnimationModule.findOne({_id: coreAnimationModule.id(req.param._id)}).then((entity) => {
                    if (entity) {
                        HZLog(HZLog.UPDATEANIMATION, req.param.user, entity, req.body);
                        entity.name = req.body.name;
                        entity.categorie = req.body.categorie;
                        entity.dict = req.body.dict;
                        entity.anim = req.body.anim;
                        entity.save();
                        res.json({result: "ok"});
                    } else {
                    	res.json({error: "Animation do not exist"});
                    }
                });
            }
        }
	}

	return new AnimationRouter();

};
