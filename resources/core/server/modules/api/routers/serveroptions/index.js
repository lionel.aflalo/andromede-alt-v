'use strict';

module.exports = function(horizon, coreServerOptions) {

  const express = require('express');
  const HZLog = horizon.require('log');

  class ServerOptionsRouter {

    constructor() {
      this.router_ = express.Router();

      this.router_.get('/', this.get_.bind(this));
      this.router_.put('/', this.update_.bind(this));

      return this.router_;
    }

    get_(req, res, next) {
      coreServerOptions.findOne({
        config: 'default'
      }).then((list) => {
        if (list) {
          res.json(list.toObject());
        } else {
          res.json({
            error: "Can't get server option"
          });
        }
      });
    }

    update_(req, res, next) {
      if (req.body) {
        coreServerOptions.findOrCreate({
          config: 'default'
        }, req.body).then((entity) => {
          if (entity) {
            HZLog(HZLog.UPDATESERVEROPTIONS, req.param.user, req.body);

            if (entity.curWeather != req.body.curWeather) {
              horizon.triggerClientEvent('horizon:weather:update', req.body.curWeather);
              entity.curWeather = req.body.curWeather;
            }

            if (entity.dayDuration != req.body.dayDuration) {
              horizon.triggerClientEvent('horizon:timecycle:update', parseInt(req.body.dayDuration, 10));
              entity.dayDuration = req.body.dayDuration;
            }

            if (entity.isMaintenance != req.body.isMaintenance) {
              entity.isMaintenance = req.body.isMaintenance;
              horizon.updateIsServerInMaintenance(entity.isMaintenance);
            }

            horizon.updateServerOptions(entity);

            entity.save();
            res.json({
              result: "ok"
            });
          }
        });
      }
    }


  }

  return new ServerOptionsRouter();

};
