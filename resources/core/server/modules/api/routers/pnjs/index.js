'use strict';

module.exports = function(horizon, corePnjModule) {

  const express = require('express');
  const HZIdentity = horizon.require('identity');
  const HZLog = horizon.require('log');

  class PnjRouter {

    constructor() {
      this.router_ = express.Router();

      this.router_.param('id', function(req, res, next, id) {
        req.param._id = id;
        next();
      });

      this.router_.get('/', this.getList_.bind(this));
      this.router_.post('/', this.add_.bind(this));
      this.router_.delete('/:id', this.remove_.bind(this));
      this.router_.put('/:id', this.update_.bind(this));

      return this.router_;
    }

    getList_(req, res, next) {
      corePnjModule.find({}).then((list) => {
        if (list) {
          res.json(list.map((item) => {
            return item.toObject()
          }));
        } else {
          res.json({
            error: "Can't get list"
          });
        }
      });
    }

    add_(req, res, next) {
      if (req.body && req.body.identity) {
        req.body._type = "Pnj";
        HZIdentity.findOrCreate({
          _id: corePnjModule.id(req.body.identity)
        }).then((identity, exist) => {
          req.body.identity = identity;
          corePnjModule.findOrCreate({
            identity: req.body.identity
          }, req.body).then((entity, exist) => {
            if (!exist)
              HZLog(HZLog.ADDPNJ, req.param.user, req.body);
            res.json(exist ? {
              error: "Pnj already exist"
            } : {
              result: "ok"
            })
            this.refreshPnj();
          });
        })
      }
    }

    remove_(req, res, next) {
      if (req.param._id) {
        corePnjModule.findOne({
          _id: corePnjModule.id(req.param._id)
        }).then((entity) => {
          if (entity) {
            HZLog(HZLog.REMOVEPNJ, req.param.user, entity);
            entity.delete();
            res.json({
              result: "ok"
            });
            this.refreshPnj();
          } else {
            res.json({
              error: "Pnj do not exist"
            });
          }
        });
      }
    }

    update_(req, res, next) {
      if (req.body && req.param._id) {
        corePnjModule.findOne({
          _id: corePnjModule.id(req.param._id)
        }).then((entity) => {
          if (entity) {
            HZIdentity.findOrCreate({
              _id: corePnjModule.id(req.body.identity)
            }).then((identity, exist) => {
              HZLog(HZLog.UPDATEPNJ, req.param.user, entity, req.body);
              entity.identity = identity
              entity.save();
              res.json({
                result: "ok"
              });
              this.refreshPnj();
            });
          } else {
            res.json({
              error: "Pnj do not exist"
            });
          }
        });
      }
    }

    refreshPnj() {
      setTimeout(() => corePnjModule.find({}).then(function(result) {
        horizon.triggerClientEvent('horizon:host:refresh:pnj', result.map((item) => {
          return item.toObject()
        }));
      }), 2000);
    }
  }

  return new PnjRouter();

};
