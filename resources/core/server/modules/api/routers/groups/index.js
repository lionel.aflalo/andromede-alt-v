'use strict';

module.exports = function(horizon, coreGroupModule) {

  const express = require('express');
  const HZLog = horizon.require('log');

  class GroupRouter {

    constructor() {
      this.router_ = express.Router();

      this.router_.param('id', function(req, res, next, id) {
        req.param._id = id;
        next();
      });

      this.router_.get('/', this.getList_.bind(this));
      this.router_.post('/', this.add_.bind(this));
      this.router_.delete('/:id', this.remove_.bind(this));
      this.router_.put('/:id', this.update_.bind(this));

      return this.router_;
    }

    getList_(req, res, next) {
      coreGroupModule.find({}).then((list) => {
        if (list) {
          res.json(list.map((item) => {
            return item.toObject()
          }));
        } else {
          res.json({
            error: "Can't get list"
          });
        }
      });
    }

    add_(req, res, next) {
      if (req.body && req.body.name) {
        req.body._type = "Group";
        coreGroupModule.findOrCreate({
          name: req.body.name,
          type: req.body.type
        }, req.body).then((entity, exist) => {
          if (!exist)
            HZLog(HZLog.ADDGROUP, req.param.user, new Date(), req.body);
          res.json(exist ? {
            error: "Group already exist"
          } : {
            result: "ok"
          })
        });
      }
    }

    remove_(req, res, next) {
      if (req.param._id) {
        coreGroupModule.findOne({
          _id: coreGroupModule.id(req.param._id)
        }).then((entity) => {
          if (entity) {
            HZLog(HZLog.REMOVEGROUP, req.param.user, new Date(), entity);
            entity.delete();
            res.json({
              result: "ok"
            });
          } else {
            res.json({
              error: "Group do not exist"
            });
          }
        });
      }
    }

    update_(req, res, next) {
      if (req.body && req.param._id) {
        coreGroupModule.findOne({
          _id: coreGroupModule.id(req.param._id)
        }).then((entity) => {
          if (entity) {
            HZLog(HZLog.UPDATEGROUP, req.param.user, new Date(), entity, req.body);
            entity.name = req.body.name;
            entity.roles = req.body.roles;
            entity.save();
            res.json({
              result: "ok"
            });
          } else {
            res.json({
              error: "Group do not exist"
            });
          }
        });
      }
    }
  }

  return new GroupRouter();

};
