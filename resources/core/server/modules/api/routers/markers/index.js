'use strict';

module.exports = function(horizon, coreMarkerModule) {

  const express = require('express');
  const HZPosition = horizon.require('position');
  const HZLog = horizon.require('log');

  class MarkerRouter {

    constructor() {
      this.router_ = express.Router();

      this.router_.param('id', function(req, res, next, id) {
        req.param._id = id;
        next();
      });

      this.router_.get('/', this.getList_.bind(this));
      this.router_.post('/', this.add_.bind(this));
      this.router_.delete('/:id', this.remove_.bind(this));
      this.router_.put('/:id', this.update_.bind(this));

      return this.router_;
    }

    getList_(req, res, next) {
      coreMarkerModule.find({}).then((list) => {
        if (list) {
          res.json(list.map((item) => {
            return item.toObject()
          }));
        } else {
          res.json({
            error: "Can't get list"
          });
        }
      });
    }

    add_(req, res, next) {
      if (req.body && req.body.name) {
        req.body._type = "Marker";
        HZPosition.findOrCreate({
          _id: coreMarkerModule.id(req.body.position)
        }).then((position, exist) => {
          req.body.position = position;
          if (!exist)
            HZLog(HZLog.ADDMARKER, req.param.user, req.body);
          coreMarkerModule.findOrCreate({
            name: req.body.name,
            position: position
          }, req.body).then((entity, exist) => {
            res.json(exist ? {
              error: "Marker already exist"
            } : {
              result: "ok"
            })
            horizon.refreshMarkerList();
          });
        })
      }
    }

    remove_(req, res, next) {
      if (req.param._id) {
        coreMarkerModule.findOne({
          _id: coreMarkerModule.id(req.param._id)
        }).then((entity) => {
          if (entity) {
            HZLog(HZLog.REMOVEMARKER, req.param.user, entity);
            entity.delete();
            res.json({
              result: "ok"
            });
            horizon.refreshMarkerList();
          } else {
            res.json({
              error: "Marker do not exist"
            });
          }
        });
      }
    }

    update_(req, res, next) {
      if (req.body && req.param._id) {
        coreMarkerModule.findOne({
          _id: coreMarkerModule.id(req.param._id)
        }).then((entity) => {
          if (entity) {
            HZPosition.findOrCreate({
              _id: coreMarkerModule.id(req.body.position)
            }).then((position, exist) => {
              HZLog(HZLog.UPDATEMARKER, req.param.user, entity, req.body);
              entity.name = req.body.name;
              entity.sprite = req.body.sprite;
              entity.scale = req.body.scale;
              entity.rgb = req.body.rgb;
              entity.a = req.body.a;
              entity.group = req.body.group;
              entity.position = position;
              entity.action = req.body.action;
              entity.description = req.body.description;
              entity.customData = JSON.parse(req.body.customData);
              entity.visibleInVehicle = req.body.visibleInVehicle;
              entity.visibleOnFoot = req.body.visibleOnFoot;
              entity.save();
              res.json({
                result: "ok"
              });
              horizon.refreshMarkerList();
            });
          } else {
            res.json({
              error: "Marker do not exist"
            });
          }
        });
      }
    }
  }

  return new MarkerRouter();

};
