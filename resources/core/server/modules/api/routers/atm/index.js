'use strict';

const async  = require('async');
const crypto = require('crypto');
const atob   = require('atob');

module.exports = function(horizon, HZBank) {

  const express              = require('express');
  const HZContainer         = horizon.require('container');
  const HZBankUser          = horizon.require('bank/user');
  const HZStandardBankUser  = horizon.require('bank/user/standard');
  const HZProBankUser       = horizon.require('bank/user/pro');
  const HZBankAccount       = horizon.require('bank/account');
  const HZBankTransaction   = horizon.require('bank/transaction');
  const HZBankAutoWire      = horizon.require('bank/autowire');
  const HZBankLoan          = horizon.require('bank/loan');
  const HZBankLoanFormulas  = horizon.require('bank/loan/formulas');
  const HZBankAccountLimits = horizon.require('bank/account/limits');
  const HZItemBankCard      = horizon.require('itembankcard'); 
  const HZIdentity          = horizon.require('identity');
  // const HZDocument          = horizon.require('document');

  class ATMRouter {

    constructor() {

      this.router = express.Router();

      this.router.param('id', function(req, res, next, id) {
        req.param._id = id;
        next();
      });

      // GET
      this.router.get('/user.accounts/:id',       this.getUserAccounts.bind(this));
      this.router.get('/transactions/:accountid', this.getTransactions.bind(this));

			// POST
      this.router.post('/auth',                       this.authUserByCard       .bind(this));
      // this.router.post('/print/accountstatement/:id', this.printAccountStatement.bind(this));
      this.router.post('/withdraw/:id',               this.withdraw             .bind(this));
      this.router.post('/deposit/:id',                this.deposit              .bind(this));
      this.router.post('/changepin',                  this.changePin            .bind(this));
      this.router.post('/transfer/:sender/:receiver', this.transfer             .bind(this));

      return this.router;
    }

    authenticate(req) {

      return new Promise((resolve, reject) => {

        if(!req.headers.authorization) {
          resolve(null);
          return;
        }

        let credentials = atob(req.headers.authorization.split(' ')[1]).split(':');

        if(credentials[1] === null)
          resolve(null);

        HZItemBankCard
          .findOne({
            number: credentials[0],
            token : credentials[1],
          }).then(card => {
            
            if(card) {
              
              if(card.token === credentials[1]) {

              	HZBankUser
              		.findOne({_id: card.user})
              		.then(user => {

              			if(user)
              				resolve(user);
              			else
              				resolve(null);

              		})
              	;

              } else resolve(null);

            } else resolve(null);

          })
        ;

      });

    }

    // GET
    getUserAccounts(req, res, next) {

      this
        .authenticate(req)
        .then(user => {

          if(user) {

            const idStr = req.params.id || '';

            if(idStr.length !== 12 && idStr.length !== 24) {
              res.json({error: 'Wrong user id'});
              return;
            }

            HZBankUser
              .findOne({_id: HZBankUser.id(idStr), active: true})
              .then(user => {
                
                if(user) {
                  
                  HZBankAccount
                    .find({_id: {$in: user.accounts}, active: true})
                    .then(accounts => {
                      res.json(accounts.map(e => e.toObject()));
                    })
                  ;

                } else res.json({error: 'Can\'t get user'});

              })
            ;

          } else res.json({error: 'Authentification required'});

        })
      ;
    }

    getTransactions(req, res, next) {

      this
        .authenticate(req)
        .then(user => {

          if(user) {

            const idStr = req.params.accountid || '';

            if(idStr.length !== 12 && idStr.length !== 24) {
              res.json({error: 'Wrong account id'});
              return;
            }

            HZBankTransaction
              .find({
                $or: [
                  {sender  : HZBankAccount.id(idStr)},
                  {receiver: HZBankAccount.id(idStr)},
                ]
              })
              .then(transactions => {
                res.json(transactions.map(e => e.toObject()));
              })
            ;
          } else res.json({error: 'Authentification required'});

        })
      ;
    }
    
    // POST
    authUserByCard(req, res, next) {

      if(req.body && req.body.number && req.body.pinCode) {
        
      	HZItemBankCard
      		.findOne({
      			number : req.body.number,
      			pinCode: req.body.pinCode
      		})
      		.then(card => {

      			if(card !== null) {

							card.token = crypto.randomBytes(16).toString('hex');

              card
                .save()
                .then(() => {

                  HZBankUser
                    .findOne({_id: card.user})
                    .then(user => {

                      if(user !== null) {

                        res.json({
                          card: card.toObject(),
                          user: user.toObject(),
                        });

                      } else res.json({error: 'User not found'});

                    })
                  ;
                })
              ;

      			} else res.json({error: 'Bad card infos'});

      		})
      	;

      } else res.json({error: 'Missing fields'});
    }

    transfer(req, res, next) {

      this
        .authenticate(req)
        .then(user => {

          if(user) {

            if(
              typeof req.body           !== 'undefined' &&
              typeof req.body.amount    !== 'undefined'
            ) {

              const initiator = user._id;
              const sender    = parseInt(req.params.sender,   10);
              const receiver  = parseInt(req.params.receiver, 10);

              HZBankAccount
                .findOne({number: sender})
                .then(senderAccount => {

                  if(senderAccount === null) {
                    res.json({error: 'Sender not found'});
                    return;
                  }

                  HZBankAccount
                    .findOne({number: receiver})
                    .then(receiverAccount => {

                      if(receiverAccount === null) {
                        res.json({error: 'Receiver not found'});
                        return;
                      }

                      senderAccount
                        .tryTransfer(receiverAccount, req.body.amount, 'manual wire from #' + sender.toString().padStart(8, '0') + ' to #' + receiver.toString().padStart(8, '0'), initiator)
                        .then(success => {

                          if(success)
                            res.json({result: 'ok'});
                          else
                            res.json({error: 'Can\'t make transfer'})

                        })
                      ;

                    })
                  ;

                })
              ;


            } else res.json({error: 'Missing fields'});
            
          } else res.json({error: 'Authentification required'});

        })
      ;
    }

    printAccountStatement(req, res, next) {

      this
        .authenticate(req)
        .then(user => {

          if(user) {

            const idStr = req.params.id || '';

            if(idStr.length !== 12 && idStr.length !== 24) {
              res.json({error: 'Wrong account id'});
              return;
            }

            const _id   = HZBankAccount.id(idStr);
            let   found = false;

            for(let i=0; i<user.accounts.length; i++) {
              if(user.accounts[i].equals(_id)) {
                found = true;
                break;
              }
            }

            if(!found) {
              res.json({error: 'Can\'t get account'});
              return;
            }

            HZBankAccount
              .findOne({_id})
              .then(account => {

                if(account === null) {
                  res.json({error: 'Can\'t get account'});
                  return;
                }

                HZIdentity
                  .findOne({residentNumber: user.residentNumber})
                  .then(identity => {

                    if(identity === null) {
                      res.json({error: 'Can\'t get identity'});
                      return;
                    }

                    HZBankTransaction
                      .find({
                        $or: [
                          {sender  : HZBankAccount.id(idStr)},
                          {receiver: HZBankAccount.id(idStr)},
                        ]
                      })
                      .then(transactions => {

                        transactions = transactions.slice(0);
                        transactions.sort((a,b) => b.timestamp - a.timestamp);
                        transactions = transactions.slice(0, 10);
                        transactions.sort((a,b) => a.timestamp - b.timestamp)

                        const transactionItems = transactions.map(e => {

                          const date       = new Date(e.timestamp * 1000);
                          const dateStr    = date.getDate() + '/' + (date.getMonth() + 1) + ' ' + (date.getHours() + 1).toString().padStart(2, '0') + ':' + (date.getMinutes() + 1).toString().padStart(2, '0');
                          const isSender   = _id.equals(e.sender);
                          const isReceiver = _id.equals(e.receiver);
                          const isNegative = (isSender && e.amount >= 0) || (isReceiver && e.amount < 0);

                          return {
                            date  : dateStr,
                            object: e.object,
                            amount: isNegative ? e.amount * -1 : e.amount
                          };

                        });

                        const doc = HZDocument.fromTemplate('bank_account_statement', 'Extrait de compte', account, transactionItems);
                        doc.count = 1;

                        const container = identity.getFirstAddonContainer();

                        if(container === null) {
                          res.json({error: 'No available container'});
                          return;
                        }

                        container.addItemInstance(doc);

                        identity
                          .save()
                          .then(() => {
                            res.json({result: 'ok'});
                          })
                        ;

                      })
                    ;

                  })
                ;

              })
            ;
          

          } else res.json({error: 'Authentification required'});

        })
      ; 

    }

    withdraw(req, res, next) {

      this
        .authenticate(req)
        .then(user => {

          if(user) {

            if(req.body && req.body.amount) {

              const idStr = req.params.id || '';

              if(idStr.length !== 12 && idStr.length !== 24) {
                res.json({error: 'Wrong account id'});
                return;
              }

              const _id   = HZBankAccount.id(idStr);
              let   found = false;

              for(let i=0; i<user.accounts.length; i++) {
                if(user.accounts[i].equals(_id)) {
                  found = true;
                  break;
                }
              }

              if(!found) {
                res.json({error: 'Can\'t get account'});
                return;
              }

              HZBankAccount
                .findOne({_id})
                .then(account => {

                  if(account === null) {
                    res.json({error: 'Can\'t get account'});
                    return;
                  }

                  HZIdentity
                    .findOne({residentNumber: user.residentNumber})
                    .then(identity => {

                      if(identity === null) {
                        res.json({error: 'Can\'t get identity'});
                        return;
                      }

                      account
                        .tryWithdraw(req.body.amount, identity, user._id)
                        .then(success => {

                          if(success)
                            res.json({result: 'ok'})
                          else
                            res.json({error: 'Can\' withdraw'})
                          
                        })
                      ;

                    })
                  ;

                })
              ;

            } else res.json({error: 'Missing fields'});

          } else res.json({error: 'Authentification required'});

        })
      ; 

    }

    deposit(req, res, next) {

      this
        .authenticate(req)
        .then(user => {

          if(user) {

            if(req.body && req.body.amount) {

              const idStr = req.params.id || '';

              if(idStr.length !== 12 && idStr.length !== 24) {
                res.json({error: 'Wrong account id'});
                return;
              }

              const _id   = HZBankAccount.id(idStr);
              let   found = false;

              for(let i=0; i<user.accounts.length; i++) {
                if(user.accounts[i].equals(_id)) {
                  found = true;
                  break;
                }
              }

              if(!found) {
                res.json({error: 'Can\'t get account'});
                return;
              }

              HZBankAccount
                .findOne({_id})
                .then(account => {

                  if(account === null) {
                    res.json({error: 'Can\'t get account'});
                    return;
                  }

                  HZIdentity
                    .findOne({residentNumber: user.residentNumber})
                    .then(async identity => {

                      if(identity === null) {
                        res.json({error: 'Can\'t get identity'});
                        return;
                      }

                      const player   = identity.getAltPlayer();
                      let   count    = req.body.amount;
                      const results  = identity.queryItem({_type: 'ItemBanknote'}).sort((a, b) => a.count - b.count);
                      const toDelete = [];

                      for(let i=0; i<results.length; i++) {

                        const result = results[i];
                        let   rCount = 0;

                        while((count > 0) && (rCount < result.item.count)) {
                          count--;
                          rCount++;
                        }
                        
                        if(rCount > 0)
                          toDelete.push({...result, count: rCount});

                      }

                      if(count === 0) {
                        
                        for(let i=0; i<toDelete.length; i++) {

                          const td       = toDelete[i];
                          td.item.count -= td.count;

                          if(td.item.count === 0) {
                            
                            await td.container.removeItem(td.item);
                            await td.item.delete();
                          
                            horizon.updateCache(player, td.container._idStr);

                          } else {

                            await td.item.save();
                            horizon.updateCache(player, td.item._idStr);

                          }

                        }

                        account.money += req.body.amount;

                        await account.save();

                        res.json({result: 'ok'});

                      } else res.json({error: 'Can\' deposit'})

                    })
                  ;

                })
              ;

            } else res.json({error: 'Missing fields'});

          } else res.json({error: 'Authentification required'});

        })
      ; 

    }

    async changePin(req, res, next) {

      if(req.body && req.body.number && req.body.pinCode) {
        
        const card   = await HZItemBankCard.findOne({number: req.body.number});
        card.pinCode = req.body.pinCode;
        
        await card.save();

        res.json({result: 'ok'})

      } else res.json({error: 'Missing fields'});

    }

  }

  return new ATMRouter();

};
