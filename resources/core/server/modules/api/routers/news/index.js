'use strict';

module.exports = function(horizon, coreNewModule) {

	const express      = require('express');
    const HZLog = horizon.require('log');

	class NewRouter {

		constructor() {
		  	this.router_ = express.Router();

			this.router_.param('id', function (req, res, next, id) {
			  req.param._id = id;
			  next();
			});

			this.router_.get('/', this.getList_.bind(this));
			this.router_.post('/', this.add_.bind(this));
			this.router_.delete('/:id', this.remove_.bind(this));
			this.router_.put('/:id', this.update_.bind(this));

			return this.router_;
		}

		getList_(req, res, next) {
            coreNewModule.find({}).then((list) => {
            	if (list) {
               		res.json(list.map((item) => { return item.toObject() }));
            	} else {
                    res.json({error: "Can't get list"});
            	}
            });
         }

        add_(req, res, next) {
	        if (req.body && req.body.title) {
                req.body._type = "New";
                coreNewModule.findOrCreate({title: req.body.title}, req.body).then((entity, exist) => {
                if (!exist)
                    HZLog(HZLog.ADDNEW, req.param.user, req.body);
                res.json(exist ? {error: "New already exist"} : {result: "ok"})
                });
	        }
        }

        remove_(req, res, next) {
	        if (req.param._id) {
                coreNewModule.findOne({_id: coreNewModule.id(req.param._id)}).then((entity) => {
                    if (entity) {
                        HZLog(HZLog.REMOVENEW, req.param.user, entity);
                    	entity.delete();
                    	res.json({result: "ok"});
                    }else {
                    	res.json({error: "New do not exist"});
                    }
                });
	        }
        }

        update_(req, res, next) {
            if (req.body && req.param._id) {
                coreNewModule.findOne({_id: coreNewModule.id(req.param._id)}).then((entity) => {
                    if (entity) {
                        HZLog(HZLog.UPDATENEW, req.param.user, entity, req.body);
                        entity.title = req.body.title;
                        entity.description = req.body.description;
                        entity.header = req.body.header;
                        entity.save();
                        res.json({result: "ok"});
                    } else {
                    	res.json({error: "New do not exist"});
                    }
                });
            }
        }
	}

	return new NewRouter();

};
