'use strict';

module.exports = function(horizon, HZArea) {

  const express = require('express');
  const HZLog  = horizon.require('log');

  class AreaRouter {

    constructor() {
      this.router = express.Router();

      this.router.param('id', function(req, res, next, id) {
        req.param._id = id;
        next();
      });

      this.router.get('/', this.getList.bind(this));
      this.router.post('/', this.add.bind(this));
      this.router.delete('/:id', this.remove.bind(this));
      this.router.put('/:id', this.update.bind(this));

      return this.router;
    }

    getList(req, res, next) {
      HZArea.find({
        tags: {$in: ['crossserver']}
      }).then((list) => {
        if (list) {
          res.json(list.map((item) => {
            return item.toObject()
          }));
        } else {
          res.json({
            error: "Can't get list"
          });
        }
      });
    }

    add(req, res, next) {
      if (req.body && req.body.name && req.body.points) {

      const area = new HZArea({
        name  : req.body.name,
        points: req.body.points,
        tags  : ['crossserver']
      });

      area
        .save()
        .then(area => {
          HZLog(HZLog.ADDAREA, req.param.user, req.body);
          res.json({
            result: "ok"
          });
        })
      ;


      }
    }

    remove(req, res, next) {
      if (req.param._id) {
        HZArea.findOne({
          _id: HZArea.id(req.param._id)
        }).then((area) => {
          if (area) {
            area.delete();
            HZLog(HZLog.REMOVEAREA, req.param.user, area);
            res.json({
              result: "ok"
            });
          } else {
            res.json({
              error: "Area does not exists"
            });
          }
        });
      }
    }

    update(req, res, next) {
      if (req.body && req.body.points && req.param._id) {
        HZArea.findOne({
          _id: HZArea.id(req.param._id)
        }).then((area) => {
          if (area) {

            area.name   = req.body.name;
            area.points = req.body.points;
            
            area
              .save()
              .then(area => {
                HZLog(HZLog.UPDATEAREA, req.param.user, req.body);
                res.json({
                  result: "ok"
                });
              })
            ;

          } else {
            res.json({
              error: "Area does not exists"
            });
          }
        });
      }
    }
  }

  return new AreaRouter();

};
