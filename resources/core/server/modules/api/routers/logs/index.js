'use strict';

module.exports = function(horizon, coreLogModule) {

  const express = require('express');

  class LogRouter {

    constructor() {
      this.router_ = express.Router();

      this.router_.get('/', this.getList_.bind(this));

      return this.router_;
    }

    getList_(req, res, next) {
      coreLogModule.find({}, 0, 20, {date: -1}).then((list) => {
        if (list) {
          res.json(list.map((item) => {
            return item.toObject()
          }));
        } else {
          res.json({
            error: "Can't get list"
          });
        }
      });
    }
  }

  return new LogRouter();

};
