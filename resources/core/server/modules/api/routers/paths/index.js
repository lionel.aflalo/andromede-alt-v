'use strict';

module.exports = function(horizon, HZPath) {

  const express = require('express');
  const HZLog = horizon.require('log');
  const HZPosition = horizon.require('position');

  class PathRouter {

    constructor() {
      this.router = express.Router();

      this.router.param('id', function(req, res, next, id) {
        req.param._id = id;
        next();
      });

      this.router.get('/', this.getList.bind(this));
      this.router.post('/', this.add.bind(this));
      this.router.delete('/:id', this.remove.bind(this));
      this.router.put('/:id', this.update.bind(this));

      return this.router;
    }

    getList(req, res, next) {
      HZPath.find({}).then((list) => {
        if (list) {
          res.json(list.map((item) => {
            return item.toObject()
          }));
        } else {
          res.json({
            error: "Can't get list"
          });
        }
      });
    }

    add(req, res, next) {
      if (req.body && req.body.name) {

        const promises = [];

        for (let i = 0; i < req.body.positions.length; i++)
          promises.push(HZPosition.findOne({
            _id: HZPosition.id(req.body.positions[i])
          }))

        Promise
          .all(promises)
          .then((positions) => {

            const path = new HZPath({
              name: req.body.name,
              positions: positions
            });

            path
              .save()
              .then(path => {
                HZLog(HZLog.ADDPATH, req.param.user, req.body);
                res.json({
                  result: "ok"
                });
              });
          });

      }
    }

    remove(req, res, next) {
      if (req.param._id) {
        HZPath.findOne({
          _id: HZPath.id(req.param._id)
        }).then((path) => {
          if (path) {
            path.delete();
            HZLog(HZLog.REMOVEPATH, req.param.user, path);
            res.json({
              result: "ok"
            });
          } else {
            res.json({
              error: "Path does not exists"
            });
          }
        });
      }
    }

    update(req, res, next) {
      if (req.body && req.param._id) {
        HZPath.findOne({
          _id: HZPath.id(req.param._id)
        }).then((path) => {
          if (path) {

            const promises = [];

            for (let i = 0; i < req.body.positions.length; i++)
              promises.push(HZPosition.findOne({
                _id: HZPosition.id(req.body.positions[i])
              }))

            Promise
              .all(promises)
              .then((positions) => {

                path.name = req.body.name;
                path.positions = positions;

                path
                  .save()
                  .then(path => {
                    HZLog(HZLog.UPDATEPATH, req.param.user, req.body);
                    res.json({
                      result: "ok"
                    });
                  });

              });

          } else {
            res.json({
              error: "Path does not exists"
            });
          }
        });
      }
    }
  }

  return new PathRouter();

};
