'use strict';

module.exports = function(horizon, HZVFS) {

	const express = require('express');

	class VFSRouter {

		constructor() {
		  
			this.router_ = express.Router();

			this.router_.param('id', function (req, res, next, id) {
			  req.param._id = id;
			  next();
			});

			this.router_.get('/:id', this.getFile.bind(this));;

			return this.router_;
		}

		getFile(req, res, next) {
			
      HZVFS.getFileStreamId(req.param._id)
        .then(data => {
          res.sendSeekable(data.stream, {length: data.data.length});
        })
        .catch(e => {
          res.status(404).end();
        })
      ;

    }
	}

	return new VFSRouter();

};
