'use strict';

module.exports = function(horizon, corePnjLifeModule) {

  const express = require('express');
  const HZPnj = horizon.require('pnj');
  const HZLog = horizon.require('log');

  class PnjLifeRouter {

    constructor() {
      this.router_ = express.Router();

      this.router_.param('id', function(req, res, next, id) {
        req.param._id = id;
        next();
      });

      this.router_.get('/', this.getList_.bind(this));
      this.router_.post('/', this.add_.bind(this));
      this.router_.delete('/:id', this.remove_.bind(this));
      this.router_.put('/:id', this.update_.bind(this));

      return this.router_;
    }

    getList_(req, res, next) {
      corePnjLifeModule.find({}).then((list) => {
        if (list) {
          res.json(list.map((item) => {
            return item.toObject()
          }));
        } else {
          res.json({
            error: "Can't get list"
          });
        }
      });
    }

    add_(req, res, next) {
      if (req.body && req.body.pnj) {
        req.body._type = "PnjLife";
        HZPnj.findOrCreate({
          _id: corePnjLifeModule.id(req.body.pnj)
        }).then((pnj, exist) => {
          req.body.pnj = pnj;
          corePnjLifeModule.findOrCreate({
            pnj: req.body.pnj
          }, req.body).then((entity, exist) => {
            if (!exist)
              HZLog(HZLog.ADDPNJLIFE, req.param.user, req.body);
            res.json(exist ? {
              error: "Pnj life already exist"
            } : {
              result: "ok"
            })
          });
        })
      }
    }

    remove_(req, res, next) {
      if (req.param._id) {
        corePnjLifeModule.findOne({
          _id: corePnjLifeModule.id(req.param._id)
        }).then((entity) => {
          if (entity) {
            HZLog(HZLog.REMOVEPNJLIFE, req.param.user, entity);
            entity.delete();
            res.json({
              result: "ok"
            });
          } else {
            res.json({
              error: "Pnj life do not exist"
            });
          }
        });
      }
    }

    update_(req, res, next) {
      if (req.body && req.param._id) {
        corePnjLifeModule.findOne({
          _id: corePnjLifeModule.id(req.param._id)
        }).then((entity) => {
          if (entity) {
            HZPnj.findOrCreate({
              _id: corePnjLifeModule.id(req.body.pnj)
            }).then((pnj, exist) => {
              HZLog(HZLog.UPDATEPNJLIFE, req.param.user, entity, req.body);
              entity.pnj = pnj
              entity.save();
              res.json({
                result: "ok"
              });
            });
          } else {
            res.json({
              error: "Pnj life do not exist"
            });
          }
        });
      }
    }
  }

  return new PnjLifeRouter();

};
