'use strict';

module.exports = function(horizon, coreHarvestzoneModule) {

  const express = require('express');
  const HZPosition = horizon.require('position');
  const HZLog = horizon.require('log');

  class HarvestzoneRouter {

    constructor() {
      this.router_ = express.Router();

      this.router_.param('id', function(req, res, next, id) {
        req.param._id = id;
        next();
      });

      this.router_.get('/', this.getList_.bind(this));
      this.router_.post('/', this.add_.bind(this));
      this.router_.delete('/:id', this.remove_.bind(this));
      this.router_.put('/:id', this.update_.bind(this));

      return this.router_;
    }

    getList_(req, res, next) {
      coreHarvestzoneModule.find({}).then((list) => {
        if (list) {
          res.json(list.map((item) => {
            return item.toObject()
          }));
        } else {
          res.json({
            error: "Can't get list"
          });
        }
      });
    }

    add_(req, res, next) {
      if (req.body && req.body.name) {
        req.body._type = "HarvestZone";
        HZPosition.findOrCreate({
          _id: coreHarvestzoneModule.id(req.body.position)
        }).then((position, exist) => {
          req.body.position = position;
          req.body.recipe = req.body.recipe || [];
          coreHarvestzoneModule.findOrCreate({
            name: req.body.name,
            position: position
          }, req.body).then((entity, exist) => {
            this.refreshHarvestzone();
            if (!exist)
              HZLog(HZLog.ADDHARVEST, req.param.user, new Date(), req.body);
            res.json(exist ? {
              error: "Harvestzone already exist"
            } : {
              result: "ok"
            })
          });
        })
      }
    }

    remove_(req, res, next) {
      if (req.param._id) {
        coreHarvestzoneModule.findOne({
          _id: coreHarvestzoneModule.id(req.param._id)
        }).then((entity) => {
          if (entity) {
            HZLog(HZLog.REMOVEHARVEST, req.param.user, new Date(), entity);
            entity.delete();
            res.json({
              result: "ok"
            });
            this.refreshHarvestzone();
          } else {
            res.json({
              error: "Harvestzone do not exist"
            });
          }
        });
      }
    }

    update_(req, res, next) {
      if (req.body && req.param._id) {
        coreHarvestzoneModule.findOne({
          _id: coreHarvestzoneModule.id(req.param._id)
        }).then((entity) => {
          if (entity) {
            HZPosition.findOrCreate({
              _id: coreHarvestzoneModule.id(req.body.position)
            }).then((position, exist) => {
              HZLog(HZLog.UPDATEHARVEST, req.param.user, new Date(), entity, req.body);
              entity.name = req.body.name;
              entity.recipe = req.body.recipe;
              entity.maxqty = req.body.maxqty;
              entity.currentqty = req.body.currentqty;
              entity.harvestinterval = req.body.harvestinterval;
              entity.regeninterval = req.body.regeninterval;
              entity.group = req.body.group;
              entity.position = position
              entity.save();
              res.json({
                result: "ok"
              });
              this.refreshHarvestzone();
            });
          } else {
            res.json({
              error: "Harvestzone do not exist"
            });
          }
        });
      }
    }


    refreshHarvestzone() {
      setTimeout(() => coreHarvestzoneModule.find({}).then(function(result) {
        horizon.triggerClientEvent('horizon:harvestzones:refresh', result.map((item) => {
          return item.toObject()
        }));
      }), 2000);
    }
  }

  return new HarvestzoneRouter();

};
