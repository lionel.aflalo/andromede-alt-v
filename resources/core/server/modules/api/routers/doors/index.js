'use strict';

module.exports = function(horizon, coreDoorModule) {

  const express = require('express');
  const HZPosition = horizon.require('position');
  const HZLog = horizon.require('log');

  class DoorRouter {

    constructor() {
      this.router_ = express.Router();

      this.router_.param('id', function(req, res, next, id) {
        req.param._id = id;
        next();
      });

      this.router_.get('/', this.getList_.bind(this));
      this.router_.post('/', this.add_.bind(this));
      this.router_.delete('/:id', this.remove_.bind(this));
      this.router_.put('/:id', this.update_.bind(this));

      return this.router_;
    }

    getList_(req, res, next) {
      coreDoorModule.find({}).then((list) => {
        if (list) {
          res.json(list.map((item) => {
            return item.toObject()
          }));
        } else {
          res.json({
            error: "Can't get list"
          });
        }
      });
    }

    add_(req, res, next) {
      if (req.body && req.body.name) {
        req.body._type = "Door";
        HZPosition.findOrCreate({
          _id: coreDoorModule.id(req.body.position)
        }).then((position, exist) => {
          req.body.position = position;
          if (!exist)
            HZLog(HZLog.ADDDOOR, req.param.user, req.body);
          coreDoorModule.findOrCreate({
            name: req.body.name,
            position: position
          }, req.body).then((entity, exist) => {
            res.json(exist ? {
              error: "Door already exist"
            } : {
              result: "ok"
            })
          });
        })
      }
    }

    remove_(req, res, next) {
      if (req.param._id) {
        coreDoorModule.findOne({
          _id: coreDoorModule.id(req.param._id)
        }).then((entity) => {
          if (entity) {
            HZLog(HZLog.REMOVEDOOR, req.param.user, entity);
            entity.delete();
            res.json({
              result: "ok"
            });
          } else {
            res.json({
              error: "Door do not exist"
            });
          }
        });
      }
    }

    update_(req, res, next) {
      if (req.body && req.param._id) {
        coreDoorModule.findOne({
          _id: coreDoorModule.id(req.param._id)
        }).then((entity) => {
          if (entity) {
            HZPosition.findOrCreate({
              _id: coreDoorModule.id(req.body.position)
            }).then((position, exist) => {
              HZLog(HZLog.UPDATEDOOR, req.param.user, entity, req.body);
              entity.name = req.body.name;
              entity.prop = req.body.prop;
              entity.group = req.body.group;
              entity.position = position;
              entity.description = req.body.description;
              entity.locked = req.body.locked;
              entity.save();
              res.json({
                result: "ok"
              });
            });
          } else {
            res.json({
              error: "Door do not exist"
            });
          }
        });
      }
    }
  }

  return new DoorRouter();

};
