'use strict';

module.exports = function(horizon, corePositionModule) {

  const express = require('express');
  const HZLog = horizon.require('log');

  class PositionRouter {

    constructor() {
      this.router_ = express.Router();

      this.router_.param('id', function(req, res, next, id) {
        req.param._id = id;
        next();
      });

      this.router_.get('/', this.getList_.bind(this));
      this.router_.post('/', this.add_.bind(this));
      this.router_.delete('/:id', this.remove_.bind(this));
      this.router_.put('/:id', this.update_.bind(this));

      return this.router_;
    }

    getList_(req, res, next) {
      corePositionModule.find({}).then((list) => {
        if (list) {
          res.json(list.map((item) => {
            return item.toObject()
          }));
        } else {
          res.json({
            error: "Can't get list"
          });
        }
      });
    }

    add_(req, res, next) {
      if (req.body && req.body.name) {
        req.body._type = "Position";
        corePositionModule.findOrCreate({
          x: req.body.x,
          y: req.body.y,
          z: req.body.z
        }, req.body).then((entity, exist) => {
          if (!exist)
            HZLog(HZLog.ADDPOSITION, req.param.user, req.body);
          res.json(exist ? {
            error: "Position already exist"
          } : {
            result: "ok"
          })
        });
      }
    }

    remove_(req, res, next) {
      if (req.param._id) {
        corePositionModule.findOne({
          _id: corePositionModule.id(req.param._id)
        }).then((entity) => {
          if (entity) {
            HZLog(HZLog.REMOVEPOSITION, req.param.user, entity);
            entity.delete();
            res.json({
              result: "ok"
            });
          } else {
            res.json({
              error: "Position do not exist"
            });
          }
        });
      }
    }

    update_(req, res, next) {
      if (req.body && req.param._id) {
        corePositionModule.findOne({
          _id: corePositionModule.id(req.param._id)
        }).then((entity) => {
          if (entity) {
            HZLog(HZLog.UPDATEPOSITION, req.param.user, req.body);
            entity.name = req.body.name;
            entity.group = req.body.group;
            entity.x = req.body.x || 0;
            entity.y = req.body.y || 0;
            entity.z = req.body.z || 0;
            entity.save();
            res.json({
              result: "ok"
            });
          } else {
            res.json({
              error: "Position do not exist"
            });
          }
        });
      }
    }
  }

  return new PositionRouter();

};
