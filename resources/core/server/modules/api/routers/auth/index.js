'use strict';

const crypto  = require('crypto');
const express = require('express');

module.exports = function(horizon, corePlayerModule) {

  const HZLog = horizon.require('log');

  class PlayerRouter {

    constructor() {

      this.router_ = express.Router();

      this.router_.get('/', this.getList_.bind(this));
      this.router_.post('/', this.login_.bind(this));

      return this.router_;
    }

    getList_(req, res, next) {
      corePlayerModule.find({
        roles: {$in: ['player:dev', 'player:admin']}
      }).then((list) => {
        if (list) {
          res.json(list.map((item) => item.toObject()));
        } else {
          res.json({
            error: "Can't get list"
          });
        }
      });
    }

    login_(req, res, next) {
      if (req.body && req.body.pseudo && req.body.password) {
        corePlayerModule.findOne({
          'identifiers.steam' : req.body.pseudo,
          'roles'             : {$in: ['player:dev', 'player:admin']}
        }).then((entity) => {
          
          if (entity != null) {

            let id          = crypto.randomBytes(16).toString('hex');
            let pass_shasum = crypto.createHash('sha256').update(req.body.password).digest('hex');
            
            if (entity.password != null && entity.password != undefined) {
              
              if (entity.password == pass_shasum) {
                
                res.json({
                  token: id
                });
                
                HZLog(HZLog.LOGIN, entity.name);

              } else {
                
                res.json({
                  error: 'Bad password'
                });
              }

            } else {

              entity.password = pass_shasum;

              res.json({
                token: id
              });

            }

            entity.token = id;
            entity.save();

            return;

          } else {

            res.json({
              error: "Can't log"
            });

          }
        });
      } else {

        res.json({
          error: "Can't log"
        });

      }
    }
  }

  return new PlayerRouter();

};
