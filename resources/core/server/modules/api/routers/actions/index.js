'use strict';

module.exports = function(horizon, coreActionModule) {

	const express      = require('express');
    const HZLog = horizon.require('log');

	class ActionRouter {

		constructor() {
		  	this.router_ = express.Router();

			this.router_.param('id', function (req, res, next, id) {
			  req.param._id = id;
			  next();
			});

			this.router_.get('/', this.getList_.bind(this));
			this.router_.post('/', this.add_.bind(this));
			this.router_.delete('/:id', this.remove_.bind(this));
			this.router_.put('/:id', this.update_.bind(this));

			return this.router_;
		}

		getList_(req, res, next) {
            coreActionModule.find({}).then((list) => {
            	if (list) {
               		res.json(list.map((item) => { return item.toObject() }));
            	} else {
                    res.json({error: "Can't get list"});
            	}
            });
         }

        add_(req, res, next) {
	        if (req.body && req.body.name) {
                req.body._type = "Action";
                coreActionModule.findOrCreate({name: req.body.name}, req.body).then((entity, exist) => {
                if (!exist)
                    HZLog(HZLog.ADDACTION, req.param.user, req.body);
                res.json(exist ? {error: "Action already exist"} : {result: "ok"})
                });
	        }
        }

        remove_(req, res, next) {
	        if (req.param._id) {
                coreActionModule.findOne({_id: coreActionModule.id(req.param._id)}).then((entity) => {
                    if (entity) {
                        HZLog(HZLog.REMOVEACTION, req.param.user, entity);
                    	entity.delete();
                    	res.json({result: "ok"});
                    }else {
                    	res.json({error: "Action do not exist"});
                    }
                });
	        }
        }

        update_(req, res, next) {
            if (req.body && req.param._id) {
                coreActionModule.findOne({_id: coreActionModule.id(req.param._id)}).then((entity) => {
                    if (entity) {
                        HZLog(HZLog.UPDATEACTION, req.param.user, entity, req.body);
                        entity.name = req.body.name;
                        entity.description = req.body.description;
                        entity.save();
                        res.json({result: "ok"});
                    } else {
                    	res.json({error: "Action do not exist"});
                    }
                });
            }
        }
	}

	return new ActionRouter();

};
