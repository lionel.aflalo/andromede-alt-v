'use strict';

const async  = require('async');
const crypto = require('crypto');
const atob   = require('atob');

module.exports = function(horizon, HZBank) {

  const express               = require('express');
  const HZLog                = horizon.require('log');
  const HZContainer          = horizon.require('container');
  const HZUnlimitedContainer = horizon.require('unlimitedcontainer');
  const HZBankUser           = horizon.require('bank/user');
  const HZStandardBankUser   = horizon.require('bank/user/standard');
  const HZProBankUser        = horizon.require('bank/user/pro');
  const HZBankAccount        = horizon.require('bank/account');
  const HZBankTransaction    = horizon.require('bank/transaction');
  const HZBankAutoWire       = horizon.require('bank/autowire');
  const HZBankLoan           = horizon.require('bank/loan');
  const HZBankLoanFormulas   = horizon.require('bank/loan/formulas');
  const HZBankAccountLimits  = horizon.require('bank/account/limits');
  const HZBankCard           = horizon.require('bank/card'); 
  const HZOS                 = horizon.require('os');

  class BankRouter {

    constructor() {

      this.router = express.Router();

      this.router.param('id', function(req, res, next, id) {
        req.param._id = id;
        next();
      });

      // GET
      this.router.get('/users',          this.getUsers          .bind(this));
      this.router.get('/accounts',       this.getAccounts       .bind(this));
      this.router.get('/useraccounts',   this.getAllUserAccounts.bind(this));
      this.router.get('/users/standard', this.getStandardUsers  .bind(this));
      this.router.get('/users/pro',      this.getProUsers       .bind(this));
      this.router.get('/autowires',      this.getAutoWires      .bind(this));
      this.router.get('/loanformulas',   this.getLoanFormulas   .bind(this));
      this.router.get('/accountlimits',  this.getAccountLimits  .bind(this));
      this.router.get('/loans',          this.getLoans          .bind(this));
      
      this.router.get('/user/:id',                this.getUser        .bind(this));
      this.router.get('/account/:id',             this.getAccount     .bind(this));
      this.router.get('/transactions/:accountid', this.getTransactions.bind(this));
      this.router.get('/stats/:sample',           this.getStats       .bind(this));
      this.router.get('/user.accounts/:id',       this.getUserAccounts.bind(this));
      this.router.get('/account.cards/:id',       this.getAccountCards.bind(this));
      
      this.router.get('/sendcustomerinfos/:id', this.sendCustomerInfos.bind(this));

      // POST
      this.router.post('/auth',      this.authUser   .bind(this));
      this.router.post('/users',     this.addUser    .bind(this));
      this.router.post('/autowires', this.addAutoWire.bind(this));
      this.router.post('/accounts',  this.addAccount .bind(this));
      this.router.post('/transfer',  this.transfer   .bind(this));
      this.router.post('/loans',     this.addLoan    .bind(this));
      this.router.post('/cards',     this.createCard .bind(this));

      // PUT
      this.router.put('/user/:id',    this.updateUser   .bind(this));
      this.router.put('/account/:id', this.updateAccount.bind(this));
      
      // DELETE
      this.router.delete('/user/:id',     this.removeUser    .bind(this));
      this.router.delete('/card/:id',     this.removeCard    .bind(this));
      this.router.delete('/account/:id',  this.removeAccount .bind(this));
      this.router.delete('/autowire/:id', this.removeAutoWire.bind(this));

      return this.router;
    }

    authenticate(req, roles = {$in: ['admin']}) {

      return new Promise((resolve, reject) => {

        if(!req.headers.authorization) {
          resolve(null);
          return;
        }

        let credentials = atob(req.headers.authorization.split(' ')[1]).split(':');

        if(credentials[1] === null)
          resolve(null);

        HZBankUser
          .findOne({
            roles : roles,
            login : credentials[0],
            token : credentials[1],
            active: true
          }).then((user) => {
            
            if(user) {
              
              if(user.token === credentials[1])
                resolve(user);
              else
                resolve(null);

            } else resolve(null);

          })
        ;

      });

    }

    // GET
    getUsers(req, res, next) {

      this
        .authenticate(req)
        .then(user => {

          if(user) {

            HZBankUser.find({}).then((users) => {
              if (users)
                res.json(users.map(item => item.toObject()));
              else
                res.json({error: 'Can\'t get users'});
            });

          } else res.json({error: 'Authentification required'});

        })
      ;
    }

    getAccounts(req, res, next) {

      this
        .authenticate(req)
        .then(user => {

          if(user) {

            HZBankAccount
              .findOfType({})
              .then(accounts => res.json(accounts.map(e => e.toObject())))
            ;

          } else res.json({error: 'Authentification required'});

        })
      ;
    }

    getAllUserAccounts(req, res, next) {

      this
        .authenticate(req)
        .then(user => {

          if(user) {

            HZBankUser.find({active: true}).then((users) => {

              const tasks        = [];
              const userAccounts = [];

              for(let i=0; i<users.length; i++) {

                ((user) => {

                  tasks.push(cb => {

                    const data = {user: user.toObject()};

                    HZBankAccount 
                      .find({_id: {$in: user.accounts}, active: true})
                      .then(accounts => {
                        data.accounts = accounts.map(e => e.toObject());
                        userAccounts.push(data);
                        cb(null);
                      })
                    ;


                  });

                })(users[i]);

              }

              async.parallelLimit(tasks, 10, (err, results) => {
                res.json(userAccounts);
              });

            });

          } else res.json({error: 'Authentification required'});

        })
      ;
    }

    getStandardUsers(req, res, next) {

      this
        .authenticate(req)
        .then(user => {

          if(user) {

            HZStandardBankUser
              .findOfType({active: true})
              .then((users) => {
                if (users)
                  res.json(users.map(item => item.toObject()));
                else
                  res.json({error: 'Can\'t get users'});
              })
            ;

          } else res.json({error: 'Authentification required'});

        })
      ;
    }

    getProUsers(req, res, next) {

      this
        .authenticate(req)
        .then(user => {

          if(user) {

            HZProBankUser
              .findOfType({active: true})
              .then((users) => {
                if (users)
                  res.json(users.map(item => item.toObject()));
                else
                  res.json({error: 'Can\'t get users'});
              })
            ;

          } else res.json({error: 'Authentification required'});

        })
      ;
    }

    getAutoWires(req, res, next) {

      this
        .authenticate(req)
        .then(user => {

          if(user) {

            HZBankAutoWire
              .findOfType({
                $or: [
                  {active: true, iterationsLeft : -1},
                  {active: true, iterationsLeft : {$gt: 0}}
                ]
              }, null, null, {timestamp: 1})
              .then((autoWires) => {
                if (autoWires)
                  res.json(autoWires.map(item => item.toObject()));
                else
                  res.json({error: 'Can\'t get auto wires'});
              })
            ;

          } else res.json({error: 'Authentification required'});

        })
      ;
    }

    getLoanFormulas(req, res, next) {

      this
        .authenticate(req)
        .then(user => {

          if(user) {

            res.json(HZBankLoanFormulas);

          } else res.json({error: 'Authentification required'});

        })
      ;
    }

    getAccountLimits(req, res, next) {

      this
        .authenticate(req)
        .then(user => {

          if(user) {

            res.json(HZBankAccountLimits);

          } else res.json({error: 'Authentification required'});

        })
      ;
    }

    getLoans(req, res, next) {

      this
        .authenticate(req)
        .then(user => {

          if(user) {

            HZBankLoan
              .findOfType({}, null, null, {timestamp: 1})
              .then((loans) => {
                if (loans)
                  res.json(loans.map(item => item.toObject()));
                else
                  res.json({error: 'Can\'t get loans'});
              })
            ;

          } else res.json({error: 'Authentification required'});

        })
      ;
    }

    getUser(req, res, next) {

      this
        .authenticate(req)
        .then(user => {

          if(user) {

            const idStr = req.params.id || '';

            if(idStr.length !== 12 && idStr.length !== 24) {
              res.json({error: 'Wrong user id'});
              return;
            }

            HZBankUser
              .findOne({_id: HZBankUser.id(idStr), active: true})
              .then(user => {
                if(user)
                  res.json(user.toObject());
                else
                  res.json({error: 'Can\'t get user'});
              })
            ;

          } else res.json({error: 'Authentification required'});

        })
      ;
    }

    getUserAccounts(req, res, next) {

      this
        .authenticate(req)
        .then(user => {

          if(user) {

            const idStr = req.params.id || '';

            if(idStr.length !== 12 && idStr.length !== 24) {
              res.json({error: 'Wrong user id'});
              return;
            }

            HZBankUser
              .findOne({_id: HZBankUser.id(idStr), active: true})
              .then(user => {
                
                if(user) {
                  
                  HZBankAccount
                    .find({_id: {$in: user.accounts}, active: true})
                    .then(accounts => {
                      res.json(accounts.map(e => e.toObject()));
                    })
                  ;

                } else res.json({error: 'Can\'t get user'});

              })
            ;

          } else res.json({error: 'Authentification required'});

        })
      ;
    }

    getAccountCards(req, res, next) {

      this
        .authenticate(req)
        .then(user => {

          if(user) {

            const idStr = req.params.id || '';

            if(idStr.length !== 12 && idStr.length !== 24) {
              res.json({error: 'Wrong user id'});
              return;
            }

            HZBankCard
              .findOfType({account: HZBankAccount.id(idStr)})
              .then(cards => {
                res.json(cards.map(e => e.toObject()));
              })
            ;

          } else res.json({error: 'Authentification required'});

        })
      ;
    }

    sendCustomerInfos(req, res, next) {

      this
        .authenticate(req)
        .then(user => {

          if(user) {

            const idStr = req.params.id || '';

            if(idStr.length !== 12 && idStr.length !== 24) {
              res.json({error: 'Wrong user id'});
              return;
            }

            HZBankUser
              .findOne({_id: HZBankUser.id(idStr)})
              .then(user => {

                if(user) {

                  HZBankCard
                    .find({$or: user.accounts.map(e => ({account: e}))})
                    .then(cards => {

                      let body =
`[Pacific Bank]

Nom d'utilisateur : ${user.login}
Mot de passe : ${user.password} 

Carte bancaire | Mot de passe
${cards.map(e => e.number + ' | ' + e.pinCode).join("\n\n")}
`;
                    
                    HZOS.os
                      .sendMessage(body, user.phoneNumber, [], 1111111111)
                      .then(_id => {
                        res.json({_id: _id.toString()});
                      })
                    ;

                    })
                  ;

                } else res.json({error: 'User not found'});

              })
            ;

          } else res.json({error: 'Authentification required'});

        })
      ;

    }

    getAccount(req, res, next) {

      this
        .authenticate(req)
        .then(user => {

          if(user) {

            const idStr = req.params.id || '';

            if(idStr.length !== 12 && idStr.length !== 24) {
              res.json({error: 'Wrong account id'});
              return;
            }

            HZBankAccount
              .findOne({_id: HZBankAccount.id(idStr)})
              .then(account => {
                if(account)
                  res.json(account.toObject());
                else
                  res.json({error: 'Can\'t get account'});
              })
            ;

          } else res.json({error: 'Authentification required'});

        })
      ;
    }

    getTransactions(req, res, next) {

      this
        .authenticate(req)
        .then(user => {

          if(user) {

            const idStr = req.params.accountid || '';

            if(idStr.length !== 12 && idStr.length !== 24) {
              res.json({error: 'Wrong account id'});
              return;
            }

            HZBankTransaction
              .find({
                $or: [
                  {sender  : HZBankAccount.id(idStr)},
                  {receiver: HZBankAccount.id(idStr)},
                ]
              })
              .then(transactions => {
                res.json(transactions.map(e => e.toObject()));
              })
            ;
          } else res.json({error: 'Authentification required'});

        })
      ;
    }

    getStats(req, res, next) {

      const sample = parseInt(req.params.sample, 10);

      this
        .authenticate(req)
        .then(user => {

          if(user) {

            const date = new Date();
            
            date.setHours(0);
            date.setMinutes(0)

            const timestamp    = Math.floor(+date / 1000);
            const minusSample  = timestamp - sample * 24 * 60 * 60;
            const data         = {};
            let   bankAccounts = null;

            async.parallel([

              cb => {

                HZBankTransaction
                  .find({timestamp: {$gte: minusSample}})
                  .then(transactions => {
                    data.transactions = transactions;
                    cb(null);
                  })
                ;
              },

              cb => {

                HZProBankUser
                  .findOne({name: 'Pacific Bank', active: true})
                  .then(user => {

                    if(user === null) {
                      
                      bankAccounts          = [];
                      data.bankTransactions = [];
                      
                      cb(null);

                      return;
                    
                    } else {

                      bankAccounts = user.accounts;

                      HZBankTransaction
                        .find({
                          $or: [
                            {
                              sender    : {$in: user.accounts},
                              timestamp : {$gte: minusSample},
                            },
                            {
                              receiver  : {$in: user.accounts},
                              timestamp : {$gte: minusSample},
                            },
                          ]
                        })
                        .then(transactions => {
                          data.bankTransactions = transactions;
                          cb(null);
                        })
                      ; 

                    }

                  })
                ;

              },

              cb => {

                HZBankLoan
                  .find({active: true})
                  .then(loans => {
                    data.loans = loans;
                    cb(null);
                  })
                ;

              }

            ], (err, results) => {
              
              const variations = [];
              const fixed      = {};

              for(let i=0; i<sample + 1; i++) {

                const sampleTime = minusSample + i * 24 * 60 * 60;
                const sampleDate = new Date(sampleTime * 1000);
                const name       = sampleDate.getDate().toString().padStart(2, '0') + '/' + (sampleDate.getMonth() + 1).toString().padStart(2, '0');
                
                // Total volume
                let totalVolume          = 0;
                const volumeTransactions = data.transactions.filter(e => e.timestamp >= sampleTime && e.timestamp < sampleTime + 24 * 60 * 60);
                volumeTransactions.forEach(e => totalVolume += Math.abs(e.amount));

                // Bank profits
                let bankProfits = 0;

                data.bankTransactions.forEach(e => {

                  const isSender   = bankAccounts.findIndex(e2 => e2.equals(e.sender))   !== -1;
                  const isReceiver = bankAccounts.findIndex(e2 => e2.equals(e.receiver)) !== -1;

                  if(e.timestamp >= sampleTime && e.timestamp < sampleTime + 24 * 60 * 60) {

                    if((isSender && e.amount < 0) || (isReceiver && e.amount > 0))
                      bankProfits += e.amount;
                    else
                      bankProfits -= e.amount;

                  }
                
                });

                // Bank gains
                let bankGains = 0;

                const bankGainsTransactions = data.bankTransactions.filter(e => {

                  const isSender   = bankAccounts.findIndex(e2 => e2.equals(e.sender))   !== -1;
                  const isReceiver = bankAccounts.findIndex(e2 => e2.equals(e.receiver)) !== -1;

                  return e.timestamp >= sampleTime && e.timestamp < sampleTime + 24 * 60 * 60 && ((isSender && e.amount < 0) || (isReceiver && e.amount > 0));
                
                });

                bankGainsTransactions.forEach(e => bankGains += Math.abs(e.amount));

                // Bank loss
                let bankLosses = 0;

                const bankLossTransactions = data.bankTransactions.filter(e => {

                  const isSender   = bankAccounts.findIndex(e2 => e2.equals(e.sender))   !== -1;
                  const isReceiver = bankAccounts.findIndex(e2 => e2.equals(e.receiver)) !== -1;

                  return e.timestamp >= sampleTime && e.timestamp < sampleTime + 24 * 60 * 60 && ((isSender && e.amount > 0) || (isReceiver && e.amount < 0));
                
                });

                bankLossTransactions.forEach(e => bankLosses += Math.abs(e.amount));

                variations[i] = {
                  name,
                  totalVolume,
                  bankProfits,
                  bankGains,
                  bankLosses,
                };

              }

              // Loans
              fixed.remainingLoans = 0;
              data.loans.forEach(e => fixed.remainingLoans += e.remaining);

              res.json({variations, fixed});


            });

          } else res.json({error: 'Authentification required'});;

        })
      ;
    }

    // POST
    authUser(req, res, next) {

      if(req.body && req.body.login && req.body.password) {

        HZBankUser
          .findOne({
            roles    : {$in: ['admin']},
            login    : req.body.login,
            password : req.body.password,
            active   : true
          })
          .then(user => {

            if(user !== null) {

              user.token = crypto.randomBytes(16).toString('hex');

              user
                .save()
                .then(() => res.json({
                  user : user.toObject(),
                  token: user.token,
                }))
              ;

            } else res.json({error: 'Bad credentials'});

          })
        ;

      } else res.json({error: 'Missing fields'});
    }

    addUser(req, res, next) {

      this
        .authenticate(req)
        .then(user => {

          if(user) {

            if(
              typeof req.body             !== 'undefined' &&
              typeof req.body.isPro       !== 'undefined' &&
              typeof req.body.address     !== 'undefined' &&
              typeof req.body.phoneNumber !== 'undefined'
            ) {

              if(req.body.isPro) {

                if(
                  typeof req.body.societyName   !== 'undefined' &&
                  typeof req.body.societyNumber !== 'undefined'
                ) {

                  const bankUser = new HZProBankUser({
                    address       : req.body.address,
                    phoneNumber   : req.body.phoneNumber.toString(),
                    name          : req.body.societyName,
                    societyNumber : req.body.societyNumber
                  });

                  bankUser
                    .save()
                    .then(() => {
                      res.json({_id: bankUser._id.toString()})
                    })
                  ;

                } else res.json({error: 'Missing fields'});

              } else {

                if(
                  typeof req.body.firstName      !== 'undefined' &&
                  typeof req.body.lastName       !== 'undefined' &&
                  typeof req.body.residentNumber !== 'undefined'
                ) {

                  const bankUser = new HZStandardBankUser({
                    address        : req.body.address,
                    phoneNumber    : req.body.phoneNumber.toString(),
                    firstName      : req.body.firstName,
                    lastName       : req.body.lastName,
                    residentNumber : req.body.residentNumber
                  });

                  bankUser
                    .save()
                    .then(user => {
                      res.json({_id: bankUser._id.toString()})
                    })
                  ;

                } else res.json({error: 'Missing fields'});

              }

            } else res.json({error: 'Missing fields'});

          } else res.json({error: 'Authentification required'});

        })
      ;
    }

    addAutoWire(req, res, next) {

      this
        .authenticate(req)
        .then(user => {

          if(user) {

            if(
              typeof req.body            !== 'undefined' &&
              typeof req.body.sender     !== 'undefined' &&
              typeof req.body.receiver   !== 'undefined' &&
              typeof req.body.amount     !== 'undefined' &&
              typeof req.body.iterations !== 'undefined' &&
              typeof req.body.day        !== 'undefined' &&
              typeof req.body.startDate  !== 'undefined'
            ) {

              const senderIdStr = req.body.sender || '';

              if(senderIdStr.length !== 12 && senderIdStr.length !== 24) {
                res.json({error: 'Wrong sender id'});
                return;
              }

              const receiverIdStr = req.body.receiver || '';

              if(receiverIdStr.length !== 12 && receiverIdStr.length !== 24) {
                res.json({error: 'Wrong receiver id'});
                return;
              }

              const autoWire = new HZBankAutoWire({
                sender     : HZBankAccount.id(req.body.sender),
                receiver   : HZBankAccount.id(req.body.receiver),
                amount     : req.body.amount,
                frequency  : 7,
                iterations : req.body.iterations,
                day        : req.body.day,
                startDate  : req.body.startDate,
              });

              autoWire
                .save()
                .then(() => {
                  res.json({_id: autoWire._id.toString()})
                })
              ;

            } else res.json({error: 'Missing fields'});

          } else res.json({error: 'Authentification required'});

        })
      ;
    }

    addAccount(req, res, next) {

      this
        .authenticate(req)
        .then(user => {

          if(user) {

            if(
              typeof req.body               !== 'undefined' &&
              typeof req.body.userId        !== 'undefined' &&
              typeof req.body.accountType   !== 'undefined' &&
              typeof req.body.accountLabel  !== 'undefined' &&
              typeof req.body.accountLimits !== 'undefined'
            ) {

              const idStr = req.body.userId;

              if(idStr.length !== 12 && idStr.length !== 24) {
                res.json({error: 'Wrong user id'});
                return;
              }

              HZBankUser
                .findOne({_id: HZBankUser.id(idStr)})
                .then(user => {

                  if(user) {

                    const account = new HZBankAccount({
                      type  : req.body.accountType.toLowerCase(),
                      label : req.body.accountLabel.toLowerCase(),
                      limits: req.body.accountLimits
                    });

                    account
                      .init()
                      .then(() => {

                        account.save()
                          .then(() => {

                            user.accounts.push(account._id);

                            user
                              .save()
                              .then(() => res.json({result: 'ok'}))
                            ;

                          })
                        ;

                      })
                    ;

                  } else res.json({error: 'Can\'t get user'});

                })
              ;

            } else res.json({error: 'Missing fields'});

          } else res.json({error: 'Authentification required'});

        })
      ;
    }

    transfer(req, res, next) {

      this
        .authenticate(req)
        .then(user => {

          if(user) {

            if(
              typeof req.body           !== 'undefined' &&
              typeof req.body.sender    !== 'undefined' &&
              typeof req.body.receiver  !== 'undefined' &&
              typeof req.body.amount    !== 'undefined'
            ) {

              const senderIdStr = req.body.sender || '';

              if(senderIdStr.length !== 12 && senderIdStr.length !== 24) {
                res.json({error: 'Wrong sender id'});
                return;
              }

              const receiverIdStr = req.body.receiver || '';

              if(receiverIdStr.length !== 12 && receiverIdStr.length !== 24) {
                res.json({error: 'Wrong sender id'});
                return;
              }

              const object    = req.body.object    ? (req.body.object === '' ? null : req.body.object) : null;
              const initiator = user._id;

              HZBankAccount
                .findOne({_id: HZBankAccount.id(senderIdStr)})
                .then(senderAccount => {

                  if(senderAccount === null) {
                    res.json({error: 'Sender not found'});
                    return;
                  }

                  HZBankAccount
                    .findOne({_id: HZBankAccount.id(receiverIdStr)})
                    .then(receiverAccount => {

                      if(receiverAccount === null) {
                        res.json({error: 'Receiver not found'});
                        return;
                      }

                      senderAccount
                        .tryTransfer(receiverAccount, req.body.amount, object, initiator, true)
                        .then(success => {

                          if(success)
                            res.json({result: 'ok'});
                          else
                            res.json({error: 'Can\'t make transfer'})

                        })
                      ;

                    })
                  ;

                })
              ;


            } else res.json({error: 'Missing fields'});
            
          } else res.json({error: 'Authentification required'});

        })
      ;
    }

    addLoan(req, res, next) {

      this
        .authenticate(req)
        .then(bankUser => {

          if(bankUser) {

            if(
              typeof req.body           !== 'undefined' &&
              typeof req.body.user      !== 'undefined' &&
              typeof req.body.receiver  !== 'undefined' &&
              typeof req.body.amount    !== 'undefined' &&
              typeof req.body.formula   !== 'undefined' &&
              typeof req.body.day       !== 'undefined' &&
              typeof req.body.startDate !== 'undefined'
            ) {

              const userIdStr = req.body.user || '';

              if(userIdStr.length !== 12 && userIdStr.length !== 24) {
                res.json({error: 'Wrong user id'});
                return;
              }

              const receiverIdStr = req.body.receiver || '';

              if(receiverIdStr.length !== 12 && receiverIdStr.length !== 24) {
                res.json({error: 'Wrong receiver id'});
                return;
              }

              HZBankUser
                .findOne({_id: HZBankUser.id(userIdStr)})
                .then(user => {

                  if(user === null) {
                    res.json({error: 'User not found'});
                    return;
                  }

                  let formula;

                  if(user instanceof HZProBankUser)
                    formula = HZBankLoanFormulas.pro[req.body.formula]      || null;
                  else
                    formula = HZBankLoanFormulas.standard[req.body.formula] || null;

                  if(formula === null) {
                    res.json({error: 'Formula not found'});
                    return;
                  }

                  HZProBankUser
                    .findOneOfType({name: 'Pacific Bank'})
                    .then(bankUser => {

                      if(bankUser === null) {
                        res.json({error: 'Pacfific Bank pro user not found'});
                        return;
                      }

                      HZBankAccount
                        .findOfType({_id: {$in: bankUser.accounts}})
                        .then(bankAccounts => {

                          const mainBankAccounts = bankAccounts.filter(e => e.type === 'main');

                          if(mainBankAccounts.length === 0) {
                            res.json({error: 'Pacific Bank has no main account'});
                            return;
                          }

                          const sender = mainBankAccounts[0];

                          const autoWire = new HZBankAutoWire({
                            sender     : HZBankAccount.id(req.body.receiver),
                            receiver   : sender._id,
                            amount     : (req.body.amount + (req.body.amount / 100) * formula.percent) / Math.floor(formula.days / 7),
                            frequency  : 7,
                            iterations : Math.floor(formula.days / 7),
                            day        : req.body.day,
                            startDate  : req.body.startDate,
                            wireTags   : ['loan.payback'],
                          });

                          autoWire
                            .save()
                            .then(() => {
                              
                              const loan = new HZBankLoan({
                                receiver : HZBankAccount.id(req.body.receiver),
                                amount   : req.body.amount,
                                autoWire : autoWire._id,
                                formula  : formula,
                              });

                              loan
                                .init()
                                .then(() => {

                                  loan
                                    .save()
                                    .then(() => {

                                      HZBankAccount
                                        .findOne({_id: HZBankAccount.id(req.body.receiver)})
                                        .then(receiver => {
                                          
                                          sender
                                            .transfer(receiver, req.body.amount, 'Prêt bancaire #' + loan.number.toString().padStart(8, '0'), bankUser._id)
                                            .then(() => {
                                              res.json({_id: loan._id.toString()})
                                            })
                                          ;

                                        })
                                      ;

                                    })
                                  ;

                                })
                              ;

                            })
                          ;

                        })
                      ;

                    })
                  ;

                })
              ;

            } else res.json({error: 'Missing fields'});

          } else res.json({error: 'Authentification required'});

        })
      ;
    }

    createCard(req, res, next) {

      this
        .authenticate(req)
        .then(user => {

          if(user) {

            if(
              typeof req.body         !== 'undefined' &&
              typeof req.body.account !== 'undefined' &&
              typeof req.body.user    !== 'undefined'
            ) {

              const data = {
                account: null,
                user   : null,
                count  : 1,
              };

              const accountIdStr = req.body.account || '';

              if(accountIdStr.length !== 12 && accountIdStr.length !== 24) {
                res.json({error: 'Wrong account id'});
                return;
              } else data.account = HZBankAccount.id(accountIdStr);

              const userIdStr = req.body.user || '';

              if(userIdStr.length !== 12 && userIdStr.length !== 24) {
                res.json({error: 'Wrong user id'});
                return;
              } else data.user = HZBankUser.id(userIdStr);

              const card = new HZBankCard(data);

              card
                .init()
                .then(() => {

                  HZUnlimitedContainer
                    .findOrCreateOfType({name: 'PacificBankPrivateVault'}, {name: 'PacificBankPrivateVault'})
                    .then(container => {

                      container.addItemInstance(card);

                      container
                        .save()
                        .then(() => res.json(card.toObject()))
                      ;

                    })
                  ;

                })
              ;

            } else res.json({error: 'Missing fields'});

          } else res.json({error: 'Authentification required'});

        })
      ;
    }


    // PUT
    updateUser(req, res, next) {

      this
        .authenticate(req)
        .then(user => {

          if(user) {

            const idStr = req.params.id || '';

            if(idStr.length !== 12 && idStr.length !== 24) {
              res.json({error: 'Wrong user id'});
              return;
            }

            HZBankUser
              .findOne({_id: HZBankUser.id(idStr)})
              .then(user => {

                if(user) {

                  if(req.body) {

                    if(typeof req.body.address !== 'undefined')
                      user.address = req.body.address;

                    if(typeof req.body.phoneNumber !== 'undefined')
                      user.phoneNumber = req.body.phoneNumber.toString();

                    user
                      .save()
                      .then(() => res.json({result: 'ok'}))
                    ;

                  } else res.json({error: 'Missing params'});

                } else res.json({error: 'Can\'t get user'});

              })
            ;

          } else res.json({error: 'Authentification required'});

        })
      ;
    }

    updateAccount(req, res, next) {

      this
        .authenticate(req)
        .then(user => {

          if(user) {

            const idStr = req.params.id || '';

            if(idStr.length !== 12 && idStr.length !== 24) {
              res.json({error: 'Wrong account id'});
              return;
            }

            HZBankAccount
              .findOne({_id: HZBankAccount.id(idStr)})
              .then(account => {

                if(account) {

                  if(req.body && req.body.limits) {

                    account.limits = req.body.limits;

                    account
                      .save()
                      .then(() => res.json({result: 'ok'}))
                    ;

                  }

                } else res.json({error: 'Can\'t get account'});

              })
            ;

          } else res.json({error: 'Authentification required'});

        })
      ;
    }

    // DELETE
    removeUser(req, res, next) {

      this
        .authenticate(req)
        .then(user => {

          if(user) {

            const idStr = req.params.id || '';

            if(idStr.length !== 12 && idStr.length !== 24) {
              res.json({error: 'Wrong user id'});
              return;
            }

            const _autoWires = [];

            HZBankUser
              .findOne({_id: HZBankUser.id(idStr), active: true})
              .then(user => {

                if(user) {

                  user.token  = null;
                  user.active = false;

                  async.parallel([

                    cb => {

                      HZBankAccount
                        .find({_id: {$in: user.accounts}})
                        .then(accounts => {

                          const promises = [];

                          accounts.forEach(e => {
                            e.active = false;
                            promises.push(e.save());
                          });

                          Promise
                            .all(promises)
                            .then(() => cb(null))
                          ;

                        })
                      ;

                    },

                    cb => {

                      HZBankCard
                        .find({account: {$in: user.accounts}})
                        .then(cards => {

                          const promises = [];

                          cards.forEach(e => {
                            promises.push(e.delete());
                          });

                          Promise
                            .all(promises)
                            .then(() => cb(null))
                          ;

                        })
                      ;

                    },

                    cb => {

                      HZBankAutoWire
                        .find({
                          $or: [
                            {sender  : {$in: user.accounts}},
                            {receiver: {$in: user.accounts}},
                          ]
                        })
                        .then(autoWires => {

                          const promises = [];

                          autoWires.forEach(e => {
                            e.active = false;
                            promises.push(e.save());
                            _autoWires.push(e._id);
                          });

                          Promise
                            .all(promises)
                            .then(() => cb(null))
                          ;

                        })
                      ;

                    }

                  ], (err, results) => {

                    HZBankLoan
                      .find({autoWire: {$in: _autoWires}})
                      .then(loans => {

                        const promises = [];

                        loans.forEach(e => {
                          e.active = false;
                          promises.push(e.save());
                        });

                        Promise
                          .all(promises)
                          .then(() => {

                            user
                              .save()
                              .then(() => res.json({result: 'ok'}))
                            ;
                            
                          })
                        ;

                      })
                    ;

                  });

                } else res.json({error: 'Can\'t get user'});
              })
            ;
          } else res.json({error: 'Authentification required'});

        })
      ;
    }

    removeCard(req, res, next) {

      this
        .authenticate(req)
        .then(user => {

          if(user) {

            const idStr = req.params.id || '';

            if(idStr.length !== 12 && idStr.length !== 24) {
              res.json({error: 'Wrong user id'});
              return;
            }

            HZBankCard
              .findOne({_id: HZBankCard.id(idStr)})
              .then(card => {

                if(card !== null) {

                  card
                    .getParentContainers()
                    .then(containers => {

                      if(containers.length > 0) {

                        containers[0].deleteItem(card);

                        containers[0]
                          .save()
                          .then(() => {

                            card
                              .delete()
                              .then(() => res.json({result: 'ok'}))
                            ;

                          })
                        ;

                      }

                    })
                  ;

                }

              })
            ;

          } else res.json({error: 'Authentification required'});

        })
      ;
    }

    removeAccount(req, res, next) {

      this
        .authenticate(req)
        .then(user => {

          if(user) {

            const idStr = req.params.id || '';

            if(idStr.length !== 12 && idStr.length !== 24) {
              res.json({error: 'Wrong user id'});
              return;
            }

            const id         = HZBankAccount.id(idStr);
            const _autoWires = [];

            HZBankAccount
              .findOne({_id: id, active: true})
              .then(account => {

                if(account) {

                  account.active = false;

                  async.parallel([

                    cb => {

                      HZBankCard
                        .find({account: id})
                        .then(cards => {

                          const promises = [];

                          cards.forEach(e => {
                            promises.push(e.delete());
                          });

                          Promise
                            .all(promises)
                            .then(() => cb(null))
                          ;

                        })
                      ;

                    },

                    cb => {

                      HZBankAutoWire
                        .find({
                          $or: [
                            {sender  : id},
                            {receiver: id},
                          ]
                        })
                        .then(autoWires => {

                          const promises = [];

                          autoWires.forEach(e => {
                            e.active = false;
                            promises.push(e.save());
                            _autoWires.push(e._id);
                          });

                          Promise
                            .all(promises)
                            .then(() => cb(null))
                          ;

                        })
                      ;

                    }

                  ], (err, results) => {

                    HZBankLoan
                      .find({autoWire: {$in: _autoWires}})
                      .then(loans => {

                        const promises = [];

                        loans.forEach(e => {
                          e.active = false;
                          promises.push(e.save());
                        });

                        Promise
                          .all(promises)
                          .then(() => {

                            account
                              .save()
                              .then(() => res.json({result: 'ok'}))
                            ;
                            
                          })
                        ;

                      })
                    ;

                  });

                } else res.json({error: 'Can\'t get account'});
              })
            ;
          } else res.json({error: 'Authentification required'});

        })
      ;
    }

    removeAutoWire(req, res, next) {

      this
        .authenticate(req)
        .then(user => {

          if(user) {

            const idStr = req.params.id || '';

            if(idStr.length !== 12 && idStr.length !== 24) {
              res.json({error: 'Wrong user id'});
              return;
            }

            HZBankAutoWire
              .findOne({_id: HZBankAutoWire.id(idStr)})
              .then(autoWire => {

                if(autoWire !== null) {

                  autoWire
                    .delete()
                    .then(() => res.json({result: 'ok'}))
                  ;

                }

              })
            ;

          } else res.json({error: 'Authentification required'});

        })
      ;
    }

  }

  return new BankRouter();

};
