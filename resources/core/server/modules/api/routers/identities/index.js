'use strict';

module.exports = function(horizon, coreIdentityModule) {

  const express = require('express');
  const HZLog = horizon.require('log');

  class IdentityRouter {

    constructor() {
      this.router_ = express.Router();

      this.router_.param('id', function(req, res, next, id) {
        req.param._id = id;
        next();
      });

      this.router_.param('server', function(req, res, next, server) {
        req.param._server = server;
        next();
      });


      this.router_.get('/', this.getList_.bind(this));
      this.router_.get('/light', this.getListLight_.bind(this));
      this.router_.get('/connected', this.getListConnected_.bind(this));
      this.router_.post('/', this.add_.bind(this));
      this.router_.delete('/:id', this.remove_.bind(this));
      this.router_.put('/:id', this.update_.bind(this));
      this.router_.get('/:id', this.get_.bind(this));
      this.router_.post('/:server/:id/kick', this.kick_.bind(this));

      return this.router_;
    }

    getListLight_(req, res, next) {
      coreIdentityModule.find({}).then((list) => {
        if (list) {
          res.json(list.map((item) => {
            return item.toStructure()
          }));
        } else {
          res.json({
            error: "Can't get list"
          });
        }
      });
    }

    getList_(req, res, next) {
      coreIdentityModule.find({}).then((list) => {
        if (list) {
          res.json(list.map((item) => {
            return item.toObject()
          }));
        } else {
          res.json({
            error: "Can't get list"
          });
        }
      });
    }

    getListConnected_(req, res, next) {
      res.json(horizon.getConnectedClientList().filter((item) => item.identityId != null));
    }

    add_(req, res, next) {
      if (req.body && req.body.name) {
        coreIdentityModule.findOrCreate({
          name: req.body.name
        }, req.body).then((entity, exist) => {
          if (!exist)
            HZLog(HZLog.ADDIDENTITY, req.param.user, new Date(), req.body);
          res.json(exist ? {
            error: "Identifier already exist"
          } : {
            result: "ok"
          })
        });
      }
    }

    remove_(req, res, next) {
      if (req.body && req.param._id) {
        coreIdentityModule.findOne({
          _id: coreIdentityModule.id(req.param._id)
        }).then((entity, exist) => {
          if (entity) {
            HZLog(HZLog.REMOVEIDENTITY, req.param.user, new Date(), entity);
            entity.delete();
            res.json({
              result: "ok"
            });
          } else {
            res.json({
              error: "Identity do not exist"
            });
          }
        });
      }
    }

    update_(req, res, next) {
      if (req.body && req.param._id) {
        coreIdentityModule.findOne({
          _id: coreIdentityModule.id(req.param._id)
        }).then((entity, exist) => {
          if (entity) {
            
            HZLog(HZLog.UPDATEIDENTITY, req.param.user, new Date(), entity, req.body);
            
            entity.name = req.body.name;
            entity.health = req.body.health;
            entity.armour = req.body.armour;
            entity.roles = req.body.roles;
            entity.stamina = req.body.stamina;
            entity.strengh = req.body.strengh;
            entity.lung_capacity = req.body.lung_capacity;
            entity.wheelie_capacity = req.body.wheelie_capacity;
            entity.flying_ability = req.body.flying_ability;
            entity.shooting_ability = req.body.shooting_ability;
            entity.stealth_ability = req.body.stealth_ability;
            
            const client = horizon.clients.find(e => coreIdentityModule.id(req.param._id).equals(e.identityId)) || null;
            
            if(client !== null) {
              client.triggerEvent("horizon:alterIdentityData", "armour", "set", req.body.armour);
              client.triggerEvent("horizon:alterIdentityData", "health", "set", req.body.health);
              client.triggerEvent("horizon:alterIdentityData", "roles", "set", req.body.roles);
              client.triggerEvent("horizon:alterIdentityData", "stamina", "set", req.body.stamina);
              client.triggerEvent("horizon:alterIdentityData", "strengh", "set", req.body.strengh);
              client.triggerEvent("horizon:alterIdentityData", "lung_capacity", "set", req.body.lung_capacity);
              client.triggerEvent("horizon:alterIdentityData", "wheelie_capacity", "set", req.body.wheelie_capacity);
              client.triggerEvent("horizon:alterIdentityData", "flying_ability", "set", req.body.flying_ability);
              client.triggerEvent("horizon:alterIdentityData", "shooting_ability", "set", req.body.shooting_ability);
              client.triggerEvent("horizon:alterIdentityData", "stealth_ability", "set", req.body.stealth_ability);
            }

            entity.save();
            res.json({
              result: "ok"
            });
          } else {
            res.json({
              error: "Identity do not exist"
            });
          }
        });
      }
    }

    get_(req, res, next) {
      if (req.body && req.param._id) {
        coreIdentityModule.findOne({
          _id: coreIdentityModule.id(req.param._id)
        }).then((entity, exist) => {
          if (entity) {
            res.json(entity.toObject());
          } else {
            res.json({
              error: "Identity do not exist"
            });
          }
        });
      }
    }

    kick_(req, res, next) {
      if (req.param._id) {
        let client = horizon.getClientBySourceAndServerName(req.param._server, req.param._id);
        if (client) {
          client.kick('But why ?');
          res.json({
            result: "ok"
          });
        } else {
          res.json({
            error: "Client is not connected"
          });
        }
      }
    }
  }

  return new IdentityRouter();

};
