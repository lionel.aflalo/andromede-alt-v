'use strict';

module.exports = function(horizon, coreBlipModule) {

  const express = require('express');
  const HZPosition = horizon.require('position');
  const HZLog = horizon.require('log');

  class BlipRouter {

    constructor() {
      this.router_ = express.Router();

      this.router_.param('id', function(req, res, next, id) {
        req.param._id = id;
        next();
      });

      this.router_.get('/', this.getList_.bind(this));
      this.router_.post('/', this.add_.bind(this));
      this.router_.delete('/:id', this.remove_.bind(this));
      this.router_.put('/:id', this.update_.bind(this));

      return this.router_;
    }

    getList_(req, res, next) {
      coreBlipModule.find({}).then((list) => {
        if (list) {
          res.json(list.map((item) => {
            return item.toObject()
          }));
        } else {
          res.json({
            error: "Can't get list"
          });
        }
      });
    }

    add_(req, res, next) {
      if (req.body && req.body.name) {
        req.body._type = "Blip";
        HZPosition.findOrCreate({
          _id: coreBlipModule.id(req.body.position)
        }).then((position, exist) => {
          req.body.position = position;
          coreBlipModule.findOrCreate({
            name: req.body.name,
            position: position
          }, req.body).then((entity, exist) => {
            this.refreshBlip();
            if (!exist)
              HZLog(HZLog.ADDBLIP, req.param.user, req.body);
            res.json(exist ? {
              error: "Blip already exist"
            } : {
              result: "ok"
            })
          });
        })
      }
    }

    remove_(req, res, next) {
      if (req.param._id) {
        coreBlipModule.findOne({
          _id: coreBlipModule.id(req.param._id)
        }).then((entity) => {
          if (entity) {
            HZLog(HZLog.REMOVEBLIP, req.param.user, entity);
            entity.delete();
            res.json({
              result: "ok"
            });
            this.refreshBlip();
          } else {
            res.json({
              error: "Blip do not exist"
            });
          }
        });
      }
    }

    update_(req, res, next) {
      if (req.body && req.param._id) {
        coreBlipModule.findOne({
          _id: coreBlipModule.id(req.param._id)
        }).then((entity) => {
          if (entity) {
            HZPosition.findOrCreate({
              _id: coreBlipModule.id(req.body.position)
            }).then((position, exist) => {
              HZLog(HZLog.UPDATEBLIP, req.param.user, entity, req.body);
              entity.name = req.body.name;
              entity.sprite = req.body.sprite;
              entity.display = req.body.display;
              entity.spriteColor = req.body.spriteColor;
              entity.group = req.body.group;
              entity.position = position
              entity.save();
              res.json({
                result: "ok"
              });
              this.refreshBlip();
            });
          } else {
            res.json({
              error: "Blip do not exist"
            });
          }
        });
      }
    }


    refreshBlip() {
      setTimeout(() => horizon.getConnectedClients().map((client) => {
        client.refreshBlips()
      }), 2000);
    }
  }

  return new BlipRouter();

};
