'use strict';

module.exports = function(horizon, corePlayerModule,authRouting) {

  const express = require('express');
  const HZLog = horizon.require('log');

  class PlayerRouter {

    constructor() {
      this.router_ = express.Router();

      this.router_.param('id', function(req, res, next, id) {
        req.param._id = id;
        next();
      });


      this.router_.param('server', function(req, res, next, server) {
        req.param._server = server;
        next();
      });

      this.router_.get('/nbconnected', this.getNbConnected_.bind(this));
      this.router_.get('/', authRouting, this.getList_.bind(this));
      this.router_.get('/light', authRouting, this.getListLight_.bind(this));
      this.router_.get('/connected', this.getListConnected_.bind(this));
      this.router_.post('/', authRouting, this.add_.bind(this));
      this.router_.delete('/:id', authRouting, this.remove_.bind(this));
      this.router_.put('/:id', authRouting, this.update_.bind(this));
      this.router_.get('/:id', authRouting, this.get_.bind(this));
      this.router_.post('/:server/:id/kick', authRouting, this.kick_.bind(this));
      this.router_.post('/:server/:id/revive', authRouting, this.revive_.bind(this));
      this.router_.post('/:server/:id/kill', authRouting, this.kill_.bind(this));

      return this.router_;
    }

    getListLight_(req, res, next) {
      corePlayerModule.find({}).then((list) => {
        if (list) {
          res.json(list.map((item) => {
            return item.toStructure()
          }));
        } else {
          res.json({
            error: "Can't get list"
          });
        }
      });
    }

    getList_(req, res, next) {
      corePlayerModule.find({}).then((list) => {
        if (list) {
          res.json(list.map((item) => {
            return item.toObject()
          }));
        } else {
          res.json({
            error: "Can't get list"
          });
        }
      });
    }

    getListConnected_(req, res, next) {
      res.json(horizon.getConnectedClientList());
    }

    getNbConnected_(req, res, next) {
      let arr = horizon.getConnectedClientList().filter((item) => { return item.identityName != null && item.identityName.length > 1});
      res.json(arr.length);
    }

    add_(req, res, next) {
      if (req.body && req.body.identifier) {
        corePlayerModule.findOrCreate({
          'identifiers.steam' : req.body.identifier
        }, req.body).then(entity => {
          res.json({
            result: "ok"
          })
        });
      }
    }

    remove_(req, res, next) {
      if (req.body && req.param._id) {
        corePlayerModule.findOne({
          _id: corePlayerModule.id(req.param._id)
        }).then((entity, exist) => {
          if (entity) {
            HZLog(HZLog.REMOVEPLAYER, req.param.user, entity);
            entity.delete();
            res.json({
              result: "ok"
            });
          } else {
            res.json({
              error: "Player do not exist"
            });
          }
        });
      }
    }

    update_(req, res, next) {
      if (req.body && req.param._id) {
        corePlayerModule.findOne({
          _id: corePlayerModule.id(req.param._id)
        }).then((entity, exist) => {
          if (entity) {
            HZLog(HZLog.UPDATEPLAYER, req.param.user, entity, req.body);
            entity.name = req.body.name;
            entity.identifiers = req.body.identifiers;
            entity.roles = req.body.roles;
            entity.save();
            res.json({
              result: "ok"
            });
          } else {
            res.json({
              error: "Player do not exist"
            });
          }
        });
      }
    }

    get_(req, res, next) {
      if (req.body && req.param._id) {
        corePlayerModule.findOne({
          _id: corePlayerModule.id(req.param._id)
        }).then((entity, exist) => {
          if (entity) {
            res.json(entity.toObject());
          } else {
            res.json({
              error: "Player do not exist"
            });
          }
        });
      }
    }

    kick_(req, res, next) {
      if (req.param._id) {
        let client = horizon.getClientBySourceAndServerName(req.param._server, req.param._id);
        if (client) {
          HZLog(HZLog.KICKPLAYER, req.param.user, client);
          client.kick('But why ?');
          res.json({
            result: "ok"
          });
        } else {
          res.json({
            error: "Client is not connected"
          });
        }
      }
    }
    
    revive_(req, res, next) {
      if (req.param._id) {
        let client = horizon.getClientBySourceAndServerName(req.param._server, req.param._id);
        if (client) {
          HZLog(HZLog.REVIVEPLAYER, req.param.user, client);
          client.revive();
          res.json({
            result: "ok"
          });
        } else {
          res.json({
            error: "Client is not connected"
          });
        }
      }
    }

    kill_(req, res, next) {
      if (req.param._id) {
        let client = horizon.getClientBySourceAndServerName(req.param._server, req.param._id);
        if (client) {
          HZLog(HZLog.KILLPLAYER, req.param.user, client);
          client.kill();
          res.json({
            result: "ok"
          });
        } else {
          res.json({
            error: "Client is not connected"
          });
        }
      }
    }
  }

  return new PlayerRouter();

};
