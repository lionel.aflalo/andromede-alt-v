'use strict';

const async  = require('async');
const crypto = require('crypto');
const atob   = require('atob');

module.exports = function(horizon, HZBank) {

  const express              = require('express');
  const HZLog               = horizon.require('log');
  const HZContainer         = horizon.require('container');
  const HZBankUser          = horizon.require('bank/user');
  const HZStandardBankUser  = horizon.require('bank/user/standard');
  const HZProBankUser       = horizon.require('bank/user/pro');
  const HZBankAccount       = horizon.require('bank/account');
  const HZBankTransaction   = horizon.require('bank/transaction');
  const HZBankAutoWire      = horizon.require('bank/autowire');
  const HZBankLoan          = horizon.require('bank/loan');
  const HZBankLoanFormulas  = horizon.require('bank/loan/formulas');
  const HZBankAccountLimits = horizon.require('bank/account/limits');
  const HZBankCard          = horizon.require('bank/card'); 

  class BankRouter {

    constructor() {

      this.router = express.Router();

      this.router.param('id', function(req, res, next, id) {
        req.param._id = id;
        next();
      });

      // GET
      this.router.get('/user/:id',                this.getUser        .bind(this));
      this.router.get('/account/:id',             this.getAccount     .bind(this));
      this.router.get('/user.accounts',           this.getUserAccounts.bind(this));
      this.router.get('/transactions/:accountid', this.getTransactions.bind(this));

      // POST
      this.router.post('/auth',     this.authUser.bind(this));
      this.router.post('/transfer', this.transfer.bind(this));

      return this.router;
    }

    authenticate(req) {

      return new Promise((resolve, reject) => {

        if(!req.headers.authorization) {
          resolve(null);
          return;
        }

        let credentials = atob(req.headers.authorization.split(' ')[1]).split(':');

        if(credentials[1] === null)
          resolve(null);

        HZBankUser
          .findOne({
            login : credentials[0],
            token : credentials[1],
            active: true
          }).then((user) => {
            
            if(user) {
              
              if(user.token === credentials[1])
                resolve(user);
              else
                resolve(null);

            } else resolve(null);

          })
        ;

      });

    }

    // GET
    getUser(req, res, next) {

      this
        .authenticate(req)
        .then(user => {

          if(user) {

            const idStr = req.params.id || '';

            if(idStr.length !== 12 && idStr.length !== 24) {
              res.json({error: 'Wrong user id'});
              return;
            }

            HZBankUser
              .findOne({_id: HZBankUser.id(idStr), active: true})
              .then(user => {
                if(user)
                  res.json(user.toObject());
                else
                  res.json({error: 'Can\'t get user'});
              })
            ;

          } else res.json({error: 'Authentification required'});

        })
      ;
    }

    getUserAccounts(req, res, next) {

      this
        .authenticate(req)
        .then(user => {

          if(user) {

            HZBankUser
              .findOne({_id: user._id, active: true})
              .then(user => {
                
                if(user) {
                  
                  HZBankAccount
                    .find({_id: {$in: user.accounts}, active: true})
                    .then(accounts => {
                      res.json(accounts.map(e => e.toObject()));
                    })
                  ;

                } else res.json({error: 'Can\'t get user'});

              })
            ;

          } else res.json({error: 'Authentification required'});

        })
      ;
    }

    getAccount(req, res, next) {

      this
        .authenticate(req)
        .then(user => {

          if(user) {

            const idStr = req.params.id || '';

            if(idStr.length !== 12 && idStr.length !== 24) {
              res.json({error: 'Wrong account id'});
              return;
            }

            const id  = HZBankAccount.id(idStr);
            const idx = user.accounts.findIndex(e => e.equals(id));

            if(idx === -1) {
              res.json({error: 'Wrong account ownsership'});
              return;
            }

            HZBankAccount
              .findOne({_id: id})
              .then(account => {
                if(account)
                  res.json(account.toObject());
                else
                  res.json({error: 'Can\'t get account'});
              })
            ;

          } else res.json({error: 'Authentification required'});

        })
      ;
    }

    getTransactions(req, res, next) {

      this
        .authenticate(req)
        .then(user => {

          if(user) {

            const idStr = req.params.accountid || '';

            if(idStr.length !== 12 && idStr.length !== 24) {
              res.json({error: 'Wrong account id'});
              return;
            }

            const id  = HZBankAccount.id(idStr);
            const idx = user.accounts.findIndex(e => e.equals(id));

            if(idx === -1) {
              res.json({error: 'Wrong account ownsership'});
              return;
            }

            HZBankTransaction
              .find({
                $or: [
                  {sender  : id},
                  {receiver: id},
                ]
              },)
              .then(transactions => {
                transactions = transactions.slice(0).sort((a,b) => b.timestamp - a.timestamp).splice(0, 10);
                res.json(transactions.map(e => e.toObject()));
              })
            ;
            
          } else res.json({error: 'Authentification required'});

        })
      ;
    }

    // POST
    authUser(req, res, next) {

      if(req.body && req.body.login && req.body.password) {

        HZBankUser
          .findOne({
            login    : req.body.login,
            password : req.body.password,
            active   : true
          })
          .then(user => {

            if(user !== null) {

              user.token = crypto.randomBytes(16).toString('hex');

              user
                .save()
                .then(() => res.json({
                  user : user.toObject(),
                  token: user.token,
                }))
              ;

            } else res.json({error: 'Bad credentials'});

          })
        ;

      } else res.json({error: 'Missing fields'});
    }

    transfer(req, res, next) {

      this
        .authenticate(req)
        .then(user => {

          if(user) {

            if(
              typeof req.body           !== 'undefined' &&
              typeof req.body.sender    !== 'undefined' &&
              typeof req.body.receiver  !== 'undefined' &&
              typeof req.body.amount    !== 'undefined'
            ) {

              const senderIdStr = req.body.sender || '';

              if(senderIdStr.length !== 12 && senderIdStr.length !== 24) {
                res.json({error: 'Wrong sender id'});
                return;
              }

              const object    = null;
              const initiator = user._id;

              HZBankAccount
                .findOne({_id: HZBankAccount.id(senderIdStr)})
                .then(senderAccount => {

                  if(senderAccount === null) {
                    res.json({error: 'Sender not found'});
                    return;
                  }

                  HZBankAccount
                    .findOne({number: req.body.receiver})
                    .then(receiverAccount => {

                      if(receiverAccount === null) {
                        res.json({error: 'Receiver not found'});
                        return;
                      }

                      senderAccount
                        .tryTransfer(receiverAccount, req.body.amount, object, initiator)
                        .then(success => {

                          if(success)
                            res.json({result: 'ok'});
                          else
                            res.json({error: 'Can\'t make transfer'})

                        })
                      ;

                    })
                  ;

                })
              ;


            } else res.json({error: 'Missing fields'});
            
          } else res.json({error: 'Authentification required'});

        })
      ;
    }

  }

  return new BankRouter();

};
