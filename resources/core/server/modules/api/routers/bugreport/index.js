'use strict';

const { Attachment } = require('discord.js');

module.exports = function(horizon) {

  const express     = require('express');
  const Utils       = horizon.require('utils');
  const HZLog      = horizon.require('log');
  const HZPlayer   = horizon.require('player');
  const HZIdentity = horizon.require('identity');
  const HZVehicle  = horizon.require('vehicle');

  class BugReportRouter {

    constructor() {

      this.router = express.Router();

      this.router.post('/upload', this.uploadReport.bind(this));

      return this.router;
    }

    uploadReport(req, res, next) {

      const { body, log, player, identity } = req.body;
      const screenshots                     = req.files ? (req.files['screenshots[]'] instanceof Array ? req.files['screenshots[]'] : [req.files['screenshots[]']]) : [];
      const playerData                      = JSON.parse(player);
      const identityData                    = JSON.parse(identity);
      
      horizon.modules.plugin_discord.client.channels.get(horizon.modules.plugin_discord.config.bugs).send(identityData.name + ' (' + playerData.name + ')');
      horizon.modules.plugin_discord.client.channels.get(horizon.modules.plugin_discord.config.bugs).send('```' + body + '```');

      horizon.modules.plugin_discord.client.channels.get(horizon.modules.plugin_discord.config.bugs).send('', new Attachment(Buffer.from(log, 'utf8'), 'CitizenFX.log'));

      screenshots.map((e,i) => {
        horizon.modules.plugin_discord.client.channels.get(horizon.modules.plugin_discord.config.bugs).send('', new Attachment(e.data, i + '.png'));
      });

      horizon.modules.plugin_discord.client.channels.get(horizon.modules.plugin_discord.config.bugs).send('', new Attachment(Buffer.from(JSON.stringify(playerData, null, 2),   'utf8'), 'player.json'));
      horizon.modules.plugin_discord.client.channels.get(horizon.modules.plugin_discord.config.bugs).send('', new Attachment(Buffer.from(JSON.stringify(identityData, null, 2), 'utf8'), 'identity.json'));

      res.json({success: true});

    }

  }

  return new BugReportRouter();

};
