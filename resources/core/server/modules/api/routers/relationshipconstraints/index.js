'use strict';

module.exports = function(horizon, HZRelationshipConstraint) {

  const express = require('express');
  const HZLog  = horizon.require('log');

  class RelationshipConstraintRouter {

    constructor() {

      this.router = express.Router();

      this.router.param('id', function(req, res, next, id) {
        req.param._id = id;
        next();
      });

      this.router.get   ('/',    this.getList.bind(this));
      this.router.post  ('/',    this.add    .bind(this));
      this.router.delete('/:id', this.remove .bind(this));
      this.router.put   ('/:id', this.update .bind(this));

      return this.router;
    }

    getList(req, res, next) {

      HZRelationshipConstraint.find({}).then((list) => {
        if (list)
          res.json(list.map(item => item.toObject()));
        else
          res.json({error: 'Can\'t get list'});
      });

    }

    add(req, res, next) {

      if(
        typeof req.body              != 'undefined' &&
        typeof req.body.group1       != 'undefined' &&
        typeof req.body.group2       != 'undefined' &&
        typeof req.body.relationship != 'undefined'
      ) {

        HZRelationshipConstraint
          .findOrCreate({
            group1      : HZRelationshipConstraint.id(req.body.group1),
            group2      : HZRelationshipConstraint.id(req.body.group2),
            relationship: HZRelationshipConstraint.id(req.body.relationship),
          })
          .then((relationshipConstraint, exists) => {

            if(exists) {

              res.json({error: 'RelationshipConstraint already exists'})

            } else {

              HZLog(HZLog.ADDRELATIONSHIPCONSTRAINT, req.param.user, new Date(), req.body);
              res.json({result: 'ok'})
            }

          })
        ;
      }
    }

    remove(req, res, next) {

      if(req.param._id) {

        HZRelationshipConstraint
          .findOne({_id: HZRelationshipConstraint.id(req.param._id)})
          .then(relationshipConstraint => {

            if (relationshipConstraint) {

              HZLog(HZLog.REMOVERELATIONSHIPCONSTRAINT, req.param.user, new Date(), relationshipConstraint);
              relationshipConstraint.delete();
              res.json({result: 'ok'});

            } else res.json({error: 'RelationshipConstraint does not exists'});

          })
        ;

      }
    }

    update(req, res, next) {

      if (typeof req.body != 'undefined' && typeof req.param._id != 'undefined') {

        HZRelationshipConstraint
          .findOne({_id: HZRelationshipConstraint.id(req.param._id)})
          .then((relationshipConstraint) => {

            if (relationshipConstraint) {

              HZLog(HZLog.UPDATERELATIONSHIPCONSTRAINT, req.param.user, new Date(), relationshipConstraint, req.body);

              relationshipConstraint.group1       = HZRelationshipConstraint.id(req.body.group1);
              relationshipConstraint.group2       = HZRelationshipConstraint.id(req.body.group2);
              relationshipConstraint.relationship = HZRelationshipConstraint.id(req.body.relationship);

              relationshipConstraint.save().then(() => res.json({result: 'ok'}));

            } else res.json({error: 'RelationshipConstraint does not exists'});

          })
        ;
      }

    }
  }

  return new RelationshipConstraintRouter();

};
