'use strict';

module.exports = function(horizon, steamApi) {

  const express = require('express');
  const HZLog = horizon.require('log');
  const Utils = horizon.require('utils');

  const HZPlayer = horizon.require('player');

  class LauncherRouter {

    constructor() {

      this.router = express.Router();

      this.router.post('/', this.post.bind(this));

      return this.router;
    }

    post(req, res, next) {

      if (req.body.ticket) {

        steamApi.ISteamUserAuth.AuthenticateUserTicket({
          appid: 218,
          ticket: req.body.ticket
        }).then((body) => {

          if (body.response.params.result == 'OK')
            res.json({});
          else
            res.json({
              error: 'Bad steam ticket'
            });

        });

      } else res.status(403).json({
        error: 'Bad authentication'
      });

    }

  }

  return new LauncherRouter();

};
