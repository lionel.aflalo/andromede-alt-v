'use strict';

module.exports = function(horizon, steamApi) {

  const express     = require('express');
  const Utils       = horizon.require('utils');
  const HZLog      = horizon.require('log');
  const HZPlayer   = horizon.require('player');
  const HZIdentity = horizon.require('identity');
  const HZVehicle  = horizon.require('vehicle');

  class LauncherRouter {

    constructor() {

      this.router = express.Router();

      this.router.get('/servers',    this.getServers   .bind(this));
      this.router.get('/identities', this.getIdentities.bind(this));

      return this.router;
    }

    getSteamId(req) {

      return steamApi.ISteamUserAuth.AuthenticateUserTicket({
        appid : 218,
        ticket: req.headers.steamticket
      }).then((body) => {

        if (body.response.params.result == 'OK') {

          const steamId = Utils.dec2hex(body.response.params.steamid);
          
          return Promise.resolve(steamId);
        
        } else return Promise.resolve(null);

      });

    }

    getServers(req, res, next) {
	  
	  	res.setHeader('Cache-Control', 'no-cache');

			this.getSteamId(req).then((steamId) => {

        HZPlayer
          .findOne({
            'identifiers.steam': steamId,
            roles: {$nin:  ['alien']}
          })
          .then(player => {
            
            if(player == null) {

              res.status(403).json({error: 'Not whitelisted'});
            
            } else {

              let results = [];

              if(player.identity !== null)
 								results = player.identity.queryItem({_type: 'VehicleKey'});

          		const _targets = results.map(e => e.item.targets);
          		let   targets  = [];

          		for(let i=0; i<_targets.length; i++)
          			targets = targets.concat(_targets[i]);

          		HZVehicle
          			.findOfType({$or: targets})
          			.then(vehicles => {

							  	const servers = horizon.config.fxservers.map(e => {

							  		const server       = horizon.servers.find(e2 => e.name === e2.name);
							  		let   vehicleCount = 0;

							  		for(let i=0; i<vehicles.length; i++)
							  			if(vehicles[i]._data && vehicles[i]._data.server && vehicles[i]._data.server.host === e.host && vehicles[i]._data.server.port === e.port)
							  				vehicleCount++;

							  		return {
								  		label       : e.label,
								  		host        : e.externalHost,
								  		port        : e.port,
								  		playerCount : horizon.clients.filter(e => e.server === server).length,
								  		maxPlayers  : horizon.config.maxPlayers,
								  		vehicleCount: vehicleCount,
								  	}

							  	}).filter(e => e.playerCount < e.maxPlayers);

						    	res.json(servers);

          			})
          		;

            }

          })
        ;

      });

    }

    getIdentities(req, res, next) {

      this.getSteamId(req).then((steamId) => {

        HZPlayer
          .findOne({
            'identifiers.steam': steamId,
            roles: {$nin:  ['alien']}
          })
          .then(player => {
            
            if(player == null) {

              res.status(403).json({error: 'Not whitelisted'});
            
            } else {

              const identities = player.toObject().identities;
              
              res.json(identities);
            }

          })
        ;

      });

    }

  }

  return new LauncherRouter();

};
