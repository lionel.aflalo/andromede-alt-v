'use strict';

module.exports = function(horizon, HZRelationship) {

  const express = require('express');
  const HZLog  = horizon.require('log');

  class RelationshipRouter {

    constructor() {

      this.router = express.Router();

      this.router.param('id', function(req, res, next, id) {
        req.param._id = id;
        next();
      });

      this.router.get   ('/',    this.getList.bind(this));
      this.router.post  ('/',    this.add    .bind(this));
      this.router.delete('/:id', this.remove .bind(this));
      this.router.put   ('/:id', this.update .bind(this));

      return this.router;
    }

    getList(req, res, next) {

      HZRelationship.find({}).then((list) => {
        if (list)
          res.json(list.map(item => item.toObject()));
        else
          res.json({error: 'Can\'t get list'});
      });

    }

    add(req, res, next) {

      if(typeof req.body != 'undefined' && typeof req.body.name != 'undefined') {

        HZRelationship
          .findOrCreate({name: req.body.name}, req.body)
          .then((relationship, exists) => {

            if(exists) {

              res.json({error: 'Relationship already exists'})

            } else {

              HZLog(HZLog.ADDRELATIONSHIP, req.param.user, new Date(), req.body);
              res.json({result: 'ok'})
            }

          })
        ;
      }
    }

    remove(req, res, next) {

      if(req.param._id) {

        HZRelationship
          .findOne({_id: HZRelationship.id(req.param._id)})
          .then(relationship => {

            if (relationship) {

              HZLog(HZLog.REMOVERELATIONSHIP, req.param.user, new Date(), relationship);
              relationship.delete();
              res.json({result: 'ok'});

            } else res.json({error: 'Relationship does not exists'});

          })
        ;

      }
    }

    update(req, res, next) {

      if (typeof req.body != 'undefined' && typeof req.param._id != 'undefined') {

        HZRelationship
          .findOne({_id: HZRelationship.id(req.param._id)})
          .then((relationship) => {

            if (relationship) {

              HZLog(HZLog.UPDATERELATIONSHIP, req.param.user, new Date(), relationship, req.body);
              relationship.name = req.body.name;
              relationship.save().then(() => res.json({result: 'ok'}));

            } else res.json({error: 'Relationship does not exists'});

          })
        ;
      }

    }
  }

  return new RelationshipRouter();

};
