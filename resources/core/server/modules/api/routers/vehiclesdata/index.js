'use strict';

module.exports = function(horizon, coreVehicleDataModule) {

	const express      = require('express');
    const HZLog = horizon.require('log');

	class VehicleDataRouter {

		constructor() {
		  	this.router_ = express.Router();

			this.router_.param('id', function (req, res, next, id) {
			  req.param._id = id;
			  next();
			});

			this.router_.get('/', this.getList_.bind(this));
			this.router_.post('/', this.add_.bind(this));
			this.router_.delete('/:id', this.remove_.bind(this));
			this.router_.put('/:id', this.update_.bind(this));

			return this.router_;
		}

		getList_(req, res, next) {
            coreVehicleDataModule.find({}).then((list) => {
            	if (list) {
               		res.json(list.map((item) => { return item.toObject() }));
            	} else {
                    res.json({error: "Can't get list"});
            	}
            });
         }

        add_(req, res, next) {
	        if (req.body && req.body.name) {
                req.body._type = "VehicleData";
                coreVehicleDataModule.findOrCreate({name: req.body.name}, req.body).then((entity, exist) => {
                if (!exist)
                    HZLog(HZLog.ADDVEHICLEDATA, req.param.user, req.body);
                res.json(exist ? {error: "Vehicle data already exist"} : {result: "ok"})
                });
	        }
        }

        remove_(req, res, next) {
	        if (req.param._id) {
                coreVehicleDataModule.findOne({_id: coreVehicleDataModule.id(req.param._id)}).then((entity) => {
                    if (entity) {
                        HZLog(HZLog.REMOVEVEHICLEDATA, req.param.user, entity);
                    	entity.delete();
                    	res.json({result: "ok"});
                    }else {
                    	res.json({error: "Vehicle data do not exist"});
                    }
                });
	        }
        }

        update_(req, res, next) {
            if (req.body && req.param._id) {
                coreVehicleDataModule.findOne({_id: coreVehicleDataModule.id(req.param._id)}).then((entity) => {
                    if (entity) {
                        HZLog(HZLog.UPDATEVEHICLEDATA, req.param.user, entity, req.body);
                        entity.name = req.body.name;
                        entity.description = req.body.description;
                        entity.category = req.body.category;
                        entity.nbdoor = req.body.nbdoor;
                        entity.weight = req.body.weight;
                        entity.volumeX = req.body.volumeX;
                        entity.volumeY = req.body.volumeY;
                        entity.volumeZ = req.body.volumeZ;
                        entity.trunklocation = req.body.trunklocation;
                        entity.fueltankcapacity = req.body.fueltankcapacity;
                        entity.fueltype = req.body.fueltype;
                        entity.fuelconsumption = req.body.fuelconsumption;
                        entity.price = req.body.price;
                        entity.maxspeed = req.body.maxspeed;
                        entity.save();
                        res.json({result: "ok"});
                    } else {
                    	res.json({error: "Vehicle data do not exist"});
                    }
                });
            }
        }
	}

	return new VehicleDataRouter();

};
