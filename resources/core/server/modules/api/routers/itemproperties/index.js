'use strict';

module.exports = function(horizon, coreItemPropertiesModule) {

  const express = require('express');
  const HZLog = horizon.require('log');

  class ItemPropertiesRouter {

    constructor() {
      this.router_ = express.Router();

      this.router_.param('id', function(req, res, next, id) {
        req.param._id = id;
        next();
      });

      this.router_.get('/', this.getList_.bind(this));
      this.router_.post('/', this.add_.bind(this));
      this.router_.delete('/:id', this.remove_.bind(this));
      this.router_.put('/:id', this.update_.bind(this));

      return this.router_;
    }

    getList_(req, res, next) {
      coreItemPropertiesModule.find({}).then((list) => {
        if (list) {
          res.json(list.map((item) => {
            return item.toObject()
          }));
        } else {
          res.json({
            error: "Can't get list"
          });
        }
      });
    }

    add_(req, res, next) {
      if (req.body && req.body.name) {
        req.body._type = "ItemProperties";
        coreItemPropertiesModule.findOrCreate({
          name: req.body.name
        }, req.body).then((entity, exist) => {
          if (!exist)
            HZLog(HZLog.ADDITEM, req.param.user, req.body);
          res.json(exist ? {
            error: "ItemProperties already exist"
          } : {
            result: "ok"
          })
        });
      }
    }

    remove_(req, res, next) {
      if (req.param._id) {
        coreItemPropertiesModule.findOne({
          _id: coreItemPropertiesModule.id(req.param._id)
        }).then((entity) => {
          if (entity) {
            HZLog(HZLog.REMOVEITEM, req.param.user, entity);
            entity.delete();
            res.json({
              result: "ok"
            });
          } else {
            res.json({
              error: "ItemProperties do not exist"
            });
          }
        });
      }
    }

    update_(req, res, next) {
      if (req.body && req.param._id) {
        coreItemPropertiesModule.findOne({
          _id: coreItemPropertiesModule.id(req.param._id)
        }).then((entity) => {
          if (entity) {
            HZLog(HZLog.UPDATEITEM, req.param.user, entity, req.body);
            entity.name = req.body.name;
            entity.mass = req.body.mass;
            entity.sizex = req.body.sizex;
            entity.sizey = req.body.sizey;
            entity.sizez = req.body.sizez;
            entity.removeable = req.body.removeable;
            entity.giveable = req.body.giveable;
            entity.compressible = req.body.compressible;
            entity.save();
            res.json({
              result: "ok"
            });
          } else {
            res.json({
              error: "ItemProperties do not exist"
            });
          }
        });
      }
    }
  }

  return new ItemPropertiesRouter();

};
