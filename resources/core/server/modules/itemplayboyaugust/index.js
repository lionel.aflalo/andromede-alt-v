module.exports = (horizon, config) => {
   
  const HZItem = horizon.require('item');

  class HZItemPlayboyaugust extends HZItem {

  }

  HZItemPlayboyaugust
    .inheritFields(HZItem)
    .inheritActions(HZItem)
    .field('_type', {default: 'ItemPlayboyaugust'})
    .field('image', {default: 'playboyaugust'})
    .field('shape', {default: {"grid":[],"offset":{"x":5.000000000000014,"y":29.03414096916299}}})
    .field('name',  {default: 'itemplayboyaugust'})
    .field('label', {default: 'Playboy Août'})
  ;

  return HZItemPlayboyaugust;

}