module.exports = (horizon, config) => {
     
    const HZItem = horizon.require('item');
  
    class HZItemWeed extends HZItem {
  
    }
  
    HZItemWeed
      .inheritFields(HZItem)
      .inheritActions(HZItem)
      .field('_type', {default: 'ItemWeed'})
      .field('image', {default: 'weed'})
      .field('shape', {default: {"grid":[{"x":-6.000000000000398,"y":-26.763480392157135}],"offset":{"x":6.000000000000398,"y":26.763480392157135}}})
      .field('name',  {default: 'itemweed'})
      .field('label', {default: 'Weed'})
    ;
  
    return HZItemWeed;
  
  }