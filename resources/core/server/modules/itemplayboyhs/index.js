module.exports = (horizon, config) => {
   
  const HZItem = horizon.require('item');

  class HZItemPlayboyhs extends HZItem {

  }

  HZItemPlayboyhs
    .inheritFields(HZItem)
    .inheritActions(HZItem)
    .field('_type', {default: 'ItemPlayboyhs'})
    .field('image', {default: 'playboyhs'})
    .field('shape', {default: {"grid":[],"offset":{"x":4.999999999999986,"y":26.355119825708016}}})
    .field('name',  {default: 'itemplayboyhs'})
    .field('label', {default: 'Playboy HS'})
  ;

  return HZItemPlayboyhs;

}