'use strict';

const alt = require('alt');

module.exports = function(horizon, config) {

  const $                = horizon.$;
  const Utils            = horizon.require('utils');
  const HZItem           = horizon.require('item');
  const HZTrunkContainer = horizon.require('trunkcontainer');
  
	class HZVehicle extends HZItem {

    get altVehicle() {
      return alt.Vehicle.all.find(e => (e.model === this.model) && (e.numberPlateText === this.numberPlateText));
    }

    static create(model, coords, rot = {x: 0, y: 0, z: 0}) {
      
      return new Promise(async (resolve, reject) => {

        if(typeof model === 'string')
          model = Utils.joaat(model);

        const veh = new alt.Vehicle(model, coords.x, coords.y, coords.z, rot.x, rot.y, rot.z);

        veh.numberPlateText = Utils.getRandomString(8);

        // await horizon.$.utils.delay(1000);

        veh.modKit = 1;

        const mods = {};

        for(let i=0; i<50; i++)
          mods[i] = veh.getMod(i);

        const vehicle = new HZVehicle({
          model                : veh.model,
          pos                  : {x: coords.x, y: coords.y, z: coords.z},
          rot                  : {x: rot.x, y: rot.y, z: rot.z},
          mods                 : mods,
          appearanceDataBase64 : veh.getAppearanceDataBase64(),
          damageStatusBase64   : veh.getDamageStatusBase64(),
          gamestateDataBase64  : veh.getGamestateDataBase64(),
          scriptDataBase64     : veh.getScriptDataBase64(),
          activeRadioStation   : veh.activeRadioStation,
          bodyAdditionalHealth : veh.bodyAdditionalHealth,
          bodyHealth           : veh.bodyHealth,
          customPrimaryColor   : {r: veh.customPrimaryColor.x,   g: veh.customPrimaryColor.y,   b: veh.customPrimaryColor.z},
          customSecondaryColor : {r: veh.customSecondaryColor.x, g: veh.customSecondaryColor.y, b: veh.customSecondaryColor.z},
          customTires          : veh.customTires,
          darkness             : veh.darkness,
          dashboardColor       : veh.dashboardColor,
          daylightOn           : veh.daylightOn,
          dirtLevel            : veh.dirtLevel,
          engineHealth         : veh.engineHealth,
          engineOn             : veh.engineOn,
          flamethrowerActive   : veh.flamethrowerActive,
          handbrakeActive      : veh.handbrakeActive,
          hasArmoredWindows    : veh.hasArmoredWindows,
          headlightColor       : veh.headlightColor,
          interiorColor        : veh.interiorColor,
          lightsMultiplier     : veh.lightsMultiplier,
          livery               : veh.livery,
          lockState            : veh.lockState,
          manualEngineControl  : veh.manualEngineControl,
          modKit               : veh.modKit,
          modKitsCount         : veh.modKitsCount,
          neon                 : veh.neon,
          neonColor            : {r: veh.neonColor.x, g: veh.neonColor.y, b: veh.neonColor.z},
          nightlightOn         : veh.nightlightOn,
          numberPlateIndex     : veh.numberPlateIndex,
          numberPlateText      : veh.numberPlateText,
          pearlColor           : veh.pearlColor,
          petrolTankHealth     : veh.petrolTankHealth,
          primaryColor         : veh.primaryColor,
          repairsCount         : veh.repairsCount,
          roofLivery           : veh.roofLivery,
          roofOpened           : veh.roofOpened,
          secondaryColor       : veh.secondaryColor,
          sirenActive          : veh.sirenActive,
          tireSmokeColor       : veh.tireSmokeColor,
          wheelColor           : veh.wheelColor,
          wheelsCount          : veh.wheelsCount,
          windowTint           : veh.windowTint,
        });

        await vehicle.save();
        
        veh.setSyncedMeta('_id', vehicle._idStr);

        resolve(vehicle);

      });

    }

    static restoreAll() {

      return new Promise(async (resolve, reject) => {

        const vehicles = await HZVehicle.findOfType({});

        for(let i=0; i<vehicles.length; i++)
          vehicles[i].restore();

        resolve();

      });

    }

    static async saveAll() {

      const vehs = alt.Vehicle.all;

      for(let i=0; i<vehs.length; i++) {

        const veh    = vehs[i];
        const _idStr = veh.getSyncedMeta('_id');

        if(_idStr) {

          const vehicle = await HZVehicle.findOne({_id: HZVehicle.id(_idStr)});
          vehicle.update(veh);
          vehicle.save();

        }

      }

    }

    update(veh) {

      const mods = {};

      for(let i=0; i<50; i++)
        mods[i] = veh.getMod(i);

      this.load({
        model                : veh.model,
        pos                  : {x: veh.pos.x, y: veh.pos.y, z: veh.pos.z},
        rot                  : {x: veh.rot.x, y: veh.rot.y, z: veh.rot.z},
        mods                 : mods,
        appearanceDataBase64 : veh.getAppearanceDataBase64(),
        damageStatusBase64   : veh.getDamageStatusBase64(),
        gamestateDataBase64  : veh.getGamestateDataBase64(),
        scriptDataBase64     : veh.getScriptDataBase64(),
        activeRadioStation   : veh.activeRadioStation,
        bodyAdditionalHealth : veh.bodyAdditionalHealth,
        bodyHealth           : veh.bodyHealth,
        customPrimaryColor   : {r: veh.customPrimaryColor.x,   g: veh.customPrimaryColor.y,   b: veh.customPrimaryColor.z},
        customSecondaryColor : {r: veh.customSecondaryColor.x, g: veh.customSecondaryColor.y, b: veh.customSecondaryColor.z},
        customTires          : veh.customTires,
        darkness             : veh.darkness,
        dashboardColor       : veh.dashboardColor,
        daylightOn           : veh.daylightOn,
        dirtLevel            : veh.dirtLevel,
        engineHealth         : veh.engineHealth,
        engineOn             : veh.engineOn,
        flamethrowerActive   : veh.flamethrowerActive,
        handbrakeActive      : veh.handbrakeActive,
        hasArmoredWindows    : veh.hasArmoredWindows,
        headlightColor       : veh.headlightColor,
        interiorColor        : veh.interiorColor,
        lightsMultiplier     : veh.lightsMultiplier,
        livery               : veh.livery,
        lockState            : veh.lockState,
        manualEngineControl  : veh.manualEngineControl,
        modKit               : veh.modKit,
        modKitsCount         : veh.modKitsCount,
        neon                 : veh.neon,
        neonColor            : {r: veh.neonColor.x, g: veh.neonColor.y, b: veh.neonColor.z},
        nightlightOn         : veh.nightlightOn,
        numberPlateIndex     : veh.numberPlateIndex,
        numberPlateText      : veh.numberPlateText,
        pearlColor           : veh.pearlColor,
        petrolTankHealth     : veh.petrolTankHealth,
        primaryColor         : veh.primaryColor,
        repairsCount         : veh.repairsCount,
        roofLivery           : veh.roofLivery,
        roofOpened           : veh.roofOpened,
        secondaryColor       : veh.secondaryColor,
        sirenActive          : veh.sirenActive,
        tireSmokeColor       : veh.tireSmokeColor,
        wheelColor           : veh.wheelColor,
        wheelsCount          : veh.wheelsCount,
        windowTint           : veh.windowTint,
      });

    }

    async restore() {

      const veh = new alt.Vehicle(this.model, this.pos.x, this.pos.y, this.pos.z, this.rot.x, this.rot.y, this.rot.z);
      
      veh.setAppearanceDataBase64(this.appearanceDataBase64);
      veh.setDamageStatusBase64(this.damageStatusBase64);
      veh.setGamestateDataBase64(this.gamestateDataBase64);
      veh.setScriptDataBase64(this.scriptDataBase64);
      
      veh.modKit = 1;

      for(let i=0; i<50; i++)
        veh.setMod(i, this.mods[i]);

      veh.activeRadioStation    = this.activeRadioStation;
      veh.bodyAdditionalHealth  = this.bodyAdditionalHealth;
      veh.bodyHealth            = this.bodyHealth;
      veh.customPrimaryColor    = this.customPrimaryColor;
      veh.customSecondaryColor  = this.customSecondaryColor;
      veh.customTires           = this.customTires;
      veh.darkness              = this.darkness;
      veh.dashboardColor        = this.dashboardColor;
      veh.daylightOn            = this.daylightOn;
      veh.dirtLevel             = this.dirtLevel;
      veh.engineHealth          = this.engineHealth;
      veh.engineOn              = this.engineOn;
      veh.flamethrowerActive    = this.flamethrowerActive;
      veh.handbrakeActive       = this.handbrakeActive;
      veh.hasArmoredWindows     = this.hasArmoredWindows;
      veh.headlightColor        = this.headlightColor;
      veh.interiorColor         = this.interiorColor;
      veh.lightsMultiplier      = this.lightsMultiplier;
      veh.livery                = this.livery;
      veh.lockState             = this.lockState;
      veh.manualEngineControl   = this.manualEngineControl;
      veh.modKit                = this.modKit;
      veh.modKitsCount          = this.modKitsCount;
      veh.neon                  = this.neon;
      veh.neonColor             = this.neonColor;
      veh.nightlightOn          = this.nightlightOn;
      veh.numberPlateIndex      = this.numberPlateIndex;
      veh.numberPlateText       = this.numberPlateText;
      veh.pearlColor            = this.pearlColor;
      veh.petrolTankHealth      = this.petrolTankHealth;
      veh.primaryColor          = this.primaryColor;
      veh.repairsCount          = this.repairsCount;
      veh.roofLivery            = this.roofLivery;
      veh.roofOpened            = this.roofOpened;
      veh.secondaryColor        = this.secondaryColor;
      veh.sirenActive           = this.sirenActive;
      veh.tireSmokeColor        = this.tireSmokeColor;
      veh.wheelColor            = this.wheelColor;
      veh.wheelsCount           = this.wheelsCount;
      veh.windowTint            = this.windowTint;

      veh.setSyncedMeta('_id', this._idStr);
    }

    delete() {
      return Promise.all([
        this.trunk.delete(),
        super.delete()
      ]);
    }

	}

  function generateTrunkContainer() {

    switch(this.model) {

      default : {

        return new HZTrunkContainer({
          name  : 'vehicletrunk',
          rows  : 8,
          cols  : 8,
          _data : {
            vehicle: {
              model: this.model,
              plate: this.plate,
            }
          }
        });
      }

    }

  }

	HZVehicle
    .inheritFields(HZItem)
    .inheritActions(HZItem)
    .field('_type'                , {default: 'Vehicle'})
    .field('name'                 , {default: 'genericvehicle'})
    .field('trunk'                , {subdoc :  HZTrunkContainer, default: generateTrunkContainer})
    .field('model'                , {required: true})
    .field('pos'                  , {required: true})
    .field('rot'                  , {required: true})
    .field('mods'                 , {default:  {}})
    .field('appearanceDataBase64' , {required: true})
    .field('damageStatusBase64'   , {required: true})
    .field('gamestateDataBase64'  , {required: true})
    .field('scriptDataBase64'     , {required: true})
    .field('activeRadioStation'   , {required: true})
    .field('bodyAdditionalHealth' , {required: true})
    .field('bodyHealth'           , {required: true})
    .field('customPrimaryColor'   , {required: true})
    .field('customSecondaryColor' , {required: true})
    .field('customTires'          , {required: true})
    .field('darkness'             , {required: true})
    .field('dashboardColor'       , {required: true})
    .field('daylightOn'           , {required: true})
    .field('dirtLevel'            , {required: true})
    .field('engineHealth'         , {required: true})
    .field('engineOn'             , {required: true})
    .field('flamethrowerActive'   , {required: true})
    .field('handbrakeActive'      , {required: true})
    .field('hasArmoredWindows'    , {required: true})
    .field('headlightColor'       , {required: true})
    .field('interiorColor'        , {required: true})
    .field('lightsMultiplier'     , {required: true})
    .field('livery'               , {required: true})
    .field('lockState'            , {required: true})
    .field('manualEngineControl'  , {required: true})
    .field('modKit'               , {required: true})
    .field('modKitsCount'         , {required: true})
    .field('neon'                 , {required: true})
    .field('neonColor'            , {required: true})
    .field('nightlightOn'         , {required: true})
    .field('numberPlateIndex'     , {required: true})
    .field('numberPlateText'      , {required: true})
    .field('pearlColor'           , {required: true})
    .field('petrolTankHealth'     , {required: true})
    .field('primaryColor'         , {required: true})
    .field('repairsCount'         , {required: true})
    .field('roofLivery'           , {required: true})
    .field('roofOpened'           , {required: true})
    .field('secondaryColor'       , {required: true})
    .field('sirenActive'          , {required: true})
    .field('tireSmokeColor'       , {required: true})
    .field('wheelColor'           , {required: true})
    .field('wheelsCount'          , {required: true})
    .field('windowTint'           , {required: true})
    .prop('persist', () => true);
  ;
  
  HZVehicle.__init = async function(cb) {
    
    await HZVehicle.restoreAll();

    setInterval(HZVehicle.saveAll, 10000);

    /*
    horizon.on('player.inited', async player => {

      const vehicles = await HZVehicle.findOfType({});

      for(let i=0; i<vehicles.length; i++) {
        const vehicle = vehicles[i];
        alt.emitClient(null, 'hz:vehicle:update', vehicle.altVehicle, vehicle._idStr);
      }

    });
    */

    cb();
  }

	return HZVehicle;

};
