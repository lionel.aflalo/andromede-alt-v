module.exports = (horizon, config) => {
   
  const HZItem = horizon.require('item');

  class HZItemPlayboydecember extends HZItem {

  }

  HZItemPlayboydecember
    .inheritFields(HZItem)
    .inheritActions(HZItem)
    .field('_type', {default: 'ItemPlayboydecember'})
    .field('image', {default: 'playboydecember'})
    .field('shape', {default: {"grid":[{"x":-1.9999999999999147,"y":-34.25770925110123},{"x":-1.9999999999999147,"y":65.74229074889877}],"offset":{"x":1.9999999999999147,"y":34.25770925110123}}})
    .field('name',  {default: 'itemplayboydecember'})
    .field('label', {default: 'Playboy Décembre'})
  ;

  return HZItemPlayboydecember;

}