'use strict';

module.exports = function(horizon, config) {

	const $            = horizon.$;
	const HZItem       = horizon.require('item');
	const HZItemTomato = horizon.require('itemtomato');
	
	const GROW_TIME = 10 * 60;

	const MODELS = [
	  'tomato_1',
	  'tomato_2',
	  'tomato_3',
	  'tomato_4',
	  'tomato_5',
	  'tomato_6',
	  'tomato_7',
	  'tomato_8',
	  'tomato_9',
	  'tomato_10',
	].map(e => $.utils.joaat(e));

	class HZSproutTomato extends HZItem {

		morph() {
			
			const now = Math.floor(new Date / 1000);

			if(now - this.createTime >= GROW_TIME)
				return new HZItemTomato({count: this.count * 5});

			return null;
		}

	}

	HZSproutTomato
		.inheritFields(HZItem)
		.inheritActions(HZItem)
		.field('_type',     {default: 'SproutTomato'})
		.field('name',      {default: 'SproutTomato'})
		.field('morphable', {default: true})
		.field('moveable',  {default: false})
		.field('image',     {default: 'sprouttomato'})
    .prop('ttl',        () => 60000)
    .prop('model', function() {

    	const now     = Math.floor(new Date / 1000);
    	const elapsed = now - this.createTime;
    	let   ratio   = elapsed / GROW_TIME;

    	if(ratio > 1)
    		ratio = 1;

    	const idx = Math.floor(ratio * (MODELS.length - 1))

    	return MODELS[idx];

		})
	;

	return HZSproutTomato;
};
