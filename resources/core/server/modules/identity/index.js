'use strict';

const alt      = require('alt');
const ObjectID = require('mongodb').ObjectID;

module.exports = function(horizon, config) {

	const Utils                        = horizon.require('utils');
	const HZEnrolable                  = horizon.require('enrolable');
  const HZIdentityBaseContainer      = horizon.require('identitybasecontainer');
  const HZIdentityComponentContainer = horizon.require('identitycomponentcontainer');
	const HZIdentityPropContainer      = horizon.require('identitypropcontainer');
	const HZPedComponent               = horizon.require('pedcomponent');
	const HZPedProp                    = horizon.require('pedprop');
	
	class HZIdentity extends HZEnrolable {
		
    async ensureContainers() {
      
    	if(this.containersEnsured)
    		return;

      for(let i=0; i<config.containers.length; i++) {

        let found = false;

        for(let j=0; j<this.containers.length; j++) {
          if(this.containers[j] instanceof config.containers[i]) {
            found = true;
            break;
          }
        }

        if(!found) {

          this.containers.push(new config.containers[i]({
						identity: this._id
					}))
					
        }
      }

			const promises = this.containers.map(e => e.save());

			await Promise.all(promises);

      this.containersEnsured = true;

    }

		async ensureComponents() {

			const player    = this.getAltPlayer();
			const container = this.containers.find(e => e instanceof HZIdentityComponentContainer);
			const curr      = container.items.filter(e => e);

			await Promise.all(curr.map(e => e.delete()));

			container.items.length = 0;

			for(const c in this.model.components) {

				const data = {
					...this.model.components[c],
					component: parseInt(c, 10),
					parentModel: this.model.hash
				};

				const component = new HZPedComponent(data);
				container.items.push(component);
			}

			await container.save(true);

			horizon.updateCache(player, container._idStr);

		}

		async ensureProps() {

			const player    = this.getAltPlayer();
			const container = this.containers.find(e => e instanceof HZIdentityPropContainer);
			const curr      = container.items.filter(e => e);

			await Promise.all(curr.map(e => e.delete()));

			container.items.length = 0;

			for(const c in this.model.props) {

				const data = {
					...this.model.props[c],
					component: parseInt(c, 10),
					parentModel: this.model.hash
				};

				const prop = new HZPedProp(data);
				container.items.push(prop);
			}

			await container.save(true);

			horizon.updateCache(player, container._idStr);

		}

		ensureModel() {

			const componentContainer = this.containers.find(e => e instanceof HZIdentityComponentContainer);
			const propContainer      = this.containers.find(e => e instanceof HZIdentityPropContainer);
			const components         = {};
			const props              = {};

			for(let i=0; i<componentContainer.items.length; i++) {

				const item = componentContainer.items[i];

				if(!item)
					continue;

				components[item.component] = {drawable: item.drawable, texture: item.texture, palette: item.palette};

			}

			this.model.components = components;

			for(let i=0; i<propContainer.items.length; i++) {

				const item = propContainer.items[i];

				if(!item)
					continue;

				props[item.component] = {drawable: item.drawable, texture: item.texture};

			}

			this.model.props = props;

			const player = this.getAltPlayer();

			if(player)
				player.setSyncedMeta('model', this.model);

		}

		getAltPlayer() {
			return this._idStr ? alt.Player.all.find(e => e.getSyncedMeta('identity') && (e.getSyncedMeta('identity') === this._idStr)) : null;
		}

		getFreeSpace(item) {
			
			let _x, _y;

			const container = this.containers.find(container => {

				for(let y=0; y<container.rows; y++) {
					for(let x=0; x<container.cols; x++) {
						if(container.check(item, x, y)) {
						
							_x = x;
							_y = y;

							return true;
						}
					}
				}

				return false;

			});
			
			if(container)
				return {container, x: _x, y: _y}
			else
				return null;

    }
    
    hasItem(item) {
      
      for(let i=0; i<this.containers.length; i++) {

        const container = this.containers[i];

        if(container.items.find(e => e && e._id.equals(item._id)))
          return true;
      }

      return false;

    }

	  queryItem(query = {}){

	    const matches = [];
      
	    for(let i=0; i<this.containers.length; i++) {

	      const container = this.containers[i];

	      for(let j=0; j<container.items.length; j++) {

	        const item       = container.items[j];
	        let   match      = true;
	        let   matchCount = 0;

	        if(item === null || typeof item === 'undefined') {
	        	
	        	match = false;

	        } else {


		        for(let k in query) {

		          if(query.hasOwnProperty(k)) {

		          	let check = false;

		          	if(query[k] instanceof ObjectID)
		          		check = query[k].equals(item[k]);
		          	else
		          		check = query[k] === item[k];

		          	if(!check) {
		            	match = false;
		            	break;
		          	} else matchCount ++;
		          }
		        }

	        }

	        if(matchCount == 0)
	        	match = false;

	        if(match)
	          matches.push({container, item});

	      }

	    }

	    return matches;
    }

  }

	HZIdentity
		.collection('identities')
		.inheritFields(HZEnrolable)
		.field('_type',              {default: 'Identity'})
		/* .field('name',            {required: true}) */
		.field('birthDate',          {default: Math.floor(new Date('1990-12-31') / 1000)})
		.field('residentNumber',     {default: Utils.getRandomInt(1000000000, 9999999999)})
		.field('weight',             {default: 45})
		.field('size',               {default: 150})
		.field('model',              {required: true})
		.field('health',             {default: 200})
		.field('armour',             {default: 0})
		.field('loadout',            {default: []})
		.field('position',           {default: null})
		.field('containers',         {subdoc: HZIdentityBaseContainer, default: []})
		.field('containersEnsured',  {default: false})
		.field('status',             {default: {}})
    .prop('persist', () => true)
	;

	HZIdentity.__init = function(cb) {

		alt.onClient('hz:identity:update:model', async (player, model) => {

			if(model) {

				model.hash = player.model;
				
				horizon.logger.info(`${player.getSyncedMeta('player')} => Identity updated (model)`);

				const identity = await player.getIdentity();
				identity.model = model;

				await identity.ensureComponents();
				await identity.ensureProps();
				await identity.save();
			}
			
    });
    
    alt.onClient('hz:identity:revive', async (player, _idStr) => {

      horizon.logger.info(`${player.getSyncedMeta('player')} => Revive ${_idStr}`);

      const identity = await HZIdentity.findOne({_id: HZIdentity.id(_idStr)});

      if(identity) {
        const targetPlayer = identity.getAltPlayer();
        targetPlayer.spawn(targetPlayer.pos.x, targetPlayer.pos.y, targetPlayer.pos.z - 0.75, 0);
      }

    });

		cb();

	}

	return HZIdentity;

};
