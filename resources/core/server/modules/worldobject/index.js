'use strict';

const CUBE_SIZE = 100;

module.exports = function(horizon, config) {

	const HZEntity = horizon.require('entity');
 
	class HZWorldObject extends HZEntity {
    
    static get cubeSize() { return CUBE_SIZE };

		set position(val) {

			this.fields.position = val;

      if(val) {

        const x = Math.floor(val.x);
        const y = Math.floor(val.y);
        const z = Math.floor(val.z);
  
        this.fields.cube = {
          x: Math.floor((x - (x % CUBE_SIZE)) / CUBE_SIZE),
          y: Math.floor((y - (y % CUBE_SIZE)) / CUBE_SIZE),
          z: Math.floor((z - (z % CUBE_SIZE)) / CUBE_SIZE),
        };

      } else this.fields.cube = {x: 0, y: 0, z: -8000};

			this.__toSave = true;
		}

	}

	HZWorldObject
		.inheritFields(HZEntity)
		.field('_type',    {default: 'WorldObject'})
		.field('model',    {required: true})
		.field('position', {required: true, customSetter: true})
    .field('cube',     {})
	;

	return HZWorldObject;
};
