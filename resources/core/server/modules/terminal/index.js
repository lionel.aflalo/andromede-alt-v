const EventEmitter = require('events');
const readline     = require('readline');
const alt          = require('alt');

module.exports = (horizon, config) => {

  const mem = {};

  class HZTerminal extends EventEmitter {

    constructor() {

      super();

      /*
      this.rl = readline.createInterface({
        input   : process.stdin,
        output  : process.stdout,
        terminal: false
      });
      
      this.rl.on('line', function(line){
        eval(`console.dir(((alt, horizon, mem) => {${line}})(alt, horizon, mem))`);
      });
      */

    }

  }
  
  const instance = new HZTerminal();

  return instance;

};