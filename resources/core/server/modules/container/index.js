'use strict';

const alt   = require('alt');
const async = require('async');
const hash  = require('object-hash');

module.exports = function(horizon, config) {

  const $           = horizon.$;
  const HZItem      = horizon.require('item');

  const GRID_SIZE = 100;

  const generators = {};

  class HZContainer extends HZItem {
    
  	static generator(name, cb) {
  		
  		generators[this.prototype.constructor.name]       = generators[this.prototype.constructor.name] || {};
  		generators[this.prototype.constructor.name][name] = cb;
  		
  		this.field('hasGenerators', {default: true});
  	 
      return this;
  	}

    constructor(data) {

      super(data);

      if (new.target === HZContainer)
        throw new TypeError('Cannot construct HZContainer instances directly, must be extended');
     
      this.constraints = [];
      this.whitelist   = [];
      this.blacklist   = [];

    }

    constraint(name, cb, type = 'put') {
      this.constraints.push({name, cb, type});
    }

    getConstraints(type, item, count = null, identity = null) {

      return new Promise((resolve, reject) => {

        const currConstraints = this.constraints.filter(e => e.type === type)
        const constraints     = [];
        count                 = count || item.count;

        if(!item.moveable)
          constraints.push('moveable');

        if(type === 'put') {

          const checkContainerRecursion = (obj) => {

            if(!obj)
              return;

            if(obj == this || (this._id !== null && this._id.equals(obj._id))) {
              constraints.push('recursion');
              return;
            }

            if(obj instanceof HZItem) {

              for(let i=0; i<obj.subdocs.length; i++) {

                const subdoc = obj.subdocs[i];

                if(subdoc.type !== HZItem && !HZItem.prototype.isPrototypeOf(subdoc.type.prototype))
                  continue;

                if(subdoc.isArray) {
                  
                  for(let j=0; j<obj[subdoc.name].length; j++)
                    checkContainerRecursion(obj[subdoc.name][j]);
                
                } else checkContainerRecursion(obj[subdoc.name]);

              }

            }
          }

          checkContainerRecursion(item);
            
          if(typeof this.whitelist != 'undefined') {

            let whitelisted = false;

            for(let i=0; i<this.whitelist.length; i++) {
              if(item instanceof this.whitelist[i]) {
                whitelisted = true;
                break;
              }
            }

            if(!whitelisted)
              constraints.push('whitelist');

          }

          if(typeof this.blacklist != 'undefined') {

            let blacklisted = false;

            for(let i=0; i<this.blacklist.length; i++) {
              if(item instanceof this.blacklist[i]) {
                blacklisted = true;
                break;
              }
            }

            if(blacklisted)
              constraints.push('blacklist');

          }

        }

        if(constraints.length == 0) {

          const tasks = [];

          for(let i=0; i<currConstraints.length; i++) {

            (currConstraint => {

              tasks.push(cb => {

                currConstraint.cb.apply(this, [item, count, identity, check => {

                  if(!check)
                    constraints.push(currConstraint.name);

                  cb(null);

                }]);

              });

            })(currConstraints[i]);

          }

          async.parallelLimit(tasks, 10, (err, results) => {

            if(type === 'put') {

              if(this.maxItems != -1) {

                let _count = 0;

                for(let i=0; i<this.items.length; i++)
                  if(this.items[i] !== null)
                    _count += this.items[i].count;

                if(_count + count > this.maxItems)
                  constraints.push('count');
              }

            }

            resolve(constraints);


          });
        
        } else resolve(constraints);

      });

    }

    computeShapes() {

      const computed = [];

      for(let i=0; i<this.rows; i++) {

        computed[i] = [];

        for(let j=0; j<this.cols; j++)
          computed[i].push(false);

      }

      for(let i=0; i<this.items.length; i++) {

        const item    = this.items[i];
        const shape1  = item.shape;
        const shape2  = [];
        const bb      = {x: Infinity, y: Infinity};
        let   offsetX = 0;
        let   offsetY = 0;

        shape1.grid.forEach(e => {

          if(e.x < bb.x)
            bb.x = e.x;

          if(e.y < bb.y)
            bb.y = e.y;

        });

        if(bb.x < 0)
          offsetX = Math.abs(bb.x);

        if(bb.y < 0)
          offsetY = Math.abs(bb.y);

        bb.x += offsetX;
        bb.y += offsetY;

        shape1.grid.forEach(e => {

          const x  = e.x + offsetX;
          const y  = e.y + offsetY;
          const gX = Math.floor(x / GRID_SIZE);
          const gY = Math.floor(y / GRID_SIZE);

          shape2[gY]     = shape2[gY] || [];
          shape2[gY][gX] = true;
          
        });

        for(let y=0; y<shape2.length; y++) {
          for(let x=0; x<shape2[y].length; x++) {

            const _x = x + item.x;
            const _y = y + item.y;

            if(shape2[y][x]) {

              if(computed[_y] === undefined || computed[_y][_x] === undefined)
                return false;

              computed[_y][_x] = true;
            }
          }
        }

      }

      return computed;

    }

    check(item, x, y, stackCount = 0) {
      
      if(this.rows === -1 && this.cols === -1)
        return true;

      const shape    = item.getShape();
      const computed = this.computeShapes(shape);

      if(stackCount > 0) {
        
        for(let i=0; i<this.items.length; i++) {

          const item2 = this.items[i];

          if(item2 && (item2.x === item.x) && (item2.y === item.y) && (item2.constructor === item.constructor) && (item2.count + stackCount <= item2.limit))
            return true;
        
        }

      }

      for(let iy=0; iy<shape.length; iy++) {
        for(let ix=0; ix<shape[iy].length; ix++) {

          const gY = iy + y;
          const gX = ix + x;

          if(gY < 0 || gX < 0)
            return false;

          for(let y1=0; y1<shape.length; y1++) {
            for(let x1=0; x1<shape[y1].length; x1++) {

              const _x = x1 + x;
              const _y = y1 + y;

              if(shape[y1][x1] && computed[_y] && computed[_y][_x] || ((computed[_y] === undefined || computed[_y][_x] === undefined) && shape[y1][x1]))
                return false;

            }
          }

        }
      }

      return true;
    }

		getFreeSpace(item, count = null) {
      
      count = count ? count : item.count;

      for(let i=0; i<this.items.length; i++) {

        const item2 = this.items[i];

        if(item2 && (item2.constructor === item.constructor) && (item2.count + count <= item2.limit))
          return item2;
      
      }

      for(let y=0; y<this.rows; y++) {
        for(let x=0; x<this.cols; x++) {

          if(this.check(item, x, y)) {
            return {x, y};
          }

        }
      }

      return null;

    }

    addItem(item, x = null, y = null, check = true) {

      return new Promise(async (resolve, reject) => {
        
        if(!check || this.check(item, x, y)) {
          
          item.x = x;
          item.y = y;

          item.save();
          this.items.push(item);

          this.save();

          resolve(true);

        } else setImmediate(() => resolve(false));

      });

    }

    removeItem(item) {

      return new Promise(async (resolve, reject) => {

        const idx = this.items.findIndex(e => e && ((e === item) || item._id.equals(e._id)));

        if(idx !== -1) {
          
          this.items.splice(idx, 1);
          
          this.save();

          resolve(true);

        } else setImmediate(() => resolve(false));

      });
    }

    getSlot(item) {
      
    }

    save(...args) {

      for(let i=0; i<this.items.length; i++) {

        const item = this.items[i];

        if(item)
          item.container = this._id;
      }

      return super.save(...args);
    }
  }

  HZContainer
    .inheritFields(HZItem)
    .inheritActions(HZItem)
    .field('_type', {default: 'Container'})
    .field('items', {subdoc: HZItem, default: []})
    .field('rows',  {required: true})
    .field('cols',  {required: true})
    .field('hasGenerators', {default: false})
    .field('generatorData', {default: {}})
    .prop('label', function() { return this.name; })
  ;

  HZContainer.__init = async function(cb) {

    // Move item
    alt.onClient('hz:item:move', async (player, fromIdStr, toIdStr, itemIdStr, x, y, count = 1) => {
      
      if(count < 1)
        return;

      const hzPlayer = await horizon.modules.player.findOne({_id: horizon.modules.player.id(player.getSyncedMeta('player'))});

      if(!hzPlayer)
        return;

      count = Math.floor(count);

      const fromId = fromIdStr ? HZContainer.id(fromIdStr) : null;
      const toId   = toIdStr   ? HZContainer.id(toIdStr)   : null;
      const itemId = HZItem.id(itemIdStr);
      
      let from = null;
      let to   = null;
      let item = null;

      if(fromId)
        from = await HZContainer.findOne({_id: fromId});

      if(toId) {
        
        if(fromIdStr === toIdStr)
          to = from;
        else
          to = await HZContainer.findOne({_id: toId});
      
      }

      if(fromIdStr === toIdStr) {
  
        item = from.items.find(e => e._id.equals(itemId));

        if(item) {
          
          const foundSame = (to.rows !== -1 && to.cols !== -1) && to.items.find(e => e && (e.constructor === item.constructor) && (e.x === x && e.y === y));
          
          if(foundSame) {

            if((foundSame.count + count) > foundSame.limit) {
              horizon.updateCache(player, from._idStr);
              return;
            }

            item.count      -= count;
            foundSame.count += count;

            if(item.count <= 0) {
              from.removeItem(item);
            }
            
            foundSame.save()

            if(from instanceof horizon.modules.worldcontainer)
              from.updateCube(item.world.position, item, foundSame);
            else
              horizon.updateCache(player, from._idStr, item._idStr, foundSame._idStr);

          } else if((count < item.count) && to.check(item, x, y)) {

            const newItem = item.clone();
            item.count    = item.count - count;
            newItem.count = count;

            await newItem.save();
            await to.addItem(newItem, x, y);

            item.save();
            to.save();

            if(from instanceof horizon.modules.worldcontainer)
              from.updateCube(item.world.position, item, newItem);
            else
              horizon.updateCache(player, from._idStr, item._idStr, newItem._idStr);

          } else {

            if(from instanceof horizon.modules.worldcontainer) {

              item.world = {position: player.pos};

            } else {
              
              item.x = x;
              item.y = y;  
            }

            item.save();

            if(from instanceof horizon.modules.worldcontainer)
              from.updateCube(item.world.position, item);
            else
              horizon.updateCache(player, from._idStr, item._idStr);

          }

        }

      } else {
        
        const idx = from.items.findIndex(e => e._id.equals(itemId));
  
        if(idx !== -1) {
          
          const _item     = from.items[idx];
          const foundSame = (to.rows !== -1 && to.cols !== -1) && to.items.find(e => e && (e.constructor === _item.constructor) && (e.x === x && e.y === y));

          //if(!_item.moveable && !hzPlayer.hasRole('admin'))
          //  return;

          if(foundSame) {

            if((foundSame.count + count) > foundSame.limit) {
              horizon.updateCache(player, from._idStr, to._idStr, foundSame._idStr);
              return;
            }

            _item.count     -= count;
            foundSame.count += count;

            if(_item.count <= 0) {
              from.removeItem(_item);
            }

            foundSame.save();

            const toUpdate = [_item._idStr, foundSame._idStr];

            if(from instanceof horizon.modules.worldcontainer)
              from.updateCube(_item.world.position);
            else
              toUpdate.push(from._idStr);

            if(to instanceof horizon.modules.worldcontainer)
              to.updateCube(foundSame.world.position);
            else
              toUpdate.push(to._idStr);

            horizon.updateCache(player, ...toUpdate);
          
          } else if((count < _item.count) && (!(to instanceof horizon.modules.worldcontainer) && to.check(_item, x, y))) {

            const newItem = _item.clone();
            _item.count   = _item.count - count;
            newItem.count = count;

            await newItem.save();

            if(to instanceof horizon.modules.worldcontainer)
              await to.addItem(newItem, player.pos);
            else
              await to.addItem(newItem, x, y);

            _item.save();
            to.save();

            const toUpdate = [_item._idStr, newItem._idStr];

            if(from instanceof horizon.modules.worldcontainer)
              from.updateCube(_item.world.position);
            else
              toUpdate.push(from._idStr);

            if(to instanceof horizon.modules.worldcontainer)
              to.updateCube(newItem.world.position);
            else
              toUpdate.push(to._idStr);

            horizon.updateCache(player, ...toUpdate);

          } else {
          
            let success = await from.removeItem(_item);
    
            if(success) {

              if(to instanceof horizon.modules.worldcontainer) {
                
                to.addItem(_item, player.pos);
                to.updateCube(_item.world.position, from, _item);
              
              } else {

                to.addItem(_item, x, y);
                horizon.updateCache(player, to._idStr, from._idStr, _item._idStr);
              
              }

            }

          }
  
        }

      }

    });

    // Generators
    const processGenerators = async () => {

      const containers = await HZContainer.find({hasGenerators: true});
      const now        = Math.floor(new Date / 1000);
      let   toUpdate   = [];

      for(let i=0; i<containers.length; i++) {

        const container      = containers[i];
        const currGenerators = generators[container.constructor.name];

        let hasUpdate = false;

        for(let name in currGenerators) {

          const generator               = currGenerators[name];
          container.generatorData[name] = container.generatorData[name] || {};
          const data                    = container.generatorData[name];
          
          let items = await generator(now, data, container);

          if(!items)
            continue;

          if(!(items instanceof Array))
            items = [items];

          for(let j=0; j<items.length; j++) {

            const newItem = items[j];

            if(!newItem)
              continue;

            hasUpdate = true;

            const result = container.getFreeSpace(newItem, newItem.count);

            if(result) {

              if(result instanceof HZItem) {
                
                result.count += newItem.count;
                result.save();
  
                toUpdate.push(result._idStr);
              
              } else {
                
                if(container instanceof horizon.modules.worldcontainer) {

                  newItem.world = item.world;
    
                  await newItem.save();

                  container.items.push(newItem);

                  toUpdate.push(container._idStr);

                } else {
    
                  const result  = container.getFreeSpace(newItem, newItem.count);
      
                  if(result) {
        
                    await newItem.save();
      
                    if(result instanceof HZItem) {
                      
                      result.count += newItem.count;
                      result.save();
        
                      toUpdate.push(result._idStr);
                    
                    } else {
                      
                      newItem.x = result.x;
                      newItem.y = result.y;  
    
                      await newItem.save();
      
                      container.items.push(newItem);

                      toUpdate.push(container._idStr);
        
                    }

                  }
      
                }
  
              }

            }

          }
          
        }
        
        container.save();
      }

      toUpdate = toUpdate.filter((v, i, a) => a.indexOf(v) === i); 

      horizon.updateCache(null, ...toUpdate);

      setTimeout(processGenerators, 10000);
      
    };

    // Morph
    const processMorph = async () => {

      const items      = await HZItem.find({morphable: true});
      const now        = Math.floor(new Date / 1000);
      let   toUpdate   = [];

      for(let i=0; i<items.length; i++) {

        const item = items[i];

        let newItems = item.morph(now);

        if(newItems) {
          
          if(!(newItems instanceof Array))
            newItems = [newItems];

          const container = await HZContainer.findOne({_id: item.container});

          if(container === null)
            continue;

          for(let j=0; j<newItems.length; j++) {

            const newItem = newItems[j];

            if(container instanceof horizon.modules.worldcontainer) {

              newItem.world     = item.world;
              newItem.container = container._id;

              await newItem.save();

              container.items.push(newItem);

              toUpdate.push(container._idStr);

            } else {

              const result = container.getFreeSpace(newItem, newItem.count);
  
              if(result) {
    
                await newItem.save();
  
                if(result instanceof HZItem) {
                  
                  result.count += newItem.count;
                  result.save();
    
                  toUpdate.push(result._idStr);
                
                } else {
                  
                  newItem.x         = result.x;
                  newItem.y         = result.y; 
                  newItem.container = container._id; 

                  await newItem.save();
  
                  container.items.push(newItem);

                  toUpdate.push(container._idStr);
    
                }

              }
  
            }

          }

          await container.removeItem(item);
          await item.delete();
          await container.save();

          toUpdate.push(container._idStr);

        }

      }

      toUpdate = toUpdate.filter((v, i, a) => a.indexOf(v) === i); 

      horizon.updateCache(null, ...toUpdate);

      setTimeout(processMorph, 10000);
      
    };

    processGenerators();
    processMorph();

    cb();
    
  }

  return HZContainer;

};
