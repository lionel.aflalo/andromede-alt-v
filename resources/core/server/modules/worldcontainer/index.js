'use strict';

const alt = require('alt');

const CUBE_SIZE = 100;

module.exports = function(horizon, config) {

	const $               = horizon.$;
	const { getDistance } = $.utils;
	
	const HZContainer = horizon.require('container');
 
	class HZWorldContainer extends HZContainer {

    addItem(item, position, y = null) {

			if(y !== null)
				throw new Error('y should be null because this is a world container');

      return new Promise(async (resolve, reject) => {
        
				item.world = {position: {x: position.x, y: position.y, z: position.z}};
				item.save();

				this.items.push(item);

				this.save();

				resolve(true);

      });

		}
		
		updateCube(position, ...items) {

			const cube = {
				x: (position.x - (position.x % CUBE_SIZE)) / CUBE_SIZE,
				y: (position.y - (position.y % CUBE_SIZE)) / CUBE_SIZE,
				z: (position.z - (position.z % CUBE_SIZE)) / CUBE_SIZE,
			};

			const players = alt.Player.all.filter(e => {

				const playerPos = e.pos;

				const playerCube = {
					x: (playerPos.x - (playerPos.x % CUBE_SIZE)) / CUBE_SIZE,
					y: (playerPos.y - (playerPos.y % CUBE_SIZE)) / CUBE_SIZE,
					z: (playerPos.z - (playerPos.z % CUBE_SIZE)) / CUBE_SIZE,
				};

				return (cube.x === playerCube.x) && (cube.y === playerCube.y) && (cube.z === playerCube.z);

			});

			for(let i=0; i<players.length; i++)
				horizon.updateCache(players[i], this._idStr, ...items.map(e => e._idStr));

		}

		serialize(player) {

			const cube = {
				x: (player.pos.x - (player.pos.x % CUBE_SIZE)) / CUBE_SIZE,
				y: (player.pos.y - (player.pos.y % CUBE_SIZE)) / CUBE_SIZE,
				z: (player.pos.z - (player.pos.z % CUBE_SIZE)) / CUBE_SIZE,
			};

			const data = super.serialize(player);

			data.items = data.items.filter((e,i) => {
				
				return (
					this.items[i] && this.items[i].world && this.items[i].world.cube &&
					(this.items[i].world.cube.x === cube.x) &&
					(this.items[i].world.cube.y === cube.y) &&
					(this.items[i].world.cube.z === cube.z)
				);

			});

			return data;
		}

	}

	HZWorldContainer
		.inheritFields(HZContainer)
		.field('_type', {default: 'WorldContainer'})
		.prop('persist', () => true)
	;

	HZWorldContainer.__init = async function(cb) {

		const world = await HZWorldContainer.findOrCreateOfType({name: 'world'}, {
			name: 'world',
			rows: -1,
			cols: -1
    });

		cb();

	}

	return HZWorldContainer;
};
