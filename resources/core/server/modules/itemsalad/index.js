module.exports = (horizon, config) => {
     
    const HZItem = horizon.require('item');
  
    class HZItemSalad extends HZItem {
  
    }
  
    HZItemSalad
      .inheritFields(HZItem)
      .inheritActions(HZItem)
      .field('_type', {default: 'ItemSalad'})
      .field('image', {default: 'salad'})
      .field('shape', {default: {"grid":[{"x":-47,"y":-3.0972222222221717},{"x":53,"y":-3.0972222222221717}],"offset":{"x":47,"y":3.0972222222221717}}})
      .field('name',  {default: 'itemsalad'})
      .field('label', {default: 'Salade'})
    ;
  
    return HZItemSalad;
  
  }