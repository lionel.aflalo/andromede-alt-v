const alt = require('alt');

module.exports = (horizon, config) => {

  class HZSyncPlayer {
    
    syncFacialAnim(player, anim) {
      player.setSyncedMeta('setting.facial_anim', anim);
    }
    
  }

  const instance = new HZSyncPlayer();

  instance.__init = function(cb) {

    alt.onClient('hz:sync:player:setting.facial_anim', (player, anim) => {
      instance.syncFacialAnim(player, anim);
    });

    cb();
  }

  return instance;
}