module.exports = (horizon, config) => {
   
  const HZItem = horizon.require('item');

  class HZItemPlayboyjune extends HZItem {

  }

  HZItemPlayboyjune
    .inheritFields(HZItem)
    .inheritActions(HZItem)
    .field('_type', {default: 'ItemPlayboyjune'})
    .field('image', {default: 'playboyjune'})
    .field('shape', {default: {"grid":[],"offset":{"x":3.000000000000014,"y":38.240213523131615}}})
    .field('name',  {default: 'itemplayboyjune'})
    .field('label', {default: 'Playboy Juin'})
  ;

  return HZItemPlayboyjune;

}