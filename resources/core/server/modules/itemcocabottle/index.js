module.exports = (horizon, config) => {
     
    const HZItem = horizon.require('item');
  
    class HZItemCocabottle extends HZItem {
  
      async use(cb, identity, count = 1) {

        const player   = identity.getAltPlayer();
        const toUpdate = [];

        identity.status.hunger += 0.15;
        identity.status.thirst += 0.4;

        if(identity.status.hunger > 1)
          identity.status.hunger = 1;

        if(identity.status.thirst > 1)
          identity.status.thirst = 1;

        this.count--;

        if(this.count === 0) {

          const container = await horizon.modules.container.findOne({_id: this.container});

          if(container) {
            container.removeItem(this);
            toUpdate.push(container._idStr);
          }

          this.delete();

        } else {
          
          this.save();

          toUpdate.push(this._idStr);

        }
        
        identity.cache(true);

        horizon.updateCache(player, ...toUpdate);

        cb();
      }

    }
  
    HZItemCocabottle
      .inheritFields(HZItem)
      .inheritActions(HZItem)
      .field('_type', {default: 'ItemCocabottle'})
      .field('image', {default: 'cocabottle'})
      .field('shape', {default: {"grid":[{"x":-8.449894796014632,"y":-4.5501052039853676},{"x":-8.449894796014632,"y":95.44989479601463},{"x":-8.449894796014632,"y":195.44989479601463}],"offset":{"x":8.449894796014632,"y":4.5501052039853676}}})
      .field('name',  {default: 'itemcocabottle'})
      .field('label', {default: 'Bouteille de coca'})
    ;
  
    return HZItemCocabottle;
  
  }