module.exports = (horizon, config) => {
     
    const HZItem = horizon.require('item');
  
    class HZItemBait extends HZItem {
  
    }
  
    HZItemBait
      .inheritFields(HZItem)
      .inheritActions(HZItem)
      .field('_type', {default: 'ItemBait'})
      .field('image', {default: 'bait'})
      .field('shape', {default: {"grid":[{"x":-0.9999999999998863,"y":-17.90459770114944}],"offset":{"x":0.9999999999998863,"y":17.90459770114944}}})
      .field('name',  {default: 'itembait'})
      .field('label', {default: 'Appât'})
    ;
  
    return HZItemBait;
  
  }