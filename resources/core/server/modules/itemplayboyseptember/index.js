module.exports = (horizon, config) => {
   
  const HZItem = horizon.require('item');

  class HZItemPlayboyseptember extends HZItem {

  }

  HZItemPlayboyseptember
    .inheritFields(HZItem)
    .inheritActions(HZItem)
    .field('_type', {default: 'ItemPlayboyseptember'})
    .field('image', {default: 'playboyseptember'})
    .field('shape', {default: {"grid":[],"offset":{"x":6,"y":39.66019417475718}}})
    .field('name',  {default: 'itemplayboyseptember'})
    .field('label', {default: 'Playboy Septembre'})
  ;

  return HZItemPlayboyseptember;

}