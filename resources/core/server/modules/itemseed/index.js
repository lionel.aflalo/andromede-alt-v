'use strict';

const alt   = require('alt');
const async = require('async');

module.exports = function(horizon, config) {

  const Utils            = horizon.require('utils');
  const HZContainer      = horizon.require('container');
  const HZWorldContainer = horizon.require('worldcontainer');
  const HZItem           = horizon.require('item');
  const HZArea           = horizon.require('area');

	class HZItemSeed extends HZItem {

		async plant(cb, identity, ...args) {

      if(!identity) {
        cb();
        return;
      }

      const player    = identity.getAltPlayer();
      const inAreaIds = HZArea.playerAreas.get(player) || [];
      const areas     = await HZArea.find({_id: {$in: inAreaIds.map(e => HZArea.id(e))}});
      let   inFertile = false;

      for(let i=0; i<areas.length; i++) {
        
        if(areas[i].tags.indexOf('fertile') !== -1) {
          inFertile = true;
          break;
        }
      }

      if(!inFertile) {
        alt.emitClient(player, 'hz:notification:create', '<span style="color:reed">Action impossible, zone non fertile</span>');
        cb();
        return;
      }

      this.count--;

      if(this.count === 0) {
        
        const container = await HZContainer.findOne({_id: this.container});
        
        await container.removeItem(this);
        await this.delete();
      
      } else {

        await this.save();
        horizon.updateCache(player, this._idStr);
        
      }

      alt.emitClient(player, 'hz:seed:doplantanim');

      setTimeout(async () => {

        const item = new (this.grow())({count: 1});

        await item.save();

        const world = await HZWorldContainer.findOne({name: 'world'});

        await world.addItem(item, player.pos);

        horizon.updateCache(player, this.container.toString());
        world.updateCube(item.world.position);

      }, 8000);

      cb();

    }
    
    grow() {
      return null;
    }

	}

	HZItemSeed
		.inheritFields(HZItem)
		.inheritActions(HZItem)
    .field('_type', {default: 'ItemSeed'})
    .field('image', {default: 'seed'})
    .field('shape', {default: {"grid":[{"x":-32.0982905982906,"y":-6.901709401709397}],"offset":{"x":32.0982905982906,"y":6.901709401709397}}})
    .field('name',  {default: 'itemseed'})
    .field('label', {default: 'Graine'})
    .field('removeable',   {default: true})
    .field('giveable',     {default: true})
    .field('compressible', {default: true})
    .field('equipable',    {default: false})
    .field('mass',         {default: 1})
    .field('volume',       {default: {x: 0.25, y: 0.25, z: 0.25}})
    .action('plant', 'plant', true)
	;

	HZItemSeed.__init = function(cb) {

		cb();

	}

	return HZItemSeed;
};
