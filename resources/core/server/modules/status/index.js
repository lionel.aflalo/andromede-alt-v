const alt = require('alt');

module.exports = (horizon, config) => {

  const $          = horizon.$;
  const HZIdentity = horizon.require('identity');

  class HZStatus {

    constructor() {
      this.status = [];
    }

    register(name, cb, icon, color) {
      this.status.push({name, cb, icon, color});
    }

    async tick() {

      const players        = alt.Player.all;
      const identityIdsStr = players.map(e => e.getSyncedMeta('identity'));
      const identityIds    = identityIdsStr.map(e => e ? HZIdentity.id(e) : null);
      const promises       = identityIds.map(e => HZIdentity.findOne({_id: e}));
      const identities     = await Promise.all(promises);

      for(let i=0; i<identities.length; i++) {

        const identity = identities[i];

        if(!identity)
          return;

        const player = identity.getAltPlayer();
        
        for(let j=0; j<this.status.length; j++) {

          const {name, cb}      = this.status[j];
          const status          = identity.status[name] === undefined ? 1.0 : identity.status[name];
          identity.status[name] = cb(player, identity, status);
          
        }

        identity.cache(true);
      }

    }

  }

  const instance = new HZStatus();

  instance.__init = function(cb) {

    instance.register('hunger', (player, identity, prev) => {
      
      let next = prev - 1 / (3600 * 4);

      if(next < 0)
        next = 0;

      if(next === 0) {
        
        player.health = 0;

        if(prev !== 0)
          alt.emitClient(player, 'hz:notification:create', 'Vous êtes dans le coma à cause du manque de nourriture');
      }

      return next;

    }, 'utensils', 'rgb(236, 200, 78)');

    instance.register('thirst', (player, identity, prev) => {
      
      let next = prev - 1 / (3600 * 3);

      if(next < 0)
        next = 0;

      if(prev !== 0 && next === 0)
        player.health = 0;

      if(next === 0) {
        
        player.health = 0;

        if(prev !== 0)
          alt.emitClient(player, 'hz:notification:create', 'Vous êtes dans le coma à cause du manque d\'eau');
      }

      return next;

    }, 'tint', 'rgb(53, 165, 240)');

    setInterval(() => instance.tick(), 1000);

    setInterval(() => {

      alt.Player.all.forEach(async player => {

        const identityIdStr = player.getSyncedMeta('identity');
        const identityId    = HZIdentity.id(identityIdStr);
        const identity      = await HZIdentity.findOne({_id: identityId});

        if(!identity)
          return;

        const status = [];
  
        for(let j=0; j<instance.status.length; j++) {
          const {name, icon, color} = instance.status[j];
          status.push({name, val: identity.status[name], icon, color});
        }
  
        alt.emitClient(player, 'hz:status:update', status);
  
      });

    }, 15000);

    horizon.on('player.inited', async player => {

      const identityIdStr = player.getSyncedMeta('identity');
      const identityId    = HZIdentity.id(identityIdStr);
      const identity      = await HZIdentity.findOne({_id: identityId});
      const status        = [];

      for(let j=0; j<instance.status.length; j++) {
        const {name, icon, color} = instance.status[j];
        status.push({name, val: identity.status[name], icon, color});
      }

      alt.emitClient(player, 'hz:status:update', status);

    });

    cb();
  }

  return instance;
}