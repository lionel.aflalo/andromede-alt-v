module.exports = (horizon, config) => {
     
    const HZItem = horizon.require('item');
  
    class HZItemOrangejuice extends HZItem {
  
    }
  
    HZItemOrangejuice
      .inheritFields(HZItem)
      .inheritActions(HZItem)
      .field('_type', {default: 'ItemOrangejuice'})
      .field('image', {default: 'orangejuice'})
      .field('shape', {default: {"grid":[{"x":-12.000000000000341,"y":-5.876569037657191}],"offset":{"x":12.000000000000341,"y":5.876569037657191}}})
      .field('name',  {default: 'itemorangejuice'})
      .field('label', {default: 'Jus d\'orange'})
    ;
  
    return HZItemOrangejuice;
  
  }