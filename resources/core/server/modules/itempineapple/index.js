module.exports = (horizon, config) => {
     
    const HZItem = horizon.require('item');
  
    class HZItemPineapple extends HZItem {
  
      async use(cb, identity, count = 1) {

        const player   = identity.getAltPlayer();
        const toUpdate = [];

        identity.status.hunger += 0.1;

        if(identity.status.hunger > 1)
          identity.status.hunger = 1;

        if(identity.status.thirst > 1)
          identity.status.thirst = 1;

        this.count--;

        if(this.count === 0) {

          const container = await horizon.modules.container.findOne({_id: this.container});

          if(container) {
            container.removeItem(this);
            toUpdate.push(container._idStr);
          }

          this.delete();

        } else {
          
          this.save();

          toUpdate.push(this._idStr);

        }
        
        identity.cache(true);

        horizon.updateCache(player, ...toUpdate);

        cb();
      }

    }
  
    HZItemPineapple
      .inheritFields(HZItem)
      .inheritActions(HZItem)
      .field('_type', {default: 'ItemPineapple'})
      .field('image', {default: 'pineapple'})
      .field('shape', {default: {"grid":[{"x":-10.66023942937818,"y":-7.33976057062182},{"x":-10.66023942937818,"y":92.66023942937818}],"offset":{"x":10.66023942937818,"y":7.33976057062182}}})
      .field('name',  {default: 'itempineapple'})
      .field('label', {default: 'Ananas'})
    ;
  
    return HZItemPineapple;
  
  }