'use strict';

const alt = require('alt');

module.exports = function(horizon, config) {
	
	const HZContainer = horizon.require('container');
 
	class HZSimpleContainer extends HZContainer {

	}

	HZSimpleContainer
		.inheritFields(HZContainer)
		.field('_type', {default: 'SimpleContainer'})
  ;
  
	return HZSimpleContainer;
};
