module.exports = (horizon, config) => {
     
    const HZItem = horizon.require('item');
  
    class HZItemMoney extends HZItem {
  
    }
  
    HZItemMoney
      .inheritFields(HZItem)
      .inheritActions(HZItem)
      .field('_type', {default: 'ItemMoney'})
      .field('image', {default: 'money'})
      .field('shape', {default: {"grid":[{"x":-1.0000000000001137,"y":-7.6875000000001705}],"offset":{"x":1.0000000000001137,"y":7.6875000000001705}}})
      .field('name',  {default: 'itemmoney'})
      .field('label', {default: 'Money'})
    ;
  
    return HZItemMoney;
  
  }