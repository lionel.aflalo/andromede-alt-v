'use strict';

const alt = require('alt');

module.exports = function(horizon, config) {

	const HZContainer    = horizon.require('container');
	const HZItemBanknote = horizon.require('itembanknote');

	class HZIdentityBaseContainer extends HZContainer {

	}

	const salaryGenerator = async function(now, data, container) {

		const last = data.last || now;

		if((now - data.last) < config.salaryInterval)
			return null;

		const identity = await horizon.modules.identity.findOne({_id: container.identity});

		if(!identity)
			return null;

		const player   = identity.getAltPlayer();

		if(!player)
			return null;

		let count = 0;
		
		for(const role in config.salaries) {

			if(identity.hasRole(role))
				count += config.salaries[role];
		
		}

		data.last = now;

		return new HZItemBanknote({count});
	}

	HZIdentityBaseContainer
    .inheritFields(HZContainer)
    .inheritActions(HZContainer)
		.field('_type',    {default: 'IdentityBaseContainer'})
		.field('name',     {default:'identity_base'})
    .field('rows',     {default: 4})
    .field('cols',     {default: 8})
		.field('label',    {default: 'Personnage'})
		.field('identity', {required: true})
		.generator('salary', salaryGenerator);
	;

	return HZIdentityBaseContainer;

};
