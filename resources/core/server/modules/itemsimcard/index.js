const async = require('async');

module.exports = (horizon, config) => {
     
    const Utils  = horizon.require('utils');
    const HZItem = horizon.require('item');
  
    class HZItemSimcard extends HZItem {
  
      initNumber(cb) {

        let found = false;
  
        async.whilst(
          ()    => !found,
          (cb2) => {
  
            let number = Utils.getRandomInt(10000, 99999).toString();
  
            HZItemSimcard.findOne({number})
              .then(simcard => {
  
                if(simcard == null)
                  found = true
  
                cb2(null, number)
              })
            ;
  
          },
          cb
        );
      }


    }
  
    HZItemSimcard
      .inheritFields(HZItem)
      .inheritActions(HZItem)
      .field('_type',  {default: 'ItemSimcard'})
      .field('image',  {default: 'simcard'})
      .field('shape',  {default: {"grid":[{"x":-2,"y":-19.000000000000004}],"offset":{"x":2,"y":19}}})
      .field('name',   {default: 'itemsimcard'})
      .field('label',  {default: 'Carte SIM'})
      .field('number', {default: null, init: 'initNumber'})
      .prop('persist', () => false)
    ;

    return HZItemSimcard;
  
  }