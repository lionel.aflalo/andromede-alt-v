module.exports = (horizon, config) => {
     
    const HZItem = horizon.require('item');
  
    class HZItemStud extends HZItem {
  
    }
  
    HZItemStud
      .inheritFields(HZItem)
      .inheritActions(HZItem)
      .field('_type', {default: 'ItemStud'})
      .field('image', {default: 'stud'})
      .field('shape', {default: {"grid":[{"x":-39,"y":-13.635994587280038},{"x":-39,"y":86.36400541271996},{"x":61,"y":-13.635994587280038},{"x":61,"y":86.36400541271996}],"offset":{"x":39,"y":13.635994587280038}}})
      .field('name',  {default: 'itemstud'})
      .field('label', {default: 'Stud'})
    ;
  
    return HZItemStud;
  
  }