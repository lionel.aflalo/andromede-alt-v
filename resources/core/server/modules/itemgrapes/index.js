module.exports = (horizon, config) => {
     
    const HZItem = horizon.require('item');
  
    class HZItemGrapes extends HZItem {
  
      async use(cb, identity, count = 1) {

        const player   = identity.getAltPlayer();
        const toUpdate = [];

        identity.status.hunger += 0.05;

        if(identity.status.hunger > 1)
          identity.status.hunger = 1;

        if(identity.status.thirst > 1)
          identity.status.thirst = 1;

        this.count--;

        if(this.count === 0) {

          const container = await horizon.modules.container.findOne({_id: this.container});

          if(container) {
            container.removeItem(this);
            toUpdate.push(container._idStr);
          }

          this.delete();

        } else {
          
          this.save();

          toUpdate.push(this._idStr);

        }
        
        identity.cache(true);

        horizon.updateCache(player, ...toUpdate);

        cb();
      }

    }
  
    HZItemGrapes
      .inheritFields(HZItem)
      .inheritActions(HZItem)
      .field('_type', {default: 'ItemGrapes'})
      .field('image', {default: 'grapes'})
      .field('shape', {default: {"grid":[{"x":-2.9999999999998863,"y":-14.066037735849022}],"offset":{"x":2.9999999999998863,"y":14.066037735849022}}})
      .field('name',  {default: 'itemgrapes'})
      .field('label', {default: 'Raisin'})
    ;
  
    return HZItemGrapes;
  
  }