const request = require('request');
const Discord = require('discord.js');

module.exports = (horizon, config) => {

  const HZPlayer = horizon.require('player');

  class HZDiscord {

    static get config() { return config; }

    constructor() {

      this.config = config;
      this.client = new Discord.Client();

      this.client.on('ready', () => {
        
        console.log(`[Discord] Logged in as ${this.client.user.tag}!`);
        
        const tick = () => {

          const guild       = this.client.guilds.get(config.guild);
          const whitelisted = guild.roles.get(config.whitelist).members.map(e => e.user.id);

          HZPlayer
            .find({
              'identifiers.discord': {$in: whitelisted},
              roles: {$nin: ['whitelist']}
            })
            .then((players) => {

              for(let i=0; i<players.length; i++) {

                console.log('[Discord] Whitelisting ' + players[i].name);
                
                players[i].roles.push('whitelist');
                players[i].save();

              }

            })
          ;

          HZPlayer
            .find({
              'identifiers.discord': {$nin: whitelisted},
              roles: {$in: ['whitelist']}
            })
            .then((players) => {

              for(let i=0; i<players.length; i++) {

                console.log('[Discord] Removing ' + players[i].name + ' from whitelist');
                
                players[i].roles = players[i].roles.filter(e => e !== 'whitelist');
                players[i].save();

              }

            })
          ;
          
        };

        setInterval(tick, 60 * 1000);

      });

      this.client.login(config.token);
    }

    log(msg) {
      // this.client.channels.get(config.logs).send(msg);
    }

  }

  return new HZDiscord();

}
