module.exports = (horizon, config) => {
     
    const HZItem = horizon.require('item');
  
    class HZItemPotatoes extends HZItem {
  
    }
  
    HZItemPotatoes
      .inheritFields(HZItem)
      .inheritActions(HZItem)
      .field('_type', {default: 'ItemPotatoes'})
      .field('image', {default: 'potatoes'})
      .field('shape', {default: {"grid":[{"x":-2.9999999999993747,"y":-15.71575342465735}],"offset":{"x":2.9999999999993747,"y":15.71575342465735}}})
      .field('name',  {default: 'itempotatoes'})
      .field('label', {default: 'Pommes de terre'})
    ;
  
    return HZItemPotatoes;
  
  }