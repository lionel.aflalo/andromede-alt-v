module.exports = (horizon, config) => {
   
  const HZItem = horizon.require('item');

  class HZItemPlayboymay extends HZItem {

  }

  HZItemPlayboymay
    .inheritFields(HZItem)
    .inheritActions(HZItem)
    .field('_type', {default: 'ItemPlayboymay'})
    .field('image', {default: 'playboymay'})
    .field('shape', {default: {"grid":[],"offset":{"x":6.999999999999986,"y":38.87116729424429}}})
    .field('name',  {default: 'itemplayboymay'})
    .field('label', {default: 'Playboy Mai'})
  ;

  return HZItemPlayboymay;

}