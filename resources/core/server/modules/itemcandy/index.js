module.exports = (horizon, config) => {
     
    const HZItem = horizon.require('item');
  
    class HZItemCandy extends HZItem {
  
    }
  
    HZItemCandy
      .inheritFields(HZItem)
      .inheritActions(HZItem)
      .field('_type', {default: 'ItemCandy'})
      .field('image', {default: 'candy'})
      .field('shape', {default: {"grid":[{"x":-9.000000000000227,"y":-7.49160671462846}],"offset":{"x":9.000000000000227,"y":7.49160671462846}}})
      .field('name',  {default: 'itemcandy'})
      .field('label', {default: 'Bonbon'})
    ;
  
    return HZItemCandy;
  
  }