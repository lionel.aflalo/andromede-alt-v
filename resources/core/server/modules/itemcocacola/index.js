module.exports = (horizon, config) => {
     
    const HZItem = horizon.require('item');
  
    class HZItemCocacola extends HZItem {
  
      async use(cb, identity, count = 1) {

        const player   = identity.getAltPlayer();
        const toUpdate = [];

        identity.status.hunger += 0.075;
        identity.status.thirst += 0.1;

        if(identity.status.hunger > 1)
          identity.status.hunger = 1;

        if(identity.status.thirst > 1)
          identity.status.thirst = 1;

        this.count--;

        if(this.count === 0) {

          const container = await horizon.modules.container.findOne({_id: this.container});

          if(container) {
            container.removeItem(this);
            toUpdate.push(container._idStr);
          }

          this.delete();

        } else {
          
          this.save();

          toUpdate.push(this._idStr);

        }
        
        identity.cache(true);

        horizon.updateCache(player, ...toUpdate);

        cb();
      }


    }
  
    HZItemCocacola
      .inheritFields(HZItem)
      .inheritActions(HZItem)
      .field('_type', {default: 'ItemCocacola'})
      .field('image', {default: 'cocacola'})
      .field('shape', {default: {"grid":[{"x":-25,"y":-4.474813432835845}],"offset":{"x":25,"y":4.474813432835845}}})
      .field('name',  {default: 'itemcocacola'})
      .field('label', {default: 'Coca-Cola'})
    ;
  
    return HZItemCocacola;
  
  }