module.exports = (horizon, config) => {

	const HZBankLoanFormulas = {
		
		standard: [
			{days: 1  * 7, percent: 2},
			{days: 4  * 7, percent: 5},
			{days: 8  * 7, percent: 9},
			{days: 16 * 7, percent: 17},
			{days: 20 * 7, percent: 21},
		],

		pro: [
			{days: 1  * 7, percent: 1},
			{days: 4  * 7, percent: 3},
			{days: 8  * 7, percent: 5},
			{days: 16 * 7, percent: 9},
			{days: 20 * 7, percent: 13},
		]

	};

	return HZBankLoanFormulas;

}