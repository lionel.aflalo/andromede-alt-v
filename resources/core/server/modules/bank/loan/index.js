'use strict';

module.exports = function(horizon, config) {

  const HZEntity = horizon.require('entity');
	
	class HZBankLoan extends HZEntity {

  	initNumber(cb) {

  		HZBankLoan
  			.findOfType({}, null, 1, {number: -1})
  			.then(loans => {
  				cb(null, loans.length === 0 ? 0 : loans[0].number + 1);
  			});
  		;

  	}

	}

	HZBankLoan
		.collection('bank.loans')
		.inheritFields(HZEntity)
		.field('_type',    {default: 'BankLoan'})
		.field('receiver', {required: true})
		.field('amount',   {required: true})
		.field('remaining', {default: function(){ return this.amount; }})
		.field('autoWire', {required: true})
		.field('formula',  {required: true})
		.field('number',   {default: null, init: 'initNumber'})
		.field('active',   {default: true})
	;

	return HZBankLoan;

}
