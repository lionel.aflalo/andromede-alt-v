const async = require('async');

module.exports = (horizon, config) => {

	const HZBankTransaction = horizon.require('bank/transaction');

	const HZBankAccountLimits = [
		{
			name: 'basic',
			data: [
				{time: 24, amount: 100},
			],
		},
		{
			name: 'standard',
			data: [
				{time: 24, amount: 200},
			],
		},
		{
			name: 'advanced',
			data: [
				{time: 24, amount: 500},
			],
		},
		{
			name: 'unlimited',
			data: [],
		},
	];

	HZBankAccountLimits.check = function(account, amount) {

		return new Promise((resolve, reject) => {

			const now    = Math.floor(new Date / 1000);
			const limits = this.find(e => e.name === account.limits) || null;

			if(limits === null) {
				resolve(true);
				return;
			}

			const tasks     = [];
			const timestamp = Math.floor(new Date / 1000);

			for(let i=0; i<limits.data.length; i++) {

				(limit => {

					tasks.push(cb => {

						const start = timestamp - limit.time * 60 * 60;
						const end   = timestamp;

						HZBankTransaction
							.find({
                $or: [
                  {sender  : account._id, timestamp: {$gte: start, $lte: end}},
                  {receiver: account._id, timestamp: {$gte: start, $lte: end}},
                ]
							})
							.then(transactions => {
								
								transactions = transactions.filter(e => {
									return e.operationTags.indexOf('loan.payback') === -1;
								});

								let total = amount;
								
								transactions.forEach(e => {
									if((e.sender.equals(account._id) && e.amount > 0) || (e.receiver.equals(account._id) && e.amount < 0))
										total += Math.abs(e.amount)
								});

								cb(null, total <= limit.amount);

							})
						;

					});

				})(limits.data[i]);

			}

			async.parallel(tasks, (err, results) => {

				const success = results.reduce((a, e) => a && e, true);

				resolve(success);

			});

		});


	}.bind(HZBankAccountLimits);

	return HZBankAccountLimits;

}