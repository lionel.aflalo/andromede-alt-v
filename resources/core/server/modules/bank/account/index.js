'use strict';

const async = require('async');

module.exports = function(horizon, config) {

  const HZAccount           = horizon.require('account');
  const HZBankTransaction   = horizon.require('bank/transaction');
  const HZBankUser          = horizon.require('bank/user');
  const HZBankAccountLimits = horizon.require('bank/account/limits');
  const HZIdentity          = horizon.require('identity');
  const HZItemBankNote      = horizon.require('itembanknote');
  const HZWorldContainer    = horizon.require('worldcontainer');

	class HZBankAccount extends HZAccount {
		
		transfer(receiver, amount, object = null, initiator = null, tags = []) {

			this.money -= amount;
			receiver.money += amount;

			const transaction = new HZBankTransaction({
				initiator     : initiator,
				sender        : this._id,
				receiver      : receiver._id,
				timestamp     : Math.floor(+new Date / 1000),
				operationType : 'transfer',
				operationTags : tags,
				object        : object ? object : 'Virement de #' + this.number.toString().padStart(8, '0') + ' à #' + receiver.number.toString().padStart(8, '0'),
				amount        : amount,
			})

			return Promise.all([
				this.save(),
				receiver.save(),
				transaction.save(),
			]);

		}

		async withdraw(amount, identity, initiator = null) {

			return new Promise((resolve, reject) => {

				if(isNaN(amount)) {
					resolve(false);
					return;
				}

				const player = identity.getAltPlayer();

				this.money -= amount;

				HZBankUser
					.findOne({_id: initiator})
					.then(user => {

						if(user === null) {
							resolve(false);
							return;
						}

						const transaction = new HZBankTransaction({
							initiator     : initiator,
							sender        : this._id,
							receiver      : this._id,
							timestamp     : Math.floor(+new Date / 1000),
							operationType : 'withdraw',
							object        : 'Retrait',
							amount        : amount,
						})

						Promise.all([
							this.save(),
							transaction.save(),
						])
						.then(async () => {

							const banknote = new HZItemBankNote({count: amount});
							const matches  = identity.queryItem({_type: banknote._type}).filter(e => (e.item.count + amount) <= e.item.limit);

							if(matches.length > 0) {

								const match  = matches[0].item;
								match.count += amount;

								await match.save();

								horizon.updateCache(player, match._idStr);

								resolve(true);
								return;
							}

							const freeSpace = identity.getFreeSpace(banknote);

							if(freeSpace) {
								
								await banknote.save();
								await freeSpace.container.addItem(banknote, freeSpace.x, freeSpace.y);
								
								horizon.updateCache(player, freeSpace.container._idStr);

								resolve(true);
								return;

							} else {

								resolve(false);
								return;

							}

						});

					})
				;

			});

		}

		removeCash(amount, initiator = null, object = 'cash out') {

			this.money -= amount;

			const transaction = new HZBankTransaction({
				initiator     : initiator,
				sender        : this._id,
				receiver      : this._id,
				timestamp     : Math.floor(+new Date / 1000),
				operationType : 'withdraw',
				object        : object,
				amount        : amount,
			});

			return Promise.all([
				this.save(),
				transaction.save(),
			])

		}

		tryTransfer(receiver, amount, object = null, initiator = null, bypassInitiator = false) {

			return new Promise((resolve, reject) => {

				if(isNaN(amount) || amount <= 0 || this.money < amount) {
					
					resolve(false);
				
				} else {

					if(initiator === null) {
					
						this
							.transfer(receiver, amount, object, null)
							.then(() => resolve(true))
						;

					} else {

						if(bypassInitiator) {

							HZBankAccountLimits
								.check(this, amount)
								.then(success => {

									if(success) {

										this
											.transfer(receiver, amount, object, initiator)
											.then(() => resolve(true))
										;

									} else resolve(false);

								})
							;

						} else {

							HZBankUser
								.findOne({_id: initiator})
								.then(user => {

									if(user.accounts.findIndex(e => e.equals(this._id)) !== -1) {

											HZBankAccountLimits
												.check(this, amount)
												.then(success => {

													if(success) {

														this
															.transfer(receiver, amount, object, initiator)
															.then(() => resolve(true))
														;

													} else resolve(false);

												})
											;

									} else resolve(false);

								})
							;

						}

					}

				}

			});
		}

		tryWithdraw(amount, identity, initiator = null) {

			return new Promise((resolve, reject) => {

				if(amount <= 0 || this.money < amount) {
					
					resolve(false);
				
				} else {

					if(identity instanceof HZIdentity) {

						HZBankAccountLimits
							.check(this, amount)
							.then(success => {

								if(success) {

									this
										.withdraw(amount, identity, initiator)
										.then(resolve)
									;

								} else resolve(false);

							})
						;

					} else resolve(false);

				}

			});
		}

  	initNumber(cb) {

  		HZBankAccount
  			.findOfType({}, null, 1, {number: -1})
  			.then(accounts => {
  				cb(null, accounts.length === 0 ? 0 : accounts[0].number + 1);
  			});
  		;

  	}

	}

	HZBankAccount
		.collection('bank.accounts')
		.inheritFields(HZAccount)
		.field('_type',  {default: 'BankAccount'})
		.field('type',   {required: true})
		.field('label',  {default: ''})
		.field('number', {default: null, init: 'initNumber'})
		.field('limits', {required: true})
		.field('active', {default: true})
	;

	return HZBankAccount;

}
