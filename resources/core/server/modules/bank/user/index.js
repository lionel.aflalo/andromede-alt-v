'use strict';

module.exports = function(horizon, config) {

  const HZEnrolable = horizon.require('enrolable');

	class HZBankUser extends HZEnrolable {
		
	}

	HZBankUser
		.collection('bank.users')
		.inheritFields(HZEnrolable)
		.field('_type',       {default: 'BankUser'})
		//.field('address',     {required: true})
		//.field('phoneNumber', {required: true})
		.field('accounts',       {default: []})
		.field('token',          {default: null})
		.field('active',         {default: true})
		.field('residentNumber', {required: true})
	;

	return HZBankUser;

}
