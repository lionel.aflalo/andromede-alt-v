'use strict';

const urlify = require('urlify').create({
  addEToUmlauts:true,
  szToSs:true,
  spaces:"_",
  nonPrintable:"_",
  trim:true
});

module.exports = function(horizon, config) {

	const Utils       = horizon.require('utils');
  const HZBankUser = horizon.require('bank/user');

	class HZStandardBankUser extends HZBankUser {
		
	}

	HZStandardBankUser
		.inheritFields(HZBankUser)
		.field('_type',          {default: 'StandardBankUser'})
		.field('firstName',      {required: true})
		.field('lastName',       {required: true})
		.field('residentNumber', {required: true})
		.field('login',          {default: function() { return urlify(this.firstName.toLowerCase()) + '.' + urlify(this.lastName.toLowerCase()) + Utils.getRandomInt(0, 9999) }})
		.field('password',       {default: Utils.getRandomString(10)})
	;

	return HZStandardBankUser;

}
