'use strict';

const urlify = require('urlify').create({
  addEToUmlauts:true,
  szToSs:true,
  spaces:"_",
  nonPrintable:"_",
  trim:true
});


module.exports = function(horizon, config) {

	const Utils       = horizon.require('utils');
  const HZBankUser = horizon.require('bank/user');

	class HZProBankUser extends HZBankUser {
		
	}

	HZProBankUser
		.inheritFields(HZBankUser)
		.field('_type',         {default: 'ProBankUser'})
		.field('name',          {required: true})
		.field('societyNumber', {required: true})
		.field('login',          {default: function() { return urlify(this.name.toLowerCase()) + Utils.getRandomInt(0, 9999) }})
		.field('password',       {default: Utils.getRandomString(10)})
	;

	return HZProBankUser;

}
