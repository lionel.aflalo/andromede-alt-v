'use strict';

const async = require('async');

module.exports = function(horizon, config) {

  const HZEntity       = horizon.require('entity');
  const HZBankAccount  = horizon.require('bank/account');
  const HZBankLoan     = horizon.require('bank/loan');

	class HZBankAutoWire extends HZEntity {
		
		static get TIME_UNIT() { return 24 * 60 * 60;  }

		process() {

			return new Promise((resolve, reject) => {

				const data = {};

				async.parallel([

					cb => {

						HZBankAccount
							.findOne({_id: this.sender})
							.then(account => {
								data.sender = account;
								cb(null);
							})
						;

					},

					cb => {

						HZBankAccount
							.findOne({_id: this.receiver})
							.then(account => {
								data.receiver = account;
								cb(null);
							})
						;

					}

				], (err, results) => {

					data.sender
						.transfer(data.receiver, this.amount, 'auto wire from #' + data.sender.number.toString().padStart(8, '0') + ' to #' + data.receiver.number.toString().padStart(8, '0'), null, this.wireTags)
						.then(() => {

							this.lastIteration = Math.floor(new Date / 1000);

							if(this.iterationsLeft -1 >= 0)
								this.iterationsLeft--;

							this
								.save()
								.then(() => {

									HZBankLoan
										.findOneOfType({autoWire: this._id})
										.then(loan => {

											if(loan !== null) {
												
												loan.remaining -= this.amount;

												if(loan.remaining <= 0)
													loan.active = false;

												loan
													.save();

											}

											resolve();

										})
									;
								})
							;

						})
					;

				});

			});

		}

		static processAll() {

			return new Promise((resolve, reject) => {

				const date = new Date;

	      HZBankAutoWire
	      	.findOfType({
		        $or: [
		          {iterationsLeft : -1},
		          {iterationsLeft : {$gt: 0}}
		        ],
		        day: date.getDay(),
		        startDate: {$lte: Math.floor(date / 1000)}
		      })
		      .then(autoWires => {

		      	const tasks = [];
		      	let   count = 0;

		      	for(let i=0; i<autoWires.length; i++) {

		      		(autoWire => {
		      			
		      			const now     = Math.floor(new Date / 1000);
		      			const elapsed = now - autoWire.lastIteration;

		      			if(autoWire.lastIteration === -1 || elapsed > HZBankAutoWire.TIME_UNIT * autoWire.frequency) {
		      				tasks.push(cb => autoWire.process().then(() => cb(null)));
		      				count++;
		      			}

		      		})(autoWires[i]);

		      	}

		      	async.parallelLimit(tasks, 10, (err, results) => resolve(count));

		      })
		     ;

			});
		}

	}

	HZBankAutoWire
		.collection('bank.autowires')
		.inheritFields(HZEntity)
		.field('_type',          {default: 'BankAutoWire'})
		.field('sender',         {required: true})
		.field('receiver',       {required: true})
		.field('amount',         {required: true})
		.field('startDate',      {required: true})
		.field('frequency',      {default:  7})
		.field('iterations',     {default: -1})
		.field('day',            {default: 0})
		.field('iterationsLeft', {default: function() { return this.iterations; }})
		.field('lastIteration',  {default: -1})
		.field('wireTags',       {default: []})
		.field('active',         {default: true})
	;

	return HZBankAutoWire;

}
