module.exports = (horizon, config) => {

	const BankTransactionTypes = {
		credit   :  1,
		debit    : -1,
		withdraw : -1,
		transfer : -1,
	};

	return BankTransactionTypes;

}