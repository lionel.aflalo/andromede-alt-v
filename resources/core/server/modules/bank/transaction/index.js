'use strict';

module.exports = function(horizon, config) {

  const HZEntity = horizon.require('entity');

	class HZBankTransaction extends HZEntity {

	}

	HZBankTransaction
		.inheritFields(HZEntity)
		.collection('bank.transactions')
		.field('_type',         {default : 'BankTransaction'})
		.field('initiator',     {default : null})
		.field('sender',        {required: true})
		.field('receiver',      {required: true})
		.field('timestamp',     {required: true})
		.field('operationType', {required: true})
		.field('operationTags', {default : []})
		.field('object',        {required: true})
		.field('amount',        {required: true})
	;

	return HZBankTransaction;

}
