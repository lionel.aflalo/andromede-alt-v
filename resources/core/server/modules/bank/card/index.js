'use strict';

const async = require('async');

module.exports = function(horizon, config) {

	const Utils   = horizon.require('utils');
  const HZItem = horizon.require('item');

	class HZBankCard extends HZItem {
		
    initNumber(cb) {

    	let found = false;

      async.whilst(
        ()    => !found,
        (cb2) => {

          let number = '';

          for(let i=0; i<16; i++)
          	number += Utils.getRandomInt(0, 9).toString();

          HZBankCard.findOne({number})
            .then(card => {

              if(card === null)
                found = true

              cb2(null, number)
            })
          ;

        },
        cb
      );

    }

	}

	HZBankCard
		.inheritFields(HZItem)
		.inheritActions(HZItem)
		.field('_type',   {default: 'BankCard'})
		.field('name',    {default: 'bankcard'})
		.field('mass',    {default: 10})
		.field('volume',  {default: {x: 5, y: 10, z: 0.1}})
		.field('number',  {default: null, init: 'initNumber'})
		.field('cvv',     {default: () => Utils.getRandomInt(0, 999) .toString().padStart(3, '0')})
		.field('pinCode', {default: () => Utils.getRandomInt(0, 9999).toString().padStart(4, '0')})
		.field('account', {default: null})
		.field('user',    {default: null})
		.field('token',   {default: null})
		.field('image',   {default: 'bankcard'})
		.prop('label',    function() {

			const number = this.number || '';
			let fNumber  = '';

			for(let i=0; i < number.length; i++) {
				
				fNumber += number[i];
				
				if((i+1) % 4 === 0 && i !== 0 && (i+1) < number.length)
					fNumber += ' ';
			}

			return 'bankcard:' + fNumber + ' CVV:' + this.cvv;

		})
	;

	return HZBankCard;

}
