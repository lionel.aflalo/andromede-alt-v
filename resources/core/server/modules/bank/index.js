'use strict';

const async = require('async');

module.exports = function(horizon, config) {

	const HZBank             = {};
	const HZBankAutoWire     = horizon.require('bank/autowire');
	const HZBankCard         = horizon.require('itembankcard');

	HZBank.__init = function(cb) {
		
		const userTasks = [];

		for(let i=0; i<config.default.length; i++) {

			(userConfig => {

				userTasks.push(userCb => {

					userConfig._type
						.findOrCreate({
							name: userConfig.data.name
						}, userConfig.data)
						.then(user => {

							if(user.accounts.length > 0) {
								userCb(null);
								return;
							}

							const accountTasks = [];
							
							for(let i=0; i<userConfig.accounts.length; i++) {

								(accountData => {
									
									accountTasks.push(accountCb => {

										const account = new accountData._type(accountData.data);

										account
											.init()
											.then(() => {

												account
													.save()
													.then(() => {
														accountCb(null, account._id);
													})
												;
											})
										;

									});

								})(userConfig.accounts[i]);

							}

							async.series(accountTasks, (err, accounts) => {
									
								user.accounts = accounts;
								
								user
									.save()
									.then(() => userCb(null))
								;

							});

						})
					;

				});

			})(config.default[i]);

		}

		async.series(userTasks, (err, result) => {

			const tick = function() {

				HZBankAutoWire
					.processAll()
					.then((count) => {
						console.log(`Processed ${count} AutoWires`);
					})
				;

				return tick;
			}

			setInterval(tick(), 60 * 60 * 1000); // Check every hour

			cb();

		});

	}

	return HZBank;
}
