module.exports = (horizon, config) => {
     
    const HZItem = horizon.require('item');
  
    class HZItemBanknote extends HZItem {
  
    }
  
    HZItemBanknote
      .inheritFields(HZItem)
      .inheritActions(HZItem)
      .field('_type', {default: 'ItemBanknote'})
      .field('image', {default: 'banknote'})
      .field('shape', {default: {"grid":[{"x":-5,"y":-9}],"offset":{"x":5,"y":9}}})
      .field('name',  {default: 'itembanknote'})
      .field('label', {default: 'Billet de banque'})
      .prop('limit', () => 1500)
    ;
    
    return HZItemBanknote;
  
  }