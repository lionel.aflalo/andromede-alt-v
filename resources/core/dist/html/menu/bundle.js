var app = (function () {
    'use strict';

    function noop() { }
    function assign(tar, src) {
        // @ts-ignore
        for (const k in src)
            tar[k] = src[k];
        return tar;
    }
    function run(fn) {
        return fn();
    }
    function blank_object() {
        return Object.create(null);
    }
    function run_all(fns) {
        fns.forEach(run);
    }
    function is_function(thing) {
        return typeof thing === 'function';
    }
    function safe_not_equal(a, b) {
        return a != a ? b == b : a !== b || ((a && typeof a === 'object') || typeof a === 'function');
    }
    function not_equal(a, b) {
        return a != a ? b == b : a !== b;
    }
    function create_slot(definition, ctx, fn) {
        if (definition) {
            const slot_ctx = get_slot_context(definition, ctx, fn);
            return definition[0](slot_ctx);
        }
    }
    function get_slot_context(definition, ctx, fn) {
        return definition[1]
            ? assign({}, assign(ctx.$$scope.ctx, definition[1](fn ? fn(ctx) : {})))
            : ctx.$$scope.ctx;
    }
    function get_slot_changes(definition, ctx, changed, fn) {
        return definition[1]
            ? assign({}, assign(ctx.$$scope.changed || {}, definition[1](fn ? fn(changed) : {})))
            : ctx.$$scope.changed || {};
    }

    function append(target, node) {
        target.appendChild(node);
    }
    function insert(target, node, anchor) {
        target.insertBefore(node, anchor || null);
    }
    function detach(node) {
        node.parentNode.removeChild(node);
    }
    function destroy_each(iterations, detaching) {
        for (let i = 0; i < iterations.length; i += 1) {
            if (iterations[i])
                iterations[i].d(detaching);
        }
    }
    function element(name) {
        return document.createElement(name);
    }
    function text(data) {
        return document.createTextNode(data);
    }
    function space() {
        return text(' ');
    }
    function empty() {
        return text('');
    }
    function listen(node, event, handler, options) {
        node.addEventListener(event, handler, options);
        return () => node.removeEventListener(event, handler, options);
    }
    function attr(node, attribute, value) {
        if (value == null)
            node.removeAttribute(attribute);
        else
            node.setAttribute(attribute, value);
    }
    function children(element) {
        return Array.from(element.childNodes);
    }
    function set_data(text, data) {
        data = '' + data;
        if (text.data !== data)
            text.data = data;
    }

    let current_component;
    function set_current_component(component) {
        current_component = component;
    }
    function get_current_component() {
        if (!current_component)
            throw new Error(`Function called outside component initialization`);
        return current_component;
    }
    function onMount(fn) {
        get_current_component().$$.on_mount.push(fn);
    }
    function afterUpdate(fn) {
        get_current_component().$$.after_render.push(fn);
    }
    function onDestroy(fn) {
        get_current_component().$$.on_destroy.push(fn);
    }
    function setContext(key, context) {
        get_current_component().$$.context.set(key, context);
    }
    function getContext(key) {
        return get_current_component().$$.context.get(key);
    }

    const dirty_components = [];
    const resolved_promise = Promise.resolve();
    let update_scheduled = false;
    const binding_callbacks = [];
    const render_callbacks = [];
    const flush_callbacks = [];
    function schedule_update() {
        if (!update_scheduled) {
            update_scheduled = true;
            resolved_promise.then(flush);
        }
    }
    function add_binding_callback(fn) {
        binding_callbacks.push(fn);
    }
    function add_render_callback(fn) {
        render_callbacks.push(fn);
    }
    function flush() {
        const seen_callbacks = new Set();
        do {
            // first, call beforeUpdate functions
            // and update components
            while (dirty_components.length) {
                const component = dirty_components.shift();
                set_current_component(component);
                update(component.$$);
            }
            while (binding_callbacks.length)
                binding_callbacks.shift()();
            // then, once components are updated, call
            // afterUpdate functions. This may cause
            // subsequent updates...
            while (render_callbacks.length) {
                const callback = render_callbacks.pop();
                if (!seen_callbacks.has(callback)) {
                    callback();
                    // ...so guard against infinite loops
                    seen_callbacks.add(callback);
                }
            }
        } while (dirty_components.length);
        while (flush_callbacks.length) {
            flush_callbacks.pop()();
        }
        update_scheduled = false;
    }
    function update($$) {
        if ($$.fragment) {
            $$.update($$.dirty);
            run_all($$.before_render);
            $$.fragment.p($$.dirty, $$.ctx);
            $$.dirty = null;
            $$.after_render.forEach(add_render_callback);
        }
    }
    let outros;
    function group_outros() {
        outros = {
            remaining: 0,
            callbacks: []
        };
    }
    function check_outros() {
        if (!outros.remaining) {
            run_all(outros.callbacks);
        }
    }
    function on_outro(callback) {
        outros.callbacks.push(callback);
    }

    function get_spread_update(levels, updates) {
        const update = {};
        const to_null_out = {};
        const accounted_for = { $$scope: 1 };
        let i = levels.length;
        while (i--) {
            const o = levels[i];
            const n = updates[i];
            if (n) {
                for (const key in o) {
                    if (!(key in n))
                        to_null_out[key] = 1;
                }
                for (const key in n) {
                    if (!accounted_for[key]) {
                        update[key] = n[key];
                        accounted_for[key] = 1;
                    }
                }
                levels[i] = n;
            }
            else {
                for (const key in o) {
                    accounted_for[key] = 1;
                }
            }
        }
        for (const key in to_null_out) {
            if (!(key in update))
                update[key] = undefined;
        }
        return update;
    }
    function mount_component(component, target, anchor) {
        const { fragment, on_mount, on_destroy, after_render } = component.$$;
        fragment.m(target, anchor);
        // onMount happens after the initial afterUpdate. Because
        // afterUpdate callbacks happen in reverse order (inner first)
        // we schedule onMount callbacks before afterUpdate callbacks
        add_render_callback(() => {
            const new_on_destroy = on_mount.map(run).filter(is_function);
            if (on_destroy) {
                on_destroy.push(...new_on_destroy);
            }
            else {
                // Edge case - component was destroyed immediately,
                // most likely as a result of a binding initialising
                run_all(new_on_destroy);
            }
            component.$$.on_mount = [];
        });
        after_render.forEach(add_render_callback);
    }
    function destroy(component, detaching) {
        if (component.$$) {
            run_all(component.$$.on_destroy);
            component.$$.fragment.d(detaching);
            // TODO null out other refs, including component.$$ (but need to
            // preserve final state?)
            component.$$.on_destroy = component.$$.fragment = null;
            component.$$.ctx = {};
        }
    }
    function make_dirty(component, key) {
        if (!component.$$.dirty) {
            dirty_components.push(component);
            schedule_update();
            component.$$.dirty = blank_object();
        }
        component.$$.dirty[key] = true;
    }
    function init(component, options, instance, create_fragment, not_equal$$1, prop_names) {
        const parent_component = current_component;
        set_current_component(component);
        const props = options.props || {};
        const $$ = component.$$ = {
            fragment: null,
            ctx: null,
            // state
            props: prop_names,
            update: noop,
            not_equal: not_equal$$1,
            bound: blank_object(),
            // lifecycle
            on_mount: [],
            on_destroy: [],
            before_render: [],
            after_render: [],
            context: new Map(parent_component ? parent_component.$$.context : []),
            // everything else
            callbacks: blank_object(),
            dirty: null
        };
        let ready = false;
        $$.ctx = instance
            ? instance(component, props, (key, value) => {
                if ($$.ctx && not_equal$$1($$.ctx[key], $$.ctx[key] = value)) {
                    if ($$.bound[key])
                        $$.bound[key](value);
                    if (ready)
                        make_dirty(component, key);
                }
            })
            : props;
        $$.update();
        ready = true;
        run_all($$.before_render);
        $$.fragment = create_fragment($$.ctx);
        if (options.target) {
            if (options.hydrate) {
                // eslint-disable-next-line @typescript-eslint/no-non-null-assertion
                $$.fragment.l(children(options.target));
            }
            else {
                // eslint-disable-next-line @typescript-eslint/no-non-null-assertion
                $$.fragment.c();
            }
            if (options.intro && component.$$.fragment.i)
                component.$$.fragment.i();
            mount_component(component, options.target, options.anchor);
            flush();
        }
        set_current_component(parent_component);
    }
    class SvelteComponent {
        $destroy() {
            destroy(this, true);
            this.$destroy = noop;
        }
        $on(type, callback) {
            const callbacks = (this.$$.callbacks[type] || (this.$$.callbacks[type] = []));
            callbacks.push(callback);
            return () => {
                const index = callbacks.indexOf(callback);
                if (index !== -1)
                    callbacks.splice(index, 1);
            };
        }
        $set() {
            // overridden by instance, if it has props
        }
    }

    /* src\Menu.svelte generated by Svelte v3.5.1 */

    function create_fragment(ctx) {
    	var div2, div0, t0, t1, div1, current, dispose;

    	const default_slot_1 = ctx.$$slots.default;
    	const default_slot = create_slot(default_slot_1, ctx, null);

    	return {
    		c() {
    			div2 = element("div");
    			div0 = element("div");
    			t0 = text(ctx.label);
    			t1 = space();
    			div1 = element("div");

    			if (default_slot) default_slot.c();
    			div0.className = "label svelte-1bengl4";

    			div1.className = "items";
    			div2.className = "menu svelte-1bengl4";
    			dispose = listen(div0, "click", ctx.click_handler);
    		},

    		l(nodes) {
    			if (default_slot) default_slot.l(div1_nodes);
    		},

    		m(target, anchor) {
    			insert(target, div2, anchor);
    			append(div2, div0);
    			append(div0, t0);
    			append(div2, t1);
    			append(div2, div1);

    			if (default_slot) {
    				default_slot.m(div1, null);
    			}

    			current = true;
    		},

    		p(changed, ctx) {
    			if (!current || changed.label) {
    				set_data(t0, ctx.label);
    			}

    			if (default_slot && default_slot.p && changed.$$scope) {
    				default_slot.p(get_slot_changes(default_slot_1, ctx, changed, null), get_slot_context(default_slot_1, ctx, null));
    			}
    		},

    		i(local) {
    			if (current) return;
    			if (default_slot && default_slot.i) default_slot.i(local);
    			current = true;
    		},

    		o(local) {
    			if (default_slot && default_slot.o) default_slot.o(local);
    			current = false;
    		},

    		d(detaching) {
    			if (detaching) {
    				detach(div2);
    			}

    			if (default_slot) default_slot.d(detaching);
    			dispose();
    		}
    	};
    }

    function instance($$self, $$props, $$invalidate) {
    	let { label            = 'Menu' } = $$props;
      const resetCallbacks = [];

      setContext('main', {
        registerReset  : cb => resetCallbacks.push(cb),
        unregisterReset: cb => resetCallbacks.splice(resetCallbacks.indexOf(cb), 1),
        reset          : () => resetCallbacks.forEach(cb => cb()),
      });

    	let { $$slots = {}, $$scope } = $$props;

    	function click_handler(e) {
    		return resetCallbacks.forEach(cb => cb());
    	}

    	$$self.$set = $$props => {
    		if ('label' in $$props) $$invalidate('label', label = $$props.label);
    		if ('$$scope' in $$props) $$invalidate('$$scope', $$scope = $$props.$$scope);
    	};

    	return {
    		label,
    		resetCallbacks,
    		click_handler,
    		$$slots,
    		$$scope
    	};
    }

    class Menu extends SvelteComponent {
    	constructor(options) {
    		super();
    		init(this, options, instance, create_fragment, not_equal, ["label", "resetCallbacks"]);
    	}

    	get resetCallbacks() {
    		return this.$$.ctx.resetCallbacks;
    	}
    }

    /* src\MenuItemSlider.svelte generated by Svelte v3.5.1 */

    function create_fragment$1(ctx) {
    	var div4, div3, div0, t0, div1, t1, t2, div2, t3, span, t4_value = ctx.getLabel(ctx.value), t4, t5, div4_class_value, dispose;

    	return {
    		c() {
    			div4 = element("div");
    			div3 = element("div");
    			div0 = element("div");
    			t0 = space();
    			div1 = element("div");
    			t1 = text(ctx.label);
    			t2 = space();
    			div2 = element("div");
    			t3 = text(" < ");
    			span = element("span");
    			t4 = text(t4_value);
    			t5 = text(" >");
    			div0.className = "label-spacer svelte-4ahfs";
    			div1.className = "label-text svelte-4ahfs";
    			span.className = "label-value svelte-4ahfs";
    			div2.className = "label-slider svelte-4ahfs";
    			div3.className = "label svelte-4ahfs";
    			div4.className = div4_class_value = "" + (`menu-item menu-item-slider ${ctx.classes.join(' ')}`) + " svelte-4ahfs";

    			dispose = [
    				listen(div3, "click", ctx.onLabelClick),
    				listen(div4, "wheel", ctx.onItemWheel)
    			];
    		},

    		m(target, anchor) {
    			insert(target, div4, anchor);
    			append(div4, div3);
    			append(div3, div0);
    			append(div3, t0);
    			append(div3, div1);
    			append(div1, t1);
    			append(div3, t2);
    			append(div3, div2);
    			append(div2, t3);
    			append(div2, span);
    			append(span, t4);
    			append(div2, t5);
    		},

    		p(changed, ctx) {
    			if (changed.label) {
    				set_data(t1, ctx.label);
    			}

    			if ((changed.value) && t4_value !== (t4_value = ctx.getLabel(ctx.value))) {
    				set_data(t4, t4_value);
    			}

    			if ((changed.classes) && div4_class_value !== (div4_class_value = "" + (`menu-item menu-item-slider ${ctx.classes.join(' ')}`) + " svelte-4ahfs")) {
    				div4.className = div4_class_value;
    			}
    		},

    		i: noop,
    		o: noop,

    		d(detaching) {
    			if (detaching) {
    				detach(div4);
    			}

    			run_all(dispose);
    		}
    	};
    }

    function instance$1($$self, $$props, $$invalidate) {
    	

      let { menu     = null, name     = 'menu-item', type     = 'slider', label    = 'Menu item', value    = 0, index    = -1, depth    = 0, path     = [], classes  = [], onChange = null, options  = null, min      = -Infinity, max      = +Infinity, step     = 1 } = $$props;

      const mainContext = getContext('main');

      $$invalidate('path', path = path.slice(0));

      path.push(index);

      if(options instanceof Array) {

        $$invalidate('min', min  = 0);
        $$invalidate('max', max  = options.length - 1);
        $$invalidate('step', step = 1);

      }

      const reset = function(e) {

        const idx = classes.indexOf('active');

        if(idx !== -1)
          classes.splice(idx, 1);

        $$invalidate('classes', classes = classes.slice(0));
      };

      const onItemWheel = function(e) {
        
        e.preventDefault();

        const prev = value;

        if(e.deltaY < 0) {
          
          $$invalidate('value', value += step);

          if(value > max)
            $$invalidate('value', value = max);
            
        } else if(e.deltaY > 0) {
          
          $$invalidate('value', value -= step);

          if(value < min)
            $$invalidate('value', value = min);

        }

        if(value !== prev)
          window.parent.postMessage(JSON.stringify({action: 'menu_change', data: {name, type, label, value, index, depth, path}}), '*');
      };

      const onLabelClick = function(e) {
        e.preventDefault();
        mainContext.reset();
      };

      const getLabel = function(idx) {

        if(options instanceof Array && options.length > idx)
          return options[idx];
        else
          return idx.toString();
      };

      onMount(() => {
        mainContext.registerReset(reset);
      });

      onDestroy(() => {
        mainContext.unregisterReset(reset);
      });

    	$$self.$set = $$props => {
    		if ('menu' in $$props) $$invalidate('menu', menu = $$props.menu);
    		if ('name' in $$props) $$invalidate('name', name = $$props.name);
    		if ('type' in $$props) $$invalidate('type', type = $$props.type);
    		if ('label' in $$props) $$invalidate('label', label = $$props.label);
    		if ('value' in $$props) $$invalidate('value', value = $$props.value);
    		if ('index' in $$props) $$invalidate('index', index = $$props.index);
    		if ('depth' in $$props) $$invalidate('depth', depth = $$props.depth);
    		if ('path' in $$props) $$invalidate('path', path = $$props.path);
    		if ('classes' in $$props) $$invalidate('classes', classes = $$props.classes);
    		if ('onChange' in $$props) $$invalidate('onChange', onChange = $$props.onChange);
    		if ('options' in $$props) $$invalidate('options', options = $$props.options);
    		if ('min' in $$props) $$invalidate('min', min = $$props.min);
    		if ('max' in $$props) $$invalidate('max', max = $$props.max);
    		if ('step' in $$props) $$invalidate('step', step = $$props.step);
    	};

    	return {
    		menu,
    		name,
    		type,
    		label,
    		value,
    		index,
    		depth,
    		path,
    		classes,
    		onChange,
    		options,
    		min,
    		max,
    		step,
    		reset,
    		onItemWheel,
    		onLabelClick,
    		getLabel
    	};
    }

    class MenuItemSlider extends SvelteComponent {
    	constructor(options) {
    		super();
    		init(this, options, instance$1, create_fragment$1, safe_not_equal, ["menu", "name", "type", "label", "value", "index", "depth", "path", "classes", "onChange", "options", "min", "max", "step", "reset", "onItemWheel", "onLabelClick", "getLabel"]);
    	}

    	get reset() {
    		return this.$$.ctx.reset;
    	}

    	get onItemWheel() {
    		return this.$$.ctx.onItemWheel;
    	}

    	get onLabelClick() {
    		return this.$$.ctx.onLabelClick;
    	}

    	get getLabel() {
    		return this.$$.ctx.getLabel;
    	}
    }

    /* src\MenuItemInput.svelte generated by Svelte v3.5.1 */

    function create_fragment$2(ctx) {
    	var div1, div0, input, div1_class_value, dispose;

    	return {
    		c() {
    			div1 = element("div");
    			div0 = element("div");
    			input = element("input");
    			attr(input, "type", "text");
    			input.value = ctx.value;
    			input.placeholder = ctx.label;
    			input.className = "svelte-1q5o5vw";
    			div0.className = "label svelte-1q5o5vw";
    			div1.className = div1_class_value = "" + (`menu-item menu-item-input ${ctx.classes.join(' ')}`) + " svelte-1q5o5vw";

    			dispose = [
    				listen(input, "keyup", ctx.onItemChange),
    				listen(div0, "click", ctx.onLabelClick)
    			];
    		},

    		m(target, anchor) {
    			insert(target, div1, anchor);
    			append(div1, div0);
    			append(div0, input);
    		},

    		p(changed, ctx) {
    			if (changed.value) {
    				input.value = ctx.value;
    			}

    			if (changed.label) {
    				input.placeholder = ctx.label;
    			}

    			if ((changed.classes) && div1_class_value !== (div1_class_value = "" + (`menu-item menu-item-input ${ctx.classes.join(' ')}`) + " svelte-1q5o5vw")) {
    				div1.className = div1_class_value;
    			}
    		},

    		i: noop,
    		o: noop,

    		d(detaching) {
    			if (detaching) {
    				detach(div1);
    			}

    			run_all(dispose);
    		}
    	};
    }

    function instance$2($$self, $$props, $$invalidate) {
    	

      let { menu     = null, name     = 'menu-item', type     = 'input', label    = 'Menu item', value    = '', index    = -1, depth    = 0, path     = [], classes  = [] } = $$props;

      const mainContext = getContext('main');

      $$invalidate('path', path = path.slice(0));

      path.push(index);

      const reset = function(e) {

        const idx = classes.indexOf('active');

        if(idx !== -1)
          classes.splice(idx, 1);

        $$invalidate('classes', classes = classes.slice(0));
      };

      const onItemChange = function(e) {
        e.preventDefault();
        $$invalidate('value', value = e.target.value);
        window.parent.postMessage(JSON.stringify({action: 'menu_change', data: {name, type, label, value, index, depth, path}}), '*');
      };
      
      const onLabelClick = function(e) {
        e.preventDefault();
        mainContext.reset();
      };

      onMount(() => {
        mainContext.registerReset(reset);
      });

      onDestroy(() => {
        mainContext.unregisterReset(reset);
      });

    	$$self.$set = $$props => {
    		if ('menu' in $$props) $$invalidate('menu', menu = $$props.menu);
    		if ('name' in $$props) $$invalidate('name', name = $$props.name);
    		if ('type' in $$props) $$invalidate('type', type = $$props.type);
    		if ('label' in $$props) $$invalidate('label', label = $$props.label);
    		if ('value' in $$props) $$invalidate('value', value = $$props.value);
    		if ('index' in $$props) $$invalidate('index', index = $$props.index);
    		if ('depth' in $$props) $$invalidate('depth', depth = $$props.depth);
    		if ('path' in $$props) $$invalidate('path', path = $$props.path);
    		if ('classes' in $$props) $$invalidate('classes', classes = $$props.classes);
    	};

    	return {
    		menu,
    		name,
    		type,
    		label,
    		value,
    		index,
    		depth,
    		path,
    		classes,
    		reset,
    		onItemChange,
    		onLabelClick
    	};
    }

    class MenuItemInput extends SvelteComponent {
    	constructor(options) {
    		super();
    		init(this, options, instance$2, create_fragment$2, safe_not_equal, ["menu", "name", "type", "label", "value", "index", "depth", "path", "classes", "reset", "onItemChange", "onLabelClick"]);
    	}

    	get reset() {
    		return this.$$.ctx.reset;
    	}

    	get onItemChange() {
    		return this.$$.ctx.onItemChange;
    	}

    	get onLabelClick() {
    		return this.$$.ctx.onLabelClick;
    	}
    }

    /* src\MenuItemCheck.svelte generated by Svelte v3.5.1 */

    function create_fragment$3(ctx) {
    	var div4, div3, div0, t0, div1, t1, t2, div2, input, div4_class_value, dispose;

    	return {
    		c() {
    			div4 = element("div");
    			div3 = element("div");
    			div0 = element("div");
    			t0 = space();
    			div1 = element("div");
    			t1 = text(ctx.label);
    			t2 = space();
    			div2 = element("div");
    			input = element("input");
    			div0.className = "label-spacer svelte-18cj4hh";
    			div1.className = "label-text svelte-18cj4hh";
    			attr(input, "type", "checkbox");
    			input.className = "svelte-18cj4hh";
    			div2.className = "label-checkbox svelte-18cj4hh";
    			div3.className = "label svelte-18cj4hh";
    			div4.className = div4_class_value = "" + (`menu-item menu-item-slider ${ctx.classes.join(' ')}`) + " svelte-18cj4hh";

    			dispose = [
    				listen(input, "change", ctx.input_change_handler),
    				listen(input, "click", ctx.onItemChange),
    				listen(div3, "click", ctx.onLabelClick)
    			];
    		},

    		m(target, anchor) {
    			insert(target, div4, anchor);
    			append(div4, div3);
    			append(div3, div0);
    			append(div3, t0);
    			append(div3, div1);
    			append(div1, t1);
    			append(div3, t2);
    			append(div3, div2);
    			append(div2, input);

    			input.checked = ctx.value;
    		},

    		p(changed, ctx) {
    			if (changed.label) {
    				set_data(t1, ctx.label);
    			}

    			if (changed.value) input.checked = ctx.value;

    			if ((changed.classes) && div4_class_value !== (div4_class_value = "" + (`menu-item menu-item-slider ${ctx.classes.join(' ')}`) + " svelte-18cj4hh")) {
    				div4.className = div4_class_value;
    			}
    		},

    		i: noop,
    		o: noop,

    		d(detaching) {
    			if (detaching) {
    				detach(div4);
    			}

    			run_all(dispose);
    		}
    	};
    }

    function instance$3($$self, $$props, $$invalidate) {
    	

      const mainContext = getContext('main');

      let { menu     = null, name     = 'menu-item', type     = 'check', label    = 'Menu item', value    = false, index    = -1, depth    = 0, path     = [], classes  = [], min  = -Infinity, max  = +Infinity, step = 1 } = $$props;

      $$invalidate('path', path = path.slice(0));

      path.push(index);

      const reset = function(e) {

        const idx = classes.indexOf('active');

        if(idx !== -1)
          classes.splice(idx, 1);

        $$invalidate('classes', classes = classes.slice(0));
      };

      const onItemChange = function(e) {
        e.preventDefault();
        setTimeout(() => {
          $$invalidate('value', value = !e.target.checked);
          window.parent.postMessage(JSON.stringify({action: 'menu_change', data: {name, type, label, value, index, depth, path}}), '*');
        }, 0);
      };

      const onLabelClick = function(e) {
        e.preventDefault();
        mainContext.reset();
      };

      onMount(() => {
        mainContext.registerReset(reset);
      });

      onDestroy(() => {
        mainContext.unregisterReset(reset);
      });

    	function input_change_handler() {
    		value = this.checked;
    		$$invalidate('value', value);
    	}

    	$$self.$set = $$props => {
    		if ('menu' in $$props) $$invalidate('menu', menu = $$props.menu);
    		if ('name' in $$props) $$invalidate('name', name = $$props.name);
    		if ('type' in $$props) $$invalidate('type', type = $$props.type);
    		if ('label' in $$props) $$invalidate('label', label = $$props.label);
    		if ('value' in $$props) $$invalidate('value', value = $$props.value);
    		if ('index' in $$props) $$invalidate('index', index = $$props.index);
    		if ('depth' in $$props) $$invalidate('depth', depth = $$props.depth);
    		if ('path' in $$props) $$invalidate('path', path = $$props.path);
    		if ('classes' in $$props) $$invalidate('classes', classes = $$props.classes);
    		if ('min' in $$props) $$invalidate('min', min = $$props.min);
    		if ('max' in $$props) $$invalidate('max', max = $$props.max);
    		if ('step' in $$props) $$invalidate('step', step = $$props.step);
    	};

    	return {
    		menu,
    		name,
    		type,
    		label,
    		value,
    		index,
    		depth,
    		path,
    		classes,
    		min,
    		max,
    		step,
    		reset,
    		onItemChange,
    		onLabelClick,
    		input_change_handler
    	};
    }

    class MenuItemCheck extends SvelteComponent {
    	constructor(options) {
    		super();
    		init(this, options, instance$3, create_fragment$3, safe_not_equal, ["menu", "name", "type", "label", "value", "index", "depth", "path", "classes", "min", "max", "step", "reset", "onItemChange", "onLabelClick"]);
    	}

    	get reset() {
    		return this.$$.ctx.reset;
    	}

    	get onItemChange() {
    		return this.$$.ctx.onItemChange;
    	}

    	get onLabelClick() {
    		return this.$$.ctx.onLabelClick;
    	}
    }

    /* src\MenuItem.svelte generated by Svelte v3.5.1 */

    function get_each_context(ctx, list, i) {
    	const child_ctx = Object.create(ctx);
    	child_ctx.item = list[i];
    	child_ctx.index = i;
    	return child_ctx;
    }

    // (185:4) {#each children as item, index}
    function create_each_block(ctx) {
    	var switch_instance_anchor, current;

    	var switch_instance_spread_levels = [
    		ctx.item,
    		{ menu: ctx.menu },
    		{ index: ctx.index },
    		{ path: ctx.path },
    		{ depth: ctx.depth + 1 }
    	];

    	var switch_value = ctx.itemTypes[ctx.item.type || 'default'];

    	function switch_props(ctx) {
    		let switch_instance_props = {};
    		for (var i = 0; i < switch_instance_spread_levels.length; i += 1) {
    			switch_instance_props = assign(switch_instance_props, switch_instance_spread_levels[i]);
    		}
    		return { props: switch_instance_props };
    	}

    	if (switch_value) {
    		var switch_instance = new switch_value(switch_props());
    	}

    	return {
    		c() {
    			if (switch_instance) switch_instance.$$.fragment.c();
    			switch_instance_anchor = empty();
    		},

    		m(target, anchor) {
    			if (switch_instance) {
    				mount_component(switch_instance, target, anchor);
    			}

    			insert(target, switch_instance_anchor, anchor);
    			current = true;
    		},

    		p(changed, ctx) {
    			var switch_instance_changes = (changed.children || changed.menu || changed.path || changed.depth) ? get_spread_update(switch_instance_spread_levels, [
    				(changed.children) && ctx.item,
    				(changed.menu) && { menu: ctx.menu },
    				{ index: ctx.index },
    				(changed.path) && { path: ctx.path },
    				(changed.depth) && { depth: ctx.depth + 1 }
    			]) : {};

    			if (switch_value !== (switch_value = ctx.itemTypes[ctx.item.type || 'default'])) {
    				if (switch_instance) {
    					group_outros();
    					const old_component = switch_instance;
    					on_outro(() => {
    						old_component.$destroy();
    					});
    					old_component.$$.fragment.o(1);
    					check_outros();
    				}

    				if (switch_value) {
    					switch_instance = new switch_value(switch_props());

    					switch_instance.$$.fragment.c();
    					switch_instance.$$.fragment.i(1);
    					mount_component(switch_instance, switch_instance_anchor.parentNode, switch_instance_anchor);
    				} else {
    					switch_instance = null;
    				}
    			}

    			else if (switch_value) {
    				switch_instance.$set(switch_instance_changes);
    			}
    		},

    		i(local) {
    			if (current) return;
    			if (switch_instance) switch_instance.$$.fragment.i(local);

    			current = true;
    		},

    		o(local) {
    			if (switch_instance) switch_instance.$$.fragment.o(local);
    			current = false;
    		},

    		d(detaching) {
    			if (detaching) {
    				detach(switch_instance_anchor);
    			}

    			if (switch_instance) switch_instance.$destroy(detaching);
    		}
    	};
    }

    function create_fragment$4(ctx) {
    	var div5, div3, div0, t0, div1, span_1, t1, t2, div2, t3, div4, div5_class_value, current, dispose;

    	var each_value = ctx.children;

    	var each_blocks = [];

    	for (var i = 0; i < each_value.length; i += 1) {
    		each_blocks[i] = create_each_block(get_each_context(ctx, each_value, i));
    	}

    	function outro_block(i, detaching, local) {
    		if (each_blocks[i]) {
    			if (detaching) {
    				on_outro(() => {
    					each_blocks[i].d(detaching);
    					each_blocks[i] = null;
    				});
    			}

    			each_blocks[i].o(local);
    		}
    	}

    	return {
    		c() {
    			div5 = element("div");
    			div3 = element("div");
    			div0 = element("div");
    			div0.innerHTML = `<span class="svelte-1qrexfo"></span>`;
    			t0 = space();
    			div1 = element("div");
    			span_1 = element("span");
    			t1 = text(ctx.label);
    			t2 = space();
    			div2 = element("div");
    			div2.innerHTML = `<span class="svelte-1qrexfo"></span>`;
    			t3 = space();
    			div4 = element("div");

    			for (var i = 0; i < each_blocks.length; i += 1) {
    				each_blocks[i].c();
    			}
    			div0.className = "before svelte-1qrexfo";
    			span_1.className = "svelte-1qrexfo";
    			div1.className = "content";
    			div2.className = "after svelte-1qrexfo";
    			div3.className = "label svelte-1qrexfo";
    			div4.className = "label-children svelte-1qrexfo";
    			div5.className = div5_class_value = "" + (`menu-item ${ctx.classes.join(' ')}`) + " svelte-1qrexfo";

    			dispose = [
    				listen(div3, "click", ctx.onLabelClick),
    				listen(div5, "click", ctx.onItemClick)
    			];
    		},

    		m(target, anchor) {
    			insert(target, div5, anchor);
    			append(div5, div3);
    			append(div3, div0);
    			append(div3, t0);
    			append(div3, div1);
    			append(div1, span_1);
    			append(span_1, t1);
    			append(div3, t2);
    			append(div3, div2);
    			append(div5, t3);
    			append(div5, div4);

    			for (var i = 0; i < each_blocks.length; i += 1) {
    				each_blocks[i].m(div4, null);
    			}

    			current = true;
    		},

    		p(changed, ctx) {
    			if (!current || changed.label) {
    				set_data(t1, ctx.label);
    			}

    			if (changed.itemTypes || changed.children || changed.menu || changed.path || changed.depth) {
    				each_value = ctx.children;

    				for (var i = 0; i < each_value.length; i += 1) {
    					const child_ctx = get_each_context(ctx, each_value, i);

    					if (each_blocks[i]) {
    						each_blocks[i].p(changed, child_ctx);
    						each_blocks[i].i(1);
    					} else {
    						each_blocks[i] = create_each_block(child_ctx);
    						each_blocks[i].c();
    						each_blocks[i].i(1);
    						each_blocks[i].m(div4, null);
    					}
    				}

    				group_outros();
    				for (; i < each_blocks.length; i += 1) outro_block(i, 1, 1);
    				check_outros();
    			}

    			if ((!current || changed.classes) && div5_class_value !== (div5_class_value = "" + (`menu-item ${ctx.classes.join(' ')}`) + " svelte-1qrexfo")) {
    				div5.className = div5_class_value;
    			}
    		},

    		i(local) {
    			if (current) return;
    			for (var i = 0; i < each_value.length; i += 1) each_blocks[i].i();

    			current = true;
    		},

    		o(local) {
    			each_blocks = each_blocks.filter(Boolean);
    			for (let i = 0; i < each_blocks.length; i += 1) outro_block(i, 0, 0);

    			current = false;
    		},

    		d(detaching) {
    			if (detaching) {
    				detach(div5);
    			}

    			destroy_each(each_blocks, detaching);

    			run_all(dispose);
    		}
    	};
    }

    function instance$4($$self, $$props, $$invalidate) {
    	

      const itemTypes = {
        default: MenuItem_1,
        slider : MenuItemSlider,
        input  : MenuItemInput,
        check  : MenuItemCheck,
      };

      let { menu     = null, name     = 'menu-item', type     = 'default', label    = 'Menu item', value    = null, index    = -1, depth    = 0, path     = [], children = [], classes  = [] } = $$props;

      const mainContext = getContext('main');

      if(children.length > 0)
        classes.push('has-children');

      classes.push(`depth-${depth}`);

      $$invalidate('path', path = path.slice(0));

      path.push(index);

      const reset = function(e) {

        let idx = classes.indexOf('active');

        if(idx !== -1)
          classes.splice(idx, 1);

        $$invalidate('classes', classes = classes.slice(0));
      };

      const onItemClick = function(e) {
        
        e.preventDefault();

        if(children.length > 0) {

          const idx = classes.indexOf('active');

          if(idx === -1)
            classes.push('active');

          $$invalidate('classes', classes = classes.slice(0));
          
        }

      };

      const onLabelClick = function(e) {
        e.preventDefault();
        mainContext.reset();
        window.parent.postMessage(JSON.stringify({action: 'menu_select', data: {name, type, label, value, index, depth, path}}), '*');
      };

      onMount(() => {
        mainContext.registerReset(reset);
      });

      onDestroy(() => {
        mainContext.unregisterReset(reset);
      });

    	$$self.$set = $$props => {
    		if ('menu' in $$props) $$invalidate('menu', menu = $$props.menu);
    		if ('name' in $$props) $$invalidate('name', name = $$props.name);
    		if ('type' in $$props) $$invalidate('type', type = $$props.type);
    		if ('label' in $$props) $$invalidate('label', label = $$props.label);
    		if ('value' in $$props) $$invalidate('value', value = $$props.value);
    		if ('index' in $$props) $$invalidate('index', index = $$props.index);
    		if ('depth' in $$props) $$invalidate('depth', depth = $$props.depth);
    		if ('path' in $$props) $$invalidate('path', path = $$props.path);
    		if ('children' in $$props) $$invalidate('children', children = $$props.children);
    		if ('classes' in $$props) $$invalidate('classes', classes = $$props.classes);
    	};

    	return {
    		itemTypes,
    		menu,
    		name,
    		type,
    		label,
    		value,
    		index,
    		depth,
    		path,
    		children,
    		classes,
    		reset,
    		onItemClick,
    		onLabelClick
    	};
    }

    class MenuItem_1 extends SvelteComponent {
    	constructor(options) {
    		super();
    		init(this, options, instance$4, create_fragment$4, safe_not_equal, ["itemTypes", "menu", "name", "type", "label", "value", "index", "depth", "path", "children", "classes", "reset", "onItemClick", "onLabelClick"]);
    	}

    	get itemTypes() {
    		return this.$$.ctx.itemTypes;
    	}

    	get reset() {
    		return this.$$.ctx.reset;
    	}

    	get onItemClick() {
    		return this.$$.ctx.onItemClick;
    	}

    	get onLabelClick() {
    		return this.$$.ctx.onLabelClick;
    	}
    }

    /* src\App.svelte generated by Svelte v3.5.1 */

    function get_each_context$1(ctx, list, i) {
    	const child_ctx = Object.create(ctx);
    	child_ctx.item = list[i];
    	child_ctx.index = i;
    	return child_ctx;
    }

    // (226:4) {#each menu.children as item, index}
    function create_each_block$1(ctx) {
    	var switch_instance_anchor, current;

    	var switch_instance_spread_levels = [
    		ctx.item,
    		{ menu: ctx.menu },
    		{ index: ctx.index }
    	];

    	var switch_value = ctx.itemTypes[ctx.item.type || 'default'];

    	function switch_props(ctx) {
    		let switch_instance_props = {};
    		for (var i = 0; i < switch_instance_spread_levels.length; i += 1) {
    			switch_instance_props = assign(switch_instance_props, switch_instance_spread_levels[i]);
    		}
    		return { props: switch_instance_props };
    	}

    	if (switch_value) {
    		var switch_instance = new switch_value(switch_props());
    	}

    	return {
    		c() {
    			if (switch_instance) switch_instance.$$.fragment.c();
    			switch_instance_anchor = empty();
    		},

    		m(target, anchor) {
    			if (switch_instance) {
    				mount_component(switch_instance, target, anchor);
    			}

    			insert(target, switch_instance_anchor, anchor);
    			current = true;
    		},

    		p(changed, ctx) {
    			var switch_instance_changes = changed.menu ? get_spread_update(switch_instance_spread_levels, [
    				ctx.item,
    				{ menu: ctx.menu },
    				{ index: ctx.index }
    			]) : {};

    			if (switch_value !== (switch_value = ctx.itemTypes[ctx.item.type || 'default'])) {
    				if (switch_instance) {
    					group_outros();
    					const old_component = switch_instance;
    					on_outro(() => {
    						old_component.$destroy();
    					});
    					old_component.$$.fragment.o(1);
    					check_outros();
    				}

    				if (switch_value) {
    					switch_instance = new switch_value(switch_props());

    					switch_instance.$$.fragment.c();
    					switch_instance.$$.fragment.i(1);
    					mount_component(switch_instance, switch_instance_anchor.parentNode, switch_instance_anchor);
    				} else {
    					switch_instance = null;
    				}
    			}

    			else if (switch_value) {
    				switch_instance.$set(switch_instance_changes);
    			}
    		},

    		i(local) {
    			if (current) return;
    			if (switch_instance) switch_instance.$$.fragment.i(local);

    			current = true;
    		},

    		o(local) {
    			if (switch_instance) switch_instance.$$.fragment.o(local);
    			current = false;
    		},

    		d(detaching) {
    			if (detaching) {
    				detach(switch_instance_anchor);
    			}

    			if (switch_instance) switch_instance.$destroy(detaching);
    		}
    	};
    }

    // (225:2) <Menu label={menu.label}>
    function create_default_slot(ctx) {
    	var each_1_anchor, current;

    	var each_value = ctx.menu.children;

    	var each_blocks = [];

    	for (var i = 0; i < each_value.length; i += 1) {
    		each_blocks[i] = create_each_block$1(get_each_context$1(ctx, each_value, i));
    	}

    	function outro_block(i, detaching, local) {
    		if (each_blocks[i]) {
    			if (detaching) {
    				on_outro(() => {
    					each_blocks[i].d(detaching);
    					each_blocks[i] = null;
    				});
    			}

    			each_blocks[i].o(local);
    		}
    	}

    	return {
    		c() {
    			for (var i = 0; i < each_blocks.length; i += 1) {
    				each_blocks[i].c();
    			}

    			each_1_anchor = empty();
    		},

    		m(target, anchor) {
    			for (var i = 0; i < each_blocks.length; i += 1) {
    				each_blocks[i].m(target, anchor);
    			}

    			insert(target, each_1_anchor, anchor);
    			current = true;
    		},

    		p(changed, ctx) {
    			if (changed.itemTypes || changed.menu) {
    				each_value = ctx.menu.children;

    				for (var i = 0; i < each_value.length; i += 1) {
    					const child_ctx = get_each_context$1(ctx, each_value, i);

    					if (each_blocks[i]) {
    						each_blocks[i].p(changed, child_ctx);
    						each_blocks[i].i(1);
    					} else {
    						each_blocks[i] = create_each_block$1(child_ctx);
    						each_blocks[i].c();
    						each_blocks[i].i(1);
    						each_blocks[i].m(each_1_anchor.parentNode, each_1_anchor);
    					}
    				}

    				group_outros();
    				for (; i < each_blocks.length; i += 1) outro_block(i, 1, 1);
    				check_outros();
    			}
    		},

    		i(local) {
    			if (current) return;
    			for (var i = 0; i < each_value.length; i += 1) each_blocks[i].i();

    			current = true;
    		},

    		o(local) {
    			each_blocks = each_blocks.filter(Boolean);
    			for (let i = 0; i < each_blocks.length; i += 1) outro_block(i, 0, 0);

    			current = false;
    		},

    		d(detaching) {
    			destroy_each(each_blocks, detaching);

    			if (detaching) {
    				detach(each_1_anchor);
    			}
    		}
    	};
    }

    function create_fragment$5(ctx) {
    	var div, current;

    	var menu_1 = new Menu({
    		props: {
    		label: ctx.menu.label,
    		$$slots: { default: [create_default_slot] },
    		$$scope: { ctx }
    	}
    	});

    	return {
    		c() {
    			div = element("div");
    			menu_1.$$.fragment.c();
    			div.className = "app svelte-s6m53i";
    		},

    		m(target, anchor) {
    			insert(target, div, anchor);
    			mount_component(menu_1, div, null);
    			add_binding_callback(() => ctx.div_binding(div, null));
    			current = true;
    		},

    		p(changed, ctx) {
    			var menu_1_changes = {};
    			if (changed.menu) menu_1_changes.label = ctx.menu.label;
    			if (changed.$$scope || changed.menu) menu_1_changes.$$scope = { changed, ctx };
    			menu_1.$set(menu_1_changes);

    			if (changed.items) {
    				ctx.div_binding(null, div);
    				ctx.div_binding(div, null);
    			}
    		},

    		i(local) {
    			if (current) return;
    			menu_1.$$.fragment.i(local);

    			current = true;
    		},

    		o(local) {
    			menu_1.$$.fragment.o(local);
    			current = false;
    		},

    		d(detaching) {
    			if (detaching) {
    				detach(div);
    			}

    			menu_1.$destroy();

    			ctx.div_binding(null, div);
    		}
    	};
    }

    function instance$5($$self, $$props, $$invalidate) {
    	

      let { ref     = null } = $$props;

      const itemTypes = {
        default: MenuItem_1,
        slider : MenuItemSlider,
        input  : MenuItemInput,
        check  : MenuItemCheck,
      };
      let onClick        = () => setTimeout(updateSize, 0);

      let { menu = {
        label: 'Menu',
        children: []
      } } = $$props;

      const onMessage = function(e) {

        let msg = {};
        
        try { msg = JSON.parse(e.data); } catch(e) {}
        if(msg.action === 'undefined')
          return;

        switch(msg.action) {

          case 'open_menu' : {
            
            menu.label    = msg.label || 'Menu'; $$invalidate('menu', menu);
            menu.children = msg.items instanceof Array ? msg.items : []; $$invalidate('menu', menu);

            $$invalidate('menu', menu = {...menu});

            break;
          }

          case 'update_menu' : {

            let path = msg.path.slice(0);
            let obj  = menu;

            while(path.length > 0)
              obj = obj.children[path.shift()];

            for(let k in msg.data)
              obj[k] = msg.data[k];

            $$invalidate('menu', menu = {...menu});

            break;
          }

          default: break;

        }

      };

      const updateSize = function() {
        
        const elems    = ref.getElementsByTagName('*');
        const MAX_SIZE = 8192;

        let width  = MAX_SIZE;
        let height = 0;

        let left   = MAX_SIZE;
        let right  = 0;

        let first = true;

        for (let i = 0; i<elems.length; i++) {

            const elem = elems[i];
            const bb   = elems[i].getBoundingClientRect();

            if(bb.left < left)
              left = bb.left;
            else if(bb.right > right)
              right = bb.right;

            width  = right - left;

            if(elem.classList.contains('menu-item')) {

              const bb = elem.getBoundingClientRect();

              const _height = bb.top + 35;

              if(_height > height)
                height = _height;

              let bestWidth = 100;
              const items   = elem.parentNode.querySelectorAll(':scope > .menu-item');

              items.forEach(e => {

                const child  = e.querySelector(':scope > .label > *:nth-child(2)');
                const width2 = child ? child.clientWidth : 0;
                
                if(width2 > bestWidth)
                  bestWidth = width2;
              });

              bestWidth = bestWidth + 150;

              if(first) {
                
                first = false;
                
                document.querySelector('.menu > .label').style.width = (bestWidth - 9) + 'px';
                document.querySelectorAll('.menu > .items > .menu-item > .label').forEach(e => e.style.width = bestWidth + 'px');
              }

              items.forEach(e => {

                e.querySelector(':scope > .label').style.width = bestWidth + 'px';

                const children = e.querySelector(':scope > .label-children');

                if(children)
                  children.style.marginLeft = (bestWidth + 11) + 'px';

              });

            }
        }

        height = height > 37 ? height : 37;

        document.body.style.width  = width  + 'px';
        document.body.style.height = height + 'px';
      };

      onMount(() => {
        window.addEventListener('message', onMessage);
        window.addEventListener('click', onClick);
        window.parent.postMessage(JSON.stringify({action: 'component_mounted'}), '*');

        /*
        setTimeout(() => {

          menu.children = [
            {name: 'fooa', type: 'default', label: 'fooA sdsdsd sdsdsdsdsds sdfdfddf dffddfd fddf', children: [
            {name: 'fooa1', type: 'default', label: 'fooA1', children: [
              {name: 'fooba', type: 'default', label: 'fooBa'},
              {name: 'fooca', type: 'default', label: 'fooCa'},
              {name: 'fooa2', type: 'default', label: 'fooA2', children: [
                {name: 'fooba', type: 'default', label: 'fooBa'},
                {name: 'fooca', type: 'default', label: 'fooCa'},
                {name: 'fooba', type: 'default', label: 'fooBa'},
                {name: 'fooca', type: 'default', label: 'fooCa'},
                {name: 'fooba', type: 'default', label: 'fooBa'},
                {name: 'fooca', type: 'default', label: 'fooCa'},
                {name: 'fooba', type: 'default', label: 'fooBa'},
                {name: 'fooca', type: 'default', label: 'fooCa'},
              ]},
            ]},
            {name: 'foobb', type: 'default', label: 'fooBb'},
            {name: 'foocb', type: 'default', label: 'fooCb'},
            ]},
            {name: 'fooa', type: 'default', label: 'fooA', children: [
              {name: 'fooa1', type: 'default', label: 'fooA1', children: [
                {name: 'fooba', type: 'default', label: 'fooBa'},
                {name: 'fooa2', type: 'default', label: 'fooA2', children: [
                  {name: 'fooba', type: 'default', label: 'fooBa'},
                  {name: 'fooca', type: 'default', label: 'fooCa'},
                  {name: 'fooba', type: 'default', label: 'fooBa'},
                  {name: 'fooca', type: 'default', label: 'fooCa'},
                  {name: 'aaaaa', type: 'slider', label: 'fooC', options:['foo', 'bar', 'baz']},
                  {name: 'fooba', type: 'default', label: 'fooBa'},
                  {name: 'fooca', type: 'default', label: 'fooCa'},
                  {name: 'fooba', type: 'default', label: 'fooBa'},
                  {name: 'fooca', type: 'default', label: 'fooCa'},
                  {name: 'fooc', type: 'check', label: 'fooD'},
                  {name: 'aaaaa', type: 'slider', label: 'fooE'},
                  {name: 'fooc', type: 'input', label: 'fooC'},
                  {name: 'aaaaa', type: 'slider', label: 'fooF', options:['foo', 'bar', 'baz']},
                ]},
                {name: 'fooca', type: 'default', label: 'fooCa'},
              ]},
              {name: 'foobb', type: 'default', label: 'fooBb'},
              {name: 'foocb', type: 'default', label: 'fooCb'},
            ]},
          ];

          menu = {...menu};

        }, 1000);
        */

      });

      onDestroy(() => {
        window.removeEventListener('message', onMessage);
        window.removeEventListener('click',  onClick);
      });

      afterUpdate(() => updateSize());

    	function div_binding($$node, check) {
    		ref = $$node;
    		$$invalidate('ref', ref);
    	}

    	$$self.$set = $$props => {
    		if ('ref' in $$props) $$invalidate('ref', ref = $$props.ref);
    		if ('menu' in $$props) $$invalidate('menu', menu = $$props.menu);
    	};

    	return {
    		ref,
    		itemTypes,
    		menu,
    		onMessage,
    		updateSize,
    		div_binding
    	};
    }

    class App extends SvelteComponent {
    	constructor(options) {
    		super();
    		init(this, options, instance$5, create_fragment$5, safe_not_equal, ["ref", "itemTypes", "menu", "onMessage", "updateSize"]);
    	}

    	get itemTypes() {
    		return this.$$.ctx.itemTypes;
    	}

    	get onMessage() {
    		return this.$$.ctx.onMessage;
    	}

    	get updateSize() {
    		return this.$$.ctx.updateSize;
    	}
    }

    const app = new App({
    	target: document.body,
    	props: {
    	}
    });

    return app;

}());
//# sourceMappingURL=bundle.js.map
