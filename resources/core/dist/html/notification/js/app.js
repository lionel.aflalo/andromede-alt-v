(() => {

  window.App = {
    id: 0,
  };

  App.timeouts = {}

  App.init = function() {
    window.addEventListener('message', (e) => App.onMessage(JSON.parse(e.data)));
  }

  App.onMessage = function(msg) {

    switch(msg.action) {
      
      case 'notification' : {
        
        const timeout = msg.timeout === undefined ? 5000  : msg.timeout;
        const before  = msg.before  === undefined ? false : msg.before;
        const id      = (msg.id || App.id++).toString();
        const content = document.getElementById('content');
        const elem    = document.querySelector('div[data-id="' + id + '"]') || document.createElement('div');

        elem.innerHTML = msg.html;

        elem.setAttribute('data-id', id);

        if(before)
          content.insertBefore(elem, content.firstChild || null);
        else
          content.appendChild(elem);

        clearTimeout(App.timeouts[id]);

        App.timeouts[id] = setTimeout(() => content.removeChild(elem), timeout);

        break;
      }

      default: break;

    }

  }

  App.init();

})();