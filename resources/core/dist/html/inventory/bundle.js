var app = (function () {
    'use strict';

    function noop() { }
    function assign(tar, src) {
        // @ts-ignore
        for (const k in src)
            tar[k] = src[k];
        return tar;
    }
    function run(fn) {
        return fn();
    }
    function blank_object() {
        return Object.create(null);
    }
    function run_all(fns) {
        fns.forEach(run);
    }
    function is_function(thing) {
        return typeof thing === 'function';
    }
    function safe_not_equal(a, b) {
        return a != a ? b == b : a !== b || ((a && typeof a === 'object') || typeof a === 'function');
    }

    function append(target, node) {
        target.appendChild(node);
    }
    function insert(target, node, anchor) {
        target.insertBefore(node, anchor || null);
    }
    function detach(node) {
        node.parentNode.removeChild(node);
    }
    function destroy_each(iterations, detaching) {
        for (let i = 0; i < iterations.length; i += 1) {
            if (iterations[i])
                iterations[i].d(detaching);
        }
    }
    function element(name) {
        return document.createElement(name);
    }
    function text(data) {
        return document.createTextNode(data);
    }
    function space() {
        return text(' ');
    }
    function empty() {
        return text('');
    }
    function listen(node, event, handler, options) {
        node.addEventListener(event, handler, options);
        return () => node.removeEventListener(event, handler, options);
    }
    function attr(node, attribute, value) {
        if (value == null)
            node.removeAttribute(attribute);
        else
            node.setAttribute(attribute, value);
    }
    function children(element) {
        return Array.from(element.childNodes);
    }
    function set_data(text, data) {
        data = '' + data;
        if (text.data !== data)
            text.data = data;
    }
    function set_style(node, key, value, important) {
        node.style.setProperty(key, value, important ? 'important' : '');
    }
    function custom_event(type, detail) {
        const e = document.createEvent('CustomEvent');
        e.initCustomEvent(type, false, false, detail);
        return e;
    }

    let current_component;
    function set_current_component(component) {
        current_component = component;
    }
    function get_current_component() {
        if (!current_component)
            throw new Error(`Function called outside component initialization`);
        return current_component;
    }
    function beforeUpdate(fn) {
        get_current_component().$$.before_update.push(fn);
    }
    function onMount(fn) {
        get_current_component().$$.on_mount.push(fn);
    }
    function createEventDispatcher() {
        const component = current_component;
        return (type, detail) => {
            const callbacks = component.$$.callbacks[type];
            if (callbacks) {
                // TODO are there situations where events could be dispatched
                // in a server (non-DOM) environment?
                const event = custom_event(type, detail);
                callbacks.slice().forEach(fn => {
                    fn.call(component, event);
                });
            }
        };
    }

    const dirty_components = [];
    const binding_callbacks = [];
    const render_callbacks = [];
    const flush_callbacks = [];
    const resolved_promise = Promise.resolve();
    let update_scheduled = false;
    function schedule_update() {
        if (!update_scheduled) {
            update_scheduled = true;
            resolved_promise.then(flush);
        }
    }
    function add_render_callback(fn) {
        render_callbacks.push(fn);
    }
    function flush() {
        const seen_callbacks = new Set();
        do {
            // first, call beforeUpdate functions
            // and update components
            while (dirty_components.length) {
                const component = dirty_components.shift();
                set_current_component(component);
                update(component.$$);
            }
            while (binding_callbacks.length)
                binding_callbacks.pop()();
            // then, once components are updated, call
            // afterUpdate functions. This may cause
            // subsequent updates...
            for (let i = 0; i < render_callbacks.length; i += 1) {
                const callback = render_callbacks[i];
                if (!seen_callbacks.has(callback)) {
                    callback();
                    // ...so guard against infinite loops
                    seen_callbacks.add(callback);
                }
            }
            render_callbacks.length = 0;
        } while (dirty_components.length);
        while (flush_callbacks.length) {
            flush_callbacks.pop()();
        }
        update_scheduled = false;
    }
    function update($$) {
        if ($$.fragment) {
            $$.update($$.dirty);
            run_all($$.before_update);
            $$.fragment.p($$.dirty, $$.ctx);
            $$.dirty = null;
            $$.after_update.forEach(add_render_callback);
        }
    }
    const outroing = new Set();
    let outros;
    function group_outros() {
        outros = {
            r: 0,
            c: [],
            p: outros // parent group
        };
    }
    function check_outros() {
        if (!outros.r) {
            run_all(outros.c);
        }
        outros = outros.p;
    }
    function transition_in(block, local) {
        if (block && block.i) {
            outroing.delete(block);
            block.i(local);
        }
    }
    function transition_out(block, local, detach, callback) {
        if (block && block.o) {
            if (outroing.has(block))
                return;
            outroing.add(block);
            outros.c.push(() => {
                outroing.delete(block);
                if (callback) {
                    if (detach)
                        block.d(1);
                    callback();
                }
            });
            block.o(local);
        }
    }

    const globals = (typeof window !== 'undefined' ? window : global);

    function get_spread_update(levels, updates) {
        const update = {};
        const to_null_out = {};
        const accounted_for = { $$scope: 1 };
        let i = levels.length;
        while (i--) {
            const o = levels[i];
            const n = updates[i];
            if (n) {
                for (const key in o) {
                    if (!(key in n))
                        to_null_out[key] = 1;
                }
                for (const key in n) {
                    if (!accounted_for[key]) {
                        update[key] = n[key];
                        accounted_for[key] = 1;
                    }
                }
                levels[i] = n;
            }
            else {
                for (const key in o) {
                    accounted_for[key] = 1;
                }
            }
        }
        for (const key in to_null_out) {
            if (!(key in update))
                update[key] = undefined;
        }
        return update;
    }
    function get_spread_object(spread_props) {
        return typeof spread_props === 'object' && spread_props !== null ? spread_props : {};
    }
    function mount_component(component, target, anchor) {
        const { fragment, on_mount, on_destroy, after_update } = component.$$;
        fragment.m(target, anchor);
        // onMount happens before the initial afterUpdate
        add_render_callback(() => {
            const new_on_destroy = on_mount.map(run).filter(is_function);
            if (on_destroy) {
                on_destroy.push(...new_on_destroy);
            }
            else {
                // Edge case - component was destroyed immediately,
                // most likely as a result of a binding initialising
                run_all(new_on_destroy);
            }
            component.$$.on_mount = [];
        });
        after_update.forEach(add_render_callback);
    }
    function destroy_component(component, detaching) {
        if (component.$$.fragment) {
            run_all(component.$$.on_destroy);
            component.$$.fragment.d(detaching);
            // TODO null out other refs, including component.$$ (but need to
            // preserve final state?)
            component.$$.on_destroy = component.$$.fragment = null;
            component.$$.ctx = {};
        }
    }
    function make_dirty(component, key) {
        if (!component.$$.dirty) {
            dirty_components.push(component);
            schedule_update();
            component.$$.dirty = blank_object();
        }
        component.$$.dirty[key] = true;
    }
    function init(component, options, instance, create_fragment, not_equal, prop_names) {
        const parent_component = current_component;
        set_current_component(component);
        const props = options.props || {};
        const $$ = component.$$ = {
            fragment: null,
            ctx: null,
            // state
            props: prop_names,
            update: noop,
            not_equal,
            bound: blank_object(),
            // lifecycle
            on_mount: [],
            on_destroy: [],
            before_update: [],
            after_update: [],
            context: new Map(parent_component ? parent_component.$$.context : []),
            // everything else
            callbacks: blank_object(),
            dirty: null
        };
        let ready = false;
        $$.ctx = instance
            ? instance(component, props, (key, ret, value = ret) => {
                if ($$.ctx && not_equal($$.ctx[key], $$.ctx[key] = value)) {
                    if ($$.bound[key])
                        $$.bound[key](value);
                    if (ready)
                        make_dirty(component, key);
                }
                return ret;
            })
            : props;
        $$.update();
        ready = true;
        run_all($$.before_update);
        $$.fragment = create_fragment($$.ctx);
        if (options.target) {
            if (options.hydrate) {
                // eslint-disable-next-line @typescript-eslint/no-non-null-assertion
                $$.fragment.l(children(options.target));
            }
            else {
                // eslint-disable-next-line @typescript-eslint/no-non-null-assertion
                $$.fragment.c();
            }
            if (options.intro)
                transition_in(component.$$.fragment);
            mount_component(component, options.target, options.anchor);
            flush();
        }
        set_current_component(parent_component);
    }
    class SvelteComponent {
        $destroy() {
            destroy_component(this, 1);
            this.$destroy = noop;
        }
        $on(type, callback) {
            const callbacks = (this.$$.callbacks[type] || (this.$$.callbacks[type] = []));
            callbacks.push(callback);
            return () => {
                const index = callbacks.indexOf(callback);
                if (index !== -1)
                    callbacks.splice(index, 1);
            };
        }
        $set() {
            // overridden by instance, if it has props
        }
    }

    /* src\Cell.svelte generated by Svelte v3.12.1 */

    function create_fragment(ctx) {
    	var div, div_class_value, dispose;

    	return {
    		c() {
    			div = element("div");
    			attr(div, "class", div_class_value = "cell " + ctx.classes.join(' ') + " svelte-u9suhf");
    			set_style(div, "left", "" + ctx.x * GRID_SIZE + "px");
    			set_style(div, "top", "" + ctx.y * GRID_SIZE + "px");

    			dispose = [
    				listen(div, "mouseover", ctx.onMouseOver),
    				listen(div, "mouseOut", ctx.onMouseOut)
    			];
    		},

    		m(target, anchor) {
    			insert(target, div, anchor);
    		},

    		p(changed, ctx) {
    			if ((changed.classes) && div_class_value !== (div_class_value = "cell " + ctx.classes.join(' ') + " svelte-u9suhf")) {
    				attr(div, "class", div_class_value);
    			}

    			if (changed.x) {
    				set_style(div, "left", "" + ctx.x * GRID_SIZE + "px");
    			}

    			if (changed.y) {
    				set_style(div, "top", "" + ctx.y * GRID_SIZE + "px");
    			}
    		},

    		i: noop,
    		o: noop,

    		d(detaching) {
    			if (detaching) {
    				detach(div);
    			}

    			run_all(dispose);
    		}
    	};
    }

    const GRID_SIZE = 100;

    function instance($$self, $$props, $$invalidate) {
    	const dispatch = createEventDispatcher();

    	let { raw   = false, x     = 0, y     = 0, state = -1 } = $$props;
    	
    	let classes = [];

    	function onMouseOver(e) {
    		dispatch('hover', {x, y});
    	}

    	function onMouseOut(e) {
    		dispatch('blur', {x, y});
    	}

    	beforeUpdate(() => {

    		const _classes = [];

    		if(raw)
    			_classes.push('raw');

    		if(state === 1)
    			_classes.push('state-valid');
    		else if(state === 0)
    			_classes.push('state-invalid');

    		$$invalidate('classes', classes = _classes);
    	});

    	$$self.$set = $$props => {
    		if ('raw' in $$props) $$invalidate('raw', raw = $$props.raw);
    		if ('x' in $$props) $$invalidate('x', x = $$props.x);
    		if ('y' in $$props) $$invalidate('y', y = $$props.y);
    		if ('state' in $$props) $$invalidate('state', state = $$props.state);
    	};

    	return {
    		raw,
    		x,
    		y,
    		state,
    		classes,
    		onMouseOver,
    		onMouseOut
    	};
    }

    class Cell extends SvelteComponent {
    	constructor(options) {
    		super();
    		init(this, options, instance, create_fragment, safe_not_equal, ["raw", "x", "y", "state"]);
    	}
    }

    /* src\Item.svelte generated by Svelte v3.12.1 */

    function get_each_context(ctx, list, i) {
    	const child_ctx = Object.create(ctx);
    	child_ctx.action = list[i];
    	return child_ctx;
    }

    function get_each_context_1(ctx, list, i) {
    	const child_ctx = Object.create(ctx);
    	child_ctx.cell = list[i];
    	child_ctx.index = i;
    	return child_ctx;
    }

    // (229:2) {#if !raw}
    function create_if_block_4(ctx) {
    	var div, current;

    	let each_value_1 = ctx.shape.grid;

    	let each_blocks = [];

    	for (let i = 0; i < each_value_1.length; i += 1) {
    		each_blocks[i] = create_each_block_1(get_each_context_1(ctx, each_value_1, i));
    	}

    	const out = i => transition_out(each_blocks[i], 1, 1, () => {
    		each_blocks[i] = null;
    	});

    	return {
    		c() {
    			div = element("div");

    			for (let i = 0; i < each_blocks.length; i += 1) {
    				each_blocks[i].c();
    			}
    			attr(div, "class", "grid svelte-1u3y174");
    		},

    		m(target, anchor) {
    			insert(target, div, anchor);

    			for (let i = 0; i < each_blocks.length; i += 1) {
    				each_blocks[i].m(div, null);
    			}

    			current = true;
    		},

    		p(changed, ctx) {
    			if (changed.shape || changed.GRID_SIZE || changed.state) {
    				each_value_1 = ctx.shape.grid;

    				let i;
    				for (i = 0; i < each_value_1.length; i += 1) {
    					const child_ctx = get_each_context_1(ctx, each_value_1, i);

    					if (each_blocks[i]) {
    						each_blocks[i].p(changed, child_ctx);
    						transition_in(each_blocks[i], 1);
    					} else {
    						each_blocks[i] = create_each_block_1(child_ctx);
    						each_blocks[i].c();
    						transition_in(each_blocks[i], 1);
    						each_blocks[i].m(div, null);
    					}
    				}

    				group_outros();
    				for (i = each_value_1.length; i < each_blocks.length; i += 1) {
    					out(i);
    				}
    				check_outros();
    			}
    		},

    		i(local) {
    			if (current) return;
    			for (let i = 0; i < each_value_1.length; i += 1) {
    				transition_in(each_blocks[i]);
    			}

    			current = true;
    		},

    		o(local) {
    			each_blocks = each_blocks.filter(Boolean);
    			for (let i = 0; i < each_blocks.length; i += 1) {
    				transition_out(each_blocks[i]);
    			}

    			current = false;
    		},

    		d(detaching) {
    			if (detaching) {
    				detach(div);
    			}

    			destroy_each(each_blocks, detaching);
    		}
    	};
    }

    // (231:6) {#each shape.grid as cell, index}
    function create_each_block_1(ctx) {
    	var current;

    	var cell = new Cell({
    		props: {
    		x: (ctx.cell.x + ctx.shape.offset.x) / GRID_SIZE$1,
    		y: (ctx.cell.y + ctx.shape.offset.y) / GRID_SIZE$1,
    		state: ctx.state
    	}
    	});

    	return {
    		c() {
    			cell.$$.fragment.c();
    		},

    		m(target, anchor) {
    			mount_component(cell, target, anchor);
    			current = true;
    		},

    		p(changed, ctx) {
    			var cell_changes = {};
    			if (changed.shape) cell_changes.x = (ctx.cell.x + ctx.shape.offset.x) / GRID_SIZE$1;
    			if (changed.shape) cell_changes.y = (ctx.cell.y + ctx.shape.offset.y) / GRID_SIZE$1;
    			if (changed.state) cell_changes.state = ctx.state;
    			cell.$set(cell_changes);
    		},

    		i(local) {
    			if (current) return;
    			transition_in(cell.$$.fragment, local);

    			current = true;
    		},

    		o(local) {
    			transition_out(cell.$$.fragment, local);
    			current = false;
    		},

    		d(detaching) {
    			destroy_component(cell, detaching);
    		}
    	};
    }

    // (236:2) {#if _selectedCount > 1}
    function create_if_block_3(ctx) {
    	var div, span, t;

    	return {
    		c() {
    			div = element("div");
    			span = element("span");
    			t = text(ctx._selectedCount);
    			attr(span, "class", "svelte-1u3y174");
    			attr(div, "class", "count svelte-1u3y174");
    		},

    		m(target, anchor) {
    			insert(target, div, anchor);
    			append(div, span);
    			append(span, t);
    		},

    		p(changed, ctx) {
    			if (changed._selectedCount) {
    				set_data(t, ctx._selectedCount);
    			}
    		},

    		d(detaching) {
    			if (detaching) {
    				detach(div);
    			}
    		}
    	};
    }

    // (244:2) {#if contextVisible}
    function create_if_block(ctx) {
    	var div2, div0, t0, t1, t2, show_if = ctx.playerRoles.indexOf('admin') !== -1, t3, t4, div1, dispose;

    	var if_block0 = (!!ctx.currentItem && ctx.currentItem._type === 'ItemKey') && create_if_block_2(ctx);

    	var if_block1 = (show_if) && create_if_block_1(ctx);

    	let each_value = ctx.actions;

    	let each_blocks = [];

    	for (let i = 0; i < each_value.length; i += 1) {
    		each_blocks[i] = create_each_block(get_each_context(ctx, each_value, i));
    	}

    	return {
    		c() {
    			div2 = element("div");
    			div0 = element("div");
    			t0 = text(ctx.label);
    			t1 = space();
    			if (if_block0) if_block0.c();
    			t2 = space();
    			if (if_block1) if_block1.c();
    			t3 = space();

    			for (let i = 0; i < each_blocks.length; i += 1) {
    				each_blocks[i].c();
    			}

    			t4 = space();
    			div1 = element("div");
    			div1.textContent = "Fermer";
    			attr(div0, "class", "context-action label svelte-1u3y174");
    			attr(div1, "class", "context-action close svelte-1u3y174");
    			attr(div2, "class", "context svelte-1u3y174");
    			set_style(div2, "left", "" + ctx.contextX + "px");
    			set_style(div2, "top", "" + ctx.contextY + "px");
    			dispose = listen(div1, "mousedown", ctx.mousedown_handler_2);
    		},

    		m(target, anchor) {
    			insert(target, div2, anchor);
    			append(div2, div0);
    			append(div0, t0);
    			append(div2, t1);
    			if (if_block0) if_block0.m(div2, null);
    			append(div2, t2);
    			if (if_block1) if_block1.m(div2, null);
    			append(div2, t3);

    			for (let i = 0; i < each_blocks.length; i += 1) {
    				each_blocks[i].m(div2, null);
    			}

    			append(div2, t4);
    			append(div2, div1);
    		},

    		p(changed, ctx) {
    			if (changed.label) {
    				set_data(t0, ctx.label);
    			}

    			if (!!ctx.currentItem && ctx.currentItem._type === 'ItemKey') {
    				if (if_block0) {
    					if_block0.p(changed, ctx);
    				} else {
    					if_block0 = create_if_block_2(ctx);
    					if_block0.c();
    					if_block0.m(div2, t2);
    				}
    			} else if (if_block0) {
    				if_block0.d(1);
    				if_block0 = null;
    			}

    			if (changed.playerRoles) show_if = ctx.playerRoles.indexOf('admin') !== -1;

    			if (show_if) {
    				if (!if_block1) {
    					if_block1 = create_if_block_1(ctx);
    					if_block1.c();
    					if_block1.m(div2, t3);
    				}
    			} else if (if_block1) {
    				if_block1.d(1);
    				if_block1 = null;
    			}

    			if (changed.actions) {
    				each_value = ctx.actions;

    				let i;
    				for (i = 0; i < each_value.length; i += 1) {
    					const child_ctx = get_each_context(ctx, each_value, i);

    					if (each_blocks[i]) {
    						each_blocks[i].p(changed, child_ctx);
    					} else {
    						each_blocks[i] = create_each_block(child_ctx);
    						each_blocks[i].c();
    						each_blocks[i].m(div2, t4);
    					}
    				}

    				for (; i < each_blocks.length; i += 1) {
    					each_blocks[i].d(1);
    				}
    				each_blocks.length = each_value.length;
    			}

    			if (changed.contextX) {
    				set_style(div2, "left", "" + ctx.contextX + "px");
    			}

    			if (changed.contextY) {
    				set_style(div2, "top", "" + ctx.contextY + "px");
    			}
    		},

    		d(detaching) {
    			if (detaching) {
    				detach(div2);
    			}

    			if (if_block0) if_block0.d();
    			if (if_block1) if_block1.d();

    			destroy_each(each_blocks, detaching);

    			dispose();
    		}
    	};
    }

    // (247:6) {#if !!currentItem && currentItem._type === 'ItemKey'}
    function create_if_block_2(ctx) {
    	var div, t_value = ctx.currentItem.targets.filter(func).map(func_1).join(', ') + "", t;

    	return {
    		c() {
    			div = element("div");
    			t = text(t_value);
    			attr(div, "class", "context-action svelte-1u3y174");
    		},

    		m(target, anchor) {
    			insert(target, div, anchor);
    			append(div, t);
    		},

    		p(changed, ctx) {
    			if ((changed.currentItem) && t_value !== (t_value = ctx.currentItem.targets.filter(func).map(func_1).join(', ') + "")) {
    				set_data(t, t_value);
    			}
    		},

    		d(detaching) {
    			if (detaching) {
    				detach(div);
    			}
    		}
    	};
    }

    // (250:6) {#if playerRoles.indexOf('admin') !== -1}
    function create_if_block_1(ctx) {
    	var div, dispose;

    	return {
    		c() {
    			div = element("div");
    			div.textContent = "(admin) Delete Item";
    			attr(div, "class", "context-action svelte-1u3y174");
    			dispose = listen(div, "mousedown", ctx.mousedown_handler);
    		},

    		m(target, anchor) {
    			insert(target, div, anchor);
    		},

    		d(detaching) {
    			if (detaching) {
    				detach(div);
    			}

    			dispose();
    		}
    	};
    }

    // (253:6) {#each actions as action}
    function create_each_block(ctx) {
    	var div, t_value = ctx.action + "", t, dispose;

    	function mousedown_handler_1(...args) {
    		return ctx.mousedown_handler_1(ctx, ...args);
    	}

    	return {
    		c() {
    			div = element("div");
    			t = text(t_value);
    			attr(div, "class", "context-action svelte-1u3y174");
    			dispose = listen(div, "mousedown", mousedown_handler_1);
    		},

    		m(target, anchor) {
    			insert(target, div, anchor);
    			append(div, t);
    		},

    		p(changed, new_ctx) {
    			ctx = new_ctx;
    			if ((changed.actions) && t_value !== (t_value = ctx.action + "")) {
    				set_data(t, t_value);
    			}
    		},

    		d(detaching) {
    			if (detaching) {
    				detach(div);
    			}

    			dispose();
    		}
    	};
    }

    function create_fragment$1(ctx) {
    	var div1, t0, t1, div0, img_1, t2, div1_class_value, current, dispose;

    	var if_block0 = (!ctx.raw) && create_if_block_4(ctx);

    	var if_block1 = (ctx._selectedCount > 1) && create_if_block_3(ctx);

    	var if_block2 = (ctx.contextVisible) && create_if_block(ctx);

    	return {
    		c() {
    			div1 = element("div");
    			if (if_block0) if_block0.c();
    			t0 = space();
    			if (if_block1) if_block1.c();
    			t1 = space();
    			div0 = element("div");
    			img_1 = element("img");
    			t2 = space();
    			if (if_block2) if_block2.c();
    			attr(img_1, "src", ctx.img);
    			attr(img_1, "alt", ctx._id);
    			set_style(img_1, "margin-left", "" + (ctx.raw ? 0 : ctx.offset.x) + "px");
    			set_style(img_1, "margin-top", "" + (ctx.raw ? 0 : ctx.offset.y) + "px");
    			attr(div0, "class", "image svelte-1u3y174");
    			attr(div1, "class", div1_class_value = "item " + (!!ctx.drag ? 'disablemouse' : '') + " " + (ctx.raw ? 'raw' : '') + " svelte-1u3y174");
    			set_style(div1, "left", "" + ctx.x * GRID_SIZE$1 + "px");
    			set_style(div1, "top", "" + ctx.y * GRID_SIZE$1 + "px");

    			dispose = [
    				listen(img_1, "error", onImgError),
    				listen(div1, "mousedown", ctx.onMouseDown),
    				listen(div1, "contextmenu", ctx.onContextMenu),
    				listen(div1, "mousewheel", ctx.onMouseWheel)
    			];
    		},

    		m(target, anchor) {
    			insert(target, div1, anchor);
    			if (if_block0) if_block0.m(div1, null);
    			append(div1, t0);
    			if (if_block1) if_block1.m(div1, null);
    			append(div1, t1);
    			append(div1, div0);
    			append(div0, img_1);
    			append(div1, t2);
    			if (if_block2) if_block2.m(div1, null);
    			current = true;
    		},

    		p(changed, ctx) {
    			if (!ctx.raw) {
    				if (if_block0) {
    					if_block0.p(changed, ctx);
    					transition_in(if_block0, 1);
    				} else {
    					if_block0 = create_if_block_4(ctx);
    					if_block0.c();
    					transition_in(if_block0, 1);
    					if_block0.m(div1, t0);
    				}
    			} else if (if_block0) {
    				group_outros();
    				transition_out(if_block0, 1, 1, () => {
    					if_block0 = null;
    				});
    				check_outros();
    			}

    			if (ctx._selectedCount > 1) {
    				if (if_block1) {
    					if_block1.p(changed, ctx);
    				} else {
    					if_block1 = create_if_block_3(ctx);
    					if_block1.c();
    					if_block1.m(div1, t1);
    				}
    			} else if (if_block1) {
    				if_block1.d(1);
    				if_block1 = null;
    			}

    			if (!current || changed.img) {
    				attr(img_1, "src", ctx.img);
    			}

    			if (!current || changed._id) {
    				attr(img_1, "alt", ctx._id);
    			}

    			if (!current || changed.raw || changed.offset) {
    				set_style(img_1, "margin-left", "" + (ctx.raw ? 0 : ctx.offset.x) + "px");
    				set_style(img_1, "margin-top", "" + (ctx.raw ? 0 : ctx.offset.y) + "px");
    			}

    			if (ctx.contextVisible) {
    				if (if_block2) {
    					if_block2.p(changed, ctx);
    				} else {
    					if_block2 = create_if_block(ctx);
    					if_block2.c();
    					if_block2.m(div1, null);
    				}
    			} else if (if_block2) {
    				if_block2.d(1);
    				if_block2 = null;
    			}

    			if ((!current || changed.drag || changed.raw) && div1_class_value !== (div1_class_value = "item " + (!!ctx.drag ? 'disablemouse' : '') + " " + (ctx.raw ? 'raw' : '') + " svelte-1u3y174")) {
    				attr(div1, "class", div1_class_value);
    			}

    			if (!current || changed.x) {
    				set_style(div1, "left", "" + ctx.x * GRID_SIZE$1 + "px");
    			}

    			if (!current || changed.y) {
    				set_style(div1, "top", "" + ctx.y * GRID_SIZE$1 + "px");
    			}
    		},

    		i(local) {
    			if (current) return;
    			transition_in(if_block0);
    			current = true;
    		},

    		o(local) {
    			transition_out(if_block0);
    			current = false;
    		},

    		d(detaching) {
    			if (detaching) {
    				detach(div1);
    			}

    			if (if_block0) if_block0.d();
    			if (if_block1) if_block1.d();
    			if (if_block2) if_block2.d();
    			run_all(dispose);
    		}
    	};
    }

    const GRID_SIZE$1 = 100;

    function onImgError(e) {
      
      this.onerror = null;
      this.src     = 'http://assets/core-assets/dist/img/items/undefined.png';

      return true;
    }

    const func = (e) => !!e && e.type === 'vehicle';

    const func_1 = (e) => e.plate;

    function instance$1($$self, $$props, $$invalidate) {

      let { playerRoles      = [], raw              = false, _id              = '', x                = 0, y                = 0, image            = '', label            = '', shape            = [], actions          = [], count            = 1, drag             = null, currentContainer = null, currentItem      = null, mouseX           = 0, mouseY           = 0, state            = -1, container        = '', getShape       = null, computeShapes  = null, check          = null, getImage       = null, setDraggedItem = null, getCache       = null, setDragCount   = null, isBody = false } = $$props;

      let img            = '';
      let offset         = {x: 0, y: 0};
      let contextVisible = false;
      let contextX       = 0;
      let contextY       = 0;

      let _shape         = [];
      let _lastCount     = count;
      let _selectedCount = count;

      function onMouseDown(e) {
        
        e.preventDefault();
        
        if(e.button === 0)
          setDraggedItem(_id, container, _selectedCount);
      }

      function onMouseWheel(e) {

        e.preventDefault();
        e.stopPropagation();

        if(e.deltaY < 0) {

        $$invalidate('_selectedCount', _selectedCount++, _selectedCount);

          if(_selectedCount > count)
            $$invalidate('_selectedCount', _selectedCount = count);

        } else if(e.deltaY > 0) {

          $$invalidate('_selectedCount', _selectedCount--, _selectedCount);

          if(_selectedCount < 1)
            $$invalidate('_selectedCount', _selectedCount = 1);

        }

        setDragCount(_selectedCount);

      }

      function onContextMenu(e) {
        
        e.preventDefault();

        if(isBody)
          return;

        $$invalidate('contextX', contextX       = e.screenX);
        $$invalidate('contextY', contextY       = e.screenY);
        $$invalidate('contextVisible', contextVisible = true);

      }

      function onAction(e, action) {
        
        e.stopPropagation();
        $$invalidate('contextVisible', contextVisible = false);

        window.parent.postMessage(JSON.stringify({action: 'item_action', _id, itemAction: action}), '*');
      }

      function onAdminAction(e, action) {
        
        e.stopPropagation();
        $$invalidate('contextVisible', contextVisible = false);

        window.parent.postMessage(JSON.stringify({action: 'item_action_admin', _id, itemAction: action}), '*');
      }

      beforeUpdate(() => {

        $$invalidate('img', img           = getImage(_id));
        const result  = getShape(shape);
        _shape        = result.shape;
        $$invalidate('offset', offset        = result.offset);

        if(count !== _lastCount) {
          $$invalidate('_selectedCount', _selectedCount = count);
          _lastCount     = count;
        }

        if((drag === _id) && currentContainer && currentItem) {

          $$invalidate('x', x = mouseX);
          $$invalidate('y', y = mouseY);
          
          if(check(currentContainer, currentItem, x, y))
            $$invalidate('state', state = 1);
          else
            $$invalidate('state', state = 0);
        }

      });

    	const mousedown_handler = (e) => onAdminAction(e, 'delete_item');

    	const mousedown_handler_1 = ({ action }, e) => onAction(e, action);

    	const mousedown_handler_2 = (e) => {e.stopPropagation(); $$invalidate('contextVisible', contextVisible = false);};

    	$$self.$set = $$props => {
    		if ('playerRoles' in $$props) $$invalidate('playerRoles', playerRoles = $$props.playerRoles);
    		if ('raw' in $$props) $$invalidate('raw', raw = $$props.raw);
    		if ('_id' in $$props) $$invalidate('_id', _id = $$props._id);
    		if ('x' in $$props) $$invalidate('x', x = $$props.x);
    		if ('y' in $$props) $$invalidate('y', y = $$props.y);
    		if ('image' in $$props) $$invalidate('image', image = $$props.image);
    		if ('label' in $$props) $$invalidate('label', label = $$props.label);
    		if ('shape' in $$props) $$invalidate('shape', shape = $$props.shape);
    		if ('actions' in $$props) $$invalidate('actions', actions = $$props.actions);
    		if ('count' in $$props) $$invalidate('count', count = $$props.count);
    		if ('drag' in $$props) $$invalidate('drag', drag = $$props.drag);
    		if ('currentContainer' in $$props) $$invalidate('currentContainer', currentContainer = $$props.currentContainer);
    		if ('currentItem' in $$props) $$invalidate('currentItem', currentItem = $$props.currentItem);
    		if ('mouseX' in $$props) $$invalidate('mouseX', mouseX = $$props.mouseX);
    		if ('mouseY' in $$props) $$invalidate('mouseY', mouseY = $$props.mouseY);
    		if ('state' in $$props) $$invalidate('state', state = $$props.state);
    		if ('container' in $$props) $$invalidate('container', container = $$props.container);
    		if ('getShape' in $$props) $$invalidate('getShape', getShape = $$props.getShape);
    		if ('computeShapes' in $$props) $$invalidate('computeShapes', computeShapes = $$props.computeShapes);
    		if ('check' in $$props) $$invalidate('check', check = $$props.check);
    		if ('getImage' in $$props) $$invalidate('getImage', getImage = $$props.getImage);
    		if ('setDraggedItem' in $$props) $$invalidate('setDraggedItem', setDraggedItem = $$props.setDraggedItem);
    		if ('getCache' in $$props) $$invalidate('getCache', getCache = $$props.getCache);
    		if ('setDragCount' in $$props) $$invalidate('setDragCount', setDragCount = $$props.setDragCount);
    		if ('isBody' in $$props) $$invalidate('isBody', isBody = $$props.isBody);
    	};

    	return {
    		playerRoles,
    		raw,
    		_id,
    		x,
    		y,
    		image,
    		label,
    		shape,
    		actions,
    		count,
    		drag,
    		currentContainer,
    		currentItem,
    		mouseX,
    		mouseY,
    		state,
    		container,
    		getShape,
    		computeShapes,
    		check,
    		getImage,
    		setDraggedItem,
    		getCache,
    		setDragCount,
    		isBody,
    		img,
    		offset,
    		contextVisible,
    		contextX,
    		contextY,
    		_selectedCount,
    		onMouseDown,
    		onMouseWheel,
    		onContextMenu,
    		onAction,
    		onAdminAction,
    		mousedown_handler,
    		mousedown_handler_1,
    		mousedown_handler_2
    	};
    }

    class Item extends SvelteComponent {
    	constructor(options) {
    		super();
    		init(this, options, instance$1, create_fragment$1, safe_not_equal, ["playerRoles", "raw", "_id", "x", "y", "image", "label", "shape", "actions", "count", "drag", "currentContainer", "currentItem", "mouseX", "mouseY", "state", "container", "getShape", "computeShapes", "check", "getImage", "setDraggedItem", "getCache", "setDragCount", "isBody"]);
    	}
    }

    /* src\Container.svelte generated by Svelte v3.12.1 */

    function get_each_context$1(ctx, list, i) {
    	const child_ctx = Object.create(ctx);
    	child_ctx.item = list[i];
    	child_ctx.i = i;
    	return child_ctx;
    }

    function get_each_context_2(ctx, list, i) {
    	const child_ctx = Object.create(ctx);
    	child_ctx.cell = list[i];
    	child_ctx.x = i;
    	return child_ctx;
    }

    function get_each_context_1$1(ctx, list, i) {
    	const child_ctx = Object.create(ctx);
    	child_ctx.row = list[i];
    	child_ctx.y = i;
    	return child_ctx;
    }

    // (221:2) {:else}
    function create_else_block(ctx) {
    	var each_1_anchor, current;

    	let each_value_1 = ctx.grid;

    	let each_blocks = [];

    	for (let i = 0; i < each_value_1.length; i += 1) {
    		each_blocks[i] = create_each_block_1$1(get_each_context_1$1(ctx, each_value_1, i));
    	}

    	const out = i => transition_out(each_blocks[i], 1, 1, () => {
    		each_blocks[i] = null;
    	});

    	return {
    		c() {
    			for (let i = 0; i < each_blocks.length; i += 1) {
    				each_blocks[i].c();
    			}

    			each_1_anchor = empty();
    		},

    		m(target, anchor) {
    			for (let i = 0; i < each_blocks.length; i += 1) {
    				each_blocks[i].m(target, anchor);
    			}

    			insert(target, each_1_anchor, anchor);
    			current = true;
    		},

    		p(changed, ctx) {
    			if (changed.grid) {
    				each_value_1 = ctx.grid;

    				let i;
    				for (i = 0; i < each_value_1.length; i += 1) {
    					const child_ctx = get_each_context_1$1(ctx, each_value_1, i);

    					if (each_blocks[i]) {
    						each_blocks[i].p(changed, child_ctx);
    						transition_in(each_blocks[i], 1);
    					} else {
    						each_blocks[i] = create_each_block_1$1(child_ctx);
    						each_blocks[i].c();
    						transition_in(each_blocks[i], 1);
    						each_blocks[i].m(each_1_anchor.parentNode, each_1_anchor);
    					}
    				}

    				group_outros();
    				for (i = each_value_1.length; i < each_blocks.length; i += 1) {
    					out(i);
    				}
    				check_outros();
    			}
    		},

    		i(local) {
    			if (current) return;
    			for (let i = 0; i < each_value_1.length; i += 1) {
    				transition_in(each_blocks[i]);
    			}

    			current = true;
    		},

    		o(local) {
    			each_blocks = each_blocks.filter(Boolean);
    			for (let i = 0; i < each_blocks.length; i += 1) {
    				transition_out(each_blocks[i]);
    			}

    			current = false;
    		},

    		d(detaching) {
    			destroy_each(each_blocks, detaching);

    			if (detaching) {
    				detach(each_1_anchor);
    			}
    		}
    	};
    }

    // (219:2) {#if raw}
    function create_if_block_2$1(ctx) {
    	var current;

    	var cell = new Cell({
    		props: {
    		x: 0,
    		y: 0,
    		raw: true
    	}
    	});
    	cell.$on("hover", ctx.onCellHover);
    	cell.$on("blur", ctx.onCellBlur);

    	return {
    		c() {
    			cell.$$.fragment.c();
    		},

    		m(target, anchor) {
    			mount_component(cell, target, anchor);
    			current = true;
    		},

    		p: noop,

    		i(local) {
    			if (current) return;
    			transition_in(cell.$$.fragment, local);

    			current = true;
    		},

    		o(local) {
    			transition_out(cell.$$.fragment, local);
    			current = false;
    		},

    		d(detaching) {
    			destroy_component(cell, detaching);
    		}
    	};
    }

    // (223:4) {#each row as cell, x}
    function create_each_block_2(ctx) {
    	var current;

    	var cell = new Cell({
    		props: { x: ctx.x, y: ctx.y }
    	});
    	cell.$on("hover", ctx.onCellHover);
    	cell.$on("blur", ctx.onCellBlur);

    	return {
    		c() {
    			cell.$$.fragment.c();
    		},

    		m(target, anchor) {
    			mount_component(cell, target, anchor);
    			current = true;
    		},

    		p: noop,

    		i(local) {
    			if (current) return;
    			transition_in(cell.$$.fragment, local);

    			current = true;
    		},

    		o(local) {
    			transition_out(cell.$$.fragment, local);
    			current = false;
    		},

    		d(detaching) {
    			destroy_component(cell, detaching);
    		}
    	};
    }

    // (222:3) {#each grid as row, y}
    function create_each_block_1$1(ctx) {
    	var each_1_anchor, current;

    	let each_value_2 = ctx.row;

    	let each_blocks = [];

    	for (let i = 0; i < each_value_2.length; i += 1) {
    		each_blocks[i] = create_each_block_2(get_each_context_2(ctx, each_value_2, i));
    	}

    	const out = i => transition_out(each_blocks[i], 1, 1, () => {
    		each_blocks[i] = null;
    	});

    	return {
    		c() {
    			for (let i = 0; i < each_blocks.length; i += 1) {
    				each_blocks[i].c();
    			}

    			each_1_anchor = empty();
    		},

    		m(target, anchor) {
    			for (let i = 0; i < each_blocks.length; i += 1) {
    				each_blocks[i].m(target, anchor);
    			}

    			insert(target, each_1_anchor, anchor);
    			current = true;
    		},

    		p(changed, ctx) {
    			if (changed.grid) {
    				each_value_2 = ctx.row;

    				let i;
    				for (i = 0; i < each_value_2.length; i += 1) {
    					const child_ctx = get_each_context_2(ctx, each_value_2, i);

    					if (each_blocks[i]) {
    						each_blocks[i].p(changed, child_ctx);
    						transition_in(each_blocks[i], 1);
    					} else {
    						each_blocks[i] = create_each_block_2(child_ctx);
    						each_blocks[i].c();
    						transition_in(each_blocks[i], 1);
    						each_blocks[i].m(each_1_anchor.parentNode, each_1_anchor);
    					}
    				}

    				group_outros();
    				for (i = each_value_2.length; i < each_blocks.length; i += 1) {
    					out(i);
    				}
    				check_outros();
    			}
    		},

    		i(local) {
    			if (current) return;
    			for (let i = 0; i < each_value_2.length; i += 1) {
    				transition_in(each_blocks[i]);
    			}

    			current = true;
    		},

    		o(local) {
    			each_blocks = each_blocks.filter(Boolean);
    			for (let i = 0; i < each_blocks.length; i += 1) {
    				transition_out(each_blocks[i]);
    			}

    			current = false;
    		},

    		d(detaching) {
    			destroy_each(each_blocks, detaching);

    			if (detaching) {
    				detach(each_1_anchor);
    			}
    		}
    	};
    }

    // (231:3) {#if item && (item.count > 0) && (item._id !== drag)}
    function create_if_block_1$1(ctx) {
    	var current;

    	var item_spread_levels = [
    		{ playerRoles: ctx.playerRoles },
    		ctx.item,
    		{ container: ctx._id },
    		{ isBody: ctx.isBody },
    		{ raw: ctx.raw },
    		{ drag: ctx.drag },
    		{ currentContainer: ctx.currentContainer },
    		{ currentItem: ctx.currentItem },
    		{ mouseX: ctx.mouseX },
    		{ mouseY: ctx.mouseY },
    		{ getShape: ctx.getShape },
    		{ computeShapes: ctx.computeShapes },
    		{ check: ctx.check },
    		{ getImage: ctx.getImage },
    		{ setDraggedItem: ctx.setDraggedItem },
    		{ getCache: ctx.getCache },
    		{ setDragCount: ctx.setDragCount }
    	];

    	let item_props = {};
    	for (var i = 0; i < item_spread_levels.length; i += 1) {
    		item_props = assign(item_props, item_spread_levels[i]);
    	}
    	var item = new Item({ props: item_props });

    	return {
    		c() {
    			item.$$.fragment.c();
    		},

    		m(target, anchor) {
    			mount_component(item, target, anchor);
    			current = true;
    		},

    		p(changed, ctx) {
    			var item_changes = (changed.playerRoles || changed._items || changed._id || changed.isBody || changed.raw || changed.drag || changed.currentContainer || changed.currentItem || changed.mouseX || changed.mouseY || changed.getShape || changed.computeShapes || changed.check || changed.getImage || changed.setDraggedItem || changed.getCache || changed.setDragCount) ? get_spread_update(item_spread_levels, [
    									(changed.playerRoles) && { playerRoles: ctx.playerRoles },
    			(changed._items) && get_spread_object(ctx.item),
    			(changed._id) && { container: ctx._id },
    			(changed.isBody) && { isBody: ctx.isBody },
    			(changed.raw) && { raw: ctx.raw },
    			(changed.drag) && { drag: ctx.drag },
    			(changed.currentContainer) && { currentContainer: ctx.currentContainer },
    			(changed.currentItem) && { currentItem: ctx.currentItem },
    			(changed.mouseX) && { mouseX: ctx.mouseX },
    			(changed.mouseY) && { mouseY: ctx.mouseY },
    			(changed.getShape) && { getShape: ctx.getShape },
    			(changed.computeShapes) && { computeShapes: ctx.computeShapes },
    			(changed.check) && { check: ctx.check },
    			(changed.getImage) && { getImage: ctx.getImage },
    			(changed.setDraggedItem) && { setDraggedItem: ctx.setDraggedItem },
    			(changed.getCache) && { getCache: ctx.getCache },
    			(changed.setDragCount) && { setDragCount: ctx.setDragCount }
    								]) : {};
    			item.$set(item_changes);
    		},

    		i(local) {
    			if (current) return;
    			transition_in(item.$$.fragment, local);

    			current = true;
    		},

    		o(local) {
    			transition_out(item.$$.fragment, local);
    			current = false;
    		},

    		d(detaching) {
    			destroy_component(item, detaching);
    		}
    	};
    }

    // (230:2) {#each _items as item, i}
    function create_each_block$1(ctx) {
    	var if_block_anchor, current;

    	var if_block = (ctx.item && (ctx.item.count > 0) && (ctx.item._id !== ctx.drag)) && create_if_block_1$1(ctx);

    	return {
    		c() {
    			if (if_block) if_block.c();
    			if_block_anchor = empty();
    		},

    		m(target, anchor) {
    			if (if_block) if_block.m(target, anchor);
    			insert(target, if_block_anchor, anchor);
    			current = true;
    		},

    		p(changed, ctx) {
    			if (ctx.item && (ctx.item.count > 0) && (ctx.item._id !== ctx.drag)) {
    				if (if_block) {
    					if_block.p(changed, ctx);
    					transition_in(if_block, 1);
    				} else {
    					if_block = create_if_block_1$1(ctx);
    					if_block.c();
    					transition_in(if_block, 1);
    					if_block.m(if_block_anchor.parentNode, if_block_anchor);
    				}
    			} else if (if_block) {
    				group_outros();
    				transition_out(if_block, 1, 1, () => {
    					if_block = null;
    				});
    				check_outros();
    			}
    		},

    		i(local) {
    			if (current) return;
    			transition_in(if_block);
    			current = true;
    		},

    		o(local) {
    			transition_out(if_block);
    			current = false;
    		},

    		d(detaching) {
    			if (if_block) if_block.d(detaching);

    			if (detaching) {
    				detach(if_block_anchor);
    			}
    		}
    	};
    }

    // (253:2) {#if !isBody && (drag !== null) && currentContainer && (currentContainer._id === _id)}
    function create_if_block$1(ctx) {
    	var current;

    	var item_spread_levels = [
    		{ playerRoles: ctx.playerRoles },
    		ctx._draggedItem,
    		{ count: ctx.dragCount },
    		{ container: ctx._id },
    		{ isBody: ctx.isBody },
    		{ raw: ctx.raw },
    		{ drag: ctx.drag },
    		{ currentContainer: ctx.currentContainer },
    		{ currentItem: ctx.currentItem },
    		{ mouseX: ctx.mouseX },
    		{ mouseY: ctx.mouseY },
    		{ getShape: ctx.getShape },
    		{ computeShapes: ctx.computeShapes },
    		{ check: ctx.check },
    		{ getImage: ctx.getImage },
    		{ setDraggedItem: ctx.setDraggedItem },
    		{ getCache: ctx.getCache },
    		{ "[setDragCount]": true }
    	];

    	let item_props = {};
    	for (var i = 0; i < item_spread_levels.length; i += 1) {
    		item_props = assign(item_props, item_spread_levels[i]);
    	}
    	var item = new Item({ props: item_props });

    	return {
    		c() {
    			item.$$.fragment.c();
    		},

    		m(target, anchor) {
    			mount_component(item, target, anchor);
    			current = true;
    		},

    		p(changed, ctx) {
    			var item_changes = (changed.playerRoles || changed._draggedItem || changed.dragCount || changed._id || changed.isBody || changed.raw || changed.drag || changed.currentContainer || changed.currentItem || changed.mouseX || changed.mouseY || changed.getShape || changed.computeShapes || changed.check || changed.getImage || changed.setDraggedItem || changed.getCache) ? get_spread_update(item_spread_levels, [
    									(changed.playerRoles) && { playerRoles: ctx.playerRoles },
    			(changed._draggedItem) && get_spread_object(ctx._draggedItem),
    			(changed.dragCount) && { count: ctx.dragCount },
    			(changed._id) && { container: ctx._id },
    			(changed.isBody) && { isBody: ctx.isBody },
    			(changed.raw) && { raw: ctx.raw },
    			(changed.drag) && { drag: ctx.drag },
    			(changed.currentContainer) && { currentContainer: ctx.currentContainer },
    			(changed.currentItem) && { currentItem: ctx.currentItem },
    			(changed.mouseX) && { mouseX: ctx.mouseX },
    			(changed.mouseY) && { mouseY: ctx.mouseY },
    			(changed.getShape) && { getShape: ctx.getShape },
    			(changed.computeShapes) && { computeShapes: ctx.computeShapes },
    			(changed.check) && { check: ctx.check },
    			(changed.getImage) && { getImage: ctx.getImage },
    			(changed.setDraggedItem) && { setDraggedItem: ctx.setDraggedItem },
    			(changed.getCache) && { getCache: ctx.getCache },
    			item_spread_levels[17]
    								]) : {};
    			item.$set(item_changes);
    		},

    		i(local) {
    			if (current) return;
    			transition_in(item.$$.fragment, local);

    			current = true;
    		},

    		o(local) {
    			transition_out(item.$$.fragment, local);
    			current = false;
    		},

    		d(detaching) {
    			destroy_component(item, detaching);
    		}
    	};
    }

    function create_fragment$2(ctx) {
    	var div3, div0, t0, t1, div1, current_block_type_index, if_block0, t2, div2, t3, div3_class_value, current;

    	var if_block_creators = [
    		create_if_block_2$1,
    		create_else_block
    	];

    	var if_blocks = [];

    	function select_block_type(changed, ctx) {
    		if (ctx.raw) return 0;
    		return 1;
    	}

    	current_block_type_index = select_block_type(null, ctx);
    	if_block0 = if_blocks[current_block_type_index] = if_block_creators[current_block_type_index](ctx);

    	let each_value = ctx._items;

    	let each_blocks = [];

    	for (let i = 0; i < each_value.length; i += 1) {
    		each_blocks[i] = create_each_block$1(get_each_context$1(ctx, each_value, i));
    	}

    	const out = i => transition_out(each_blocks[i], 1, 1, () => {
    		each_blocks[i] = null;
    	});

    	var if_block1 = (!ctx.isBody && (ctx.drag !== null) && ctx.currentContainer && (ctx.currentContainer._id === ctx._id)) && create_if_block$1(ctx);

    	return {
    		c() {
    			div3 = element("div");
    			div0 = element("div");
    			t0 = text(ctx.label);
    			t1 = space();
    			div1 = element("div");
    			if_block0.c();
    			t2 = space();
    			div2 = element("div");

    			for (let i = 0; i < each_blocks.length; i += 1) {
    				each_blocks[i].c();
    			}

    			t3 = space();
    			if (if_block1) if_block1.c();
    			attr(div0, "class", "container-label svelte-1s7sot0");
    			attr(div1, "class", "container-grid svelte-1s7sot0");
    			attr(div2, "class", "container-items svelte-1s7sot0");
    			attr(div3, "class", div3_class_value = "container " + (ctx.raw ? 'raw' : '') + " " + ctx.cls + " svelte-1s7sot0");
    			set_style(div3, "width", "" + (ctx.cols * GRID_SIZE$2 + 1) + "px");
    			set_style(div3, "height", "" + (ctx.rows * GRID_SIZE$2 + 1) + "px");
    			attr(div3, "data-id", ctx._id);
    		},

    		m(target, anchor) {
    			insert(target, div3, anchor);
    			append(div3, div0);
    			append(div0, t0);
    			append(div3, t1);
    			append(div3, div1);
    			if_blocks[current_block_type_index].m(div1, null);
    			append(div3, t2);
    			append(div3, div2);

    			for (let i = 0; i < each_blocks.length; i += 1) {
    				each_blocks[i].m(div2, null);
    			}

    			append(div2, t3);
    			if (if_block1) if_block1.m(div2, null);
    			current = true;
    		},

    		p(changed, ctx) {
    			if (!current || changed.label) {
    				set_data(t0, ctx.label);
    			}

    			var previous_block_index = current_block_type_index;
    			current_block_type_index = select_block_type(changed, ctx);
    			if (current_block_type_index === previous_block_index) {
    				if_blocks[current_block_type_index].p(changed, ctx);
    			} else {
    				group_outros();
    				transition_out(if_blocks[previous_block_index], 1, 1, () => {
    					if_blocks[previous_block_index] = null;
    				});
    				check_outros();

    				if_block0 = if_blocks[current_block_type_index];
    				if (!if_block0) {
    					if_block0 = if_blocks[current_block_type_index] = if_block_creators[current_block_type_index](ctx);
    					if_block0.c();
    				}
    				transition_in(if_block0, 1);
    				if_block0.m(div1, null);
    			}

    			if (changed._items || changed.drag || changed.playerRoles || changed._id || changed.isBody || changed.raw || changed.currentContainer || changed.currentItem || changed.mouseX || changed.mouseY || changed.getShape || changed.computeShapes || changed.check || changed.getImage || changed.setDraggedItem || changed.getCache || changed.setDragCount) {
    				each_value = ctx._items;

    				let i;
    				for (i = 0; i < each_value.length; i += 1) {
    					const child_ctx = get_each_context$1(ctx, each_value, i);

    					if (each_blocks[i]) {
    						each_blocks[i].p(changed, child_ctx);
    						transition_in(each_blocks[i], 1);
    					} else {
    						each_blocks[i] = create_each_block$1(child_ctx);
    						each_blocks[i].c();
    						transition_in(each_blocks[i], 1);
    						each_blocks[i].m(div2, t3);
    					}
    				}

    				group_outros();
    				for (i = each_value.length; i < each_blocks.length; i += 1) {
    					out(i);
    				}
    				check_outros();
    			}

    			if (!ctx.isBody && (ctx.drag !== null) && ctx.currentContainer && (ctx.currentContainer._id === ctx._id)) {
    				if (if_block1) {
    					if_block1.p(changed, ctx);
    					transition_in(if_block1, 1);
    				} else {
    					if_block1 = create_if_block$1(ctx);
    					if_block1.c();
    					transition_in(if_block1, 1);
    					if_block1.m(div2, null);
    				}
    			} else if (if_block1) {
    				group_outros();
    				transition_out(if_block1, 1, 1, () => {
    					if_block1 = null;
    				});
    				check_outros();
    			}

    			if ((!current || changed.raw || changed.cls) && div3_class_value !== (div3_class_value = "container " + (ctx.raw ? 'raw' : '') + " " + ctx.cls + " svelte-1s7sot0")) {
    				attr(div3, "class", div3_class_value);
    			}

    			if (!current || changed.cols) {
    				set_style(div3, "width", "" + (ctx.cols * GRID_SIZE$2 + 1) + "px");
    			}

    			if (!current || changed.rows) {
    				set_style(div3, "height", "" + (ctx.rows * GRID_SIZE$2 + 1) + "px");
    			}

    			if (!current || changed._id) {
    				attr(div3, "data-id", ctx._id);
    			}
    		},

    		i(local) {
    			if (current) return;
    			transition_in(if_block0);

    			for (let i = 0; i < each_value.length; i += 1) {
    				transition_in(each_blocks[i]);
    			}

    			transition_in(if_block1);
    			current = true;
    		},

    		o(local) {
    			transition_out(if_block0);

    			each_blocks = each_blocks.filter(Boolean);
    			for (let i = 0; i < each_blocks.length; i += 1) {
    				transition_out(each_blocks[i]);
    			}

    			transition_out(if_block1);
    			current = false;
    		},

    		d(detaching) {
    			if (detaching) {
    				detach(div3);
    			}

    			if_blocks[current_block_type_index].d();

    			destroy_each(each_blocks, detaching);

    			if (if_block1) if_block1.d();
    		}
    	};
    }

    const GRID_SIZE$2 = 100;

    function instance$2($$self, $$props, $$invalidate) {
    	

    	const dispatch = createEventDispatcher();

    	let { playerRoles      = [], cls              = '', raw              = false, filter           = e => true } = $$props;
    	let { _id              = '', rows             = 0, cols             = 0, items            = [], name             = '', label            = '', grid             = [], drag             = null, currentContainer = null, currentItem      = null, mouseX           = 0, mouseY           = 0, dragCount        = 0, getShape       = null, computeShapes  = null, check          = null, getImage       = null, setDraggedItem = null, getCache       = null, setDragCount   = null, isBody = false } = $$props;

    	let _items       = [];
    	let _draggedItem = null;

      /* methods */
    	function getGrid() {

    		const grid = [];

    		for(let y=0; y<rows; y++) {
    			
    			grid[y] = [];

    			for(let x=0; x<cols; x++)
    				grid[y][x] = false;
    		}

    		return grid;

    	}
    	/* callbacks */
    	function onCellHover(e) {
    		dispatch('cell_hover',{container: _id, x: e.detail.x, y: e.detail.y});
    	}

    	function onCellBlur(e) {
    		dispatch('cell_blur',{container: _id, x: e.detail.x, y: e.detail.y});
    	}

    	beforeUpdate(() => {

    		$$invalidate('grid', grid = getGrid());

    		if(rows === -1)
          $$invalidate('raw', raw = true);
        else
    			$$invalidate('raw', raw = false);
    			
    		$$invalidate('_items', _items = items.filter(filter));

    		if(drag !== null && currentContainer && currentContainer._id === _id)
    			$$invalidate('_draggedItem', _draggedItem = getCache().items[drag]);

    	});

    	$$self.$set = $$props => {
    		if ('playerRoles' in $$props) $$invalidate('playerRoles', playerRoles = $$props.playerRoles);
    		if ('cls' in $$props) $$invalidate('cls', cls = $$props.cls);
    		if ('raw' in $$props) $$invalidate('raw', raw = $$props.raw);
    		if ('filter' in $$props) $$invalidate('filter', filter = $$props.filter);
    		if ('_id' in $$props) $$invalidate('_id', _id = $$props._id);
    		if ('rows' in $$props) $$invalidate('rows', rows = $$props.rows);
    		if ('cols' in $$props) $$invalidate('cols', cols = $$props.cols);
    		if ('items' in $$props) $$invalidate('items', items = $$props.items);
    		if ('name' in $$props) $$invalidate('name', name = $$props.name);
    		if ('label' in $$props) $$invalidate('label', label = $$props.label);
    		if ('grid' in $$props) $$invalidate('grid', grid = $$props.grid);
    		if ('drag' in $$props) $$invalidate('drag', drag = $$props.drag);
    		if ('currentContainer' in $$props) $$invalidate('currentContainer', currentContainer = $$props.currentContainer);
    		if ('currentItem' in $$props) $$invalidate('currentItem', currentItem = $$props.currentItem);
    		if ('mouseX' in $$props) $$invalidate('mouseX', mouseX = $$props.mouseX);
    		if ('mouseY' in $$props) $$invalidate('mouseY', mouseY = $$props.mouseY);
    		if ('dragCount' in $$props) $$invalidate('dragCount', dragCount = $$props.dragCount);
    		if ('getShape' in $$props) $$invalidate('getShape', getShape = $$props.getShape);
    		if ('computeShapes' in $$props) $$invalidate('computeShapes', computeShapes = $$props.computeShapes);
    		if ('check' in $$props) $$invalidate('check', check = $$props.check);
    		if ('getImage' in $$props) $$invalidate('getImage', getImage = $$props.getImage);
    		if ('setDraggedItem' in $$props) $$invalidate('setDraggedItem', setDraggedItem = $$props.setDraggedItem);
    		if ('getCache' in $$props) $$invalidate('getCache', getCache = $$props.getCache);
    		if ('setDragCount' in $$props) $$invalidate('setDragCount', setDragCount = $$props.setDragCount);
    		if ('isBody' in $$props) $$invalidate('isBody', isBody = $$props.isBody);
    	};

    	return {
    		playerRoles,
    		cls,
    		raw,
    		filter,
    		_id,
    		rows,
    		cols,
    		items,
    		name,
    		label,
    		grid,
    		drag,
    		currentContainer,
    		currentItem,
    		mouseX,
    		mouseY,
    		dragCount,
    		getShape,
    		computeShapes,
    		check,
    		getImage,
    		setDraggedItem,
    		getCache,
    		setDragCount,
    		isBody,
    		_items,
    		_draggedItem,
    		onCellHover,
    		onCellBlur
    	};
    }

    class Container extends SvelteComponent {
    	constructor(options) {
    		super();
    		init(this, options, instance$2, create_fragment$2, safe_not_equal, ["playerRoles", "cls", "raw", "filter", "_id", "rows", "cols", "items", "name", "label", "grid", "drag", "currentContainer", "currentItem", "mouseX", "mouseY", "dragCount", "getShape", "computeShapes", "check", "getImage", "setDraggedItem", "getCache", "setDragCount", "isBody"]);
    	}
    }

    /* src\App.svelte generated by Svelte v3.12.1 */
    const { window: window_1 } = globals;

    function get_each_context$2(ctx, list, i) {
    	const child_ctx = Object.create(ctx);
    	child_ctx.container = list[i];
    	return child_ctx;
    }

    function get_each_context_1$2(ctx, list, i) {
    	const child_ctx = Object.create(ctx);
    	child_ctx.container = list[i];
    	return child_ctx;
    }

    // (846:3) {#if container._type === 'IdentityComponentContainer'}
    function create_if_block_1$2(ctx) {
    	var t0, t1, t2, t3, t4, t5, current;

    	var container0_spread_levels = [
    		{ cls: 'identity-container-body identity-container-component-4' },
    		ctx.container,
    		{ label: 'pantalon' },
    		{ isBody: true },
    		{ filter: func$1 },
    		{ drag: ctx.drag },
    		{ dragCount: ctx.dragCount },
    		{ currentContainer: ctx.currentContainer },
    		{ currentItem: ctx.currentItem },
    		{ mouseX: ctx.mouseX },
    		{ mouseY: ctx.mouseY },
    		{ getShape: getShape },
    		{ computeShapes: computeShapes },
    		{ check: ctx.check },
    		{ getImage: ctx.getImage },
    		{ setDraggedItem: ctx.setDraggedItem },
    		{ getCache: ctx.getCache },
    		{ setDragCount: ctx.setDragCount }
    	];

    	let container0_props = {};
    	for (var i = 0; i < container0_spread_levels.length; i += 1) {
    		container0_props = assign(container0_props, container0_spread_levels[i]);
    	}
    	var container0 = new Container({ props: container0_props });
    	container0.$on("cell_hover", ctx.onCellHover);
    	container0.$on("cell_blur", onCellBlur);

    	var container1_spread_levels = [
    		{ cls: 'identity-container-body identity-container-component-5' },
    		ctx.container,
    		{ label: 'sac' },
    		{ isBody: true },
    		{ filter: func_1$1 },
    		{ drag: ctx.drag },
    		{ dragCount: ctx.dragCount },
    		{ currentContainer: ctx.currentContainer },
    		{ currentItem: ctx.currentItem },
    		{ mouseX: ctx.mouseX },
    		{ mouseY: ctx.mouseY },
    		{ getShape: getShape },
    		{ computeShapes: computeShapes },
    		{ check: ctx.check },
    		{ getImage: ctx.getImage },
    		{ setDraggedItem: ctx.setDraggedItem },
    		{ getCache: ctx.getCache },
    		{ setDragCount: ctx.setDragCount }
    	];

    	let container1_props = {};
    	for (var i = 0; i < container1_spread_levels.length; i += 1) {
    		container1_props = assign(container1_props, container1_spread_levels[i]);
    	}
    	var container1 = new Container({ props: container1_props });
    	container1.$on("cell_hover", ctx.onCellHover);
    	container1.$on("cell_blur", onCellBlur);

    	var container2_spread_levels = [
    		{ cls: 'identity-container-body identity-container-component-6' },
    		ctx.container,
    		{ label: 'chaussures' },
    		{ isBody: true },
    		{ filter: func_2 },
    		{ drag: ctx.drag },
    		{ dragCount: ctx.dragCount },
    		{ currentContainer: ctx.currentContainer },
    		{ currentItem: ctx.currentItem },
    		{ mouseX: ctx.mouseX },
    		{ mouseY: ctx.mouseY },
    		{ getShape: getShape },
    		{ computeShapes: computeShapes },
    		{ check: ctx.check },
    		{ getImage: ctx.getImage },
    		{ setDraggedItem: ctx.setDraggedItem },
    		{ getCache: ctx.getCache },
    		{ setDragCount: ctx.setDragCount }
    	];

    	let container2_props = {};
    	for (var i = 0; i < container2_spread_levels.length; i += 1) {
    		container2_props = assign(container2_props, container2_spread_levels[i]);
    	}
    	var container2 = new Container({ props: container2_props });
    	container2.$on("cell_hover", ctx.onCellHover);
    	container2.$on("cell_blur", onCellBlur);

    	var container3_spread_levels = [
    		{ cls: 'identity-container-body identity-container-component-7' },
    		ctx.container,
    		{ label: 'chaine' },
    		{ isBody: true },
    		{ filter: func_3 },
    		{ drag: ctx.drag },
    		{ dragCount: ctx.dragCount },
    		{ currentContainer: ctx.currentContainer },
    		{ currentItem: ctx.currentItem },
    		{ mouseX: ctx.mouseX },
    		{ mouseY: ctx.mouseY },
    		{ getShape: getShape },
    		{ computeShapes: computeShapes },
    		{ check: ctx.check },
    		{ getImage: ctx.getImage },
    		{ setDraggedItem: ctx.setDraggedItem },
    		{ getCache: ctx.getCache },
    		{ setDragCount: ctx.setDragCount }
    	];

    	let container3_props = {};
    	for (var i = 0; i < container3_spread_levels.length; i += 1) {
    		container3_props = assign(container3_props, container3_spread_levels[i]);
    	}
    	var container3 = new Container({ props: container3_props });
    	container3.$on("cell_hover", ctx.onCellHover);
    	container3.$on("cell_blur", onCellBlur);

    	var container4_spread_levels = [
    		{ cls: 'identity-container-body identity-container-component-8' },
    		ctx.container,
    		{ label: 't-shirt' },
    		{ isBody: true },
    		{ filter: func_4 },
    		{ drag: ctx.drag },
    		{ dragCount: ctx.dragCount },
    		{ currentContainer: ctx.currentContainer },
    		{ currentItem: ctx.currentItem },
    		{ mouseX: ctx.mouseX },
    		{ mouseY: ctx.mouseY },
    		{ getShape: getShape },
    		{ computeShapes: computeShapes },
    		{ check: ctx.check },
    		{ getImage: ctx.getImage },
    		{ setDraggedItem: ctx.setDraggedItem },
    		{ getCache: ctx.getCache },
    		{ setDragCount: ctx.setDragCount }
    	];

    	let container4_props = {};
    	for (var i = 0; i < container4_spread_levels.length; i += 1) {
    		container4_props = assign(container4_props, container4_spread_levels[i]);
    	}
    	var container4 = new Container({ props: container4_props });
    	container4.$on("cell_hover", ctx.onCellHover);
    	container4.$on("cell_blur", onCellBlur);

    	var container5_spread_levels = [
    		{ cls: 'identity-container-body identity-container-component-9' },
    		ctx.container,
    		{ label: 'pare-balle' },
    		{ isBody: true },
    		{ filter: func_5 },
    		{ drag: ctx.drag },
    		{ dragCount: ctx.dragCount },
    		{ currentContainer: ctx.currentContainer },
    		{ currentItem: ctx.currentItem },
    		{ mouseX: ctx.mouseX },
    		{ mouseY: ctx.mouseY },
    		{ getShape: getShape },
    		{ computeShapes: computeShapes },
    		{ check: ctx.check },
    		{ getImage: ctx.getImage },
    		{ setDraggedItem: ctx.setDraggedItem },
    		{ getCache: ctx.getCache },
    		{ setDragCount: ctx.setDragCount }
    	];

    	let container5_props = {};
    	for (var i = 0; i < container5_spread_levels.length; i += 1) {
    		container5_props = assign(container5_props, container5_spread_levels[i]);
    	}
    	var container5 = new Container({ props: container5_props });
    	container5.$on("cell_hover", ctx.onCellHover);
    	container5.$on("cell_blur", onCellBlur);

    	var container6_spread_levels = [
    		{ cls: 'identity-container-body identity-container-component-11' },
    		ctx.container,
    		{ label: 'torse' },
    		{ isBody: true },
    		{ filter: func_6 },
    		{ drag: ctx.drag },
    		{ dragCount: ctx.dragCount },
    		{ currentContainer: ctx.currentContainer },
    		{ currentItem: ctx.currentItem },
    		{ mouseX: ctx.mouseX },
    		{ mouseY: ctx.mouseY },
    		{ getShape: getShape },
    		{ computeShapes: computeShapes },
    		{ check: ctx.check },
    		{ getImage: ctx.getImage },
    		{ setDraggedItem: ctx.setDraggedItem },
    		{ getCache: ctx.getCache },
    		{ setDragCount: ctx.setDragCount }
    	];

    	let container6_props = {};
    	for (var i = 0; i < container6_spread_levels.length; i += 1) {
    		container6_props = assign(container6_props, container6_spread_levels[i]);
    	}
    	var container6 = new Container({ props: container6_props });
    	container6.$on("cell_hover", ctx.onCellHover);
    	container6.$on("cell_blur", onCellBlur);

    	return {
    		c() {
    			container0.$$.fragment.c();
    			t0 = space();
    			container1.$$.fragment.c();
    			t1 = space();
    			container2.$$.fragment.c();
    			t2 = space();
    			container3.$$.fragment.c();
    			t3 = space();
    			container4.$$.fragment.c();
    			t4 = space();
    			container5.$$.fragment.c();
    			t5 = space();
    			container6.$$.fragment.c();
    		},

    		m(target, anchor) {
    			mount_component(container0, target, anchor);
    			insert(target, t0, anchor);
    			mount_component(container1, target, anchor);
    			insert(target, t1, anchor);
    			mount_component(container2, target, anchor);
    			insert(target, t2, anchor);
    			mount_component(container3, target, anchor);
    			insert(target, t3, anchor);
    			mount_component(container4, target, anchor);
    			insert(target, t4, anchor);
    			mount_component(container5, target, anchor);
    			insert(target, t5, anchor);
    			mount_component(container6, target, anchor);
    			current = true;
    		},

    		p(changed, ctx) {
    			var container0_changes = (changed._identityContainers || changed.drag || changed.dragCount || changed.currentContainer || changed.currentItem || changed.mouseX || changed.mouseY || changed.getShape || changed.computeShapes || changed.check || changed.getImage || changed.setDraggedItem || changed.getCache || changed.setDragCount) ? get_spread_update(container0_spread_levels, [
    									container0_spread_levels[0],
    			(changed._identityContainers) && get_spread_object(ctx.container),
    			container0_spread_levels[2],
    			container0_spread_levels[3],
    			container0_spread_levels[4],
    			(changed.drag) && { drag: ctx.drag },
    			(changed.dragCount) && { dragCount: ctx.dragCount },
    			(changed.currentContainer) && { currentContainer: ctx.currentContainer },
    			(changed.currentItem) && { currentItem: ctx.currentItem },
    			(changed.mouseX) && { mouseX: ctx.mouseX },
    			(changed.mouseY) && { mouseY: ctx.mouseY },
    			(changed.getShape) && { getShape: getShape },
    			(changed.computeShapes) && { computeShapes: computeShapes },
    			(changed.check) && { check: ctx.check },
    			(changed.getImage) && { getImage: ctx.getImage },
    			(changed.setDraggedItem) && { setDraggedItem: ctx.setDraggedItem },
    			(changed.getCache) && { getCache: ctx.getCache },
    			(changed.setDragCount) && { setDragCount: ctx.setDragCount }
    								]) : {};
    			container0.$set(container0_changes);

    			var container1_changes = (changed._identityContainers || changed.drag || changed.dragCount || changed.currentContainer || changed.currentItem || changed.mouseX || changed.mouseY || changed.getShape || changed.computeShapes || changed.check || changed.getImage || changed.setDraggedItem || changed.getCache || changed.setDragCount) ? get_spread_update(container1_spread_levels, [
    									container1_spread_levels[0],
    			(changed._identityContainers) && get_spread_object(ctx.container),
    			container1_spread_levels[2],
    			container1_spread_levels[3],
    			container1_spread_levels[4],
    			(changed.drag) && { drag: ctx.drag },
    			(changed.dragCount) && { dragCount: ctx.dragCount },
    			(changed.currentContainer) && { currentContainer: ctx.currentContainer },
    			(changed.currentItem) && { currentItem: ctx.currentItem },
    			(changed.mouseX) && { mouseX: ctx.mouseX },
    			(changed.mouseY) && { mouseY: ctx.mouseY },
    			(changed.getShape) && { getShape: getShape },
    			(changed.computeShapes) && { computeShapes: computeShapes },
    			(changed.check) && { check: ctx.check },
    			(changed.getImage) && { getImage: ctx.getImage },
    			(changed.setDraggedItem) && { setDraggedItem: ctx.setDraggedItem },
    			(changed.getCache) && { getCache: ctx.getCache },
    			(changed.setDragCount) && { setDragCount: ctx.setDragCount }
    								]) : {};
    			container1.$set(container1_changes);

    			var container2_changes = (changed._identityContainers || changed.drag || changed.dragCount || changed.currentContainer || changed.currentItem || changed.mouseX || changed.mouseY || changed.getShape || changed.computeShapes || changed.check || changed.getImage || changed.setDraggedItem || changed.getCache || changed.setDragCount) ? get_spread_update(container2_spread_levels, [
    									container2_spread_levels[0],
    			(changed._identityContainers) && get_spread_object(ctx.container),
    			container2_spread_levels[2],
    			container2_spread_levels[3],
    			container2_spread_levels[4],
    			(changed.drag) && { drag: ctx.drag },
    			(changed.dragCount) && { dragCount: ctx.dragCount },
    			(changed.currentContainer) && { currentContainer: ctx.currentContainer },
    			(changed.currentItem) && { currentItem: ctx.currentItem },
    			(changed.mouseX) && { mouseX: ctx.mouseX },
    			(changed.mouseY) && { mouseY: ctx.mouseY },
    			(changed.getShape) && { getShape: getShape },
    			(changed.computeShapes) && { computeShapes: computeShapes },
    			(changed.check) && { check: ctx.check },
    			(changed.getImage) && { getImage: ctx.getImage },
    			(changed.setDraggedItem) && { setDraggedItem: ctx.setDraggedItem },
    			(changed.getCache) && { getCache: ctx.getCache },
    			(changed.setDragCount) && { setDragCount: ctx.setDragCount }
    								]) : {};
    			container2.$set(container2_changes);

    			var container3_changes = (changed._identityContainers || changed.drag || changed.dragCount || changed.currentContainer || changed.currentItem || changed.mouseX || changed.mouseY || changed.getShape || changed.computeShapes || changed.check || changed.getImage || changed.setDraggedItem || changed.getCache || changed.setDragCount) ? get_spread_update(container3_spread_levels, [
    									container3_spread_levels[0],
    			(changed._identityContainers) && get_spread_object(ctx.container),
    			container3_spread_levels[2],
    			container3_spread_levels[3],
    			container3_spread_levels[4],
    			(changed.drag) && { drag: ctx.drag },
    			(changed.dragCount) && { dragCount: ctx.dragCount },
    			(changed.currentContainer) && { currentContainer: ctx.currentContainer },
    			(changed.currentItem) && { currentItem: ctx.currentItem },
    			(changed.mouseX) && { mouseX: ctx.mouseX },
    			(changed.mouseY) && { mouseY: ctx.mouseY },
    			(changed.getShape) && { getShape: getShape },
    			(changed.computeShapes) && { computeShapes: computeShapes },
    			(changed.check) && { check: ctx.check },
    			(changed.getImage) && { getImage: ctx.getImage },
    			(changed.setDraggedItem) && { setDraggedItem: ctx.setDraggedItem },
    			(changed.getCache) && { getCache: ctx.getCache },
    			(changed.setDragCount) && { setDragCount: ctx.setDragCount }
    								]) : {};
    			container3.$set(container3_changes);

    			var container4_changes = (changed._identityContainers || changed.drag || changed.dragCount || changed.currentContainer || changed.currentItem || changed.mouseX || changed.mouseY || changed.getShape || changed.computeShapes || changed.check || changed.getImage || changed.setDraggedItem || changed.getCache || changed.setDragCount) ? get_spread_update(container4_spread_levels, [
    									container4_spread_levels[0],
    			(changed._identityContainers) && get_spread_object(ctx.container),
    			container4_spread_levels[2],
    			container4_spread_levels[3],
    			container4_spread_levels[4],
    			(changed.drag) && { drag: ctx.drag },
    			(changed.dragCount) && { dragCount: ctx.dragCount },
    			(changed.currentContainer) && { currentContainer: ctx.currentContainer },
    			(changed.currentItem) && { currentItem: ctx.currentItem },
    			(changed.mouseX) && { mouseX: ctx.mouseX },
    			(changed.mouseY) && { mouseY: ctx.mouseY },
    			(changed.getShape) && { getShape: getShape },
    			(changed.computeShapes) && { computeShapes: computeShapes },
    			(changed.check) && { check: ctx.check },
    			(changed.getImage) && { getImage: ctx.getImage },
    			(changed.setDraggedItem) && { setDraggedItem: ctx.setDraggedItem },
    			(changed.getCache) && { getCache: ctx.getCache },
    			(changed.setDragCount) && { setDragCount: ctx.setDragCount }
    								]) : {};
    			container4.$set(container4_changes);

    			var container5_changes = (changed._identityContainers || changed.drag || changed.dragCount || changed.currentContainer || changed.currentItem || changed.mouseX || changed.mouseY || changed.getShape || changed.computeShapes || changed.check || changed.getImage || changed.setDraggedItem || changed.getCache || changed.setDragCount) ? get_spread_update(container5_spread_levels, [
    									container5_spread_levels[0],
    			(changed._identityContainers) && get_spread_object(ctx.container),
    			container5_spread_levels[2],
    			container5_spread_levels[3],
    			container5_spread_levels[4],
    			(changed.drag) && { drag: ctx.drag },
    			(changed.dragCount) && { dragCount: ctx.dragCount },
    			(changed.currentContainer) && { currentContainer: ctx.currentContainer },
    			(changed.currentItem) && { currentItem: ctx.currentItem },
    			(changed.mouseX) && { mouseX: ctx.mouseX },
    			(changed.mouseY) && { mouseY: ctx.mouseY },
    			(changed.getShape) && { getShape: getShape },
    			(changed.computeShapes) && { computeShapes: computeShapes },
    			(changed.check) && { check: ctx.check },
    			(changed.getImage) && { getImage: ctx.getImage },
    			(changed.setDraggedItem) && { setDraggedItem: ctx.setDraggedItem },
    			(changed.getCache) && { getCache: ctx.getCache },
    			(changed.setDragCount) && { setDragCount: ctx.setDragCount }
    								]) : {};
    			container5.$set(container5_changes);

    			var container6_changes = (changed._identityContainers || changed.drag || changed.dragCount || changed.currentContainer || changed.currentItem || changed.mouseX || changed.mouseY || changed.getShape || changed.computeShapes || changed.check || changed.getImage || changed.setDraggedItem || changed.getCache || changed.setDragCount) ? get_spread_update(container6_spread_levels, [
    									container6_spread_levels[0],
    			(changed._identityContainers) && get_spread_object(ctx.container),
    			container6_spread_levels[2],
    			container6_spread_levels[3],
    			container6_spread_levels[4],
    			(changed.drag) && { drag: ctx.drag },
    			(changed.dragCount) && { dragCount: ctx.dragCount },
    			(changed.currentContainer) && { currentContainer: ctx.currentContainer },
    			(changed.currentItem) && { currentItem: ctx.currentItem },
    			(changed.mouseX) && { mouseX: ctx.mouseX },
    			(changed.mouseY) && { mouseY: ctx.mouseY },
    			(changed.getShape) && { getShape: getShape },
    			(changed.computeShapes) && { computeShapes: computeShapes },
    			(changed.check) && { check: ctx.check },
    			(changed.getImage) && { getImage: ctx.getImage },
    			(changed.setDraggedItem) && { setDraggedItem: ctx.setDraggedItem },
    			(changed.getCache) && { getCache: ctx.getCache },
    			(changed.setDragCount) && { setDragCount: ctx.setDragCount }
    								]) : {};
    			container6.$set(container6_changes);
    		},

    		i(local) {
    			if (current) return;
    			transition_in(container0.$$.fragment, local);

    			transition_in(container1.$$.fragment, local);

    			transition_in(container2.$$.fragment, local);

    			transition_in(container3.$$.fragment, local);

    			transition_in(container4.$$.fragment, local);

    			transition_in(container5.$$.fragment, local);

    			transition_in(container6.$$.fragment, local);

    			current = true;
    		},

    		o(local) {
    			transition_out(container0.$$.fragment, local);
    			transition_out(container1.$$.fragment, local);
    			transition_out(container2.$$.fragment, local);
    			transition_out(container3.$$.fragment, local);
    			transition_out(container4.$$.fragment, local);
    			transition_out(container5.$$.fragment, local);
    			transition_out(container6.$$.fragment, local);
    			current = false;
    		},

    		d(detaching) {
    			destroy_component(container0, detaching);

    			if (detaching) {
    				detach(t0);
    			}

    			destroy_component(container1, detaching);

    			if (detaching) {
    				detach(t1);
    			}

    			destroy_component(container2, detaching);

    			if (detaching) {
    				detach(t2);
    			}

    			destroy_component(container3, detaching);

    			if (detaching) {
    				detach(t3);
    			}

    			destroy_component(container4, detaching);

    			if (detaching) {
    				detach(t4);
    			}

    			destroy_component(container5, detaching);

    			if (detaching) {
    				detach(t5);
    			}

    			destroy_component(container6, detaching);
    		}
    	};
    }

    // (855:3) {#if container._type === 'IdentityPropContainer'}
    function create_if_block$2(ctx) {
    	var t0, t1, t2, t3, current;

    	var container0_spread_levels = [
    		{ cls: 'identity-container-body identity-container-prop-0' },
    		ctx.container,
    		{ label: 'casque' },
    		{ isBody: true },
    		{ filter: func_7 },
    		{ drag: ctx.drag },
    		{ dragCount: ctx.dragCount },
    		{ currentContainer: ctx.currentContainer },
    		{ currentItem: ctx.currentItem },
    		{ mouseX: ctx.mouseX },
    		{ mouseY: ctx.mouseY },
    		{ getShape: getShape },
    		{ computeShapes: computeShapes },
    		{ check: ctx.check },
    		{ getImage: ctx.getImage },
    		{ setDraggedItem: ctx.setDraggedItem },
    		{ getCache: ctx.getCache },
    		{ setDragCount: ctx.setDragCount }
    	];

    	let container0_props = {};
    	for (var i = 0; i < container0_spread_levels.length; i += 1) {
    		container0_props = assign(container0_props, container0_spread_levels[i]);
    	}
    	var container0 = new Container({ props: container0_props });
    	container0.$on("cell_hover", ctx.onCellHover);
    	container0.$on("cell_blur", onCellBlur);

    	var container1_spread_levels = [
    		{ cls: 'identity-container-body identity-container-prop-1' },
    		ctx.container,
    		{ label: 'lunettes' },
    		{ isBody: true },
    		{ filter: func_8 },
    		{ drag: ctx.drag },
    		{ dragCount: ctx.dragCount },
    		{ currentContainer: ctx.currentContainer },
    		{ currentItem: ctx.currentItem },
    		{ mouseX: ctx.mouseX },
    		{ mouseY: ctx.mouseY },
    		{ getShape: getShape },
    		{ computeShapes: computeShapes },
    		{ check: ctx.check },
    		{ getImage: ctx.getImage },
    		{ setDraggedItem: ctx.setDraggedItem },
    		{ getCache: ctx.getCache },
    		{ setDragCount: ctx.setDragCount }
    	];

    	let container1_props = {};
    	for (var i = 0; i < container1_spread_levels.length; i += 1) {
    		container1_props = assign(container1_props, container1_spread_levels[i]);
    	}
    	var container1 = new Container({ props: container1_props });
    	container1.$on("cell_hover", ctx.onCellHover);
    	container1.$on("cell_blur", onCellBlur);

    	var container2_spread_levels = [
    		{ cls: 'identity-container-body identity-container-prop-2' },
    		ctx.container,
    		{ label: 'oreilles' },
    		{ isBody: true },
    		{ filter: func_9 },
    		{ drag: ctx.drag },
    		{ dragCount: ctx.dragCount },
    		{ currentContainer: ctx.currentContainer },
    		{ currentItem: ctx.currentItem },
    		{ mouseX: ctx.mouseX },
    		{ mouseY: ctx.mouseY },
    		{ getShape: getShape },
    		{ computeShapes: computeShapes },
    		{ check: ctx.check },
    		{ getImage: ctx.getImage },
    		{ setDraggedItem: ctx.setDraggedItem },
    		{ getCache: ctx.getCache },
    		{ setDragCount: ctx.setDragCount }
    	];

    	let container2_props = {};
    	for (var i = 0; i < container2_spread_levels.length; i += 1) {
    		container2_props = assign(container2_props, container2_spread_levels[i]);
    	}
    	var container2 = new Container({ props: container2_props });
    	container2.$on("cell_hover", ctx.onCellHover);
    	container2.$on("cell_blur", onCellBlur);

    	var container3_spread_levels = [
    		{ cls: 'identity-container-body identity-container-prop-6' },
    		ctx.container,
    		{ label: 'montre' },
    		{ isBody: true },
    		{ filter: func_10 },
    		{ drag: ctx.drag },
    		{ dragCount: ctx.dragCount },
    		{ currentContainer: ctx.currentContainer },
    		{ currentItem: ctx.currentItem },
    		{ mouseX: ctx.mouseX },
    		{ mouseY: ctx.mouseY },
    		{ getShape: getShape },
    		{ computeShapes: computeShapes },
    		{ check: ctx.check },
    		{ getImage: ctx.getImage },
    		{ setDraggedItem: ctx.setDraggedItem },
    		{ getCache: ctx.getCache },
    		{ setDragCount: ctx.setDragCount }
    	];

    	let container3_props = {};
    	for (var i = 0; i < container3_spread_levels.length; i += 1) {
    		container3_props = assign(container3_props, container3_spread_levels[i]);
    	}
    	var container3 = new Container({ props: container3_props });
    	container3.$on("cell_hover", ctx.onCellHover);
    	container3.$on("cell_blur", onCellBlur);

    	var container4_spread_levels = [
    		{ cls: 'identity-container-body identity-container-prop-7' },
    		ctx.container,
    		{ label: 'bracelet' },
    		{ isBody: true },
    		{ filter: func_11 },
    		{ drag: ctx.drag },
    		{ dragCount: ctx.dragCount },
    		{ currentContainer: ctx.currentContainer },
    		{ currentItem: ctx.currentItem },
    		{ mouseX: ctx.mouseX },
    		{ mouseY: ctx.mouseY },
    		{ getShape: getShape },
    		{ computeShapes: computeShapes },
    		{ check: ctx.check },
    		{ getImage: ctx.getImage },
    		{ setDraggedItem: ctx.setDraggedItem },
    		{ getCache: ctx.getCache },
    		{ setDragCount: ctx.setDragCount }
    	];

    	let container4_props = {};
    	for (var i = 0; i < container4_spread_levels.length; i += 1) {
    		container4_props = assign(container4_props, container4_spread_levels[i]);
    	}
    	var container4 = new Container({ props: container4_props });
    	container4.$on("cell_hover", ctx.onCellHover);
    	container4.$on("cell_blur", onCellBlur);

    	return {
    		c() {
    			container0.$$.fragment.c();
    			t0 = space();
    			container1.$$.fragment.c();
    			t1 = space();
    			container2.$$.fragment.c();
    			t2 = space();
    			container3.$$.fragment.c();
    			t3 = space();
    			container4.$$.fragment.c();
    		},

    		m(target, anchor) {
    			mount_component(container0, target, anchor);
    			insert(target, t0, anchor);
    			mount_component(container1, target, anchor);
    			insert(target, t1, anchor);
    			mount_component(container2, target, anchor);
    			insert(target, t2, anchor);
    			mount_component(container3, target, anchor);
    			insert(target, t3, anchor);
    			mount_component(container4, target, anchor);
    			current = true;
    		},

    		p(changed, ctx) {
    			var container0_changes = (changed._identityContainers || changed.drag || changed.dragCount || changed.currentContainer || changed.currentItem || changed.mouseX || changed.mouseY || changed.getShape || changed.computeShapes || changed.check || changed.getImage || changed.setDraggedItem || changed.getCache || changed.setDragCount) ? get_spread_update(container0_spread_levels, [
    									container0_spread_levels[0],
    			(changed._identityContainers) && get_spread_object(ctx.container),
    			container0_spread_levels[2],
    			container0_spread_levels[3],
    			container0_spread_levels[4],
    			(changed.drag) && { drag: ctx.drag },
    			(changed.dragCount) && { dragCount: ctx.dragCount },
    			(changed.currentContainer) && { currentContainer: ctx.currentContainer },
    			(changed.currentItem) && { currentItem: ctx.currentItem },
    			(changed.mouseX) && { mouseX: ctx.mouseX },
    			(changed.mouseY) && { mouseY: ctx.mouseY },
    			(changed.getShape) && { getShape: getShape },
    			(changed.computeShapes) && { computeShapes: computeShapes },
    			(changed.check) && { check: ctx.check },
    			(changed.getImage) && { getImage: ctx.getImage },
    			(changed.setDraggedItem) && { setDraggedItem: ctx.setDraggedItem },
    			(changed.getCache) && { getCache: ctx.getCache },
    			(changed.setDragCount) && { setDragCount: ctx.setDragCount }
    								]) : {};
    			container0.$set(container0_changes);

    			var container1_changes = (changed._identityContainers || changed.drag || changed.dragCount || changed.currentContainer || changed.currentItem || changed.mouseX || changed.mouseY || changed.getShape || changed.computeShapes || changed.check || changed.getImage || changed.setDraggedItem || changed.getCache || changed.setDragCount) ? get_spread_update(container1_spread_levels, [
    									container1_spread_levels[0],
    			(changed._identityContainers) && get_spread_object(ctx.container),
    			container1_spread_levels[2],
    			container1_spread_levels[3],
    			container1_spread_levels[4],
    			(changed.drag) && { drag: ctx.drag },
    			(changed.dragCount) && { dragCount: ctx.dragCount },
    			(changed.currentContainer) && { currentContainer: ctx.currentContainer },
    			(changed.currentItem) && { currentItem: ctx.currentItem },
    			(changed.mouseX) && { mouseX: ctx.mouseX },
    			(changed.mouseY) && { mouseY: ctx.mouseY },
    			(changed.getShape) && { getShape: getShape },
    			(changed.computeShapes) && { computeShapes: computeShapes },
    			(changed.check) && { check: ctx.check },
    			(changed.getImage) && { getImage: ctx.getImage },
    			(changed.setDraggedItem) && { setDraggedItem: ctx.setDraggedItem },
    			(changed.getCache) && { getCache: ctx.getCache },
    			(changed.setDragCount) && { setDragCount: ctx.setDragCount }
    								]) : {};
    			container1.$set(container1_changes);

    			var container2_changes = (changed._identityContainers || changed.drag || changed.dragCount || changed.currentContainer || changed.currentItem || changed.mouseX || changed.mouseY || changed.getShape || changed.computeShapes || changed.check || changed.getImage || changed.setDraggedItem || changed.getCache || changed.setDragCount) ? get_spread_update(container2_spread_levels, [
    									container2_spread_levels[0],
    			(changed._identityContainers) && get_spread_object(ctx.container),
    			container2_spread_levels[2],
    			container2_spread_levels[3],
    			container2_spread_levels[4],
    			(changed.drag) && { drag: ctx.drag },
    			(changed.dragCount) && { dragCount: ctx.dragCount },
    			(changed.currentContainer) && { currentContainer: ctx.currentContainer },
    			(changed.currentItem) && { currentItem: ctx.currentItem },
    			(changed.mouseX) && { mouseX: ctx.mouseX },
    			(changed.mouseY) && { mouseY: ctx.mouseY },
    			(changed.getShape) && { getShape: getShape },
    			(changed.computeShapes) && { computeShapes: computeShapes },
    			(changed.check) && { check: ctx.check },
    			(changed.getImage) && { getImage: ctx.getImage },
    			(changed.setDraggedItem) && { setDraggedItem: ctx.setDraggedItem },
    			(changed.getCache) && { getCache: ctx.getCache },
    			(changed.setDragCount) && { setDragCount: ctx.setDragCount }
    								]) : {};
    			container2.$set(container2_changes);

    			var container3_changes = (changed._identityContainers || changed.drag || changed.dragCount || changed.currentContainer || changed.currentItem || changed.mouseX || changed.mouseY || changed.getShape || changed.computeShapes || changed.check || changed.getImage || changed.setDraggedItem || changed.getCache || changed.setDragCount) ? get_spread_update(container3_spread_levels, [
    									container3_spread_levels[0],
    			(changed._identityContainers) && get_spread_object(ctx.container),
    			container3_spread_levels[2],
    			container3_spread_levels[3],
    			container3_spread_levels[4],
    			(changed.drag) && { drag: ctx.drag },
    			(changed.dragCount) && { dragCount: ctx.dragCount },
    			(changed.currentContainer) && { currentContainer: ctx.currentContainer },
    			(changed.currentItem) && { currentItem: ctx.currentItem },
    			(changed.mouseX) && { mouseX: ctx.mouseX },
    			(changed.mouseY) && { mouseY: ctx.mouseY },
    			(changed.getShape) && { getShape: getShape },
    			(changed.computeShapes) && { computeShapes: computeShapes },
    			(changed.check) && { check: ctx.check },
    			(changed.getImage) && { getImage: ctx.getImage },
    			(changed.setDraggedItem) && { setDraggedItem: ctx.setDraggedItem },
    			(changed.getCache) && { getCache: ctx.getCache },
    			(changed.setDragCount) && { setDragCount: ctx.setDragCount }
    								]) : {};
    			container3.$set(container3_changes);

    			var container4_changes = (changed._identityContainers || changed.drag || changed.dragCount || changed.currentContainer || changed.currentItem || changed.mouseX || changed.mouseY || changed.getShape || changed.computeShapes || changed.check || changed.getImage || changed.setDraggedItem || changed.getCache || changed.setDragCount) ? get_spread_update(container4_spread_levels, [
    									container4_spread_levels[0],
    			(changed._identityContainers) && get_spread_object(ctx.container),
    			container4_spread_levels[2],
    			container4_spread_levels[3],
    			container4_spread_levels[4],
    			(changed.drag) && { drag: ctx.drag },
    			(changed.dragCount) && { dragCount: ctx.dragCount },
    			(changed.currentContainer) && { currentContainer: ctx.currentContainer },
    			(changed.currentItem) && { currentItem: ctx.currentItem },
    			(changed.mouseX) && { mouseX: ctx.mouseX },
    			(changed.mouseY) && { mouseY: ctx.mouseY },
    			(changed.getShape) && { getShape: getShape },
    			(changed.computeShapes) && { computeShapes: computeShapes },
    			(changed.check) && { check: ctx.check },
    			(changed.getImage) && { getImage: ctx.getImage },
    			(changed.setDraggedItem) && { setDraggedItem: ctx.setDraggedItem },
    			(changed.getCache) && { getCache: ctx.getCache },
    			(changed.setDragCount) && { setDragCount: ctx.setDragCount }
    								]) : {};
    			container4.$set(container4_changes);
    		},

    		i(local) {
    			if (current) return;
    			transition_in(container0.$$.fragment, local);

    			transition_in(container1.$$.fragment, local);

    			transition_in(container2.$$.fragment, local);

    			transition_in(container3.$$.fragment, local);

    			transition_in(container4.$$.fragment, local);

    			current = true;
    		},

    		o(local) {
    			transition_out(container0.$$.fragment, local);
    			transition_out(container1.$$.fragment, local);
    			transition_out(container2.$$.fragment, local);
    			transition_out(container3.$$.fragment, local);
    			transition_out(container4.$$.fragment, local);
    			current = false;
    		},

    		d(detaching) {
    			destroy_component(container0, detaching);

    			if (detaching) {
    				detach(t0);
    			}

    			destroy_component(container1, detaching);

    			if (detaching) {
    				detach(t1);
    			}

    			destroy_component(container2, detaching);

    			if (detaching) {
    				detach(t2);
    			}

    			destroy_component(container3, detaching);

    			if (detaching) {
    				detach(t3);
    			}

    			destroy_component(container4, detaching);
    		}
    	};
    }

    // (845:2) {#each _identityContainers as container}
    function create_each_block_1$2(ctx) {
    	var t, if_block1_anchor, current;

    	var if_block0 = (ctx.container._type === 'IdentityComponentContainer') && create_if_block_1$2(ctx);

    	var if_block1 = (ctx.container._type === 'IdentityPropContainer') && create_if_block$2(ctx);

    	return {
    		c() {
    			if (if_block0) if_block0.c();
    			t = space();
    			if (if_block1) if_block1.c();
    			if_block1_anchor = empty();
    		},

    		m(target, anchor) {
    			if (if_block0) if_block0.m(target, anchor);
    			insert(target, t, anchor);
    			if (if_block1) if_block1.m(target, anchor);
    			insert(target, if_block1_anchor, anchor);
    			current = true;
    		},

    		p(changed, ctx) {
    			if (ctx.container._type === 'IdentityComponentContainer') {
    				if (if_block0) {
    					if_block0.p(changed, ctx);
    					transition_in(if_block0, 1);
    				} else {
    					if_block0 = create_if_block_1$2(ctx);
    					if_block0.c();
    					transition_in(if_block0, 1);
    					if_block0.m(t.parentNode, t);
    				}
    			} else if (if_block0) {
    				group_outros();
    				transition_out(if_block0, 1, 1, () => {
    					if_block0 = null;
    				});
    				check_outros();
    			}

    			if (ctx.container._type === 'IdentityPropContainer') {
    				if (if_block1) {
    					if_block1.p(changed, ctx);
    					transition_in(if_block1, 1);
    				} else {
    					if_block1 = create_if_block$2(ctx);
    					if_block1.c();
    					transition_in(if_block1, 1);
    					if_block1.m(if_block1_anchor.parentNode, if_block1_anchor);
    				}
    			} else if (if_block1) {
    				group_outros();
    				transition_out(if_block1, 1, 1, () => {
    					if_block1 = null;
    				});
    				check_outros();
    			}
    		},

    		i(local) {
    			if (current) return;
    			transition_in(if_block0);
    			transition_in(if_block1);
    			current = true;
    		},

    		o(local) {
    			transition_out(if_block0);
    			transition_out(if_block1);
    			current = false;
    		},

    		d(detaching) {
    			if (if_block0) if_block0.d(detaching);

    			if (detaching) {
    				detach(t);
    			}

    			if (if_block1) if_block1.d(detaching);

    			if (detaching) {
    				detach(if_block1_anchor);
    			}
    		}
    	};
    }

    // (867:2) {#each _regularContainers as container}
    function create_each_block$2(ctx) {
    	var current;

    	var container_spread_levels = [
    		{ playerRoles: ctx.playerRoles },
    		ctx.container,
    		{ drag: ctx.drag },
    		{ dragCount: ctx.dragCount },
    		{ currentContainer: ctx.currentContainer },
    		{ currentItem: ctx.currentItem },
    		{ mouseX: ctx.mouseX },
    		{ mouseY: ctx.mouseY },
    		{ getShape: getShape },
    		{ computeShapes: computeShapes },
    		{ check: ctx.check },
    		{ getImage: ctx.getImage },
    		{ setDraggedItem: ctx.setDraggedItem },
    		{ getCache: ctx.getCache },
    		{ setDragCount: ctx.setDragCount }
    	];

    	let container_props = {};
    	for (var i = 0; i < container_spread_levels.length; i += 1) {
    		container_props = assign(container_props, container_spread_levels[i]);
    	}
    	var container = new Container({ props: container_props });
    	container.$on("cell_hover", ctx.onCellHover);
    	container.$on("cell_blur", onCellBlur);

    	return {
    		c() {
    			container.$$.fragment.c();
    		},

    		m(target, anchor) {
    			mount_component(container, target, anchor);
    			current = true;
    		},

    		p(changed, ctx) {
    			var container_changes = (changed.playerRoles || changed._regularContainers || changed.drag || changed.dragCount || changed.currentContainer || changed.currentItem || changed.mouseX || changed.mouseY || changed.getShape || changed.computeShapes || changed.check || changed.getImage || changed.setDraggedItem || changed.getCache || changed.setDragCount) ? get_spread_update(container_spread_levels, [
    									(changed.playerRoles) && { playerRoles: ctx.playerRoles },
    			(changed._regularContainers) && get_spread_object(ctx.container),
    			(changed.drag) && { drag: ctx.drag },
    			(changed.dragCount) && { dragCount: ctx.dragCount },
    			(changed.currentContainer) && { currentContainer: ctx.currentContainer },
    			(changed.currentItem) && { currentItem: ctx.currentItem },
    			(changed.mouseX) && { mouseX: ctx.mouseX },
    			(changed.mouseY) && { mouseY: ctx.mouseY },
    			(changed.getShape) && { getShape: getShape },
    			(changed.computeShapes) && { computeShapes: computeShapes },
    			(changed.check) && { check: ctx.check },
    			(changed.getImage) && { getImage: ctx.getImage },
    			(changed.setDraggedItem) && { setDraggedItem: ctx.setDraggedItem },
    			(changed.getCache) && { getCache: ctx.getCache },
    			(changed.setDragCount) && { setDragCount: ctx.setDragCount }
    								]) : {};
    			container.$set(container_changes);
    		},

    		i(local) {
    			if (current) return;
    			transition_in(container.$$.fragment, local);

    			current = true;
    		},

    		o(local) {
    			transition_out(container.$$.fragment, local);
    			current = false;
    		},

    		d(detaching) {
    			destroy_component(container, detaching);
    		}
    	};
    }

    function create_fragment$3(ctx) {
    	var div4, div0, t0, div1, t1, div3, div2, current, dispose;

    	let each_value_1 = ctx._identityContainers;

    	let each_blocks_1 = [];

    	for (let i = 0; i < each_value_1.length; i += 1) {
    		each_blocks_1[i] = create_each_block_1$2(get_each_context_1$2(ctx, each_value_1, i));
    	}

    	const out = i => transition_out(each_blocks_1[i], 1, 1, () => {
    		each_blocks_1[i] = null;
    	});

    	let each_value = ctx._regularContainers;

    	let each_blocks = [];

    	for (let i = 0; i < each_value.length; i += 1) {
    		each_blocks[i] = create_each_block$2(get_each_context$2(ctx, each_value, i));
    	}

    	const out_1 = i => transition_out(each_blocks[i], 1, 1, () => {
    		each_blocks[i] = null;
    	});

    	return {
    		c() {
    			div4 = element("div");
    			div0 = element("div");

    			for (let i = 0; i < each_blocks_1.length; i += 1) {
    				each_blocks_1[i].c();
    			}

    			t0 = space();
    			div1 = element("div");

    			for (let i = 0; i < each_blocks.length; i += 1) {
    				each_blocks[i].c();
    			}

    			t1 = space();
    			div3 = element("div");
    			div2 = element("div");
    			div2.textContent = "X";
    			attr(div0, "class", "identity-body svelte-8ndr49");
    			attr(div1, "class", "inventory scrollable svelte-8ndr49");
    			set_style(div1, "width", "" + ctx.Math.floor(ctx._regularContainers.map(func_12).reduce(func_13, 0) * GRID_SIZE$3) + "px");
    			attr(div2, "class", "inventory-action close svelte-8ndr49");
    			attr(div3, "class", "inventory-actions svelte-8ndr49");
    			attr(div4, "class", "inventory-wrap svelte-8ndr49");

    			dispose = [
    				listen(window_1, "message", ctx.message_handler),
    				listen(window_1, "mouseup", ctx.onMouseUp),
    				listen(div2, "click", ctx.click_handler)
    			];
    		},

    		m(target, anchor) {
    			insert(target, div4, anchor);
    			append(div4, div0);

    			for (let i = 0; i < each_blocks_1.length; i += 1) {
    				each_blocks_1[i].m(div0, null);
    			}

    			append(div4, t0);
    			append(div4, div1);

    			for (let i = 0; i < each_blocks.length; i += 1) {
    				each_blocks[i].m(div1, null);
    			}

    			append(div4, t1);
    			append(div4, div3);
    			append(div3, div2);
    			current = true;
    		},

    		p(changed, ctx) {
    			if (changed._identityContainers || changed.drag || changed.dragCount || changed.currentContainer || changed.currentItem || changed.mouseX || changed.mouseY || changed.getShape || changed.computeShapes || changed.check || changed.getImage || changed.setDraggedItem || changed.getCache || changed.setDragCount) {
    				each_value_1 = ctx._identityContainers;

    				let i;
    				for (i = 0; i < each_value_1.length; i += 1) {
    					const child_ctx = get_each_context_1$2(ctx, each_value_1, i);

    					if (each_blocks_1[i]) {
    						each_blocks_1[i].p(changed, child_ctx);
    						transition_in(each_blocks_1[i], 1);
    					} else {
    						each_blocks_1[i] = create_each_block_1$2(child_ctx);
    						each_blocks_1[i].c();
    						transition_in(each_blocks_1[i], 1);
    						each_blocks_1[i].m(div0, null);
    					}
    				}

    				group_outros();
    				for (i = each_value_1.length; i < each_blocks_1.length; i += 1) {
    					out(i);
    				}
    				check_outros();
    			}

    			if (changed.playerRoles || changed._regularContainers || changed.drag || changed.dragCount || changed.currentContainer || changed.currentItem || changed.mouseX || changed.mouseY || changed.getShape || changed.computeShapes || changed.check || changed.getImage || changed.setDraggedItem || changed.getCache || changed.setDragCount) {
    				each_value = ctx._regularContainers;

    				let i;
    				for (i = 0; i < each_value.length; i += 1) {
    					const child_ctx = get_each_context$2(ctx, each_value, i);

    					if (each_blocks[i]) {
    						each_blocks[i].p(changed, child_ctx);
    						transition_in(each_blocks[i], 1);
    					} else {
    						each_blocks[i] = create_each_block$2(child_ctx);
    						each_blocks[i].c();
    						transition_in(each_blocks[i], 1);
    						each_blocks[i].m(div1, null);
    					}
    				}

    				group_outros();
    				for (i = each_value.length; i < each_blocks.length; i += 1) {
    					out_1(i);
    				}
    				check_outros();
    			}

    			if (!current || changed._regularContainers) {
    				set_style(div1, "width", "" + ctx.Math.floor(ctx._regularContainers.map(func_12).reduce(func_13, 0) * GRID_SIZE$3) + "px");
    			}
    		},

    		i(local) {
    			if (current) return;
    			for (let i = 0; i < each_value_1.length; i += 1) {
    				transition_in(each_blocks_1[i]);
    			}

    			for (let i = 0; i < each_value.length; i += 1) {
    				transition_in(each_blocks[i]);
    			}

    			current = true;
    		},

    		o(local) {
    			each_blocks_1 = each_blocks_1.filter(Boolean);
    			for (let i = 0; i < each_blocks_1.length; i += 1) {
    				transition_out(each_blocks_1[i]);
    			}

    			each_blocks = each_blocks.filter(Boolean);
    			for (let i = 0; i < each_blocks.length; i += 1) {
    				transition_out(each_blocks[i]);
    			}

    			current = false;
    		},

    		d(detaching) {
    			if (detaching) {
    				detach(div4);
    			}

    			destroy_each(each_blocks_1, detaching);

    			destroy_each(each_blocks, detaching);

    			run_all(dispose);
    		}
    	};
    }

    const GRID_SIZE$3       = 100;

    function getShape(_shape) {

        const shape  = [];
    	const bb     = {x: Infinity, y: Infinity};
    	const offset = {x: 0, y: 0};

        _shape.grid.forEach(e => {

          if(e.x < bb.x)
            bb.x = e.x;

          if(e.y < bb.y)
            bb.y = e.y;

        });

        if(bb.x < 0)
          offset.x = Math.abs(bb.x);

        if(bb.y < 0)
          offset.y = Math.abs(bb.y);

        bb.x += offset.x;
        bb.y += offset.y;

        _shape.grid.forEach(e => {

          const x  = e.x + offset.x;
          const y  = e.y + offset.y;
          const gX = Math.floor(x / GRID_SIZE$3);
          const gY = Math.floor(y / GRID_SIZE$3);

          shape[gY]     = shape[gY] || [];
          shape[gY][gX] = true;

          let xMax = 0;

          shape.forEach(y => y.forEach((x, ix) => {
            if(ix > xMax)
              xMax = ix;
          }));

          for(let _y = 0; _y<shape.length; _y++)
            for(let _x = 0; _x <= xMax; _x++)
              if(!shape[_y][_x])
                shape[_y][_x] = false;

        });

        return {shape, offset};
    }

    function computeShapes(container, ignore = null, x = null, y = null) {

    	const computed = [];

    	for(let i=0; i<container.rows; i++) {

    		computed[i] = [];

    		for(let j=0; j<container.cols; j++)
    			computed[i].push(false);

    	}

    	for(let i=0; i<container.items.length; i++) {

    		const item    = container.items[i];
    		const shape1  = item.shape;
    		const shape2  = [];
          const bb      = {x: Infinity, y: Infinity};
    		let   offsetX = 0;
    		let   offsetY = 0;

          shape1.grid.forEach(e => {

            if(e.x < bb.x)
              bb.x = e.x;

            if(e.y < bb.y)
              bb.y = e.y;

          });

          if(bb.x < 0)
            offsetX = Math.abs(bb.x);

          if(bb.y < 0)
            offsetY = Math.abs(bb.y);

          bb.x += offsetX;
          bb.y += offsetY;

          shape1.grid.forEach(e => {

            const x  = e.x + offsetX;
            const y  = e.y + offsetY;
            const gX = Math.floor(x / GRID_SIZE$3);
            const gY = Math.floor(y / GRID_SIZE$3);

            shape2[gY]     = shape2[gY] || [];
    			shape2[gY][gX] = true;
    			
    		});

    		for(let y=0; y<shape2.length; y++) {
    			for(let x=0; x<shape2[y].length; x++) {

    				const _x = x + item.x;
    				const _y = y + item.y;

    				if(shape2[y][x]) {

                if(computed[_y] === undefined || computed[_y][_x] === undefined)
                  return false;

                computed[_y][_x] = true;
              }
    			}
    		}

    	}

    	if(ignore) {

    		const {shape, offset} = getShape(ignore.shape);

    		for(let sy=0; sy<shape.length; sy++) {
    			for(let sx=0; sx<shape[sy].length; sx++) {

    				const _x = sx + x;
    				const _y = sy + y;
    				
    				if(shape[sy][sx] && computed[_y] && computed[_y][_x])
    					computed[_y][_x] = false;

    			}
    		}

    	}

    	return computed;
    }

    function onCellBlur(e) {
    	
    }

    const func$1 = (e) => (e._type === 'PedComponent') && (e.component === 4);

    const func_1$1 = (e) => (e._type === 'PedComponent') && (e.component === 5);

    const func_2 = (e) => (e._type === 'PedComponent') && (e.component === 6);

    const func_3 = (e) => (e._type === 'PedComponent') && (e.component === 7);

    const func_4 = (e) => (e._type === 'PedComponent') && (e.component === 8);

    const func_5 = (e) => (e._type === 'PedComponent') && (e.component === 9);

    const func_6 = (e) => (e._type === 'PedComponent') && (e.component === 11);

    const func_7 = (e) => (e._type === 'PedProp') && (e.component === 0);

    const func_8 = (e) => (e._type === 'PedProp') && (e.component === 1);

    const func_9 = (e) => (e._type === 'PedProp') && (e.component === 2);

    const func_10 = (e) => (e._type === 'PedProp') && (e.component === 6);

    const func_11 = (e) => (e._type === 'PedProp') && (e.component === 7);

    const func_12 = (e) => e.cols;

    const func_13 = (a,b) => a > b ? a : b;

    function instance$3($$self, $$props, $$invalidate) {

    	/*
    	export let containers = [
    		{
    			_type: 'IdentityComponentContainer',
    			identity: 'abc123',
    			_id  : '123azerty1230',
    			label: 'IdentityComponentContainer',
    			rows: -1,
    			cols: -1,
    			items: [
    				{
    					"_id" : "5d84a92abab08601dcc622e",
    					"_data" : {},
    					"_type" : "PedComponent",
    					"container" : "5d84a929bab08601dcc622de",
    					"name" : "pedcomponent",
    					"label" : "Item",
    					"count" : 1,
    					"image" : "undefined",
    					"shape" : {
    						"grid" : [ 
    							{
    								"x" : -17,
    								"y" : -7
    							}
    						],
    						"offset" : {
    							"x" : 17,
    							"y" : 7
    						}
    					},
    					"x" : 0,
    					"y" : 0,
    					"world" : null,
    					"lifetime" : -1,
    					"createTime" : 1568975146,
    					"parentModel" : 1885233650,
    					"component" : 1,
    					"drawable" : 0,
    					"texture" : 0,
    					"palette" : 0
    				},
    				{
    					"_id" : "5d84a92abab08601dcc62a",
    					"_data" : {},
    					"_type" : "PedComponent",
    					"container" : "5d84a929bab08601dcc622de",
    					"name" : "pedcomponent",
    					"label" : "Item",
    					"count" : 1,
    					"image" : "undefined",
    					"shape" : {
    						"grid" : [ 
    							{
    								"x" : -17,
    								"y" : -7
    							}
    						],
    						"offset" : {
    							"x" : 17,
    							"y" : 7
    						}
    					},
    					"x" : 0,
    					"y" : 0,
    					"world" : null,
    					"lifetime" : -1,
    					"createTime" : 1568975146,
    					"parentModel" : 1885233650,
    					"component" : 2,
    					"drawable" : 0,
    					"texture" : 0,
    					"palette" : 0
    				},
    				{
    					"_id" : "5d84a92abab086cc622ea",
    					"_data" : {},
    					"_type" : "PedComponent",
    					"container" : "5d84a929bab08601dcc622de",
    					"name" : "pedcomponent",
    					"label" : "Item",
    					"count" : 1,
    					"image" : "undefined",
    					"shape" : {
    						"grid" : [ 
    							{
    								"x" : -17,
    								"y" : -7
    							}
    						],
    						"offset" : {
    							"x" : 17,
    							"y" : 7
    						}
    					},
    					"x" : 0,
    					"y" : 0,
    					"world" : null,
    					"lifetime" : -1,
    					"createTime" : 1568975146,
    					"parentModel" : 1885233650,
    					"component" : 3,
    					"drawable" : 0,
    					"texture" : 0,
    					"palette" : 0
    				},
    				{
    					"_id" : "5d84a92aba01dcc622ea",
    					"_data" : {},
    					"_type" : "PedComponent",
    					"container" : "5d84a929bab08601dcc622de",
    					"name" : "pedcomponent",
    					"label" : "Item",
    					"count" : 1,
    					"image" : "undefined",
    					"shape" : {
    						"grid" : [ 
    							{
    								"x" : -17,
    								"y" : -7
    							}
    						],
    						"offset" : {
    							"x" : 17,
    							"y" : 7
    						}
    					},
    					"x" : 0,
    					"y" : 0,
    					"world" : null,
    					"lifetime" : -1,
    					"createTime" : 1568975146,
    					"parentModel" : 1885233650,
    					"component" : 4,
    					"drawable" : 0,
    					"texture" : 0,
    					"palette" : 0
    				},
    				{
    					"_id" : "5d84ab08601dcc622ea",
    					"_data" : {},
    					"_type" : "PedComponent",
    					"container" : "5d84a929bab08601dcc622de",
    					"name" : "pedcomponent",
    					"label" : "Item",
    					"count" : 1,
    					"image" : "undefined",
    					"shape" : {
    						"grid" : [ 
    							{
    								"x" : -17,
    								"y" : -7
    							}
    						],
    						"offset" : {
    							"x" : 17,
    							"y" : 7
    						}
    					},
    					"x" : 0,
    					"y" : 0,
    					"world" : null,
    					"lifetime" : -1,
    					"createTime" : 1568975146,
    					"parentModel" : 1885233650,
    					"component" : 5,
    					"drawable" : 0,
    					"texture" : 0,
    					"palette" : 0
    				},
    				{
    					"_id" : "5d84b08601dcc622ea",
    					"_data" : {},
    					"_type" : "PedComponent",
    					"container" : "5d84a929bab08601dcc622de",
    					"name" : "pedcomponent",
    					"label" : "Item",
    					"count" : 1,
    					"image" : "undefined",
    					"shape" : {
    						"grid" : [ 
    							{
    								"x" : -17,
    								"y" : -7
    							}
    						],
    						"offset" : {
    							"x" : 17,
    							"y" : 7
    						}
    					},
    					"x" : 0,
    					"y" : 0,
    					"world" : null,
    					"lifetime" : -1,
    					"createTime" : 1568975146,
    					"parentModel" : 1885233650,
    					"component" : 6,
    					"drawable" : 0,
    					"texture" : 0,
    					"palette" : 0
    				},
    				{
    					"_id" : "5d8408601dcc622ea",
    					"_data" : {},
    					"_type" : "PedComponent",
    					"container" : "5d84a929bab08601dcc622de",
    					"name" : "pedcomponent",
    					"label" : "Item",
    					"count" : 1,
    					"image" : "undefined",
    					"shape" : {
    						"grid" : [ 
    							{
    								"x" : -17,
    								"y" : -7
    							}
    						],
    						"offset" : {
    							"x" : 17,
    							"y" : 7
    						}
    					},
    					"x" : 0,
    					"y" : 0,
    					"world" : null,
    					"lifetime" : -1,
    					"createTime" : 1568975146,
    					"parentModel" : 1885233650,
    					"component" : 7,
    					"drawable" : 0,
    					"texture" : 0,
    					"palette" : 0
    				},
    				{
    					"_id" : "5d808601dcc622ea",
    					"_data" : {},
    					"_type" : "PedComponent",
    					"container" : "5d84a929bab08601dcc622de",
    					"name" : "pedcomponent",
    					"label" : "Item",
    					"count" : 1,
    					"image" : "undefined",
    					"shape" : {
    						"grid" : [ 
    							{
    								"x" : -17,
    								"y" : -7
    							}
    						],
    						"offset" : {
    							"x" : 17,
    							"y" : 7
    						}
    					},
    					"x" : 0,
    					"y" : 0,
    					"world" : null,
    					"lifetime" : -1,
    					"createTime" : 1568975146,
    					"parentModel" : 1885233650,
    					"component" : 8,
    					"drawable" : 0,
    					"texture" : 0,
    					"palette" : 0
    				},
    				{
    					"_id" : "5d84a92abab0860",
    					"_data" : {},
    					"_type" : "PedComponent",
    					"container" : "5d84a929bab08601dcc622de",
    					"name" : "pedcomponent",
    					"label" : "Item",
    					"count" : 1,
    					"image" : "undefined",
    					"shape" : {
    						"grid" : [ 
    							{
    								"x" : -17,
    								"y" : -7
    							}
    						],
    						"offset" : {
    							"x" : 17,
    							"y" : 7
    						}
    					},
    					"x" : 0,
    					"y" : 0,
    					"world" : null,
    					"lifetime" : -1,
    					"createTime" : 1568975146,
    					"parentModel" : 1885233650,
    					"component" : 9,
    					"drawable" : 0,
    					"texture" : 0,
    					"palette" : 0
    				},
    				{
    					"_id" : "5d84a92abab086",
    					"_data" : {},
    					"_type" : "PedComponent",
    					"container" : "5d84a929bab08601dcc622de",
    					"name" : "pedcomponent",
    					"label" : "Item",
    					"count" : 1,
    					"image" : "undefined",
    					"shape" : {
    						"grid" : [ 
    							{
    								"x" : -17,
    								"y" : -7
    							}
    						],
    						"offset" : {
    							"x" : 17,
    							"y" : 7
    						}
    					},
    					"x" : 0,
    					"y" : 0,
    					"world" : null,
    					"lifetime" : -1,
    					"createTime" : 1568975146,
    					"parentModel" : 1885233650,
    					"component" : 10,
    					"drawable" : 0,
    					"texture" : 0,
    					"palette" : 0
    				},
    				{
    					"_id" : "5d84a92abab08",
    					"_data" : {},
    					"_type" : "PedComponent",
    					"container" : "5d84a929bab08601dcc622de",
    					"name" : "pedcomponent",
    					"label" : "Item",
    					"count" : 1,
    					"image" : "undefined",
    					"shape" : {
    						"grid" : [ 
    							{
    								"x" : -17,
    								"y" : -7
    							}
    						],
    						"offset" : {
    							"x" : 17,
    							"y" : 7
    						}
    					},
    					"x" : 0,
    					"y" : 0,
    					"world" : null,
    					"lifetime" : -1,
    					"createTime" : 1568975146,
    					"parentModel" : 1885233650,
    					"component" : 11,
    					"drawable" : 0,
    					"texture" : 0,
    					"palette" : 0
    				},
    			]
    		},
    		{
    			_id  : '456qwerty456',
    			label: 'Bar',
    			rows: -1,
    			cols: -1,
    			items: [
    				{
    					_id  : 'vbcvb',
    					label: 'Pistolet',
    					image: 'weaponpistol',
    					shape: {"grid":[{"x":-9.000000000000085,"y":-1.8106060606060055},{"x":-9.000000000000085,"y":98.189393939394},{"x":90.99999999999991,"y":-1.8106060606060055},{"x":190.99999999999991,"y":-1.8106060606060055}],"offset":{"x":9.000000000000085,"y":1.8106060606060055}},
    					x : 0,
              y : 0,
              actions: ['foo', 'bar', 'baz'],
    					count: 12,
    					limit: 1,
    				},
    				{
    					_id  : 'hjkjk',
    					label: 'Carabine spéciale',
    					image: 'weaponspecialcarbinemk2',
    					shape: {"grid":[{"x":-17.000000000000114,"y":-1.1917344173441506},{"x":82.99999999999989,"y":-1.1917344173441506},{"x":82.99999999999989,"y":98.80826558265585},{"x":182.9999999999999,"y":-1.1917344173441506},{"x":182.9999999999999,"y":98.80826558265585},{"x":282.9999999999999,"y":-1.1917344173441506},{"x":382.9999999999999,"y":-1.1917344173441506}],"offset":{"x":17.000000000000114,"y":1.1917344173441506}},
    					x : 0,
              y : 5,
              actions: ['foo', 'bar', 'baz'],
    					count: 12,
    					limit: 1,
    				},
    				{
    					_id  : 'hjjm',
    					label: 'Coca',
    					image: 'coca',
    					shape: {"grid":[{"x":-18.999999999999773,"y":-5.5198300283284425},{"x":-18.999999999999773,"y":94.48016997167156},{"x":81.00000000000023,"y":-5.5198300283284425},{"x":81.00000000000023,"y":94.48016997167156}],"offset":{"x":18.999999999999773,"y":5.5198300283284425}},
    					x : 0,
              y : 2,
              actions: ['foo', 'bar', 'baz'],
    					count: 12,
    					limit: 1,
    				},
    				{
    					_id  : 'asd',
    					label: 'Doliprane',
    					image: 'doliprane',
    					shape: {"grid":[{"x":-8,"y":-28.4375}],"offset":{"x":8,"y":28.4375}},
    					x : 6,
              y : 4,
              actions: ['foo', 'bar', 'baz'],
    					count: 12,
    					limit: 1,
    				}
    			]
    		}

    	];
    	*/

    	let { containers       = [], playerRoles      = [], identity         = 'abc123', drag             = null, dragContainer    = null, dragCount        = 0, cache            = {containers: {}, items: {}}, currentContainer = null, currentItem      = null, mouseX           = 0, mouseY           = 0 } = $$props;

    	let _identityContainers = [];
    	let _regularContainers  = [];

      function check(container, item, x, y) {
        
        const foundSame = container.items.find(e => !!e && (e._type === item._type) && (e.x === x) && (e.y === y));

        if(foundSame && cache.items[item._id] && cache.items[item._id].limit > 1)
          return true;

        if(container.rows === -1)
          return true;
        
    		const {shape, offset} = getShape(item.shape);
    		const computed        = computeShapes(container, item, currentItem.x , currentItem.y);

    		for(let cy=0; cy<computed.length; cy++) {
    			for(let cx=0; cx<computed[cy].length; cx++) {
    				
    				const gY = cy + x;
    				const gX = cx + y;

    				if(gY < 0 || gX < 0)
    					return false;

    				for(let sy=0; sy<shape.length; sy++)
    					for(let sx=0; sx<shape[sy].length; sx++)
    						if((computed[sy + y] === undefined) || (computed[sy + y][sx + x] === undefined) || (computed[sy + y][sx + x] === true))
    							return false;

    			}
    		}

    		return true;
    	}
    	
    	function getImage(itemId) {

        if(itemId && cache.items[itemId])
          return 'http://assets/core-assets/dist/img/items/' + cache.items[itemId].image + '.png';
        else
          return 'http://assets/core-assets/dist/img/items/undefined.png';

    	}

    	function setDraggedItem(itemId, containerId, count = 1) {
    		$$invalidate('drag', drag          = itemId);
        $$invalidate('dragContainer', dragContainer = containerId);
        $$invalidate('dragCount', dragCount     = count);
    	}

      function setDragCount(count) {
        $$invalidate('dragCount', dragCount = count);
      }

    	function getCache() {
    		return cache;
    	}

      function onMessage(e) {

        let msg = {};
        
        try { msg = JSON.parse(e.data); } catch(e) {}
        if(msg.action === 'undefined')
          return;

        switch(msg.action) {

    			case 'set_identity' : {
    				$$invalidate('identity', identity = msg.identity);
    				break;
    			}

    			case 'set_player_roles' : {
    				$$invalidate('playerRoles', playerRoles = msg.roles);
    				break;
    			}

          case 'set_containers' : {
    				$$invalidate('containers', containers = msg.containers);
            break;
          }
    			
          default: break;

        }

    	}

    	function onMouseUp(e) {

    		if(!currentContainer || !currentItem || !dragContainer)
    			return;

    		if(check(currentContainer, currentItem, mouseX, mouseY)) {

    			const container = cache.containers[dragContainer];

    			const idx = container.items.findIndex(e => e._id === currentItem._id);

    			if(idx !== -1)
    				container.items.splice(idx, 1);

    			$$invalidate('currentItem', currentItem.x = mouseX, currentItem);
    			$$invalidate('currentItem', currentItem.y = mouseY, currentItem);

    			currentContainer.items.push(currentItem);

    			$$invalidate('containers', containers);

    			window.parent.postMessage(JSON.stringify({
    				action: 'move_item',
            from  : container._id,
            to    : currentContainer._id,
            item  : currentItem._id,
            x     : mouseX,
            y     : mouseY,
            count : dragCount,
    			}), '*');

    		}

    		setDraggedItem(null);
    	}

    	function onCellHover(e) {

    		$$invalidate('currentContainer', currentContainer = cache.containers[e.detail.container]);
    		$$invalidate('currentItem', currentItem      = drag ? cache.items[drag] : null);
    		$$invalidate('mouseX', mouseX           = e.detail.x);
    		$$invalidate('mouseY', mouseY           = e.detail.y);

    	}

    	beforeUpdate(() => {
    		
    		$$invalidate('_identityContainers', _identityContainers = containers.filter(e => e.identity === identity && (e._type === 'IdentityComponentContainer' || e._type === 'IdentityPropContainer')));
    		$$invalidate('_regularContainers', _regularContainers  = containers.filter(e => _identityContainers.indexOf(e) === -1));

    		const cachedContainers = {};
    		const cachedItems      = {};

    		for(let i=0; i<containers.length; i++) {

    			const container = containers[i];

    			cachedContainers[container._id] = container;

    			for(let j=0; j<container.items.length; j++) {

    				const item = container.items[j];

    				cachedItems[item._id] = item;

    			}
    		}

    		$$invalidate('cache', cache.containers = cachedContainers, cache);
    		$$invalidate('cache', cache.items      = cachedItems, cache);

        if(drag && (cache.items[drag] === undefined))
          setDraggedItem(null);

    	});

    	onMount(() => {
    		window.parent.postMessage(JSON.stringify({action: 'ready'}), '*');
    	});

    	const message_handler = (e) => onMessage(e);

    	const click_handler = (e) => window.parent.postMessage(JSON.stringify({action: 'internal:destroy'}, '*'));

    	$$self.$set = $$props => {
    		if ('containers' in $$props) $$invalidate('containers', containers = $$props.containers);
    		if ('playerRoles' in $$props) $$invalidate('playerRoles', playerRoles = $$props.playerRoles);
    		if ('identity' in $$props) $$invalidate('identity', identity = $$props.identity);
    		if ('drag' in $$props) $$invalidate('drag', drag = $$props.drag);
    		if ('dragContainer' in $$props) $$invalidate('dragContainer', dragContainer = $$props.dragContainer);
    		if ('dragCount' in $$props) $$invalidate('dragCount', dragCount = $$props.dragCount);
    		if ('cache' in $$props) $$invalidate('cache', cache = $$props.cache);
    		if ('currentContainer' in $$props) $$invalidate('currentContainer', currentContainer = $$props.currentContainer);
    		if ('currentItem' in $$props) $$invalidate('currentItem', currentItem = $$props.currentItem);
    		if ('mouseX' in $$props) $$invalidate('mouseX', mouseX = $$props.mouseX);
    		if ('mouseY' in $$props) $$invalidate('mouseY', mouseY = $$props.mouseY);
    	};

    	return {
    		containers,
    		playerRoles,
    		identity,
    		drag,
    		dragContainer,
    		dragCount,
    		cache,
    		currentContainer,
    		currentItem,
    		mouseX,
    		mouseY,
    		_identityContainers,
    		_regularContainers,
    		check,
    		getImage,
    		setDraggedItem,
    		setDragCount,
    		getCache,
    		onMessage,
    		onMouseUp,
    		onCellHover,
    		Math,
    		JSON,
    		window,
    		message_handler,
    		click_handler
    	};
    }

    class App extends SvelteComponent {
    	constructor(options) {
    		super();
    		init(this, options, instance$3, create_fragment$3, safe_not_equal, ["containers", "playerRoles", "identity", "drag", "dragContainer", "dragCount", "cache", "currentContainer", "currentItem", "mouseX", "mouseY"]);
    	}
    }

    const app = new App({
    	target: document.body,
    	props: {

    	}
    });

    return app;

}());
//# sourceMappingURL=bundle.js.map
