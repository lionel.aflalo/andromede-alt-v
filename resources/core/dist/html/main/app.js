(async () => {

  Ventus.WindowManager.prototype.modes = {
    default: Ventus.WindowManager.prototype.modes.default
  };

  const app = {
    root   : null,
    main   : null,
    wm     : null,
    windows: [],
    scale  : 1.0,
  };

  window.app = app;

  app.init = function() {

    return new Promise((resolve, reject) => {

      alt.on('script.message', msg => this.onMessage(JSON.parse(msg)));

      this.root = document.querySelector('#root');
      this.main = document.querySelector('#main');
      this.wm   = new Ventus.WindowManager(this.main);

      window.addEventListener('message', e => this.onWindowMessage(e), false);
      
      setInterval(() => {

        this.windows.forEach(win => {
          
          if(win.view.el.classList.contains('fitcontent')) {
            
            const iframe = win.$content.el.querySelector('iframe');

            if(iframe instanceof HTMLIFrameElement) {

              if(iframe.contentWindow.document.body === null)
                return;

              const width  = iframe.contentWindow.document.body.clientWidth;
              const height = iframe.contentWindow.document.body.clientHeight;

              win.width  = width;
              win.height = height;

            }
          }

          if(win.view.el.classList.contains('center')) {
            win.x = 'unset';
            win.y = 'unset';
          }
  
        });;

      }, 100);

      window.addEventListener('keypress', e => {

        switch(e.keyCode) {

          case 43: {
            this.scale += 0.1;
            document.body.style.transform = 'scale(' + this.scale + ')';
            document.body.style.transformOrigin = '0 0';
            break;
          }

          case 45 : {
            this.scale -= 0.1;
            document.body.style.transform = 'scale(' + this.scale + ')';
            document.body.style.transformOrigin = '0 0';
            break;
          }

          default: break;
        }

      });

      setTimeout(() => resolve(), 100);

    });

  }

  app.openWindow = function(id, props = undefined) {

    // Re-open window
    if(!props) {

      const win = this.windows.find(e => e.$hud !== undefined && e.$hud.id === id);

      if(win !== undefined)
        win.open();

      return;
    }

    // Create new window
    let   win    = null;
    const width  = (props.width  === undefined) ? 800 : props.width;
    const height = (props.height === undefined) ? 600 : props.height;
    const x      = document.body.clientWidth  / 2 - width  / 2;
    const y      = document.body.clientHeight / 2 - height / 2;

    const data = {
  
      width, height,
      x, y,
      title: '',
      resizable  : false,
      closable   : true,
      animations : false,
      stayinspace: true,
      widget     : false,
      classname  : '',

      ...props,

      url : undefined,
      html: undefined,

      events: {

        open: () => {
          this.postScriptMessage({action: 'window_open', id});
        },

        closed: () => {
          this.postScriptMessage({action: 'window_closed', id});
        },

        destroyed: () => {
          
          const idx = this.windows.indexOf(win);

          if(idx !== -1)
            this.windows.splice(idx, 1);

          this.postScriptMessage({action: 'window_destroyed', id});
        }

      }
    };

    if(props.url !== undefined) {

      data.content     = document.createElement('iframe');
      data.content.src = props.url;
      
      data.content.setAttribute('sandbox',           'allow-forms allow-pointer-lock allow-same-origin allow-scripts');
      data.content.setAttribute('allowtransparency', 'true');

      data.content.addEventListener('load', () => {
        this.postScriptMessage({action: 'window_loaded', id});
      });

      if(data.classname.indexOf('fitcontent') !== -1)
        iFrameResize({log: false, heightCalculationMethod: 'max', widthCalculationMethod: 'max'}, data.content);

    } else if(props.html !== undefined) {
      
      data.content = props.html;
    
    } else data.content = props.content || '';

    win      = this.wm.createWindow(data);
    win.$hud = {id};

    this.windows.push(win);

    win.open();

    if(props.url === undefined)
      this.postScriptMessage({action: 'window_loaded', id});

    return win;
  }

  app.closeWindow = function(id) {

    const win = this.windows.find(e => e.$hud !== undefined && e.$hud.id === id);

    if(win !== undefined)
      win.close();

  }

  app.destroyWindow = function(id) {

    const win = this.windows.find(e => e.$hud !== undefined && e.$hud.id === id);

    if(win !== undefined)
      win.destroy();

  }

  app.onWindowMessage = function(evt) {

    let msg = {};
    
    try { msg = JSON.parse(evt.data); } catch(e) {}

    const win = this.windows.find(e => {
    
      const iframe = e.$content.el.querySelector('iframe');

      if(iframe.contentWindow == evt.source)
        return true;
      else
        return false
    });

    if(win !== undefined) {

      this.postScriptMessage({action: 'window_message', id: win.$hud.id, msg});

    }

  }

  app.postWindowMessage = function(id, msg) {

    const win = this.windows.find(e => e.$hud.id === id);

    if(win !== undefined) {
      const iframe = win.$content.el.querySelector('iframe');
      iframe.contentWindow.postMessage(JSON.stringify(msg), '*');
    }
  }

  app.postScriptMessage = function(msg) {
    alt.emit('hud.message', JSON.stringify(msg));
  }

  app.onMessage = function(msg) {

    if(msg.action === undefined)
      return;

    switch(msg.action) {

      case 'script.message' : {
        this.onMessage(JSON.parse(msg.msg));
      }

      case 'show' : {

        this.root.classList.add('active');

        if(msg.overlay === true)
          this.root.classList.remove('nooverlay');
        else if(msg.overlay === false)
          this.root.classList.add('nooverlay');

        break;
      }

      case 'hide' : {

        this.root.classList.remove('active');

        if(msg.overlay === true)
          this.root.classList.remove('nooverlay');
        else if(msg.overlay === false)
          this.root.classList.add('nooverlay');

        break;
      }

      case 'open_window' : {
        this.openWindow(msg.id, msg.props);
        break;
      }

      case 'close_window' : {
        this.closeWindow(msg.id);
        break;
      }
      
      case 'destroy_window' : {
        this.destroyWindow(msg.id);
        break;
      }

      case 'post_window_message' : {
        this.postWindowMessage(msg.id, msg.msg);
        break;
      }

      default: break;

    }

  }

  /* Wait window and app init */
  let count = 2;

  window.addEventListener('load', async e => {
    if(--count == 0)
      app.postScriptMessage({action: 'init'});
  });

  await app.init();

  if(--count == 0)
  app.postScriptMessage({action: 'init'});

})();