'use strict';

var alt = require('alt');

const si = alt.setInterval   || setInterval;
const nt = alt.nextTick      || process.nextTick;

// <-- withCache
const cachedClasses = new Map();

si(() => {

  for(const ext in cachedClasses)
    for(const cls in cachedClasses[ext])
      if(!cachedClasses[ext][cls].__exists)
        delete cachedClasses[ext][cls];

}, 60000);

const callbacks = {};

alt.onClient('altv-api:request', (player, name, id, ...args) => {

  if(callbacks[name] === undefined)
    return;
    
  callbacks[name](player, (...data) => {
    alt.emitClient(player, 'altv-api:response', id, ...data);
  }, ...args);

});
