import * as alt from 'alt';
import * as natives from 'natives';

// Fingerpointing

export default class Fingerpointing {
  constructor() {
    this.active = false;
    this.interval = null;
    this.cleanStart = false;
    this.gameplayCam = natives.createCameraWithParams('gameplay');
    this.localPlayer = alt.Player.local;
  }

  start() {
    if (!this.active) {
      this.active = true;

      this.requestAnimDictPromise('anim@mp_point')
        .then(() => {
          natives.setPedCurrentWeaponVisible(this.localPlayer.scriptID, 0, 1, 1, 1);
          natives.setPedConfigFlag(this.localPlayer.scriptID, 36, true);
          natives.taskMoveNetworkByName(
            this.localPlayer.scriptID,
            'task_mp_pointing',
            0.5,
            false,
            'anim@mp_point',
            24
          );
          natives.removeAnimDict('anim@mp_point');
          this.cleanStart = true;
          this.interval = alt.setInterval(this.process.bind(this), 0);
        })
        .catch(() => {
          alt.log('Promise returned reject Pointing');
        });
    }
  }

  stop() {
    if (this.active) {
      if (this.interval) {
        alt.clearInterval(this.interval);
      }
      this.interval = null;

      this.active = false;

      if (this.cleanStart) {
        this.cleanStart = false;
        natives.requestTaskMoveNetworkStateTransition(this.localPlayer.scriptID, 'Stop');

        if (!natives.isPedInjured(this.localPlayer.scriptID)) {
          natives.clearPedSecondaryTask(this.localPlayer.scriptID);
        }
        if (!natives.isPedInAnyVehicle(this.localPlayer.scriptID, true)) {
          natives.setPedCurrentWeaponVisible(this.localPlayer.scriptID, 1, 1, 1, 1);
        }
        natives.setPedConfigFlag(this.localPlayer.scriptID, 36, false);
        natives.clearPedSecondaryTask(this.localPlayer.scriptID);
      }
    }
  }

  getRelativePitch() {
    let camRot = natives.getGameplayCamRot(2);
    return camRot.x - natives.getEntityPitch(this.localPlayer.scriptID);
  }

  process() {
    if (this.active) {
      natives.isTaskMoveNetworkActive(this.localPlayer.scriptID);

      let camPitch = this.getRelativePitch();

      if (camPitch < -70.0) {
        camPitch = -70.0;
      } else if (camPitch > 42.0) {
        camPitch = 42.0;
      }
      camPitch = (camPitch + 70.0) / 112.0;

      let camHeading = natives.getGameplayCamRelativeHeading();

      let cosCamHeading = Math.cos(camHeading);
      let sinCamHeading = Math.sin(camHeading);

      if (camHeading < -180.0) {
        camHeading = -180.0;
      } else if (camHeading > 180.0) {
        camHeading = 180.0;
      }
      camHeading = (camHeading + 180.0) / 360.0;

      let coords = natives.getOffsetFromEntityInWorldCoords(
        this.localPlayer.scriptID,
        cosCamHeading * -0.2 - sinCamHeading * (0.4 * camHeading + 0.3),
        sinCamHeading * -0.2 + cosCamHeading * (0.4 * camHeading + 0.3),
        0.6
      );

      let ray = natives.startShapeTestCapsule(
        coords.x,
        coords.y,
        coords.z - 0.2,
        coords.x,
        coords.y,
        coords.z + 0.2,
        1.0,
        95,
        this.localPlayer.scriptID,
        7
      );
      let [_, blocked, coords1, coords2, entity] = natives.getShapeTestResult(
        ray,
        false,
        null,
        null,
        null
      );
      //alt.log("Blocked: " + blocked);
      //alt.log("Entity: " + natives.getEntityType(entity));

      natives.setTaskMoveNetworkSignalFloat(this.localPlayer.scriptID, 'Pitch', camPitch);
      natives.setTaskMoveNetworkSignalFloat(
        this.localPlayer.scriptID,
        'Heading',
        camHeading * -1.0 + 1.0
      );
      natives.setTaskMoveNetworkSignalBool(this.localPlayer.scriptID, 'isBlocked', blocked);
      natives.setTaskMoveNetworkSignalBool(
        this.localPlayer.scriptID,
        'isFirstPerson',
        natives._0xEE778F8C7E1142E2(natives._0x19CAFA3C87F7C2FF()) === 4
      );
    }
  }

  requestAnimDictPromise(dict) {
    natives.requestAnimDict(dict);
    return new Promise((resolve, reject) => {
      let check = alt.setInterval(() => {
        if (natives.hasAnimDictLoaded(dict)) {
          alt.clearInterval(check);
          // alt.log('Anim dict loaded');
          resolve(true);
        } else {
          // alt.log('Requesting Animdict.');
        }
      }, 5);
    });
  }
}