import * as alt from 'alt';
import * as natives from 'natives';

// Crouch 
export default class Crouch {

  constructor() {

    this.crouched = false;

    natives.requestAnimSet('move_ped_crouched');

    alt.setInterval(() => {
      natives.disableControlAction(0, 36, true);
    }, 0);
  }

  toggle() {

    const player = alt.Player.local;

    if(this.crouched) {

      natives.resetPedMovementClipset(player.scriptID, 0);

    } else {

      if (natives.isPedInAnyVehicle(player.scriptID, false)) {
        if (!natives.isPedOnAnyBike(player.scriptID)) {
          return;
        }
      } else {
        const currentWeapon = natives.getSelectedPedWeapon(player.scriptID);
  
        if (
          natives.getWeaponClipSize(currentWeapon) > 0 &&
          !natives.isPedJumping(player.scriptID) &&
          !natives.isPlayerClimbing(player.scriptID)
        ) {
          return;
        }
      }

      natives.setPedMovementClipset(player.scriptID, 'move_ped_crouched', 0.25);

    }

    this.crouched = !this.crouched;
  }
}