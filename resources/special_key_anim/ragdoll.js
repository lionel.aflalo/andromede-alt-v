import * as alt from 'alt';
import * as natives from 'natives';


// Ragdoll 
export default class Ragdoll {

  constructor() {
    this.ragdoll = false;

    alt.setInterval(() => {
      if(this.ragdoll)
        this.doRagdoll();
    }, 0);
  }

  toggle() {
    this.ragdoll = !this.ragdoll;
  }

  doRagdoll() {

    const player = alt.Player.local;

    if (natives.isPedInAnyVehicle(player.scriptID, false)) {
      if (!natives.isPedOnAnyBike(player.scriptID)) {
        return;
      }
    } else {
      const currentWeapon = natives.getSelectedPedWeapon(player.scriptID);

      if (
        natives.getWeaponClipSize(currentWeapon) > 0 &&
        !natives.isPedJumping(player.scriptID) &&
        !natives.isPlayerClimbing(player.scriptID)
      ) {
        return;
      }
    }

    natives.setPedToRagdoll(player.scriptID, 1500, 1500, 0, true, true, true);
  }
}