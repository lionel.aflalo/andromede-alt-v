import * as alt     from 'alt';
import * as natives from 'natives';

import Crouch         from 'crouch';
import Fingerpointing from 'fingerpointing';
import Ragdoll        from 'ragdoll';
import HandsUp        from 'handsup';
import CrossHand      from 'crosshand';

// Crouch
const crouch = new Crouch();

alt.on('keyup', key => {

  if (key === 17) {
    crouch.toggle();
  }
});

// Ragdoll
const ragdoll = new Ragdoll();
const ragdollKey = 187;

alt.on('keyup', key => {
  if (key === ragdollKey) {
    ragdoll.toggle();
  }
});

// Fingerpointing
const pointing = new Fingerpointing();
alt.on('keydown', key => {
  if (key == 'B'.charCodeAt(0)) {
    pointing.start();
  }
});

alt.on('keyup', key => {
  if (key == 'B'.charCodeAt(0)) {
    pointing.stop();
  }
});

// Handsup
const handsUp = new HandsUp();
alt.on('keydown', key => {
  if (key == 'X'.charCodeAt(0)) {
    handsUp.start();
  }
});

alt.on('keyup', key => {
  if (key == 'X'.charCodeAt(0)) {
    handsUp.stop();
  }
});

// Crosshand
const crossHand = new CrossHand();
alt.on('keydown', key => {
  if (key == 'G'.charCodeAt(0)) {
    crossHand.start();
  }
});

alt.on('keyup', key => {
  if (key == 'G'.charCodeAt(0)) {
    crossHand.stop();
  }
});
