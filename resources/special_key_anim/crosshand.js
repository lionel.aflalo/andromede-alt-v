import * as alt from 'alt';
import * as natives from 'natives';

export default class CrossHand {
  constructor() {
    this.active = false;
    this.interval = null;
    this.cleanStart = false;
    this.localPlayer = alt.Player.local;
  }

  start() {
    if (!this.active) {
      this.active = true;

      this.requestAnimDictPromise('amb@world_human_hang_out_street@female_arms_crossed@base')
        .then(() => {
          natives.setPedCurrentWeaponVisible(this.localPlayer.scriptID, 0, 1, 1, 1);
          // natives.setPedConfigFlag(this.localPlayer.scriptID, 36, true);
          natives.taskPlayAnim(
            this.localPlayer.scriptID,
            'amb@world_human_hang_out_street@female_arms_crossed@base',
            'base',
            8.0,
            8.0,
            -1,
            50,
            0,
            false,
            false,
            false
          );
          natives.removeAnimDict('amb@world_human_hang_out_street@female_arms_crossed@base');
          this.cleanStart = true;
          this.interval = alt.setInterval(this.process.bind(this), 0);
        })
        .catch(() => {
        //   alt.log('Promise returned reject Handsup');
        });
    }
  }

  stop() {
    if (this.active) {
      if (this.interval) {
        alt.clearInterval(this.interval);
      }
      this.interval = null;

      this.active = false;

      if (this.cleanStart) {
        this.cleanStart = false;
        // natives.setNetworkTaskAction(this.localPlayer.scriptID, 'Stop');

        if (!natives.isPedInjured(this.localPlayer.scriptID)) {
          natives.clearPedSecondaryTask(this.localPlayer.scriptID);
        }
        if (!natives.isPedInAnyVehicle(this.localPlayer.scriptID, true)) {
          natives.setPedCurrentWeaponVisible(this.localPlayer.scriptID, 1, 1, 1, 1);
        }
        natives.setPedConfigFlag(this.localPlayer.scriptID, 36, false);
        natives.clearPedSecondaryTask(this.localPlayer.scriptID);
      }
    }
  }

  requestAnimDictPromise(dict) {
    natives.requestAnimDict(dict);
    return new Promise((resolve, reject) => {
      let check = alt.setInterval(() => {
        if (natives.hasAnimDictLoaded(dict)) {
          alt.clearInterval(check);
        //   alt.log('Anim dict loaded');
          resolve(true);
        } else {
        //   alt.log('Requesting Animdict.');
        }
      }, 5);
    });
  }
}
