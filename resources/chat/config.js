export const Config = {
    welcomeMessage: "",
    hideHudOnF7: false,
    autoHide: true
}

export default Config;