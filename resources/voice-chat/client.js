import * as alt from 'alt';
import * as game from 'natives';

alt.initVoice();
alt.setMicGain(1.0);

alt.setInterval(() => {

  if(game.isControlJustPressed(0, 249)) {
    alt.enableVoiceInput();
  }
  else if(game.isControlJustReleased(0, 249)) {
    alt.disableVoiceInput();
  }

}, 0);