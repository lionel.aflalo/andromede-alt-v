import * as alt from 'alt';
import game from 'natives';

alt.on('connectionComplete', (mapChanged) => {
  alt.log('All scripts compmletely loaded');
  if(mapChanged) {
    alt.log('Reloading map store');
    game.loadSpDlcMaps();
    alt.log('Map store reloaded');
  }
});

alt.on('consoleCommand', (command, ...args) => {
  if(command == 'reloadMap') {
    alt.log('Reloading map store');
    game.loadSpDlcMaps();
    alt.log('Map store reloaded');
  }
})
